
CREATE TABLE `eos_account_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `def` tinyint(4) NOT NULL DEFAULT '0',
  `options` mediumtext COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_log` (
  `id` bigint(20) NOT NULL,
  `logdate` datetime NOT NULL,
  `logname` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `cmd` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_router` (
  `id` bigint(20) NOT NULL,
  `route` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `auth` tinyint(4) NOT NULL DEFAULT '1',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_router_group` (
  `id` bigint(20) NOT NULL,
  `id_router` bigint(20) NOT NULL,
  `id_group` int(11) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `id_lang` int(11) NOT NULL,
  `options` longtext COLLATE utf8mb4_unicode_ci,
  `password_date` datetime DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_user_cmd` (
  `id` bigint(20) NOT NULL,
  `token` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `cmd` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmddate` datetime NOT NULL,
  `cmdexpiration` datetime NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_account_user_group` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_group` int(11) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_country` int(11) UNSIGNED DEFAULT NULL,
  `id_state` int(11) UNSIGNED DEFAULT NULL,
  `id_customer` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `id_manufacturer` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `alias` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_city` bigint(20) NOT NULL,
  `other` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(32) DEFAULT NULL,
  `cif` varchar(16) DEFAULT NULL,
  `reference` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1',
  `options` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_account_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_group_status` (`status`),
  ADD KEY `idx_account_group_def` (`def`);

ALTER TABLE `eos_account_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_log_logdate` (`logdate`),
  ADD KEY `idx_account_log_ip` (`ip`),
  ADD KEY `idx_account_log_id_user` (`id_user`);

ALTER TABLE `eos_account_router`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_account_router_route` (`route`),
  ADD KEY `idx_account_router_container` (`container`),
  ADD KEY `idx_account_router_auth` (`auth`);

ALTER TABLE `eos_account_router_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_router_group_router` (`id_router`),
  ADD KEY `fk_account_router_group_group` (`id_group`);

ALTER TABLE `eos_account_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_account_user_username` (`username`),
  ADD UNIQUE KEY `idx_account_user_email` (`email`),
  ADD KEY `idx_account_user_status` (`status`),
  ADD KEY `idx_account_user_id_lang` (`id_lang`);

ALTER TABLE `eos_account_user_cmd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_user_cmd_user` (`id_user`),
  ADD KEY `idx_account_user_cmd_cmddate` (`cmddate`),
  ADD KEY `idx_account_user_cmd_cmdexpiration` (`cmdexpiration`);

ALTER TABLE `eos_account_user_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_user_group_user` (`id_user`),
  ADD KEY `fk_account_user_group_group` (`id_group`);

ALTER TABLE `eos_address`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_account_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_router`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_router_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_user_cmd`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_user_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_account_router_group`
  ADD CONSTRAINT `fk_account_router_group_group` FOREIGN KEY (`id_group`) REFERENCES `eos_account_group` (`id`),
  ADD CONSTRAINT `fk_account_router_group_router` FOREIGN KEY (`id_router`) REFERENCES `eos_account_router` (`id`) ON DELETE CASCADE;

ALTER TABLE `eos_account_user_cmd`
  ADD CONSTRAINT `fk_account_user_cmd_user` FOREIGN KEY (`id_user`) REFERENCES `eos_account_user` (`id`) ON DELETE CASCADE;
