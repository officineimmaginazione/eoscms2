ALTER TABLE `eos_account_user_group` DROP `id`, DROP `ins_id`, DROP `ins_date`, DROP `up_id`, DROP `up_date`;
ALTER TABLE `eos_account_user` ADD `email_alternative` VARCHAR(150) NULL DEFAULT NULL AFTER `email`;
