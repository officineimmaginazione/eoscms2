CREATE TABLE `eos_cart` (
  `id` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `total` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `eos_cart_row` (
  `id` bigint(20) NOT NULL,
  `id_cart` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_customer` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_cart_cart_rule` (
  `id_cart` bigint(20) NOT NULL,
  `id_cart_rule` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_cart`  ADD PRIMARY KEY (`id`),
 ADD KEY `idx_eos_cart_status` (`status`);

ALTER TABLE `eos_cart_row` ADD PRIMARY KEY (`id`),
  ADD KEY `eos_cart_row_id_cart` (`id_cart`);

ALTER TABLE `eos_cart` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_cart_row` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_cart_cart_rule`
  ADD KEY `eos_cart_cart_rule_id_cart` (`id_cart`),
  ADD KEY `eos_cart_cart_rule_id_cart_rule` (`id_cart_rule`);

ALTER TABLE `eos_cart_row`
  ADD CONSTRAINT `fk_eos_cart_product` FOREIGN KEY (`id_cart`) REFERENCES `eos_cart` (`id`);

ALTER TABLE `eos_cart_cart_rule`
  ADD CONSTRAINT `fk_eos_cart_cart_rule` FOREIGN KEY (`id_cart`) REFERENCES `eos_cart` (`id`);

INSERT INTO eos_cart select * from eos_order_cart;
INSERT INTO eos_cart_row select * from eos_order_cart_product;


