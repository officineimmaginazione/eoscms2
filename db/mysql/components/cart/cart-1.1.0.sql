ALTER TABLE `eos_cart` 
    ADD `id_currency` bigint(20) NOT NULL DEFAULT '0' AFTER `id`,
    ADD `id_customer` bigint(20) NOT NULL DEFAULT '0' AFTER `id_currency`,  
    ADD `id_address_delivery` bigint(20)  DEFAULT NULL AFTER `id_address_delivery`,
    ADD `id_address_invoice` bigint(20)  DEFAULT NULL AFTER `id_address_invoice`,
    ADD `ins_id` int(11) DEFAULT NULL AFTER `total`,
    ADD `ins_date` datetime DEFAULT NULL AFTER `ins_id`,
    ADD `up_id` int(11) DEFAULT NULL  AFTER `ins_date`,
    ADD `up_date` datetime DEFAULT NULL AFTER `up_id`; 

ALTER TABLE `eos_cart_row` ADD `id_product_child` bigint(20) NOT NULL DEFAULT '0' AFTER `id_product`; 

DROP TABLE eos_order_cart_product; 

DROP TABLE eos_order_cart; 

