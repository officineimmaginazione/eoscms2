ALTER TABLE `eos_cart` 
ADD KEY `idx_eos_cart_id_customer` (`id_customer`),
ADD KEY `idx_eos_cart_id_address_delivery` (`id_address_delivery`), 
ADD KEY `idx_eos_cart_id_agent` (`id_agent`);