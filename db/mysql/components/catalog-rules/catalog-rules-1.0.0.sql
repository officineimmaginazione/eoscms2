CREATE TABLE IF NOT EXISTS `eos_catalog_rules` (
    `id` bigint(20) unsigned NOT NULL,
    `name` varchar(255) NOT NULL,
    `id_product` bigint(20) unsigned NOT NULL,
    `id_currency` bigint(20) unsigned NOT NULL,
    `id_group` bigint(20) unsigned NOT NULL,
    `id_customer` bigint(20) unsigned NOT NULL,
    `id_product_attribute` bigint(20) unsigned NOT NULL,
    `price` decimal(20,6) NOT NULL,
    `from_quantity` mediumint(8) unsigned NOT NULL,
    `reduction` decimal(20,6) NOT NULL,
    `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
    `reduction_type` varchar(255) NOT NULL,
    `from` datetime NOT NULL,
    `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_catalog_rules_condition` (
    `id` bigint(20) unsigned NOT NULL,
    `id_catalog_rules` bigint(20) unsigned NOT NULL,
    `type` varchar(255) NOT NULL,
    `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_catalog_rules`  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_catalog_rules_condition` ADD PRIMARY KEY (`id`),
  ADD KEY `eos_catalog_rules_id_catalog_rules` (`id_catalog_rules`);

ALTER TABLE `eos_catalog_rules` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_catalog_rules_condition` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_catalog_rules_condition`
  ADD CONSTRAINT `fk_eos_catalog_rules_condition` FOREIGN KEY (`id_catalog_rules`) REFERENCES `eos_catalog_rules` (`id`);