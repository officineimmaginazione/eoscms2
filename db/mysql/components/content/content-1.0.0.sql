CREATE TABLE `eos_content` (
  `id` bigint(20) NOT NULL,
  `type` smallint(6) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_content_article` (
  `id_content_lang` bigint(20) NOT NULL,
  `author` int(11) NOT NULL,
  `publish_date` datetime NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_content_category` (
  `id_content` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_content_lang` (
  `id` bigint(20) NOT NULL,
  `id_content` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `content` mediumtext  NOT NULL,
  `options` mediumtext ,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_content_section` (
  `id_content` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_content_type` (`type`),
  ADD KEY `idx_eos_content_status` (`status`),
  ADD KEY `idx_eos_content_parent` (`parent`);

ALTER TABLE `eos_content_category`
  ADD KEY `idx_eos_content_category_id_content` (`id_content`),
  ADD KEY `idx_eos_content_category_id_category` (`id_category`);

ALTER TABLE `eos_content_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_lang_id_content` (`id_content`),
  ADD KEY `idx_content_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_content_section`
  ADD PRIMARY KEY (`id_content`),
  ADD UNIQUE KEY `idx_content_section_name` (`name`);  

ALTER TABLE `eos_content`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_content_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;  

ALTER TABLE `eos_content_article`
  ADD CONSTRAINT `fk_eos_content_article` FOREIGN KEY (`id_content_lang`) REFERENCES `eos_content_lang` (`id`);  
  
ALTER TABLE `eos_content_section`
  ADD CONSTRAINT `fk_eos_content_section` FOREIGN KEY (`id_content`) REFERENCES `eos_content` (`id`);
  
ALTER TABLE `eos_content_lang`
  ADD CONSTRAINT `fk_eos_content_lang` FOREIGN KEY (`id_content`) REFERENCES `eos_content` (`id`);  