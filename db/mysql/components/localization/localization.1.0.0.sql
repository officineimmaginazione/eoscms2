CREATE TABLE `eos_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `status_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_site` tinyint(4) NOT NULL DEFAULT '0',
  `status_site` tinyint(4) NOT NULL DEFAULT '0',
  `iso_code` char(2) NOT NULL,
  `language_code` varchar(32) NOT NULL,
  `datelite_format` varchar(32) NOT NULL DEFAULT 'Y-m-d',
  `datefull_format` varchar(32) NOT NULL DEFAULT 'Y-m-d H:i:s'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `eos_lang` (`id`, `name`, `status_site`,  `status_admin`, `iso_code`, `language_code`, `datelite_format`, `datefull_format`) VALUES
(1, 'Italiano (Italian)', 1, 1, 'it', 'it-it', 'd/m/Y', 'd/m/Y H:i:s');

CREATE TABLE `eos_localization_city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_state` int(11) NOT NULL,
  `zip_code` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` float NOT NULL DEFAULT '0',
  `longitude` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_localization_country` (
  `id` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `iso_code` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `eos_localization_country_lang` (
  `id` int(11) NOT NULL,
  `id_country` int(11) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `eos_localization_region` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_country` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_localization_state` (
  `id` int(11) NOT NULL,
  `iso_code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_region` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `eos_currency` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `conversion_rate` decimal(13,6) NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_lang`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_city_id_state` (`id_state`);

ALTER TABLE `eos_localization_country_lang`
  ADD PRIMARY KEY (`id_country`, `id_lang`);

ALTER TABLE `eos_localization_country`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_localization_country_lang`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_localization_region`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_localization_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_state_id_region` (`id_region`);

ALTER TABLE `eos_currency`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_localization_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_country_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_localization_city`
  ADD CONSTRAINT `fk_eos_localization_city` FOREIGN KEY (`id_state`) REFERENCES `eos_localization_state` (`id`);

ALTER TABLE `eos_localization_state`
  ADD CONSTRAINT `fk_eos_state` FOREIGN KEY (`id_region`) REFERENCES `eos_localization_region` (`id`);
