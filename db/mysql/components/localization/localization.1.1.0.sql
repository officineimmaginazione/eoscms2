ALTER TABLE `eos_localization_country_lang` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_localization_state` ADD `id_country` INT NOT NULL DEFAULT '0' AFTER `id_region`;
ALTER TABLE `eos_localization_state` CHANGE `id_region` `id_region` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `eos_localization_state` CHANGE `name` `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
