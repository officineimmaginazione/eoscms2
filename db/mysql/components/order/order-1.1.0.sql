CREATE TABLE `eos_order_cart` (
  `id` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `total` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `eos_order_cart_product` (
  `id` bigint(20) NOT NULL,
  `id_order_cart` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_order_cart`  ADD PRIMARY KEY (`id`),
 ADD KEY `idx_eos_order_cart_id_order` (`status`);

ALTER TABLE `eos_order_cart_product` ADD PRIMARY KEY (`id`),
  ADD KEY `eos_order_cart_product_id_order_cart` (`id_order_cart`);

ALTER TABLE `eos_order_cart` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_order_cart_product` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_order_cart_product`
  ADD CONSTRAINT `fk_eos_order_cart_product` FOREIGN KEY (`id_order_cart`) REFERENCES `eos_order_cart` (`id`);

ALTER TABLE `eos_order_row` ADD `options` MEDIUMTEXT NOT NULL AFTER `price`; 