CREATE TABLE IF NOT EXISTS `eos_address` (
  `id` bigint(20)  NOT NULL,
  `id_country` int(111)  NOT NULL,
  `id_state` int(11)  DEFAULT NULL,
  `id_customer` bigint(20)  NOT NULL DEFAULT '0',
  `id_manufacturer` bigint(20)  NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `status` tinyint(4)  NOT NULL DEFAULT '1',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_order` 
    ADD `id_customer` bigint(20) NOT NULL DEFAULT '0', 
    ADD `id_address` bigint(20) NOT NULL DEFAULT '0',
    ADD `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000'
    AFTER `id`; 

ALTER TABLE `eos_order_row` 
    ADD `id_product_child` bigint(20)  DEFAULT NULL,
    ADD `quantity` int(10)  NOT NULL DEFAULT '0',
    ADD `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `reduction_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
    ADD `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000'
    AFTER `id`; 

ALTER TABLE `eos_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_address_id_customer` (`id_customer`)
  ADD KEY `idx_eos_address_id_manufacturer` (`id_manufacturer`);

ALTER TABLE `eos_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_address` CHANGE `dni` `cif` VARCHAR(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `eos_address` CHANGE `vat_number` `vat` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `eos_order` CHANGE `fiscal_code` `cif` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
