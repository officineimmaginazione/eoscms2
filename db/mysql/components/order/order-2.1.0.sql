ALTER TABLE `eos_order` 
ADD KEY `idx_eos_order_id_customer` (`id_customer`),
ADD KEY `idx_eos_order_id_address_delivery` (`id_address_delivery`), 
ADD KEY `idx_eos_order_id_agent` (`id_agent`);
