CREATE TABLE `eos_product` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `id_category` bigint(20) NOT NULL DEFAULT '0',
  `price` float(7,4) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_product_lang` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_product_category` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_product_category_lang` (
  `id` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_status` (`status`),
  ADD KEY `idx_eos_product_id_category` (`id_category`);

ALTER TABLE `eos_product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_status` (`status`);

ALTER TABLE `eos_product_category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_category_lang` (`id_category`);

ALTER TABLE `eos_product_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_lang` (`id_product`);
  
ALTER TABLE `eos_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_category_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `eos_product_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `eos_product_lang`
  ADD CONSTRAINT `fk_eos_product_lang` FOREIGN KEY (`id_product`) REFERENCES `eos_product` (`id`);
  
ALTER TABLE `eos_product_category_lang`
  ADD CONSTRAINT `fk_eos_product_category_lang` FOREIGN KEY (`id_category`) REFERENCES `eos_product_category` (`id`);