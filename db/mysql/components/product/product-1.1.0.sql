CREATE TABLE `eos_product_image` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL,
  `url`  varchar(250) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_product_rules` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `qty_min` int(11) NOT NULL,
  `qty_max` int(11) NOT NULL,
  `price` float(7,4) NOT NULL,
  `discount` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_product_rules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_rules` (`id_product`);

ALTER TABLE `eos_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_id_product` (`id_product`);

ALTER TABLE `eos_product_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_rules`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_image`
  ADD CONSTRAINT `fk_eos_product_image` FOREIGN KEY (`id_product`) REFERENCES `eos_product` (`id`);
  
ALTER TABLE `eos_product_rules`
  ADD CONSTRAINT `fk_eos_product_rules` FOREIGN KEY (`id_product`) REFERENCES `eos_product` (`id`);