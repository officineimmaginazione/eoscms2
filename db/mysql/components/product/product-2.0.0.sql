CREATE TABLE IF NOT EXISTS `eos_product_accessory` (
  `id_product_1` bigint(20)  NOT NULL,
  `id_product_2` bigint(20)  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_attribute` (
  `id` bigint(20) NOT NULL,
  `id_attribute_group` bigint(20) NOT NULL,
  `color` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_attribute_lang` (
  `id` bigint(20) NOT NULL,
  `id_attribute` bigint(20) NOT NULL,
  `id_lang` bigint(20) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_attribute_group` (
  `id` bigint(20) NOT NULL,
  `is_color_group` tinyint(1) NOT NULL,
  `group_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_attribute_group_lang` (
  `id` bigint(20) NOT NULL,
  `id_attribute_group` bigint(20) NOT NULL,
  `id_lang` bigint(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `public_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `eos_product_product_attribute_combination` (
  `id_attribute` bigint(20)  NOT NULL,
  `id_product_attribute` bigint(20)  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_product_category` (
  `id_product` bigint(20)  NOT NULL,
  `id_category` bigint(20)  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_manufacturer` (
  `id` bigint(20)  NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_manufacturer_lang` (
  `id` bigint(20)  NOT NULL,
  `id_manufacturer` bigint(20)  NOT NULL,
  `id_lang` int(11)  NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `eos_product_child` (
  `id` bigint(20)  NOT NULL,
  `id_product` bigint(20)  NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

CREATE TABLE IF NOT EXISTS `eos_product_child_attribute_combination` (
  `id_attribute` int(10)  NOT NULL,
  `id_product_child` bigint(20)  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `eos_product_accessory`
    ADD KEY `idx_eos_product_accessory_id_product_1` (`id_product_1`),
    ADD KEY `idx_eos_product_accessory_id_product_2` (`id_product_2`);

ALTER TABLE `eos_product_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_attribute_id_attribute_group` (`id_attribute_group`);

ALTER TABLE `eos_product_attribute_lang`
    ADD PRIMARY KEY (`id`),
    ADD KEY `idx_eos_product_attribute_lang_id_attribute` (`id_attribute`),
    ADD KEY `idx_eos_product_attribute_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_product_attribute_group`
    ADD PRIMARY KEY (`id`),
    ADD KEY `idx_eos_product_id_product_group_type` (`group_type`);

ALTER TABLE `eos_product_attribute_group_lang`
  ADD PRIMARY KEY (`id`),
    ADD KEY `idx_eos_product_attribute_group_lang_id_attribute` (`id_attribute_group`),
    ADD KEY `idx_eos_product_attribute_group_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_product_product_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`,`id_product`),
  ADD KEY `idx_eos_product_attribute_combination_id_product_attribute` (`id_product_attribute`);

ALTER TABLE `eos_product_product_category`
  ADD PRIMARY KEY (`id_product`,`id_category`),
  ADD KEY `idx_eos_product_product_category_id_product` (`id_product`),
  ADD KEY `idx_eos_product_product_category_id_category` (`id_category`);

ALTER TABLE `eos_product_child`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_child_id_product` (`id_product`);

ALTER TABLE `eos_product_child_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`, `id_product_child`),
  ADD KEY `idx_eos_product_child_attribute_combination_id_attribute` (`id_attribute`),
  ADD KEY `idx_eos_product_child_attribute_combination_id_product_child` (`id_product_child`);

ALTER TABLE `eos_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_stock_id_category` (`id_product`),
  ADD KEY `idx_eos_stock_id_product` (`id_product_child`);

ALTER TABLE `eos_manufacturer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_manufacturer_status` (`status`);

ALTER TABLE `eos_manufacturer_lang`,
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_manufacturer_lang_id_manufacturer` (`id_manufacturer`),
  ADD KEY `idx_eos_manufacturer_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_product_child`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_attribute`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_attribute_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_attribute_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_attribute_group_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_stock`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_manufacturer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_manufacturer_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product_attribute_lang`
  ADD CONSTRAINT `fk_eos_product_attribute_lang` FOREIGN KEY (`id_attribute`) REFERENCES `eos_product_attribute` (`id`);

ALTER TABLE `eos_product_attribute_group_lang`
  ADD CONSTRAINT `fk_eos_product_attribute_group_lang` FOREIGN KEY (`id_attribute_group`) REFERENCES `eos_product_attribute_group` (`id`);

ALTER TABLE `eos_manufacturer_lang`
  ADD CONSTRAINT `fk_eos_manufacturer_lang` FOREIGN KEY (`id_manufacturer`) REFERENCES `eos_manufacturer` (`id`);

ALTER TABLE `eos_product_child`
  ADD CONSTRAINT `fk_eos_product_child` FOREIGN KEY (`id_product`) REFERENCES `eos_product_child` (`id`);

ALTER TABLE `eos_product` 
    ADD `id_manufacturer` int(10)  DEFAULT NULL
    AFTER `id_category`; 

ALTER TABLE `eos_product_lang` 
    ADD `description_short` TEXT NULL DEFAULT NULL 
    AFTER `name`; 

ALTER TABLE `eos_product` 
    ADD `quantity` int(10) NOT NULL DEFAULT '0',
    ADD `on_sale` tinyint(1)  NOT NULL DEFAULT '0',
    ADD `online_only` tinyint(1)  NOT NULL DEFAULT '0',
    ADD `ean13` varchar(13) DEFAULT NULL,
    ADD `isbn` varchar(32) DEFAULT NULL,
    ADD `upc` varchar(12) DEFAULT NULL,
    ADD `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
    ADD `out_of_stock` tinyint(4)  NOT NULL DEFAULT '2',
    ADD `show_price` tinyint(1) NOT NULL DEFAULT '1'
    AFTER `price`; 

INSERT INTO `eos_product_product_category` select id, id_category from eos_product;

ALTER TABLE eos_product DROP COLUMN id_category;