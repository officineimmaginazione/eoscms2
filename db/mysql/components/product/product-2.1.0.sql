CREATE TABLE IF NOT EXISTS `eos_product_stock` (
  `id` bigint(20)  NOT NULL,
  `id_product` bigint(20)  NOT NULL,
  `id_product_child` bigint(20)  NULL DEFAULT NULL,
  `quantity_available` decimal(20,4)  NOT NULL DEFAULT '0',
  `quantity_physical` decimal(20,4)  NOT NULL DEFAULT '0',
  `quantity_engaged` decimal(20,4) NOT NULL DEFAULT '0',
  `quantity_reserved` decimal(20,4) NOT NULL DEFAULT '0',
  `quantity_ordered1` decimal(20,4) NOT NULL DEFAULT '0',
  `quantity_ordered2` decimal(20,4) NOT NULL DEFAULT '0',
  `date_ordered1` date DEFAULT NULL,
  `date_ordered2` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

/* quantity_available = quantity_physical + quantity_ordered1 + quantity_ordered2 - quantity_engaged - quantity_reserved */

ALTER TABLE `eos_product_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_stock_id_product` (`id_product`);

ALTER TABLE `eos_product_stock`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_product` 
    ADD `min_quantity` decimal(20,4) NOT NULL DEFAULT '0'
    AFTER `quantity`; 

ALTER TABLE eos_product DROP COLUMN quantity;
ALTER TABLE eos_product_child DROP COLUMN quantity;


ALTER TABLE `eos_product` CHANGE `price` `price` DECIMAL(20,6) NOT NULL DEFAULT '0';
ALTER TABLE `eos_order` CHANGE `amount` `amount` DECIMAL(20,6) NOT NULL DEFAULT '0'; 
ALTER TABLE `eos_order_row` CHANGE `price` `price` DECIMAL(20,6) NOT NULL DEFAULT '0';
ALTER TABLE `eos_order_row` CHANGE `quantity` `quantity` DECIMAL(20,4) NOT NULL DEFAULT '0';