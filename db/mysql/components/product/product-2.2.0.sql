CREATE TABLE `eos_tax` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1',
  `reference` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

CREATE TABLE `eos_tax_lang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_tax` bigint(20) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

ALTER TABLE `eos_tax`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_tax_status` (`status`);

ALTER TABLE `eos_tax`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `eos_tax_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_tax_lang_id_tax` (`id_tax`),
  ADD KEY `idx_eos_tax_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_tax_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `eos_product` 
    ADD `id_tax` bigint(20) NOT NULL DEFAULT 0 
    AFTER `price`; 

ALTER TABLE `eos_product` 
    ADD `quantity_per_pack` bigint(20) NOT NULL DEFAULT 0 
    AFTER `min_quantity`; 