CREATE TABLE `eos_strutturesanitarie_facility` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy` smallint(6) NOT NULL,
  `id_transaction` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) NOT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_facility_user` (
  `id` int(11) NOT NULL,
  `id_facility` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_facility_images` (
  `id` int(11) NOT NULL,
  `id_facility` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `path` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_service` (
  `id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos` smallint(6) NOT NULL,
  `icon` VARCHAR(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_facility_service` (
  `id` int(11) NOT NULL,
  `id_facility` int(11) NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_facility_subscription` (
  `id` int(11) NOT NULL,
  `id_facility` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_subscription` int(11) NOT NULL,
  `end_subscription` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_type` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_facility_type` (
  `id` int(11) NOT NULL,
  `id_facility` int(11) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_care` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_strutturesanitarie_care_type` (
  `id` int(11) NOT NULL,
  `id_care` int(11) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_strutturesanitarie_facility` ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_strutturesanitarie_facility` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_strutturesanitarie_facility_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_user_id_user` (`id_user`),
  ADD KEY `idx_eos_strutturesanitarie_facility_user_id_facility` (`id_facility`);

ALTER TABLE `eos_strutturesanitarie_facility_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_images_id_facility` (`id_facility`),
  ADD KEY `idx_eos_strutturesanitarie_facility_images_type` (`type`);

ALTER TABLE `eos_strutturesanitarie_facility_service` 
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_service` (`type`);

ALTER TABLE `eos_strutturesanitarie_facility_service` 
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_service_id_facility` (`id_facility`),
  ADD KEY `idx_eos_strutturesanitarie_facility_service_id_service` (`id_service`);

ALTER TABLE `eos_strutturesanitarie_type` 
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_strutturesanitarie_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_strutturesanitarie_facility_type` 
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_type_id_facility` (`id_facility`),
  ADD KEY `idx_eos_strutturesanitarie_facility_type_id_type` (`id_type`);

ALTER TABLE `eos_strutturesanitarie_facility_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_strutturesanitarie_care` 
  ADD PRIMARY KEY (`id`);

ALTER TABLE `eos_strutturesanitarie_care` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_strutturesanitarie_care_type` 
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_care_type_id_care` (`id_care`),
  ADD KEY `idx_eos_strutturesanitarie_care_type_id_type` (`id_type`);

ALTER TABLE `eos_strutturesanitarie_care_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_strutturesanitarie_facility_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_strutturesanitarie_facility_subscription_id_facility` (`id_facility`),
  ADD KEY `idx_eos_strutturesanitarie_facility_subscription_id_order` (`id_order`),
  ADD KEY `idx_eos_strutturesanitarie_facility_subscription_end_subscription` (`end_subscription`);

ALTER TABLE `eos_strutturesanitarie_facility_user`
  ADD CONSTRAINT `fk_eos_strutturesanitarie_facility_user` FOREIGN KEY (`id_facility`) REFERENCES `eos_strutturesanitarie_facility` (`id`);

ALTER TABLE `eos_strutturesanitarie_facility_images`
  ADD CONSTRAINT `fk_eos_strutturesanitarie_facility_images` FOREIGN KEY (`id_facility`) REFERENCES `eos_strutturesanitarie_facility` (`id`);

ALTER TABLE `eos_strutturesanitarie_facility_service`
  ADD CONSTRAINT `fk_eos_strutturesanitarie_facility_service` FOREIGN KEY (`id_facility`) REFERENCES `eos_strutturesanitarie_facility` (`id`);

ALTER TABLE `eos_strutturesanitarie_facility_type`
  ADD CONSTRAINT `fk_eos_strutturesanitarie_facility_service` FOREIGN KEY (`id_facility`) REFERENCES `eos_strutturesanitarie_facility` (`id`));


ALTER TABLE `eos_strutturesanitarie_facility_subscription`
  ADD CONSTRAINT `fk_eos_strutturesanitarie_facility_subscription` FOREIGN KEY (`id_facility`) REFERENCES `eos_strutturesanitarie_facility` (`id`);