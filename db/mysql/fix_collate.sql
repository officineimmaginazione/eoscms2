/* Script che corregge la collate utf-8 sbasgliata nei primi EOS e non solo solo*/
alter table eos_lang convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table eos_admin_user convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table eos_admin_user_profile convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table eos_content_lang convert to character set utf8mb4 collate utf8mb4_unicode_ci;

alter table eos_order_cart_product convert to character set utf8mb4 collate utf8mb4_unicode_ci;