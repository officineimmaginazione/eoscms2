CREATE TABLE `eos_setting` (
  `id` int(11) NOT NULL,
  `object` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_menu_lang` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_admin_user` (
  `id` int(11) NOT NULL,
  `email` varchar(150)  NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(100)  NOT NULL,
  `status` tinyint(4) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_admin_user_profile` (
  `id_user` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `name` varchar(75)  NOT NULL,
  `surname` varchar(75) NOT NULL,
  `registration_date` datetime DEFAULT NULL,
  `lastvisit_date` datetime DEFAULT NULL,
  `options` mediumtext  NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_router` (
  `path` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `menu` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_seo` (
  `id_object` bigint(20) NOT NULL,
  `component` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_lang` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_raw` mediumtext COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menu_name` (`name`);

ALTER TABLE `eos_menu_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu_lang_menu` (`id_menu`),
  ADD KEY `fk_menu_lang_id_lang` (`id_lang`);

ALTER TABLE `eos_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_menu_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_menu_lang`
  ADD CONSTRAINT `fk_menu_lang_menu` FOREIGN KEY (`id_menu`) REFERENCES `eos_menu` (`id`);

ALTER TABLE `eos_router`
  ADD PRIMARY KEY (`path`,`lang`),
  ADD UNIQUE KEY `idx_router_route` (`route`,`lang`),
  ADD KEY `idx_router_container` (`container`);

ALTER TABLE `eos_seo`
  ADD PRIMARY KEY (`component`,`id_object`,`id_lang`);

ALTER TABLE `eos_admin_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_admin_user_email` (`email`);

ALTER TABLE `eos_admin_user_profile`
  ADD KEY `idx_admin_user_profile_type` (`type`);

ALTER TABLE `eos_admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 
ALTER TABLE `eos_admin_user_profile`
  ADD CONSTRAINT `fk_admin_user_profile` FOREIGN KEY (`id_user`) REFERENCES `eos_admin_user` (`id`);