CREATE TABLE `eos_image` (
  `id` bigint(20) NOT NULL,
  `type_object` varchar(50) NOT NULL,
  `id_object` bigint(20) NOT NULL,
  `cover` tinyint(4) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `path`  varchar(250) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_attachment` (
  `id` bigint(20) NOT NULL,
  `type_object` varchar(50) NOT NULL,
  `id_object` bigint(20) NOT NULL,
  `mime` varchar(128)  NOT NULL,
  `size` int(11) NOT NULL,
  `path`  varchar(250) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_admin_group` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `authorization` mediumtext,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_image_component` (`component`),
  ADD KEY `idx_eos_image_cover` (`cover`);

ALTER TABLE `eos_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_attachment_mime` (`mime`);

ALTER TABLE `eos_admin_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_admin_group_status` (`status`);

ALTER TABLE `eos_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_admin_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_attachment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

INSERT INTO `eos_image` (`id_object`, `pos`, `path`, `ins_id`, `ins_date`, `up_id`, `up_date`) select `id_product`, `pos`, `url`, `ins_id`, `ins_date`, `up_id`, `up_date` from eos_product_image;
UPDATE `eos_image` SET `component`=1 WHERE 1 

ALTER TABLE `eos_admin_user`
    ADD `name` varchar(75)  NOT NULL AFTER `password`,
    ADD `surname` varchar(75) NOT NULL AFTER `password`,
    ADD `type` smallint(6) NOT NULL AFTER `password`,
    ADD `registration_date` datetime DEFAULT NULL AFTER `password`,
    ADD `lastvisit_date` datetime DEFAULT NULL AFTER `password`; 

INSERT INTO `eos_admin_user` (`name`, `surname`, `type`, `registration_date`, `lastvisit_date`) select `name`, `surname`, `type`, `registration_date`, `lastvisit_date` from eos_admin_user_profile;

DROP TABLE eos_admin_user_profile;