CREATE TABLE `eos_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `status_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_site` tinyint(4) NOT NULL DEFAULT '0',
  `status_site` tinyint(4) NOT NULL DEFAULT '0',
  `iso_code` char(2) CHARACTER SET utf8 NOT NULL,
  `language_code` char(5) CHARACTER SET utf8 NOT NULL,
  `datelite_format` char(32) CHARACTER SET utf8 NOT NULL DEFAULT 'Y-m-d',
  `datefull_format` char(32) CHARACTER SET utf8 NOT NULL DEFAULT 'Y-m-d H:i:s'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_lang`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `eos_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `eos_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `eos_menu_lang` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_eos_menu_name` (`name`);

ALTER TABLE `eos_menu_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_eos_menu_lang_menu` (`id_menu`),
  ADD KEY `fk_eos_menu_lang_id_lang` (`id_lang`);  

ALTER TABLE `eos_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_menu_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `eos_menu_lang`
  ADD CONSTRAINT `fk_menu_lang_menu` FOREIGN KEY (`id_menu`) REFERENCES `eos_menu` (`id`);

CREATE TABLE `eos_router` (
  `path` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `menu` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
  
ALTER TABLE `eos_router`
  ADD PRIMARY KEY (`path`,`lang`),
  ADD UNIQUE KEY `idx_router_route` (`route`,`lang`),
  ADD KEY `idx_eos_router_container` (`container`);
  
CREATE TABLE `eos_seo` (
  `id_object` bigint(20) NOT NULL,
  `component` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_lang` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_raw` mediumtext COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_seo`
  ADD PRIMARY KEY (`component`,`id_object`,`id_lang`);

CREATE TABLE `eos_setting` (
  `id` int(11) NOT NULL,
  `object` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `eos_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_setting_object` (`object`);

ALTER TABLE `eos_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


/* quantità righe */
ALTER TABLE `eos_order_row` ADD `quantity` INT NOT NULL DEFAULT '0' AFTER `type_object`; 

/* Inserito CAP/ZIP Code e options */
ALTER TABLE `eos_order` ADD `zip_code` VARCHAR(50) NOT NULL AFTER `address`; 
ALTER TABLE `eos_order` ADD `options` MEDIUMTEXT NULL AFTER `id_transaction`; 

ALTER TABLE `eos_order_cart` ADD `options` MEDIUMTEXT NULL AFTER `total`; 

ALTER TABLE `eos_reservation_type` ADD `options` MEDIUMTEXT NULL AFTER `status`; 

ALTER TABLE `eos_reservation_category` ADD `availability_customer_limit` INT(11)  NULL AFTER `id`; 
ALTER TABLE `eos_reservation_category` ADD `options` MEDIUMTEXT NULL AFTER `availability_customer_limit`; 


ALTER TABLE `eos_content`  ADD `extra_status` TINYINT NOT NULL DEFAULT '0'  AFTER `parent`,  ADD `extra` VARCHAR(150) NULL  AFTER `extra_status`;


update eos_setting set value = '1001' where object='content' and name='dbversion' and value='1000';
