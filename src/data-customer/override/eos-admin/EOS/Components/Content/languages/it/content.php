<?php

return
    ['content.content.page.field.extra.text.1' => 'Titolo sezione',
        'content.content.page.field.extra.text.2' => 'Sottotitolo sezione',
        'content.content.page.field.extra.image.1' => 'Immagine sezione',
        'content.content.page.field.extra.html.1.1' => 'Html prima sezione',
        'content.content.page.field.extra.html.2.1' => 'Html seconda sezione',
        'content.content.page.field.extra.2' => 'Titolo sezione personalizzato' ];
