<?php

use \EOS\Components\System\Classes\AuthRole;

$this->removeMapComponent(['GET'], 'System', ['menu/index']);
$this->mapComponent(['GET'], 'System', ['menu/index' => 'MenuController:index'], ['auth' => ['system.menu' => [AuthRole::READ]]]);

//$this->removeRouteComponentController('System', 'MenuController');
$this->replaceRouteComponentController('System', 'MenuController', 'MenuController');

//$this->debugPrintRoutes();