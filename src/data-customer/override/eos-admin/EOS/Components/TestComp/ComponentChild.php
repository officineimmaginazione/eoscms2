<?php

namespace EOS\Components\TestComp;

class ComponentChild extends \EOS\System\Component\Component
{

    public function addProfiler($value)
    {
        $profiler = $this->application->getContainer()->get('profiler');
        if (!is_null($profiler))
        {
            $profiler->start();
            $profiler->stop($value);
        }
    }

    // Metodo ereditato
    public function load()
    {
        parent::load();
        $disp = $this->getEventDispatcher();

        $disp->addListener(\EOS\System\Events::TERMINATE, function (\EOS\System\Event\SystemEvent $event) 
        {
            //$this->addProfiler('onTerminate');
            if (_EOS_DEBUG_)
            {
                //  echo 'Terminate Event';
            }
        });

        $disp->addListener(\EOS\System\Events::CONTROLLER, function (\EOS\System\Event\SystemEvent $event)
        {
            $this->addProfiler('onController');
        });

        // HTML
        $disp->addListener(\EOS\System\Views\Events::HTML, function (\EOS\System\Event\SystemEvent $event) 
        {
            $this->addProfiler('onViewHtml');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_BEFORE_RENDER, function (\EOS\System\Event\RenderEvent $event) 
        {
            $this->addProfiler('onViewBeforeRender');
            //     $event->appendOutput('onViewBeforeRender');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_AFTER_RENDER, function (\EOS\System\Event\RenderEvent $event) 
        {
            $this->addProfiler('onViewAfterRender');
            //$event->appendOutput('onViewAfterRender');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_BEFORE_CONTENT, function (\EOS\System\Event\EchoEvent $event) 
        {
            $this->addProfiler('onBeforeContent');
            if (_EOS_DEBUG_)
            {
               // echo 'onBeforeContent';
            }
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_AFTER_CONTENT, function (\EOS\System\Event\EchoEvent $event) 
        {
            if (_EOS_DEBUG_)
            {
              //  echo 'onAfterContent';
            }
            $this->addProfiler('onAfterContent');
        });

        // RAW
        $disp->addListener(\EOS\System\Views\Events::RAW, function (\EOS\System\Event\SystemEvent $event) 
        {
            $this->addProfiler('onViewRAW');
        });

        $disp->addListener(\EOS\System\Views\Events::RAW_BEFORE_RENDER, function (\EOS\System\Event\RenderEvent $event) 
        {
            $this->addProfiler('onRAWBeforeRender');
            //$event->appendOutput('onRAWBeforeRender');
        });

        $disp->addListener(\EOS\System\Views\Events::RAW_AFTER_RENDER, function (\EOS\System\Event\RenderEvent $event)
        {
            $this->addProfiler('onRAWAfterRender');
            //$event->appendOutput('onRAWAfterRender');
        });
    }

}
