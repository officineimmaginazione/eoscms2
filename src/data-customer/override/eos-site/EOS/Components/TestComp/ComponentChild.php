<?php

namespace EOS\Components\TestComp;

class ComponentChild extends \EOS\System\Component\Component
{

    public function addProfiler($value)
    {
        $profiler = $this->application->getContainer()->get('profiler');
        if (!is_null($profiler))
        {
            $profiler->start();
            $profiler->stop($value);
        }
    }

    // Metodo ereditato
    public function load()
    {
        parent::load();
        $self = $this;
        $disp = $this->getEventDispatcher();

        $disp->addListener(\EOS\System\Events::TERMINATE, function (\EOS\System\Event\SystemEvent $event) use ($self)
        {
            //$self->addProfiler('onTerminate');
            if (_EOS_DEBUG_)
            {
                //  echo 'Terminate Event';
            }
        });

        $disp->addListener(\EOS\System\Events::CONTROLLER, function (\EOS\System\Event\SystemEvent $event) use ($self)
        {
            $self->addProfiler('onController');
        });

        // HTML
        $disp->addListener(\EOS\System\Views\Events::HTML, function (\EOS\System\Event\SystemEvent $event) use ($self)
        {
            $self->addProfiler('onViewHtml');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_BEFORE_RENDER, function (\EOS\System\Event\RenderEvent $event) use ($self)
        {
            $self->addProfiler('onViewBeforeRender');
            //     $event->appendOutput('onViewBeforeRender');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_AFTER_RENDER, function (\EOS\System\Event\RenderEvent $event) use ($self)
        {
            $self->addProfiler('onViewAfterRender');
            //$event->appendOutput('onViewAfterRender');
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_BEFORE_CONTENT, function (\EOS\System\Event\EchoEvent $event) use ($self)
        {
            $self->addProfiler('onBeforeContent');
            if (_EOS_DEBUG_)
            {
               // echo 'onBeforeContent';
            }
        });

        $disp->addListener(\EOS\System\Views\Events::HTML_AFTER_CONTENT, function (\EOS\System\Event\EchoEvent $event) use ($self)
        {
            if (_EOS_DEBUG_)
            {
              //  echo 'onAfterContent';
            }
            $self->addProfiler('onAfterContent');
        });

        // RAW
        $disp->addListener(\EOS\System\Views\Events::RAW, function (\EOS\System\Event\SystemEvent $event) use ($self)
        {
            $self->addProfiler('onViewRAW');
        });

        $disp->addListener(\EOS\System\Views\Events::RAW_BEFORE_RENDER, function (\EOS\System\Event\RenderEvent $event) use ($self)
        {
            $self->addProfiler('onRAWBeforeRender');
            //$event->appendOutput('onRAWBeforeRender');
        });

        $disp->addListener(\EOS\System\Views\Events::RAW_AFTER_RENDER, function (\EOS\System\Event\RenderEvent $event) use ($self)
        {
            $self->addProfiler('onRAWAfterRender');
            //$event->appendOutput('onRAWAfterRender');
        });
    }

}
