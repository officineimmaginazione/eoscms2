<!DOCTYPE html>
<html lang="<?php echo $this->lang->getCurrent()->iso; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
        $this->writeHead();
        \EOS\UI\Loader\Library::load($this, 'Bootstrap');
        \EOS\UI\Loader\Library::load($this, 'Icons');
        $this->addStyleSheetLink($this->path->getThemeUrl() . "css/style.css");
        $this->writeLink();
        ?>
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Tema default</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                  <?php
                  (new \EOS\Components\System\Widgets\MenuWidget($this, 'main-menu'))->write();
                  ?>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <?php
        (new \EOS\Components\System\Widgets\BreadcrumbWidget($this, 'main-menu'))->write();
        ?>
        <div class="container">
            <div class="row">
                <?php
                $this->writeContent();
                ?>
            </div>

        </div><!-- /.container -->
        <?php
        (new \EOS\Components\Content\Widgets\SectionWidget($this, 'footer'))->write();
        (new \EOS\Components\Content\Widgets\CategoryWidget($this, '43'))->write();
        $this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
        $this->writeScript();
        ?>
    </body>
</html>