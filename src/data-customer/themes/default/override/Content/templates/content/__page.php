<?php

if (empty($this->data['options']['customcode']))
{
    $css = $this->encodeHtml(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'css'));
    echo '<div class="page-block' . (empty($css) ? '' : ' ' . $css) . '">' . "\n";
    if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
    {
        echo '<h1 class="page-title">' . $this->encodeHtml($this->data['title']) . '</h1>' . "\n";
    }
    if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle'])))
    {
        echo '<h2 class="page-subtitle">' . $this->encodeHtml($this->data['subtitle']) . '</h2>' . "\n";
    }
    echo '<div class="page-content">';
    echo $this->data['content'];
    echo '<div>' . "\n";
    echo '<div>' . "\n";
    $extra = \EOS\System\Util\ArrayHelper::getArray($this->data['options'], 'extra');
    foreach ($extra as $item)
    {
        foreach ($item as $key => $values)
        {
            if ($key != 'html-mode')
            {
                foreach ($values as $v)
                {
                    echo $key . ' => ' . $v . '<br>' . "\n";
                }
            }
        }
    }
} else
{
    include($this->path->getThemeCustomPath() . $this->data['options']['customcode'] . '.php');
} 

