<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes;

use EOS\System\Util\ArrayHelper;

class ContentSubscriber extends \EOS\System\Event\Subscriber
{

    private function getRoute(array $data)
    {
        $type = \EOS\Components\Content\Classes\ContentType::getStr($data['type']);
        return 'content/' . $type . '/' . $data['id'];
    }

    public static function getSubscribedEvents()
    {
        return [
            \EOS\System\Views\Events::HTML_BEFORE_RENDER => 'ViewRender',
            \EOS\Components\Content\Classes\Events::CONTENT_EDIT_TEMPLATE_RENDER => 'contentEditTemplateRender',
            \EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_PARSE => 'contentEditDataParse',
            \EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_PREPARE => 'contentEditDataPrepare',
            \EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_AFTER_SAVE => 'contentEditDataAfterSave',
            \EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_BEFORE_DELETE => 'contentEditDataBeforeDelete'];
    }

    public function viewRender(\EOS\System\Event\RenderEvent $event)
    {
        if (($event->getSubject()->getComponentName() == 'Content') &&
            ($event->getSubject()->getComponentTemplate() == 'content/edit'))
        {
            \EOS\UI\Loader\Library::load($event->getSubject(), 'Select2');
        }
    }

    public function contentEditTemplateRender(\EOS\System\Event\EchoEvent $event)
    {
        $view = $event->getSubject();
        $w = new \EOS\Components\Account\Widgets\AuthEditorWidget($view);
        $w->values = $view->data['account-group'];
        $w->write();
        $view->util->addScriptBlockReady('$(\'#account-autheditor\').appendTo(\'#tab-adv\');');
    }

    public function contentEditDataParse(\EOS\System\Event\DataEvent $event)
    {
        $data = $event->getData();
        $rm = new RouterManager($this->container);
        if (empty(ArrayHelper::getInt($data, 'id')))
        {
            $data['account-group'] = [];
        } else
        {
            $data['account-group'] = $rm->getRouteGroups($rm->getRouteID($this->getRoute($data)));
        }

        $event->setData($data);
    }

    public function contentEditDataPrepare(\EOS\System\Event\DataEvent $event)
    {
        
    }

    public function contentEditDataAfterSave(\EOS\System\Event\DataEvent $event)
    {
        $data = $event->getData();
        $rm = new RouterManager($this->container);


        $container = false;
        if ((int) $data['type'] === \EOS\Components\Content\Classes\ContentType::Category)
        {
            $container = true;
        }
        $tmpGroup = ArrayHelper::getValue($data, 'account-group', []);
        if (is_array($tmpGroup))
        {
            $groups = $tmpGroup;
        } else
        {
            $groups = [$tmpGroup];
        }

        $rm->setRouteAndGroups($this->getRoute($data), $container, true, $groups);
    }

    public function contentEditDataBeforeDelete(\EOS\System\Event\DataEvent $event)
    {
        
    }

}
