<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace EOS\Components\Account\Classes;

use EOS\System\Util\StringHelper;
use EOS\System\Routing\PathHelper;

class RouterManager extends \EOS\Components\System\Classes\AuthModel
{

    public function getRouteID(string $route): int
    {
        $res = 0;
        $q = $this->db->prepare('select id from #__account_router where route = :route');
        $q->bindValue(':route', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route)));
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            $res = $r['id'];
        }
        return $res;
    }

    public function getRouteGroups(int $idRoute): array
    {
        $q = $this->db->prepare('select id_group from #__account_router_group where id_router = :id_router');
        $q->bindValue(':id_router', $idRoute);
        $q->execute();
        $list = $q->fetchAll();
        $res = [];
        if (!empty($list))
        {
            foreach ($list as $r)
            {
                $res[] = (int) $r['id_group'];
            }
        }
        return $res;
    }

    public function setRoute(string $route, bool $container, bool $auth): int
    {
        $id = $this->getRouteID($route);
        if ($id === 0)
        {
            $q = $this->db->prepare('insert into  #__account_router 
                  (route, container, auth, ins_id, ins_date, up_id, up_date)
                   values (:route, :container, :auth, :ins_id, :ins_date, :up_id, :up_date)');
            $q->bindValue(':ins_id', $this->user->getUserID());
            $q->bindValue(':ins_date', $this->db->util->nowToDBDateTime());
        } else
        {
            $q = $this->db->prepare('update #__account_router 
                  set route = :route, 
                  container = :container, 
                  auth = :auth, 
                  up_id = :up_id,
                  up_date = :up_date
                  where id = :id');
            $q->bindValue(':id', $id);
        }
        $q->bindValue(':route', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route)));
        $q->bindValue(':container', $container ? 1 : 0);
        $q->bindValue(':auth', $auth ? 1 : 0);
        $q->bindValue(':up_id', $this->user->getUserID());
        $q->bindValue(':up_date', $this->db->util->nowToDBDateTime());
        $q->execute();
        if ($id === 0)
        {
            $id = $this->db->lastInsertId();
        }

        return $id;
    }

    public function setRouteAndGroups(string $route, bool $container, bool $auth, array $groups): int
    {
        if (empty($groups))
        {
            return $this->deleteRoute($route);
        }
        $idRouter = $this->setRoute($route, $container, $auth);
        $fixedGroups = [];
        foreach ($groups as $g)
        {
            $fixedGroups[] = (int) $g;
        }
        // Cancello i gruppo che non esistono
        $groupList = implode(',', $fixedGroups);
        $qDelete = $this->db->prepare('delete from #__account_router_group 
                   where id_router = ' . $idRouter . ' and id_group not in (' . $groupList . ')');
        $qDelete->execute();
        $dbGroups = $this->getRouteGroups($idRouter);
        $q = $this->db->prepare('insert into  #__account_router_group 
                  (id_router, id_group, ins_id, ins_date, up_id, up_date)
                   values (:id_router, :id_group, :ins_id, :ins_date, :up_id, :up_date)');
        foreach ($fixedGroups as $g)
        {
            if (!in_array($g, $dbGroups))
            {
                $q->bindValue(':id_router', $idRouter);
                $q->bindValue(':id_group', $g);
                $q->bindValue(':ins_id', $this->user->getUserID());
                $q->bindValue(':ins_date', $this->db->util->nowToDBDateTime());
                $q->bindValue(':up_id', $this->user->getUserID());
                $q->bindValue(':up_date', $this->db->util->nowToDBDateTime());
                $q->execute();
            }
        }
        return $idRouter;
    }

    public function deleteRoute(string $route): int
    {
        $id = 0;
        if ($route !== '')
        {
            $id = $this->getRouteID($route);
            $qg = $this->db->prepare('delete from #__account_router_group where id_router = :id_router');
            $qg->bindValue(':id_router', $id);
            $qg->execute();
            $q = $this->db->prepare('delete from #__account_router where id = :id');
            $q->bindValue(':id', $id);
            $q->execute();
        }
        return $id;
    }

}
