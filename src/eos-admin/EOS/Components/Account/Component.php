<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        parent::load();
        // Aggiungo il subscriber per i componenti
        if (($this->application->componentManager->componentExists('Content')) && ($this->isActive('Account')))
        {
            $this->getEventDispatcher()->addSubscriber(new Classes\ContentSubscriber($this->getContainer()));
        }
    }

}
