<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class GroupController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('account.group.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('group/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('account.group.insert.title') : $v->trans('account.group.edit.title');
        $v->addBreadcrumb($this->path->urlFor('account', ['group', 'index']), $v->trans('account.group.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Account\Models\GroupModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('group/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Account\Models\GroupModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'ag.id');
        $dts->addColumn('name', 'ag.name', true);
        $dts->addColumn('status', 'ag.status', false);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\GroupModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'group/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\GroupModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'group/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }
    
    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            switch (ArrayHelper::getStr($data, 'command'))
            {
                case 'city':
                    $m = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                    $fv->setResult(true);
                    $fv->setOutputValue('city', $m->getCity(ArrayHelper::getInt($data, 'id')));
                    break;
                default :
                    $fv->setMessage($this->lang->transEncode('system.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

}
