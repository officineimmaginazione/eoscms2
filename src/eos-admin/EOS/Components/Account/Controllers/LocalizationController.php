<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class LocalizationController extends \EOS\Components\System\Classes\AuthController
{

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            switch (ArrayHelper::getStr($data, 'command'))
            {
                case 'citylist' :
                    $m = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                    $fv->setResult(true);
                    $fv->setOutputValue('list', $m->getCityList($data));
                    break;
                case 'city':
                    $m = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                    $fv->setResult(true);
                    $fv->setOutputValue('city', $m->getCity(ArrayHelper::getInt($data, 'id')));
                    break;
                default :
                    $fv->setMessage($this->lang->transEncode('strutturesanitarie.common.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue')))
        {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\LocalizationModel($this->container);
            switch ($datasource)
            {
                case 'city':
                    if ($format === 'select2')
                    {
                        $filter = ArrayHelper::getStr($params, 'q');
                        $res['results'] = $m->searchCity($filter, $m->getFuncFormatCityStateText());
                    } else if ($format === 'typeahead')
                    {
                        $res = $m->getCityList($m->getFuncFormatCityStateName());
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

}
