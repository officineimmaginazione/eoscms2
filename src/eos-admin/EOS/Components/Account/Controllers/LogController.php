<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;

class LogController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('account.log.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('log/default');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Account\Models\LogModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'id', false);
        $dts->addColumn('logdate', 'logdate', true);
        $dts->addColumn('ip', 'ip', true);
        $dts->addColumn('logname', 'logname', true);
        $dts->addColumn('id_user', 'id_user', false);
        $dts->addColumn('cmd', 'cmd', true);
        $dts->addColumn('status', 'status', false);
        $lang = $this->lang;
        $dts->onColumnRender('logdate', function ($value, $item) use ($lang)
        {
            return empty($value) ? '' : $lang->dbDateTimeToLangDateTime($value);
        });
        return $response->withJson($dts->toArray());
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\LogModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->deleteData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'log/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
