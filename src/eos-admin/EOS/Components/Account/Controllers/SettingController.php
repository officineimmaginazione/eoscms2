<?php

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;

class SettingController extends \EOS\Components\System\Classes\AuthController
{

    private function getThemeList()
    {
        $list = glob(_EOS_PATH_THEMES_ . '*', GLOB_ONLYDIR);
        foreach ($list as $item)
        {
            $t = basename($item);
            $res[] = ['name' => $t];
        }
        return $res;
    }

    private function getThemeTemplateList($theme)
    {
        $list = glob(_EOS_PATH_THEMES_ . $theme . DIRECTORY_SEPARATOR . '*.php');
        $res[] = ['name' => ''];
        foreach ($list as $item)
        {
            $t = basename($item, '.php');
            $res[] = ['name' => $t];
        }
        return $res;
    }

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->pageTitle = $v->trans('account.setting.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $m = new \EOS\Components\Account\Models\SettingModel($this->container);
        $v->loadiCheck();
        $v->data = $m->getData();
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('account.setting.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('account.setting.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        $v->themeList = $this->getThemeList();
        $v->themeTemplateList = $this->getThemeTemplateList($v->data['auth-theme']);
        return $v->render('setting/default', true);
    }

    public function ajaxSaveData($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\SettingModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'setting/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
