<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class UserController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('account.user.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('user/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('account.user.insert.title') : $v->trans('account.user.edit.title');
        $v->addBreadcrumb($this->path->urlFor('account', ['user', 'index']), $v->trans('account.user.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->groups = $m->getGroupList();
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('user/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'id');
        $dts->addColumn('username', 'username', true);
        $dts->addColumn('email', 'email', true);
        $dts->addColumn('name', 'name', true);
        $dts->addColumn('last_access', 'last_access', true);
        $dts->addColumn('lang', 'lang', true);
        $dts->addColumn('grouplist', 'grouplist');
        $dts->addColumn('status', 'status', false);
        $lang = $this->lang;
        $dts->onColumnRender('last_access', function ($value, $item) use ($lang)
        {
            return empty($value) ? '' : $lang->dbDateTimeToLangDateTime($value);
        });
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'user/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'user/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

    public function ajaxRequestActive($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            $error = '';
            if ($m->requestActiveData($fv->getInputData(), $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'user/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }

        return $fv->toJsonResponse();
    }

    public function ajaxRequestResetPass($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            $error = '';
            if ($m->requestResetPassData($fv->getInputData(), $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'user/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }

        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue')))
        {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            switch ($datasource)
            {
                case 'user':
                    if ($format === 'select2')
                    {
                        $filter = ArrayHelper::getStr($params, 'q');
                        $res['results'] = $m->searchList($filter);
                    } 
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

}
