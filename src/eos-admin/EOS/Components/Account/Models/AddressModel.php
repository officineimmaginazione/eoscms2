<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class AddressModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->select(null)
            ->select('a.id, a.alias, a.name, a.surname, a.company, lc.name, a.status')
            ->innerJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'));
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['id-customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['id-city'] = $rc['id_city'];
            $res['phone'] = $rc['phone'];
            $res['phone-mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['current-city'] = empty($current_city) ? [] : [$formatFunc($current_city)];
            //$res['status'] = (int) $rc['status'];
        }
        return $res;
    }

    public function getDataDiscount($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['id-customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['id-city'] = $rc['id_city'];
            $res['phone'] = $rc['phone'];
            $res['phone-mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['current-city'] = empty($current_city) ? [] : [$formatFunc($current_city)];
            $res['status'] = (int) $rc['status'];
            $tblAttribute = $this->db->tableFix('#__account_user_group aug');
            $query = $this->db->newFluent()->from($tblAttribute)
                ->select('cr.name as discount')
                ->innerJoin($this->db->tableFix('#__catalog_rules cr ON cr.id_group = aug.id_group'))
                ->where('aug.id_group', $rc['id']);
            $rca = $query->fetchAll();
            $i = 0;
            foreach ($rca as $ra)
            {
                $res['discount-' . $ra['id']] = $ra['discount'];
                $i++;
            }
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;


        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $optionsvalues = [];
            $options = json_encode($optionsvalues);
            $values = [
                'id_customer' => $data['id-customer'],
                'alias' => $data['alias'],
                'name' => $data['name'],
                'surname' => $data['surname'],
                'address1' => $data['address1'],
                'address2' => $data['address2'],
                'id_city' => $data['id-city'],
                'id_country' => 1,
                'phone' => $data['phone'],
                'phone_mobile' => $data['phone-mobile'],
                'vat' => $data['vat'],
                'company' => $data['company'],
                'cif' => $data['cif'],
                'options' => $options,
                'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__address');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_user');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function searchList($filter, $limit = 50)
    {
        $res = [];
        if ((mb_strlen($filter) >= 1) && (mb_strlen($filter) < 200))
        {
            $q = $this->getListQuery();
            $fValue = $this->db->quote('%' . $this->db->util->prepareLikeValue($filter) . '%');
            $autType = $this->getAuthType();
            $fList[] = 'u.id like ' . $fValue;
            $fList[] = 'u.name like ' . $fValue;
            switch ($autType)
            {
                case 1: // Email
                    $fList[] = 'u.email like ' . $fValue;
                    break;
                case 2: // Utente
                    $fList[] = 'u.username like ' . $fValue;
                    break;
                default: // 0 - 3 Utente o Email
                    $fList[] = 'u.email like ' . $fValue;
                    $fList[] = 'u.username like ' . $fValue;
                    break;
            }
            $q->where(implode("\n OR ", $fList));
            if ($limit > 0)
            {
                $q->limit($limit);
            }
            $res = $this->getTextList($q);
        }
        return $res;
    }

}
