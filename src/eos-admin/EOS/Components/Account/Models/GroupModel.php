<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class GroupModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__account_group ag'))
            ->select(null)
            ->select('ag.id')
            ->select('ag.name')
            ->select('ag.status')
            ->orderBy('ag.name');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__account_group ag'))
            ->where('ag.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['status'] = $rc['status'] == 1;
        }
        return $res;
    }

    public function getGroups($status = null)
    {
        $sql = 'select au.id, au.name from #__account_group au ';
        if($status != null) {
            $sql .=  'where au.status = :status ';
        }
        $sql .=  'order by au.name';
        //print_r($sql); exit();
        $q = $this->db->prepare($sql);
        if($status != null) {
            $q->bindValue(':status', $status);
        }
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;


        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__account_group');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_group');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
