<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

class LogModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__account_log'))
                ->select(null)->select('id, logdate, ip, logname, id_user, cmd, status');
        return $f;
    }

    public function deleteData($data, &$error)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_log');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
