<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\ArrayHelper;

class SettingModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getData()
    {
        $data = [];
        $data['auth-theme'] = $this->db->setting->getStr('account', 'auth.theme', '');
        $data['auth-themetemplate'] = $this->db->setting->getStr('account', 'auth.themetemplate', '');
        $data['retry-limit'] = $this->db->setting->getInt('account', 'retry.limit', 10);
        $data['lock-minutes'] = $this->db->setting->getInt('account', 'lock.minutes', 10);
        $data['log-days'] = $this->db->setting->getInt('account', 'log.days', 30);
        $data['register-minutes'] = $this->db->setting->getInt('account', 'register.minutes', 1);
        $data['auth-type'] = $this->db->setting->getInt('account', 'auth.type', 0);
        $data['auth-login'] = $this->db->setting->getBool('account', 'auth.login', false);
        $data['auth-forgot'] = $this->db->setting->getBool('account', 'auth.forgot', false);
        $data['auth-register'] = $this->db->setting->getBool('account', 'auth.register', false);

        return $data;
    }

    public function saveData(&$data, &$err)
    {
        $this->db->setting->setStr('account', 'auth.theme', ArrayHelper::getStr($data, 'auth-theme', ''));
        $this->db->setting->setStr('account', 'auth.themetemplate', ArrayHelper::getStr($data, 'auth-themetemplate', ''));
        $this->db->setting->setInt('account', 'retry.limit', ArrayHelper::getInt($data, 'retry-limit', 10));
        $this->db->setting->setInt('account', 'lock.minutes', ArrayHelper::getInt($data, 'lock-minutes', 10));
        $this->db->setting->setInt('account', 'log.days', ArrayHelper::getInt($data, 'log-days', 30));
        $this->db->setting->setInt('account', 'register.minutes', ArrayHelper::getInt($data, 'register-minutes', 1));
        $this->db->setting->setInt('account', 'auth.type', ArrayHelper::getInt($data, 'auth-type', 0));
        $this->db->setting->setBool('account', 'auth.login', ArrayHelper::getBool($data, 'auth-login', false));
        $this->db->setting->setBool('account', 'auth.forgot', ArrayHelper::getBool($data, 'auth-forgot', false));
        $this->db->setting->setBool('account', 'auth.register', ArrayHelper::getBool($data, 'auth-register', false));
        return true;
    }

}
