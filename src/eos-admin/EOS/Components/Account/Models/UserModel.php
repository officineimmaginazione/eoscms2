<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Security\CryptHelper;
use EOS\System\Util\ArrayHelper;

class UserModel extends \EOS\Components\System\Classes\AuthModel
{

    private function checkUserExists($username, $email, $idSkip)
    {
        $q = $this->db->prepare('select id from #__account_user where ((username = :username) or (email = :email)) and id <> :id');
        $q->bindValue(':username', $username);
        $q->bindValue(':email', $email);
        $q->bindValue(':id', $idSkip);
        $q->execute();
        $r = $q->fetch();
        return empty($r);
    }

    private function createUniqueUsername()
    {

        $res = '';
        while (empty($res))
        {
            // genero 8 caratteri random tra numeri e lettere
            $letters = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'), ['.', '-', '_']);
            for ($i = 0; $i < 8; $i++)
            {
                $res .= $letters[rand(0, count($letters) - 1)];
            }
            // genero 4 bytes random (8 caratteri random) 
            $res .= \EOS\System\Security\CryptHelper::randomString(4);
            // Converto il timestamp in coda
            $dt = (new \DateTimeImmutable())->getTimestamp();
            $res .= $res . dechex($dt);
            // Verifico che non sia presente nel DB
            $q = $this->db->prepare('select id from #__account_user where username = :username');
            $q->bindValue(':username', $res);
            $q->execute();
            $r = $q->fetch();
            $res = empty($r) ? $res : '';
        }
        return $res;
    }

    private function getUserGroup($id, $idGroup)
    {
        $q = $this->db->prepare('select * from #__account_user_group where id_user = :id_user and id_group =:id_group');
        $q->bindValue(':id_user', $id);
        $q->bindValue(':id_group', $idGroup);
        $q->execute();
        $r = $q->fetch();
        return $r;
    }
    
    private function saveGroupData($id, $group)
    {
        $l = empty($group) ? [$this->getDefGroupID()] : (is_array($group) ? $group : [$group]);

        $pl = [];
        for ($i = 0; $i < count($l); $i++)
        {
            $pl[] = ':g_' . $i;
        }
        $sql = 'delete from #__account_user_group where id_user = :id_user and id_group not in (' .
            implode(',', $pl) . ')';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_user', $id);
        for ($i = 0; $i < count($l); $i++)
        {
            $q->bindValue(':g_' . $i, $l[$i]);
        }
        $q->execute();
        $tblContent = $this->db->tableFix('#__account_user_group');
        foreach ($l as $v)
        {
            $values = [
                'id_user' => $id,
                'id_group' => $v,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];

            if (empty($this->getUserGroup($id, $v)))
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $query = $this->db->newFluent()->update($tblContent)->set($values)
                        ->where('id_user', $id)->where('id_group', $v);
            }
            $query->execute();
        }
    }

    private function getUserGroups($id)
    {
        $q = $this->db->prepare('select id_user, id_group from #__account_user_group where id_user = :id_user');
        $q->bindValue(':id_user', $id);
        $q->execute();
        $list = $q->fetchAll();
        $res = [];
        foreach ($list as $r)
        {
            $res[] = $r['id_group'];
        }
        return $res;
    }

    public function getListQuery()
    {
        // Create view to display user
        /* CREATE VIEW eos_view_user_admin_list AS
        SELECT u.id, u.username, u.email, u.name, u.last_access, l.iso_code as lang, GROUP_CONCAT(g.name) as grouplist, u.status FROM eos_account_user as u left OUTER join eos_lang as l ON l.id = u.id_lang left OUTER join eos_account_user_group ug ON ug.id_user = u.id left OUTER join eos_account_group g ON g.id = ug.id_group group by u.id, u.username, u.email, u.name, u.last_access, lang, u.status; */
        $f = $this->db->newFluent()->from($this->db->tableFix('#__view_user_admin_list'))
            ->select(null)
            ->select('id') 
            ->select('username') 
            ->select('email')
            ->select('name')
            ->select('last_access')
            ->select('lang')
            ->select('grouplist')
            ->select('status');
        return $f;
    }

    public function getGroupList()
    {
        $q = $this->db->prepare('select id, name, def from #__account_group where status = 1 order by name');
        $q->execute();
        return $q->fetchAll();
    }

    public function getDefGroupID()
    {
        $q = $this->db->prepare('select id, name, def from #__account_group where status = 1 and def = 1');
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int) $r['id'];
    }

    /*
      0 Bloccato - Non autentica nulla
      1 Solo Email
      2 Utente
      3 Utente o Mail
     */

    public function getAuthType()
    {
        return $this->db->setting->getInt('account', 'auth.type', 0);
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__account_user u'))
            ->where('u.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
            $pwd = new \EOS\System\Security\Password($this->container);
            $res['password'] = $pwd->generateRandom();
            if ($this->getAuthType() == 1)
            {
                $res['username'] = $this->createUniqueUsername();
            }
            $res['group'] = $this->getDefGroupID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['username'] = $rc['username'];
            $res['email'] = $rc['email'];
            $res['name'] = $rc['name'];
            $res['status'] = (int) $rc['status'];
            $res['id_lang'] = (int) $rc['id_lang'];
            $res['group'] = $this->getUserGroups($rc['id']);
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        // Rimuovo gli spazi all'utente (funziona anche con utf-8 ascii < 128)
        $data['username'] = str_replace(' ', '', $data['username']);
        if ($this->getAuthType() == 1)
        {
            // Quando uso l'autenticazione per EMail genero un nome utente se è vuoto 
            if (empty($data['username']))
            {
                $data['username'] = $this->createUniqueUsername();
            }
        }
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false)
        {
            $error = $this->lang->trans('account.user.error.email');
            $res = false;
        } else if (mb_strlen($data['username']) < 6)
        {
            $error = $this->lang->trans('account.user.error.username');
            $res = false;
        } else
        {
            if ((!empty($data['password'])) || (($data['id'] == 0)))
            {
                if (mb_strlen($data['password']) < 8)
                {
                    $error = $this->lang->trans('account.user.error.password');
                    $res = false;
                }
            }

            if (($res) && (!$this->checkUserExists($data['username'], $data['email'], $data['id'])))
            {
                $error = $this->lang->trans('account.user.error.userexists');
                $res = false;
            }
        }

        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'username' => $data['username'],
                'email' => $data['email'],
                'name' => $data['name'],
                'status' => $data['status'],
                'id_lang' => $data['id_lang'],
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            if (!empty($data['password']))
            {
                $pass = new \EOS\System\Security\Password(null);
                $values['password'] = $pass->hashPassword($data['password']);
                $values['password_date'] = $this->db->util->nowToDBDateTime();
            }
            $tblContent = $this->db->tableFix('#__account_user');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $values['options'] = '{}'; // Imposto nelle options un JSON vuoto
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $data['id'] = $this->db->lastInsertId();
            }
            $this->saveGroupData($data['id'], ArrayHelper::getValue($data, 'group'));
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_user');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    // Default token 1 settimana
    public function addCmd($idUser, $cmd, $expirationMinutes = 10080, $options = [])
    {
        // Cerco di creare un token univoco, anche solo l'id aggiunto lo renderà table
        $token = md5(uniqid(CryptHelper::randomString(15), true)) . CryptHelper::randomString(16);
        $exp = DateTimeHelper::incMinute(DateTimeHelper::now(), $expirationMinutes);
        $sql = 'insert into #__account_user_cmd (token, id_user, cmd, cmddate, cmdexpiration, ip, options) values ' .
            '(:token, :id_user, :cmd, :cmddate, :cmdexpiration, :ip, :options) ';
        $q = $this->db->prepare($sql);
        // Scrivo l'hash del token così per maggiore sicurezza, ma non è univoco e devo usare l'id per averlo univoco
        $q->bindValue(':token', hash('sha512', $token));
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':cmd', $cmd);
        $q->bindValue(':cmddate', $this->db->util->nowToDBDateTime());
        $q->bindValue(':cmdexpiration', $this->db->util->dateTimeToDBDateTime($exp));
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':options', \EOS\System\Util\ArrayHelper::toJSON($options));
        $q->execute();
        // La prima parte è il token e l'ultima l'id
        $token = $token . '-' . $this->db->lastInsertId();
        return $token;
    }

    public function deleteCmd($idUser, $cmd)
    {
        $sql = 'delete from #__account_user_cmd where id_user = :id_user and cmd = :cmd';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':cmd', $cmd);
        $q->execute();
    }

    public function serviceCmd($id, $cmd, &$error)
    {
        $this->deleteCmd($id, $cmd);
        $token = $this->addCmd($id, $cmd);
        // Devo inviare una richiesta alla parte sito perché così usa il template predefinito
        $sv = new \EOS\Components\System\Classes\FrontCommand($this->container);
        $sv->execute('Account', 'service/' . $cmd . '/' . $token);
        $res = !$sv->error;
        if ($sv->error)
        {
            $error = $sv->errorMessage;
        }
        return $res;
    }

    public function requestActiveData($data, &$error)
    {
        return $this->serviceCmd($data['id'], 'request-active', $error);
    }

    public function requestResetPassData($data, &$error)
    {
        return $this->serviceCmd($data['id'], 'request-reset-pass', $error);
    }

    public function getTextList($fluentQuery)
    {
        $res = [];
        $autType = $this->getAuthType();
        foreach ($fluentQuery as $row)
        {
            $v['id'] = $row['id'];
            switch ($autType)
            {
                case 1: // Email
                    $v['text'] = sprintf('%d | %s | %s ', $row['id'], $row['name'], $row['email']);
                    break;
                case 2: // Utente
                    $v['text'] = sprintf('%d | %s | %s', $row['id'], $row['name'], $row['username']);
                    break;
                default: // 0 - 3 Utente o Email
                    $v['text'] = sprintf('%d | %s | %s | %s', $row['id'], $row['name'], $row['username'], $row['email']);
                    break;
            }
            $res[] = $v;
        }
        return $res;
    }

    public function searchList($filter, $limit = 50)
    {
        $res = [];
        if ((mb_strlen($filter) >= 1) && (mb_strlen($filter) < 200))
        {
            $q = $this->getListQuery();
            $fValue = $this->db->quote('%' . $this->db->util->prepareLikeValue($filter) . '%');
            $autType = $this->getAuthType();
            $fList[] = 'u.id like ' . $fValue;
            $fList[] = 'u.name like ' . $fValue;
            switch ($autType)
            {
                case 1: // Email
                    $fList[] = 'u.email like ' . $fValue;
                    break;
                case 2: // Utente
                    $fList[] = 'u.username like ' . $fValue;
                    break;
                default: // 0 - 3 Utente o Email
                    $fList[] = 'u.email like ' . $fValue;
                    $fList[] = 'u.username like ' . $fValue;
                    break;
            }
            $q->where(implode("\n OR ", $fList));
            if ($limit > 0)
            {
                $q->limit($limit);
            }
            $res = $this->getTextList($q);
        }
        return $res;
    }
    
    public function getUsersByGroup($idGroup)
    {
        $sql = 'select u.id, u.name from #__account_user u ';
        $sql .=  'inner join #__account_user_group ug ON ug.id_user = u.id ';
        $sql .=  'where ug.id_group = :id_group ';
        $sql .=  'order by u.name';
        //print_r($sql); exit();
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_group', $idGroup);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function getUsers(int $idGroup = 0, $status = null)
    {
        $sql = 'select u.id, u.name from #__account_user u ';
        $sql .=  'inner join #__account_user_group ug ON ug.id_user = u.id ';
        if($idGroup != 0) {
            $sql .=  'where ug.id_group = :id_group ';
            if($status != null) {
                $sql .=  ' and u.status = :status ';
            }
        } else {
            if($status != null) {
                $sql .=  'where u.status = :status ';
            }
        }
        $sql .=  'order by u.name';
        $q = $this->db->prepare($sql);
        if($idGroup != 0) {
            $q->bindValue(':id_group', $idGroup);
        }
        if($status != null) {
            $q->bindValue(':status', $status);
        }
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

}
