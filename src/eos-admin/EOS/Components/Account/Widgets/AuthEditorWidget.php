<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Widgets;

use EOS\System\Util\ArrayHelper;

class AuthEditorWidget extends \EOS\UI\Widget\Widget
{

    public $groups = [];
    public $values = [];
    
    public function write()
    {
        $m = new \EOS\Components\Account\Models\UserModel($this->view->controller->container);
        $this->groups = $m->getGroupList(); 
        $this->renderTemplate('widget/autheditor');
    }

}
