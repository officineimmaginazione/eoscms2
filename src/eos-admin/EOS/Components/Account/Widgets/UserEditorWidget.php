<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Widgets;

use EOS\System\Util\ArrayHelper;

class UserEditorWidget extends \EOS\UI\Widget\Widget
{

    public $data = [];
    public $dataField = 'id_user';
    public $disabled = false; 
    public $multiple = false;

    public function write()
    {
        $this->renderTemplate('widget/usereditor');
    }

    public function getCurrentInfo()
    {
        $m = new \EOS\Components\Account\Models\UserModel($this->view->controller->container);
        $q = $m->getListQuery();
        $q->where('u.id', ArrayHelper::getInt($this->data, $this->dataField));
        return $m->getTextList($q);
    }

}
