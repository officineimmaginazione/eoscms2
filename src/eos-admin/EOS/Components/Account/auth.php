<?php

$this->add('account.log', 'account.dashboard.menu,account.dashboard.log');
$this->add('account.setting', 'account.dashboard.menu,account.dashboard.setting');
$this->add('account.user', 'account.dashboard.menu,account.dashboard.user');
$this->add('account.widget', 'account.dashboard.menu,account.dashboard.widget');
$this->add('account.address', 'account.dashboard.menu,account.dashboard.address');
$this->add('account.group', 'account.dashboard.menu,account.dashboard.group');