<?php

return
    [
        'account.dashboard.menu' => 'Account',
        'account.dashboard.user' => 'Account',
        'account.dashboard.log' => 'Log',
        'account.dashboard.setting' => 'Impostazioni',
        'account.dashboard.widget' => 'Widget',
        'account.dashboard.address' => 'Indirizzi',
        'account.dashboard.group' => 'Gruppi account',
];
