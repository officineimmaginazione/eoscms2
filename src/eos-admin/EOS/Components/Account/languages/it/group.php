<?php

return
    [
        'account.group.index.title' => 'Gruppi',
        'account.group.edit.title' => 'Modifica gruppo',
        'account.group.insert.title' => 'Inserisci gruppo',
        'account.group.field.id' => 'ID',
        'account.group.field.name' => 'Nome',
        'account.group.field.email' => 'E-mail',
        'account.group.field.status' => 'Attivo',
        'account.group.field.status.0' => 'No',
        'account.group.field.status.1' => 'Si',
        'account.group.field.status.2' => 'Attesa conferma attivazione',
        'account.group.field.alias' => 'Alias indirizzo',
];
