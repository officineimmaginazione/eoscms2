<?php

return [
    'account.log.index.title' => 'Log',
    'account.log.field.id' => 'ID',
    'account.log.field.ip' => 'IP',
    'account.log.field.logdate' => 'Data',
    'account.log.field.logname' => 'Accesso',
    'account.log.field.id_user' => 'IDUtente',
    'account.log.field.cmd' => 'Comando',
    'account.log.field.status' => 'Stato',
    'account.log.error' => 'Errore',
    'account.log.status.1' => 'Utente non valido',
    'account.log.status.2' => 'Password errata',
    'account.log.status.99' => 'Bloccato IP',
    'account.log.status.100' => 'Valido',
    'account.log.delete.confirm' => 'Vuoi cancellare la riga?'
];
