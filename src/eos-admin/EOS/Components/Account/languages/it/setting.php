<?php

return
    [
        'account.setting.index.title' => 'Impostazioni account',
        'account.setting.index.data' => 'Parametri',
        'account.setting.log.days' => 'Mantieni i log per giorni:',
        'account.setting.lock.minutes' => 'Blocca IP/Utente per minuti:',
        'account.setting.retry.limit' => 'Numero massimo di tentativi IP/Utente:',
        'account.setting.register.minutes' => 'Registrazioni/IP al minuto:',
        'account.setting.auth.type' => 'Tipo di autenticazione',
        'account.setting.auth.login' => 'Attiva login',
        'account.setting.auth.forgot' => 'Attiva recupero credenziali',
        'account.setting.auth.register' => 'Attiva registrazione',
        'account.setting.auth.theme' => 'Tema area riservata',
        'account.setting.auth.themetemplate' => 'Layout'
];
