<?php

return
    [
        'account.user.index.title' => 'Account',
        'account.user.edit.title' => 'Modifica account',
        'account.user.insert.title' => 'Inserisci account',
        'account.user.field.id' => 'ID',
        'account.user.field.username' => 'Account',
        'account.user.field.email' => 'E-mail',
        'account.user.field.emailalternative' => 'E-mail alternativa',
        'account.user.field.status' => 'Attivo',
        'account.user.field.status.0' => 'No',
        'account.user.field.status.1' => 'Si',
        'account.user.field.status.2' => 'Attesa conferma attivazione',
        'account.user.field.name' => 'Nome/Etichetta',
        'account.user.field.password' => 'Password',
        'account.user.field.last_access' => 'Ultimo accesso',
        'account.user.field.group' => 'Gruppo',
        'account.user.field.lang' => 'Lingua',
        'account.user.error.username' => 'Utente non valido!',
        'account.user.error.email' => 'Email non valida!',
        'account.user.error.password' => 'Password non valida! (Almeno 8 caratteri)',
        'account.user.error.userexists' => 'Utente già inserito!',
        'account.user.request.active' => 'Invia mail di attivazione',
        'account.user.request.active.question' => 'Vuoi richiedere conferma mail?',
        'account.user.request.reset-pass' => 'Invia reset password',
        'account.user.request.reset-pass.question' => 'Vuoi richiere il reset della password?'
];
