<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))->attr('name', 'status')
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-address', $this->trans('account.address.tab.address'));
$tab->addItem('tab-company', $this->trans('account.address.tab.company'));
$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.alias'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('alias'))
            ->placeholder($this->transE('account.address.field.alias.placeholder'))
            ->bind($this->data, 'alias')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.account'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id-customer'))
            ->attr('class', 'id-customer')
            ->bind($this->data, 'id-customer')
            ->bindList($this->account, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.name'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('name'))
            ->bind($this->data, 'name')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.surname'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('surname'))
            ->bind($this->data, 'surname')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.phone'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('phone'))
        ->bind($this->data, 'phone')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.phone_mobile'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('phone-mobile'))
        ->bind($this->data, 'phone-mobile')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-data');

$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.address1'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('address1'))
        ->bind($this->data, 'address1')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.address2'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('address2'))
        ->bind($this->data, 'address2')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-4">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.city'))->printRender();
        echo '</div>';
        echo '<div>';
        $url = $this->path->getUrlFor('account', 'address/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        (new \EOS\UI\Bootstrap\Select2('id-city'))
            ->addRawParams('ajax', '{delay:600, ' .
            'dataType: "json", url:"' . $url . '"}')
            ->addRawParams('minimumInputLength', 2)
            ->bind($this->data, 'id-city')
            ->bindList($this->data['current-city'], 'id', 'text')
            ->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-4">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.state'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('state'))
            ->attr('disabled', '')
            ->bind($this->data, 'state')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-4">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.zipcode'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('zipcode'))
            ->attr('disabled', '')
            ->bind($this->data, 'zipcode')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';   

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    
    
}, $this)->printRender();

$tab->endTab('tab-address');

$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.company'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('company'))
        ->bind($this->data, 'company')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.vat'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('vat'))
        ->bind($this->data, 'vat')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.address.field.cif'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('cif'))
        ->bind($this->data, 'cif')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-company');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData(proc)
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('account', 'address/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    if (proc === undefined)
                    {

                        location.href = json.redirect;
                    } else
                    {
                        proc();
                    }
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'address/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('account.address.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('account', 'address/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    $(function ()
    {
        $('#status').on('change', function ()
        {
            if ($('#status').val() == '2')
            {
                $('#btn-request-active').show();
            } else
            {
                $('#btn-request-active').hide();
            }
        });
        $('#status').trigger('change');
    });
    
    function runAjax(urlAjax, data, resultEvent)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: urlAjax,
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    resultEvent(json);
                } else
                {
                    if (json.message !== undefined)
                    {
                        bootbox.alert(json.message);
                    }
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
    }
    
    $(function ()
    {
        var options = $('#id_city');
        var updateCoords = false;
        options.on('change', function ()
        {
            var url = '<?php echo $this->path->getUrlFor('account', 'address/ajaxCommand'); ?>';
            var data = {"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", 'command': 'city', 'id': options.val()};
            runAjax(url, data, function (data)
            {
                $("input[name='zipcode']").val(data.city.zip_code);
                $("input[name='state']").val(data.city.state);
                if (updateCoords)
                {
                    $('input[name=\'latitude\']').val(data.city.latitude);
                    $('input[name=\'longitude\']').val(data.city.longitude);
                }
                updateCoords = true;
            });
        });
        options.trigger('change');
    });
    
</script>
<?php
$this->endCaptureScript();
