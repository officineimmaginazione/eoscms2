<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('Account', 'log/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('account.log.field.id'), false, false, false);
$tbl->addColumn('logdate', $this->transE('account.log.field.logdate'));
$tbl->addColumn('ip', $this->transE('account.log.field.ip'));
$tbl->addColumn('logname', $this->transE('account.log.field.logname'));
$tbl->addColumn('id_user', $this->transE('account.log.field.id_user'));
$tbl->addColumn('cmd', $this->transE('account.log.field.cmd'));
$tbl->addColumn('status', $this->transE('account.log.field.status'));
$tbl->addColumnOrder('logdate', false);
$tbl->addColumnCustom('', 'del-row', '<button class="btn  btn-danger btn-sm" id="btn-new"><i class="fa fa-trash"></i></button>');
$tbl->clickRow('function (e, r) {e.stopPropagation(); delRow(r.data().id);}', 'del-row');
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);
$this->startCaptureScript();
?>
<script>
    function renderStatus(data)
    {
        var error = true;
        var text = '<?php $this->transP('account.log.error'); ?> : ' + data;
        switch (parseInt(data))
        {
            case 1:
                text = '<?php $this->transP('account.log.status.1'); ?>';
                break;
            case 2:
                text = '<?php $this->transP('account.log.status.2'); ?>';
                break;
            case 99:
                text = '<?php $this->transP('account.log.status.99'); ?>';
                break;
            case 100:
                text = '<?php $this->transP('account.log.status.100'); ?>';
                error = false;
                break;
        }
        if (error)
        {
            return '<span class="label label-danger">' + text + '</span>';
        } else
        {
            return '<span class="label label-success">' + text + '</span>';

        }
    }


    function delRow(id)
    {
        bootbox.confirm("<?php $this->transPE('account.log.delete.confirm'); ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('Account', ['log', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": id}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            $('#datatable').DataTable().ajax.reload();
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();
