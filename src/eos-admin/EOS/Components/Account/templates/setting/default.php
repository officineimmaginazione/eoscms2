<?php
$formData = new EOS\UI\Html\Form();
$formData->id('data-form');
$formData->attr('method', 'POST');
$formData->attr('class', 'form-horizontal');
$formData->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
$tab = new EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('account.setting.index.data'));
$tab->startTab();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.theme'))->attr('class', 'col-xs-2 text-right')->printRender();

    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Select('auth-theme'))
        ->bind($this->data, 'auth-theme')
        ->bindList($this->themeList, 'name', 'name')
        ->printRender($this);
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.themetemplate'))->attr('class', 'col-xs-2 text-right')->printRender();

    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Select('auth-themetemplate'))
        ->bind($this->data, 'auth-themetemplate')
        ->bindList($this->themeTemplateList, 'name', 'name')
        ->printRender($this);
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('account.setting.retry.limit'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('retry-limit'))
        ->type('number')
        ->bind($this->data, 'retry-limit')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('account.setting.lock.minutes'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('lock-minutes'))
        ->type('number')
        ->bind($this->data, 'lock-minutes')
        ->printRender($this);
    echo '</div>';

    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('account.setting.log.days'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('log-days'))
        ->type('number')
        ->bind($this->data, 'log-days')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('account.setting.register.minutes'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('register-minutes'))
        ->type('number')
        ->bind($this->data, 'register-minutes')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    $list = [];
    $list[] = ['key' => '0', 'descr' => 'None'];
    $list[] = ['key' => '1', 'descr' => 'EMail'];
    $list[] = ['key' => '2', 'descr' => 'Username'];
    $list[] = ['key' => '3', 'descr' => 'Email/Username'];
    echo '<div class="col-sm-4">';
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.type'))->printRender();
    (new \EOS\UI\Bootstrap\Select('auth-type'))
        ->bind($this->data, 'auth-type')
        ->bindList($list, 'key', 'descr')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-sm-12">';
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.login'))->printRender();
    (new \EOS\UI\Bootstrap\Checkbox('auth-login'))
        ->bind($this->data, 'auth-login')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-12">';
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.forgot'))->printRender();
    (new \EOS\UI\Bootstrap\Checkbox('auth-forgot'))
        ->bind($this->data, 'auth-forgot')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-12">';
    (new \EOS\UI\Html\Label)->content($this->transE('account.setting.auth.register'))->printRender();
    (new \EOS\UI\Bootstrap\Checkbox('auth-register'))
        ->bind($this->data, 'auth-register')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
$tab->endTab('tab-data');
$tab->printRender($this);
$box->endContent();
$box->startFooter();
echo '<div class="row"><div class="col-xs-12">';
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
echo '</div></div>';
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$formData->endContent();
$formData->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#data-form').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('account', 'setting/ajaxsavedata'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

</script>
<?php
$this->endCaptureScript();
