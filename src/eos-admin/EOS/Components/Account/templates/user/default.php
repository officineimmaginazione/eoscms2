<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-info btn-sm pull-right')
        ->click('function (e) { addNew();}')
        ->content('<i class="fa fa-plus"></i>')
        ->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('Account', 'user/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('account.user.field.id'));
$tbl->addColumn('username', $this->transE('account.user.field.username'));
$tbl->addColumn('email', $this->transE('account.user.field.email'));
$tbl->addColumn('name', $this->transE('account.user.field.name'));
$tbl->addColumn('last_access', $this->transE('account.user.field.last_access'));
$tbl->addColumn('lang', $this->transE('account.user.field.lang'));
$tbl->addColumn('grouplist', $this->transE('account.user.field.group'), true, false);
$tbl->addColumn('status', $this->transE('account.user.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->addColumnOrder('id', true);
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'user/edit/0') ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'user/edit') ?>" + id + "/";
    }

    function renderStatus(data)
    {
        switch (parseInt(data))
        {
            case 1:
                return '<span class="label label-success"><?php $this->transP('account.user.field.status.1'); ?></span>';
                break;
            case 2:
                return '<span class="label label-warning"><?php $this->transP('account.user.field.status.2'); ?></span>';
                break;
            default:
                return '<span class="label label-danger"><?php $this->transP('account.user.field.status.0'); ?></span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
