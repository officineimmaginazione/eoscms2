<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {

    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-extra', $this->trans('system.common.extra'));

$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.email'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('email'))
            ->bind($this->data, 'email')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.username'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('username'))
            ->bind($this->data, 'username')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.name'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('name'))
        ->bind($this->data, 'name')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.password'))->printRender();
        echo '</div>';
        echo '<div>';
        $pwType = \EOS\System\Util\ArrayHelper::getInt($this->data, 'id') == 0 ? 'text' : 'password';
        (new \EOS\UI\Bootstrap\Input('password'))
            ->type($pwType)
            ->bind($this->data, 'password')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-data');


$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';

    echo '</div>';
    echo '<div class="col-12 col-md-12">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.group'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('group'))
            ->multiple()
            ->bind($this->data, 'group')
            ->bindList($this->groups, 'id', 'name')
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.lang'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id_lang'))
        ->id('id_lang')
        ->bind($this->data, 'id_lang')
        ->bindList($this->languages, 'id', 'name')
        ->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.status'))->printRender();
        echo '</div>';
        echo '<div>';
        $statusList [] = ['id' => 0, 'name' => $this->transE('account.user.field.status.0')];
        $statusList [] = ['id' => 1, 'name' => $this->transE('account.user.field.status.1')];
        $statusList [] = ['id' => 2, 'name' => $this->transE('account.user.field.status.2')];
        (new \EOS\UI\Bootstrap\Select('status'))
            ->id('status')
            ->bind($this->data, 'status')
            ->bindList($statusList, 'id', 'name')
            ->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-extra');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData(proc)
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('account', 'user/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    if (proc === undefined)
                    {

                        location.href = json.redirect;
                    } else
                    {
                        proc();
                    }
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'user/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('account.user.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('account', 'user/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function requestActive()
    {
        bootbox.confirm("<?php $this->transPE('account.user.request.active.question') ?>", function (result)
        {
            if (result)
            {
                saveData(function ()
                {
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '<?php echo $this->path->getUrlFor('account', 'user/ajaxrequestactive'); ?>',
                        data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                        contentType: "application/json",
                    })
                        .done(function (json)
                        {
                            if (json.result == true)
                            {
                                location.href = json.redirect;
                            } else
                            {
                                bootbox.alert(json.message);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error)
                        {
                            var err = textStatus + ", " + error;
                            alert(err);
                        });
                });
            }
        });
    }

    function requestResetPass()
    {
        bootbox.confirm("<?php $this->transPE('account.user.request.reset-pass.question') ?>", function (result)
        {
            if (result)
            {
                saveData(function ()
                {
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '<?php echo $this->path->getUrlFor('account', 'user/ajaxrequestresetpass'); ?>',
                        data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                        contentType: "application/json",
                    })
                        .done(function (json)
                        {
                            if (json.result == true)
                            {
                                location.href = json.redirect;
                            } else
                            {
                                bootbox.alert(json.message);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error)
                        {
                            var err = textStatus + ", " + error;
                            alert(err);
                        });
                });
            }
        });
    }

    $(function ()
    {
        $('#status').on('change', function ()
        {
            if ($('#status').val() == '2')
            {
                $('#btn-request-active').show();
            } else
            {
                $('#btn-request-active').hide();
            }
        });
        $('#status').trigger('change');
    });
</script>
<?php
$this->endCaptureScript();
