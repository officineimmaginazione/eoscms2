<?php

$groups = $this->groups;
$values = $this->values;
(new \EOS\UI\Bootstrap\FormGroup('account-autheditor'))->addContent(function () use ($groups, $values)
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('account.user.field.group'))->attr('class', 'col-xs-2 text-right')->printRender();
        echo '</div>';
        (new \EOS\UI\Bootstrap\Select2('account-group'))
        ->multiple()
        ->addRawParams('theme', '"bootstrap4"')
        ->bind(['values' => $values], 'values')
        ->bindList($groups, 'id', 'name')
        ->attr('style', 'width: 100%')
        ->printRender($this);
    }, $this->view)
    ->attr('class', 'form-group-custom select2-hidden-accessible')
    ->printRender();
$this->view->util->addScriptBlockReady('$(\'#account-autheditor\').removeClass(\'select2-hidden-accessible\');');
