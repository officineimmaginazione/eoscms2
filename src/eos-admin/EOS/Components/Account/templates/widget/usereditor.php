<?php

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->view->transE('account.user.field.username'))->attr('class', 'col-sm-2 text-right')->printRender();
    echo '<div class="col-sm-8">';
    $currInfo = $this->getCurrentInfo();
    $url = $this->view->path->getUrlFor('account', 'user/getdatasource/user/select2/' . $this->view->session->getTokenName() . '/' . $this->view->session->getTokenValue());
    $select = (new \EOS\UI\Bootstrap\Select2($this->dataField))
        ->addRawParams('ajax', '{delay:400, ' .
            'dataType: "json", url:"' . $url . '"}')
        ->addRawParams('minimumInputLength', 1);
    if ($this->multiple)
    {
        $select->multiple();
    }
    $select->bind($this->data, $this->dataField)
        ->bindList($currInfo, 'id', 'text');
    $select->printRender($this->view);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Bootstrap\Button($this->dataField . '_edit'))
        ->content('<i class="fa fa-edit"></i>')
        ->attr('class', 'pull-left')
        ->click('function (ev)
             {
             ev.preventDefault();
             $("#' . $this->dataField . '").removeAttr("disabled");
             $("#' . $this->dataField . '_edit").hide();
             }')
        ->printRender($this->view);
    echo '</div>';
    $this->view->startCaptureScriptReady();
    if (empty($currInfo) || ($this->disabled == false))
    {
        echo '$("#' . $this->dataField . '_edit").hide();';
    } else
    {
        echo '$("#' . $this->dataField . '").attr("disabled", true);';
    }
    $this->view->endCaptureScriptReady();
}, $this)->printRender($this->view);
