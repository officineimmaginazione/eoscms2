<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Classes;

class ContentType
{

    const Page = 0;
    const Article = 1;
    const Category = 2;
    const Section = 3;

    public static function getStr($ctp)
    {
        $res = 'page';
        switch ($ctp)
        {
            case static::Article:
                $res = 'article';
                break;
            case static::Category:
                $res = 'category';
                break;
            case static::Section:
                $res = 'section';
                break;
        }
        return $res;
    }

}
