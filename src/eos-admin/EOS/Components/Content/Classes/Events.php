<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Classes;

class Events
{

    // Edit
    const CONTENT_EDIT_FORM_AJAX_CONFIG = 'content.content.edit.form.ajax.config';
    const CONTENT_EDIT_DATA_PARSE = 'content.content.edit.data.parse';
    const CONTENT_EDIT_DATA_PREPARE = 'content.content.edit.data.prepare';
    const CONTENT_EDIT_DATA_BEFORE_SAVE = 'content.content.edit.data.before.save';
    const CONTENT_EDIT_DATA_AFTER_SAVE = 'content.content.edit.data.after.save';
    const CONTENT_EDIT_DATA_BEFORE_DELETE = 'content.content.edit.data.before.delete';
    const CONTENT_EDIT_TEMPLATE_RENDER = 'content.content.edit.template.render';
    // List/Default
    const CONTENT_LIST_DATA_PARSE = 'content.content.list.data.parse';
    const CONTENT_LIST_TEMPLATE_RENDER = 'content.content.list.template.render';

}
