<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Controllers;

use EOS\Components\Content\Classes\ContentType;
use EOS\System\Util\StringHelper;

class CategoryController extends ContentController
{

    protected function getType()
    {
        return ContentType::Category;
    }
    
    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->type = $this->getType();
        $v->typeStr = ContentType::getStr($v->type);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('content.content.' . $v->typeStr . '.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if (!empty($request->getQueryParam('id_lang')))
        {
            $nl = $this->lang->getLangFromDB($request->getQueryParam('id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
                $this->session->set('content.content.id_lang', $request->getQueryParam('id_lang'));
            }
        } else
        {
            if ($this->session->has('content.content.id_lang'))
            {
                $nl = $this->lang->getLangFromDB($this->session->get('content.content.id_lang'));
                if (!empty($nl))
                {
                    $v->dataLangDefault = $nl;
                }
            }
        }
        $m = $this->newModel();
        $v->items = StringHelper::encodeHtmlArray($m->getCategoryList($v->dataLangDefault['id'], 0, false), ['title']);
        return $v->render('category/default', true);
    }

}
