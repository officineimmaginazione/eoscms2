<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Controllers;

use EOS\System\Util\FormValidator;
use EOS\Components\Content\Classes\ContentType;

class ContentController extends \EOS\Components\System\Classes\AuthController
{

    protected function getType()
    {
        return ContentType::Page;
    }

    protected function newModel()
    {
        return new \EOS\Components\Content\Models\ContentModel($this->container, $this->getType());
    }

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->type = $this->getType();
        $v->typeStr = ContentType::getStr($v->type);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('content.content.' . $v->typeStr . '.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('content.content.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('content.content.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        return $v->render('content/default', true);
    }

    public function edit($request, $response, $args)
    {
        $m = $this->newModel();
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->type = $this->getType();
        $v->typeStr = ContentType::getStr($v->type);
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->getUrlFor('Content', $v->typeStr . '/index'), $v->trans('content.content.' . $v->typeStr . '.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('content.content.' . $v->typeStr . '.insert.title') : $v->trans('content.content.' . $v->typeStr . '.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->countChild = 0;
        if ($this->session->has('content.content.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('content.content.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        $v->data = $m->getData($args['id']);
        $v->customCodeList = $this->getCustomCodeFileList();
        if (in_array($v->type, [ContentType::Category, ContentType::Article]))
        {
            $v->category[] = [];
            foreach ($v->dataLangList as $l)
            {
                $v->category[$l['id']] = $m->getCategoryList($l['id'], isset($v->data['id']) ? $v->data['id'] : 0, true);
            }
            $v->countChild = $m->countChild($args['id']);
        }
        return $v->render('content/edit', true);
    }

    public function getCustomCodeFileList()
    {
        $sitetheme = $this->container->get('database')->setting->getStr('setting', 'theme');
        $list = glob(_EOS_PATH_THEMES_ . $sitetheme . DIRECTORY_SEPARATOR . 'custom' . DIRECTORY_SEPARATOR . '*.php');
        $res[] = ['name' => ''];
        foreach ($list as $item)
        {
            $t = basename($item, '.php');
            $res[] = ['name' => $t];
        }
        return $res;
    }

    public function ajaxList($request, $response, $args)
    {
        $m = $this->newModel();
        $data = $request->getParsedBody();
        if ($this->session->isValidTokenArrayKey($data))
        {
            $this->session->set('content.content.id_lang', (int) $data['id_lang']);
        }
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery((int) $data['id_lang']), $m->db);
        $dts->addColumn('id', 'c.id');
        $dts->addColumn('title', 'cl.title', true);
        $dts->addColumn('status', 'c.status');
        if ($this->getType() != ContentType::Article)
        {
            $dts->addColumn('url', 'rt.path', true);
        }

        $ev = new \EOS\System\Event\DataEvent($this, $dts->toArray());
        $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_LIST_DATA_PARSE, $ev);
        return $response->withJson($ev->getData());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        $fv->sanitizeSkipFields = ['content-'];
        $langlist = $this->lang->getArrayFromDB(true);
        foreach ($langlist as $l)
        {
            $fv->sanitizeSkipFields[] = 'extra-' . $l['id'] . '-html-';
        }
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson())
        {
            $m = $this->newModel();
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('content', ContentType::getStr($this->getType()) . '/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = $this->newModel();
            if ($m->deleteData($fv->getInputData(), $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('content', ContentType::getStr($this->getType()) . '/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }

        return $fv->toJsonResponse();
    }

}
