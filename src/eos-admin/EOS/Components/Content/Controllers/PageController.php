<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Controllers;

use EOS\Components\Content\Classes\ContentType;

class PageController extends ContentController
{

    protected function getType()
    {
        return ContentType::Page;
    }

}
