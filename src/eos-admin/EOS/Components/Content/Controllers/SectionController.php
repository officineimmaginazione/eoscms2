<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Controllers;

use EOS\Components\Content\Classes\ContentType;

class SectionController extends ContentController
{

    protected function getType()
    {
        return ContentType::Section;
    }

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->type = $this->getType();
        $v->typeStr = ContentType::getStr($v->type);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('content.content.' . $v->typeStr . '.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('content.content.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('content.content.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        return $v->render('section/default', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = $this->newModel();
        $data = $request->getParsedBody();
        if ($this->session->isValidTokenArrayKey($data))
        {
            $this->session->set('content.content.id_lang', (int) $data['id_lang']);
        }
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery((int) $data['id_lang']), $m->db);
        $dts->addColumn('id', 'c.id');
        $dts->addColumn('title', 'cl.title', true);
        $dts->addColumn('name', 'cs.name', true);
        $dts->addColumn('status', 'c.status');
        return $response->withJson($dts->toArray());
    }

}
