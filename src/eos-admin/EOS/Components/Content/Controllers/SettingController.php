<?php

namespace EOS\Components\Content\Controllers;

use EOS\System\Util\FormValidator;

class SettingController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->pageTitle = $v->trans('content.setting.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $m = new \EOS\Components\Content\Models\SettingModel($this->container);
        $v->loadiCheck();
        $v->data = $m->getData();
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('content.setting.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('content.setting.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        return $v->render('setting/default', true);
    }

    public function ajaxSaveData($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Content\Models\SettingModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('content', 'setting/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
