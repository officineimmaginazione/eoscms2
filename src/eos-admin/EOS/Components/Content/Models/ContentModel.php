<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Models;

use \EOS\Components\Content\Classes\ContentType;
use \EOS\System\Routing\PathHelper;
use \EOS\System\Util\ArrayHelper;

class ContentModel extends \EOS\Components\System\Classes\AuthModel
{

    protected $typeStr = '';
    protected $type = -1;

    public function __construct($container, $type)
    {
        $this->setType($type);
        parent::__construct($container);
    }

    protected function setType($type)
    {
        $this->type = $type;
        $this->typeStr = ContentType::getStr($type);
    }

    private function getDBRoute($id)
    {
        return 'content/' . $this->typeStr . '/' . $id;
    }

    private function getCompName()
    {
        return 'content/' . $this->typeStr;
    }

    public function getExtraFields()
    {
        $res = ['count' => 0];
        if (in_array($this->type, [ContentType::Article, ContentType::Page]))
        {
            $v = json_decode($this->db->setting->getStr('content', $this->typeStr . '.extra'), true);
            $res['count'] = ArrayHelper::getInt($v, 'count');
            $res['text'] = ArrayHelper::getInt($v, 'text');
            $res['image'] = ArrayHelper::getInt($v, 'image');
            $res['video'] = ArrayHelper::getInt($v, 'video');
            $res['link'] = ArrayHelper::getInt($v, 'link');
            $res['html'] = ArrayHelper::getInt($v, 'html');
        }
        return $res;
    }

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->where('cl.id_lang = ' . (int) $id_lang)
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->where('c.type = ?', $this->type)
            ->orderBy('cl.title');
        if ($this->useRouter())
        {
            if ($this->db->driverName == 'sqlite')
            {
                $f->leftJoin(sprintf("%s rt on ('content/%s/' || cl.id_content) = rt.route and rt.lang in (select iso_code from %s where id = %d)", $this->db->tableFix('#__router'), $this->typeStr, $this->db->tableFix('#__lang'), $id_lang));
            } else
            {
                $f->leftJoin(sprintf("%s rt on CONCAT('content/%s/', cl.id_content) = rt.route and rt.lang in (select iso_code from %s where id = %d)", $this->db->tableFix('#__router'), $this->typeStr, $this->db->tableFix('#__lang'), $id_lang));
            }
        }
        if ($this->type == ContentType::Article)
        {
            $f->leftJoin($this->db->tableFix('#__content_article ca ON ca.id_content_lang = cl.id'));
            $f->leftJoin($this->db->tableFix('#__content cat ON cat.id = c.parent'));
            $f->leftJoin($this->db->tableFix('#__content_lang catl ON  catl.id_content = cat.id and catl.id_lang =' . (int) $id_lang));
        }
        if ($this->type == ContentType::Section)
        {
            $f->leftJoin($this->db->tableFix('#__content_section cs ON cs.id_content = c.id'));
        }
        return $f;
    }

    private function getCategoryAndChild($id_lang, $id_parent, $id_exclude, $level, &$res)
    {
        if ($level > 99)
        {
            throw new \Exception('ContentMode->getCategory up 99 level!');
        }
        $query = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->select(null)->select('c.id, c.parent, cl.title, c.status, rt.path as url')
            ->where('cl.id_lang = ' . (int) $id_lang)
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->where('c.type = ?', ContentType::Category)
            ->where('c.parent = ?', $id_parent)
            ->orderBy('cl.title');
        if ($this->db->driverName == 'sqlite')
        {
            $query->leftJoin(sprintf("%s rt on ('content/%s/' || cl.id_content) = rt.route and rt.lang in (select iso_code from %s where id = %d)", $this->db->tableFix('#__router'), ContentType::getStr(ContentType::Category), $this->db->tableFix('#__lang'), $id_lang));
        } else
        {
            $query->leftJoin(sprintf("%s rt on CONCAT('content/%s/', cl.id_content) = rt.route and rt.lang in (select iso_code from %s where id = %d)", $this->db->tableFix('#__router'), ContentType::getStr(ContentType::Category), $this->db->tableFix('#__lang'), $id_lang));
        }

        $list = $query->fetchAll();
        if (!empty($list))
        {
            $space = '';
            if ($level > 0)
            {
                $space = str_replace(' ', '―', str_pad('', $level));
                $space = ' ' . $space . ' ';
            }
            foreach ($list as $r)
            {
                if ($r['id'] != $id_exclude)
                {
                    $res[] = ['id' => $r['id'], 'title' => $space . $r['title'],
                        'status' => $r['status'], 'url' => $r['url'], 'level' => $level];
                    $this->getCategoryAndChild($id_lang, $r['id'], $id_exclude, $level + 1, $res);
                }
            }
        }
    }

    public function getCategoryList($id_lang, $id_exclude, $blankRow = true)
    {
        $res = [];
        if ($blankRow)
        {
            $res[] = ['id' => 0, 'title' => '', 'status' => 0, 'level' => 0];
        }

        $this->getCategoryAndChild($id_lang, 0, $id_exclude, 0, $res);
        return $res;
    }

    public function countChild($id)
    {
        $res = 0;
        if ($id != 0)
        {
            $query = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
                ->select(null)->select('count(c.id) as ct')
                ->where('c.parent = ?', (int) $id);
            $r = $query->fetch();
            if (!empty($r))
            {
                $res = $r['ct'];
            }
        }
        return $res;
    }

    public function getData($id)
    {
        $ll = $this->lang->getArrayFromDB(true);
        $query = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->select(null)
            ->select('c.id, c.status, c.type, c.parent, c.extra_status, c.extra')
            ->where('c.type = ?', $this->type)
            ->where('c.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['type'] = $this->type;
            $res['extrafields'] = $this->getExtraFields();
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                if ($this->type == ContentType::Article)
                {
                    $res['publish_date-' . $idlang] = $this->lang->dateTimeToLangDate(\EOS\System\Util\DateTimeHelper::date());
                    $res['options-' . $idlang . '-showdate'] = true;
                }
                $res['options-' . $idlang . '-showtitle'] = true;
            }
        } else
        {
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'] == 1;
            $res['type'] = $rc['type'];
            $res['parent'] = $rc['parent'];
            $res['extra-status'] = $rc['extra_status'] == 1;
            if($rc['extra_status'] == 1) {
                $extra = [];
                $v = json_decode($rc['extra'], true);
                $res['extra-count'] = $extra['count'] = ArrayHelper::getInt($v, 'count');
                $res['extra-text'] = $extra['text'] = ArrayHelper::getInt($v, 'text');
                $res['extra-image'] = $extra['image'] = ArrayHelper::getInt($v, 'image');
                $res['extra-video'] = $extra['video'] = ArrayHelper::getInt($v, 'video');
                $res['extra-link'] = $extra['link'] = ArrayHelper::getInt($v, 'link');
                $res['extra-html'] = $extra['html'] = ArrayHelper::getInt($v, 'html');
                
                $res['extrafields'] = $extra;
            } else {
                $res['extrafields'] = $this->getExtraFields();
            }
            $router = new \EOS\System\Routing\Manager($this->container);
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__content_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_content', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if (!empty($rcl))
                {
                    $res['title-' . $idlang] = $rcl['title'];
                    $res['subtitle-' . $idlang] = $rcl['subtitle'];
                    $res['content-' . $idlang] = $rcl['content'];
                    $res['url-' . $idlang] = $router->getPath($this->getDBRoute($rcl['id_content']), $l['iso_code']);
                    if (($this->type === ContentType::Category) && (!empty($res['url-' . $idlang])))
                    {
                        $pathArray = PathHelper::explodeSlash($res['url-' . $idlang]);
                        $res['url-' . $idlang] = end($pathArray);
                    }
                    if (!empty($rcl['options']))
                    {
                        $ops = json_decode($rcl['options'], true);
                        $res['options-' . $idlang . '-editormode'] = $ops['editormode'];
                        $res['options-' . $idlang . '-template'] = $ops['template'];
                        $res['options-' . $idlang . '-showtitle'] = $ops['showtitle'];
                        $res['options-' . $idlang . '-showsubtitle'] = $ops['showsubtitle'];
                        $res['options-' . $idlang . '-customcode'] = ArrayHelper::getStr($ops, 'customcode');
                        $res['options-' . $idlang . '-withtheme'] = $ops['withtheme'];
                        $res['options-' . $idlang . '-loadexternal'] = ArrayHelper::getBool($ops, 'loadexternal', false);
                        $res['options-' . $idlang . '-css'] = ArrayHelper::getStr($ops, 'css', '');
                        if (isset($ops['extra']))
                        {
                            $this->getDataExtra($ops['extra'], $idlang, $res);
                        }
                        if ($this->type == ContentType::Article)
                        {
                            $res['options-' . $idlang . '-showdate'] = ArrayHelper::getBool($ops, 'showdate', true);
                        }
                    }

                    if ($this->type == ContentType::Article)
                    {
                        $tblContent = $this->db->tableFix('#__content_article');
                        $query = $this->db->newFluent()->from($tblContent)->select('publish_date, author')
                            ->where('id_content_lang', $rcl['id']);
                        $rcl = $query->fetch();
                        if (!empty($rcl))
                        {
                            $res['publish_date-' . $idlang] = $this->lang->dbDateToLangDate($rcl['publish_date']);
                            $res['author-' . $idlang] = $rcl['author'];
                        }
                    }
                }

                if ($this->useSeo())
                {
                    $seo = new \EOS\System\Seo\Manager($this->container);
                    $seo->getSeoArray($this->getCompName(), $id, $idlang, $res);
                }
            }
            if ($this->type == ContentType::Section)
            {
                $tblContent = $this->db->tableFix('#__content_section');
                $query = $this->db->newFluent()->from($tblContent)->select('name')
                    ->where('id_content', $rc['id']);
                $rcs = $query->fetch();
                if (!empty($rcs))
                {
                    $res['name'] = $rcs['name'];
                }
            }
        }

        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_PARSE, $ev);
        return $ev->getData();
    }

    public function getDataExtra($extra, $id_lang, &$res)
    {
        if (!empty($extra))
        {
            $i = 0;
            foreach ($extra as $item)
            {
                $i++;
                foreach ($item as $field => $values)
                {
                    if (!empty($values))
                    {
                        $n = 0;
                        foreach ($values as $v)
                        {
                            $n++;
                            if ($field == 'html-mode')
                            {
                                $extraName = 'extra-' . $id_lang . '-html-' . $i . '-' . $n . '-mode';
                            } else
                            {
                                $extraName = 'extra-' . $id_lang . '-' . $field . '-' . $i . '-' . $n;
                            }
                            $res[$extraName] = $v;
                        }
                    }
                }
            }
        }
    }

    private function isEmptyData(&$data, $id_lang)
    {
        return (empty($data['title-' . $id_lang])) && (empty($data['subtitle-' . $id_lang])) &&
            (empty($data['content-' . $id_lang])) && (empty($data['url-' . $id_lang]));
    }

    private function useRouter()
    {
        return in_array($this->type, [ContentType::Page, ContentType::Category]);
    }

    private function useSeo()
    {
        return in_array($this->type, [ContentType::Page, ContentType::Category, ContentType::Article]);
    }

    private function prepareData(&$data, &$error)
    {
        if ((!is_numeric($data['type'])) || (intval($data['type']) !== $this->type))
        {
            $error = $this->lang->trans('content.content.error.type');
            return false;
        }
        $res = true;
        $ll = $this->lang->getArrayFromDB(true);
        $isEmpty = true;
        $router = new \EOS\System\Routing\Manager($this->container);
        foreach ($ll as $l)
        {
            $id_lang = $l['id'];
            $errl = ' [' . $l['iso_code'] . ']';
            if ($this->useRouter())
            {
                $data['url-' . $id_lang] = \EOS\System\Util\StringHelper::sanitizeUrl($data['url-' . $id_lang]);
            }
            if (!$this->isEmptyData($data, $id_lang))
            {
                $isEmpty = false;
                if (empty($data['title-' . $id_lang]))
                {
                    $error = $this->lang->trans('content.content.error.title') . $errl;
                    $res = false;
                    break;
                }
                if ($this->useRouter())
                {
                    // Verifica per mettere un predefinito
                    if ((empty($data['url-' . $id_lang])) && (!$router->isValidRoutePath($this->getDBRoute($data['id']), $l['iso_code'], $data['url-' . $id_lang])))
                    {
                        $data['url-' . $id_lang] = \EOS\System\Util\StringHelper::sanitizeUrl($data['title-' . $id_lang]);
                    }
                }

                // Per le categorie devo ricostruire il path dal genitore
                if ($this->type === ContentType::Category)
                {
                    if (!empty($data['parent']))
                    {
                        $pathParent = $router->getPath($this->getDBRoute($data['parent']), $l['iso_code']);
                        if (is_null($pathParent))
                        {
                            throw new \Exception('ContentMode->prepareData parent router not found! - ' . $data['parent']);
                        }
                        $pathArr = PathHelper::explodeSlash($pathParent);
                        $pathArr[] = $data['url-' . $id_lang];
                        $data['url-' . $id_lang] = PathHelper::implodeSlash($pathArr);
                    }
                }

                if ($this->type === ContentType::Article)
                {
                    if (empty($data['parent']))
                    {
                        $error = $this->lang->trans('content.content.error.category') . $errl;
                        $res = false;
                        break;
                    }
                }

                if ($this->useRouter())
                {
                    if (!$router->isValidRoutePath($this->getDBRoute($data['id']), $l['iso_code'], $data['url-' . $id_lang]))
                    {
                        $error = $this->lang->trans('content.content.error.url') . $errl;
                        $res = false;
                        break;
                    }
                }

                if ($this->type === ContentType::Section)
                {
                    $data['name'] = \EOS\System\Util\StringHelper::sanitizeUrl($data['name']);
                    if (empty($data['name']))
                    {
                        $error = $this->lang->trans('content.content.error.name') . $errl;
                        $res = false;
                        break;
                    }
                }
            }
        }
        if ($isEmpty)
        {
            $error = $this->lang->trans('content.content.error.empty');
            $res = false;
        }

        if ($res)
        {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }

        return $res;
    }

    public function saveData(&$data, &$error)
    {
        $parent = 0;
        if (in_array($this->type, [ContentType::Category, ContentType::Article]))
        {
            if (!empty($data['parent']))
            {
                if (is_array($data['parent']))
                {
                    $data['parent'] = array_diff($data['parent'], [0]);
                    if (!empty($data['parent']))
                    {
                        $parent = (int) array_values($data['parent'])[0];
                    }
                } else
                {
                    $parent = (int) $data['parent'];
                }
            }
        }

        $data['parent'] = $parent;
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $evBefore = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_BEFORE_SAVE, $evBefore);
            $this->db->beginTransaction();
            /* Extra field for page */
            if(isset($data['extra-status'])) {
                $r = [];
                $r['count'] = (int) $data['extra-count'];
                $r['text'] = (int) $data['extra-text'];
                $r['image'] = (int) $data['extra-image'];
                $r['video'] = (int) $data['extra-video'];
                $r['link'] = (int) $data['extra-link'];
                $r['html'] = (int) $data['extra-html'];
                $extra = json_encode($r);
                $extraSetting = $r;
            } else {
                $extra = null; // Da valutare se valorizzare comunque
                $extraSetting = $this->getExtraFields();
            }
            
            $status = ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0;
            $extraStatus = ArrayHelper::getStr($data, 'extra-status') === 'on' ? 1 : 0;
            $values = [
                'type' => $this->type,
                'status' => $status,
                'parent' => $parent,
                'extra_status' => $extraStatus,
                'extra' => $extra,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__content');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $data['id'] = $this->db->lastInsertId();
            }

            $c = 0;
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                if ($this->saveDataLang($l['id'], $l['iso_code'], $data, $extraSetting))
                {
                    $c++;
                }
            }
            if ($c == 0)
            {
                throw new \Exception('Language item not found!');
            }

            if ($this->type === ContentType::Article)
            {
                $this->saveDataCategory($data);
            }
            if ($this->type === ContentType::Section)
            {
                $this->saveDataSection($data);
            }
            $this->db->commit();
            $evAfter = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_AFTER_SAVE, $evAfter);
            return true;
        }
    }

    private function saveDataLang($id_lang, $langiso, &$data, $extraSetting)
    {
        $tblContentLang = $this->db->tableFix('#__content_lang');
        $query = $this->db->newFluent()->from($tblContentLang)->select(null)->select('id')
            ->where('id_content', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        if ($this->isEmptyData($data, $id_lang))
        {
            if (!empty($item))
            {
                $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id', (int) $item['id']);
                $query->execute();
                if ($this->useRouter())
                {
                    $router = new \EOS\System\Routing\Manager($this->container);
                    $router->deleteRoute($this->getDBRoute($data['id']), $langiso);
                }

                if ($this->useSeo())
                {
                    $seo = new \EOS\System\Seo\Manager($this->container);
                    $seo->deleteSeo($this->getCompName(), $data['id'], $id_lang);
                }

                if ($this->type === ContentType::Article)
                {
                    $query = $this->db->newFluent()->deleteFrom($this->db->tableFix('#__content_article'))->where('id_content_lang', (int) $item['id']);
                    $query->execute();
                }
            }
            return false;
        } else
        {
            $dataop = [
                'editormode' => $data['content-' . $id_lang . '-mode'],
                'template' => $data['options-' . $id_lang . '-template'],
                'showtitle' => (isset($data['options-' . $id_lang . '-showtitle']) ? $data['options-' . $id_lang . '-showtitle'] == 'on' : false),
                'showsubtitle' => (isset($data['options-' . $id_lang . '-showsubtitle']) ? $data['options-' . $id_lang . '-showsubtitle'] == 'on' : false),
                'withtheme' => (isset($data['options-' . $id_lang . '-withtheme']) ? $data['options-' . $id_lang . '-withtheme'] == 'on' : false),
            ];
            ArrayHelper::setStrSkipDef($dataop, 'customcode', $data['options-' . $id_lang . '-customcode']);
            ArrayHelper::setStrSkipDef($dataop, 'css', $data['options-' . $id_lang . '-css']);
            ArrayHelper::setBoolSkipDef($dataop, 'loadexternal', (isset($data['options-' . $id_lang . '-loadexternal']) ? $data['options-' . $id_lang . '-loadexternal'] == 'on' : false));
            
            if ($this->type == ContentType::Article)
            {
                $dataop['showdate'] = (isset($data['options-' . $id_lang . '-showdate']) ? $data['options-' . $id_lang . '-showdate'] == 'on' : false);
            }
            
            $extra = $this->saveDataExtra($id_lang, $data, $extraSetting);
            if (!empty($extra))
            {
                $dataop['extra'] = $extra;
            }

            $options = json_encode($dataop);
            $values = [
                'id_content' => $data['id'],
                'id_lang' => $id_lang,
                'title' => $data['title-' . $id_lang],
                'subtitle' => $data['subtitle-' . $id_lang],
                'content' => $data['content-' . $id_lang],
                'options' => $options,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            if (empty($item))
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContentLang, $values);
                $query->execute();
                $item['id'] = $this->db->lastInsertId();
            } else
            {
                $query = $this->db->newFluent()->update($tblContentLang)->set($values)
                    ->where('id', $item['id']);
                $query->execute();
            }


            if ($this->useRouter())
            {
                $router = new \EOS\System\Routing\Manager($this->container);
                $router->userID = $this->user->getUserID();
                $container = 0;
                $menu = 1;
                if ($this->type == ContentType::Category)
                {
                    $container = 1;
                }
                $router->setRoute($this->getDBRoute($data['id']), $langiso, $data['url-' . $id_lang], $container, $menu);
            }

            if ($this->useSeo())
            {
                $seo = new \EOS\System\Seo\Manager($this->container);
                $seo->userID = $this->user->getUserID();
                $seo->setSeoArray($this->getCompName(), $data['id'], $id_lang, $data);
            }
            if ($this->type === ContentType::Article)
            {
                $idct = $item['id'];
                $tblContentLang = $this->db->tableFix('#__content_article');
                $query = $this->db->newFluent()->from($tblContentLang)->select(null)->select('id_content_lang')
                    ->where('id_content_lang', $idct);
                $item = $query->fetch();
                $values = [
                    'id_content_lang' => $idct,
                    'author' => $this->user->getUserID(),
                    'publish_date' => $this->lang->langDateToDbDate($data['publish_date-' . $id_lang]),
                    'up_id' => $this->user->getUserID(),
                    'up_date' => $this->db->util->nowToDBDateTime()
                ];
                if (empty($item))
                {
                    $values['ins_id'] = $this->user->getUserID();
                    $values['ins_date'] = $this->db->util->nowToDBDateTime();
                    $query = $this->db->newFluent()->insertInto($tblContentLang, $values);
                } else
                {
                    $query = $this->db->newFluent()->update($tblContentLang)->set($values)
                        ->where('id_content_lang', $idct);
                }
                $query->execute();
            }

            return true;
        }
    }
    
    private function saveDataExtra($id_lang, $data, $extraSetting)
    {
        $extraItems = [];
        $extraCount = $extraSetting['count'];
        if ($extraCount > 0)
        {
            unset($extraSetting['count']);
            for ($i = 1; $i <= $extraCount; $i++)
            {
                $extraItem = [];
                foreach ($extraSetting as $extraField => $extraFieldCount)
                {
                    $fieldValues = [];
                    $fieldEditor = [];
                    $isExtraEmpty = true;
                    for ($n = 1; $n <= $extraFieldCount; $n++)
                    {
                        $extraName = 'extra-' . $id_lang . '-' . $extraField . '-' . $i . '-' . $n;
                        $value = ArrayHelper::getStr($data, $extraName);
                        $fieldValues[] = $value;
                        if ($extraField == 'html')
                        {
                            if (empty($value))
                            {
                                $fieldEditor[] = '';
                            } else
                            {
                                $fieldEditor[] = ArrayHelper::getStr($data, $extraName . '-mode');
                            }
                        }
                        if (!empty($value))
                        {
                            $isExtraEmpty = false;
                        }
                    }
                    if (!$isExtraEmpty)
                    {
                        if (count($fieldValues) > 0)
                        {
                            $extraItem[$extraField] = $fieldValues;
                        }
                        if (count($fieldEditor) > 0)
                        {
                            $extraItem[$extraField . '-mode'] = $fieldEditor;
                        }
                    }
                }

                $extraItems[] = $extraItem;
            }
            // Per ridurre spazio cancello a fino al primo elemento non vuoto
            for ($i = $extraCount - 1; $i >= 0; $i--)
            {
                if (empty($extraItems[$i]))
                {
                    unset($extraItems[$i]);
                } else
                {
                    break;
                }
            }
        }
        return $extraItems;
    }

    private function saveDataCategory(&$data)
    {
        $tblContentCat = $this->db->tableFix('#__content_category');
        $catList = [];
        $catList[] = $data['parent'];
        foreach ($catList as $cat)
        {
            $query = $this->db->newFluent()->from($tblContentCat)
                ->select(null)->select('id_content, id_category')
                ->where('id_content', $data['id'])
                ->where('id_category', $cat);
            $item = $query->fetch();
            $values = [
                'id_content' => $data['id'],
                'id_category' => $cat,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            if (empty($item))
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContentCat, $values);
            } else
            {
                $query = $this->db->newFluent()->update($tblContentCat)->set($values)
                    ->where('id_content', $data['id'])
                    ->where('id_category', $cat);
            }
            $query->execute();
        }
        $query = $this->db->newFluent()->deleteFrom($tblContentCat)
            ->where('id_content', $data['id'])
            ->where('id_category not in (' . implode(',', $catList) . ')');
        $query->execute();
    }

    private function saveDataSection(&$data)
    {
        $tbl = $this->db->tableFix('#__content_section');

        $query = $this->db->newFluent()->from($tbl)
            ->select(null)->select('id_content, name')
            ->where('id_content', $data['id']);
        $item = $query->fetch();
        $values = [
            'id_content' => $data['id'],
            'name' => $data['name'],
            'up_id' => $this->user->getUserID(),
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        if (empty($item))
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $query = $this->db->newFluent()->insertInto($tbl, $values);
        } else
        {
            $query = $this->db->newFluent()->update($tbl)->set($values)
                ->where('id_content', $data['id']);
        }
        $query->execute();
    }

    public function deleteData($data, &$error)
    {
        if ((!is_numeric($data['type'])) || (intval($data['type']) !== $this->type))
        {
            $error = $this->lang->trans('content.content.error.type');
            return false;
        }
        $ev = new \EOS\System\Event\DataEvent($this, $data);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Content\Classes\Events::CONTENT_EDIT_DATA_BEFORE_DELETE, $ev);
        if (!$ev->isValid())
        {
            $error = $ev->getErrorsString();
            return false;
        }

        $this->db->beginTransaction();
        if ($this->type === ContentType::Article)
        {
            $query = $this->db->newFluent()->deleteFrom($this->db->tableFix('#__content_article'))
                ->where('id_content_lang in (select id from ' .
                $this->db->tableFix('#__content_lang') .
                ' where id_content = ?)', (int) $data['id']);
            $query->execute();

            $query = $this->db->newFluent()->deleteFrom($this->db->tableFix('#__content_category'))
                ->where('id_content = ?', (int) $data['id']);
            $query->execute();
        }

        if ($this->type === ContentType::Section)
        {
            $query = $this->db->newFluent()->deleteFrom($this->db->tableFix('#__content_section'))
                ->where('id_content = ?', (int) $data['id']);
            $query->execute();
        }

        $tblContentLang = $this->db->tableFix('#__content_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_content', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__content');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        if ($this->useRouter())
        {
            $ll = $this->lang->getArrayFromDB(true);
            $router = new \EOS\System\Routing\Manager($this->container);
            foreach ($ll as $l)
            {
                $router->deleteRoute($this->getDBRoute($data['id']), $l['iso_code']);
            }
        }

        if ($this->useSeo())
        {
            $seo = new \EOS\System\Seo\Manager($this->container);
            foreach ($ll as $l)
            {
                $seo->deleteSeo($this->getCompName(), $data['id'], $l['id']);
            }
        }
        $this->db->commit();
        return true;
    }

}
