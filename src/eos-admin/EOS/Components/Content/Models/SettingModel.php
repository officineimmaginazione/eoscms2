<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Models;

use EOS\System\Util\ArrayHelper;

class SettingModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getData()
    {
        $data = [];
        $seo = new \EOS\System\Seo\Manager($this->container);
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $idlang = $l['id'];
            $seo->getSeoArray('default', 0, $idlang, $data);
        }
        $l = ['page', 'article'];
        foreach ($l as $type)
        {
            $v = json_decode($this->db->setting->getStr('content', $type . '.extra'), true);
            $data[$type . '-extra-count'] = ArrayHelper::getInt($v, 'count');
            $data[$type . '-extra-text'] = ArrayHelper::getInt($v, 'text');
            $data[$type . '-extra-image'] = ArrayHelper::getInt($v, 'image');
            $data[$type . '-extra-video'] = ArrayHelper::getInt($v, 'video');
            $data[$type . '-extra-link'] = ArrayHelper::getInt($v, 'link');
            $data[$type . '-extra-html'] = ArrayHelper::getInt($v, 'html');
        }
        $data['category-page-count'] = $this->db->setting->getInt('content', 'category.page.count', 10);
        $data['category-page-preview-length'] = $this->db->setting->getInt('content', 'category.page.preview.length', 50);
        $data['category-widget-count'] = $this->db->setting->getInt('content', 'category.widget.count', 3);
        $data['category-widget-preview-length'] = $this->db->setting->getInt('content', 'category.widget.preview.length', 50);
        return $data;
    }

    public function saveData(&$data, &$err)
    {
        $seo = new \EOS\System\Seo\Manager($this->container);
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $idlang = $l['id'];
            $seo->setSeoArray('default', 0, $idlang, $data);
        }

        $l = ['page', 'article'];
        foreach ($l as $type)
        {
            $r = [];
            $r['count'] = (int) $data[$type . '-extra-count'];
            $r['text'] = (int) $data[$type . '-extra-text'];
            $r['image'] = (int) $data[$type . '-extra-image'];
            $r['video'] = (int) $data[$type . '-extra-video'];
            $r['link'] = (int) $data[$type . '-extra-link'];
            $r['html'] = (int) $data[$type . '-extra-html'];
            foreach ($r as $k => $v)
            {
                if ($v > 99)
                {
                    $r[$k] = 99;
                } else
                {
                    if ($v < 0)
                    {
                        $r[$k] = 0;
                    }
                }
            }
            $v = json_encode($r);
            $this->db->setting->setStr('content', $type . '.extra', $v);
        }

        $this->db->setting->setInt('content', 'category.page.count', $data['category-page-count']);
        $this->db->setting->setInt('content', 'category.page.preview.length', $data['category-page-preview-length']);
        $this->db->setting->setInt('content', 'category.widget.count', $data['category-widget-count'] );
        $this->db->setting->setInt('content', 'category.widget.preview.length', $data['category-widget-preview-length']);
        
        return true;
    }

}
