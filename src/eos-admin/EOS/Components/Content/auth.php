<?php

$this->add('content.page', 'content.dashboard.menu,content.dashboard.page');
$this->add('content.article', 'content.dashboard.menu,content.dashboard.article');
$this->add('content.category', 'content.dashboard.menu,content.dashboard.category');
$this->add('content.section', 'content.dashboard.menu,content.dashboard.section');
$this->add('content.setting', 'content.dashboard.menu,content.dashboard.setting');