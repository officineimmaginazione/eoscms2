<?php

return
    [
        'content.dashboard.menu' => 'Contenuti',
        'content.dashboard.page' => 'Pagine',
        'content.dashboard.article' => 'Articoli',
        'content.dashboard.category' => 'Categorie',
        'content.dashboard.section' => 'Sezioni',
        'content.dashboard.setting' => 'Impostazioni',
];
