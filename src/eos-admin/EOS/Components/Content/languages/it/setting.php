<?php

return
    [
        'content.setting.index.title' => 'Impostazioni contenuti',
        'content.setting.extra.warning' => 'Cambiando queste impostazioni puoi perdere i contenuti extra!',
        'content.setting.extra.page' => 'Pagine sezioni extra',
        'content.setting.extra.article' => 'Articoli sezioni extra',
        'content.setting.extra.text' => 'Liberi',
        'content.setting.extra.image' => 'Immagine',
        'content.setting.extra.video' => 'Video',
        'content.setting.extra.link' => 'Link',
        'content.setting.extra.html' => 'HTML',
        'content.setting.category.page.count' => 'Categorie articoli',
        'content.setting.category.page.preview.length' => 'Anteprima caratteri',
        'content.setting.category.widget.count' => 'Widget articoli',
        'content.setting.category.widget.preview.length' => 'Widget caratteri'
];
