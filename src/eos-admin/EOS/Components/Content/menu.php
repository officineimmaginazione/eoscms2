<?php

if ($this->componentIsActive('content'))
{
    $this->addParent('content', $this->view->trans('content.dashboard.menu'), '', 'fa fa-edit', 10);
    $this->addItem('content', 'page', $this->view->trans('content.dashboard.page'), $this->view->path->urlFor('content', ['page', 'index']), 'fa fa-circle-o', null, 'content.page');
    $this->addItem('content', 'article', $this->view->trans('content.dashboard.article'), $this->view->path->urlFor('content', ['article', 'index']), 'fa fa-circle-o', null, 'content.article');
    $this->addItem('content', 'article', $this->view->trans('content.dashboard.category'), $this->view->path->urlFor('content', ['category', 'index']), 'fa fa-circle-o', null, 'content.category');
    $this->addItem('content', 'section', $this->view->trans('content.dashboard.section'), $this->view->path->urlFor('content', ['section', 'index']), 'fa fa-circle-o', null, 'content.section');
    $this->addItem('content', 'section', $this->view->trans('content.dashboard.setting'), $this->view->path->urlFor('content', ['setting', 'index']), 'fa fa-circle-o', null, 'content.setting');
}