<?php

use \EOS\Components\System\Classes\AuthRole;

$this->mapComponent(['GET'], 'Content', ['page/index' => 'PageController:index'], ['auth' => ['content.page' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Content', ['page/edit/{id}' => 'PageController:edit'], ['auth' => ['content.page' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['page/ajaxlist' => 'PageController:ajaxlist'], ['auth' => ['content.page' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Content', ['page/ajaxsave' => 'PageController:ajaxSave'], ['auth' => ['content.page' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['page/ajaxdelete' => 'PageController:ajaxDelete'], ['auth' => ['content.page' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Content', ['article/index' => 'ArticleController:index'], ['auth' => ['content.article' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Content', ['article/edit/{id}' => 'ArticleController:edit'], ['auth' => ['content.article' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['article/ajaxlist' => 'ArticleController:ajaxlist'], ['auth' => ['content.article' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Content', ['article/ajaxsave' => 'ArticleController:ajaxSave'], ['auth' => ['content.article' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['article/ajaxdelete' => 'ArticleController:ajaxDelete'], ['auth' => ['content.article' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Content', ['category/index' => 'CategoryController:index'], ['auth' => ['content.category' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Content', ['category/edit/{id}' => 'CategoryController:edit'], ['auth' => ['content.category' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['category/ajaxlist' => 'CategoryController:ajaxlist'], ['auth' => ['content.category' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Content', ['category/ajaxsave' => 'CategoryController:ajaxSave'], ['auth' => ['content.category' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['category/ajaxdelete' => 'CategoryController:ajaxDelete'], ['auth' => ['content.category' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Content', ['section/index' => 'SectionController:index'], ['auth' => ['content.section' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Content', ['section/edit/{id}' => 'SectionController:edit'], ['auth' => ['content.section' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['section/ajaxlist' => 'SectionController:ajaxlist'], ['auth' => ['content.section' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Content', ['section/ajaxsave' => 'SectionController:ajaxSave'], ['auth' => ['content.section' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Content', ['section/ajaxdelete' => 'SectionController:ajaxDelete'], ['auth' => ['content.section' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Content', ['setting/index' => 'SettingController:index'], ['auth' => ['content.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Content', ['setting/ajaxsavedata' => 'SettingController:ajaxSaveData'], ['auth' => ['content.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);