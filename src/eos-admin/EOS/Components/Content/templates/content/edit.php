<?php
$useSeo = in_array($this->type, [EOS\Components\Content\Classes\ContentType::Article,
		EOS\Components\Content\Classes\ContentType::Page,
		EOS\Components\Content\Classes\ContentType::Category]);
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();


$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
if (!empty($this->data['extrafields']['count'] > 0)) {
	$tab->addItem('tab-extra', $this->trans('system.common.extra'));
}
if ($useSeo) {
	$tab->addItem('tab-seo', $this->trans('system.common.seo'));
}
$tab->addItem('tab-adv', $this->trans('system.common.advanced'));
$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Html\Input())->name('type')->id('data-type')->type('hidden')->bind($this->data, 'type')->printRender();

/**
	*
	*	BARRA DELLE OPZIONI - INIZIO 
	*
	**/
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function () {
	
	(new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
	if ((isset($this->data['id'])) && ($this->data['id'] != 0)) {
		if ($this->countChild == 0) {
			(new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos mr-1')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
		}
	}
	
	(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-auto flex-column')->addContent(function () {
		(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->attr("class", "mb-2 text-uppercase font-weight-bold")->printRender();
		(new \EOS\UI\Bootstrap\Toggle('status'))
				->bind($this->data, 'status')
				->printRender($this);
	}, $this)->printRender();
	
	(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
	
	if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
	
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
/**
	*
	* BARRA DELLE OPZIONI - FINE
	*
	**/


/**
 * 
 * NOME (SOLO PER SECTION) - INIZIO
 * 
 */
if (in_array($this->type, [EOS\Components\Content\Classes\ContentType::Section])) {
	(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
		echo '<div class="form-group-title">';
		(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.name'))->printRender();
		echo '</div>';
		echo '<div>';
		(new \EOS\UI\Bootstrap\Input('name'))
				->bind($this->data, 'name')
				->printRender($this);
		echo '</div>';
	}, $this)->printRender();
}
/**
 * 
 * NOME (SOLO PER SECTION) - FINE
 * 
 */


/**
 * 
 * TITOLO - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	foreach ($this->dataLangList as $l) {
		(new \EOS\UI\Bootstrap\Checkbox('options-' . $l['id'] . '-showtitle'))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'options-' . $l['id'] . '-showtitle')
				->printRender($this);
	}
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.title'))->printRender();
	echo '</div>';
	echo '<div>';
	foreach ($this->dataLangList as $l) {
		(new \EOS\UI\Bootstrap\Input('title-' . $l['id']))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'title-' . $l['id'])->printRender($this);
	}
	echo '</div>';
	?>
	<?php
}, $this)->printRender();
/**
 * 
 * TITOLO - FINE
 * 
 */


/**
 * 
 * SOTTOTITOLO - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	foreach ($this->dataLangList as $l) {
		(new \EOS\UI\Bootstrap\Checkbox('options-' . $l['id'] . '-showsubtitle'))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'options-' . $l['id'] . '-showsubtitle')
				->printRender($this);
	}
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.subtitle'))->printRender();
	echo '</div>';
	echo '<div>';
	foreach ($this->dataLangList as $l) {
		(new \EOS\UI\Bootstrap\Input('subtitle-' . $l['id']))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'subtitle-' . $l['id'])
				->printRender($this);
	}
	echo '</div>';
}, $this)->printRender();
/**
 * 
 * SOTTOTITOLO - FINE
 * 
 */


(new \EOS\UI\Bootstrap\Row())->addContent(function () {
	/**
	 * 
	 * DATA - INIZIO
	 * 
	 */
	if (in_array($this->type, [EOS\Components\Content\Classes\ContentType::Article])) {
		echo '<div class="col-12 col-md-6">';
		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			foreach ($this->dataLangList as $l) {
				(new \EOS\UI\Bootstrap\Checkbox('options-' . $l['id'] . '-showdate'))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'options-' . $l['id'] . '-showdate')
						->printRender($this);
			}
			(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.publish_date'))->printRender();
			echo '</div>';
			echo '<div>';
			foreach ($this->dataLangList as $l) {
				(new \EOS\UI\Bootstrap\DatePicker('publish_date-' . $l['id']))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'publish_date-' . $l['id'])->printRender($this);
			}
			echo '</div>';
			?>
			<?php
		}, $this)->printRender();
		echo '</div>';
	}
	/**
	 * 
	 * DATA - FINE
	 * 
	 */
	
	
	echo '<div class="col">';
	
	/**
	 * 
	 * CATEGORIA - INIZIO
	 * 
	 */
	if (in_array($this->type, [EOS\Components\Content\Classes\ContentType::Category, EOS\Components\Content\Classes\ContentType::Page])) {
		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.url'))->printRender();
			echo '</div>';
			echo '<div>';
			foreach ($this->dataLangList as $l) {
				$inpUrl = (new \EOS\UI\Bootstrap\Input('url-' . $l['id']))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'url-' . $l['id']);
				if ($this->countChild > 0) {
					$inpUrl->attr('readonly', '');
				}

				$inpUrl->printRender($this);
			}
			echo '</div>';
		}, $this)->printRender();
	}

	if (in_array($this->type, [EOS\Components\Content\Classes\ContentType::Category, EOS\Components\Content\Classes\ContentType::Article])) {
		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.category'))->printRender();
			echo '</div>';
			echo '<div>';
			foreach ($this->dataLangList as $l) {
				echo '<div class="translatable-field lang-' . $l['id'] . '">';
				(new \EOS\UI\Bootstrap\Select('parent'))
						->attr('class', 'parent-category')
						->bind($this->data, 'parent')
						->bindList($this->category[$l['id']], 'id', 'title')
						->change('function() {$(".parent-category").val($(this).val());}')
						->printRender($this);
				echo "</div>";
			}
			echo "</div>";
		}, $this)->printRender();
	}

	/**
	 * 
	 * CATEGORIA - FINE
	 */
	
	echo '</div>';
}, $this)->printRender();


/**
 * 
 * EDITOR - INIZIO
 * 
 */

(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.content'))->printRender();
	echo '</div>';
	echo '<div>';
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		(new \EOS\UI\Bootstrap\CKEditor('content-' . $l['id']))
				->bind($this->data, 'content-' . $l['id'])
				->bindSourceMode($this->data, 'options-' . $l['id'] . '-editormode')
				->fileBrowserUrl($this->path->getUrlFor('system', 'filemanager/index'))
				->printRender($this);
		echo "</div>";
	}
	echo '</div>';
}, $this)->printRender();
/**
 * 
 * EDITOR - FINE
 * 
 */
$tab->endTab('tab-data');


/**
 * 
 * CAMPI EXTRA - INIZIO
 * 
 */

if (($this->data['extrafields']['count'] > 0)) {
	$tab->startTab();
	$countExtra = $this->data['extrafields']['count'];
	for ($i = 1; $i <= $countExtra; $i++) {
		echo '<div class="card custom-accordion"><div class="card-header" id="heading' . $i . '">'
		. '<div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse' . $i . '" aria-expanded="false" aria-controls="collapse' . $i . '" role="button">'
		. '<span>Extra ' . $i . '</span><i class="fas fa-angle-down accordion-arrow"></i>'
		. '</div>' . "\n"
		. '</div>';

		echo '<div id="collapse' . $i . '" class="collapse" aria-labelledby="heading' . $i . '" data-parent="#tab-extra"><div class="card-body">' . "\n";
		$listExtraF = array_keys($this->data['extrafields']);
		unset($listExtraF[0]);
		if ($countExtra > 1) {
			$cbx = new \EOS\UI\Bootstrap\Box();
			$cbx->startContent();
		}
		foreach ($listExtraF as $extraF) {
			$countF = $this->data['extrafields'][$extraF];
			for ($n = 1; $n <= $countF; $n++) {
				(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () use ($extraF, $i, $n) {
					$defName = 'content.content.field.extra.' . $extraF;
					$typeName = 'content.content.' . $this->typeStr . '.field.extra.' . $extraF;
					$extraLabel = 'not found';
					if (!$this->lang->tryTransList([$typeName . '.' . $i . '.' . $n,
									$typeName . '.' . $n,
									$typeName], $extraLabel)) {
						$extraLabel = $this->trans($defName) . ' ' . $n;
					}

					(new \EOS\UI\Html\Label)->attr("class", "form-group-title")->content($this->encodeHtml($extraLabel))->printRender();
					foreach ($this->dataLangList as $l) {
						$extraName = 'extra-' . $l['id'] . '-' . $extraF . '-' . $i . '-' . $n;
						switch ($extraF) {
							case 'text':
								(new \EOS\UI\Bootstrap\Input($extraName))
										->attr('class', 'translatable-field lang-' . $l['id'])
										->bind($this->data, $extraName)
										->id($extraName)
										->printRender($this);
								break;
							case 'html':
								echo '<div class="translatable-field lang-' . $l['id'] . '">';
								(new \EOS\UI\Bootstrap\CKEditor($extraName))
										->bind($this->data, $extraName)
										->bindSourceMode($this->data, $extraName . '-mode')
										->fileBrowserUrl($this->path->getUrlFor('system', 'filemanager/index'))
										->printRender($this);
								echo "</div>";
								break;
							default:
								echo '<div class="input-group translatable-field lang-' . $l['id'] . '" >';
								(new \EOS\UI\Bootstrap\Input($extraName))
										->attr('class', 'translatable-field lang-' . $l['id'])
										->bind($this->data, $extraName)
										->id($extraName)
										->printRender($this);
								echo '<span class="input-group-append">';
								(new EOS\UI\Bootstrap\Button())
										->attr('class', 'btn-default')
										->content('<i class="fa fa-link"></i>')
										->click('function (ev) {ev.preventDefault(); var w = window.open("' .
												$this->path->getUrlFor('system', 'filemanager/index') .
												'?field_name=' . $extraName . '"); w.parent = window;}')
										->printRender($this);
								echo '</span>';
								echo '</div>';
								break;
						}
					}
				}, $this)->printRender();
			}
		}
		if ($countExtra > 1) {
			$cbx->endContent();
			$cbx->printRender();
		}
		echo '</div></div></div>' . "\n";
	}
	$tab->endTab('tab-extra');
}
/**
 * 
 * CAMPI EXTRA - FINE
 * 
 */

/**
 * 
 * OPZIONI SEO - INIZIO
 * 
 */
if ($useSeo) {
	$tab->startTab();
	$seoW = new \EOS\Components\System\UI\SeoEditorWidget($this);
	$seoW->data = $this->data;
	$seoW->dataLangDefault = $this->dataLangDefault;
	$seoW->dataLangList = $this->dataLangList;
	$seoW->write();
	$tab->endTab('tab-seo');
}
$tab->startTab();
/**
 * 
 * OPZIONI SEO - FINE
 * 
 */


/**
 * 
 * SELEZIONE DEL TEMA - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		if (!isset($this->data['options-' . $l['id'] . '-withtheme'])) {
			$this->data['options-' . $l['id'] . '-withtheme'] = true;
		}
		(new \EOS\UI\Bootstrap\Checkbox('options-' . $l['id'] . '-withtheme'))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'options-' . $l['id'] . '-withtheme')->printRender($this);
		echo '</div>';
	}
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.template'))->printRender();
	echo '</div>';
	echo '<div>';
	$tpl = $this->getTemplateFileList();
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		(new \EOS\UI\Bootstrap\Select('options-' . $l['id'] . '-template'))
				->bind($this->data, 'options-' . $l['id'] . '-template')
				->bindList($tpl, 'name', 'name')
				->printRender($this);
		echo "</div>";
	}
	echo '</div>';
}, $this)->printRender();
/**
 * 
 * SELEZIONE DEL TEMA - FINE
 * 
 */


/**
 * 
 * SELEZIONE CODICE PERSONALIZZATO - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.customcode'))->printRender();
	echo '</div>';
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		(new \EOS\UI\Bootstrap\Select('options-' . $l['id'] . '-customcode'))
				->bind($this->data, 'options-' . $l['id'] . '-customcode')
				->bindList($this->customCodeList, 'name', 'name')
				->printRender($this);
		echo "</div>";
	}
}, $this)->printRender();
/**
 * 
 * SELEZIONE CODICE PERSONALIZZATO - FINE
 * 
 */


/**
 * 
 * CSS - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)->content($this->transE('content.content.field.css'))->printRender();
	echo '</div>';
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		(new \EOS\UI\Bootstrap\Input('options-' . $l['id'] . '-css'))
				->attr('class', 'translatable-field lang-' . $l['id'])
				->bind($this->data, 'options-' . $l['id'] . '-css')
				->printRender($this);
		echo '</div>';
	}
}, $this)->printRender();
/**
 * 
 * CSS - FINE
 * 
 */


/**
 * 
 * ABILITA SEZIONI ESTERNE - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	foreach ($this->dataLangList as $l) {
		echo '<div class="translatable-field lang-' . $l['id'] . '">';
		(new \EOS\UI\Bootstrap\Checkbox('options-' . $l['id'] . '-loadexternal'))
				->bind($this->data, 'options-' . $l['id'] . '-loadexternal')
				->printRender($this);
		echo '</div>';
	}
	(new \EOS\UI\Html\Label)->content($this->trans('content.content.field.loadexternal'))->printRender();
	echo '</div>';
}, $this)->printRender();
/**
 * 
 * ABILITA SEZIONI ESTERNE - FINE
 * 
 */


/**
 * 
 * ABILITA SCAMPI EXTRA - INIZIO
 * 
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
	echo '<div class="form-group-title">';
	(new \EOS\UI\Bootstrap\Checkbox('extra-status'))->bind($this->data, 'extra-status')->printRender($this);
	(new \EOS\UI\Html\Label)->content($this->trans('content.content.field.extrastatus'))->printRender();
	echo '</div>';
}, $this)->printRender();
/**
 * 
 * ABILITA SCAMPI EXTRA - FINE
 * 
 */

$type = $this->typeStr;
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () use ($type) {
	echo '<div class="form-row mb-2">';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.' . $type))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-count'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-count', 0))
			->printRender($this);
	echo '</div>';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.text'))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-text'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-text', 0))
			->printRender($this);
	echo '</div>';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.image'))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-image'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-image', 0))
			->printRender($this);
	echo '</div>';
	echo '</div>';
	echo '<div class="form-row">';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.video'))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-video'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-video', 0))
			->printRender($this);
	echo '</div>';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.link'))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-link'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-link', 0))
			->printRender($this);
	echo '</div>';
	echo '<div class="col-12 col-sm-4">';
	echo '<div class="form-group-title">';
	(new \EOS\UI\Html\Label)
			->content($this->transE('content.setting.extra.html'))
			->printRender();
	echo '</div>';
	(new \EOS\UI\Bootstrap\Input('extra-html'))
			->type('number')
			->value(EOS\System\Util\ArrayHelper::getInt($this->data, 'extra-html', 0))
			->printRender($this);
	echo '</div>';
	echo '</div>';
}, $this)->printRender();

$tab->endTab('tab-adv');
$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);

$this->startCaptureScript();
?>
<script>
	
	var anchorTrigger = $('.admin-main-header').height() + $('.box-header').height() + (16*4);

	$('.admin-main').scroll(function() {
		var winTop = $('.admin-main').scrollTop();
		if(winTop >= anchorTrigger) {
			$('.fixed').css('left', $('.sidebar').width() + 32 + "px");
			$('.box-header').addClass('fixed');
		}
		else {
			$('.box-header').removeClass('fixed');
		}
	});
	
	
	
	function saveData()
	{
		for (instance in CKEDITOR.instances)
		{
			CKEDITOR.instances[instance].updateElement();
		}
		var data = $('#edit').serializeFormJSON();
		$.ajax({
			type: 'POST',
			dataType: "json",
			url: '<?php echo $this->path->getUrlFor('Content', $this->typeStr . '/ajaxsave'); ?>',
			data: JSON.stringify(data),
			contentType: "application/json",
		})
						.done(function (json)
						{
							if (json.result == true)
							{
								location.href = json.redirect;
							} else
							{
								bootbox.alert(json.message);
							}
						})
						.fail(function (jqxhr, textStatus, error)
						{
							var err = textStatus + ", " + error;
							alert(err);
						});
		return false;
	}

	function cancelData()
	{
		window.location.href = "<?php echo $this->path->getUrlFor('Content', $this->typeStr . '/index'); ?>";
	}

	function deleteData()
	{
		bootbox.confirm("<?php $this->transPE('content.content.' . $this->typeStr . '.delete.confirm') ?>", function (result)
		{
			if (result)
			{
				$.ajax({
					type: 'POST',
					dataType: "json",
					url: '<?php echo $this->path->getUrlFor('Content', $this->typeStr . '/ajaxdelete'); ?>',
					data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val(), "type": $('#data-type').val()}),
					contentType: "application/json",
				})
								.done(function (json)
								{
									if (json.result == true)
									{
										location.href = json.redirect;
									} else
									{
										bootbox.alert(json.message);
									}
								})
								.fail(function (jqxhr, textStatus, error)
								{
									var err = textStatus + ", " + error;
									alert(err);
								});
			}
		});
	}
</script>
<?php
$this->endCaptureScript();
$this->getEventDispatcher()->dispatch(EOS\Components\Content\Classes\Events::CONTENT_EDIT_TEMPLATE_RENDER, new \EOS\System\Event\EchoEvent($this));
