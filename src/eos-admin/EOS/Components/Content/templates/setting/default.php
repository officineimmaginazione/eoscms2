<?php
$formData = new EOS\UI\Html\Form();
$formData->id('data-form');
$formData->attr('method', 'POST');
$formData->attr('class', 'form-horizontal');
$formData->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
$tab = new EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-seo', $this->trans('system.common.seo'));
$tab->addItem('tab-extra', $this->trans('system.common.extra'));
$tab->startTab();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function () 
{
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('content.setting.category.page.count'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('category-page-count'))
        ->type('number')
        ->bind($this->data, 'category-page-count')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('content.setting.category.page.preview.length'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('category-page-preview-length'))
        ->type('number')
        ->bind($this->data, 'category-page-preview-length')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('content.setting.category.widget.count'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('category-widget-count'))
        ->type('number')
        ->bind($this->data, 'category-widget-count')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-sm-2">';
    (new \EOS\UI\Html\Label)
        ->content($this->transE('content.setting.category.widget.preview.length'))
        ->printRender();
    (new \EOS\UI\Bootstrap\Input('category-widget-preview-length'))
        ->type('number')
        ->bind($this->data, 'category-widget-preview-length')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
$tab->endTab('tab-data');
$tab->startTab();
$seoW = new \EOS\Components\System\UI\SeoEditorWidget($this);
$seoW->data = $this->data;
$seoW->dataLangDefault = $this->dataLangDefault;
$seoW->dataLangList = $this->dataLangList;
$seoW->write();
$tab->endTab('tab-seo');
$tab->startTab();
echo '<div class="callout callout-warning">';
echo '<i class="icon fa fa-warning"></i> ';
(new \EOS\UI\Html\Label)
    ->content($this->transE('content.setting.extra.warning'))
    ->printRender();
echo '</div>';
$l = ['page', 'article'];
foreach ($l as $type)
{
    (new \EOS\UI\Bootstrap\FormGroup())->addContent(function () use ($type)
    {
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.' . $type))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-count'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-count', 0))
            ->printRender($this);
        echo '</div>';
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.text'))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-text'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-text', 0))
            ->printRender($this);
        echo '</div>';
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.image'))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-image'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-image', 0))
            ->printRender($this);
        echo '</div>';
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.video'))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-video'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-video', 0))
            ->printRender($this);
        echo '</div>';
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.link'))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-link'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-link', 0))
            ->printRender($this);
        echo '</div>';
        echo '<div class="col-sm-2">';
        (new \EOS\UI\Html\Label)
            ->content($this->transE('content.setting.extra.html'))
            ->printRender();
        (new \EOS\UI\Bootstrap\Input($type . '-extra-html'))
            ->type('number')
            ->value(EOS\System\Util\ArrayHelper::getInt($this->data, $type . '-extra-html', 0))
            ->printRender($this);
        echo '</div>';
    }, $this)->printRender();
}
$tab->endTab('tab-extra');
$tab->printRender($this);
$box->endContent();
$box->startFooter();
echo '<div class="row"><div class="col-xs-12">';
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
echo '</div></div>';
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$formData->endContent();
$formData->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#data-form').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Content', 'setting/ajaxsavedata'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

</script>
<?php
$this->endCaptureScript();
$this->getEventDispatcher()->dispatch(EOS\Components\Content\Classes\Events::CONTENT_LIST_TEMPLATE_RENDER, new \EOS\System\Event\EchoEvent($this));