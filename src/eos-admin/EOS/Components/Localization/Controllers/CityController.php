<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class CityController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Localization\Models\CityModel($this->container);
        $v->pageTitle = $v->trans('localization.city.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('city/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $ms = new \EOS\Components\Localization\Models\StateModel($this->container);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->state = $ms->getList($v->dataLangDefault['id']);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('localization.city.insert.title') : $v->trans('localization.city.edit.title');
        $v->addBreadcrumb($this->path->urlFor('localization', ['city', 'index']), $v->trans('localization.city.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Localization\Models\CityModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('city/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Localization\Models\CityModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'lc.id');
        $dts->addColumn('name', 'lc.name', true);
        $dts->addColumn('zip_code', 'lc.zip_code', true);
        $dts->addColumn('state_name', 'ls.name', true);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\CityModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'city/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\CityModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'city/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
