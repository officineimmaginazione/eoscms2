<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class LanguageController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Localization\Models\LanguageModel($this->container);
        $v->pageTitle = $v->trans('localization.language.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('language/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('localization.language.insert.title') : $v->trans('localization.language.edit.title');
        $v->addBreadcrumb($this->path->urlFor('localization', ['language', 'index']), $v->trans('localization.language.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Localization\Models\LanguageModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('language/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Localization\Models\LanguageModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'l.id');
        $dts->addColumn('name', 'l.name', true);
        $dts->addColumn('iso_code', 'l.iso_code', true);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\LanguageModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'language/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\LanguageModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'language/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
