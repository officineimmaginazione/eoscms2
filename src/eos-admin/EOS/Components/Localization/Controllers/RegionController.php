<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class RegionController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Localization\Models\RegionModel($this->container);
        $v->pageTitle = $v->trans('localization.region.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('region/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $mc = new \EOS\Components\Localization\Models\CountryModel($this->container);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->country = $mc->getList($v->dataLangDefault['id']);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('localization.region.insert.title') : $v->trans('localization.region.edit.title');
        $v->addBreadcrumb($this->path->urlFor('localization', ['region', 'index']), $v->trans('localization.region.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Localization\Models\RegionModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('region/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Localization\Models\RegionModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'lr.id');
        $dts->addColumn('name', 'lr.name', true);
        $dts->addColumn('isocode', 'lc.iso_code', true);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\RegionModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'region/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\RegionModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'region/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
