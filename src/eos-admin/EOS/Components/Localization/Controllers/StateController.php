<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class StateController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Localization\Models\StateModel($this->container);
        $v->pageTitle = $v->trans('localization.state.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('state/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $mr = new \EOS\Components\Localization\Models\RegionModel($this->container);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->region = $mr->getList($v->dataLangDefault['id']);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('localization.state.insert.title') : $v->trans('localization.state.edit.title');
        $v->addBreadcrumb($this->path->urlFor('localization', ['state', 'index']), $v->trans('localization.state.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Localization\Models\StateModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('state/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Localization\Models\StateModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'ls.id');
        $dts->addColumn('name', 'ls.name', true);
        $dts->addColumn('isocode', 'ls.iso_code', true);
        $dts->addColumn('region', 'lr.name', true);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\StateModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'state/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Localization\Models\StateModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('localization', 'state/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
