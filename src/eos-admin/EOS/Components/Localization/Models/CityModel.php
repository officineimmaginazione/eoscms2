<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class CityModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__localization_city lc'))
            ->select(null)
            ->select('lc.id, lc.name, ls.name, lc.zip_code, lc.latitude, lc.longitude')
            ->innerJoin($this->db->tableFix('#__localization_state ls ON ls.id = lc.id_state'));
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__localization_city lc'))
            ->where('lc.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['id-state'] = $rc['id_state'];
            $res['zip-code'] = $rc['zip_code'];
            $res['latitude'] = $rc['latitude'];
            $res['longitude'] = $rc['latitude'];
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $optionsvalues = [];
            $options = json_encode($optionsvalues);
            $values = [
                'name' => $data['name'],
                'id_state' => $data['id-state'],
                'zip_code' => $data['zip-code'],
                'latitude' => $data['latitude'],
                'latitude' => $data['latitude']
            ];
            $tblContent = $this->db->tableFix('#__localization_city');
            if ($data['id'] == 0)
            {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__localization_city');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
