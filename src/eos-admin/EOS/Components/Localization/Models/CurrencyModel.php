<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosofc.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class TaxModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__currency c'))
            ->select(null)
            ->select('c.id, c.name, c.iso_code, c.conversion_rate, c.status')
            ->where('tl.id_lang = ' . (int) $id_lang);
        return $f;
    }
    
    public function getList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__currency c'))
            ->select(null)->select('c.id, c.name')
            ->where('c.status = 1')
            ->orderBy('c.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__currency c'))
            ->where('c.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['status'] = $rc['status'] == 1;
            $res['reference'] = $rc['reference'];
            $res['status'] = $rc['status'] == 1;
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $optionsvalues = [];
            $options = json_encode($optionsvalues);
            $values = [
                'rate' => $data['rate'],
                'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
                'reference' => $data['reference']
            ];
            $tblContent = $this->db->tableFix('#__currency');
            if ($data['id'] == 0)
            {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }
    
    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__currency');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
