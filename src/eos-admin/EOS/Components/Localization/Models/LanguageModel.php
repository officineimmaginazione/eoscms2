<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class LanguageModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__lang l'))
            ->select(null)
            ->select('l.id, l.name, l.status_admin, l.default_admin, l.default_site, l.status_site, l.iso_code, l.language_code, l.datelite_format`, `datefull_format`');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__lang l'))
            ->where('l.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['status-admin'] = $rc['status_admin'];
            $res['default-admin'] = $rc['default_admin'];
            $res['default-site'] = $rc['default_site'];
            $res['status-site'] = $rc['status_site'];
            $res['iso-code'] = $rc['iso_code'];
            $res['language-code'] = $rc['language_code'];
            $res['datelite-format'] = $rc['datelite_format'];
            $res['datefull-format'] = $rc['datefull_format'];
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'status_admin' => $data['status-admin'],
                'default_admin' => $data['default-admin'],
                'default_site' => $data['default-site'],
                'status_site' => $data['status-site'],
                'iso_code' => $data['iso-code'],
                'language_code' => $data['language-code'],
                'datelite_format' => $data['datelite-format'],
                'datefull_format' => $data['datefull-format']
            ];

            $tblContent = $this->db->tableFix('#__lang');
            if ($data['id'] == 0)
            {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__lang');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
