<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class StateModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__localization_state ls'))
            ->select(null)
            ->select('ls.id, ls.name, lr.name, ls.iso_code')
            ->innerJoin($this->db->tableFix('#__localization_region lr ON lr.id = ls.id_region'));
        return $f;
    }
    
    public function getList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__localization_state lr'))
            ->select(null)->select('lr.id, lr.name')
            ->orderBy('lr.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__localization_state ls'))
            ->where('ls.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['iso-code'] = $rc['iso_code'];
            $res['id-region'] = $rc['id_region'];
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $optionsvalues = [];
            $options = json_encode($optionsvalues);
            $values = [
                'name' => $data['name'],
                'iso_code' => $data['iso-code'],
                'id_region' => $data['id-region']
            ];
            $tblContent = $this->db->tableFix('#__localization_state');
            if ($data['id'] == 0)
            {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__localization_state');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
