<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Localization\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class TaxModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__tax t'))
            ->select(null)
            ->select('t.id, t.rate, t.status, t.reference, tl.name')
            ->innerJoin($this->db->tableFix('#__tax_lang tl ON tl.id_tax = t.id'))
            ->where('tl.id_lang = ' . (int) $id_lang);
        return $f;
    }
    
    public function getList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__tax t'))
            ->select(null)->select('t.id, tl.name')
            ->where('tl.id_lang = ' . (int)$id_lang)
            ->where('t.status = 1')
            ->innerJoin($this->db->tableFix('#__tax_lang tl ON tl.id_tax = t.id'))          
            ->orderBy('tl.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__tax t'))
            ->where('t.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else
        {
            $res['id'] = $rc['id'];
            $res['rate'] = $rc['rate'];
            $res['status'] = $rc['status'] == 1;
            $res['reference'] = $rc['reference'];
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__tax_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_tax', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                }
            }
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $optionsvalues = [];
            $options = json_encode($optionsvalues);
            $values = [
                'rate' => $data['rate'],
                'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
                'reference' => $data['reference']
            ];
            $tblContent = $this->db->tableFix('#__tax');
            if ($data['id'] == 0)
            {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $values['id'] = $this->db->lastInsertId();
            }
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                $this->saveDataLang($l['id'], $l['iso_code'], $data);
            }
            $this->db->commit();
            return true;
        }
    }
    
    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__tax_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_tax', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $dataop = [];
        $values = [
            'id_tax' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang]
        ];
        if ($item == null)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__tax');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
