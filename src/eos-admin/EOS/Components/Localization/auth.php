<?php

$this->add('localization.city', 'localization.dashboard.menu,localization.dashboard.city');
$this->add('localization.state', 'localization.dashboard.menu,localization.dashboard.state');
$this->add('localization.region', 'localization.dashboard.menu,localization.dashboard.region');
$this->add('localization.country', 'localization.dashboard.menu,localization.dashboard.country');
$this->add('localization.language', 'localization.dashboard.menu,localization.dashboard.language');
$this->add('localization.tax', 'localization.dashboard.menu,localization.dashboard.tax');