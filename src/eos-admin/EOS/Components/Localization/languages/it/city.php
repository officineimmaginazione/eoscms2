<?php

return
    [
        'localization.city.index.title' => 'Città',
        'localization.city.edit.title' => 'Modifica città',
        'localization.city.insert.title' => 'Inserisci città',
        'localization.city.field.id' => 'ID',
        'localization.city.field.name' => 'Nome',
        'localization.city.field.status' => 'Attivo',
        'localization.city.field.status.0' => 'No',
        'localization.city.field.status.1' => 'Si',
        'localization.city.field.zipcode' => 'CAP',
        'localization.city.field.state' => 'Provincia',
        'localization.city.field.longitude' => 'Latitudine',
        'localization.city.field.latitude' => 'Longitudine',
];
