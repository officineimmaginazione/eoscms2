<?php

return
    [
        'localization.country.index.title' => 'Nazione',
        'localization.country.edit.title' => 'Modifica nazione',
        'localization.country.insert.title' => 'Inserisci nazione',
        'localization.country.field.id' => 'ID',
        'localization.country.field.name' => 'Nazione',
        'localization.country.field.isocode' => 'Codice ISO',
        'localization.country.field.status' => 'Attivo',
        'localization.country.field.status.0' => 'No',
        'localization.country.field.status.1' => 'Si'
];
