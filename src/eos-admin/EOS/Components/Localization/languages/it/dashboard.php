<?php

return
    [
        'localization.dashboard.menu' => 'Localizzazione',
        'localization.dashboard.city' => 'Città',
        'localization.dashboard.state' => 'Province',
        'localization.dashboard.region' => 'Regioni',
        'localization.dashboard.country' => 'Nazioni',
        'localization.dashboard.language' => 'Lingue',
        'localization.dashboard.tax' => 'Aliquote',
    ];
