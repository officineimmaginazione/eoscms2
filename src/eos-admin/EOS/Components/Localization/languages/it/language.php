<?php

return
    [
        'localization.language.index.title' => 'Lingue',
        'localization.language.edit.title' => 'Modifica lingua',
        'localization.language.insert.title' => 'Inserisci lingua',
        'localization.language.field.id' => 'ID',
        'localization.language.field.name' => 'Lingua',
        'localization.language.field.isocode' => 'Codice ISO',
        'localization.language.field.status' => 'Attivo',
        'localization.language.field.status.0' => 'No',
        'localization.language.field.status.1' => 'Si',
        'localization.language.field.isocode' => 'Codice ISO',
        'localization.language.field.statusadmin' => 'Backend - Stato Lingua',
        'localization.language.field.defaultadmin' => 'Backend - Lingua di default',
        'localization.language.field.statussite' => 'Frontend - Stato Lingua',
        'localization.language.field.defaultsite' => 'Frontend - Lingua di default',
        'localization.language.field.languagecode' => 'Codice lingua',
        'localization.language.field.dateliteformat' => 'Data - Formato corto',
        'localization.language.field.datefullformat' => 'Data - Formato lungo',
];
