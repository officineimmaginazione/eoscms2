<?php

return
    [
        'localization.region.index.title' => 'Regioni',
        'localization.region.edit.title' => 'Modifica regioni',
        'localization.region.insert.title' => 'Inserisci regioni',
        'localization.region.field.id' => 'ID',
        'localization.region.field.country' => 'Nazione',
        'localization.region.field.status' => 'Attivo',
        'localization.region.field.status.0' => 'No',
        'localization.region.field.status.1' => 'Si',
        'localization.region.field.name' => 'Nome',
        'localization.region.field.isocode' => 'Codice Nazione'
];
