<?php

return
    [
        'localization.state.index.title' => 'Province',
        'localization.state.edit.title' => 'Modifica provincia',
        'localization.state.insert.title' => 'Inserisci provincia',
        'localization.state.field.id' => 'ID',
        'localization.state.field.name' => 'Nome',
        'localization.state.field.isocode' => 'Codice',
        'localization.state.field.region' => 'Regione',
        'localization.state.field.status' => 'Attivo',
        'localization.state.field.status.0' => 'No',
        'localization.state.field.status.1' => 'Si',
];
