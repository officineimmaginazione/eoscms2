<?php

return
    [
        'localization.tax.index.title' => 'Aliquote',
        'localization.tax.edit.title' => 'Modifica aliquota',
        'localization.tax.insert.title' => 'Inserisci aliquota',
        'localization.tax.field.id' => 'ID',
        'localization.tax.field.name' => 'Aliquota',
        'localization.tax.field.rate' => 'Percentuale',
        'localization.tax.field.status' => 'Attivo',
        'localization.tax.field.status.0' => 'No',
        'localization.tax.field.status.1' => 'Si',
        'localization.tax.field.reference' => 'Codice esterno',
];
