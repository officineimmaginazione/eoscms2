<?php

if ($this->componentIsActive('localization'))
{
    $this->addParent('localization', $this->view->trans('localization.dashboard.menu'), '', 'fa fa-globe', 90);
    $this->addItem('localization', 'city', $this->view->trans('localization.dashboard.city'), $this->view->path->urlFor('localization', ['city', 'index']), 'fa fa-circle-o', null, 'localization.city');
    $this->addItem('localization', 'state', $this->view->trans('localization.dashboard.state'), $this->view->path->urlFor('localization', ['state', 'index']), 'fa fa-circle-o', null, 'localization.state');
    $this->addItem('localization', 'region', $this->view->trans('localization.dashboard.region'), $this->view->path->urlFor('localization', ['region', 'index']), 'fa fa-circle-o', null, 'localization.region');
    $this->addItem('localization', 'country', $this->view->trans('localization.dashboard.country'), $this->view->path->urlFor('localization', ['country', 'index']), 'fa fa-circle-o', null, 'localization.country');
    $this->addItem('localization', 'language', $this->view->trans('localization.dashboard.language'), $this->view->path->urlFor('localization', ['language', 'index']), 'fa fa-circle-o', null, 'localization.language');
    $this->addItem('localization', 'tax', $this->view->trans('localization.dashboard.tax'), $this->view->path->urlFor('localization', ['tax', 'index']), 'fa fa-circle-o', null, 'localization.tax');
}