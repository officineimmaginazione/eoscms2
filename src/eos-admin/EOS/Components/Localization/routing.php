<?php

use \EOS\Components\System\Classes\AuthRole;

/* City */
$this->mapComponent(['GET'], 'Localization', ['city/index' => 'CityController:index'], ['auth' => ['localization.city' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['city/edit/{id}' => 'CityController:edit'], ['auth' => ['localization.city' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['city/ajaxlist' => 'CityController:ajaxList'], ['auth' => ['localization.city' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['city/ajaxsave' => 'CityController:ajaxSave'], ['auth' => ['localization.city' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['city/ajaxdelete' => 'CityController:ajaxDelete'], ['auth' => ['localization.city' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['city/ajaxcommand' => 'CityController:ajaxCommand'], ['auth' => ['localization.city' => [AuthRole::READ]]]);

/* Country */
$this->mapComponent(['GET'], 'Localization', ['country/index' => 'CountryController:index'], ['auth' => ['localization.country' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['country/edit/{id}' => 'CountryController:edit'], ['auth' => ['localization.country' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['country/ajaxlist' => 'CountryController:ajaxList'], ['auth' => ['localization.country' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['country/ajaxsave' => 'CountryController:ajaxSave'], ['auth' => ['localization.country' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['country/ajaxdelete' => 'CountryController:ajaxDelete'], ['auth' => ['localization.country' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['country/ajaxcommand' => 'CountryController:ajaxCommand'], ['auth' => ['localization.country' => [AuthRole::READ]]]);

/* Language */
$this->mapComponent(['GET'], 'Localization', ['language/index' => 'LanguageController:index'], ['auth' => ['localization.language' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['language/edit/{id}' => 'LanguageController:edit'], ['auth' => ['localization.language' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['language/ajaxlist' => 'LanguageController:ajaxList'], ['auth' => ['localization.language' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['language/ajaxsave' => 'LanguageController:ajaxSave'], ['auth' => ['localization.language' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['language/ajaxdelete' => 'LanguageController:ajaxDelete'], ['auth' => ['localization.language' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['language/ajaxcommand' => 'LanguageController:ajaxCommand'], ['auth' => ['localization.language' => [AuthRole::READ]]]);

/* Region */
$this->mapComponent(['GET'], 'Localization', ['region/index' => 'RegionController:index'], ['auth' => ['localization.region' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['region/edit/{id}' => 'RegionController:edit'], ['auth' => ['localization.region' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['region/ajaxlist' => 'RegionController:ajaxList'], ['auth' => ['localization.region' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['region/ajaxsave' => 'RegionController:ajaxSave'], ['auth' => ['localization.region' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['region/ajaxdelete' => 'RegionController:ajaxDelete'], ['auth' => ['localization.region' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['region/ajaxcommand' => 'RegionController:ajaxCommand'], ['auth' => ['localization.region' => [AuthRole::READ]]]);

/* State */
$this->mapComponent(['GET'], 'Localization', ['state/index' => 'StateController:index'], ['auth' => ['localization.state' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['state/edit/{id}' => 'StateController:edit'], ['auth' => ['localization.state' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['state/ajaxlist' => 'StateController:ajaxList'], ['auth' => ['localization.state' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['state/ajaxsave' => 'StateController:ajaxSave'], ['auth' => ['localization.state' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['state/ajaxdelete' => 'StateController:ajaxDelete'], ['auth' => ['localization.state' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['state/ajaxcommand' => 'StateController:ajaxCommand'], ['auth' => ['localization.state' => [AuthRole::READ]]]);

/* Tax */
$this->mapComponent(['GET'], 'Localization', ['tax/index' => 'TaxController:index'], ['auth' => ['localization.tax' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Localization', ['tax/edit/{id}' => 'TaxController:edit'], ['auth' => ['localization.tax' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['tax/ajaxlist' => 'TaxController:ajaxList'], ['auth' => ['localization.tax' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Localization', ['tax/ajaxsave' => 'TaxController:ajaxSave'], ['auth' => ['localization.tax' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Localization', ['tax/ajaxdelete' => 'TaxController:ajaxDelete'], ['auth' => ['localization.tax' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Localization', ['tax/ajaxcommand' => 'TaxController:ajaxCommand'], ['auth' => ['localization.tax' => [AuthRole::READ]]]);
