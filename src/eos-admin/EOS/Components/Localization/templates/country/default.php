<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-info btn-sm pull-right')
        ->click('function (e) { addNew();}')
        ->content('<i class="fa fa-plus"></i>')
        ->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('Localization', 'country/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('localization.country.field.id'));
$tbl->addColumn('name', $this->transE('localization.country.field.name'));
$tbl->addColumn('isocode', $this->transE('localization.country.field.isocode'));
$tbl->addColumn('status', $this->transE('localization.country.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->addColumnOrder('id', true);
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('localization', 'city/edit/0') ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->getUrlFor('localization', 'city/edit') ?>" + id + "/";
    }

    function renderStatus(data)
    {
        switch (parseInt(data))
        {
            case 1:
                return '<span class="label label-success"><?php $this->transP('localization.country.field.status.1'); ?></span>';
                break;
            case 2:
                return '<span class="label label-warning"><?php $this->transP('localization.country.field.status.2'); ?></span>';
                break;
            default:
                return '<span class="label label-danger"><?php $this->transP('localization.country.field.status.0'); ?></span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
