<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function ()
{
	(new \EOS\UI\Bootstrap\Button('btn-new'))
	->attr('class', ' btn-success ml-auto mr-1')
	->click('function (e) { addNew();}')
	->content('Crea')
	->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('Localization', 'language/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('localization.language.field.id'));
$tbl->addColumn('name', $this->transE('localization.language.field.name'));
$tbl->addColumn('isocode', $this->transE('localization.language.field.isocode'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->addColumnOrder('id', true);
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('localization', 'language/edit/0') ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->getUrlFor('localization', 'language/edit') ?>" + id + "/";
    }

    function renderStatus(data)
    {
        switch (parseInt(data))
        {
            case 1:
                return '<span class="label label-success"><?php $this->transP('localization.language.field.status.1'); ?></span>';
                break;
            case 2:
                return '<span class="label label-warning"><?php $this->transP('localization.language.field.status.2'); ?></span>';
                break;
            default:
                return '<span class="label label-danger"><?php $this->transP('localization.language.field.status.0'); ?></span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
