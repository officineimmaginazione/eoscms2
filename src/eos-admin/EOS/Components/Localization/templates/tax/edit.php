<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))->attr('name', 'status')
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
    if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.name'))->printRender();
    foreach ($this->dataLangList as $l)
    {
        (new \EOS\UI\Bootstrap\Input('name-' . $l['id']))
            ->attr('class', 'translatable-field lang-' . $l['id'])
            ->bind($this->data, 'name-' . $l['id'])->printRender($this);
    }
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('localization.tax.field.rate'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('rate'))
            ->bind($this->data, 'rate')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('localization.tax.field.reference'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('reference'))
        ->bind($this->data, 'reference')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-data');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData(proc)
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('localization', 'tax/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    if (proc === undefined)
                    {

                        location.href = json.redirect;
                    } else
                    {
                        proc();
                    }
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('localization', 'tax/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('localization.tax.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('localization', 'tax/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    $(function ()
    {
        $('#status').on('change', function ()
        {
            if ($('#status').val() == '2')
            {
                $('#btn-request-active').show();
            } else
            {
                $('#btn-request-active').hide();
            }
        });
        $('#status').trigger('change');
    });
    
    function runAjax(urlAjax, data, resultEvent)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: urlAjax,
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    resultEvent(json);
                } else
                {
                    if (json.message !== undefined)
                    {
                        bootbox.alert(json.message);
                    }
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
    }
    
    
</script>
<?php
$this->endCaptureScript();
