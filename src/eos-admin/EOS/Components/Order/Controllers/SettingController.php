<?php

namespace EOS\Components\Order\Controllers;

use EOS\System\Util\FormValidator;

class SettingController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->pageTitle = $v->trans('order.setting.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $m = new \EOS\Components\Order\Models\SettingModel($this->container);
        $v->loadiCheck();
        $v->data = $m->getData();
        return $v->render('setting/default', true);
    }

    public function ajaxSaveData($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Order\Models\SettingModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('order', 'setting/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
