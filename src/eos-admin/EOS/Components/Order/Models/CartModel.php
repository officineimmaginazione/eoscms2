<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;

class CartModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
            //->select(null)->select('c.name, c.surname, c.phone, c.email, osl.name as status, os.color')
            ->select(null)->select('c.id, a.company, a.alias, c.total, c.status')
            ->leftJoin($this->db->tableFix('#__address as a ON c.id_address_invoice = a.id'))
            ->orderBy('c.id','DESC');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
            ->select(null)
            ->select('c.id')
            ->select('c.status')
            ->select('c.total')
            ->select('c.id_customer')
            ->select('c.id_address_delivery')
            ->select('c.id_address_invoice')
            ->select('c.id_agent')
            ->select('c.ins_date')
            ->select('c.options')
            ->where('c.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'];
            $res['amount'] = $rc['total'];
            $res['ins_date'] = $rc['ins_date'];
            $options = json_decode($rc['options'], true);
            $query2 = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->where('a.id = ?', (int) $rc['id_address_delivery']);
            $rc2 = $query2->fetch();
            if (!empty($rc2))
            {
                $optionsaddress = json_decode($rc2['options'], true);
                $res['name'] = $rc2['name'];
                $res['surname'] = $rc2['surname'];
                $res['phone'] = $rc2['phone'];
                $res['address'] = $rc2['address1'];
                $res['company'] = $rc2['company'];
                $res['vat'] = $rc2['vat'];
                $res['cif'] = $rc2['cif'];
                $res['note'] = $optionsaddress['note'];
                $ml = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                $current_city = $ml->getCity(ArrayHelper::getInt($rc2, 'id_city'));
                $res['city'] = (isset($current_city['name']) && $current_city['name'] != '')  ? $current_city['name'] : '';
                $res['state'] = (isset($current_city['statecode']) && $current_city['statecode'] != '') ? $current_city['statecode'] : '';
                $res['country'] = (isset($current_city['country']) && $current_city['name'] != '') ? $current_city['country'] : '';
            } else {
                $res['name'] = '';
                $res['surname'] = '';
                $res['phone'] = '';
                $res['email'] = '';
                $res['address'] = '';
                $res['city'] = '';
                $res['state'] = '';
                $res['country'] = '';
                $res['company'] = '';
                $res['vat'] = '';
                $res['cif'] = '';
                $res['note'] = '';
            }
            $query3 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_agent']);
            $rc3 = $query3->fetch();
            if (!empty($rc3))
            {
                $res['agent-name'] = $rc3['name'];
            }
            $query4 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_customer']);
            $rc4 = $query4->fetch();
            if (!empty($rc4))
            {
                $res['email'] = $rc4['email_alternative'];
            }
        }   
        return $res;
    }
    
    public function getDataRow($id)
    {
        $sql = 'select cr.id, cr.id_product, pl.name, pl.description, ml.name as manufacturer_name, cr.quantity, cr.options, p.price, p.type_condition, t.rate ' .
            ' from #__cart_row cr ' .
            ' left outer join #__product p on cr.id_product = p.id ' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' left outer join #__tax t on t.id = p.id_tax ' .
            ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        //print_r($sql); exit();
        $r = $q->fetchAll();
        $i = 0;
        $res = [];
        foreach ($r as $item)
        {
            $sql = 'select pal.name' .
                ' from #__product_product_attribute_combination ppac ' .
                ' inner join #__product_attribute pa on pa.id = ppac.id_attribute ' .
                ' inner join #__product_attribute_lang pal on pal.id_attribute = pa.id' .
                ' where ppac.id_product = :id_product and pa.id_attribute_group  = 1';
            $q = $this->db->prepare($sql);
            $q->bindValue(':id_product', $item['id_product']);
            $q->execute();
            //$res = $q->fetchAll();
            $attribute = $q->fetch();
            $res[$i]['id'] = $item['id'];
            $res[$i]['id_product'] = $item['id_product'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['description'] = $item['description'];
            $res[$i]['manufacturer'] = $item['manufacturer_name'];
            $res[$i]['quantity'] = $item['quantity'];
            $res[$i]['price'] = $item['price'];
            $res[$i]['rate'] = $item['rate'];
            $res[$i]['type_condition'] = $item['type_condition'];
            $res[$i]['options'] = $item['options'];
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id_product']);
            foreach ($listimage as $single)
            {
                $img = $image->getObject($item['id_product'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                if (!empty($img))
                {
                    $res[$i]['url'] = $img->url;
                }
            }
            $i++;
        }
        //print_r($res); exit();
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'status' => $data['status'],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblCart = $this->db->tableFix('#__cart');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblCart, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblCart)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $this->db->commit();

        return true;
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__product_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_product', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

}
