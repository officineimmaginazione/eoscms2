<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

class OrderModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__order o'))
            ->leftJoin($this->db->tableFix('#__order_state as os ON o.status = os.id'))
            ->leftJoin($this->db->tableFix('#__order_state_lang as osl ON os.id = osl.id_order_state'))
            //->select(null)->select('o.name, o.surname, o.phone, o.email, osl.name as status, os.color')
            ->select(null)->select('o.name, o.surname, o.company, o.phone, o.email')
            ->where('osl.id_lang = ?', $this->lang->getCurrentID())
            ->orderBy('o.id','DESC');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__order o'))
            ->select(null)
            ->select('o.id')
            ->select('o.name')
            ->select('o.surname')
            ->select('o.phone')
            ->select('o.email')
            ->select('o.address')
            ->select('o.city')
            ->select('o.state')
            ->select('o.country')
            ->select('o.company')
            ->select('o.vat')
            ->select('o.cif')
            ->select('o.notes')
            ->select('o.status')
            ->select('o.amount')
            ->where('o.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['phone'] = $rc['phone'];
            $res['email'] = $rc['email'];
            $res['address'] = $rc['address'];
            $res['city'] = $rc['city'];
            $res['state'] = $rc['state'];
            $res['country'] = $rc['country'];
            $res['company'] = $rc['company'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['notes'] = $rc['notes'];
            $res['status'] = $rc['status'];
            $res['amount'] = $rc['amount'];
        }
        return $res;
    }
    
    public function getDataRow($id)
    {
        $sql = 'select oro.id, oro.id_order, oro.product_name, oro.quantity, oro.price, ml.name as manufacturer ' .
            ' from #__order_row oro' .
            ' left outer join #__product p on oro.id_object = p.id ' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' left outer join #__tax t on t.id = p.id_tax ' .
            ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
            ' where oro.id_order = :id_order';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_order', (int)$id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'status' => $data['status'],
            'upd_id' => 0,
            'upd_date' => new \FluentLiteral('NOW()')
        ];
        $tblOrder = $this->db->tableFix('#__order');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblOrder, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblOrder)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $this->db->commit();

        return true;
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__product_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_product', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

}
