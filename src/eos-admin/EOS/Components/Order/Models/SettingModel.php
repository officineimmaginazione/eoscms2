<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;

class SettingModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getData()
    {
        $data = [];
        $type = "payment";
        $v = json_decode($this->db->setting->getStr('order', $type . '.paypal'), true);
        if(isset($v) && $v != null) {
            $data[$type . '-paypal-business'] = ArrayHelper::getStr($v, 'business');
            $data[$type . '-paypal-apiuser'] = ArrayHelper::getStr($v, 'apiuser');
            $data[$type . '-paypal-apipassword'] = ArrayHelper::getStr($v, 'apipassword');
            $data[$type . '-paypal-apisignature'] = ArrayHelper::getStr($v, 'apisignature');
            $data[$type . '-paypal-express'] = ArrayHelper::getValue($v, 'express');
            $data[$type . '-paypal-sandbox'] = ArrayHelper::getValue($v, 'sandbox');
            $data[$type . '-paypal-status'] = ArrayHelper::getValue($v, 'status');
        }
        $v = json_decode($this->db->setting->getStr('order', $type . '.xpay'), true);
        if(isset($v) && $v != null) {
            $data[$type . '-xpay-url'] = ArrayHelper::getStr($v, 'url');
            $data[$type . '-xpay-urlpost'] = ArrayHelper::getStr($v, 'urlpost');
            $data[$type . '-xpay-urlback'] = ArrayHelper::getStr($v, 'urlback');
            $data[$type . '-xpay-alias'] = ArrayHelper::getValue($v, 'alias');
            $data[$type . '-xpay-mac'] = ArrayHelper::getValue($v, 'mac');
            $data[$type . '-xpay-status'] = ArrayHelper::getStr($v, 'status');
        }
        json_decode($this->db->setting->getStr('order', $type . '.unicredit'), true);
        if(isset($v) && $v != null) {
            $data[$type . '-unicredit-sandbox-terminalid'] = ArrayHelper::getStr($v, 'sandbox-terminalid');
            $data[$type . '-unicredit-sandbox-requestkey'] = ArrayHelper::getStr($v, 'sandbox-requestkey');
            $data[$type . '-unicredit-sandbox-urlserver'] = ArrayHelper::getStr($v, 'sandbox-urlserver');
            $data[$type . '-unicredit-sandbox-failpayment'] = ArrayHelper::getValue($v, 'sandbox-failpayment');
            $data[$type . '-unicredit-sandbox-status'] = ArrayHelper::getValue($v, 'sandbox-status');
            $data[$type . '-unicredit-sandbox-userref'] = ArrayHelper::getStr($v, 'sandbox-userref');
            $data[$type . '-unicredit-sandbox-requestkey'] = ArrayHelper::getStr($v, 'sandbox-requestkey');
            $data[$type . '-unicredit-live-terminalid'] = ArrayHelper::getStr($v, 'live-terminalid');
            $data[$type . '-unicredit-live-requestkey'] = ArrayHelper::getStr($v, 'live-requestkey');
            $data[$type . '-unicredit-live-urlserver'] = ArrayHelper::getStr($v, 'live-urlserver');
            $data[$type . '-unicredit-live-userref'] = ArrayHelper::getStr($v, 'live-userref');
            $data[$type . '-unicredit-ssl'] = ArrayHelper::getValue($v, 'ssl');
            $data[$type . '-unicredit-status'] = ArrayHelper::getValue($v, 'status');
        }
        $data['order-payment-status'] = $this->db->setting->getBool('order', 'payment.status');
        $data['order-catalog-status'] = $this->db->setting->getBool('order', 'catalog.status');
        return $data;
    }

    public function saveData(&$data, &$err)
    {
        
        $r = [];
        $type = "payment";
        $r['apiuser'] = $data[$type . '-paypal-apiuser'];
        $r['business'] = $data[$type . '-paypal-business'];
        $r['apipassword'] = $data[$type . '-paypal-apipassword'];
        $r['apisignature'] =  $data[$type . '-paypal-apisignature'];
        $r['express'] = (isset($data[$type . '-paypal-express']) ? $data[$type . '-paypal-express'] == 'on' : false);
        $r['sandbox'] = (isset($data[$type . '-paypal-sandbox']) ? $data[$type . '-paypal-sandbox'] == 'on' : false);
        $r['status'] = (isset($data[$type . '-paypal-status']) ? $data[$type . '-paypal-status'] == 'on' : false);
        $v = json_encode($r);
        $this->db->setting->setStr('order', $type . '.paypal', $v);
        $r = [];
        $r['url'] = $data[$type . '-xpay-url'];
        $r['urlpost'] = $data[$type . '-xpay-urlpost'];
        $r['urlback'] =  $data[$type . '-xpay-urlback'];
        $r['alias'] = $data[$type . '-xpay-alias'];
        $r['mac'] = $data[$type . '-xpay-mac'];
        $r['status'] = (isset($data[$type . '-xpay-status']) ? $data[$type . '-xpay-status'] == 'on' : false);
        $v = json_encode($r);
        $this->db->setting->setStr('order', $type . '.xpay', $v);
        $r = [];
        $r['sandbox-terminalid'] = $data[$type . '-unicredit-sandbox-terminalid'];
        $r['sandbox-requestkey'] = $data[$type . '-unicredit-sandbox-requestkey'];
        $r['sandbox-urlserver'] =  $data[$type . '-unicredit-sandbox-urlserver'];
        $r['sandbox-userref'] =  $data[$type . '-unicredit-sandbox-userref'];
        $r['sandbox-failpayment'] = (isset($data[$type . '-unicredit-sandbox-failpayment']) ? $data[$type . '-unicredit-sandbox-failpayment'] == 'on' : false);
        $r['sandbox-status'] = (isset($data[$type . '-unicredit-sandbox-status']) ? $data[$type . '-unicredit-sandbox-status'] == 'on' : false);
        $r['live-urlserver'] =  $data[$type . '-unicredit-live-urlserver'];
        $r['live-terminalid'] = $data[$type . '-unicredit-live-terminalid'];
        $r['live-requestkey'] = $data[$type . '-unicredit-live-requestkey'];
        $r['live-userref'] = $data[$type . '-unicredit-live-userref'];
        $r['ssl'] = (isset($data[$type . '-unicredit-ssl']) ? $data[$type . '-unicredit-ssl'] == 'on' : false);
        $r['status'] = (isset($data[$type . '-unicredit-status']) ? $data[$type . '-unicredit-status'] == 'on' : false);
        $v = json_encode($r);
        $this->db->setting->setStr('order', $type . '.unicredit', $v);
        
        $this->db->setting->setBool('order', 'payment.status', (isset($data['order-payment-status']) ? $data['order-payment-status'] == 'on' : false));
        $this->db->setting->setBool('order', 'catalog.status', (isset($data['order-catalog-status']) ? $data['order-catalog-status'] == 'on' : false));
        
        return true;
    }

}
