<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

class StateModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__order_state as os'))
            ->leftJoin($this->db->tableFix('#__order_state_lang as osl ON os.id = osl.id_order_state'))
            ->where('osl.id_lang = ?', $this->lang->getCurrentID())
            ->orderBy('os.id');
        return $f;
    }

    public function getList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__order_state as os'))
            ->leftJoin($this->db->tableFix('#__order_state_lang as osl ON os.id = osl.id_order_state'))
            ->select('osl.name, os.color')
            ->where('osl.id_lang = ?', $this->lang->getCurrentID());
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__order_state os'))
            ->select(null)
            ->select('os.id')
            ->select('os.color')
            ->select('os.invoice')
            ->select('os.send_email')
            ->where('os.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['color'] = $rc['color'];
            $res['invoice'] = $rc['invoice'];
            $res['send_email'] = $rc['send_email'];
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__order_state_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_order_state', $id)
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                }
            }
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'color' => $data['color'],
            'send_email' => $data['send_email'],
            'upd_id' => $this->user->getUserID(),
            'upd_date' => $this->db->util->nowToDBDateTime()
        ];
        $tblContent = $this->db->tableFix('#__order_state');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $data);
        }
        $this->db->commit();
        return true;
    }

    private function saveDataLang($id_lang, &$data)
    {
        $tblContent = $this->db->tableFix('#__order_state_lang');
        $query = $this->db->newFluent()->from($tblContent)->select('id')
            ->where('id_order_state', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_order_state' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['title-' . $id_lang]
       ];
        if ($item == null)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }

    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__order_state');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__order_state_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_order_state', (int) $data['id']);
        $query->execute();
        return true;
    }

}
