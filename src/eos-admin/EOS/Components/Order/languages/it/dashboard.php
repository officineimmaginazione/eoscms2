<?php

return
    [
        'order.dashboard.menu' => 'Ordini',
        'order.dashboard.manage' => 'Gestione',
        'order.dashboard.setting' => 'Impostazioni',
];
