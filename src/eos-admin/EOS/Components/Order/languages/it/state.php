<?php

return
    [
        'order.state.index.title' => 'Stato ordini',
        'order.state.edit.title' => 'Modifca stato',
        'order.state.insert.title' => 'Inserimento stato',
        'order.state.field.id' => '#',
        'order.state.field.state' => 'Stato',
        'order.state.field.color' => 'Colore',
        'order.state.field.sendemail' => 'Invia email',
        'order.state.field.invoice' => 'Genera fattura',
        'order.state.field.deactive' => 'Disattivata',
        'order.state.field.active' => 'Attivata'
];
