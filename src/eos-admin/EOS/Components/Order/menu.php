<?php

if ($this->componentIsActive('order'))
{
    $this->addParent('order', 'Ordini', '', 'fa fa-shopping-cart', 20);
    $this->addItem('order', 'order', 'Elenco', $this->view->path->urlFor('order', ['order', 'index']), 'fa fa-circle-o', null, 'order.manage');
    $this->addItem('order', 'order', 'Stati ordini', $this->view->path->urlFor('order', ['state', 'index']), 'fa fa-circle-o', null, 'order.setting');
    $this->addItem('order', 'order', 'Impostazioni', $this->view->path->urlFor('order', ['setting', 'index']), 'fa fa-circle-o', null, 'order.setting');
    $this->addItem('order', 'order', 'Carrelli', $this->view->path->urlFor('order', ['cart', 'index']), 'fa fa-circle-o', null, 'order.manage');
    
}