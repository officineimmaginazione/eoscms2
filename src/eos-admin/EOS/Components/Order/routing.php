<?php

use \EOS\Components\System\Classes\AuthRole;

/* Order */
$this->mapComponent(['GET'], 'Order', ['order/index' => 'OrderController:index'], ['auth' => ['order.manage' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Order', ['order/edit/{id}' => 'OrderController:edit'], ['auth' => ['order.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['order/ajaxlist' => 'OrderController:ajaxlist'], ['auth' => ['order.manage' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Order', ['order/ajaxsave' => 'OrderController:ajaxSave'], ['auth' => ['order.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['order/ajaxdelete' => 'OrderController:ajaxDelete'], ['auth' => ['order.manage' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Order', ['order/ajaxrowlist' => 'OrderController:ajaxrowlist'], ['auth' => ['order.manage' => [AuthRole::READ]]]);

/* State */
$this->mapComponent(['GET'], 'Order', ['state/index' => 'StateController:index'], ['auth' => ['order.setting' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Order', ['state/edit/{id}' => 'StateController:edit'], ['auth' => ['order.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['state/ajaxlist' => 'StateController:ajaxlist'], ['auth' => ['order.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Order', ['state/ajaxsave' => 'StateController:ajaxSave'], ['auth' => ['order.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['state/ajaxdelete' => 'StateController:ajaxDelete'], ['auth' => ['order.setting' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Order', ['setting/index' => 'SettingController:index'], ['auth' => ['order.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Order', ['setting/ajaxsavedata' => 'SettingController:ajaxSaveData'], ['auth' => ['order.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);

/* Cart */
$this->mapComponent(['GET'], 'Order', ['cart/index' => 'CartController:index'], ['auth' => ['order.manage' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Order', ['cart/edit/{id}' => 'CartController:edit'], ['auth' => ['order.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['cart/ajaxlist' => 'CartController:ajaxlist'], ['auth' => ['order.manage' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Order', ['cart/ajaxsave' => 'CartController:ajaxSave'], ['auth' => ['order.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Order', ['cart/ajaxdelete' => 'CartController:ajaxDelete'], ['auth' => ['order.manage' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Order', ['cart/ajaxrowlist' => 'CartController:ajaxrowlist'], ['auth' => ['order.manage' => [AuthRole::READ]]]);
