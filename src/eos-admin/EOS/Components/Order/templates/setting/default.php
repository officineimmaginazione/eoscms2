<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {

    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
    if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-order', $this->trans('order.setting.order'));
$tab->addItem('tab-payment-paypal', $this->trans('order.setting.payment.paypal'));
$tab->addItem('tab-payment-xpay', $this->trans('order.setting.payment.xpay'));
$tab->addItem('tab-payment-unicredit', $this->trans('order.setting.payment.unicredit'));
/* Status */
$tab->startTab();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.status.label'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('order-payment-status'))->bind($this->data, 'order-payment-status')->attr('name', 'order-payment-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.catalogmode'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('order-catalog-status'))->bind($this->data, 'order-catalog-status')->attr('name', 'order-catalog-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
$tab->endTab('tab-order');
/* Paypal */
$tab->startTab();
echo '<div class="callout callout-warning">';
echo '<i class="icon fa fa-warning"></i> ';
(new \EOS\UI\Html\Label)
    ->content($this->transE('order.setting.extra.warning'))
    ->printRender();
echo '</div>';
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.status'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-paypal-status'))->attr('name', 'payment-paypal-status')->bind($this->data, 'payment-paypal-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.sandbox'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-paypal-sandbox'))->attr('name', 'payment-paypal-sandbox')->bind($this->data, 'payment-paypal-sandbox')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.express'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-paypal-express'))->attr('name', 'payment-paypal-express')->bind($this->data, 'payment-paypal-express')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.business'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-paypal-business'))->attr('name', 'payment-paypal-business')->bind($this->data, 'payment-paypal-business')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.apiuser'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-paypal-apiuser'))->attr('name', 'payment-paypal-apiuser')->bind($this->data, 'payment-paypal-apiuser')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.apipassword'))->printRender();
        echo '</div>';
        echo '<div>';
            (new \EOS\UI\Bootstrap\Input('payment-paypal-apipassword'))->attr('name', 'payment-paypal-apipassword')->bind($this->data, 'payment-paypal-apipassword')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.paypal.apisignature'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-paypal-apisignature'))->attr('name', 'payment-paypal-apisignature')->bind($this->data, 'payment-paypal-apisignature')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
$tab->endTab('tab-payment-paypal');
/* Xpay */
$tab->startTab();
echo '<div class="callout callout-warning">';
echo '<i class="icon fa fa-warning"></i> ';
(new \EOS\UI\Html\Label)
    ->content($this->transE('order.setting.extra.warning'))
    ->printRender();
echo '</div>';
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.status'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-xpay-status'))->attr('name', 'payment-xpay-status')->bind($this->data, 'payment-xpay-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.xpay.url'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-xpay-url'))->attr('name', 'payment-xpay-url')->bind($this->data, 'payment-xpay-url')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.xpay.urlpost'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-xpay-urlpost'))->attr('name', 'payment-xpay-urlpost')->bind($this->data, 'payment-xpay-urlpost')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.xpay.urlback'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-xpay-urlback'))->attr('name', 'payment-xpay-urlback')->bind($this->data, 'payment-xpay-urlback')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.xpay.alias'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-xpay-alias'))->attr('name', 'payment-xpay-alias')->bind($this->data, 'payment-xpay-alias')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.xpay.mac'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-xpay-mac'))->attr('name', 'payment-xpay-mac')->bind($this->data, 'payment-xpay-mac')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
$tab->endTab('tab-payment-xpay');
/* Pagonline */
$tab->startTab();
echo '<div class="callout callout-warning">';
echo '<i class="icon fa fa-warning"></i> ';
(new \EOS\UI\Html\Label)
    ->content($this->transE('order.setting.extra.warning'))
    ->printRender();
echo '</div>';
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.status'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-unicredit-status'))->attr('name', 'payment-unicredit-status')->bind($this->data, 'payment-unicredit-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.ssl'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-unicredit-ssl'))->attr('name', 'payment-unicredit-ssl')->bind($this->data, 'payment-unicredit-ssl')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.status'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-unicredit-sandbox-status'))->attr('name', 'payment-unicredit-sandbox-status')->bind($this->data, 'payment-unicredit-sandbox-status')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.failpayment'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Checkbox('payment-unicredit-sandbox-failpayment'))->attr('name', 'payment-unicredit-sandbox-failpayment')->bind($this->data, 'payment-unicredit-sandbox-failpayment')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.terminalid'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-sandbox-terminalid'))->attr('name', 'payment-unicredit-sandbox-terminalid')->bind($this->data, 'payment-unicredit-sandbox-terminalid')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.requestkey'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-sandbox-requestkey'))->attr('name', 'payment-unicredit-sandbox-requestkey')->bind($this->data, 'payment-unicredit-sandbox-requestkey')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.userref'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-sandbox-userref'))->attr('name', 'payment-unicredit-sandbox-userref')->bind($this->data, 'payment-unicredit-sandbox-userref')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.sandbox.urlserver'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-sandbox-urlserver'))->attr('name', 'payment-unicredit-sandbox-urlserver')->bind($this->data, 'payment-unicredit-sandbox-urlserver')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.live.terminalid'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-live-terminalid'))->attr('name', 'payment-unicredit-live-terminalid')->bind($this->data, 'payment-unicredit-live-terminalid')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.live.requestkey'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-live-requestkey'))->attr('name', 'payment-unicredit-live-requestkey')->bind($this->data, 'payment-unicredit-live-requestkey')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.live.urlserver'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-live-urlserver'))->attr('name', 'payment-unicredit-live-urlserver')->bind($this->data, 'payment-unicredit-live-urlserver')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.setting.payment.unicredit.live.userref'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('payment-unicredit-live-userref'))->attr('name', 'payment-unicredit-live-userref')->bind($this->data, 'payment-unicredit-live-userref')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
$tab->endTab('tab-payment-unicredit');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);

$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Order', 'setting/ajaxsavedata'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

</script>
<?php
$this->endCaptureScript();
