<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

class AttributeController extends \EOS\Components\System\Classes\AuthController
{

    public function indexAttributeGroup($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('product.attribute.group.index.title');
        $v->addBreadcrumb($this->path->urlFor('Product', ['attribute-group', 'index']), $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('attribute/default-attribute-group', true);
    }
    
    public function indexAttribute($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->addBreadcrumb($this->path->urlFor('Product', ['attribute-group', 'index']), $v->trans('product.attribute.group.index.title'));
        $v->pageTitle = $v->trans('product.attribute.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->idgroup = $args['id'];
        return $v->render('attribute/default-attribute', true);
    }

    public function editAttributeGroup($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->urlFor('Product', ['attribute-group', 'index']), $v->trans('product.attribute.group.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('product.attribute.group.insert.title') : $v->trans('product.attribute.group.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getGroupData($args['id']);
        return $v->render('attribute/edit-attribute-group', true);
    }
    
    public function editAttribute($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->loadColorPicker();
        $v->addBreadcrumb($this->path->urlFor('Product', ['attribute-group', 'index']), $v->trans('product.attribute.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('product.attribute.insert.title') : $v->trans('product.attribute.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        $v->groupattribute = $m->getGroupList(1);
        return $v->render('attribute/edit-attribute', true);
    }

    public function ajaxListAttribute($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang'], $args['id']), $m->db);
        $dts->addColumn('id', 'pa.id');
        $dts->addColumn('name', 'pal.name', true);
        //     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }
    
    public function ajaxListAttributeGroup($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getGroupListQuery($data['id_lang']), $m->db);
        $dts->addColumn('id', 'pag.id');
        $dts->addColumn('name', 'pagl.name', true);
        //     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSaveAttribute($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['attribute-group', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }
    
    public function ajaxSaveAttributeGroup($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
                if ($m->saveGroupData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['attribute-group', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDeleteAttribute($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['attribute', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }
    
    public function ajaxDeleteAttributeGroup($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\AttributeModel($this->container);
                if ($m->deleteGroupData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['attribute-group', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
