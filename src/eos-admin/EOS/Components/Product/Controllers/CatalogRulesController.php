<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

class CatalogRulesController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('product.catalogrules.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('catalogrules/default', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\CatalogRulesModel($this->container);
        $manufacturer = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
        $categories = new \EOS\Components\Product\Models\CategoryModel($this->container);
        $attribute = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $attribute = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $user = new \EOS\Components\Account\Models\UserModel($this->container);
        $group = new \EOS\Components\Account\Models\GroupModel($this->container);
        $v = $this->newView($request, $response); 
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->urlFor('Product', ['catalog-rules', 'index']), $v->trans('product.catalogrules.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('product.catalogrules.insert.title') : $v->trans('product.catalogrules.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        $v->manufacturer = $manufacturer->getList(1);
        $v->categories = $categories->getList(1);
        $v->attributes = $attribute->getListComplete(1);
        $v->user = $user->getUsers();
        $v->group = $group->getGroups();
        return $v->render('catalogrules/edit', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\CatalogRulesModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang']), $m->db);
        $dts->addColumn('id', 'cr.id');
        $dts->addColumn('name', 'cr.name', true);
        //     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\CatalogRulesModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['catalog-rules', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\CatalogRulesModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['catalog-rules', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
