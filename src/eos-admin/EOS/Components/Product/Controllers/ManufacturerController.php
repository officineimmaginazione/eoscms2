<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

class ManufacturerController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('product.manufacturer.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('manufacturer/default', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->urlFor('Product', ['manufacturer', 'index']), $v->trans('product.manufacturer.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('product.manufacturer.insert.title') : $v->trans('product.manufacturer.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        return $v->render('manufacturer/edit', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang']), $m->db);
        $dts->addColumn('id', 'm.id');
        $dts->addColumn('name', 'ml.name', true);
        //     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['manufacturer', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['manufacturer', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
