<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

class AttributeModel extends \EOS\System\Models\Model
{
    public function getListQuery($id_lang, $id_group = null)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
            ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pal.id_attribute = pa.id'))
            ->where('pal.id_lang = ' . (int)$id_lang)
            ->orderBy('pal.name');
        if(isset($id_group) && $id_group != '') {
            $f->where('pa.id_attribute_group = ' . (int)$id_group);
        }    
        return $f;
    }
    
    public function getList($id_lang, $id_group = null)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
            ->select(null)->select('pa.id, pal.name')
            ->where('pal.id_lang = ' . (int)$id_lang)
            ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pal.id_attribute = pa.id'))          
            ->orderBy('pal.name');
        if(isset($id_group) && $id_group != '') {
            $query->where('pa.id_attribute_group = ' . (int)$id_group);
        }       
        $list = $query->fetchAll();
        return $list;
    }
    
        public function getListComplete($id_lang) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute_group pag'))
            ->select(null)->select('pag.id, pagl.name')
            ->where('pagl.id_lang = ' . (int)$id_lang)
            ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pagl.id_attribute_group = pag.id'))          
            ->orderBy('pagl.id');
        $groups = $query->fetchAll();
        if ($groups == null)
        {
            $list = [];
        } else
        {
            foreach ($groups as $group)
            {
                
                $groupid = $group['id'];
                $list[$groupid]['name_group'] = $group['name'];
                $list[$groupid]['id'] = $groupid;
                $tblContent = $this->db->tableFix('#__product_attribute_lang');
                $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
                    ->select(null)->select('pa.id, pal.name')
                    ->where('pal.id_lang = ' . (int)$id_lang)
                    ->where('pa.id_attribute_group = ' . (int)$groupid)
                    ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pal.id_attribute = pa.id'))          
                    ->orderBy('pal.name');
                $listattribute = $query->fetchAll();
                if (empty($listattribute))
                {
                   $list[$groupid]['attribute'] = [];  
                }
                else
                {
                    foreach ($listattribute as $attribute)
                    {
                        $list[$groupid]['attribute'][$attribute['id']]['id']= $attribute['id'];
                        $list[$groupid]['attribute'][$attribute['id']]['name'] = $attribute['name'];
                    }
                }
            }
            
        }
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
            ->select(null)
            ->select('pa.id')
            ->select('pa.id_attribute_group')
            ->select('pa.color')
            ->select('pa.position')
            ->where('pa.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['attribute-group'] = $rc['id_attribute_group'];
            $res['color'] = $rc['color'];
            $res['position'] = $rc['position'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__product_attribute_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id_attribute')
                    ->where('id_attribute', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                }
            }
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'id_attribute_group' => $data['attribute-group'],
            'color' => $data['color'],
            'position' => \EOS\System\Util\ArrayHelper::getInt($data, 'position', 0)
        ];
        $tblContent = $this->db->tableFix('#__product_attribute');
        if ($data['id'] == 0)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $l['iso_code'], $data);
        }
        $this->db->commit();

        return true;
    }

    public function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__product_attribute_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_attribute', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_attribute' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang],
        ];
        if ($item == null)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
        
    }

    public function lastIDinsert() {
        $this->db->tableFix('#__product_attribute');
        return $this->db->lastInsertId();
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__product_attribute_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_attribute', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product_attribute');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }
    
    public function getGroupListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute_group pag'))
            ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pagl.id_attribute_group = pag.id'))
            ->where('pagl.id_lang = ' . (int)$id_lang)
            ->orderBy('pagl.name');
        return $f;
    }
    
    public function getGroupList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute_group pag'))
            ->select(null)->select('pag.id, pagl.name')
            ->where('pagl.id_lang = ' . (int)$id_lang)
            ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pagl.id_attribute_group = pag.id'))          
            ->orderBy('pagl.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getGroupData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute_group pag'))
            ->select(null)
            ->select('pag.id')
            ->where('pag.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__product_attribute_group_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id_attribute_group')
                    ->where('id_attribute_group', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['public-name-' . $idlang] = $rcl['public_name'];
                }
            }
        }
        return $res;
    }

    public function saveGroupData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'group_type' => $data['group-type'],
            'is_color_group' => 0,
            'position' => 0,
        ];
        $tblContent = $this->db->tableFix('#__product_attribute_group');
        if ($data['id'] == 0)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveGroupDataLang($l['id'], $l['iso_code'], $data);
        }
        $this->db->commit();

        return true;
    }

    private function saveGroupDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__product_attribute_group_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_attribute_group', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_attribute_group' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang],
            'public_name' => $data['public-name-' . $id_lang],
        ];
        if ($item == null)
        {
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
        
    }

    public function deleteGroupData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__product_attribute_group_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_attribute_group', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product_attribute_group');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }
        
   

}
