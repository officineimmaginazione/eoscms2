<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

class CatalogRulesModel extends \EOS\System\Models\Model
{
    
    private function getCompName()
    {
        return 'catalogrules';
    }

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__catalog_rules cr'))
            ->select('cr.id')
            ->select('cr.name')
            ->select('cr.reduction')
            ->select('cr.price')
            ->orderBy('cr.name');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__catalog_rules cr'))
            ->select(null)
            ->select('cr.id')
            ->select('cr.name')
            ->select('cr.id_group')
            ->select('cr.id_customer')
            ->select('cr.reduction')
            ->select('cr.reduction_type')
            ->select('cr.price')
            ->where('cr.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['id-group'] = $rc['id_group'];
            $res['id-customer'] = $rc['id_customer'];
            $res['reduction'] = $rc['reduction'];
            $res['reduction-type'] = $rc['reduction_type'];
            $res['price'] = $rc['price'];
            $tblContent = $this->db->tableFix('#__catalog_rules_condition');
            $query = $this->db->newFluent()->from($tblContent)->select('type')->select('value')
                ->where('id_catalog_rules', $rc['id']);
            $rcr = $query->fetchAll();
            foreach ($rcr as $r)
            {
                if($r['type'] == 'group-customer') {
                    $res['id_group'] = $r['value'];
                }
                if($r['type'] == 'customer') {
                    $res['id_customer'] = $r['value'];
                }
                if($r['type'] == 'manufacturer') {
                    $res['id_manufacturer'] = $r['value'];
                }
                if($r['type'] == 'category') {
                    $res['id_category'] = $r['value'];
                }
                if($r['type'] == 'attribute') {
                    $tblContent = $this->db->tableFix('#__product_attribute');
                    $query = $this->db->newFluent()->from($tblContent)->select('id_attribute_group')
                        ->where('id', $r['value']);
                    $attributegroup = $query->fetch();
                    $res['attribute-'.$attributegroup['id_attribute_group']] = $r['value'];
                }
            }
            
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'name' => $data['name'],
            'id_group' => $data['id-group'],
            'id_customer' => $data['id-customer'],
            'reduction' => $data['reduction'],
            'reduction_type' => $data['reduction-type'],
            'price' => $data['price']
        ];
        $tblContent = $this->db->tableFix('#__catalog_rules');
        if ($data['id'] == 0)
        {
           $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $this->db->commit();
        $this->saveCondition($data);
        return true;
    }

    private function saveCondition(&$data)
    {
        $mag = new AttributeModel($this->container);
        $tblContent = $this->db->tableFix('#__catalog_rules_condition');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_catalog_rules', (int) $data['id']);
        $query->execute();
        $attributegroups = $mag->getGroupList(1);
        foreach ($attributegroups as $attributegroup) {
            if(isset($data['attribute-'.$attributegroup['id']]) && $data['attribute-'.$attributegroup['id']] != '' && $data['attribute-'.$attributegroup['id']] != 0) {
                $values = [
                    'id_catalog_rules' => $data['id'],
                    'type' => 'attribute',
                    'value' => $data['attribute-'.$attributegroup['id']]
                ];
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
                $query->execute();
            } 
        }
        if(isset($data['manufacturer']) && $data['manufacturer'] != '' && $data['manufacturer'] != 0) {
            $values = [
                'id_catalog_rules' => $data['id'],
                'type' => 'manufacturer',
                'value' => $data['manufacturer']
            ];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
        }
        if(isset($data['category']) && $data['category'] != '' && $data['category'] != 0) {
            $values = [
                'id_catalog_rules' => $data['id'],
                'type' => 'category',
                'value' => $data['category']
            ];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
        }
        if(isset($data['id_customer']) && $data['id_customer'] != '' && $data['id_customer'] != 0) {
            $values = [
                'id_catalog_rules' => $data['id'],
                'type' => 'customer',
                'value' => $data['id_customer']
            ];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
        }
        if(isset($data['id_group']) && $data['id_group'] != '' && $data['id_group'] != 0) {
            $values = [
                'id_catalog_rules' => $data['id'],
                'type' => 'group-customer',
                'value' => $data['id_group']
            ];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__catalog_rules_condition');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_catalog_rules', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__catalog_rules');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

}
