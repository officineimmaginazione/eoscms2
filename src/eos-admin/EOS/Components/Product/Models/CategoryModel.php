<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class CategoryModel extends \EOS\Components\System\Classes\AuthModel
{
    private function getCompName()
    {
        return 'product/category';
    }

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))
            ->where('pcl.id_lang = ' . (int)$id_lang)
            ->orderBy('pcl.name');
        return $f;
    }
    
    public function getList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.id, pcl.name')
            ->where('pcl.id_lang = ' . (int)$id_lang)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))          
            ->orderBy('pcl.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)
            ->select('pc.id')
            ->select('pc.status')
            ->where('pc.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'] == 1;
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__product_category_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_category', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                }
            }
        }
        $seo = new \EOS\System\Seo\Manager($this->container);
        $seo->getSeoArray($this->getCompName(), $id, $idlang, $res);
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
            'up_id' => $this->user->getUserID(),
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__product_category');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $l['iso_code'], $data);
        }
        $this->db->commit();
        return true;
    }

    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__product_category_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_category', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_category' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang],
            'description' => $data['descr-' . $id_lang],
            'up_id' => $this->user->getUserID(),
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $seo = new \EOS\System\Seo\Manager($this->container);
        $seo->setSeoArray($this->getCompName(), $data['id'], $id_lang, $data);
        if ($item == null)
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__product_category_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_category', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product_category');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

}
