<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class ManufacturerModel extends \EOS\Components\System\Classes\AuthModel
{
    private function getCompName()
    {
        return 'manufacturer';
    }

    public function getListQuery($id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'))
            ->where('ml.id_lang = ' . (int)$id_lang)
            ->orderBy('ml.name');
        return $f;
    }
    
    public function getList($id_lang)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)->select('m.id, ml.name')
            ->where('ml.id_lang = ' . (int)$id_lang)
            ->where('m.status = 1')
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'))          
            ->orderBy('ml.name');
        $list = $query->fetchAll();
        return $list;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)
            ->select('m.id')
            ->select('m.status')
            ->select('m.reference')
            ->where('m.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'] == 1;
            $res['reference'] = $rc['reference'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__manufacturer_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_manufacturer', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                }
            }
        }
        $seo = new \EOS\System\Seo\Manager($this->container);
        $seo->getSeoArray($this->getCompName(), $id, $idlang, $res);
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'reference' => ArrayHelper::getStr($data, 'reference'),
            'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
            'up_id' => $this->user->getUserID(),
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__manufacturer');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $l['iso_code'], $data);
        }
        $this->db->commit();

        return true;
    }

    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__manufacturer_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_manufacturer', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_manufacturer' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang],
            'description' => $data['descr-' . $id_lang],
            'up_id' => $this->user->getUserID(),
            'up_date' => new \FluentLiteral('NOW()')
        ];
        if ($item == null)
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
        $seo = new \EOS\System\Seo\Manager($this->container);
        $seo->setSeoArray($this->getCompName(), $data['id'], $id_lang, $data);
        
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__manufacturer_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_manufacturer', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__manufacturer');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

}
