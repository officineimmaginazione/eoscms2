<?php

$this->add('product.attribute', 'product.dashboard.menu,product.dashboard.attribute');
$this->add('product.category', 'product.dashboard.menu,product.dashboard.category');
$this->add('product.manufacturer', 'product.dashboard.menu,product.dashboard.manufacturer');
$this->add('product.product', 'product.dashboard.menu,product.dashboard.product');
$this->add('product.catalogrules', 'product.dashboard.menu,product.dashboard.catalogrules');
