<?php

return
    [
        'product.attribute.index.title' => 'Attributi',
        'product.attribute.edit.title' => 'Modifica attributi',
        'product.attribute.insert.title' => 'Inserimento attributi',
        'product.attribute.field.id' => 'ID',
        'product.attribute.field.name' => 'Nome',
        'product.attribute.field.color' => 'Colore',
        'product.attribute.field.group' => 'Gruppo',
        'product.attribute.field.lang' => 'Lingua',
        'product.attribute.delete.confirm' => 'Vuoi cancellare l\'attributi?',
        'product.attribute.group.index.title' => 'Gruppo attributi',
        'product.attribute.group.edit.title' => 'Modifica gruppo attributi',
        'product.attribute.group.insert.title' => 'Inserimento gruppo attributi',
        'product.attribute.group.field.id' => 'ID',
        'product.attribute.group.field.name' => 'Nome / Codice interno',
        'product.attribute.group.field.type' => 'Tipologia',
        'product.attribute.group.field.publicname' => 'Nome visualizzato',
        'product.attribute.group.delete.confirm' => 'Vuoi cancellare il gruppo di attributi?',
];
