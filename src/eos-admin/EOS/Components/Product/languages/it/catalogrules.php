<?php

return
    [
        'product.catalogrules.index.title' => 'Regole Catalogo',
        'product.catalogrules.edit.title' => 'Modifica regole catalogo',
        'product.catalogrules.insert.title' => 'Inserimento regole catalogo',
        'product.catalogrules.field.id' => 'ID',
        'product.catalogrules.field.name' => 'Nome',
        'product.catalogrules.field.manufacturer' => 'Brand',
        'product.catalogrules.field.category' => 'Categoria',
        'product.catalogrules.field.reduction_type' => 'Tipologia regola',
        'product.catalogrules.field.reduction_type.0' => 'Sconto',
        'product.catalogrules.field.reduction_type.1' => 'Sconto sommato',
        'product.catalogrules.field.reduction_type.2' => 'Prezzo fisso',
        'product.catalogrules.field.reduction' => 'Sconto',
        'product.catalogrules.field.price' => 'Prezzo',
        'product.catalogrules.field.idgroup' => 'Gruppo Clienti',
        'product.catalogrules.field.idcustomer' => 'Cliente singolo',
        
];
