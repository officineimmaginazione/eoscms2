<?php

return
    [
        'product.category.index.title' => 'Categorie',
        'product.category.edit.title' => 'Modifica categoria',
        'product.category.insert.title' => 'Inserimento categoria',
        'product.category.field.id' => 'ID',
        'product.category.field.name' => 'Nome',
        'product.category.field.description' => 'Descrizione',
        'product.category.field.lang' => 'Lingua',
        'product.category.delete.confirm' => 'Vuoi cancellare la categoria?',
];
