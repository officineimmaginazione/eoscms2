<?php

return
    [
        'product.manufacturer.index.title' => 'Brand',
        'product.manufacturer.edit.title' => 'Modifica brand',
        'product.manufacturer.insert.title' => 'Inserimento brand',
        'product.manufacturer.field.id' => 'ID',
        'product.manufacturer.field.name' => 'Nome',
        'product.manufacturer.field.description' => 'Descrizione',
        'product.manufacturer.field.lang' => 'Lingua',
        'product.manufacturer.delete.confirm' => 'Vuoi cancellare il brand?',
];
