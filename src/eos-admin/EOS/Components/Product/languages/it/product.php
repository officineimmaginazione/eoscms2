<?php

return
    [
        'product.product.index.title' => 'Prodotti',
        'product.product.edit.title' => 'Modifica prodotti',
        'product.product.insert.title' => 'Inserimento prodotti',
        'product.product.field.id' => 'ID',
        'product.product.field.name' => 'Nome',
        'product.product.field.description-short' => 'Descrizione breve',
        'product.product.field.description' => 'Descrizione',
        'product.product.field.lang' => 'Lingua',
        'product.product.delete.confirm' => 'Vuoi cancellare la pagina?',
        'product.product.field.price' => 'Prezzo',
        'product.product.field.image.default' => 'Immagine Principale',
        'product.product.field.status' => 'Attivo',
        'product.product.field.ean13' => 'EAN13',
        'product.product.field.isbn' => 'ISBN',
        'product.product.field.upc' => 'UPC',
        'product.product.field.ext_code' => 'Altro codice',
        'product.product.field.width' => 'Larghezza',
        'product.product.field.height' => 'Altezza',
        'product.product.field.depth' => 'Profondità',
        'product.product.field.weight' => 'Peso',
        'product.product.field.manufacturer' => 'Brand',
        'product.product.field.category' => 'Categoria',
        'product.product.field.attribute.placeholder' => 'Attributo personalizzato',
        'product.product.field.conditiontype' => 'Condizione prodotto',
        'product.product.field.featured' => 'Tipologia di visualizzazione'
];
