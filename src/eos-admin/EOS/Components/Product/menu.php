<?php

if ($this->componentIsActive('product'))
{
    $this->addParent('product', 'Prodotti', '', 'fa fa-tags', 20);
    $this->addItem('product', 'product', 'Articoli', $this->view->path->urlFor('product', ['product', 'index']), 'fa fa-circle-o', null, 'product.product');
    $this->addItem('product', 'category', 'Categorie', $this->view->path->urlFor('product', ['category', 'index']), 'fa fa-circle-o', null, 'product.category');
    $this->addItem('product', 'manufacturer', 'Brand', $this->view->path->urlFor('product', ['manufacturer', 'index']), 'fa fa-circle-o', null, 'product.manufacturer');
    $this->addItem('product', 'attribute', 'Attributi', $this->view->path->urlFor('product', ['attribute-group', 'index']), 'fa fa-circle-o', null, 'product.attribute');
    $this->addItem('product', 'catalogrules', 'Regole Catalogo', $this->view->path->urlFor('product', ['catalog-rules', 'index']), 'fa fa-circle-o', null, 'product.catalogrules');
}