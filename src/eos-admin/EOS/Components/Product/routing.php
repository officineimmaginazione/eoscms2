<?php

use \EOS\Components\System\Classes\AuthRole;

$this->mapComponent(['GET'], 'Product', ['product/index' => 'ProductController:index'], ['auth' => ['product.product' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['product/edit/{id}' => 'ProductController:edit'], ['auth' => ['product.product' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['product/ajaxlist' => 'ProductController:ajaxlist'], ['auth' => ['product.product' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['product/ajaxsave' => 'ProductController:ajaxSave'], ['auth' => ['product.product' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['product/ajaxdelete' => 'ProductController:ajaxDelete'], ['auth' => ['product.product' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Product', ['product/ajaximageupload' => 'ProductController:ajaxImageUpload'], ['auth' => ['product.product' => [AuthRole::UPDATE]]]);
$this->mapComponent(['POST'], 'Product', ['product/ajaxattachmentupload' => 'ProductController:ajaxAttachmentUpload'], ['auth' => ['product.product' => [AuthRole::UPDATE]]]);

$this->mapComponent(['GET'], 'Product', ['category/index' => 'CategoryController:index'], ['auth' => ['product.category' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['category/edit/{id}' => 'CategoryController:edit'], ['auth' => ['product.category' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['category/ajaxlist' => 'CategoryController:ajaxlist'], ['auth' => ['product.category' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['category/ajaxsave' => 'CategoryController:ajaxSave'], ['auth' => ['product.category' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['category/ajaxdelete' => 'CategoryController:ajaxDelete'], ['auth' => ['product.category' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Product', ['manufacturer/index' => 'ManufacturerController:index'], ['auth' => ['product.manufacturer' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['manufacturer/edit/{id}' => 'ManufacturerController:edit'], ['auth' => ['product.manufacturer' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['manufacturer/ajaxlist' => 'ManufacturerController:ajaxlist'], ['auth' => ['product.manufacturer' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['manufacturer/ajaxsave' => 'ManufacturerController:ajaxSave'], ['auth' => ['product.manufacturer' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['manufacturer/ajaxdelete' => 'ManufacturerController:ajaxDelete'], ['auth' => ['product.manufacturer' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Product', ['attribute/index/{id}' => 'AttributeController:indexAttribute'], ['auth' => ['product.attribute' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['attribute/edit/{id}' => 'AttributeController:editAttribute'], ['auth' => ['product.attribute' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['attribute/ajaxlist/{id}' => 'AttributeController:ajaxListAttribute'], ['auth' => ['product.attribute' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['attribute/ajaxsave' => 'AttributeController:ajaxSaveAttribute'], ['auth' => ['product.attribute' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['attribute/ajaxdelete' => 'AttributeController:ajaxDeleteAttribute'], ['auth' => ['product.attribute' => [AuthRole::DELETE]]]);
$this->mapComponent(['GET'], 'Product', ['attribute-group/index' => 'AttributeController:indexAttributeGroup'], ['auth' => ['product.attribute' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['attribute-group/edit/{id}' => 'AttributeController:editAttributeGroup'], ['auth' => ['product.attribute' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['attribute-group/ajaxlist' => 'AttributeController:ajaxListAttributeGroup'], ['auth' => ['product.attribute' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['attribute-group/ajaxsave' => 'AttributeController:ajaxSaveAttributeGroup'], ['auth' => ['product.attribute' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['attribute-group/ajaxdelete' => 'AttributeController:ajaxDeleteAttributeGroup'], ['auth' => ['product.attribute' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Product', ['catalog-rules/index' => 'CatalogRulesController:index'], ['auth' => ['product.catalogrules' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Product', ['catalog-rules/edit/{id}' => 'CatalogRulesController:edit'], ['auth' => ['product.catalogrules' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['catalog-rules/ajaxlist' => 'CatalogRulesController:ajaxlist'], ['auth' => ['product.catalogrules' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Product', ['catalog-rules/ajaxsave' => 'CatalogRulesController:ajaxSave'], ['auth' => ['product.catalogrules' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Product', ['catalog-rules/ajaxdelete' => 'CatalogRulesController:ajaxDelete'], ['auth' => ['product.catalogrules' => [AuthRole::DELETE]]]);