<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function ()
{
	(new \EOS\UI\Bootstrap\Button('btn-new'))
	->attr('class', ' btn-success ml-auto mr-1')
	->click('function (e) { addNew();}')
	->content('Crea')
	->printRender($this);
	if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Product', ['catalog-rules', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('product.catalogrules.field.id'));
$tbl->addColumn('name', $this->transE('product.catalogrules.field.name'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id'];?>;
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('Product', ['catalog-rules', 'edit', '0']) ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
          id_lang = n_id_lang;
          $('#datatable').DataTable().ajax.reload();
        }
    }
    
    function editRow(id)
    {
      window.location.href = "<?php echo $this->path->urlFor('Product', ['catalog-rules', 'edit']) ?>" + id + "/";     
    }
</script>
<?php
$this->endCaptureScript();
