<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-standard', $this->trans('system.common.standard'));
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

/**
 * TITOLO - INIZIO
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{   
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.name'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('price'))->attr('name', 'name')->bind($this->data, 'name')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.reduction_type'))->printRender();
        echo '</div>';
        echo '<div>';
        $reductiontypeList [] = ['id' => 'discount', 'name' => $this->transE('product.catalogrules.field.reduction_type.0')];
        $reductiontypeList [] = ['id' => 'price', 'name' => $this->transE('product.catalogrules.field.reduction_type.2')];
        (new \EOS\UI\Bootstrap\Select('reduction-type'))
            ->id('reduction-type')
            ->bind($this->data, 'reduction-type')
            ->bindList($reductiontypeList, 'id', 'name')
            ->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.reduction'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('reduction'))->attr('name', 'reduction')->bind($this->data, 'reduction')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.price'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('price'))->attr('name', 'price')->bind($this->data, 'price')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();


/**
 * EDITOR - FINE
 */
$tab->endTab('tab-standard');

$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.idgroup'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id-group'))
            ->attr('class', 'id-group')
            ->bind($this->data, 'id-group')
            ->bindList($this->group, 'id', 'name', true)
            ->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.idcustomer'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id-customer'))
            ->attr('class', 'id-customer')
            ->bind($this->data, 'id-customer')
            ->bindList($this->user, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';

    
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.category'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('category'))
            ->attr('class', 'category')
            ->bind($this->data, 'id_category')
            ->bindList($this->categories, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.catalogrules.field.manufacturer'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('manufacturer'))
            ->attr('class', 'manufacturer')
            ->bind($this->data, 'id_manufacturer')
            ->bindList($this->manufacturer, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

//print_r($this->attributes);
$i = 0;
$count = count($this->attributes);
//print_r($this->attributes);
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    foreach ($this->attributes as $attributegroup)
    {
        echo '<div class="col-12 col-md-6">';
        (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () use ($attributegroup)
        {
            echo '<div class="form-group-title">';
            (new \EOS\UI\Html\Label)->content($attributegroup['name_group'])->printRender();
            echo '</div>';
            echo '<div>';
            (new \EOS\UI\Bootstrap\Select('attribute-' . $attributegroup['id']))
                ->attr('class', 'attribute-' . $attributegroup['id'])
                ->bind($this->data, 'attribute-' . $attributegroup['id'])
                ->bindList($attributegroup['attribute'], 'id', 'name', true)
                ->printRender($this);
            echo "</div>";
            ?>
            <?php
        }, $this)->printRender();
        echo '</div>';
    }
    
    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-data');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);

$this->startCaptureScript();
?>


<script>
    function saveData()
    {
        for (instance in CKEDITOR.instances)
        {
            CKEDITOR.instances[instance].updateElement();
        }
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('Product', ['catalog-rules', 'ajaxsave']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->urlFor('Product', ['catalog-rules', 'index']); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('product.catalogrules.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('Product', ['catalog-rules', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        }
                        else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    $(function ()
    {

        $('input[type="checkbox"]').change(checkboxChanged);

        function checkboxChanged()
        {
            var $this = $(this),
                checked = $this.prop("checked"),
                container = $this.parent(),
                siblings = container.siblings();

            container.find('input[type="checkbox"]')
                .prop({
                    indeterminate: false,
                    checked: checked
                })
                .siblings('label')
                .removeClass('custom-checked custom-unchecked custom-indeterminate')
                .addClass(checked ? 'custom-checked' : 'custom-unchecked');

            checkSiblings(container, checked);
        }

        function checkSiblings($el, checked)
        {
            var parent = $el.parent().parent(),
                all = true,
                indeterminate = false;

            $el.siblings().each(function ()
            {
                return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });

            if (all && checked)
            {
                parent.children('input[type="checkbox"]')
                    .prop({
                        indeterminate: false,
                        checked: checked
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

                checkSiblings(parent, checked);
            }
            else if (all && !checked)
            {
                indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

                parent.children('input[type="checkbox"]')
                    .prop("checked", checked)
                    .prop("indeterminate", indeterminate)
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

                checkSiblings(parent, checked);
            }
            else
            {
                $el.parents("li").children('input[type="checkbox"]')
                    .prop({
                        indeterminate: true,
                        checked: false
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass('custom-indeterminate');
            }
        }
    });


</script>
<?php
$this->endCaptureScript();




/* echo '<ul class="treeview">
        <li>
            <input type="checkbox" name="tall" id="tall">
            <label for="tall" class="custom-unchecked">Tall Things</label>
            
            <ul>
                 <li>
                     <input type="checkbox" name="tall-1" id="tall-1">
                     <label for="tall-1" class="custom-unchecked">Buildings</label>
                 </li>
                 <li>
                     <input type="checkbox" name="tall-2" id="tall-2">
                     <label for="tall-2" class="custom-unchecked">Giants</label>
                     <ul>
                         <li>
                             <input type="checkbox" name="tall-2-1" id="tall-2-1">
                             <label for="tall-2-1" class="custom-unchecked">Andre</label>
                         </li>
                         <li class="last">
                             <input type="checkbox" name="tall-2-2" id="tall-2-2">
                             <label for="tall-2-2" class="custom-unchecked">Paul Bunyan</label>
                         </li>
                     </ul>
                 </li>
                 <li class="last">
                     <input type="checkbox" name="tall-3" id="tall-3">
                     <label for="tall-3" class="custom-unchecked">Two sandwiches</label>
                 </li>
            </ul>
        </li>
        <li class="last">
            <input type="checkbox" name="short" id="short">
            <label for="short" class="custom-unchecked">Short Things</label>
            
            <ul>
                 <li>
                     <input type="checkbox" name="short-1" id="short-1">
                     <label for="short-1" class="custom-unchecked">Smurfs</label>
                 </li>
                 <li>
                     <input type="checkbox" name="short-2" id="short-2">
                     <label for="short-2" class="custom-unchecked">Mushrooms</label>
                 </li>
                 <li class="last">
                     <input type="checkbox" name="short-3" id="short-3">
                     <label for="short-3" class="custom-unchecked">One Sandwich</label>
                 </li>
            </ul>
        </li>
    </ul>
	
	</div>'; */
