<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))->attr('name', 'status')
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
    if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-standard', $this->trans('system.common.standard'));
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-extra', $this->trans('system.common.extra'));
$tab->addItem('tab-seo', $this->trans('system.common.seo'));
$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

/**
 * TITOLO - INIZIO
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.name'))->printRender();
    foreach ($this->dataLangList as $l)
    {
        (new \EOS\UI\Bootstrap\Input('name-' . $l['id']))
            ->attr('class', 'translatable-field lang-' . $l['id'])
            ->bind($this->data, 'name-' . $l['id'])->printRender($this);
    }
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.isbn'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('isbn'))->attr('name', 'isbn')->bind($this->data, 'isbn')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.ean13'))->printRender();
        echo '</div>';
        echo '<div>';
            (new \EOS\UI\Bootstrap\Input('ean13'))->attr('name', 'ean13')->bind($this->data, 'ean13')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.upc'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('upc'))->attr('name', 'upc')->bind($this->data, 'upc')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.ext_code'))->printRender();
        echo '</div>';
        echo '<div>';
            (new \EOS\UI\Bootstrap\Input('ext_code'))->attr('name', 'ext_code')->bind($this->data, 'ext_code')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

/**
 * Description - INIZIO
 */
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.description-short'))->printRender();
    echo '</div>';
    echo '<div>';
    foreach ($this->dataLangList as $l)
    {
        echo '<div class="translatable-field lang-' . $l['id'] . '">';
        (new \EOS\UI\Bootstrap\CKEditor('descr-short-' . $l['id']))
            ->bind($this->data, 'descr-short-' . $l['id'])
            ->bindSourceMode($this->data, 'options-' . $l['id'] . '-editormode')
            ->fileBrowserUrl($this->path->getUrlFor('system', 'filemanager/index'))
            ->printRender($this);
        echo "</div>";
    }
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.description'))->printRender();
    echo '</div>';
    echo '<div>';
    foreach ($this->dataLangList as $l)
    {
        echo '<div class="translatable-field lang-' . $l['id'] . '">';
        (new \EOS\UI\Bootstrap\CKEditor('descr-' . $l['id']))
            ->bind($this->data, 'descr-' . $l['id'])
            ->bindSourceMode($this->data, 'options-' . $l['id'] . '-editormode')
            ->fileBrowserUrl($this->path->getUrlFor('system', 'filemanager/index'))
            ->printRender($this);
        echo "</div>";
    }
    echo '</div>';
}, $this)->printRender();

/**
 * EDITOR - FINE
 */
$tab->endTab('tab-standard');

$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.price'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('price'))->attr('name', 'price')->bind($this->data, 'price')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.width'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('width'))->attr('name', 'width')->bind($this->data, 'width')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.height'))->printRender();
        echo '</div>';
        echo '<div>';
            (new \EOS\UI\Bootstrap\Input('height'))->attr('name', 'height')->bind($this->data, 'height')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.depth'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('depth'))->attr('name', 'depth')->bind($this->data, 'depth')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.weight'))->printRender();
        echo '</div>';
        echo '<div>';
            (new \EOS\UI\Bootstrap\Input('weight'))->attr('name', 'weight')->bind($this->data, 'weight')->printRender($this);
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-data');

$tab->startTab();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.category'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id-category'))
            ->attr('class', 'id-category')
            ->bind($this->data, 'id-category')
            ->bindList($this->categories, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.manufacturer'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('id-manufacturer'))
            ->attr('class', 'id-manufacturer')
            ->bind($this->data, 'id_manufacturer')
            ->bindList($this->manufacturer, 'id', 'name', true)
            ->printRender($this);
        echo "</div>";
        ?>
        <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

$i = 0;
$count = count($this->attributes);
//print_r($this->attributes);
(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    foreach ($this->attributes as $attributegroup)
    {
        echo '<div class="col-12 col-md-6">';
        (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () use ($attributegroup)
        {
            echo '<div class="form-group-title">';
            (new \EOS\UI\Html\Label)->content($attributegroup['name_group'])->printRender();
            echo '</div>';
            echo '<div class="row">';
            echo '<div class="col-12 col-md-6">';
            (new \EOS\UI\Bootstrap\Select('attribute-' . $attributegroup['id']))
                ->attr('class', 'attribute-' . $attributegroup['id'])
                ->bind($this->data, 'attribute-' . $attributegroup['id'])
                ->bindList($attributegroup['attribute'], 'id', 'name', true)
                ->printRender($this);
            echo "</div>";
            echo '<div class="col-12 col-md-6">';
                (new \EOS\UI\Bootstrap\Input('attribute-' . $attributegroup['id'].'-custom'))->attr('name', 'attribute-' . $attributegroup['id'].'-custom')->placeholder($this->transE('product.product.field.attribute.placeholder'))->printRender($this);
            echo '</div>';
            echo "</div>";
            ?>
            <?php
        }, $this)->printRender();
        echo '</div>';
    }
    
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function ()
{
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('product.product.field.image.default'))->printRender();
        echo '</div>';
        echo '<div>';
//        echo '<div class="input-group input-group-sm">';
//        (new \EOS\UI\Bootstrap\Input('img-0'))->attr('name', 'img-0')->bind($this->data, 'url-0')->id('img-0')->printRender($this);
//        echo '<span class="input-group-btn">';
//        (new EOS\UI\Bootstrap\Button())
//            ->attr('class', 'btn-flat')
//            ->contentEncode('..')
//            ->click('function (ev) {ev.preventDefault(); var w = window.open("' .
//                $this->path->getUrlFor('system', 'filemanager/index') .
//                '?field_name=' . 'img-0' . '"); w.parent = window;}')
//            ->printRender($this);
//        echo '</span>';
        //
         $mImage = new \EOS\UI\System\Media\Image(
                    $this->mediaImage, 'media-image-product',
                    $this->path->urlFor('product', ['product', 'ajaximageupload']));
                $mImage->bind($this->data, 'media-image-product');
                $mImage->printRender($this);
        //
        echo '</div>';
        echo '</div>';
        ?>
        <?php
    }, $this)->printRender();

    echo '</div>';
    
    ?>
    <?php
}, $this)->printRender();

$tab->endTab('tab-extra');

$tab->startTab();
$seoW = new \EOS\Components\System\UI\SeoEditorWidget($this);
$seoW->data = $this->data;
$seoW->dataLangDefault = $this->dataLangDefault;
$seoW->dataLangList = $this->dataLangList;
$seoW->write();
$tab->endTab('tab-seo');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);

$this->startCaptureScript();
?>


<script>
    function saveData()
    {
        for (instance in CKEDITOR.instances)
        {
            CKEDITOR.instances[instance].updateElement();
        }
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('Product', ['product', 'ajaxsave']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->urlFor('Product', ['product', 'index']); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('product.product.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('Product', ['product', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        }
                        else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    $(function ()
    {

        $('input[type="checkbox"]').change(checkboxChanged);

        function checkboxChanged()
        {
            var $this = $(this),
                checked = $this.prop("checked"),
                container = $this.parent(),
                siblings = container.siblings();

            container.find('input[type="checkbox"]')
                .prop({
                    indeterminate: false,
                    checked: checked
                })
                .siblings('label')
                .removeClass('custom-checked custom-unchecked custom-indeterminate')
                .addClass(checked ? 'custom-checked' : 'custom-unchecked');

            checkSiblings(container, checked);
        }

        function checkSiblings($el, checked)
        {
            var parent = $el.parent().parent(),
                all = true,
                indeterminate = false;

            $el.siblings().each(function ()
            {
                return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });

            if (all && checked)
            {
                parent.children('input[type="checkbox"]')
                    .prop({
                        indeterminate: false,
                        checked: checked
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

                checkSiblings(parent, checked);
            }
            else if (all && !checked)
            {
                indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

                parent.children('input[type="checkbox"]')
                    .prop("checked", checked)
                    .prop("indeterminate", indeterminate)
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

                checkSiblings(parent, checked);
            }
            else
            {
                $el.parents("li").children('input[type="checkbox"]')
                    .prop({
                        indeterminate: true,
                        checked: false
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass('custom-indeterminate');
            }
        }
    });


</script>
<?php
$this->endCaptureScript();




/* echo '<ul class="treeview">
        <li>
            <input type="checkbox" name="tall" id="tall">
            <label for="tall" class="custom-unchecked">Tall Things</label>
            
            <ul>
                 <li>
                     <input type="checkbox" name="tall-1" id="tall-1">
                     <label for="tall-1" class="custom-unchecked">Buildings</label>
                 </li>
                 <li>
                     <input type="checkbox" name="tall-2" id="tall-2">
                     <label for="tall-2" class="custom-unchecked">Giants</label>
                     <ul>
                         <li>
                             <input type="checkbox" name="tall-2-1" id="tall-2-1">
                             <label for="tall-2-1" class="custom-unchecked">Andre</label>
                         </li>
                         <li class="last">
                             <input type="checkbox" name="tall-2-2" id="tall-2-2">
                             <label for="tall-2-2" class="custom-unchecked">Paul Bunyan</label>
                         </li>
                     </ul>
                 </li>
                 <li class="last">
                     <input type="checkbox" name="tall-3" id="tall-3">
                     <label for="tall-3" class="custom-unchecked">Two sandwiches</label>
                 </li>
            </ul>
        </li>
        <li class="last">
            <input type="checkbox" name="short" id="short">
            <label for="short" class="custom-unchecked">Short Things</label>
            
            <ul>
                 <li>
                     <input type="checkbox" name="short-1" id="short-1">
                     <label for="short-1" class="custom-unchecked">Smurfs</label>
                 </li>
                 <li>
                     <input type="checkbox" name="short-2" id="short-2">
                     <label for="short-2" class="custom-unchecked">Mushrooms</label>
                 </li>
                 <li class="last">
                     <input type="checkbox" name="short-3" id="short-3">
                     <label for="short-3" class="custom-unchecked">One Sandwich</label>
                 </li>
            </ul>
        </li>
    </ul>
	
	</div>'; */
