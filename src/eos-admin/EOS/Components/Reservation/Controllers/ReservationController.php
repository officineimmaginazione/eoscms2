<?php

namespace EOS\Components\Reservation\Controllers;

class ReservationController extends \EOS\Components\System\Classes\AuthController
{

    public function calendar($request, $response, $args)
    {
        $r = new \EOS\Components\Reservation\Models\ReservationModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadCalendar();
        $v->pageTitle = $this->lang->trans('reservation.calendar.title');
        $v->pageSubtitle = $this->lang->trans('reservation.calendar.subtitle');
        //$v->addBreadcrumb($this->path->urlFor('reservation', ['default', 'list']), $this->lang->trans('reservation.common.breadcrumb'), '');
        $v->addBreadcrumb('', $this->lang->trans('reservation.calendar.breadcrumb'), '');
        $v->caldate = $r->getCalendarDate();
        return $v->render('calendar/calendar', true);
    }

    public function setting($request, $response, $args)
    {
        $l = new \EOS\Components\Reservation\Models\LocationModel($this->container);
        $c = new \EOS\Components\Reservation\Models\CategoryModel($this->container);
        $t = new \EOS\Components\Reservation\Models\TypeModel($this->container);
        $tm = new \EOS\Components\Reservation\Models\TimeslotModel($this->container);
        $v = $this->newView($request, $response);
        $v->location = $l->getList();
        $v->category = $c->getList();
        $v->type = $t->getList();
        $v->timeslot = $tm->getList();
        $v->pageTitle = $this->lang->trans('reservation.setting.title');
        $v->pageSubtitle = $this->lang->trans('reservation.setting.subtitle');
        //$v->addBreadcrumb($this->path->urlFor('reservation', ['default', 'list']), $this->lang->trans('reservation.common.breadcrumb'), '');
        $v->addBreadcrumb('', $this->lang->trans('reservation.setting.title'), '');
        return $v->render('setting/default', true);
    }

    public function listReservation($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $this->lang->trans('reservation.list.title');
        $v->pageSubtitle = $this->lang->trans('reservation.list.subtitle');
        //$v->addBreadcrumb($this->path->urlFor('reservation', ['default', 'list']), $this->lang->trans('reservation.common.breadcrumb'), '');
        $v->addBreadcrumb('', $this->lang->trans('reservation.list.breadcrumb'), '');
        return $v->render('default/list', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Reservation\Models\ReservationModel($this->container);
        $l = new \EOS\Components\Reservation\Models\LocationModel($this->container);
        $c = new \EOS\Components\Reservation\Models\CategoryModel($this->container);
        $t = new \EOS\Components\Reservation\Models\TypeModel($this->container);
        $ts = new \EOS\Components\Reservation\Models\TimeslotModel($this->container);
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->location = $l->getList();
        $v->category = $c->getList();
        $v->type = $t->getList();
        $v->timeslot = $ts->getList();
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        $v->pageTitle = $this->lang->trans('reservation.default.edit.title');
        $v->pageSubtitle = $this->lang->trans('reservation.default.edit.subtitle');
        $v->addBreadcrumb($this->path->urlFor('reservation', ['default', 'list']), $this->lang->trans('reservation.common.breadcrumb'), '');
        $v->addBreadcrumb('', $this->lang->trans('reservation.default.edit.breadcrumb'), '');
        return $v->render('default/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $r = new \EOS\Components\Reservation\Models\ReservationModel($this->container);
        $q = $r->getList();
        $dt = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $q, $this->container->get('database'));
        $dt->addColumn('id', 'r.id');
        $dt->addColumn('date_reservation', 'r.date_reservation');
        $dt->addColumn('location', 'rl.name');
        $dt->addColumn('category', 'rcl.name');
        $dt->addColumn('type', 'rtl.name');
        $dt->addColumn('name', 'r.name');
        $dt->addColumn('surname', 'r.surname');
        $dt->addColumn('email', 'r.email');
        $dt->addColumn('phone', 'r.phone');
        $dt->addColumn('notes', 'r.notes');
        $lang = $this->lang;
        $dt->onColumnRender('date_reservation', function ($value, $item) use ($lang)
        {
            if (empty($value))
            {
                return '';
            } else
            {
                return $lang->dbDateToLangDate($value);
            }
        });
        return $response->withHeader('Content-type', 'application/json')->write($dt->toJSON());
    }

    public function ajaxEdit($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $rm = new \EOS\Components\Reservation\Models\ReservationModel($this->container);
                if ($rm->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['default', 'list']);
                } else
                {
                    $result['message'] = $this->lang->trans('reservation.common.error'); //'Invalid email or password';
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Reservation\Models\ReservationModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['default', 'list']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
