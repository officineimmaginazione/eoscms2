<?php

namespace EOS\Components\Reservation\Controllers;

class SpecialdayController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('reservation.specialday.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $m = new \EOS\Components\Reservation\Models\SpecialdayModel($this->container);
        $v->timeSlotList = $m->getTimeSlotList();
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        return $v->render('specialday/default', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Reservation\Models\SpecialdayModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'sp.id');
        $dts->addColumn('special_day', 'sp.special_day');
        $dts->addColumn('timeslot', 'tll.name', true);
        $dts->addColumn('status', 'sp.status', false);
        $dts->onColumnRender('status', function ($value, $item)
        {
            if ($value == 0)
            {
                return $this->lang->trans('reservation.specialday.field.status.close');
            }
            {
                return $this->lang->trans('reservation.specialday.field.status.open');
            }
        });
        $lang = $this->lang;
        $dts->onColumnRender('special_day', function ($value, $item) use ($lang)
        {
            return $lang->dbDateToLangDate($value);
        });
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {

        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Reservation\Models\SpecialdayModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['specialday', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Reservation\Models\SpecialdayModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['specialday', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
