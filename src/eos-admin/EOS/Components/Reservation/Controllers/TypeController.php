<?php

namespace EOS\Components\Reservation\Controllers;

class TypeController extends \EOS\Components\System\Classes\AuthController
{

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Reservation\Models\TypeModel($this->container);
        $ml = new \EOS\Components\Reservation\Models\LocationModel($this->container);
        $mc = new \EOS\Components\Reservation\Models\CategoryModel($this->container);
        $mts = new \EOS\Components\Reservation\Models\TimeslotModel($this->container);
        $v = $this->newView($request, $response);
        $v->location = $ml->getList();
        $v->category = $mc->getList();
        $v->timeslot = $mts->getList();
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        return $v->render('type/edit');
    }
    
    public function ajaxEdit($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Reservation\Models\TypeModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['setting', 'default']);
                } else
                {                    
                    $result['message'] = $this->lang->trans('reservation.common.error');//'Invalid email or password';
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }
    
    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Reservation\Models\TypeModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Reservation', ['setting', 'default']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

}
