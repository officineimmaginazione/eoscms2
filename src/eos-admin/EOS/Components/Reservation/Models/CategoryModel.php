<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

use EOS\System\Util\ArrayHelper;

class CategoryModel extends \EOS\System\Models\Model
{

    public function getList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation_category as rc'))
            ->leftJoin($this->db->tableFix('#__reservation_category_lang as rcl ON rc.id = rcl.id_category'))
            ->select('rcl.name, rcl.description')
            ->where('rcl.id_lang = ?', $this->lang->getCurrentID());
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_category rc'))
            ->select(null)
            ->select('rc.id')
            ->select('rc. availability_customer_limit ')
            ->select('rc.options')
            ->where('rc.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['availability_customer_limit'] = $rc['availability_customer_limit'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__reservation_category_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_category', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                }
            }
            $options = ArrayHelper::fromJSON($rc['options']);
            $res['first_lock_type_available'] = ArrayHelper::getBool($options, 'first_lock_type_available');        
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $options['first_lock_type_available'] = ArrayHelper::getBool($data, 'first_lock_type_available');
        $values = [
            'availability_customer_limit' => \EOS\System\Util\ArrayHelper::getInt($data, 'availability_customer_limit'),
            'options' => ArrayHelper::toJSON($options),
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__reservation_category');
        if ($data['id'] == 0)
        {
            $values['availability_customer_limit'] = \EOS\System\Util\ArrayHelper::getInt($data, 'availability_customer_limit');
            $values['options'] = ArrayHelper::toJSON($options);
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $data);
        }

        return true;
    }

    private function saveDataLang($id_lang, &$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_category_lang');
        $query = $this->db->newFluent()->from($tblContent)->select('id')
            ->where('id_category', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_category' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['title-' . $id_lang],
            'description' => $data['descr-' . $id_lang],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        if ($item == null)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }
    
    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_category');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__reservation_category_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_category', (int) $data['id']);
        $query->execute();
        return true;
    }
 
}
