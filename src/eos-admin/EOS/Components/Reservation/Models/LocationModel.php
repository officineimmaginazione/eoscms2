<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

use EOS\System\Util\ArrayHelper;

class LocationModel extends \EOS\System\Models\Model
{

    public function getLocationRoute($id)
    {
        return 'reservation/booking/index/' . $id;
    }

    public function getList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation_location'));
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_location rl'))
            ->select(null)
            ->select('rl.id')
            ->select('rl.name')
            ->select('rl.address')
            ->select('rl.city')
            ->select('rl.state')
            ->select('rl.country')
            ->select('rl.phone')
            ->select('rl.email')
            ->select('rl.default_location')
            ->select('rl.options')
            ->where('rl.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
            $res['startafterday'] = 0;
            $res['startaftertime'] = '00:00';
        } else
        {

            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['address'] = $rc['address'];
            $res['city'] = $rc['city'];
            $res['state'] = $rc['state'];
            $res['country'] = $rc['country'];
            $res['phone'] = $rc['phone'];
            $res['default_location'] = $rc['default_location'];
            $res['email'] = $rc['email'];
            $options = ArrayHelper::fromJSON($rc['options']);
            $openDays = ArrayHelper::getArray($options, 'opendays');
            foreach ($openDays as $day)
            {
                $res['day-' . $day] = true;
            }
            $res['startafterday'] = ArrayHelper::getInt($options, 'startafterday', 0);
            $res['startaftertime'] = ArrayHelper::getStr($options, 'startaftertime', '00:00');
            $res['mode'] = ArrayHelper::getInt($options, 'mode', 1);
            $res['usecart'] = ArrayHelper::getBool($options, 'usecart');
            $ll = $this->lang->getArrayFromDB(true);
            $router = new \EOS\System\Routing\Manager($this->container);
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $path = $router->getPath($this->getLocationRoute($res['id']), $l['iso_code']);
                $res['url-' . $idlang] = $path;
            }
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__reservation_location');
        $options = [];
        $options['opendays'] = [];
        for ($i = 0; $i < 7; $i++)
        {
            if (isset($data['day-' . $i]))
            {
                $options['opendays'][] = $i;
            }
        }
        $options['mode'] = ArrayHelper::getInt($data, 'mode', 1);
        $options['usecart'] = ArrayHelper::getInt($data, 'usecart') == 1;
        
        $options['startafterday'] = ArrayHelper::getInt($data, 'startafterday', 0);
        $options['startaftertime'] = ArrayHelper::getStr($data, 'startaftertime', '00:00');
        $values['options'] = ArrayHelper::toJSON($options);
        if ($data['id'] == 0)
        {
            $values['name'] = $data['name'];
            $values['address'] = $data['address'];
            $values['city'] = $data['city'];
            $values['state'] = $data['state'];
            $values['country'] = $data['country'];
            $values['phone'] = $data['phone'];
            $values['email'] = $data['email'];
            $values['default_location'] = ArrayHelper::getInt($data, 'default_location', 0);
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['name'] = $data['name'];
            $values['address'] = $data['address'];
            $values['city'] = $data['city'];
            $values['state'] = $data['state'];
            $values['country'] = $data['country'];
            $values['phone'] = $data['phone'];
            $values['email'] = $data['email'];
            $values['default_location'] = ArrayHelper::getInt($data, 'default_location', 0);
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        // Creo un url
        $router = new \EOS\System\Routing\Manager($this->container);
        $url = 'reservation-' . \EOS\System\Util\StringHelper::sanitizeUrl($data['name']) . '-' . $data['id'];
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $idlang = $l['id'];
            $url = ArrayHelper::getStr($data, 'url-' . $idlang);
            if (empty($url))
            {
                $router->deleteRoute($this->getLocationRoute($data['id']), $l['iso_code']);
            } else
            {
                $router->setRoute($this->getLocationRoute($data['id']), $l['iso_code'], $url, 0, 1);
            }
        }
        $this->db->commit();
        return true;
    }

    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_location');
        $this->db->beginTransaction();
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();

        $router = new \EOS\System\Routing\Manager($this->container);
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $router->deleteRoute($this->getLocationRoute($data['id']), $l['iso_code']);
        }
        $this->db->commit();
        return true;
    }

}
