<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

class ReservationModel extends \EOS\System\Models\Model
{

    public function getCalendarDate($from = '1900-01-01 00:00:00', $to = '2100-12-31 23:59:59')
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation r'))
            ->leftJoin($this->db->tableFix('#__reservation_type as rt ON rt.id = r.id_type'))
            ->leftJoin($this->db->tableFix('#__reservation_type_lang as rtl ON rt.id = rtl.id_type'))
            ->select(null)
            ->select('r.name, r.surname, r.date_reservation, r.id_category, rtl.name as type, r.notes, r.arrival')
            ->where('r.date_reservation > ?', $from)
            ->where('r.date_reservation < ?', $to)
            ->where('rtl.id_lang = ?', $this->lang->getCurrentID());
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }

    public function getReservationCount($today = false, $thismonth = false)
    {
        if ($today == true || $thismonth == true)
        {
            $firsttoday = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            $lasttoday = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
            $firstdaymonth = mktime(0, 0, 0, date('m'), 1, date('Y'));
            $lastdaymonth = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
            ($today == true) ? $from = $firsttoday : $from = $firstdaymonth;
            ($today == true) ? $to = $lasttoday : $to = $lastdaymonth;
            $q = $this->db->prepare('select name, surname, date_reservation from #__reservation where date_reservation > :from and date_reservation < :to');
            $q->bindValue(':from', \EOS\System\Routing\PathHelper::removeSlash($from));
            $q->bindValue(':to', \EOS\System\Routing\PathHelper::removeSlash($to));
        } else
        {
            $q = $this->db->prepare('select * from #__reservation');
        }
        $q->execute();
        return $q->rowCount();
    }

    public function getList()
    {
        return $this->db->newFluent()->from($this->db->tableFix('#__reservation as r'))
                ->leftJoin($this->db->tableFix('#__reservation_location as rl ON r.id_location = rl.id'))
                ->leftJoin($this->db->tableFix('#__reservation_category as rc ON r.id_category = rc.id'))
                ->leftJoin($this->db->tableFix('#__reservation_category_lang as rcl ON rc.id = rcl.id_category'))
                ->leftJoin($this->db->tableFix('#__reservation_type as rt ON r.id_type = rt.id'))
                ->leftJoin($this->db->tableFix('#__reservation_type_lang as rtl ON rt.id = rtl.id_type'))
                ->leftJoin($this->db->tableFix('#__reservation_timeslot as rts ON r.id_timeslot = rts.id'))
                ->leftJoin($this->db->tableFix('#__reservation_timeslot_lang as rtsl ON rts.id = rtsl.id_timeslot'))
                ->where('rcl.id_lang = ?', $this->lang->getCurrentID())
                ->where('rtl.id_lang = ?', $this->lang->getCurrentID())
                ->where('rtsl.id_lang = ?', $this->lang->getCurrentID());
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation r'))
            ->select(null)
            ->select('r.id')
            ->select('r.id_location')
            ->select('r.id_category')
            ->select('r.id_type')
            ->select('r.id_timeslot')
            ->select('r.date_reservation')
            ->select('r.name')
            ->select('r.surname')
            ->select('r.email')
            ->select('r.phone')
            ->select('r.company')
            ->select('r.vat')
            ->select('r.notes')
            ->where('r.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['id_location'] = $rc['id_location'];
            $res['id_category'] = $rc['id_category'];
            $res['id_type'] = $rc['id_type'];
            $res['id_timeslot'] = $rc['id_timeslot'];
            $res['date_reservation'] = $this->lang->dbDateToLangDate($rc['date_reservation']);
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['email'] = $rc['email'];
            $res['phone'] = $rc['phone'];
            $res['company'] = $rc['company'];
            $res['vat'] = $rc['vat'];
            $res['notes'] = $rc['notes'];
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $values = [
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__reservation');
        if ($data['id'] == 0)
        {
            $values['id_location'] = $data['location'];
            $values['id_category'] = $data['category'];
            $values['id_type'] = $data['type'];
            $values['id_timeslot'] = $data['timeslot'];
            $values['date_reservation'] = $this->lang->langDateToDbDate($data['date_reservation']);
            $values['name'] = $data['name'];
            $values['surname'] = $data['surname'];
            $values['email'] = $data['email'];
            $values['phone'] = $data['phone'];
            $values['company'] = $data['company'];
            $values['vat'] = $data['vat'];
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $values['id_location'] = $data['location'];
            $values['id_category'] = $data['category'];
            $values['id_type'] = $data['type'];
            $values['id_timeslot'] = $data['timeslot'];
            $values['date_reservation'] = $this->lang->langDateToDbDate($data['date_reservation']);
            $values['name'] = $data['name'];
            $values['surname'] = $data['surname'];
            $values['email'] = $data['email'];
            $values['phone'] = $data['phone'];
            $values['company'] = $data['company'];
            $values['vat'] = $data['vat'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        return true;
    }

    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__reservation');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        return true;
    }

    /* Connessioni con il vecchio DB */

    public function getReservationOld()
    {
        return $this->db->newFluent()
                ->from($this->db->tableFix('#__prenotazioni as p'))
                ->select('p.id', 'p.nome_prenotazione', 'numeropersone_prenotazione', 'ggmmaa_prenotazione', 'data_prenotazione', 'prefisso_prenotazione', 'telefono_prenotazione', 'email_prenotazione', 'richieste_prenotazioni', 'confermata_prenotazione', 'desidero_pernottare', 'ggmmaa_arrivo', 'ggmmaa_partenza', 'numero_adulti', 'numero_camere')
                ->where('ggmmaa_prenotazione > "2016-01-01 00:00:00"');
    }

    public function getCalendarDateOld($from = '1900-01-01 00:00:00', $to = '2100-12-31 23:59:59')
    {
        $q = $this->db->prepare('select nome_prenotazione, ggmmaa_prenotazione, ora_prenotazione, email_prenotazione, telefono_prenotazione, richieste_prenotazioni from #__prenotazioni where ggmmaa_prenotazione > :from and ggmmaa_prenotazione < :to');
        $q->bindValue(':from', \EOS\System\Routing\PathHelper::removeSlash($from));
        $q->bindValue(':to', \EOS\System\Routing\PathHelper::removeSlash($to));
        $q->execute();
        if ($q->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }

}
