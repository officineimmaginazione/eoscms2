<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

class SpecialdayModel extends \EOS\System\Models\Model
{

    public function getListQuery()
    {
        $idlang = $this->lang->getCurrent()->id;
        $f = $this->db->newFluent()->from($this->db->tableFix('#__reservation_special_day sp'))
            ->leftJoin($this->db->tableFix('#__reservation_timeslot tl ON sp.id_timeslot = tl.id'))
            ->leftJoin($this->db->tableFix('#__reservation_timeslot_lang tll ON  tll.id_timeslot = tl.id and tll.id_lang=' . $idlang))
            ->orderBy('sp.special_day DESC, sp.id DESC');
        return $f;
    }

    public function getTimeSlotList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation_timeslot as rt'))
            ->leftJoin($this->db->tableFix('#__reservation_timeslot_lang as rtl ON rt.id = rtl.id_timeslot'))
            ->select(null)->select('rt.id, rtl.name')
            ->where('rtl.id_lang = ?', $this->lang->getCurrent()->id)
            ->orderBy('name');
        $r = $q->fetchAll();
        if (is_null($r))
        {
            return [];
        }
        return $r;
    }

    public function saveData(&$data)
    {
        $dt = $this->lang->langDateToDbDate($data['ins-date']);
        $values = [
            'special_day' => $dt,
            'id_timeslot' => (int) $data['ins-timeslot'],
            'status' => (int) $data['ins-status'],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()'),
            'ins_id' => 0,
            'ins_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__reservation_special_day');
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
        return true;
    }

    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_special_day');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        return true;
    }

}
