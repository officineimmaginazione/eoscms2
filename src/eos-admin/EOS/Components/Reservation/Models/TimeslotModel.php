<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

class TimeslotModel extends \EOS\System\Models\Model
{
     public function getList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation_timeslot as rt'))
            ->leftJoin($this->db->tableFix('#__reservation_timeslot_lang as rtl ON rt.id = rtl.id_timeslot'))
            ->select('rtl.name')
            ->where('rtl.id_lang = ?', $this->lang->getCurrentID());
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        } else
        {
            return null;
        }
    }
    
    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_timeslot rt'))
            ->select(null)
            ->select('rt.id')
            ->select('rt.start')
            ->select('rt.finish')
            ->where('rt.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['start'] = $rc['start'];
            $res['finish'] = $rc['finish'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__reservation_timeslot_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_timeslot', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                }
            }
        }
        return $res;
    }
    
    public function saveData(&$data)
    {
        $values = [
            'start' => $data['start'],
            'finish' => $data['finish'],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblContent = $this->db->tableFix('#__reservation_timeslot');
        if ($data['id'] == 0)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $data);
        }

        return true;
    }

    private function saveDataLang($id_lang, &$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_timeslot_lang');
        $query = $this->db->newFluent()->from($tblContent)->select('id')
            ->where('id_timeslot', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_timeslot' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['title-' . $id_lang],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        if ($item == null)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }
    
        public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_timeslot');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__reservation_timeslot_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_timeslot', (int) $data['id']);
        $query->execute();
        return true;
    }
    
}