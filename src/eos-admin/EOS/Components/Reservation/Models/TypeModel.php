<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

use EOS\System\Util\ArrayHelper;

class TypeModel extends \EOS\System\Models\Model
{

    public function getList()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__reservation_type as rt'))
            ->leftJoin($this->db->tableFix('#__reservation_type_lang as rtl ON rt.id = rtl.id_type'))
            ->select('rtl.name, rtl.description')
            ->where('rtl.id_lang = ?', $this->lang->getCurrentID());
        $q->execute();
        if ($q->execute()->rowCount() > 0)
        {
            return $q;
        }
        else
        {
            return null;
        }
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_type rt'))
            ->select(null)
            ->select('rt.id')
            ->select('rt.id_location')
            ->select('rt.id_category')
            ->select('rt.availability_total')
            ->select('rt.availability_customer')
            ->select('rt.price_reservation')
            ->select('rt.status')
            ->select('rt.options')
            ->where('rt.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        }
        else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['id_location'] = $rc['id_location'];
            $res['id_category'] = $rc['id_category'];
            $res['availability_total'] = $rc['availability_total'];
            $res['availability_customer'] = $rc['availability_customer'];
            $res['price_reservation'] = $rc['price_reservation'];
            $res['status'] = $rc['status'];
            $options = json_decode($rc['options'], true);
            $res['deposit_percent'] = $options['deposit_percent'];
            $res['deposit_day'] = $options['deposit_day'];
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__reservation_type_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_type', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                }
            }
            $tblTimeslot = $this->db->tableFix('#__reservation_type_timeslot');
            $query = $this->db->newFluent()->from($tblTimeslot)->select('id_type, id_timeslot')
                ->where('id_type', $rc['id']);
            $rcts = $query->fetchAll();
            if ($rcts != null)
            {
                foreach ($rcts as $timeslot)
                {
                    $res['timeslot-' . $timeslot['id_timeslot']] = $timeslot['id_timeslot'];
                }
            }
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $options['deposit_percent'] = ArrayHelper::getStr($data, 'deposit_percent');
        $options['deposit_day'] = ArrayHelper::getStr($data, 'deposit_day');
        $values = [
            'id_location' => $data['location'],
            'id_category' => $data['category'],
            'availability_total' => $data['availability_total'],
            'availability_customer' => $data['availability_customer'],
            'price_reservation' => $data['price_reservation'],
            'status' => $data['status'],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()'),
            'options' => json_encode($options)
        ];
        $tblContent = $this->db->tableFix('#__reservation_type');
        if ($data['id'] == 0)
        {
            $values['id_location'] = $data['location'];
            $values['id_category'] = $data['category'];
            $values['availability_total'] = $data['availability_total'];
            $values['availability_customer'] = $data['availability_customer'];
            $values['price_reservation'] = $data['price_reservation'];
            $values['status'] = $data['status'];
            $values['options'] = json_encode($options);
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        }
        else
        {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0)
        {
            $data['id'] = $this->db->lastInsertId();
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l)
        {
            $this->saveDataLang($l['id'], $data);
        }
        $this->saveDataTimeslot($data);
        return true;
    }

    private function saveDataLang($id_lang, &$data)
    {
        $tblContent = $this->db->tableFix('#__reservation_type_lang');
        $query = $this->db->newFluent()->from($tblContent)->select('id')
            ->where('id_type', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $values = [
            'id_type' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['title-' . $id_lang],
            'description' => $data['descr-' . $id_lang],
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        if ($item == null)
        {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        }
        else
        {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
    }

    private function saveDataTimeslot(&$data)
    {
        $tblTypeTimeslot = $this->db->tableFix('#__reservation_type_timeslot');
        $query = $this->db->newFluent()->deleteFrom($tblTypeTimeslot)->where('id_type', (int) $data['id']);
        $query->execute();
        $tsl = $this->getTimeslotList();
        foreach ($tsl as $ts)
        {
            if(isset($data['timeslot-'.$ts['id']])) {
            $values = [
                'id_type' => $data['id'],
                'id_timeslot' => $ts['id'],
                'up_id' => 0,
                'up_date' => new \FluentLiteral('NOW()'),
                'ins_id' => 0,
                'ins_date' => new \FluentLiteral('NOW()')
            ];
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblTypeTimeslot, $values);
            $query->execute();
            }
        }
    }

    public function deleteData(&$data)
    {
        $tblType = $this->db->tableFix('#__reservation_type');
        $query = $this->db->newFluent()->deleteFrom($tblType)->where('id', (int) $data['id']);
        $query->execute();
        $tblType = $this->db->tableFix('#__reservation_type_lang');
        $query = $this->db->newFluent()->deleteFrom($tblType)->where('id_type', (int) $data['id']);
        $query->execute();
        return true;
    }

    public function getTimeslotList()
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_timeslot ts'))
            ->select(null)
            ->select('ts.id');
        $rc = $query->fetchAll();
        if ($rc == null)
        {
            $res = [];
        }
        else
        {
            $res = $rc;
        }
        return $res;
    }

}
