<?php

return
    [
        'reservation.calendar.title' => 'Calendario',
        'reservation.calendar.subtitle' => 'Prenotazioni',
        'reservation.calendar.breadcrumb' => 'Calendario'
];