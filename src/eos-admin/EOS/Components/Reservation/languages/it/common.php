<?php

return
    [
        'reservation.common.breadcrumb' => 'Prenotazioni',
        'reservation.common.title' => 'Prenotazioni',
        'reservation.common.active' => 'Attivo',
        'reservation.common.deactive' => 'Disattivo',
];