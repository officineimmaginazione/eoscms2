<?php

return
    [
        'reservation.default.edit.title' => 'Modifica prenotazione',
        'reservation.default.edit.subtitle' => '',
        'reservation.default.edit.breadcrumb' => 'Modifica prenotazione',
        'reservation.default.field.location' => 'Sede',
        'reservation.default.field.category' => 'Categoria',
        'reservation.default.field.type' => 'Tipo',
        'reservation.default.field.timeslot' => 'Orario',
        'reservation.default.field.name' => 'Nome',
        'reservation.default.field.surname' => 'Cognome',
        'reservation.default.field.email' => 'eMail',
        'reservation.default.field.phone' => 'Telefono',
        'reservation.default.field.company' => 'Azienda',
        'reservation.default.field.vat' => 'Partita Iva',
        'reservation.default.field.notes' => 'Note',
        'reservation.default.field.date' => 'Data prenotazione'
        
];
