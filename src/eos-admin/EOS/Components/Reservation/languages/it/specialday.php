<?php

return
    [
        'reservation.specialday.index.title' => 'Giorni speciali',
        'reservation.specialday.field.specialday' => 'Giorno',
        'reservation.specialday.field.timeslot' => 'Tipo',
        'reservation.specialday.field.status' => 'Stato',
        'reservation.specialday.field.status.open' => 'Aperto',
        'reservation.specialday.field.status.close' => 'Chiuso',
        'reservation.specialday.delete.confirm' => 'Vuoi eliminare il giorno speciale?',
        'reservation.specialday.add.confirm' => 'Vuoi aggiungere il giorno speciale?'
];
