<?php
if ($this->componentIsActive('reservation'))
{
    $this->addParent('reservation', 'Prenotazioni', '', 'fa fa-calendar', 10);
    $this->addItem('reservation', 'calendar', 'Calendario', $this->view->path->urlFor('reservation', ['calendar', 'calendar']), 'fa fa-circle-o', null, 'reservation.manage');
    $this->addItem('reservation', 'list', 'Lista', $this->view->path->urlFor('reservation', ['default', 'list']), 'fa fa-circle-o', null, 'reservation.manage');
    $this->addItem('reservation', 'setting', 'Impostazioni', $this->view->path->urlFor('reservation', ['setting', 'default']), 'fa fa-circle-o', null, 'reservation.setting');
    $this->addItem('reservation', 'setting', 'Giorni speciali', $this->view->path->urlFor('reservation', ['specialday', 'index']), 'fa fa-circle-o', null, 'reservation.setting');
}