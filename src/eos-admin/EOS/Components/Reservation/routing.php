<?php

use \EOS\Components\System\Classes\AuthRole;

/* Calendar */
$this->mapComponent(['GET'], 'Reservation', ['calendar/calendar' => 'ReservationController:calendar'], ['auth' => ['reservation.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);

/* Reservation */
$this->mapComponent(['GET'], 'Reservation', ['default/list' => 'ReservationController:listreservation'], ['auth' => ['reservation.manage' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Reservation', ['default/edit/{id}' => 'ReservationController:edit'], ['auth' => ['reservation.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['ajaxlist' => 'ReservationController:ajaxlist'], ['auth' => ['reservation.manage' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Reservation', ['ajaxedit' => 'ReservationController:ajaxedit'], ['auth' => ['reservation.manage' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['ajaxdelete' => 'ReservationController:ajaxDelete'], ['auth' => ['reservation.manage' => [AuthRole::DELETE]]]);

/* Setting */
$this->mapComponent(['GET'], 'Reservation', ['setting/default' => 'ReservationController:setting'], ['auth' => ['reservation.setting' => [AuthRole::READ]]]);

/* Setting - Location */
$this->mapComponent(['GET'], 'Reservation', ['location/edit/{id}' => 'LocationController:edit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['location/ajaxedit' => 'LocationController:ajaxEdit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['location/ajaxdelete' => 'LocationController:ajaxDelete'], ['auth' => ['reservation.setting' => [AuthRole::DELETE]]]);

/* Setting - Category */
$this->mapComponent(['GET'], 'Reservation', ['category/edit/{id}' => 'CategoryController:edit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['category/ajaxedit' => 'CategoryController:ajaxEdit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['category/ajaxdelete' => 'CategoryController:ajaxDelete'], ['auth' => ['reservation.setting' => [AuthRole::DELETE]]]);

/* Setting - Type */
$this->mapComponent(['GET'], 'Reservation', ['type/edit/{id}' => 'TypeController:edit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['type/ajaxedit' => 'TypeController:ajaxEdit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['type/ajaxdelete' => 'TypeController:ajaxDelete'], ['auth' => ['reservation.setting' => [AuthRole::DELETE]]]);

/* Setting - Timeslot */
$this->mapComponent(['GET'], 'Reservation', ['timeslot/edit/{id}' => 'TimeslotController:edit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['timeslot/ajaxedit' => 'TimeslotController:ajaxEdit'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['timeslot/ajaxdelete' => 'TimeslotController:ajaxDelete'], ['auth' => ['reservation.setting' => [AuthRole::DELETE]]]);

/* Special Day */
$this->mapComponent(['GET'], 'Reservation', ['specialday/index' => 'SpecialdayController:index'], ['auth' => ['reservation.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Reservation', ['specialday/ajaxlist' => 'SpecialdayController:ajaxlist'], ['auth' => ['reservation.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Reservation', ['specialday/ajaxsave' => 'SpecialdayController:ajaxSave'], ['auth' => ['reservation.setting' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Reservation', ['specialday/ajaxdelete' => 'SpecialdayController:ajaxDelete'], ['auth' => ['reservation.setting' => [AuthRole::DELETE]]]);
