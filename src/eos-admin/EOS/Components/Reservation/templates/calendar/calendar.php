<?php \EOS\UI\Loader\Library::load($this, 'FullCalendar'); ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php $this->startCaptureScript(); 
if($this->caldate != null) {
?>
<script>
    $(function ()
    {
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele)
        {
            ele.each(function ()
            {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }
        ini_events($('#external-events div.external-event'));
        
        var lang = '<?php echo $this->lang->getCurrentISO(); ?>';
        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
        $('#calendar').fullCalendar({
            lang: lang,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'Oggi',
                month: 'Mese',
                week: 'Settimana',
                day: 'Giorno'
            },
            //Random default events
            events: [
                <?php 
                foreach($this->caldate as $reservation) {
                    echo "{";
                    echo "title: '".str_replace("'", "\'", $reservation['name'])." ".str_replace("'", "\'", $reservation['surname']) ."',";
                    echo "description: '".$reservation['type'].'\n'.preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($reservation['notes']))."',";
                    if(isset($reservation['arrival']) && $reservation['arrival'] != '') {
                        $arrival = $reservation['arrival'];
                        $depart = $reservation['arrival'];
                    }
                    else {
                        $arrival = '08:00';
                        $depart = '20:00';
                    }
                    echo "start: '".date('Y', strtotime($reservation['date_reservation']))."-".date('m', strtotime($reservation['date_reservation']))."-".date('d', strtotime($reservation['date_reservation']))."T".$arrival.":00"."',";
                    echo "end: '".date('Y', strtotime($reservation['date_reservation']))."-".date('m', strtotime($reservation['date_reservation']))."-".date('d', strtotime($reservation['date_reservation']))."T".$depart.":00"."',";
                    if ($reservation['id_category']==1) echo 'backgroundColor: "#f56954",';
                    else echo 'backgroundColor: "#57bfd9",';
                    echo 'borderColor: "#ddd"';
                    if (next($reservation)==true) echo '},';
                    else echo '}';
                }
                ?>
            ],
            eventRender: function(event, element) { 
            element.find('.fc-title').append("<br/>" + event.description); 
        },
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay)
            { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked'))
                {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

    });
</script>
<?php
}
$this->endCaptureScript();


