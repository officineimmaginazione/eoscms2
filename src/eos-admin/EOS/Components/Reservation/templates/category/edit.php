<div class="row">
    <div class="col-xs-12">
        <?php
        $form = new EOS\UI\Html\Form('edit');
        $form->id('edit');
        $form->attr('method', 'POST');
        $form->attr('class', 'form-horizontal');
        $form->startContent();
        $box = new EOS\UI\Bootstrap\Box();
        $box->startHeader();
        $box->endHeader();
        $box->startContent();
        (new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.name'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            foreach ($this->dataLangList as $l)
            {
                (new \EOS\UI\Bootstrap\Input('title-' . $l['id']))->attr('class', 'translatable-field lang-' . $l['id'])->bind($this->data, 'name-'.$l['id'])->printRender($this);
            }
            echo '</div>';
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\DropDownMenu('lng2');
            $sel->attr('class', 'select-lang');
            $sel->onChange('function(item) {changeLanguage(item)}');
            $sel->bind($this->dataLangDefault, 'id');
            $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.descr'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            foreach ($this->dataLangList as $l)
            {
                (new \EOS\UI\Bootstrap\Textarea('descr-' . $l['id']))->attr('class', 'translatable-field lang-' . $l['id'])->bind($this->data, 'descr-'.$l['id'])->printRender($this);
            }
            echo '</div>';
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\DropDownMenu('dr-ta');
            $sel->attr('class', 'select-lang');
            $sel->onChange('function(item) {changeLanguage(item)}');
            $sel->bind($this->dataLangDefault, 'id');
            $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.availabilitycustomerlimit'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            (new \EOS\UI\Bootstrap\Input('availability_customer_limit'))->bind($this->data, 'availability_customer_limit')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.firstlocktypeavailable'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="first_lock_type_available" value="1" ' . (\EOS\System\Util\ArrayHelper::getInt($this->data, 'first_lock_type_available', 0) != 0 ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.default.checkbox') . '</label></div>';
            echo '</div>';
        }, $this)->printRender();
        
        $box->endContent();
        $box->startFooter();
        {
            (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
        }
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();  }')->printRender($this);
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();  }')->printRender($this);
        $box->endFooter();
        $box->printRender($this);
        $this->writeTokenHtml();
        $form->endContent();
        $form->printRender($this);
        ?>
    </div><!-- /.col -->
</div><!-- /.row -->
<?php 
$this->startCaptureScript(); 
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('reservation', ['category', 'ajaxedit']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;

    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('Reservation', 'setting/default'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('reservation.setting.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('Reservation', 'category/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();

