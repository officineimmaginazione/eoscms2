<div class="row">
    <div class="col-xs-12">
        <?php
        $form = new EOS\UI\Html\Form('edit-reservation');
        $form->id('edit');
        $form->attr('method', 'POST');
        $form->attr('class', 'form-horizontal');
        $form->startContent();
        $box = new EOS\UI\Bootstrap\Box();
        $box->startHeader();
        $box->endHeader();
        $box->startContent();
        (new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.location'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            $sel = new EOS\UI\Bootstrap\Select('location');
            foreach ($this->location as $location)
            {
                if ($this->data != null && $location['id'] == $this->data['id_location'])
                    $sel->addOption($location['id'], $location['name'], true);
                else
                    $sel->addOption($location['id'], $location['name']);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.category'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            $sel = new EOS\UI\Bootstrap\Select('category');
            foreach ($this->category as $category)
            {
                if ($this->data != null && $category['id'] == $this->data['id_category'])
                    $sel->addOption($category['id'], $category['name'], true);
                else
                    $sel->addOption($category['id'], $category['name']);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.type'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            $sel = new EOS\UI\Bootstrap\Select('type');
            foreach ($this->type as $type)
            {
                if ($this->data != null && $type['id'] == $this->data['id_type'])
                    $sel->addOption($type['id'], $type['name'], true);
                else
                    $sel->addOption($type['id'], $type['name']);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.timeslot'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            $sel = new EOS\UI\Bootstrap\Select('timeslot');
            foreach ($this->timeslot as $timeslot)
            {
                if ($this->data != null && $timeslot['id'] == $this->data['id_type'])
                    $sel->addOption($timeslot['id'], $timeslot['name'], true);
                else
                    $sel->addOption($timeslot['id'], $timeslot['name']);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.date'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\DatePicker('date_reservation'))->attr('name', 'date_reservation')->bind($this->data, 'date_reservation')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.name'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('name'))->attr('name', 'name')->bind($this->data, 'name')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.surname'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('surname'))->attr('name', 'surname')->bind($this->data, 'surname')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.email'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('email'))->attr('name', 'email')->bind($this->data, 'email')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.phone'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('phone'))->attr('name', 'phone')->bind($this->data, 'phone')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.company'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('company'))->attr('name', 'company')->bind($this->data, 'company')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.vat'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Input('vat'))->attr('name', 'vat')->bind($this->data, 'vat')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.default.field.notes'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-3">';
            (new \EOS\UI\Bootstrap\Textarea('notes'))->attr('name', 'notes')->bind($this->data, 'notes')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        $box->endContent();
        $box->startFooter();
        {
            (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
        }
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();  }')->printRender($this);
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos  pull-right')->click('function (e) {e.preventDefault(); cancelData();  }')->printRender($this);
        $box->endFooter();
        $box->printRender($this);
        $this->writeTokenHtml();
        $form->endContent();
        $form->printRender($this);
        ?>
    </div><!-- /.col -->
</div><!-- /.row -->
<?php 
$this->startCaptureScript(); 
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('reservation', ['', 'ajaxedit']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;

    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('Reservation', 'default/list'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('reservation.setting.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('reservation', ['', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();

