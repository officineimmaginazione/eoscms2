<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function ()
{
	$title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
	(new \EOS\UI\Bootstrap\Heading1('content-header-title'))
	->content($title)
	->printRender($this);
	(new \EOS\UI\Bootstrap\Button('btn-new'))
	->attr('class', ' btn-success ml-auto')
	->click('function (e) { addNew();}')
	->content('Crea')
	->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('reservation', ['', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', '#');
$tbl->addColumn('date_reservation', 'Data');
$tbl->addColumn('location', 'Sede');
$tbl->addColumn('category', 'Categoria');
$tbl->addColumn('type', 'Tipo');
$tbl->addColumn('name', 'Nome');
$tbl->addColumn('surname', 'Cognome');
$tbl->addColumn('email', 'eMail');
$tbl->addColumn('phone', 'Telefono');
$tbl->addColumn('notes', 'Note');
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('reservation', ['default', 'edit', '0']) ?>";
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->urlFor('reservation', ['default', 'edit']) ?>" + id + "/";
    }
</script>
<?php
$this->endCaptureScript();
