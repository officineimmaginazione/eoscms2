<div class="row">
    <div class="col-xs-12">
        <?php
        $form = new EOS\UI\Html\Form('insert-location');
        $form->id('edit');
        $form->attr('method', 'POST');
        $form->attr('class', 'form-horizontal');
        $form->startContent();
        $box = new EOS\UI\Bootstrap\Box();
        $box->startHeader();
        $box->endHeader();
        $box->startContent();
        (new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.name'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            (new \EOS\UI\Bootstrap\Input('name'))->bind($this->data, 'name')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.address'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            (new \EOS\UI\Bootstrap\Input('address'))->bind($this->data, 'address')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.city'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('city'))->bind($this->data, 'city')->printRender();
            echo '</div>';
        }, $this)->printRender();

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.state'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('state'))->bind($this->data, 'state')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.country'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('country'))->bind($this->data, 'country')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.phone'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('phone'))->bind($this->data, 'phone')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.email'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-4">';
            (new \EOS\UI\Bootstrap\Input('email'))->bind($this->data, 'email')->printRender();
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.default'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="default_location" value="1" ' . (\EOS\System\Util\ArrayHelper::getInt($this->data, 'default_location', 0) != 0 ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.default.checkbox') . '</label></div>';
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.dayopen'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-1" value="1" ' . (isset($this->data['day-1']) ? 'checked' : '') . '>' . $this->transE('reservation.setting.field.location.dayopen.monday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-2" value="2" ' . (isset($this->data['day-2']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.tuesday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-3" value="3" ' . (isset($this->data['day-3']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.wednesday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-4" value="4" ' . (isset($this->data['day-4']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.thursday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-5" value="5" ' . (isset($this->data['day-5']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.friday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-6" value="6" ' . (isset($this->data['day-6']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.saturday') . '</label></div>';
            echo '</div>';
            echo '<div class="col-xs-1">';
            echo '<div class="checkbox"><label><input type="checkbox" name="day-0" value="0" ' . (isset($this->data['day-0']) ? 'checked' : '') . '> ' . $this->transE('reservation.setting.field.location.dayopen.sunday') . '</label></div>';
            echo '</div>';
        }, $this)->printRender();

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.mode'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\Select('mode');
            $selMode = \EOS\System\Util\ArrayHelper::getInt($this->data, 'mode');
            for ($i = 1; $i <= 4; $i++)
            {
                $sel->addOption($i, $this->transE('reservation.setting.field.location.mode' . $i), $selMode == $i);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.usecart'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\Select('usecart');
            $selCart = \EOS\System\Util\ArrayHelper::getInt($this->data, 'usecart');
            $sel->addOption(0, $this->transE('reservation.common.deactive'), $selCart);
            $sel->addOption(1, $this->transE('reservation.common.active'), $selCart);
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.url'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            foreach ($this->dataLangList as $l)
            {
                $inpUrl = (new \EOS\UI\Bootstrap\Input('url-' . $l['id']))
                    ->attr('class', 'translatable-field lang-' . $l['id'])
                    ->bind($this->data, 'url-' . $l['id']);
                $inpUrl->printRender($this);
            }
            echo '</div>';
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\DropDownMenu('lng-url');
            $sel->attr('class', 'select-lang');
            $sel->onChange('function(item) {changeLanguage(item)}');
            $sel->bind($this->dataLangDefault, 'id');
            $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender($this);

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.location.startafterdate'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2 ">';
            (new \EOS\UI\Bootstrap\Input('startafterday'))->attr('name', 'startafterday')->bind($this->data, 'startafterday')->attr('type', 'number')->printRender($this);
            echo '</div>';
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\TimePicker('startaftertime'))->bind($this->data, 'startaftertime')->attr('class', 'timepicker')->printRender($this);
            echo '</div>';
        }, $this)->printRender();

        $box->endContent();
        $box->startFooter();
        {
            (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
        }
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();  }')->printRender($this);
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();  }')->printRender($this);
        $box->endFooter();
        $box->printRender($this);
        $this->writeTokenHtml();
        $form->endContent();
        $form->printRender($this);
        ?>
    </div><!-- /.col -->
</div><!-- /.row -->
<?php
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('reservation', ['location', 'ajaxedit']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;

    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('Reservation', 'setting/default'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('reservation.setting.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('Reservation', 'location/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();



