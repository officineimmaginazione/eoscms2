<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Location/Sedi</h3>
                <div class="pull-right box-tools">
                    <a class="btn btn-info btn-sm" href="<?php echo $this->path->urlFor('reservation', ['location', 'edit']) . "0" ?>" title="Add"><i class="fa fa-plus"></i></a>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nome</th>
                        <th>Indirizzo</th>
                        <th>Città</th>
                        <th>eMail</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                    if (count($this->location))
                    {
                        foreach ($this->location as $location)
                        {
                            echo "<tr>";
                            echo "<td>" . $location['id'] . "</td>";
                            echo "<td>" . $location['name'] . "</td>";
                            echo "<td>" . $location['address'] . "</td>";
                            echo "<td>" . $location['city'] . "</td>";
                            echo "<td>" . $location['email'] . "</td>";
                            echo "<td class='text-right'><a href='" . $this->path->urlFor('reservation', ['location', 'edit']) . $location['id'] . "' class='btn btn-info btn-sm' id='btn-new'><i class='fa fa-edit'></i></a></td>";
                            echo "<tr>";
                        }
                    }
                    ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Categoria</h3>
                <div class="pull-right box-tools">
                    <a class="btn btn-info btn-sm" href="<?php echo $this->path->urlFor('reservation', ['category', 'edit']) . "0" ?>" title="Add"><i class="fa fa-plus"></i></a>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Categoria</th>
                        <th>Descrizione</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                    if (count($this->category))
                    {
                        foreach ($this->category as $category)
                        {
                            echo "<tr>";
                            echo "<td>" . $category['id'] . "</td>";
                            echo "<td>" . $category['name'] . "</td>";
                            echo "<td>" . $category['description'] . "</td>";
                            echo "<td class='text-right'><a href='" . $this->path->urlFor('reservation', ['category', 'edit']) . $category['id'] . "' class='btn btn-info btn-sm' id='btn-new'><i class='fa fa-edit'></i></a></td>";
                            echo "<tr>";
                        }
                    }
                    ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Orari</h3>
                <div class="pull-right box-tools">
                    <a class="btn btn-info btn-sm" href="<?php echo $this->path->urlFor('reservation', ['timeslot', 'edit']) . "0" ?>" title="Add"><i class="fa fa-plus"></i></a>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Fasce orarie</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                    if (count($this->timeslot))
                    {
                        foreach ($this->timeslot as $timeslot)
                        {
                            echo "<tr>";
                            echo "<td>" . $timeslot['id'] . "</td>";
                            echo "<td>" . $timeslot['name'] . "</td>";
                            echo "<td class='text-right'><a href='" . $this->path->urlFor('reservation', ['timeslot', 'edit']) . $timeslot['id'] . "' class='btn btn-info btn-sm' id='btn-new'><i class='fa fa-edit'></i></a></td>";
                            echo "<tr>";
                        }
                    }
                    ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tipologia</h3>
                <div class="pull-right box-tools">
                    <a class="btn btn-info btn-sm" href="<?php echo $this->path->urlFor('reservation', ['type', 'edit']) . "0" ?>" title="Add"><i class="fa fa-plus"></i></a>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Tipo</th>
                        <th>Descrizione</th>
                        <th>Stato</th>
                        <th>Disponibilità<br />Totali</th>
                        <th>Disponibilità<br />Online</th>
                        <th>Prezzo</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                    if (!empty($this->type))
                    {
                        foreach ($this->type as $type)
                        {
                            echo "<tr>";
                            echo "<td>" . $type['id'] . "</td>";
                            echo "<td>" . $type['name'] . "</td>";
                            echo "<td>" . $type['description'] . "</td>";
                            echo "<td>" . ($type['status'] == 1 ? "<span class='label label-success'>Attivo</span>" : "<span class='label label-danger'>Disattivo</span>") . "</td>";
                            echo "<td>" . $type['availability_total'] . "</td>";
                            echo "<td>" . $type['availability_customer'] . "</td>";
                            echo "<td>" . $type['price_reservation'] . "</td>";
                            echo "<td class='text-right'><a href='" . $this->path->urlFor('reservation', ['type', 'edit']) . $type['id'] . "' class='btn btn-info btn-sm' id='btn-new'><i class='fa fa-edit'></i></a></td>";
                            echo "<tr>";
                        }
                    }
                    ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</section><!-- /.content -->

