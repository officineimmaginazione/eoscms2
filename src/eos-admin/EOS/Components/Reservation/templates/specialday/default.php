<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startContent();
$form = new EOS\UI\Html\Form();
$form->id('ins-form');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();

echo '<div class="col-xs-2">';
$dt = new \EOS\UI\Bootstrap\DatePicker('ins-date');
$dt->id('ins-date');
$dt->value((new DateTime())->format($this->lang->getCurrent()->dateformat));
$dt->printRender($this);
echo '</div>';
echo '<div class="col-xs-2">';
(new \EOS\UI\Bootstrap\Select('ins-timeslot'))
    ->id('ins-timeslot')
    ->bindList($this->timeSlotList, 'id', 'name')
    ->printRender($this);
echo '</div>';
echo '<div class="col-xs-2">';
$tp = [];
$tp[] = ['id' => 0, 'descr' => $this->trans('reservation.specialday.field.status.close')];
$tp[] = ['id' => 1, 'descr' => $this->trans('reservation.specialday.field.status.open')];
(new \EOS\UI\Bootstrap\Select('ins-status'))
    ->id('ins-status')
    ->bindList($tp, 'id', 'descr')
    ->printRender($this);
echo '</div>';
echo '<div class="col-xs-1">';
(new \EOS\UI\Bootstrap\Button('btn-new'))
    ->attr('class', ' btn-info btn-sm')
    ->click('function (e) {e.preventDefault(); addNew();}')
    ->content('<i class="fa fa-plus"></i>')
    ->printRender($this);
echo '</div>';
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$box->endContent();
$box->printRender($this);

$box = new EOS\UI\Bootstrap\Box();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Reservation', ['specialday', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', '', false, false, false);
$tbl->addColumn('special_day', $this->transE('reservation.specialday.field.specialday'));
$tbl->addColumn('timeslot', $this->transE('reservation.specialday.field.timeslot'));
$tbl->addColumn('status', $this->transE('reservation.specialday.field.status'));
$tbl->addColumnCustom('', 'del-row', '<button class="btn  btn-danger btn-sm" id="btn-new"><i class="fa fa-trash"></i></button>');
$tbl->clickRow('function (e, r) {e.stopPropagation(); delRow(r.data().id);}', 'del-row');
$tbl->addRawParams('bFilter', 'false');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        var msg = "<?php $this->transPE('reservation.specialday.add.confirm'); ?>";
        msg = msg + '<br>'+ $('#ins-date').val() + ' - ' + $('#ins-timeslot option:selected').html();
        msg = msg + ' - ' + $('#ins-status option:selected').html();
        bootbox.confirm(msg, function (result)
        {
            if (result)
            {
                var data = $('#ins-form').serializeFormJSON();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('Reservation', ['specialday', 'ajaxsave']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            $('#datatable').DataTable().ajax.reload();
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function delRow(id)
    {
        bootbox.confirm("<?php $this->transPE('reservation.specialday.delete.confirm'); ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('Reservation', ['specialday', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": id}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            $('#datatable').DataTable().ajax.reload();
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

</script>
<?php
$this->endCaptureScript();
