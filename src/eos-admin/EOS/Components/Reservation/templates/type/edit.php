<div class="row">
    <div class="col-xs-12">
        <?php
        $form = new EOS\UI\Html\Form('insert-location');
        $form->id('edit');
        $form->attr('method', 'POST');
        $form->attr('class', 'form-horizontal');
        $form->startContent();
        $box = new EOS\UI\Bootstrap\Box();
        $box->startHeader();
        $box->endHeader();
        $box->startContent();
        (new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.location'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            $sel = new EOS\UI\Bootstrap\Select('location');
            foreach ($this->location as $location)
            {
                if ($this->data != null && $location['id'] == $this->data['id_location'])
                    $sel->addOption($location['id'], $location['name'], true);
                else
                    $sel->addOption($location['id'], $location['name']);
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.category'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            $sel = new EOS\UI\Bootstrap\Select('category');
            if (!empty($this->category))
            {
                foreach ($this->category as $category)
                {
                    if ($this->data != null && $category['id'] == $this->data['id_category'])
                        $sel->addOption($category['id'], $category['name'], true);
                    else
                        $sel->addOption($category['id'], $category['name']);
                }
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.status'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\Select('status');
            if ($this->data != null && $this->data['status'] == 1)
            {
                $sel->addOption(0, $this->transE('reservation.common.deactive'));
                $sel->addOption(1, $this->transE('reservation.common.active'), true);
            } else
            {
                $sel->addOption(0, $this->transE('reservation.common.deactive'), true);
                $sel->addOption(1, $this->transE('reservation.common.active'));
            }
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.availabilitytotal'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('availability_total'))->attr('name', 'availability_total')->bind($this->data, 'availability_total')->attr('type', 'number')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.availabilitycustomer'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('availability_customer'))->attr('name', 'availability_customer')->bind($this->data, 'availability_customer')->attr('type', 'number')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.pricereservation'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('price_reservation'))->attr('name', 'price_reservation')->bind($this->data, 'price_reservation')->attr('type', 'number')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.name'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            foreach ($this->dataLangList as $l)
            {
                (new \EOS\UI\Bootstrap\Input('title-' . $l['id']))->attr('class', 'translatable-field lang-' . $l['id'])->bind($this->data, 'name-' . $l['id'])->printRender($this);
            }
            echo '</div>';
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\DropDownMenu('lng2');
            $sel->attr('class', 'select-lang');
            $sel->onChange('function(item) {changeLanguage(item)}');
            $sel->bind($this->dataLangDefault, 'id');
            $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();

        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.category.descr'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-8">';
            foreach ($this->dataLangList as $l)
            {
                (new \EOS\UI\Bootstrap\Textarea('descr-' . $l['id']))->attr('class', 'translatable-field lang-' . $l['id'])->bind($this->data, 'descr-' . $l['id'])->printRender($this);
            }
            echo '</div>';
            echo '<div class="col-xs-2">';
            $sel = new EOS\UI\Bootstrap\DropDownMenu('dr-ta');
            $sel->attr('class', 'select-lang');
            $sel->onChange('function(item) {changeLanguage(item)}');
            $sel->bind($this->dataLangDefault, 'id');
            $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
            $sel->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.timeslot'))->attr('class', 'col-xs-2 text-right')->printRender();
            if (!empty($this->timeslot))
            {
                foreach ($this->timeslot as $timeslot)
                {
                    echo '<div class="col-xs-1">';
                    echo '<div class="checkbox"><label><input type="checkbox" name="timeslot-' . $timeslot['id'] . '" value="1" ' . (isset($this->data['timeslot-' . $timeslot['id']]) ? 'checked' : '') . '>' . $this->encodeHtml($timeslot['name']) . '</label></div>';
                    echo '</div>';
                }
            }
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.depositpercent'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('deposit_percent'))->attr('name', 'deposit_percent')->bind($this->data, 'deposit_percent')->attr('type', 'number')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
        {
            (new \EOS\UI\Html\Label)->content($this->transE('reservation.setting.field.type.depositday'))->attr('class', 'col-xs-2 text-right')->printRender();
            echo '<div class="col-xs-2">';
            (new \EOS\UI\Bootstrap\Input('deposit_day'))->attr('name', 'deposit_day')->bind($this->data, 'deposit_day')->attr('type', 'number')->printRender($this);
            echo '</div>';
        }, $this)->printRender();
        $box->endContent();
        $box->startFooter();
        if ((isset($this->data['id'])) && ($this->data['id'] != 0))
        {
            (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
        }
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();  }')->printRender($this);
        (new \EOS\UI\Bootstrap\Button())->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();  }')->printRender($this);
        $box->endFooter();
        $box->printRender($this);
        $this->writeTokenHtml();
        $form->endContent();
        $form->printRender($this);
        ?>
    </div><!-- /.col -->
</div><!-- /.row -->
<?php 
$this->startCaptureScript(); 
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('reservation', ['type', 'ajaxedit']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;

    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('Reservation', 'setting/default'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('reservation.setting.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('Reservation', 'type/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();

