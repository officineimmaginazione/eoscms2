<?php

$res = new \EOS\Components\Reservation\Models\ReservationModel($this->view->controller->container);
if ($this->view->controller->container->get('database')->setting->getStr('resevation', 'dbversion') >= 1000)
{
    $this->addSummaryWidget('green', 'Prenotazioni Totali', $res->getReservationCount(), 'ion-ios-paper-outline', $this->view->path->urlFor('reservation', ['', 'index']), 3);
    $this->addSummaryWidget('aqua', 'Prenotati oggi', $res->getReservationCount(true, false), 'ion-ios-paper-outline', $this->view->path->urlFor('reservation', ['', 'index']), 1);
    $this->addSummaryWidget('yellow', 'Prenotati mese', $res->getReservationCount(false, true), 'ion-ios-paper-outline', $this->view->path->urlFor('reservation', ['', 'index']), 2);
}