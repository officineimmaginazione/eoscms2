<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use EOS\Components\System\Classes\AuthLevel;

class AuthController extends \EOS\System\Controllers\Controller
{

    protected $authorizations = [];
    public $user = null;

    protected function loadAuthorization()
    {
        
    }

    public function __construct($container)
    {
        $this->user = new AuthUser($container);
        $this->user->checkLogged();
        if ($container->has('mapAuth'))
        {
            $this->authorizations = $container['mapAuth'];
        } else
        {
            // Controllo di sicurezza che la Component della System sia caricata correttamente
            throw new \Exception('Errore imprevisto verificare sia caricato all\'avvio il middleware "AuthMapMiddleware!');
        }
        $this->loadAuthorization();
        $this->user->checkAuthorizationList($this->authorizations);
        parent::__construct($container);
    }

    public function newView($request, $response, $classView = '')
    {
        if ($classView == '')
        {
            $classView = '\EOS\Components\System\Classes\AuthView';
        }
        return parent::newView($request, $response, $classView);
    }

}
