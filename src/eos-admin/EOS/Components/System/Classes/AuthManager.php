<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use EOS\System\Util\ArrayHelper;
use EOS\System\Component\ComponentHelper;

class AuthManager
{

    private $idUser = 0;
    private $idGroup = -1;
    private $container;
    private $userAuthorizations = [];
    private $authorizations = [];

    public function __construct($container)
    {
        $this->container = $container;
        $this->loadAuthorization();
    }

    public function loadAuthorization()
    {
        $this->authorizations = [];
        ComponentHelper::includeRoot($this->container, 'auth', $this);
    }

    public function getAuthorizations(): array
    {
        return $this->authorizations;
    }

    public function getUserAuthorizations(int $idUser = 0): array
    {
        if (($idUser !== $this->idUser) || (empty($this->userAuthorizations)))
        {
            $this->userAuthorizations = [];
            $this->idUser = $idUser;
            $this->idGroup = -1; // Forzo a un gruppo che non esiste
            $db = $this->container->get('database');
            $q = $db->prepare('select u.id_group, g.authorization 
                from #__admin_user u 
                left join #__admin_group g on u.id_group = g.id and g.status = 1
                where u.id = :id ');
            $q->bindValue(':id', $idUser);
            $q->execute();
            $r = $q->fetch();
            if (!empty($r))
            {
                $this->idGroup = (int) $r['id_group'];
                $this->userAuthorizations = empty($r['authorization']) ? [] : json_decode($r['authorization'], true);
                if ($this->idGroup === 0) // Super amministatore
                {
                    $roles = AuthRole::getList();
                    foreach (array_keys($this->authorizations) as $k)
                    {
                        foreach ($roles as $role)
                        {
                            $this->userAuthorizations[$k][$role] = true;
                        }
                    }
                }
            }
        }
        return $this->userAuthorizations;
    }

    public function add(string $authName, string $authLang, array $authRoles = [])
    {
        $this->authorizations[$authName] = ['lang' => $authLang, 'roles' => $authRoles];
    }

    public function getLangTrans(string $authName): string
    {
        $lang = $this->container->get('language');
        $s = ArrayHelper::getMdStr($this->authorizations, [$authName, 'lang'], '');
        if (!empty($s))
        {
            $l = explode(',', $s);
            if (!empty($s))
            {
                foreach ($l as &$v)
                {
                    $v = $lang->trans($v);
                }
                $s = implode(' - ', $l);
            }
        }
        return $s;
    }

}
