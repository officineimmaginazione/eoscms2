<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2018
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Util\ArrayHelper;

class AuthMapMiddleware extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $route = $request->getAttribute('route');
        if (empty($route))
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $auth = ArrayHelper::getArray($route->mapOptions, 'auth');
        $user = new AuthUser($this->container);
        $user->checkAuthorizationList($auth);
        $this->container['mapAuth'] = $auth;
        return $next($request, $response);
    }

}
