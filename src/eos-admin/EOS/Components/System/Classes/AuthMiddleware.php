<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;

class AuthMiddleware extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $uri = $request->getUri();
        $p = PathHelper::removeSlash($uri->getPath($uri));
        // Controllo se è un comando di Cron
        if (StringHelper::startsWith($p, 'system/cron/run'))
        {
            $p = '';
        }
        if (!in_array($p, ['', 'system/access/login', 'system/access/signin']))
        {
            $user = new AuthUser($this->container);
            if (!$user->isLogged())
            {
                $newuri = $uri->withPath('');
                return $response->withRedirect($newuri, 302);
            }
        }

        return $next($request, $response);
    }

}
