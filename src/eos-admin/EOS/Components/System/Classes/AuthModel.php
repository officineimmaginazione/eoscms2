<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

class AuthModel extends \EOS\System\Models\Model
{

    protected $user = null;

    public function __construct($container)
    {
        $this->user = new AuthUser($container);
        parent::__construct($container);
    }
}
