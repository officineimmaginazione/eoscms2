<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

class AuthRole
{

    const CREATE = 'create';
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const ALL = 'all';
    
    
    public static function getList(): array
    {
        return [static::CREATE, static::READ, static::UPDATE, static::DELETE, static::ALL];
    }

}
