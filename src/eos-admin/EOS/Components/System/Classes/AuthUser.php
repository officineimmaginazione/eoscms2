<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use EOS\Components\System\Classes\AuthRole;
use EOS\System\Util\ArrayHelper;
use EOS\Components\System\Classes\AuthManager;

class AuthUser
{

    protected $session;
    protected $authorizations = [];
    protected $container;

    protected function loadAuthorizations()
    {
        if (empty($this->authorizations))
        {
            $this->authorizations = $this->getAuthManager()->getUserAuthorizations($this->getUserID());
        }
    }

    protected function raiseAccessDenied($authName, $authRole)
    {
        $msg = $this->container->get('language')->trans('system.common.accessdenied') . $this->getAuthManager()->getLangTrans($authName);
        $msg .= "\n".' ['.$authRole.']';
        throw new \Exception($msg);
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->session = $this->container->get('session');
    }

    public function isLogged()
    {
        return $this->session->get('authadmin', false);
    }

    public function checkLogged()
    {
        if (!$this->isLogged())
        {
            throw new \Exception('Login non valida!');
        }
    }

    public function signin($passwordDB, $password)
    {
        if (password_verify($password, $passwordDB))
        {
            $this->session->set('authadmin', true);
            return true;
        } else
        {
            return false;
        }
    }

    public function saveInfo($id, $username, $email)
    {
        $userj = json_encode(['id' => $id, 'username' => $username, 'email' => $email]);
        $this->session->set('admin.user', $userj);
    }

    public function getInfo()
    {
        $userj = $this->session->get('admin.user');
        return json_decode($userj, false);
    }

    public function getUserID(): int
    {
        $user = $this->getInfo();
        return empty($user) ? 0 : (int) $user->id;
    }

    public function getAuthManager(): AuthManager
    {
        if (!$this->container->has('authManager'))
        {
            $this->container['authManager'] = new AuthManager($this->container);
        }
        return $this->container->get('authManager');
    }

    public function checkAuthorizationList(array $authList)
    {
        foreach ($authList as $authName => $roleList)
        {
            if (empty($roleList))
            {
                $this->raiseAccessDenied($authName, '');
            }
            $res = false;
            $list = [];
            foreach ($roleList as $role)
            {
                $list[] = $role;
                if ($this->isAuthorized($authName, $role))
                {
                    $res = true;
                }
            }
            if (!$res)
            {
                $this->raiseAccessDenied($authName, implode(', ', $list));
            }
        }
    }

    public function checkAuthorization(string $authName, string $authRole)
    {
        if ((!$this->isLogged()) || (!$this->isAuthorized($authName, $authRole)))
        {
            $this->raiseAccessDenied($authName, $authRole);
        }
    }

    public function isAuthorized(string $authName, string $authRole): bool
    {
        $this->loadAuthorizations();
        return strlen($authName) === 0 ? true : ArrayHelper::getMdBool($this->authorizations, [$authName, $authRole], false);
    }

    public function isAuthorizedCreate(string $authName): bool
    {
        return $this->isAuthorized($authName, AuthRole::CREATE);
    }

    public function isAuthorizedRead(string $authName): bool
    {
        return $this->isAuthorized($authName, AuthRole::READ);
    }

    public function isAuthorizedUpdate(string $authName): bool
    {
        return $this->isAuthorized($authName, AuthRole::UPDATE);
    }

    public function isAuthorizedDelete(string $authName): bool
    {
        return $this->isAuthorized($authName, AuthRole::DELETE);
    }

}
