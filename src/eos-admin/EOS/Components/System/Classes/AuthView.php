<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

class AuthView extends \EOS\System\Views\Html
{

    public $pageTitle;
    public $pageSubtitle;
    public $breadcrumbs = [];

    public function __construct($controller, $request, $response)
    {
        parent::__construct($controller, $request, $response);
        $this->user = new AuthUser($controller->container);
        \EOS\UI\Loader\Library::load($this, 'Bootstrap');
        \EOS\UI\Loader\Library::load($this, 'Bootbox');
        $this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $this->addBreadcrumb($this->path->urlFor('system', ['dashboard', 'index']), $this->trans('system.common.home'), 'fa fa-dashboard');
    }

    public function addBreadcrumb($href, $title, $icon = '')
    {
        $href = ($href == '') ? 'javascript:void(0)' : $href;
        $this->breadcrumbs[] = ['href' => $href, 'title' => $title, 'icon' => $icon];
    }

    public function loadCalendar()
    {
        \EOS\UI\Loader\Library::load($this, 'FullCalendar');
    }

    public function loadDataTables()
    {
        \EOS\UI\Loader\Library::load($this, 'DataTables');
    }

    public function loadCKEditor()
    {
        \EOS\UI\Loader\Library::load($this, 'CKEditor');
    }

    public function loadiCheck()
    {
        \EOS\UI\Loader\Library::load($this, 'iCheck');
    }

    public function loadSelect2()
    {
        \EOS\UI\Loader\Library::load($this, 'Select2');
    }
    
    public function loadColorPicker()
    {
        \EOS\UI\Loader\Library::load($this, 'ColorPicker');
    }

    public function getTemplateFileList()
    {
        $sitetheme = $this->controller->container->get('database')->setting->getStr('setting', 'theme');
        $list = glob(_EOS_PATH_THEMES_ . $sitetheme . DIRECTORY_SEPARATOR . '*.php');
        $res[] = ['name' => 'default'];
        foreach ($list as $item)
        {
            $t = basename($item, '.php');
            if ($t != 'default')
            {
                $res[] = ['name' => $t];
            }
        }
        return $res;
    }

}
