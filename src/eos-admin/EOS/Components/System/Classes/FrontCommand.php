<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use EOS\System\Routing\PathHelper;

class FrontCommand
{

    protected $container;
    public $idLang = null;
    public $error = false;
    public $errorMessage = '';
    public $resultCode = null;
    public $result = null;
    public $requestUrl = ''; // Generato in automatico

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function execute($component, $path, $command = 'GET', $data = [])
    {
        $p = $this->container->get('path');
        $urlArr = [PathHelper::removeSlash($p->getBaseUrl())];
        $isMultiLang = $this->container->get('database')->setting->getBool('setting', 'multilanguage', true);
        if ($isMultiLang)
        {
            $lang = $this->container->get('language');
            $l = $lang->getLangFromDB($this->idLang);
            if (empty($l))
            {
                $l = $lang->getDefaultFromDB(true);
            }
            if (empty($l))
            {
                throw new \Exception('FrontCommand - Manca la lingua selezionata o di default!');
            }

            $urlArr[] = $l['iso_code'];
        }
        $urlArr[] = strtolower($component);
        $urlArr[] = strtolower(PathHelper::removeSlash($path));
        $url = PathHelper::implodeSlash($urlArr) . '/';
        $this->requestUrl = $url;
        // Devo inviare una richiesta alla parte sito perché così usa il template predefinito
        $h = new \EOS\System\Http\Client();
        // Disattiva controllo SSL per SELF certificati locali
        $h->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $h->setOpt(CURLOPT_SSL_VERIFYHOST, FALSE);
        switch (strtoupper($command))
        {
            case 'GET':
                $this->result = $h->get($url, $data);
                break;
            case 'POST':
                // Così encoda in automatico
                $h->setHeader('Content-Type', 'application/json');
                $this->result = $h->post($url, $data, true);
                break;
            default:
                throw new \Exception('Invalid command!');
        }
        $this->error = $h->error;
        if ($h->error)
        {
            $this->resultCode = $h->errorCode;
            $this->errorMessage = $h->errorMessage;
        } else
        {
            $this->resultCode = $h->httpStatusCode;
            $this->errorMessage = '';
        }

        return !$this->error;
    }

}
