<?php

namespace EOS\Components\System;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        parent::load();

        $this->application->addMapMiddleware('\EOS\Components\System\Classes\AuthMapMiddleware');
    }

}
