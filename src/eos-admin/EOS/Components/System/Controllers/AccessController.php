<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\DebugHelper;

class AccessController extends \EOS\System\Controllers\Controller
{

    
    public function login($request, $response, $args)
    {
        $this->container->currentParams['themeTemplate'] = 'login';
        $this->session->resetToken();
        $v = $this->newView($request, $response);
        return $v->render('access/login');
    }

    public function signin($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $um = new \EOS\Components\System\Models\UserModel($this->container);
                $au = new \EOS\Components\System\Classes\AuthUser($this->container);
                $user = $um->getPasswordFromUserEmail($parsedBody['email']);
                if ($au->signin($user['password'], $parsedBody['password']))
                {
                    $au->saveInfo($user['id'], $user['username'], $user['email']);
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('System', ['dashboard', 'index']);
                    $this->session->resetToken();
                } else
                {
                    
                    $result['message'] = $this->lang->trans('system.access.login-error');//'Invalid email or password';
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function logout($request, $response, $args)
    {
        $this->session->destroy();
        $newUrl = $this->path->urlFor('system', ['access', 'login']);
        return $response->withRedirect($newUrl, 302);
    }

}
