<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */
/* Questo controlle è aperto sulla pubblica */

namespace EOS\Components\System\Controllers;

class CronController extends \EOS\System\Controllers\Controller
{

    public function run($request, $response)
    {
        $m = new \EOS\Components\System\Models\CronModel($this->container);
        $m->runJob($request->getAttribute('job'));
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow');
    }

}
