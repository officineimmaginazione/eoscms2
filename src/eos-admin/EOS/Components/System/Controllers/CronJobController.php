<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class CronJobController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\CronJobModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('importexport.job.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->items = $m->getList();
        return $v->render('cronjob/default');
    }

}
