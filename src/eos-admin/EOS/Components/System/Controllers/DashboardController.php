<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\DebugHelper;

class DashboardController extends \EOS\Components\System\Classes\AuthController
{  
    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->pageTitle = 'Dashboard';
        $v->pageSubtitle = '';
        return $v->render('dashboard/default', true);
    }
}
