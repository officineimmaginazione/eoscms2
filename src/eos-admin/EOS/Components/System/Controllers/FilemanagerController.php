<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Routing\PathHelper;

class FilemanagerController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        return $v->render('filemanager/default', false);
    }

    public function ajax($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        $v->user = $this->user; // Injection dell'utente sul Raw
        $data = $v->render('filemanager/config-js')->getBody();
        $this->session->set('filemanager.config', $data);
        $this->session->set('filemanager.configdefault', $data);
        $this->session->set('filemanager.library', PathHelper::addDirSep(_EOS_PATH_ROOT_) . _EOS_URL_LIBRARY_ . DIRECTORY_SEPARATOR . 'filemanager' . DIRECTORY_SEPARATOR . 'e1.0' . DIRECTORY_SEPARATOR);
        require_once(_EOS_PATH_SYSTEM_LIBS_ . 'Filemanager' . DIRECTORY_SEPARATOR . 'filemanager.php');
    }

    public function configJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        $v->user = $this->user; // Injection dell'utente sul Raw
        return $v->render('filemanager/config-js');
    }

}
