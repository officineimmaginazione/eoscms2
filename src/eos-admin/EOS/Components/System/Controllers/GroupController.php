<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class GroupController extends \EOS\Components\System\Classes\AuthController
{


    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('system.group.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('group/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('system.group.insert.title') : $v->trans('system.group.edit.title');
        $v->addBreadcrumb($this->path->urlFor('system', ['group', 'index']), $v->trans('system.group.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\System\Models\GroupModel($this->container);
        $v->data = $m->getData($args['id']);
        return $v->render('group/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\GroupModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'id');
        $dts->addColumn('name', 'name', true);
        $dts->addColumn('status', 'status', false);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\GroupModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'group/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\GroupModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'group/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
