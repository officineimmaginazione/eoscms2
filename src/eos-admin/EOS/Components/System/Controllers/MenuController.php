<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class MenuController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\MenuModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('system.menu.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->items = $m->getList();
        return $v->render('menu/default');
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\MenuModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->getUrlFor('System', 'menu/index'), $v->trans('system.menu.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('system.menu.insert.title') : $v->trans('system.menu.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('system.menu.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('system.menu.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        $v->data = $m->getData($args['id']);
        $v->routers = [];
        foreach ($v->dataLangList as $l)
        {
          $v->routers[$l['id']] = $m->getRouteList($l['iso_code']); 
        }
        
        // https://github.com/dbushell/Nestable
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'menu/menu.js'));
        $v->addStyleSheetLink($this->path->getExtraUrlFor('system', 'menu/menu.css'));
        return $v->render('menu/edit', true);
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\MenuModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'menu/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\MenuModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'menu/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

    public function menuJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        return $v->render('menu/menu', 'js');
    }

    public function menuCSS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'text/css'), 0);
        return $v->render('menu/menu', 'css');
    }

}
