<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class PositionController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\PositionModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('system.position.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->items = $m->getList();
        return $v->render('position/default');
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\PositionModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->addBreadcrumb($this->path->getUrlFor('System', 'position/index'), $v->trans('system.position.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('system.position.insert.title') : $v->trans('system.position.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        if ($this->session->has('system.position.id_lang'))
        {
            $nl = $this->lang->getLangFromDB($this->session->get('system.position.id_lang'));
            if (!empty($nl))
            {
                $v->dataLangDefault = $nl;
            }
        }
        $v->data = $m->getData($args['id']);
        return $v->render('position/edit', true);
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\PositionModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'position/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\PositionModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'position/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
