<?php

namespace EOS\Components\System\Controllers;

class ResourceController extends \EOS\Components\System\Classes\AuthController
{

    public function appJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        return $v->render('resource/app-js');
    }

    public function keepAliveJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        return $v->render('resource/keepalive-js', 'php');
    }

}
