<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Controllers;

class ServiceController extends \EOS\Components\System\Classes\AuthController
{

    public function keepAlive($request, $response, $args)
    {
        $data = [];
        $data['datetime'] = \EOS\System\Util\DateTimeHelper::formatISO(\EOS\System\Util\DateTimeHelper::now());
        return $response->withHeader('X-Robots-Tag', 'noindex')->withJson($data);
    } 

}
