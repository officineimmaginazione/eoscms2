<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class SettingController extends \EOS\Components\System\Classes\AuthController
{
    private function getThemeList()
    {
        $res = [];
        $list = glob(_EOS_PATH_THEMES_ . '*', GLOB_ONLYDIR);
        foreach ($list as $item)
        {
            $t = basename($item);
            $res[] = ['name' => $t];
        }
        return $res;
    }
    
    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('system.setting.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $m = new \EOS\Components\System\Models\SettingModel($this->container);
        $v->setting = $m->getSetting();
        $v->data = $m->getSettingData();
        $v->themeList = $this->getThemeList();
        $v->loadiCheck();
        return $v->render('setting/default', true);
    }


    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\SettingModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'id');
        $dts->addColumn('object', 'object', true);
        $dts->addColumn('name', 'name', true);
        $dts->addColumn('value', 'value', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $result = [];
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\System\Models\SettingModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('System', ['setting', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result = [];
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\System\Models\SettingModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('System', ['setting', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxSaveData($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\SettingModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveSettingData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'setting/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxTestMail($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        if ($fv->prepareInputDataFromJson())
        {
            try
            {
                $mg = new \EOS\System\Mail\Message($this->container);
                $mg->subject = $this->lang->trans('system.common.test');
                $mg->content = $mg->subject;
                $mg->to = $this->container['database']->setting->getStr('email_setting', 'from');
                if ($mg->send())
                {
                    $fv->setResult(true);
                    $fv->setMessage($this->lang->trans('system.common.test'));
                } else
                {
                    $fv->setMessage($mg->processError);
                }
            } catch (\Exception $e)
            {
                $fv->setMessage($e->getMessage());
            }
        }
        return $fv->toJsonResponse();
    }

}
