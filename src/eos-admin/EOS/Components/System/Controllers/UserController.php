<?php

namespace EOS\Components\System\Controllers;

use EOS\System\Util\FormValidator;

class UserController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('system.user.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('user/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('system.user.insert.title') : $v->trans('system.user.edit.title');
        $v->addBreadcrumb($this->path->urlFor('system', ['user', 'index']), $v->trans('system.user.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\System\Models\UserModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->groups = $m->getGroupList();
        return $v->render('user/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\System\Models\UserModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'au.id');
        $dts->addColumn('username', 'au.username', true);
        $dts->addColumn('email', 'au.email', true);
        $dts->addColumn('ugroup', 'ag.name', true);
        $dts->addColumn('status', 'au.status', false);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\UserModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'user/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\System\Models\UserModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('system', 'user/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

}
