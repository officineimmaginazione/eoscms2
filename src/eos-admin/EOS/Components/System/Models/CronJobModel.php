<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

use EOS\System\Util\StringHelper;

class CronJobModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getList(): array
    {
        $cM = new CronModel($this->container);
        $res = [];
        $list = $cM->getList();

        foreach ($list as $item)
        {
            $res[] = ['id' => StringHelper::encodeHtml(pathinfo($item, PATHINFO_FILENAME))];
        }

        return $res;
    }

}
