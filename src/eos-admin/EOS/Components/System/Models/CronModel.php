<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Util\ArrayHelper;

class CronModel extends \EOS\System\Models\Model
{

    private $defExt = 'json';
    private $cronPath;
    private $jobsPath;
    private $log;
    private $logToScreen = true;

    private function createCronPath()
    {
        $path = $this->container->get('path');
        $p = PathHelper::addDirSep($path->getUploadsPath() . 'cron');
        if (!is_dir($p))
        {
            mkdir($p);
        }
        // Creo un .htaccess che blocca tutto!!!!
        $htaccess = $p . '.htaccess';
        if (!file_exists($htaccess))
        {
            file_put_contents($htaccess, "<Files *.*>\n Order Deny,Allow\n Deny from all\n</Files>");
        }
        $this->cronPath = $p;
    }

    private function createJobsPath()
    {
        $p = PathHelper::addDirSep($this->cronPath . 'jobs');
        if (!is_dir($p))
        {
            mkdir($p);
        }
        $this->jobsPath = $p;
    }

    private function createLog()
    {
        $p = PathHelper::addDirSep($this->cronPath . 'logs');
        if (!is_dir($p))
        {
            mkdir($p);
        }

        $this->log = new \EOS\System\Util\LogFile($p . 'logs.log');
    }

    private function runTask(string $class, array $params)
    {

        $taskClass = new \ReflectionClass($class);
        $task = $taskClass->newInstanceArgs([$this->container, $params]);
        $task->logProc = function ($value)
        {
            $this->log->add($value);
            if ($this->logToScreen)
            {
                echo $value.'<br>';
            }
        };
        $task->run();
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->createCronPath();
        $this->createJobsPath();
        $this->createLog();
    }

    public function getList(): array
    {
        $res = glob($this->jobsPath . '*.' . $this->defExt);
        return $res;
    }

    public function runJob(string $name)
    {
        $this->log->captureException(function () use ($name)
        {
            $name = StringHelper::sanitizeFileName($name);
            $p = $this->jobsPath . $name . '.' . $this->defExt;
            if (!file_exists($p))
            {
                throw new \Exception('Invalid job:' . $name);
            }

            $tasks = ArrayHelper::fromJSON(file_get_contents($p));
            foreach ($tasks as $t)
            {
                $this->runTask(ArrayHelper::getStr($t, 'task'), ArrayHelper::getArray($t, 'params'));
            }
        });
    }

}
