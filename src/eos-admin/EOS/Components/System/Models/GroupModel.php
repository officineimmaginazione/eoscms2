<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

use EOS\System\Util\ArrayHelper;
use \EOS\Components\System\Classes\AuthRole;

class GroupModel extends \EOS\Components\System\Classes\AuthModel
{

    private function checkGroupExists(string $name, int $idSkip)
    {
        $q = $this->db->prepare('select id from #__admin_group where (name = :name) and id <> :id');
        $q->bindValue(':name', $name);
        $q->bindValue(':id', $idSkip);
        $q->execute();
        $r = $q->fetch();
        return empty($r);
    }

    private function prepareAuth(array &$data)
    {
        $am = $this->user->getAuthManager();
        $da = $am->getAuthorizations();
        $al = array_keys($da);
        $aRes = [];
        foreach ($al as $name)
        {
            $roles = AuthRole::getList();
            $v = [];
            foreach ($roles as $role)
            {
                $b = ArrayHelper::getBool($data, 'auth_' . $name . '_' . $role, false);
                if ($b === true)
                {
                    $v[$role] = $b;
                }
            }
            if (!empty($v))
            {
                $aRes[$name] = $v;
            }
        }
        $data['authorization'] = json_encode($aRes);
    }

    private function prepareData(array &$data, string &$error)
    {
        $res = true;
        if (strlen($data['name']) === 0)
        {
            $error = $this->lang->trans('system.group.error.name');
            $res = false;
        } else if (!$this->checkGroupExists($data['name'], (int) $data['id']))
        {
            $error = $this->lang->trans('system.group.error.groupexists');
            $res = false;
        }

        if ($res)
        {
            $this->prepareAuth($data);
        }

        return $res;
    }

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__admin_group'))
                ->select(null)->select('id, name, status');
        return $f;
    }

    public function getData(int $id): array
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__admin_group g'))
            ->where('g.id = ?', (int) $id);
        $rc = $query->fetch();
        $res = [];
        $am = $this->user->getAuthManager();
        $authorization = $am->getAuthorizations();
        $roles = AuthRole::getList();
        foreach ($authorization as $k => &$a)
        {
            if (empty($a['roles']))
            {
                $a['roles'] = $roles;
            }
            $a['lang'] = $am->getLangTrans($k);
        }
        unset($a); // Rimuove solo il puntatore se usato per referenza prima
        if (empty(!$rc))
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['status'] = (int) $rc['status'] === 1;
            $userAuthStr = $rc['authorization'];
            if (!empty($userAuthStr))
            {
                $userAuth = json_decode($userAuthStr, true);
                foreach ($authorization as $k => &$r)
                {
                    foreach ($roles as $role)
                    {
                        $r['authList'][$role] = ArrayHelper::getMdBool($userAuth, [$k, $role], false);
                    }
                }
                unset($r); // Rimuove solo il puntatore se usato per referenza prima
            }
        }

        $res['authorization'] = $authorization;
        return $res;
    }

    public function saveData(array &$data, string &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'authorization' => ArrayHelper::getStr($data, 'authorization'),
                'status' => (isset($data['status']) ? $data['status'] == 'on' : false),
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tbl = $this->db->tableFix('#__admin_group');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tbl, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            $this->db->commit();
            return true;
        }
    }

    public function deleteData(array $data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__admin_group');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
