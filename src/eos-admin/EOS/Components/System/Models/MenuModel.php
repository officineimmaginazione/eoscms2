<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

class MenuModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getList()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__menu m'))
            ->select(null)->select('m.id, m.name, m.status')
            ->orderBy('m.name');
        return $f->fetchAll();
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__menu m'))
            ->select(null)
            ->select('m.id, m.name, m.status')
            ->where('m.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
        } else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['status'] = $rc['status'] == 1;
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__menu_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_menu', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if (!empty($rcl))
                {
                    $res['data-'.$l['id']] = json_decode($rcl['data'], true);
                    if (!empty($rcl['options']))
                    {
                        $ops = json_decode($rcl['options'], true);
                        $res['options-' . $idlang . '-id'] = $ops['id'];
                        $res['options-' . $idlang . '-class'] = $ops['class'];
                    }
                }
            }
        }
        return $res;
    }
    
    public function getRouteList($langiso)
    {
         $query = $this->db->newFluent()->from($this->db->tableFix('#__router r'))
             ->select(null)
            ->select('r.path, r.route')
            ->where('r.lang = ?', $langiso)
            ->where('r.menu = ?', 1);
        $rc = $query->fetchAll(); 
        if (empty($rc))
        {
            return [];
        }
        else
        {
            return $rc;
        }
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        $data['name'] =  \EOS\System\Util\StringHelper::sanitizeUrl($data['name']);
        if (empty($data['name']))
        {
            $error = $this->lang->trans('system.menu.error.name');
            $res = false;
        }

        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $status = \EOS\System\Util\ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0;
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'status' => $status,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__menu');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $data['id'] = $this->db->lastInsertId();
            }
            $ll = $this->lang->getArrayFromDB(true);
            $c = 0;
            foreach ($ll as $l)
            {
                if ($this->saveDataLang($l['id'], $l['iso_code'], $data))
                {
                    $c++;
                }
            }
            if ($c == 0)
            {
                throw new \Exception('Language item not found!');
            }
            $this->db->commit();
            return true;
        };
    }

    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContentLang = $this->db->tableFix('#__menu_lang');
        $query = $this->db->newFluent()->from($tblContentLang)->select(null)->select('id')
            ->where('id_menu', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();

        $menu = ['menu' => json_decode($data['data-'.$id_lang], true)];
        $dataop = [
                'id' => $data['options-' . $id_lang . '-id'],
                'class' => $data['options-' . $id_lang . '-class']
            ];
        $options = json_encode($dataop);
        $values = [
            'id_menu' => $data['id'],
            'id_lang' => $id_lang,
            'data' => json_encode($menu),
            'options' => $options,
            'up_id' => $this->user->getUserID(),
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        if (empty($item))
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $query = $this->db->newFluent()->insertInto($tblContentLang, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContentLang)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();

        return true;
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__menu_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_menu', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__menu');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
