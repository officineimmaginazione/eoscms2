<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

class PositionModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getList()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__position p'))
            ->orderBy('p.name, p.ordering');
        return $f->fetchAll();
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__position p'))
            ->where('p.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['name'] = $rc['name'];
            $res['widget_name'] = $rc['widget_name'];
            $res['widget_class'] = $rc['widget_class'];
            $res['status'] = $rc['status'] == 1;
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__position_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_position', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if (!empty($rcl))
                {
                    $res['filter_type-' . $l['id']] = $rcl['filter_type']; 
                    $res['data-' . $l['id']] = json_decode($rcl['data'], true);
                }
            }
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        $data['name'] = \EOS\System\Util\StringHelper::sanitizeUrl($data['name']);
        if (empty($data['name']))
        {
            $error = $this->lang->trans('system.position.error.name');
            $res = false;
        }

        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'widget_name' => $data['widget_name'],
                'widget_class' => $data['widget_class'],
                'status' => (isset($data['status']) ? $data['status'] == 'on' : false),
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__position');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $data['id'] = $this->db->lastInsertId();
            }
            $ll = $this->lang->getArrayFromDB(true);
            foreach ($ll as $l)
            {
                if ($this->saveDataLang($l['id'], $l['iso_code'], $data))
                {
                    
                }
            }
            $this->db->commit();
            return true;
        }
    }

    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContentLang = $this->db->tableFix('#__position_lang');
        $query = $this->db->newFluent()->from($tblContentLang)->select(null)->select('id')
            ->where('id_position', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();

        $filter = [];//['menu' => json_decode($data['data-' . $id_lang], true)];
        $values = [
            'id_position' => $data['id'],
            'id_lang' => $id_lang,
            'filter_type' => \EOS\System\Util\ArrayHelper::getInt($data, 'filter_type-' . $id_lang, 0),
            'data' => json_encode($filter),
            'up_id' => $this->user->getUserID(),
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        if (empty($item))
        {
            $values['ins_id'] = $this->user->getUserID();
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $query = $this->db->newFluent()->insertInto($tblContentLang, $values);
            $item['id'] = $this->db->lastInsertId();
        } else
        {
            $query = $this->db->newFluent()->update($tblContentLang)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();

        return true;
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContentLang = $this->db->tableFix('#__position_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_position', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__position');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

}
