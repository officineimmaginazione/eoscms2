<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

use EOS\System\Util\ArrayHelper;

class SettingModel extends \EOS\System\Models\Model
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__setting'));
        return $f;
    }

    public function getSetting()
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__setting'));
        $r = $q->fetchAll();
        if (is_null($r))
        {
            return [];
        }
        return $r;
    }

    public function saveData(&$data)
    {
        $values = [
            'object' => $data['object'],
            'name' => $data['name'],
            'value' => $data['value']
        ];
        $tblContent = $this->db->tableFix('#__setting');
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
        return true;
    }

    public function deleteData(&$data)
    {
        $tblContent = $this->db->tableFix('#__setting');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        return true;
    }

    private function getItem($list, $object, $name)
    {
        foreach ($list as $r)
        {
            if (($r['object'] == $object) && ($r['name'] == $name))
            {
                return $r['value'];
            }
        }
        return null;
    }

    public function getSettingData()
    {
        $l = $this->getSetting();
        $data['setting-theme'] = $this->getItem($l, 'setting', 'theme');   
        // Email
        $data['email_setting-host'] = $this->getItem($l, 'email_setting', 'host');
        $data['email_setting-port'] = $this->getItem($l, 'email_setting', 'port');
        if (empty($data['email_setting-port']))
        {
          $data['email_setting-port'] = 25;
        }
        $data['email_setting-auth'] = $this->getItem($l, 'email_setting', 'auth') == 'true';
        $data['email_setting-secure'] = $this->getItem($l, 'email_setting', 'secure');
        $data['email_setting-username'] = $this->getItem($l, 'email_setting', 'username');
        $data['email_setting-password'] = $this->getItem($l, 'email_setting', 'password');
        $data['email_setting-from'] = $this->getItem($l, 'email_setting', 'from');  
        return $data;
    }
    
    public function saveSettingData(&$data, &$err)
    {
        $this->db->setting->setValue('setting', 'theme', $data['setting-theme']);
        // Email
        $this->db->setting->setStr('email_setting', 'host', $data['email_setting-host']);
        $this->db->setting->setStr('email_setting', 'port', $data['email_setting-port']);
        $this->db->setting->setBool('email_setting', 'auth',  ArrayHelper::getBool($data, 'email_setting-auth', false));
        $this->db->setting->setValue('email_setting', 'secure', $data['email_setting-secure']);
        $this->db->setting->setStr('email_setting', 'username', $data['email_setting-username']);
        $this->db->setting->setStr('email_setting', 'password', $data['email_setting-password']);
        $this->db->setting->setStr('email_setting', 'from', $data['email_setting-from']);
        return true;
    }

}
