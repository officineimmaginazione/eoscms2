<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Models;

class UserModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__admin_user au'))
                ->leftJoin($this->db->tableFix('#__admin_group ag ON ag.id = au.id_group'))
                ->select(null)->select('au.id, au.username, au.email, ag.name as ugroup, au.status');

        return $f;
    }

    public function getPasswordFromUserEmail($useremail)
    {
        $q = $this->db->prepare('select * from #__admin_user where username = :username or email = :email');
        $q->bindValue(':username', \EOS\System\Routing\PathHelper::removeSlash($useremail));
        $q->bindValue(':email', \EOS\System\Routing\PathHelper::removeSlash($useremail));
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            return null;
        } else
        {
            return $r;
        }
    }

    private function checkUserExists(string $username, string $email, int $idSkip)
    {
        $q = $this->db->prepare('select id from #__admin_user where ((username = :username) or (email = :email)) and id <> :id');
        $q->bindValue(':username', $username);
        $q->bindValue(':email', $email);
        $q->bindValue(':id', $idSkip);
        $q->execute();
        $r = $q->fetch();
        return empty($r);
    }

    public function getData(int $id): array
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__admin_user u'))
            ->where('u.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
        } else
        {
            $res['id'] = $rc['id'];
            $res['username'] = $rc['username'];
            $res['email'] = $rc['email'];
            $res['id_group'] = $rc['id_group'];
            $res['status'] = $rc['status'] == 1;
        }
        return $res;
    }

    private function prepareData(array &$data, string &$error)
    {
        $res = true;
        // Rimuovo gli spazi all'utente (funziona anche con utf-8 ascii < 128)
        $data['username'] = str_replace(' ', '', $data['username']);
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false)
        {
            $error = $this->lang->trans('system.user.error.email');
            $res = false;
        } else if (empty($data['username']))
        {
            $error = $this->lang->trans('system.user.error.username');
            $res = false;
        } else
        {
            if ((!empty($data['password'])) || (($data['id'] == 0)))
            {
                if (mb_strlen($data['password']) < 8)
                {
                    $error = $this->lang->trans('system.user.error.password');
                    $res = false;
                }
            }

            if (($res) && (!$this->checkUserExists($data['username'], $data['email'], (int) $data['id'])))
            {
                $error = $this->lang->trans('system.user.error.userexists');
                $res = false;
            }
        }

        return $res;
    }

    public function saveData(array &$data, string &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $status = \EOS\System\Util\ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0;
            $this->db->beginTransaction();
            $values = [
                'username' => $data['username'],
                'email' => $data['email'],
                'name' => '',
                'surname' => '',
                'id_group' => $data['id_group'],
                'status' => $status,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            if (!empty($data['password']))
            {
                $pass = new \EOS\System\Security\Password(null);
                $values['password'] = $pass->hashPassword($data['password']);
            }
            $tblContent = $this->db->tableFix('#__admin_user');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            $this->db->commit();
            return true;
        }
    }

    public function deleteData(array $data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__admin_user');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function getGroupList(): array
    {
        $q = $this->db->prepare('select id, name, status from #__admin_group order by name');
        $q->execute();
        $g = $q->fetchAll();
        $sa = [];
        $sa['id'] = 0;
        $sa['name'] = $this->lang->trans('system.user.superadmin');
        $sa['status'] = 1;
        return array_merge([$sa], $g);
    }

}
