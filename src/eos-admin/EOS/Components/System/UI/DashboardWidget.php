<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\UI;

use EOS\System\Component\ComponentHelper;

class DashboardWidget extends \EOS\UI\Widget\Widget
{

    private $_sw = [];

    protected function prepare()
    {
        parent::prepare();
        ComponentHelper::includeRoot($this->view->controller->container, 'widget', $this);
    }

    public function addSummaryWidget($colorbox = 'green', $title = '', $data = '', $icon = '', $link = '', $pos = '')
    {
        $this->_sw[] = ['colorbox' => $colorbox, 'title' => $title, 'link' => $link, 'position' => $pos, 'icon' => $icon, 'data' => $data];
    }

    public function writeSummaryWidget()
    {
        usort($this->_sw, function($a, $b)
        {
            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        foreach ($this->_sw as $item)
        {
            printf('<div class="col-lg-3 col-xs-6">');
            printf('<div class="small-box bg-' . $item['colorbox'] . '">');
            printf('<div class="inner">');
            printf('<h3>' . $item['data'] . '</h3>');
            printf('<p>' . $item['title'] . '</p>');
            printf('</div>');
            printf('<div class="icon">');
            printf('<i class="ion ' . $item['icon'] . '"></i>');
            printf('</div>');
            printf('<a href="' . $item['link'] . '" class="small-box-footer">Maggiori informazioni <i class="fa fa-arrow-circle-right"></i></a>');
            printf('</div>');
            printf('</div>');
        }
    }

    public function componentIsActive($componentName)
    {
        return $this->view->controller->container->get('database')->setting->getInt(strtolower($componentName), 'dbversion') > 0;
    }

}
