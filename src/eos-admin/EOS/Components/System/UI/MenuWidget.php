<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\UI;

use EOS\System\Component\ComponentHelper;
use EOS\System\Routing\PathHelper;
use EOS\Components\System\Classes\AuthRole;

class MenuWidget extends \EOS\UI\Widget\Widget
{

    private $_parents = [];

    protected function prepare()
    {
        parent::prepare();
        ComponentHelper::includeRoot($this->view->controller->container, 'menu', $this);
    }

    private function getParentIndex(string $name): int
    {
        $r = -1;
        for ($i = 0; $i < count($this->_parents); $i++)
        {
            if (strtolower($this->_parents[$i]['name']) == strtolower($name))
            {
                $r = $i;
                break;
            }
        }
        return $r;
    }

    public function addParent(string $name, string $title, string $link = '', string $icon = '', $position = null, string $authName = '', string $authRole = AuthRole::READ): int
    {
        $pos = is_null($position) ? count($this->_parents) : (int) $position;
        $idx = $this->getParentIndex($name);
        if ($idx < 0)
        {
            $idx = count($this->_parents);
            $l = $link == '' ? '#' : $link;
            $this->_parents[] = ['name' => $name, 'title' => $title, 'link' => $l, 'position' => $pos, 'icon' => $icon, 'items' => [], 'active' => false, 'authName' => $authName, 'authRole' => $authRole];
        }
        if (!is_null($position))
        {
            $this->_parents[$idx]['position'] = $position;
        }
        return $idx;
    }

    public function addItem(string $parent, string $name, string $title, string $link, string $icon = '', $position = null, string $authName = '', string $authRole = AuthRole::READ)
    {
        $idx = $this->addParent($parent, $parent);
        $pos = is_null($position) ? count($this->_parents[$idx]['items']) : (int) $position;
        $this->_parents[$idx]['items'][] = ['name' => $name, 'title' => $title, 'link' => $link, 'icon' => $icon, 'position' => $pos, 'active' => false, 'authName' => $authName, 'authRole' => $authRole];
    }

    public function write()
    {
        $this->loadActive();
        // parent::write();
        usort($this->_parents, function($a, $b)
        {
            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        for ($i = 0; $i < count($this->_parents); $i++)
        {
            usort($this->_parents[$i]['items'], function($a, $b)
            {
                return ($a['position'] < $b['position']) ? -1 : 1;
            });
        }

        echo '<div class="d-flex sidebar-menu">' . "\n";
        echo '<div class="nav flex-column nav-pills" id="menu-depth-0" role="tablist" aria-orientation="vertical">' . "\n";
        $user = $this->view->controller->user;
        $authParents = [];
        foreach ($this->_parents as $menu)
        {
            if ($user->isAuthorized($menu['authName'], $menu['authRole']))
            {
                $add = true;
                $items = $menu['items'];
                if (!empty($items))
                {
                    $authItems = [];
                    foreach ($items as $item)
                    {
                        if ($user->isAuthorized($item['authName'], $item['authRole']))
                        {
                            $authItems[] = $item;
                        }
                    }
                    $menu['items'] = $authItems;
                    $add = !empty($authItems); 
                }
                if ($add)
                {
                    $authParents[] = $menu;
                }
            }
        }
        foreach ($authParents as $menu)
        {
            $rootActive = $menu['active'] ? 'active' : '';
            $icon = '';
            if ($menu['icon'] != '')
            {
                $icon = '<i class="' . $menu['icon'] . '"></i>';
            }
            if (count($menu['items']) > 0)
            {
                printf('<a class="nav-link %s" id="#pills-%s-tab" href="#pills-%s" data-toggle="pill" role="tab" aria-controls="pills-%s">%s <span>%s</span></a>', $rootActive, $menu['name'], $menu['name'], $menu['name'], $icon, $this->view->encodeHtml($menu['title']));
            } else
            {
                printf('<a class="nav-link %s" href="%s">%s <span>%s</span></a>', $rootActive, $menu['link'], $icon, $this->view->encodeHtml($menu['title']));
            }
        }
        echo '</div>' . "\n";

        echo '<div class="tab-content" id="menu-depth-1">' . "\n";
        foreach ($authParents as $menu)
        {
            if (count($menu['items']) > 0)
            {
                $rootActive = $menu['active'] ? ' show active' : '';
                echo '<div class="tab-pane fade' . $rootActive . '" id="pills-' . $menu['name'] . '" role="tabpanel" aria-labelledby="pills-' . $menu['name'] . '-tab">';
                echo '<span class="tab-pane-title">' . $this->view->encodeHtml($menu['title']) . '</span>';
                foreach ($menu['items'] as $item)
                {
                    $childActive = $item['active'] ? 'active' : '';
                    printf('<a class="%s" href="%s">%s</a>', $childActive, $this->view->encodeHtml($item['link']), $this->view->encodeHtml($item['title'])) . "\n";
                }
                echo '</div>' . "\n";
            }
        }
        echo '</div>' . "\n";
        echo '</div>' . "\n";
    }

    private function loadActive()
    {
        $calcPath = '';
        if (count($this->view->breadcrumbs) > 2)
        {
            $calcPath = $this->view->breadcrumbs[1]['href'];
        } else
        {
            $currentPath = \EOS\System\Routing\PathHelper::removeSlash($this->view->request->getUri()->getPath());
            $l = PathHelper::explodeSlash($currentPath);
            if (count($l) > 2)
            {
                $comp = $l[0];
                array_shift($l);
                $calcPath = $this->view->path->urlFor($comp, $l);
            }
        }
        // Devo calcolare l'url finale per il confronto
        if (!empty($calcPath))
        {

            for ($ir = 0; $ir < count($this->_parents); $ir++)
            {
                for ($ic = 0; $ic < count($this->_parents[$ir]['items']); $ic++)
                {
                    if ($this->_parents[$ir]['items'][$ic]['link'] == $calcPath)
                    {
                        $this->_parents[$ir]['items'][$ic]['active'] = true;
                        $this->_parents[$ir]['active'] = true;
                    }
                }
            }


            /* foreach ($this->_parents as &$parent) {
              $parent['active'] = ($parent['link'] == $calcPath) ? true : false;
              foreach ($parent['items'] as  &$item) {
              $item['active'] = ($item['link'] == $calcPath) ? true : false;
              }
              } */
        }
    }

    public function componentIsActive($componentName)
    {
        return $this->view->controller->container->get('database')->setting->getInt(strtolower($componentName), 'dbversion') > 0;
    }

}
