<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\UI;

use EOS\System\Util\ArrayHelper;

class SeoEditorWidget extends \EOS\UI\Widget\Widget {

	public $data = [];
	public $dataLangDefault = [];
	public $dataLangList = [];

	public function write() {
		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->view->transE('system.seo.meta_title'))->printRender();
			echo '</div>';
			foreach ($this->dataLangList as $l) {
				(new \EOS\UI\Bootstrap\Input('seo-' . $l['id'] . '-meta_title'))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'seo-' . $l['id'] . '-meta_title')->printRender($this->view);
			}
		}, $this)->printRender();

		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->view->transE('system.seo.meta_description'))->printRender();
			echo '</div>';
			foreach ($this->dataLangList as $l) {
				(new \EOS\UI\Bootstrap\Input('seo-' . $l['id'] . '-meta_description'))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'seo-' . $l['id'] . '-meta_description')->printRender($this->view);
			}
		}, $this)->printRender();

		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->view->transE('system.seo.keywords'))->printRender();
			echo '</div>';
			foreach ($this->dataLangList as $l) {
				(new \EOS\UI\Bootstrap\Textarea('seo-' . $l['id'] . '-keywords'))
						->attr('class', 'translatable-field lang-' . $l['id'])
						->bind($this->data, 'seo-' . $l['id'] . '-keywords')->printRender($this->view);
			}
		}, $this)->printRender();

		(new \EOS\UI\Bootstrap\FormGroup())->attr("class", "form-group-custom")->addContent(function () {
			echo '<div class="form-group-title">';
			(new \EOS\UI\Html\Label)->content($this->view->transE('system.seo.meta_raw'))->printRender();
			echo '</div>';
			foreach ($this->dataLangList as $l) {
				$idLng = $l['id'];
				echo '<div id="seo-' . $idLng . '-meta_raw-container" class="translatable-field lang-' . $idLng . '">';
				echo '<input class="seo-metaraw" type="hidden" name="seo-' . $idLng . '-meta_raw">';
				echo '</div>' . "\n";
				$btn = new \EOS\UI\Bootstrap\Button();
				$btn->attr('class', 'btn-success translatable-field lang-' . $idLng);
				$btn->click('function (event){ event.preventDefault(); newSeoRow("' . $idLng . '", 0, "", ""); }');
				$btn->content($this->view->transE('content.content.meta.add'));
				$btn->printRender($this->view);
			}
			echo '<div class="d-none">';
			$sel = new \EOS\UI\Bootstrap\DropDownMenu('lng-seo-metaraw');
			$sel->attr('class', 'select-lang');
			$sel->onChange('function(item) {changeLanguage(item)}');
			$sel->bind($this->dataLangDefault, 'id');
			$sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
			$sel->printRender($this->view);
			echo '</div>';
		}, $this)->printRender();
		$this->view->startCaptureScript();
		echo '<script>' . "\n";
		echo 'function newSeoRow(idlng, mtype, mname, mcontent){' . "\n";
		echo '$("#seo-"+idlng+"-meta_raw-container").append("<div class=\"form-group form-group-custom\">';
		echo '<div class=\"form-row \">';
		echo '<div class=\"col\"><select class=\"custom-select seo-metaraw-sl-type\">';
		echo '<option value=\"0\""+ ((mtype == 0) ? " selected" : "")+">name</option>';
		echo '<option value=\"1\""+ ((mtype == 1) ? " selected" : "")+">property</option>';
		echo '</select></div>';
		echo '<div class=\"col\"><input class=\"form-control seo-metaraw-sl-name\" type=\"text\" value=\""+mname+"\"></div>';
		echo '<div class=\"col-auto\"><label>Content</label></div>';
		echo '<div class=\"col\"><input class=\"form-control seo-metaraw-sl-content\" type=\"text\" value=\""+mcontent+"\"></div>';
		echo '<div class=\"col\"><button class=\"btn btn-danger seo-metaraw-btn-delete\"><i class=\"fa fa-trash\"></i></button></div>';
		echo '</div></div>");';
		echo 'loadSeoDeleteEvent();' . "\n";
		echo 'serializeSeo($("#seo-"+idlng+"-meta_raw-container")[0]);';
		echo '}' . "\n";
		echo 'function loadSeoDeleteEvent(){' . "\n";
		echo '$("button.seo-metaraw-btn-delete").click(function (event) {event.preventDefault(); ' . "\n";
		echo ' var cont = $(this).parent().parent().parent(); ' . "\n";
		echo ' $(this).parent().parent().remove(); ' . "\n";
		echo ' serializeSeo(cont); });' . "\n";
		echo '$(".seo-metaraw-sl-type, .seo-metaraw-sl-name, .seo-metaraw-sl-content").change(function (event) { serializeSeo($(this).parent().parent().parent()); });' . "\n";
		echo '}' . "\n";
		echo 'function serializeSeo(container){' . "\n";
		echo ' var res = []; ' . "\n";
		echo '  $(container).children().each(function (){' . "\n";
		echo '  if (this.tagName.toLowerCase() == "div"){' . "\n";
		echo '  var mtype = $(this).find(".seo-metaraw-sl-type").val();' . "\n";
		echo '  var mname = $(this).find(".seo-metaraw-sl-name").val(); ' . "\n";
		echo '  var mcontent = $(this).find(".seo-metaraw-sl-content").val()' . "\n";
		echo '  var item = { "type": mtype, "name": mname,"content": mcontent};' . "\n";
		echo '  res.push(item);' . "\n";
		echo ' }' . "\n";
		echo '   }); ' . "\n";
		echo ' $(container).find(".seo-metaraw").val(JSON.stringify(res));' . "\n";
		echo '}' . "\n";
		echo '</script>' . "\n";
		$this->view->endCaptureScript();
		$this->view->startCaptureScriptReady();
		foreach ($this->dataLangList as $l) {
			$metaRaw = ArrayHelper::getArray($this->data, 'seo-' . $l['id'] . '-meta_raw');
			foreach ($metaRaw as $item) {
				echo sprintf('newSeoRow("%d", "%s", "%s", "%s");', $l['id'], ArrayHelper::getInt($item, 'type'), $this->view->encodeHtml(ArrayHelper::getStr($item, 'name')), $this->view->encodeHtml(ArrayHelper::getStr($item, 'content'))) . "\n";
			}
		}
		$this->view->endCaptureScriptReady();
	}

}
