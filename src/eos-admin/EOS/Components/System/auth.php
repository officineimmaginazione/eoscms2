<?php

$this->add('system.user', 'system.dashboard.menu,system.dashboard.user');
$this->add('system.group', 'system.dashboard.menu,system.dashboard.group');
$this->add('system.filemanager', 'system.dashboard.menu,system.dashboard.filemanager');
$this->add('system.menu', 'system.dashboard.menu,system.dashboard.menueditor');
$this->add('system.cronjob', 'system.dashboard.menu,system.dashboard.cronjob');
$this->add('system.setting', 'system.dashboard.menu,system.dashboard.setting');
