<?php

return
    [
        'system.access.login' => 'Login',
        'system.access.login-error' => 'Invalid user name or password!'
];
