<?php

return
    [
        'system.access.login'				=> 'Accedi',
        'system.access.login-error' => 'Utente o password non validi!',
        'system.access.username'		=> 'Nome utente',
        'system.access.password'		=> 'Password',
        'system.access.logout'		=> 'Logout'
        
];
