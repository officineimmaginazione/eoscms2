<?php

return
    [
        'system.common.invalid.token' => 'Token di protezione non valido, ricarica la pagina o cambia browser!',
        'system.common.invalid.json' => 'JSON non valido!',
        'system.common.invalid.multipart' => 'MultiPart non valido!',
        'system.common.invalid.origin' => 'Origine della richiesta non valida, ricarica la pagina o cambia browser!',
        'system.common.save' => 'Salva',
        'system.common.cancel' => 'Annulla',
        'system.common.edit' => 'Modifica',
        'system.common.delete' => 'Elimina',
        'system.common.insert' => 'Inserimento',
        'system.common.data' => 'Dati',
        'system.common.extra' => 'Extra',
        'system.common.seo' => 'SEO',
        'system.common.advanced' => 'Avanzate',
        'system.common.active' => 'Attivo',
        'system.common.inactive' => 'Non attivo',
        'system.common.yes' => 'Si',
        'system.common.no' => 'No',
        'system.common.home' => 'Home',
        'system.common.test' => 'Test',
        'system.common.price' => 'Prezzi',
        'system.common.combination' => 'Combinazioni',
        'system.common.status' => 'Stato',
        'system.common.language' => 'Lingua',
        'system.common.standard' => 'Impostazioni di base',
        'system.common.accessdenied' => 'Accesso negato alla risorsa: '
];
