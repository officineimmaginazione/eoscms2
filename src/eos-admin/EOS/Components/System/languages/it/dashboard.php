<?php

return
    [
        'system.dashboard.dashboard' => 'Dashboard',
        'system.dashboard.home' => 'Home',
        'system.dashboard.menu' => 'Sistema',
        'system.dashboard.user' => 'Utenti',
        'system.dashboard.group' => 'Gruppi',
        'system.dashboard.menueditor' => 'Menu',
        'system.dashboard.position' => 'Posizioni',
        'system.dashboard.filemanager' => 'File manager',
        'system.dashboard.cronjob' => 'Cron Jobs',
        'system.dashboard.setting' => 'Impostazioni'
];
