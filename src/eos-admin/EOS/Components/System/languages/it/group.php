<?php

return
    [
        'system.dashboard.group' => 'Gruppi utente',
        'system.group.index.title' => 'Gruppi',
        'system.group.edit.title' => 'Modifica gruppo',
        'system.group.insert.title' => 'Nuovo gruppo',
        'system.group.field.id' => 'ID',
        'system.group.field.name' => 'Nome',
        'system.group.field.status' => 'Attivo',
        'system.group.field.role.name' => 'Nome',
        'system.group.field.role.create' => 'Crea',
        'system.group.field.role.read' => 'Leggi',
        'system.group.field.role.update' => 'Modifica',
        'system.group.field.role.delete' => 'Elimina',
        'system.group.field.role.all' => 'Tutto'
];
