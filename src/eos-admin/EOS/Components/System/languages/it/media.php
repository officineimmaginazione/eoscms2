<?php

return
    [
        'system.media.image.error.uploaderror' => 'Errore imprevisto durante l\'upload!',
        'system.media.image.error.mimetype' => 'Formato immagine non supportato!',
        'system.media.image.error.size' => 'Ridurre le dimensioni del file!',
        'system.media.image.error.filenotfound' => 'Errore imprevisto non trovo il file scaricato!',
        'system.media.image.delete' => 'Vuoi cancellare l\' immagine?',
        'system.media.attachment.error.uploaderror' => 'Errore imprevisto durante l\'upload!',
        'system.media.attachment.error.mimetype' => 'Formato media non supportato!',
        'system.media.attachment.error.size' => 'Ridurre le dimensioni del file!',
        'system.media.attachment.error.filenotfound' => 'Errore imprevisto non trovo il file scaricato!',
        'system.media.attachment.delete' => 'Vuoi cancellare il file?'
];
