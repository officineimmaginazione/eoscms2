<?php

return
    [
        'system.menu.index.title' => 'Menu',
        'system.menu.field.id' => 'ID',
        'system.menu.field.name' => 'Nome',
        'system.menu.field.status' => 'Attivo',
        'system.menu.field.css' => 'CSS class',
        'system.menu.field.idhtml' => 'ID html',
        'system.menu.field.path' => 'Percorso',
        'system.menu.field.title' => 'Titolo',
        'system.menu.insert.title' => 'Inserisci menu',
        'system.menu.edit.title' => 'Modifica menu',
        'system.menu.error.name' => 'Nome non valido!',
        'system.menu.delete.confirm' => 'Vuoi cancellare il menu?',
        'system.menu.delete.item.confirm' => 'Vuoi eliminare la voce di menu?'
];
