<?php

return
    [
        'system.position.index.title' => 'Posizioni',
        'system.position.field.id' => 'ID',
        'system.position.field.name' => 'Nome',
        'system.position.field.widget_class' => 'Classe widget',
        'system.position.field.widget_name' => 'Nome/ID widget',
        'system.position.field.ordering' => 'Ordine',
        'system.position.field.status' => 'Attivo',
        'system.position.field.iso_code' => 'Lingua',
        'system.position.insert.title' => 'Inserisci posizione',
        'system.position.edit.title' => 'Modifica menu',
        'system.position.error.name' => 'Nome non valido!',
        'system.position.field.status.none' => 'Nessuna pagina',
        'system.position.field.status.all' => 'Tutte',
        'system.position.field.status.select' => 'Seleziona',
        'system.position.field.status.exclude' => 'Escludi',
        'system.position.delete.confirm' => 'Vuoi cancellare il menu?'
];
