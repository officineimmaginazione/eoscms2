<?php

return
    [
        'system.seo.meta_title' => 'Meta title',
        'system.seo.meta_description' => 'Meta description',
        'system.seo.keywords' => 'Keywords',
        'system.seo.meta_raw' => 'Meta personalizzati'
];
