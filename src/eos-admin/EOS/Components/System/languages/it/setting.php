<?php

return
    [
        'system.setting.index.title' => 'Impostazioni',
        'system.setting.field.object' => 'Oggetto',
        'system.setting.field.name' => 'Chiave',
        'system.setting.field.value' => 'Valore',
        'system.setting.add.confirm' => 'Aggiungi valore',     
        'system.setting.delete.confirm' => 'Cancella valore',
        'system.setting.theme' => 'Tema',
        'system.setting.email' => 'Impostazioni e-mail',
        'system.setting.email.host' => 'Host',
        'system.setting.email.port' => 'Porta',
        'system.setting.email.auth' => 'Autenticazione',
        'system.setting.email.secure' => 'Sicurezza',
        'system.setting.email.username' => 'Utente',
        'system.setting.email.password' => 'Password',
        'system.setting.email.from' => 'Mittente predefinito',
        'system.setting' => 'Impostazioni di sistema'
];
