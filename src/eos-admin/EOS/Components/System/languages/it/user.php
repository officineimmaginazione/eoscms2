<?php

return
    [
        'system.user.index.title' => 'Utenti',
        'system.user.edit.title' => 'Modifica utente',
        'system.user.insert.title' => 'Inserisci utente',
        'system.user.field.id' => 'ID',
        'system.user.field.username' => 'Utente',
        'system.user.field.email' => 'E-mail',
        'system.user.field.status' => 'Attivo',
        'system.user.field.ugroup' => 'Gruppo',
        'system.user.field.password' => 'Password',
        'system.user.error.username' => 'Utente non valido!',
        'system.user.error.email' => 'Email non valida!',
        'system.user.error.password' => 'Password non valida! (Almeno 8 caratteri)',
        'system.user.error.userexists' => 'Utente già inserito!',
        'system.user.delete.confirm' => 'Vuoi cancellare l\'utente "definitivamente"?',
        'system.user.superadmin' => 'Super amministratore'
];
