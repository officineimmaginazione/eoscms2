<?php

if ($this->componentIsActive('system'))
{
    $this->addParent('home', '', $this->view->path->urlFor('system', ['dashboard', 'index']), 'logo-icon', 1);
    $this->addParent('dashboard', $this->view->trans('system.dashboard.dashboard'), $this->view->path->urlFor('system', ['dashboard', 'index']), 'fa fa-tachometer', 2);
    $this->addParent('system', $this->view->trans('system.dashboard.menu'), '', 'fa fa-wrench', 100);
    $this->addItem('system', 'user', $this->view->trans('system.dashboard.user'), $this->view->path->urlFor('system', ['user', 'index']), 'fa fa-user', null, 'system.user');
    $this->addItem('system', 'group', $this->view->trans('system.dashboard.group'), $this->view->path->urlFor('system', ['group', 'index']), 'fa fa-user', null, 'system.group');
    $this->addItem('system', 'menu', $this->view->trans('system.dashboard.menueditor'), $this->view->path->urlFor('system', ['menu', 'index']), 'fa fa-bars', null, 'system.menu');
    //$this->addItem('system', 'menu', $this->view->trans('system.dashboard.position'), $this->view->path->urlFor('system', ['position', 'index']), 'fa fa-outdent');
    $this->addItem('system', 'filemanager', $this->view->trans('system.dashboard.filemanager'), $this->view->path->urlFor('system', ['filemanager', 'index']), 'fa fa-files-o', null, 'system.filemanager');
    $this->addItem('system', 'cronjob', $this->view->trans('system.dashboard.cronjob'), $this->view->path->urlFor('system', ['cronjob', 'index']), 'fa fa-file-import', null, 'system.cronjob');
    $this->addItem('system', 'setting', $this->view->trans('system.dashboard.setting'), $this->view->path->urlFor('system', ['setting', 'index']), 'fa fa-cogs', null, 'system.setting');
}
