<?php

use \EOS\Components\System\Classes\AuthRole;

$this->mapComponent(['GET'], 'System', ['access/login' => 'AccessController:login', 'access/logout' => 'AccessController:logout']);
$this->mapComponent(['POST'], 'System', ['/access/signin/' => 'AccessController:signin']);
$this->mapComponent(['GET'], 'System', ['dashboard/index' => 'DashboardController:index']);

$this->mapComponent(['GET'], 'System', ['user/index' => 'UserController:index'], ['auth' => ['system.user' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'System', ['user/edit/{id}' => 'UserController:edit'], ['auth' => ['system.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['user/ajaxlist' => 'UserController:ajaxList'], ['auth' => ['system.user' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'System', ['user/ajaxsave' => 'UserController:ajaxSave'], ['auth' => ['system.user' => [AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['user/ajaxdelete' => 'UserController:ajaxDelete'], ['auth' => ['system.user' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'System', ['group/index' => 'GroupController:index'], ['auth' => ['system.group' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'System', ['group/edit/{id}' => 'GroupController:edit'], ['auth' => ['system.group' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['group/ajaxlist' => 'GroupController:ajaxList'], ['auth' => ['system.group' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'System', ['group/ajaxsave' => 'GroupController:ajaxSave'], ['auth' => ['system.group' => [AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['group/ajaxdelete' => 'GroupController:ajaxDelete'], ['auth' => ['system.group' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'System', ['resource/app.js' => 'ResourceController:appJS']);
$this->mapComponent(['GET'], 'System', ['resource/keepalive.js' => 'ResourceController:keepAliveJS']);

$this->mapComponent(['GET'], 'System', ['setting/index' => 'SettingController:index'], ['auth' => ['system.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'System', ['setting/ajaxlist' => 'SettingController:ajaxList'], ['auth' => ['system.setting' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'System', ['setting/ajaxsave' => 'SettingController:ajaxSave'], ['auth' => ['system.setting' => [AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['setting/ajaxdelete' => 'SettingController:ajaxDelete'], ['auth' => ['system.setting' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'System', ['setting/ajaxsavedata' => 'SettingController:ajaxSaveData'], ['auth' => ['system.setting' => [AuthRole::UPDATE]]]);
$this->mapComponent(['POST'], 'System', ['setting/ajaxtestmail' => 'SettingController:ajaxTestMail'], ['auth' => ['system.setting' => [AuthRole::UPDATE]]]);

$this->mapComponent(['GET'], 'System', ['menu/index' => 'MenuController:index'], ['auth' => ['system.menu' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'System', ['menu/edit/{id}' => 'MenuController:edit'], ['auth' => ['system.menu' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['menu/ajaxsave' => 'MenuController:ajaxSave'], ['auth' => ['system.menu' => [AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'System', ['menu/ajaxdelete' => 'MenuController:ajaxDelete'], ['auth' => ['system.menu' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'System', ['menu/menu.js' => 'MenuController:menuJS'], ['auth' => ['system.menu' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'System', ['menu/menu.css' => 'MenuController:menuCSS'], ['auth' => ['system.menu' => [AuthRole::READ]]]);

//$this->mapComponent(['GET'], 'System', ['position/index' => 'PositionController:index']);
//$this->mapComponent(['GET'], 'System', ['position/edit/{id}' => 'PositionController:edit']);
//$this->mapComponent(['POST'], 'System', ['position/ajaxsave' => 'PositionController:ajaxSave']);
//$this->mapComponent(['POST'], 'System', ['position/ajaxdelete' => 'PositionController:ajaxDelete']);

$this->mapComponent(['GET'], 'System', ['filemanager/index' => 'FilemanagerController:index'], ['auth' => ['system.filemanager' => [AuthRole::READ]]]);

$this->mapComponent(['GET', 'POST'], 'System', ['filemanager/ajax' => 'FilemanagerController:ajax'], ['auth' => ['system.filemanager' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE, AuthRole::DELETE]]]);
$this->mapComponent(['GET'], 'System', ['filemanager/config.js' => 'FilemanagerController:configJS'], ['auth' => ['system.filemanager' => [AuthRole::READ]]]);

$this->mapComponent(['GET'], 'System', ['service/keepalive' => 'ServiceController:keepAlive']);


$this->mapComponent(['GET'], 'System', ['cronjob/index' => 'CronJobController:index'], ['auth' => ['system.cronjob' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'System', ['cron/run/{job}' => 'CronController:run'], []);