<section class="mx-auto login-box">
    <div class="login-logo">
        <a href=""><img class="img-responsive" src="<?php echo $this->path->getThemeUrl() . 'img/eos-cms-logo.png'; ?>" alt="EOS Cms" /></a>
    </div>
    <div class="login-box-body">
        <?php
        $form = (new \EOS\UI\Html\Form())->id('form-login');
        $form->startContent();
        (new \EOS\UI\Bootstrap\FormGroup())->attr('class', 'has-feedback')->addContent(function ()
        {
						(new \EOS\UI\Html\Label)->content($this->transE('system.access.username'))->attr("class", "login-label")->printRender();
            (new \EOS\UI\Bootstrap\Input('email'))->placeholder('email')->printRender();
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\FormGroup())->attr('class', 'has-feedback')->addContent(function ()
        {
						(new \EOS\UI\Html\Label)->content($this->transE('system.access.password'))->attr("class", "login-label")->printRender();
            (new \EOS\UI\Bootstrap\Input('password'))->type('password')->placeholder('password')->printRender();
        }, $this)->printRender();
        (new \EOS\UI\Bootstrap\Button())
            ->content('Login')
            ->attr('class', 'btn-primary')
            ->printRender($this);
        $this->writeTokenHtml();
        $form->endContent();
        $form->printRender($this);
        ?>
    </div>
</section>
<?php $this->startCaptureScript(); ?>
<script>
    $('#form-login').submit(function (e)
    {
        //e.preventDefault();
        var data = $(this).serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('system', ['access', 'signin']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                }
                else
                {
                    bootbox.alert('<div class="icon"><i class="fas fa-exclamation-circle"></i></div><div class="message">' + json.message + '</div>');
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    });
</script>
<?php
$this->endCaptureScript();
