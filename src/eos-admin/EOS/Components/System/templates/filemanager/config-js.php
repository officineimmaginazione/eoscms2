<?php
//   "capabilities": ["select", "download", "rename", "delete", "replace"],
$capabilites = [];
// Di default metto lettura altrimenti se nullo ha tutte le autorizzazioni
$capabilites[] = 'select';  
$capabilites[] = 'download'; 
if ($this->user->isAuthorizedCreate('system.filemanager'))
{
  
}
if ($this->user->isAuthorizedUpdate('system.filemanager'))
{
  $capabilites[] = 'rename';  
  $capabilites[] = 'replace'; 
}
if ($this->user->isAuthorizedDelete('system.filemanager'))
{
  $capabilites[] = 'delete';  
}
$capabilitesStr = json_encode($capabilites);
?>{
       "options": {
        "culture": "<?php echo $this->lang->getCurrentISO();?>",
        "lang": "php",
        "theme": "flat-dark",
        "defaultViewMode": "grid",
        "autoload": true,
        "showFullPath": false,
        "showTitleAttr": false,
        "browseOnly": false,
        "showConfirmation": true,
        "showThumbs": true,
        "generateThumbnails": true,
        "cacheThumbnails": true,
        "searchBox": true,
        "listFiles": true,
        "fileSorting": "default",
        "chars_only_latin": true,
        "splitterWidth": 200,
        "splitterMinWidth": 200,
        "dateFormat": "d M Y H:i",
        "serverRoot": true,
        "fileRoot": "/",
        "baseUrl": "<?php echo $this->path->getUploadsUrl();?>",
        "logger": false,
        "capabilities": <?php echo $capabilitesStr; ?>,
        "plugins": [],
        "fileConnector": "<?php echo $this->path->getUrlFor('system', 'filemanager/ajax');?>"
    },
    "security": {
        "allowFolderDownload": false,
        "allowChangeExtensions": false,
        "allowNoExtension": false,
        "uploadPolicy": "DISALLOW_ALL",
        "uploadRestrictions": [
            "jpg",
            "jpe",
            "jpeg",
            "gif",
            "png",
            "svg",
            "txt",
            "pdf",
            "odp",
            "ods",
            "odt",
            "rtf",
            "doc",
            "docx",
            "xls",
            "xlsx",
            "ppt",
            "pptx",
            "csv",
            "ogv",
            "mp4",
            "webm",
            "m4v",
            "ogg",
            "mp3",
            "wav",
            "zip",
            "rar"
        ]
    },
    "upload": {
        "multiple": false,
        "number": 5,
        "overwrite": false,
        "imagesOnly": false,
        "fileSizeLimit": 16
    },
    "exclude": {
        "unallowed_files": [
            ".htaccess",
            "web.config"
        ],
        "unallowed_dirs": [
            "_thumbs",
            ".CDN_ACCESS_LOGS",
            "cloudservers"
        ],
        "unallowed_files_REGEXP": "/^\\./",
        "unallowed_dirs_REGEXP": "/^\\./"
    },
    "images": {
        "imagesExt": [
            "jpg",
            "jpe",
            "jpeg",
            "gif",
            "png",
            "svg"
        ],
        "resize": {
        	"enabled":true,
        	"maxWidth": 1280,
            "maxHeight": 1024
        }
    },
    "videos": {
        "showVideoPlayer": false,
        "videosExt": [
            "ogv",
            "mp4",
            "webm",
            "m4v"
        ],
        "videosPlayerWidth": 400,
        "videosPlayerHeight": 222
    },
    "audios": {
        "showAudioPlayer": false,
        "audiosExt": [
            "ogg",
            "mp3",
            "wav"
        ]
    },
    "pdfs": {
        "showPdfReader": false,
        "pdfsExt": [
            "pdf",
            "odp"
        ],
	    "pdfsReaderWidth": "640",
        "pdfsReaderHeight": "480"	
    },
    "edit": {
        "enabled": false,
        "lineNumbers": true,
        "lineWrapping": true,
        "codeHighlight": false,
        "theme": "elegant",
        "editExt": [
            "txt",
            "csv"
        ]
    },
    "customScrollbar": {
    	"enabled": false,
    	"theme": "inset-2-dark",
    	"button": true
    },
    "extras": {
        "extra_js": [],
        "extra_js_async": true
    },
    "icons": {
        "path": "<?php echo $this->path->getLibraryUrl().'filemanager/e1.0/';?>images/fileicons/",
        "directory": "_Open.png",
        "default": "default.png"
    }
}