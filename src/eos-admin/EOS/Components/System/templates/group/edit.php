<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.group.field.name'))->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('name'))
        ->bind($this->data, 'name')->printRender($this);
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.group.field.status'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Checkbox('status'))
        ->bind($this->data, 'status')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<table class="table table-hover">';
    echo '<thead><tr>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.name') . '</th>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.create') . '</th>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.read') . '</th>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.update') . '</th>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.delete') . '</th>';
    echo '<th scope="col">' . $this->transE('system.group.field.role.all') . '</th>';
    echo '</tr></thead><tbody>';
    $vList = [];
    foreach ($this->data['authorization'] as $name => $r)
    {
        $r['name'] = $name;
        $vList[] = $r;
    }
    
    usort($vList, function ($a, $b)
    {
        return strcmp($a["lang"], $b["lang"]);
    });

    foreach ($vList as $r)
    {
        $name = $r['name'];
        echo '<tr data-name="' . $this->encodeHtml($name) . '">';
        echo '<td>' . $this->encodeHtml($r['lang']) . '</td>';
        $roles = \EOS\Components\System\Classes\AuthRole::getList();
        foreach ($roles as $role)
        {
            echo '<td>';
            (new \EOS\UI\Bootstrap\Checkbox($this->createID()))
                ->name('auth_' . $name . '_' . $role)
                ->bind(\EOS\System\Util\ArrayHelper::getArray($r, 'authList'), $role)
                ->attr('data-role', $role)
                ->disabled(!in_array($role, $r['roles']))
                ->printRender($this);
            echo '</td>';
        }
        echo '</tr>';
    }
    echo '</tbody></table>';
}, $this)->printRender();

$box->endContent();
$box->startFooter();
if ((isset($this->data['id'])) && ($this->data['id'] != 0))
{
    (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
}
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
(new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('System', 'group/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('System', 'group/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('system.group.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('System', 'group/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json"
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
    $(function ()
    {
        $('input').on('ifChanged', function (event)
        {
            $(event.target).trigger('change');
        });
        $('input[data-role="all"]').on('change', function ()
        {
            if ($(this).is(':checked'))
            {
                $(this).closest('tr').find('input:enabled').prop('checked', true).iCheck('update');
            }
        });
    });
</script>
<?php
$this->endCaptureScript();
