<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    (new \EOS\UI\Html\Label())->content($this->transE('system.menu.index.title'))->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-info btn-sm pull-right')
        ->click('function (e) { addNew();}')
        ->content('<i class="fa fa-plus"></i>')
        ->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable');
$tbl->addRawParams('ordering', 'false');
$tbl->addRawParams('paging', 'false');
$tbl->addRawParams('info', 'false');
$tbl->addRawParams('bFilter', 'false');
$tbl->addColumn('id', $this->transE('system.menu.field.id'));
$tbl->addColumn('name', $this->transE('system.menu.field.name'));
$tbl->addColumn('status', $this->transE('system.menu.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->bindList($this->items);
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('system', 'menu/edit/0') ?>";
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->getUrlFor('system', 'menu/edit') ?>" + id + "/";
    }

    function renderStatus(data)
    {
        if (data == 1)
        {
            return '<span class="label label-success"><?php $this->transP('system.common.active'); ?></span>';
        } else
        {
            return '<span class="label label-danger"><?php $this->transP('system.common.inactive'); ?></span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
