<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.menu.field.name'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('name'))
        ->bind($this->data, 'name')->printRender($this);
    echo '</div>';
    echo '<div class="col-xs-2">';
    (new \EOS\UI\Bootstrap\Checkbox('status'))
        ->bind($this->data, 'status')
        ->printRender($this);
    $sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
    $sel->attr('class', 'select-lang');
    $sel->onChange('function(item) {changeLanguage(item);setLang(item);}');
    $sel->bind($this->dataLangDefault, 'id');
    $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
    $sel->printRender($this);
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.menu.field.idhtml'))
        ->attr('class', 'col-xs-2 text-right')
        ->printRender();
    echo '<div class="col-xs-8">';
    foreach ($this->dataLangList as $l)
    {
        echo '<div class="translatable-field lang-' . $l['id'] . '">';
        (new \EOS\UI\Bootstrap\Input('options-' . $l['id'] . '-id'))
            ->attr('class', 'translatable-field lang-' . $l['id'])
            ->bind($this->data, 'options-' . $l['id'] . '-id')
            ->printRender($this);
        echo '</div>';
    }
    echo '</div>';
    echo '<div class="col-xs-2">';
    $sel = new EOS\UI\Bootstrap\DropDownMenu('lng-options-css');
    $sel->attr('class', 'select-lang');
    $sel->onChange('function(item) {changeLanguage(item);setLang(item);}');
    $sel->bind($this->dataLangDefault, 'id');
    $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
    $sel->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.menu.field.css'))
        ->attr('class', 'col-xs-2 text-right')
        ->printRender();
    echo '<div class="col-xs-8">';
    foreach ($this->dataLangList as $l)
    {
        echo '<div class="translatable-field lang-' . $l['id'] . '">';
        (new \EOS\UI\Bootstrap\Input('options-' . $l['id'] . '-class'))
            ->attr('class', 'translatable-field lang-' . $l['id'])
            ->bind($this->data, 'options-' . $l['id'] . '-class')
            ->printRender($this);
        echo '</div>';
    }
    echo '</div>';
    echo '<div class="col-xs-2">';
    $sel = new EOS\UI\Bootstrap\DropDownMenu('lng-options-css');
    $sel->attr('class', 'select-lang');
    $sel->onChange('function(item) {changeLanguage(item);setLang(item);}');
    $sel->bind($this->dataLangDefault, 'id');
    $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
    $sel->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-8">';
    echo '</div>';
}, $this)->printRender();
?>

<button class="btn btn-sm" id="btn-menu-expand-all"><i class="fa fa-plus-square-o"></i></button>
<button class="btn btn-sm" id="btn-menu-collapse-all"><i class="fa fa-minus-square-o"></i></button>
<button id="btn-new" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button>
<?php
foreach ($this->dataLangList as $l)
{
    (new \EOS\UI\Html\Input())->name('data-' . $l['id'])->id('data-' . $l['id'])->type('hidden')->printRender();
// Menu
    echo '<div class="translatable-field lang-' . $l['id'] . '">';
    ?>
    <div class="dd" id="nestable-<?php echo $l['id']; ?>">

    </div>

    <?php
    echo '</div>';
}
//
$box->endContent();
$box->startFooter();
if ((isset($this->data['id'])) && ($this->data['id'] != 0))
{
    (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
}
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
(new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id']; ?>;
    var routers = [];
<?php
foreach ($this->routers as $k => $v)
{
    ?>
        routers[<?php echo $k; ?>] = <?php echo json_encode($v); ?>;
    <?php
}
?>

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
        }
    }

    function saveData()
    {
<?php
foreach ($this->dataLangList as $l)
{
    ?>
            $('#data-<?php echo $l['id']; ?>').val(
                JSON.stringify(serializeItemContainer($('#nestable-<?php echo $l['id']; ?> ol')[0]))
                );

<?php } ?>

        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('System', 'menu/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('System', 'menu/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('system.menu.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('System', 'menu/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function addSelectLang(sl, idlng, selvalue)
    {
        langArray = routers[idlng];
        var opt = document.createElement("option");
        opt.value = '';
        opt.innerHTML = '---------------';
        sl.appendChild(opt);

        for (idx = 0; idx < langArray.length; idx++)
        {
            var opt = document.createElement("option");
            opt.value = langArray[idx].route;
            opt.innerHTML = langArray[idx].path + ' [' + opt.value + ']';
            if (opt.value === selvalue)
            {
                $(opt).prop("selected", "selected");
            }
            sl.appendChild(opt);
        }
    }

    function addItem(parent, path, title, custom, idlng)
    {
        var li = document.createElement('li');
        parent.appendChild(li);
        var strcustom = '';
        if (custom)
        {
            strcustom = ' checked';
        }
        li.className = 'dd-item';
        var el = '<div class="dd-handle"></div>' +
            '<label><?php $this->transPE('system.menu.field.path'); ?></label>' +
            '<select class="menu-item-router"></select>' +
            '<input type="text" class="menu-item-path">' +
            '<input type="checkbox" class="menu-item-custom"' + strcustom + '>' +
            '<label><?php $this->transPE('system.menu.field.title'); ?></label>' +
            '<input type="text" class="menu-item-title">';
        $(li).append(el);
        var btn = document.createElement('div');
        btn.className = 'btn btn-danger btn-sm pull-right remove-menu';
        $(btn).append('<i class="fa fa-trash"></i>');
        $(btn).click(function (event)
        {
            event.preventDefault();
            var node = $(this);
            bootbox.confirm("<?php $this->transPE('system.menu.delete.item.confirm') ?>", function (result)
            {
                if (result)
                {
                    var ct = node.closest('li').parent('ol');
                    node.closest('li').remove();
                    if ((ct.children().length === 0) && (ct.parent().nodeName === 'li'))
                    {
                        ct.remove();
                    } else
                    {
                        resetOrder(ct);
                    }
                }
            });
        });
        sl = $(li).find('.menu-item-router:first');
        ph = $(li).find('.menu-item-path:first');
        chk = $(li).find('.menu-item-custom:first');
        tit = $(li).find('.menu-item-title:first');
        addSelectLang(sl[0], idlng, path);
        li.appendChild(btn);
        $(sl).change(function (event)
        {
            $(this).closest('li').find('.menu-item-path:first').val($(this).val());
        }
        );
        $(chk).change(function (event)
        {
            var sl = $(this).closest('li').find('.menu-item-router:first');
            var ph = $(this).closest('li').find('.menu-item-path:first');
            if ($(this).is(':checked'))
            {
                sl.hide();
                ph.show();
            } else
            {
                sl.show();
                ph.hide();
            }
        }
        );

        ph.val(path);
        if (custom)
        {
            sl.hide();
        } else
        {
            ph.hide();
        }
        tit.val(title);
        return li;
    }

    function addItemContainer(parent)
    {
        var ol = document.createElement('ol');
        ol.className = 'dd-list';
        parent.appendChild(ol);
        return ol;
    }

    function resetOrder(parent)
    {
        $(parent).children().each(function (index)
        {
            $(this).find('.dd-handle:first').html(index + 1);
            var olc = $(this).children('ol');
            if (olc.length > 0)
            {
                resetOrder(olc[0]);
                $(this).find('.remove-menu:first').hide();
            } else
            {
                $(this).find('.remove-menu:first').show();
            }
        });
    }

    function serializeItemContainer(container)
    {
        var res = [];
        $(container).children().each(function ()
        {
            var path = $(this).find('.menu-item-path:first').val();
            var title = $(this).find('.menu-item-title:first').val();
            var custom = $(this).find('.menu-item-custom:first').is(':checked');
            var children = null;
            var olc = $(this).children('ol');
            if (olc.length > 0)
            {
                var children = serializeItemContainer(olc[0]);
            }
            var item = {
                "path": path,
                "custom": custom,
                "title": title,
                "children": children
            };
            res.push(item);
        });
        return res;
    }

    function deserializeItemContainer(container, jitem, idlang)
    {
        for (var i = 0; i < jitem.length; i++)
        {
            var li = addItem(container, jitem[i].path, jitem[i].title, jitem[i].custom, idlang);
            if (jitem[i].children != null)
            {
                var lo = addItemContainer(li);
                deserializeItemContainer(lo, jitem[i].children, idlang);
            }
        }
    }

    $(function ()
    {
        $('#btn-menu-expand-all').click(function (event)
        {
            event.preventDefault();
            $('.dd').nestable('expandAll');
        });

        $('#btn-menu-collapse-all').click(function (event)
        {
            event.preventDefault();
            $('.dd').nestable('collapseAll');
        });

<?php
foreach ($this->dataLangList as $l)
{
    ?>
            addItemContainer($('#nestable-<?php echo $l['id']; ?>')[0]);

            $('#nestable-<?php echo $l['id']; ?>').nestable({
                group: 1
            }).on('change', function (e)
            {
                resetOrder($('#nestable-<?php echo $l['id']; ?> ol')[0]);
            });
    <?php
    if (isset($this->data['data-' . $l['id']]))
    {
        ?>
                var data = <?php echo json_encode($this->data['data-' . $l['id']]['menu']); ?>;
                var ol = $('#nestable-<?php echo $l['id']; ?> ol')[0];
                deserializeItemContainer(ol, data, <?php echo $l['id']; ?>);
                resetOrder(ol);
    <?php } ?>
<?php } ?>

        $('#btn-new').click(function (event)
        {
            event.preventDefault();
            var ol = $('#nestable-' + id_lang + ' ol')[0];
            addItem(ol, '', '', false, id_lang);
            resetOrder(ol);
        });

    });
</script>
<?php
$this->endCaptureScript();
