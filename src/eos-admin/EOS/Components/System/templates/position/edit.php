<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.name'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('name'))
        ->bind($this->data, 'name')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.widget_class'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('widget_class'))
        ->bind($this->data, 'widget_class')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.widget_name'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('widget_name'))
        ->bind($this->data, 'widget_name')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.status'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Checkbox('status'))
        ->bind($this->data, 'status')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-sm-1">';
    $sel = new EOS\UI\Bootstrap\DropDownMenu('lng');
    $sel->attr('class', 'select-lang');
    $sel->bind($this->dataLangDefault, 'id');
    $sel->bindList($this->dataLangList, 'id', 'iso_code', 'name');
    $sel->onChange('function(item) {changeLanguage(item);setLang(item);}');
    $sel->printRender($this);
    echo '</div>';
    foreach ($this->dataLangList as $l)
    {
        $itemDiv = (new EOS\UI\Html\Div())->attr('class', 'translatable-field lang-' . $l['id']);
        $itemDiv->startContent();
        (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.status.none'))->attr('class', 'col-sm-2 text-right')->printRender();
        echo '<div class="col-sm-1">';
        (new \EOS\UI\Bootstrap\Radio('filter_type-' . $l['id']))
            ->id('0-filter_type-' . $l['id'])
            ->value('0')
            ->bind($this->data, 'filter_type-' . $l['id'])
            ->printRender($this);
        echo '</div>';
        (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.status.all'))->attr('class', 'col-sm-1 text-right')->printRender();
        echo '<div class="col-sm-1">';
        (new \EOS\UI\Bootstrap\Radio('filter_type-' . $l['id']))
            ->id('1-filter_type-' . $l['id'])
            ->value('1')
            ->bind($this->data, 'filter_type-' . $l['id'])
            ->printRender($this);
        echo '</div>';
        (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.status.select'))->attr('class', 'col-sm-1 text-right')->printRender();
        echo '<div class="col-sm-1">';
        (new \EOS\UI\Bootstrap\Radio('filter_type-' . $l['id']))
            ->id('2-filter_type-' . $l['id'])
            ->value('2')
            ->bind($this->data, 'filter_type-' . $l['id'])
            ->printRender($this);
        echo '</div>';
        (new \EOS\UI\Html\Label)->content($this->transE('system.position.field.status.exclude'))->attr('class', 'col-sm-1 text-right')->printRender();
        echo '<div class="col-sm-1">';
        (new \EOS\UI\Bootstrap\Radio('filter_type-' . $l['id']))
            ->id('3-filter_type-' . $l['id'])
            ->value('3')
            ->bind($this->data, 'filter_type-' . $l['id'])
            ->printRender($this);
        echo '</div>';
        $itemDiv->endContent()->printRender();
    }
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-8">';
    echo '</div>';
}, $this)->printRender();
$box->endContent();
$box->startFooter();
if ((isset($this->data['id'])) && ($this->data['id'] != 0))
{
    (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
}
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
(new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
(new \EOS\UI\Html\Input())->name('id_lang')->id('id_lang')->bind($this->data, 'id_lang')->type('hidden')->printRender();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id']; ?>;
    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
        }
    }
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('System', 'position/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('System', 'position/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('system.position.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('System', 'position/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();
