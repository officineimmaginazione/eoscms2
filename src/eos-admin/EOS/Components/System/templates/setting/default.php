<?php
$tab = new EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-adv', $this->trans('system.common.advanced'));
$tab->startTab();
$formData = new EOS\UI\Html\Form();
$formData->id('data-form');
$formData->attr('method', 'POST');
$formData->attr('class', 'form-horizontal');
$formData->startContent();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.theme'))->attr('class', 'col-xs-2 text-right')->printRender();

    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Select('setting-theme'))
        ->bind($this->data, 'setting-theme')
        ->bindList($this->themeList, 'name', 'name')
        ->printRender($this);
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email'))->attr('class', 'col-xs-2 text-right')->printRender();
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.host'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-4">';
    (new \EOS\UI\Bootstrap\Input('email_setting-host'))
        ->bind($this->data, 'email_setting-host')
        ->printRender($this);
    echo '</div>';
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.port'))->attr('class', 'col-xs-1 text-right')->printRender();
    echo '<div class="col-xs-1">';
    (new \EOS\UI\Bootstrap\Input('email_setting-port'))
        ->type('number')
        ->bind($this->data, 'email_setting-port')
        ->printRender($this);

    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-2"></div>';
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.auth'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-2">';
    (new \EOS\UI\Bootstrap\Checkbox('email_setting-auth'))
        ->bind($this->data, 'email_setting-auth')
        ->printRender($this);

    echo '</div>';
    $list = [];
    $list[] = ['key' => '', 'descr' => ''];
    $list[] = ['key' => 'ssl', 'descr' => 'ssl'];
    $list[] = ['key' => 'tls', 'descr' => 'tls'];
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.secure'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-2">';
    (new \EOS\UI\Bootstrap\Select('email_setting-secure'))
        ->bind($this->data, 'email_setting-secure')
        ->bindList($list, 'key', 'descr')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-2"></div>';
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.username'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-3">';
    (new \EOS\UI\Bootstrap\Input('email_setting-username'))
        ->bind($this->data, 'email_setting-username')
        ->printRender($this);
    echo '</div>';
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.password'))->attr('class', 'col-xs-1 text-right')->printRender();
    echo '<div class="col-xs-3">';
    (new \EOS\UI\Bootstrap\Input('email_setting-password'))
        ->type('password')
        ->bind($this->data, 'email_setting-password')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-2"></div>';
    (new \EOS\UI\Html\Label)->content($this->transE('system.setting.email.from'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-3">';
    (new \EOS\UI\Bootstrap\Input('email_setting-from'))
        ->bind($this->data, 'email_setting-from')
        ->printRender($this);
    echo '</div>';
    echo '<div class="col-xs-3">';
    (new \EOS\UI\Bootstrap\Button('btn-test'))
        ->attr('class', ' btn-info btn-sm')
        ->click('function (e) {e.preventDefault(); testMail();}')
        ->content($this->transE('system.common.test'))
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
echo '<div class="row"><div class="col-xs-12">';
(new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
echo '</div></div>';
$this->writeTokenHtml();
$formData->endContent();
$formData->printRender($this);
$tab->endTab('tab-data');
$tab->startTab();
$box = new EOS\UI\Bootstrap\Box();
$box->startContent();
$form = new EOS\UI\Html\Form();
$form->id('ins-form');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();

echo '<div class="col-xs-2">';
$dt = new \EOS\UI\Bootstrap\Input('object');
$dt->id('object');
$dt->placeholder($this->transE('system.setting.field.object'));
$dt->printRender($this);
echo '</div>';
echo '<div class="col-xs-2">';
$dt = new \EOS\UI\Bootstrap\Input('name');
$dt->id('name');
$dt->placeholder($this->transE('system.setting.field.name'));
$dt->printRender($this);
echo '</div>';
echo '<div class="col-xs-2">';
$dt = new \EOS\UI\Bootstrap\Input('value');
$dt->id('value');
$dt->placeholder($this->transE('system.setting.field.value'));
$dt->printRender($this);
echo '</div>';
echo '<div class="col-xs-1">';
(new \EOS\UI\Bootstrap\Button('btn-new'))
    ->attr('class', ' btn-info btn-sm')
    ->click('function (e) {e.preventDefault(); addNew();}')
    ->content('<i class="fa fa-plus"></i>')
    ->printRender($this);
echo '</div>';
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$box->endContent();
$box->printRender($this);

$box = new EOS\UI\Bootstrap\Box();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('System', ['setting', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', '', false, false, false);
$tbl->addColumn('object', $this->transE('system.setting.field.object'));
$tbl->addColumn('name', $this->transE('system.setting.field.name'));
$tbl->addColumn('value', $this->transE('system.setting.field.value'));
$tbl->addColumnCustom('', 'del-row', '<button class="btn  btn-danger btn-sm" id="btn-new"><i class="fa fa-trash"></i></button>');
$tbl->clickRow('function (e, r) {e.stopPropagation(); delRow(r.data().id);}', 'del-row');
$tbl->addRawParams('bFilter', 'false');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);
$tab->endTab('tab-adv');
$tab->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        var msg = "<?php $this->transPE('system.setting.add.confirm'); ?>";
        msg = msg + '<br>' + $('#object').val() + ' | ' + $('#name').val();
        msg = msg + ' | ' + $('#value').val();
        bootbox.confirm(msg, function (result)
        {
            if (result)
            {
                var data = $('#ins-form').serializeFormJSON();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('System', ['setting', 'ajaxsave']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            $('#datatable').DataTable().ajax.reload();
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function delRow(id)
    {
        bootbox.confirm("<?php $this->transPE('system.setting.delete.confirm'); ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('System', ['setting', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": id}),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            $('#datatable').DataTable().ajax.reload();
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function saveData()
    {
        var data = $('#data-form').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('System', 'setting/ajaxsavedata'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function testMail()
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('System', ['setting', 'ajaxtestmail']); ?>',
            data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>"}),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    bootbox.alert(json.message);
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

</script>
<?php
$this->endCaptureScript();
