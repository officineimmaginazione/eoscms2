<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    if ($this->user->isAuthorizedCreate('system.user'))
    {
        (new \EOS\UI\Bootstrap\Button('btn-new'))
            ->attr('class', ' btn-info btn-sm pull-right')
            ->click('function (e) { addNew();}')
            ->content('<i class="fa fa-plus"></i>')
            ->printRender($this);
    }
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('System', 'user/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('system.user.field.id'));
$tbl->addColumn('username', $this->transE('system.user.field.username'));
$tbl->addColumn('email', $this->transE('system.user.field.email'));
$tbl->addColumn('ugroup', $this->transE('system.user.field.ugroup'));
$tbl->addColumn('status', $this->transE('system.user.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->addColumnOrder('username', true);
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('system', 'user/edit/0') ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->getUrlFor('system', 'user/edit') ?>" + id + "/";
    }

    function renderStatus(data)
    {
        if (data == 1)
        {
            return '<span class="label label-success"><?php $this->transP('system.common.yes'); ?></span>';
        } else
        {
            return '<span class="label label-danger"><?php $this->transP('system.common.no'); ?></span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
