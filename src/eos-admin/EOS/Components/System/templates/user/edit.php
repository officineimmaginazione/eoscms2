<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
$box->endHeader();
$box->startContent();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.user.field.email'))->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('email'))
        ->bind($this->data, 'email')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.user.field.username'))->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('username'))
        ->bind($this->data, 'username')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.user.field.password'))->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Input('password'))
        ->type('password')
        ->bind($this->data, 'password')->printRender($this);
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.user.field.ugroup'))->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Select('id_group'))
        ->bind($this->data, 'id_group')
        ->bindList($this->groups, 'id', 'name')
        ->printRender($this);
    echo "</div>";
    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    (new \EOS\UI\Html\Label)->content($this->transE('system.user.field.status'))->attr('class', 'col-xs-2 text-right')->printRender();
    echo '<div class="col-xs-8">';
    (new \EOS\UI\Bootstrap\Checkbox('status'))
        ->bind($this->data, 'status')
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
{
    echo '<div class="col-xs-8">';
    echo '</div>';
}, $this)->printRender();
$box->endContent();
$box->startFooter();
if ((isset($this->data['id'])) && ($this->data['id'] != 0) && ($this->user->isAuthorizedDelete('system.user')))
{
    (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos pull-left')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
}
if (($this->user->isAuthorizedCreate('system.user')) || ($this->user->isAuthorizedUpdate('system.user')))
{
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos pull-right')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
}
(new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos pull-right')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
(new \EOS\UI\Html\Input())->name('id_lang')->id('id_lang')->bind($this->data, 'id_lang')->type('hidden')->printRender();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('System', 'user/ajaxsave'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->getUrlFor('System', 'user/index'); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('system.user.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->getUrlFor('System', 'user/ajaxdelete'); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: "application/json"
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();
