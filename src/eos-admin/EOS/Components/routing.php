<?php
$this->get('/', function ($request, $response, $args)
{
  $p = $this->get('path');
  return $response->withRedirect($p->urlFor('system', ['access', 'login']), 302);
});