<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="robots" content="noindex, nofollow">
		<?php
		$this->title = "EOS - " . $this->pageTitle;
		$this->writeHead();
		\EOS\UI\Loader\Library::load($this, 'Icons');
		//$this->addStyleSheetLink($this->path->getThemeUrl() . "css/admin.css");
		//$this->addStyleSheetLink($this->path->getThemeUrl() . "css/eos.css");
		$this->addStyleSheetLink($this->path->getThemeUrl() . "css/eos.admin.min.css");
		$this->writeLink();
		$this->addScriptLink($this->path->getThemeUrl() . "js/app.min.js");
		$this->addScriptLink($this->path->getThemeUrl() . "js/eos.util.js");
		$this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
		?>
	</head>
	<body class="sidebar-mini eos-admin">
		<div class="container-fluid px-0 h-100">
			<div class="d-flex h-100">
				<?php include 'inc/sidebar.inc.php'; ?>
				<main class="d-flex flex-column flex-fill admin-main">
					<section class="admin-main-header">
						<div class="d-flex flex-row align-items-center">
							<?php include 'inc/breadcrumb.inc.php'; ?>
							<?php include 'inc/options.inc.php'; ?>
						</div>
					</section>
					<section class="admin-main-content">
						<?php $this->writeContent(); ?>
					</section>
					<?php include 'inc/footer.inc.php'; ?> 
				</main>
			</div>
		</div>    
		<!-- Add the sidebar's background. This div must be placed
		immediately after the control sidebar -->
		<?php $this->writeScript(); ?>
	</body>
</html>



