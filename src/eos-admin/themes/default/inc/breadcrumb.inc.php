<!-- 
-
-	BREADCRUMB - INIZIO 
-	Descrizione: Briciole di pane
-	Stili: form.scss
-
-->
<ol class="breadcrumb">
	<?php
	for ($i = 0; $i < count($this->breadcrumbs); $i++) {
		$icon = ($this->breadcrumbs[$i]['href'] == '') ? '' : '<i class="' . $this->breadcrumbs[$i]['icon'] . '"></i>';
		$active = ($this->breadcrumbs[$i]['href'] == 'javascript:void(0)') ? ' class="breadcrumb-item active"' : ' class="breadcrumb-item"';
		echo '<li' . $active . '><a href="' . $this->breadcrumbs[$i]['href'] . '">' . $icon . ' ' . $this->encodeHtml($this->breadcrumbs[$i]['title']) . '</a></li>';
	}
	?>
</ol>
<!-- BREADCRUMB - FINE -->