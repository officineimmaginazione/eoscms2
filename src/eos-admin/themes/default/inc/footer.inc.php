<?php 
/**
 * 
 * FOOTER - INIZIO 
 * Descrizione: Piè di pagina, contiene Copyright
 * Stili: footer.scss
 * 
 */
?>
<footer class="mt-auto admin-main-footer">
	Copyright &copy; 2015 - <?php echo date("Y"); ?> <a href="http://www.eoscms.io" target="_blank">EOS Cms</a> All rights reserved.
</footer>
<?php 
/**
 * 
 * FOOTER - FINE 
 * 
 */