<div class="d-flex ml-auto">
<?php 
/**
 *
 *	MENU UTENTE - INIZIO
 *	Descrizione: Contiene il menu con le opzioni disponibili per l'utente
 *	Stili: form.scss
 *
 **/
$user = $this->controller->user->getInfo();
?>
<div class="dropdown ml-2">
	<button class="btn btn-secondary btn-info dropdown-toggle" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<i class="fa fa-user"></i>
		<span class="sr-only"><?php echo $user->username; ?></span>
	</button>
	<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUser">
	  <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('system', 'dashboard/index'); ?>"><?php echo $this->transE('system.dashboard.dashboard') ?></a></li>
	  <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('system', 'access/logout'); ?>#"><?php echo $this->transE('system.access.logout') ?></a></li>
	</ul>
</div>
<?php
/**
 *
 *	MENU UTENTE - FINE
 *
 **/ 
?>
</div>