<!-- 
-
-	SIDEBAR - INIZIO 
-	Descrizione: Colonna sulla sinitra, contiene: logo, menu e sottomenu
-	Stili: sidebar.scss
-
-->
<aside class="sidebar">
	<?php
	$menu = new \EOS\Components\System\UI\MenuWidget($this);
	$menu->write();
	?>
</aside>
<!-- SIDEBAR - FINE -->
<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->