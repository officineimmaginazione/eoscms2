<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="noindex, nofollow">
        <?php
        $this->title = 'Login';
        $this->writeHead();
        \EOS\UI\Loader\Library::load($this, 'Bootstrap');
        \EOS\UI\Loader\Library::load($this, 'Bootbox');
        \EOS\UI\Loader\Library::load($this, 'Icons');
        \EOS\UI\Loader\Library::load($this, 'iCheck');
        $this->addStyleSheetLink($this->path->getThemeUrl() . "css/eos.admin.min.css");
        $this->writeLink();
        $this->addScriptLink($this->path->getThemeUrl() . "js/eos.util.js");
        ?>
    </head>
    <body class="d-flex align-items-center hold-transition login-page"><?php
        $this->writeContent();
        $this->writeScript();
        ?>
        <script>
            $(function ()
            {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
