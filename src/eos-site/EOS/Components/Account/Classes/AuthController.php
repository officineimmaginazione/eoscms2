<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes;

class AuthController extends \EOS\Components\System\Classes\SiteController
{

    public $user = null;

    public function __construct($container)
    {
        $this->user = new AuthUser($container);
        $this->user->checkLogged();
        parent::__construct($container);
        $this->user->loadTheme();
    }

}
