<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;

class AuthMapMiddleware extends \EOS\System\Middleware\Middleware
{

    private function getEventDispatcher($name = null): object
    {
        return $this->container->get('eventDispatcherManager')->getDispatcher($name);
    }

    private function hookLoadController()
    {
        $disp = $this->getEventDispatcher();
        $disp->addListener(\EOS\System\Events::CONTROLLER, function ($event)
        {
            $user = new \EOS\Components\Account\Classes\AuthUser($this->container);
            $user->checkLogged(); // Controllo che sia autenticato
            $user->loadTheme();
            $controller = $event->getSubject();
            // Faccio injection dentro i controller e forzo il template predefinito
            if (!property_exists(get_class($controller), 'user'))
            {
                $controller->user = $user;
            }
        });
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        try
        {
            $uri = $request->getUri();
            $path = $uri->getPath();
            if ($path != '/')
            {
                $pathlist = PathHelper::explodeSlash($path);
                if (count($pathlist) > 0)
                {
                    $compName = $pathlist[0];
                    $compM = $this->container->get('componentManager');
                    if ($compM->componentExists($compName))
                    {
                        $compName = $compM->getComponent($compName)->name;
                        // 1 Cerco il router nel componente: AuthRouter.php 
                        $className = '\\EOS\\Components\\' . $compName . '\\AuthRouter';
                        if (!class_exists($className))
                        {
                            // 2 Cerco il router come specifico per il componente 
                            $className = '\\EOS\\Components\\Account\\Classes\\Routers\\Auth' . $compName;
                            if (!class_exists($className))
                            {
                                $className = '\\EOS\\Components\\Account\\Classes\\Routers\\AuthRouter';
                            }
                        }

                        $router = new $className($this->container);
                        if ($router->isAuthenticated($path))
                        {
                            $this->hookLoadController();
                        }
                    }
                }
            }

            return $next($request, $response);
        } catch (\EOS\Components\Account\Classes\AuthException $ex)
        {
            $newUrl = $this->container->get('path')->urlFor('account', ['access', 'login']);
            return $response->withRedirect($newUrl, 302);
        }
    }

}
