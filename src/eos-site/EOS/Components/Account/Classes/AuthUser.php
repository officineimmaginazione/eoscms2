<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes;

class AuthUser
{

    protected $session;
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
        $this->session = $container->get('session');
        $this->db = $container->get('database');
    }

    public function isLogged(): bool
    {
        return $this->session->get('account.isauth', false);
    }

    public function checkLogged()
    {
        if (!$this->isLogged())
        {
            throw new \EOS\Components\Account\Classes\AuthException('Login non valida!');
        }
    }

    public function signin(string $passwordDB, string $password): bool
    {
        if (password_verify($password, $passwordDB))
        {
            $this->session->set('account.isauth', true);
            return true;
        } else
        {
            return false;
        }
    }

    public function saveInfo($id, $username, $email, $name, $groups)
    {
        $user = ['id' => $id, 'username' => $username, 'email' => $email, 'name' => $name, 'groups' => $groups];
        $this->session->set('account.user', \EOS\System\Util\ArrayHelper::toJSON($user));
    }

    public function getInfo(): object
    {
        $userj = $this->session->get('account.user');
        return json_decode($userj, false);
    }

    public function getUserID(): int
    {
        return $this->getInfo()->id;
    }

    public function loadTheme()
    {
        $theme = $this->db->setting->getStr('account', 'auth.theme');
        if (!empty($theme))
        {
            $this->container['currentParams']['themeName'] = $theme;
        }
        $themeTemplate = $this->db->setting->getStr('account', 'auth.themetemplate');
        if (!empty($themeTemplate))
        {
            $this->container['currentParams']['themeTemplate'] = $themeTemplate;
        }
    }

    public function isValidGroups(array $groups): bool
    {
        $res = false;
        $ug = $this->getInfo()->groups;
        foreach ($groups as $g)
        {
            if (in_array($g, $ug))
            {
                $res = true;
                break;
            }
        }
        return $res;
    }
    
    public function checkLoggedAndGroup(array $groups)
    {
        $this->checkLogged();
        if (!$this->isValidGroups($groups))
        {
            throw new \EOS\Components\Account\Classes\AuthException('Gruppo non valido!');
        }
    }

}
