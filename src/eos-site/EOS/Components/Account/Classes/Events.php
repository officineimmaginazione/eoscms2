<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes;

class Events
{

    const SERVICE_USER_DATA_PARSE = 'account.service.user.data.parse';
    const ACCESS_LOGIN = 'account.access.login';
    const ACCESS_REGISTER_FORM_AJAX_CONFIG = 'account.access.register.form.ajax.config';
    const ACCESS_REGISTER_DATA_PREPARE = 'account.access.register.data.prepare';
    const ACCESS_REGISTER_DATA_CREATE = 'account.access.register.data.create';
    const USER_INDEX_TEMPLATE_RENDER = 'account.access.index.template.render';

}
