<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes\Routers;

use EOS\System\Routing\PathHelper;

class AuthContent extends \EOS\Components\Account\Classes\Routers\AuthRouter
{
    public function isAuthenticated(string $route): bool
    {
        $res = parent::isAuthenticated($route);
        $segments = PathHelper::explodeSlash($route);
        if ($segments[0] !== 'content')
        {
            throw new \Exception('Errore imprevisto nella route: '.$route);
        }
        switch ($segments[1])
        {
            case 'page':
                $this->checkLoggedAndGroup($route);
                break;

            default:
                break;
        }
        return $res;
    }
}