<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Classes\Routers;

use EOS\System\Util\StringHelper;
use EOS\System\Routing\PathHelper;

class AuthRouter
{

    protected $container;
    protected $db;

    protected function getRoute(string $route): array
    {
        $q = $this->db->prepare('select id, route, container, auth from #__account_router where route = :route');
        $q->bindValue(':route', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route)));
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? [] : $r;
    }

    protected function getRouteID(string $route): int
    {
        $r = $this->getRoute($route);
        return empty($r) ? 0 : $r['id'];
    }

    protected function getRouteGroups(int $idRoute): array
    {
        $q = $this->db->prepare('select id_group from #__account_router_group where id_router = :id_router');
        $q->bindValue(':id_router', $idRoute);
        $q->execute();
        $list = $q->fetchAll();
        $res = [];
        if (!empty($list))
        {
            foreach ($list as $r)
            {
                $res[] = (int) $r['id_group'];
            }
        }
        return $res;
    }

    protected function checkLoggedAndGroup(string $route)
    {
        $r = $this->getRoute($route);
        if (!empty($r))
        {
            if ($r['auth'] == 1)
            {
                $user = $this->getUser();
                $user->checkLoggedAndGroup($this->getRouteGroups($r['id']));
            }
        }
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->db = $this->container->get('database');
    }

    public function isAuthenticated(string $route): bool
    {

        return false;
    }

    public function getUser(): \EOS\Components\Account\Classes\AuthUser
    {
        return new \EOS\Components\Account\Classes\AuthUser($this->container);
    }

}
