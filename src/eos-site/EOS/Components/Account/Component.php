<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        $db = $this->getContainer()->get('database');
        // Devo verificare che sia installato il modulo degli account
        if ($db->setting->getInt('account', 'dbversion') > 0)
        {
            // Aggiungo il middleware che controlla le autenticazioni trmite route e non solo
            $this->application->addMapMiddleware(new \EOS\Components\Account\Classes\AuthMapMiddleware($this->getContainer()));
        }
    }

}
