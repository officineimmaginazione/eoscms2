<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;

class AccessController extends \EOS\Components\System\Classes\SiteController
{

    public function login($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        if (!$m->getAuthLogin())
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        $v->authForgot = $m->getAuthForgot();
        $v->authRegister = $m->getAuthRegister();
        return $v->render('access/login');
    }

    public function forgot($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        // Blocco la pagina se non attivo
        if (!$m->getAuthForgot())
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        return $v->render('access/forgot');
    }

    public function register($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        // Blocco la pagina se non attivo
        if (!$m->getAuthRegister())
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        return $v->render('access/register');
    }

    public function active($request, $response)
    {
        $v = $this->newView($request, $response);
        $v->addMeta('robots', 'noindex, nofollow'); // Blocco indicizzazione pagina
        return $v->render('access/active');
    }

    public function resetPass($request, $response)
    {
        $v = $this->newView($request, $response);
        $v->addMeta('robots', 'noindex, nofollow'); // Blocco indicizzazione pagina
        return $v->render('access/reset-pass');
    }

    public function ajaxLogin($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);
            if (!$m->getAuthLogin())
            {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
            $data = $fv->getInputData();
            $error = '';
            if ($m->login($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($m->getAuthHome());
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxForgot($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);

            // Blocco la pagina se non attivo
            if (!$m->getAuthForgot())
            {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }

            $data = $fv->getInputData();
            $error = '';
            if ($m->forgot($data, $error))
            {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.forgot.send'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxRegister($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);

            // Blocco la pagina se non attivo
            if (!$m->getAuthRegister())
            {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }

            $data = $fv->getInputData();
            $error = '';
            if ($m->register($data, $error))
            {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.register.send'));
                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
