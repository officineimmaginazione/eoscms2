<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class AddressController extends \EOS\Components\Account\Classes\AuthController
{
    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Account\Models\AddressModel($this->container);
            if (isset($data['command']))
            {
                switch ($data['command'])
                {
                    case 'customerdata':
                        $data = $m->getDataDiscount(ArrayHelper::getInt($data, 'id-customer'));
                        $address = json_encode($data);
                        $fv->setOutputValue('address', $data);
                        $fv->setResult(true);
                        break;
                }
            } else
            {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }
    
    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue')))
        {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\AddressModel($this->container);
            switch ($datasource)
            {
                case 'address':
                    if ($format === 'typeahead')
                    {
                        if (isset($this->user))
                        {
                            $user = $this->user->getInfo();
                            $id = $user->id;
                            $res = $m->getAddressList($m->getFuncFormatAddressName(), $id);
                        }
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }
}
