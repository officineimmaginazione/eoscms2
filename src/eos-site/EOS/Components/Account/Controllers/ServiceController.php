<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

class ServiceController extends \EOS\Components\System\Classes\SiteController
{

    // Questa funzione può essere lanciata dal backend quindi non posso controllare la sessione
    public function requestActive($request, $response)
    {
        $m = new \EOS\Components\Account\Models\ServiceModel($this->container);
        $res = $m->requestActive($request->getAttribute('token'));
        return $response->withJson(['result' => $res]);
    }

    // Questa funzione viene richiamata dall'email
    public function active($request, $response)
    {
        $m = new \EOS\Components\Account\Models\ServiceModel($this->container);
        $res = $m->active($request->getAttribute('token'));
        if ($res)
        {
            return $response->withRedirect($this->path->urlFor('account', ['access', 'active']), 301);
        } else
        {
            return $response->withRedirect($this->path->getBaseUrl(), 301);
        }
    }

    // Questa funzione può essere lanciata dal backend quindi non posso controllare la sessione
    public function requestResetPass($request, $response)
    {
        $m = new \EOS\Components\Account\Models\ServiceModel($this->container);
        $res = $m->requestResetPass($request->getAttribute('token'));
        return $response->withJson(['result' => $res]);
    }

    // Questa funzione viene richiamata dall'email
    public function resetPass($request, $response)
    {
        $m = new \EOS\Components\Account\Models\ServiceModel($this->container);
        $res = $m->resetPass($request->getAttribute('token'));
        if ($res)
        {
            return $response->withRedirect($this->path->urlFor('account', ['access', 'reset-pass']), 301);
        } else
        {
            return $response->withRedirect($this->path->getBaseUrl(), 301);
        }
    }

}
