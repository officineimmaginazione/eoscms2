<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;

class UserController extends \EOS\Components\Account\Classes\AuthController
{

    public function index($request, $response)
    {
        $v = $this->newView($request, $response);
        return $v->render('user/default');
    }

    public function logout($request, $response)
    {
        // Distruggo la sessione per sicurezza e fa il logout così
        $this->session->destroy();
        $newUrl = $this->path->urlFor('account', ['access', 'login']);
        return $response->withRedirect($newUrl, 302);
    }

    public function edit($request, $response)
    {
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $v = $this->newView($request, $response);
        $v->data = $m->getUserData((int) $this->user->getUserID());
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        return $v->render('user/edit');
    }

    public function ajaxEdit($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->editData($data, $error))
            {
                $fv->setResult(true);
                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
