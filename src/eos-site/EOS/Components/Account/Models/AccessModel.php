<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\ArrayHelper;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class AccessModel extends \EOS\System\Models\Model
{

    public $checkRequestLimit = true;
    public $lastRegisterID = null;

    protected function checkUserExists($username, $email)
    {
        $q = $this->db->prepare('select id from #__account_user where ((username = :username) or (email = :email))');
        $q->bindValue(':username', $username);
        $q->bindValue(':email', $email);
        $q->execute();
        $r = $q->fetch();
        return empty($r);
    }

    protected function createUniqueUsername()
    {
        $res = '';
        while (empty($res))
        {
            // genero 8 caratteri random tra numeri e lettere
            $letters = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'), ['.', '-', '_']);
            for ($i = 0; $i < 8; $i++)
            {
                $res .= $letters[rand(0, count($letters) - 1)];
            }
            // genero 4 bytes random (8 caratteri random) 
            $res .= \EOS\System\Security\CryptHelper::randomString(4);
            // Converto il timestamp in coda
            $dt = (new \DateTimeImmutable())->getTimestamp();
            $res .= $res . dechex($dt);
            // Verifico che non sia presente nel DB
            $q = $this->db->prepare('select id from #__account_user where username = :username');
            $q->bindValue(':username', $res);
            $q->execute();
            $r = $q->fetch();
            $res = empty($r) ? $res : '';
        }
        return $res;
    }

    protected function isValidUsername($username)
    {
        $res = true;
        if ((mb_strlen($username) < 6) || (mb_strlen($username) > 150))
        {
            $res = false;
        } else
        {
            $letters = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'), ['.', '-', '_']);
            $ul = str_split($username);
            foreach ($ul as $c)
            {
                if (!in_array($c, $letters))
                {
                    $res = false;
                    break;
                }
            }
        }
        return $res;
    }

    protected function updateLastAccess($idUser)
    {
        $sql = 'update #__account_user set last_access = :last_access where id = :id_user';
        $q = $this->db->prepare($sql);
        $q->bindValue(':last_access', $this->db->util->nowToDBDateTime());
        $q->bindValue(':id_user', $idUser);
        $q->execute();
    }

    protected function getUserGroups($idUser)
    {
        $res = [];
        $q = $this->db->prepare('select id_group from #__account_user_group where id_user = :id_user');
        $q->bindValue(':id_user', $idUser);
        $q->execute();
        $list = $q->fetchAll();
        if (!empty($list))
        {
            foreach ($list as $r)
            {
                $res [] = (int) $r['id_group'];
            }
        }
        return $res;
    }

    protected function isValidCredentials($logname, $password, $authType, &$error)
    {
        $res = true;
        // Controllo che i campi siano pieni
        if (empty($logname) || empty($password))
        {
            return false;
        }
        // Non accesso stringhe con spazi dovrebbe prevenire alcuni sql-injection
        if (strpos($logname, ' ') !== false)
        {
            return false;
        }
        // Verifico che non arrivino stringhe lunghissime
        if ((mb_strlen($logname) > 150) || (mb_strlen($password) > 100))
        {
            return false;
        }

        // Se ho l'autenticazione solo mail controllo che sia una mail vera
        if ($authType == 1)
        {
            if (filter_var($logname, FILTER_VALIDATE_EMAIL) === false)
            {
                return false;
            }
        }

        return $res;
    }

    // Questa funzione previene che riceva più di 1 richiesta nel medesimo secondo
    // serve per evitare i robot
    protected function isRequestLimit()
    {
        $res = false;
        if ($this->checkRequestLimit)
        {
            if ($this->db->setting->getBool('account', 'request.limit', true))
            {
                $dt = DateTimeHelper::formatISO(\EOS\System\Util\DateTimeHelper::now());
                $v = $this->container->get('session')->get('account.request.datetime', '');
                $this->container->get('session')->set('account.request.datetime', $dt);
                $res = $v == $dt;
            }
        }
        return $res;
    }

    protected function getDefGroupID()
    {
        $q = $this->db->prepare('select id, name, def from #__account_group where status = 1 and def = 1');
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int) $r['id'];
    }

    /*
      0 Bloccato - Non autentica nulla
      1 Solo Email
      2 Utente
      3 Utente o Mail
     */

    public function getAuthType()
    {
        return $this->db->setting->getInt('account', 'auth.type', 0);
    }

    public function getAuthLogin()
    {
        return $this->db->setting->getBool('account', 'auth.login', false);
    }

    public function getAuthForgot()
    {
        return $this->db->setting->getBool('account', 'auth.forgot', false);
    }

    public function getAuthRegister()
    {
        return $this->db->setting->getBool('account', 'auth.register', false);
    }

    public function login($data, &$error)
    {
        $res = false;
        $log = new LogModel($this->container);
        $authType = $this->getAuthType();
        if ($authType == 0)
        {
            $error = $this->lang->trans('account.access.login.error0');
        } else if ($log->isLockedIP())
        {
            $error = $this->lang->trans('account.access.error-ip');
        } else
        {
            $logname = ArrayHelper::getStr($data, 'log-name');
            $password = ArrayHelper::getStr($data, 'log-pwd');
            $error = $this->lang->trans('account.access.login.error' . $authType);
            $logStatus = 0;
            $idUser = null;
            if ($this->isValidCredentials($logname, $password, $authType, $error))
            {
                $sql = 'select id, username, email, name, password from #__account_user where status = 1';
                switch ($authType)
                {
                    case 1:
                        $sql .= ' and email = :logname';
                        break;
                    case 2:
                        $sql .= ' and username = :logname';
                        break;
                    case 3:
                        $sql .= ' and (username = :logname or email = :logname)';
                        break;
                    default:
                        throw new \Exception('Tipo di autenticazione non valido!!!!!!!!!!!!');
                        break;
                }

                $q = $this->db->prepare($sql);
                $q->bindValue(':logname', $logname);
                $q->execute();
                $r = $q->fetch();
                $logStatus = 1;
                if (!empty($r))
                {
                    $user = new \EOS\Components\Account\Classes\AuthUser($this->container);
                    if ($user->signin($r['password'], $password))
                    {
                        $idUser = $r['id'];
                        $this->updateLastAccess($idUser);
                        $user->saveInfo($idUser, $r['username'], $r['email'], $r['name'], $this->getUserGroups($idUser));
                        $logStatus = LogModel::VALID_STATUS;
                        $ev = new \EOS\System\Event\DataEvent($this, ['id' => $idUser]);
                        $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_LOGIN, $ev);
                        $res = true;
                    } else
                    {
                        // Password errata ma utente corretto
                        $logStatus = 2;
                    }
                }
            }
            $log->add($logname, $idUser, 'login', $logStatus);
            if ((!$res) && ($log->isIPLimit()))
            {
                // Blocco per X tempo al prossimo tentativo
                $log->add($logname, $idUser, 'login', LogModel::LOCK_STATUS);
            }
        }
        return $res;
    }

    public function getAuthHome()
    {
        $homeUrl = $this->db->setting->getStr('account', 'auth.home', '');
        
        if (empty($homeUrl))
        {
            $homeUrl = $this->container->get('path')->urlFor('account', ['user', 'index']);
        } else
        {
            $pathSg = \EOS\System\Routing\PathHelper::explodeSlash($homeUrl);
            $comp = $pathSg[0];
            array_shift($pathSg);
            $homeUrl = $this->container->get('path')->urlFor($comp, $pathSg);
        }
        return $homeUrl;
    }

    public function forgot($data, &$error)
    {
        $res = false;
        $log = new LogModel($this->container);
        $authType = $this->getAuthType();
        if ($authType == 0)
        {
            $error = $this->lang->trans('account.access.forgot.error0');
        } else if ($log->isLockedIP())
        {
            $error = $this->lang->trans('account.access.error-ip');
        } else
        {
            $logname = ArrayHelper::getStr($data, 'log-name');
            $error = $this->lang->trans('account.access.forgot.error' . $authType);
            $logStatus = 0;
            $idUser = null;
            if ($this->isValidCredentials($logname, '(fake password)', $authType, $error))
            {
                $sql = 'select id, username, email from #__account_user where status = 1';
                switch ($authType)
                {
                    case 1:
                        $sql .= ' and email = :logname';
                        break;
                    case 2: case 3:
                        $sql .= ' and (username = :logname or email = :logname)';
                        break;
                    default:
                        throw new \Exception('Tipo di autenticazione non valido!!!!!!!!!!!!');
                        break;
                }

                $q = $this->db->prepare($sql);
                $q->bindValue(':logname', $logname);
                $q->execute();
                $r = $q->fetch();
                $logStatus = 1;
                if (!empty($r))
                {
                    $svc = new ServiceModel($this->container);
                    $token = $svc->addUserCmd($r['id'], 'request-reset-pass');
                    $svc->requestResetPass($token);
                    $logStatus = LogModel::VALID_STATUS;
                    $res = true;
                }
            }
            $log->add($logname, $idUser, 'forgot', $logStatus);
            if ((!$res) && ($log->isIPLimit()))
            {
                // Blocco per X tempo al prossimo tentativo
                $log->add($logname, $idUser, 'forgot', LogModel::LOCK_STATUS);
            }
        }
        return $res;
    }

    // Deve essere pubblico perché viene usato esternamente da registrazioni custom per validare i dati
    public function prepareRegister(&$data, &$error)
    {
        $res = true;
        $registerMinutes = $this->db->setting->getInt('account', 'register.minutes', 1);
        // Quando uso l'autenticazione per EMail genero un nome utente sempre!!
        if ($this->getAuthType() == 1)
        {
            $data['reg-username'] = $this->createUniqueUsername();
        }
        $log = new LogModel($this->container);
        if ($this->isRequestLimit())
        {
            $error = $this->lang->trans('account.access.error-request-second');
            $res = false;
        } else if ($log->isLockedIP())
        {
            $error = $this->lang->trans('account.access.error-ip');
            $res = false;
        } else if (($registerMinutes > 0) && ($log->countCmdMinute('register', $registerMinutes) >= 1))
        {
            $error = $this->lang->trans('account.access.register.error.limit');
            $res = false;
        } else if ((mb_strlen(ArrayHelper::getStr($data, 'reg-name')) < 2) || (mb_strlen(ArrayHelper::getStr($data, 'reg-name')) > 255))
        {
            $error = $this->lang->trans('account.access.register.error.name');
            $res = false;
        } else if (filter_var(ArrayHelper::getStr($data, 'reg-email'), FILTER_VALIDATE_EMAIL) === false)
        {
            $error = $this->lang->trans('account.access.register.error.email');
            $res = false;
        } else if (!$this->isValidUsername(ArrayHelper::getStr($data, 'reg-username')))
        {
            $error = $this->lang->trans('account.access.register.error.username');
            $res = false;
        } else if ((mb_strlen(ArrayHelper::getStr($data, 'reg-pwd1')) < 8) || (mb_strlen(ArrayHelper::getStr($data, 'reg-pwd1')) > 100))
        {
            $error = $this->lang->trans('account.access.register.error.pwd1');
            $res = false;
        } else if (ArrayHelper::getStr($data, 'reg-pwd1') !== ArrayHelper::getStr($data, 'reg-pwd2'))
        {
            $error = $this->lang->trans('account.access.register.error.pwd2');
            $res = false;
        } else if (ArrayHelper::getBool($data, 'reg-consent', false) != true)
        {
            $error = $this->lang->trans('account.access.register.error.consent');
            $res = false;
        } else if (!$this->checkUserExists(ArrayHelper::getStr($data, 'reg-username'), ArrayHelper::getStr($data, 'reg-email')))
        {
            $error = $this->lang->trans('account.access.register.error.userexists');
            $res = false;
        }

        if ($res)
        {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }
        return $res;
    }

    public function register(&$data, &$error)
    {
        $this->lastRegisterID = null;
        if (!$this->prepareRegister($data, $error))
        {
            return false;
        } else
        {
            $values['username'] = $data['reg-username'];
            $values['email'] = $data['reg-email'];
            $values['name'] = $data['reg-name'];
            $values['status'] = 2; // In attivazione
            $pass = new \EOS\System\Security\Password(null);
            $values['password'] = $pass->hashPassword($data['reg-pwd1']);
            $values['password_date'] = $this->db->util->nowToDBDateTime();
            $values['id_lang'] = $this->lang->getCurrentID();
            $values['up_id'] = 0;
            $values['up_date'] = $this->db->util->nowToDBDateTime();
            $values['ins_id'] = 0;
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $tblContent = $this->db->tableFix('#__account_user');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
            $id = $this->db->lastInsertId();
            $this->lastRegisterID = $id;
            // Devo inserire sempre il gruppo di default per gli utenti creati
            $tblUG = $this->db->tableFix('#__account_user_group');
            $vug['id_user'] = $id;
            $vug['id_group'] = $this->getDefGroupID();
            $vug['ins_id'] = 0;
            $vug['ins_date'] = $this->db->util->nowToDBDateTime();
            $qug = $this->db->newFluent()->insertInto($tblUG, $vug);
            $qug->execute();

            $ev = new \EOS\System\Event\DataEvent($this, ['id' => $id]);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_DATA_CREATE, $ev);
            $svc = new ServiceModel($this->container);
            $token = $svc->addUserCmd($id, 'request-active');
            $svc->requestActive($token);

            $log = new LogModel($this->container);
            $log->add($data['reg-email'], $id, 'register', LogModel::VALID_STATUS);
            return true;
        }
    }

}
