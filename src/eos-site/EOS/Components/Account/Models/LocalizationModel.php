<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

class LocalizationModel extends \EOS\System\Models\Model
{

    protected function formatResult($q, callable $formatFunc = null)
    {
        $res = [];
        if (is_null($formatFunc))
        {
            $res = $q->fetchAll();
        } else
        {
            while ($r = $q->fetch())
            {
                $res[] = $formatFunc($r);
            }
        }
        return $res;
    }

    public function getCityList(callable $formatFunc = null)
    {
        $sql = 'select lc.id, lc.name, lc.zip_code, ls.iso_code as statecode from #__localization_city lc 
                left join #__localization_state ls on ls.id = lc.id_state 
                order by name';
        $q = $this->db->prepare($sql);
        $q->execute();
        return $this->formatResult($q, $formatFunc);
    }

    public function getCity($id)
    {
        $sql = 'select lc.id, lc.name, lc.zip_code, lc.id_state, ls.name as state, ls.iso_code as statecode,
                ls.id_region, lr.name as region, lr.id_country, lcnl.name as country, lcn.iso_code as countrycode,
                lc.latitude, lc.longitude
                from #__localization_city lc
                left join #__localization_state ls on ls.id = lc.id_state 
                left join #__localization_region lr on lr.id = ls.id_region
                left join #__localization_country lcn on lcn.id = lr.id_country
                left join #__localization_country_lang lcnl on lcn.id = lcnl.id_country
                where lc.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', (int) $id);
        $q->execute();
        return $q->fetch();
    }

    public function searchCity($partialName, callable $formatFunc = null)
    {
        // Città più corta ha 2 cifre
        if (mb_strlen($partialName) < 2)
        {
            $res = [];
        } else
        {
            $sql = 'select lc.id, lc.name, lc.zip_code, ls.iso_code as statecode from #__localization_city lc 
                left join #__localization_state ls on ls.id = lc.id_state 
                where lc.name like :name
                order by name';
            $q = $this->db->prepare($sql);
            $q->bindValue(':name', $this->db->util->prepareLikeValue($partialName) . '%');
            $q->execute();
            $res = $this->formatResult($q, $formatFunc);
        }
        return $res;
    }

    public function getFuncFormatCityStateText()
    {
        return function ($row)
        {
            return ['id' => (int) $row['id'], 'text' => sprintf('%s (%s)', $row['name'], $row['statecode'])];
        };
    }

    public function getFuncFormatCityStateName()
    {
        return function ($row)
        {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s)', $row['name'], $row['statecode'])];
        };
    }

}
