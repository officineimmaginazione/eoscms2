<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;

class LogModel extends \EOS\System\Models\Model
{

    const LOCK_STATUS = 99;
    const VALID_STATUS = 100;

    public function clear()
    {
        $days = $this->db->setting->getInt('account', 'log.days', 30);
        $dateClear = DateTimeHelper::incDay(DateTimeHelper::now(), -$days);
        $sql = 'delete from #__account_log where logdate <= :logdate';
        $q = $this->db->prepare($sql);
        $q->bindValue(':logdate', $this->db->util->dateTimeToDBDateTime($dateClear));
        $q->execute();
    }

    public function add($username, $idUser, $cmd, $status)
    {
        $this->clear();

        $sql = 'insert into #__account_log (logdate, logname, ip, id_user, cmd, status) values ' .
            '(:logdate, :logname, :ip, :id_user, :cmd, :status) ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':logdate', $this->db->util->nowToDBDateTime());
        $q->bindValue(':logname', $username);
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':cmd', $cmd);
        $q->bindValue(':status', $status);
        $q->execute();
    }

    public function isLockedIP()
    {
        $sql = 'select count(id) as c from #__account_log where ip = :ip and status = :lockstatus and logdate >= :logdate ';
        $lockMinutes = $this->db->setting->getInt('account', 'lock.minutes', 10);
        $dateLock = DateTimeHelper::incMinute(DateTimeHelper::now(), -$lockMinutes);
        $q = $this->db->prepare($sql);
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':logdate', $this->db->util->dateTimeToDBDateTime($dateLock));
        $q->bindValue(':lockstatus', static::LOCK_STATUS);
        $q->execute();
        $r = $q->fetch();
        return $r['c'] > 0;
    }

    public function isIPLimit()
    {
        $retryLimit = $this->db->setting->getInt('account', 'retry.limit', 10);
        $sql = 'select count(id) as c from #__account_log where ip = :ip and status <= :lockstatus and logdate >= :logdate ' .
            'order by logdate desc limit 1';
        $lockMinutes = $this->db->setting->getInt('account', 'lock.minutes', 10);
        $dateLock = DateTimeHelper::incMinute(DateTimeHelper::now(), -$lockMinutes);
        $q = $this->db->prepare($sql);
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':logdate', $this->db->util->dateTimeToDBDateTime($dateLock));
        $q->bindValue(':lockstatus', static::LOCK_STATUS);
        $q->execute();
        $r = $q->fetch();
        return $r['c'] >= $retryLimit;
    }

    public function countCmdMinute($cmd, $minute = 0)
    {
        $sql = 'select count(id) as c from #__account_log where ip = :ip and cmd = :cmd and logdate >= :logdate ';
        if (empty($minute))
        {
            $minute = $this->db->setting->getInt('account', 'lock.minutes', 10);
        }
        $dateLock = DateTimeHelper::incMinute(DateTimeHelper::now(), -$minute);
        $q = $this->db->prepare($sql);
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':logdate', $this->db->util->dateTimeToDBDateTime($dateLock));
        $q->bindValue(':cmd', $cmd);
        $q->execute();
        $r = $q->fetch();
        return $r['c'];
    }

}
