<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\StringHelper;
use EOS\System\Util\ArrayHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Security\CryptHelper;

class ServiceModel extends \EOS\System\Models\Model
{

    public function __construct($container)
    {
        parent::__construct($container);
        $this->path = $this->container->get('path');
        $this->session = $this->container->get('session');
    }

    public function updateUserStatus($idUser, $status)
    {
        $sql = 'update #__account_user set status = :status where id = :id_user';
        $q = $this->db->prepare($sql);
        $q->bindValue(':status', $status);
        $q->bindValue(':id_user', $idUser);
        $q->execute();
    }

    public function updateUserPassword($idUser)
    {
        $pwd = new \EOS\System\Security\Password($this->container);
        $password = $pwd->generateRandom();
        $sql = 'update #__account_user set password = :password, password_date = :password_date where id = :id_user';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':password', $pwd->hashPassword($password));
        $q->bindValue(':password_date', $this->db->util->nowToDBDateTime());
        $q->execute();
        return $password;
    }

    public function getUserData($id)
    {
        $data = null;
        $idC = StringHelper::convertToInt($id);
        if ($idC !== 0)
        {
            $sql = 'select id, email, username, name, options, id_lang from #__account_user where id = :id';
            $q = $this->db->prepare($sql);
            $q->bindValue(':id', $idC);
            $q->execute();
            $r = $q->fetch();
            if (!empty($r))
            {
                $data['id'] = (int) $r['id'];
                $data['username'] = $r['username'];
                $data['email'] = $r['email'];
                $data['name'] = $r['name'];
                $data['options'] = ArrayHelper::fromJSON($r['options']);
                $data['id_lang'] = $r['id_lang'];
                // Aggiungo il tipo di autenticazione
                $data['auth-type'] = $this->db->setting->getInt('account', 'auth.type', 0);
                $ev = new \EOS\System\Event\DataEvent($this, $data);
                $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::SERVICE_USER_DATA_PARSE, $ev);
                $data = $ev->getData();
            }
        }
        return $data;
    }

    public function clearUserCmdExpirated()
    {
        $sql = 'delete from #__account_user_cmd where cmdexpiration < :cmdexpiration  ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':cmdexpiration', $this->db->util->nowToDBDateTime());
        $q->execute();
    }

    // Default token 1 settimana
    public function addUserCmd($idUser, $cmd, $expirationMinutes = 10080, $options = [])
    {
        // Cerco di creare un token univoco, anche solo l'id aggiunto lo renderà table
        $token = md5(uniqid(CryptHelper::randomString(16), true)) . CryptHelper::randomString(16);
        $exp = DateTimeHelper::incMinute(DateTimeHelper::now(), $expirationMinutes);
        $sql = 'insert into #__account_user_cmd (token, id_user, cmd, cmddate, cmdexpiration, ip, options) values ' .
            '(:token, :id_user, :cmd, :cmddate, :cmdexpiration, :ip, :options) ';
        $q = $this->db->prepare($sql);
        // Scrivo l'hash del token così per maggiore sicurezza, ma non è univoco e devo usare l'id per averlo univoco
        $q->bindValue(':token', hash('sha512', $token));
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':cmd', $cmd);
        $q->bindValue(':cmddate', $this->db->util->nowToDBDateTime());
        $q->bindValue(':cmdexpiration', $this->db->util->dateTimeToDBDateTime($exp));
        $q->bindValue(':ip', $this->container->get('session')->getClientIP());
        $q->bindValue(':options', \EOS\System\Util\ArrayHelper::toJSON($options));
        $q->execute();
        // La prima parte è il token e l'ultima l'id
        $token = $token . '-' . $this->db->lastInsertId();
        return $token;
    }

    public function getUserCmd($token, $cmd)
    {
        $res = null;
        if ((strlen($token) > 50) && (strlen($token) < 200)) 
        {
            $l = explode('-', $token);
            if (count($l) == 2)
            {
                $id = StringHelper::convertToInt($l[1]);
                if ($id !== 0)
                {
                    $sql = 'select * from #__account_user_cmd where id = :id and cmd = :cmd';
                    $q = $this->db->prepare($sql);
                    $q->bindValue(':id', $id);
                    $q->bindValue(':cmd', $cmd);
                    $q->execute();
                    $r = $q->fetch();
                    if (!empty($r))
                    {
                        if (CryptHelper::hashEquals(hash('sha512', $l[0]), $r['token']))
                        {
                            $res = $r;
                        }
                    }
                }
            }
        }

        return $res;
    }

    public function deleteUserCmd($idUser, $cmd)
    {
        $sql = 'delete from #__account_user_cmd where id_user = :id_user and cmd = :cmd';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_user', $idUser);
        $q->bindValue(':cmd', $cmd);
        $q->execute();
    }

    public function getCmdUserData($token, $cmd)
    {
        $data = null;
        $this->clearUserCmdExpirated(); // Cancello i comandi scaduti
        $r = $this->getUserCmd($token, $cmd);
        if (!empty($r))
        {
            $this->deleteUserCmd($r['id_user'], $cmd);
            $data = $this->getUserData($r['id_user']);
        }
        return $data;
    }

    public function requestActive($token)
    {
        $res = false;
        $data = $this->getCmdUserData($token, 'request-active');
        if (!empty($data))
        {
            // Forzo il cambio di lingua con quello dell'utente
            $this->lang->setLangFromID($data['id_lang']);
            // Reimposto lo stato a 2 così vole dire che aspetto notifica
            $this->updateUserStatus($data['id'], 2);
            $msg = new \EOS\System\Mail\Message($this->container);
            $msg->to = $data['email'];
            $msg->subject = $this->lang->trans('account.service.mail.request.active.subject');
            $newToken = $this->addUserCmd($data['id'], 'active');
            $data['link'] = $this->path->getDomainUrl() . $this->path->urlFor('account', ['service', 'active', $newToken]);
            $data['ip'] = $this->session->getClientIP();
            $msg->loadComponentContent('Account', 'service/mail/request-active', $data);
            $res = $msg->send();
        }
        return $res;
    }

    public function active($token)
    {
        $res = false;
        $data = $this->getCmdUserData($token, 'active');
        if (!empty($data))
        {
            // Forzo il cambio di lingua con quello dell'utente
            $this->lang->setLangFromID($data['id_lang']);
            // Reimposto lo stato a 1 che è attivato l'utente
            $this->updateUserStatus($data['id'], 1);
            $msg = new \EOS\System\Mail\Message($this->container);
            $msg->to = $data['email'];
            $msg->subject = $this->lang->trans('account.service.mail.active.subject');
            $data['link'] = $this->path->getDomainUrl() . $this->path->urlFor('account', ['access', 'login']);
            $data['ip'] = $this->session->getClientIP();
            $msg->loadComponentContent('Account', 'service/mail/active', $data);
            $res = $msg->send();
        }
        return $res;
    }

    public function requestResetPass($token)
    {
        $res = false;
        $data = $this->getCmdUserData($token, 'request-reset-pass');
        if (!empty($data))
        {
            // Forzo il cambio di lingua con quello dell'utente
            $this->lang->setLangFromID($data['id_lang']);
            $msg = new \EOS\System\Mail\Message($this->container);
            $msg->to = $data['email'];
            $msg->subject = $this->lang->trans('account.service.mail.request.reset-pass.subject');
            // Cancello i vecchi reset e aggiungo solo l'ultimo
            $this->deleteUserCmd($data['id'], 'reset-pass');
            $newToken = $this->addUserCmd($data['id'], 'reset-pass');
            $data['link'] = $this->path->getDomainUrl() . $this->path->urlFor('account', ['service', 'reset-pass', $newToken]);
            $data['ip'] = $this->session->getClientIP();
            $msg->loadComponentContent('Account', 'service/mail/request-reset-pass', $data);
            $res = $msg->send();
        }
        return $res;
    }

    public function resetPass($token)
    {
        $res = false;
        $data = $this->getCmdUserData($token, 'reset-pass');
        if (!empty($data))
        {
            // Forzo il cambio di lingua con quello dell'utente
            $this->lang->setLangFromID($data['id_lang']);
            $msg = new \EOS\System\Mail\Message($this->container);
            $msg->to = $data['email'];
            $msg->subject = $this->lang->trans('account.service.mail.reset-pass.subject');
            $data['link'] = $this->path->getDomainUrl() . $this->path->urlFor('account', ['access', 'login']);
            $data['ip'] = $this->session->getClientIP();
            $data['new-password'] = $this->updateUserPassword($data['id']);
            $msg->loadComponentContent('Account', 'service/mail/reset-pass', $data);
            $res = $msg->send();
        }
        return $res;
    }

}
