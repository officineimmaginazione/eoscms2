<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\ArrayHelper;

class UserModel extends \EOS\System\Models\Model
{

    private function isValidOldPwd(string $password): bool
    {
        $user = new \EOS\Components\Account\Classes\AuthUser($this->container);
        $res = false;
        $sql = 'select id, username, email, name, password from #__account_user where status = 1 and id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $user->getUserID());
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            $res = $user->signin($r['password'], $password);
        }
        return $res;
    }

    public function getAuthType(): int
    {
        return $this->db->setting->getInt('account', 'auth.type', 0);
    }

    public function getUserData(int $id): array
    {
        $sm = new ServiceModel($this->container);
        $uData = $sm->getUserData($id);
        return is_null($uData) ? [] : $uData;
    }

    public function prepareEditData(array &$data, string &$error): bool
    {
        $res = false;
        switch (ArrayHelper::getStr($data, 'command'))
        {
            case 'update-pwd':
                if (!$this->isValidOldPwd(ArrayHelper::getStr($data, 'pwd-old')))
                {
                    $error = $this->lang->trans('account.user.error.pwdold');
                } else if ((mb_strlen(ArrayHelper::getStr($data, 'pwd-new1')) < 8) || (mb_strlen(ArrayHelper::getStr($data, 'pwd-new1')) > 100))
                {
                    $error = $this->lang->trans('account.user.error.pwd1');
                } else if (ArrayHelper::getStr($data, 'pwd-new1') !== ArrayHelper::getStr($data, 'pwd-new2'))
                {
                    $error = $this->lang->trans('account.user.error.pwd2');
                } else
                {
                    $res = true;
                }

                break;

            default:
                $error = $this->lang->trans('system.common.invalid.command');
                break;
        }
        return $res;
    }

    public function editData(array $data, string &$error): bool
    {
        $res = $this->prepareEditData($data, $error);
        if ($res)
        {
            switch (ArrayHelper::getStr($data, 'command'))
            {
                case 'update-pwd':

                    $user = new \EOS\Components\Account\Classes\AuthUser($this->container);
                    $sql = 'update #__account_user set
                     password = :password,
                     password_date = :password_date,
                     up_id = :up_id,
                     up_date = :up_date
                    where status = 1 and id = :id';
                    $q = $this->db->prepare($sql);
                    $pass = new \EOS\System\Security\Password($this->container);
                    $q->bindValue(':password', $pass->hashPassword($data['pwd-new1']));
                    $q->bindValue(':password_date', $this->db->util->nowToDBDateTime());
                    $q->bindValue('up_id', 0);
                    $q->bindValue('up_date', $this->db->util->nowToDBDateTime());
                    $q->bindValue(':id', $user->getUserID());
                    $q->execute();
                    break;

                default:
                    $res = false;
                    $error = $this->lang->trans('system.common.invalid.command');
                    break;
            }
        }
        return $res;
    }

}
