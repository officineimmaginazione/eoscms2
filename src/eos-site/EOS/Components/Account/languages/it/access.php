<?php

return
    [
        'account.access.login.title' => 'Area riservata',
        'account.access.login' => 'Accedi',
        'account.access.login.forgot' => 'Credenziali dimenticate?',
        'account.access.login.register' => 'Registra',
        'account.access.logname1' => 'Email',
        'account.access.logname2' => 'Utente',
        'account.access.logname3' => 'Utente o Email',
        'account.access.password' => 'Password',
        'account.access.login.error0' => 'Accesso momentaneamente non disponibile, attendere almeno 10 minuti e riprovare',
        'account.access.login.error1' => 'Email o password non validi!',
        'account.access.login.error2' => 'Utente/Email o password non validi!',
        'account.access.login.error3' => 'Utente o password non validi!',
        'account.access.error-ip' => 'Numero di tentativi superato, attendere almeno 10 minuti e riprovare',
        'account.access.error-request-second' => 'Numero di richieste al servizio troppo elevate attendere 1 secondo!',
        'account.access.active.message' => 'Attivazione avvenuta correttamente',
        'account.access.reset-pass.message' => 'Nuova password inviata tramite e-mail',
        'account.access.forgot.title' => 'Recupera le credenziali',
        'account.access.forgot.subtitle' => 'Inserisci il tuo indirizzo e-mail qui sotto per recuperare il tuo nome utente / password',
        'account.access.forgot.logname1' => 'Email',
        'account.access.forgot.logname2' => 'Utente o Email',
        'account.access.forgot.logname3' => 'Utente o Email',
        'account.access.forgot' => 'Invia',
        'account.access.forgot.error0' => 'Accesso momentaneamente non disponibile, attendere almeno 10 minuti e riprovare',
        'account.access.forgot.error1' => 'Email non valida!',
        'account.access.forgot.error2' => 'Utente non valido!',
        'account.access.forgot.error3' => 'Utente o Email non validi!',
        'account.access.forgot.send' => 'Richiesta ripristino password inviata via email!',
        'account.access.register.title' => 'Registrazione',
        'account.access.register.name' => 'Nome',
        'account.access.register.email' => 'Email',
        'account.access.register.username' => 'Utente',
        'account.access.register.pwd1' => 'Password',
        'account.access.register.pwd2' => 'Ripeti password',
        'account.access.register.consent' => 'Acconsento al trattamento dei miei dati',
        'account.access.register' => 'Registra',
        'account.access.register.error.limit' => 'Attendere qualche minuto prima di registrare un altro utente',
        'account.access.register.error.name' => 'Nome non valido!',
        'account.access.register.error.email' => 'Email non valida!',
        'account.access.register.error.username' => 'Utente non valido (minimo 6 caratteri)!',
        'account.access.register.error.pwd1' => 'Password non valida (minimo 8 caratteri)!',
        'account.access.register.error.pwd2' => 'Password non corrispondenti!',
        'account.access.register.error.consent' => 'Acconsentire al trattamento dei dati!',
        'account.access.register.error.userexists' => 'Utente già registrato!',
        'account.access.register.send' => 'Grazie della registrazione a breve riceverete l\'e-mail per confermare l\'attivazione!'
];
