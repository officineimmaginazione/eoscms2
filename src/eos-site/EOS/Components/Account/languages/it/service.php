<?php

return
    [
        'account.service.mail.request.active.subject' => 'Attivazione account',
        'account.service.mail.request.active.link' => 'Per confermare l\'attivazione, fai clic sul link qui sotto:',
        'account.service.mail.request.reset-pass.subject' => 'Richiesta ripristino password',
        'account.service.mail.request.reset-pass.link' => 'Per reimpostare la tua password, fai clic sul link qui sotto e verrà inviata una nuova password generata:',
        'account.service.mail.active.subject' => 'Benvenuto',
        'account.service.mail.active.link' => 'Accedi all\'area riservata:',
        'account.service.mail.reset-pass.subject' => 'Nuova password',
        'account.service.mail.reset-pass.newpass' => 'La nuova password è:'
];
