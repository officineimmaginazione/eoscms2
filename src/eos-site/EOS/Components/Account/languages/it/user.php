<?php

return
    [
        'account.user.edit.title' => 'Profilo',
        'account.user.edit.name' => 'Nome',
        'account.user.edit.email' => 'Email',
        'account.user.edit.username' => 'Utente',
        'account.user.edit.pwd' => 'Password',
        'account.user.edit.save' => 'Salva',
        'account.user.edit.cancel' => 'Annulla',
        'account.user.modal.pwd.title' => 'Modifica password',
        'account.user.modal.pwd.old' => 'Vecchia password',
        'account.user.modal.pwd.new1' => 'Password',
        'account.user.modal.pwd.new2' => 'Ripeti password',
        'account.user.error.pwdold' => 'Vecchia password non valida!',
        'account.user.error.pwd1' => 'Nuova password non valida (minimo 8 caratteri)!',
        'account.user.error.pwd2' => 'Password non corrispondenti!'
];
