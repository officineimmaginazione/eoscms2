<?php

$this->mapComponent(['GET'], 'Account', ['access/login' => 'AccessController:login']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxlogin' => 'AccessController:ajaxLogin']);
$this->mapComponent(['GET'], 'Account', ['access/forgot' => 'AccessController:forgot']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxforgot' => 'AccessController:ajaxForgot']);
$this->mapComponent(['GET'], 'Account', ['access/register' => 'AccessController:register']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxregister' => 'AccessController:ajaxRegister']);
$this->mapComponent(['GET'], 'Account', ['access/active' => 'AccessController:active']);
$this->mapComponent(['GET'], 'Account', ['access/reset-pass' => 'AccessController:resetPass']);
$this->mapComponent(['GET'], 'Account', ['service/request-active/{token}' => 'ServiceController:requestActive']);
$this->mapComponent(['GET'], 'Account', ['service/active/{token}' => 'ServiceController:active']);
$this->mapComponent(['GET'], 'Account', ['service/request-reset-pass/{token}' => 'ServiceController:requestResetPass']);
$this->mapComponent(['GET'], 'Account', ['service/reset-pass/{token}' => 'ServiceController:resetPass']);
$this->mapComponent(['GET'], 'Account', ['user/index' => 'UserController:index']);
$this->mapComponent(['GET'], 'Account', ['user/logout' => 'UserController:logout']);
$this->mapComponent(['GET'], 'Account', ['user/edit' => 'UserController:edit']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxedit' => 'UserController:ajaxEdit']);
$this->mapComponent(['POST'], 'Account', ['address/ajaxcommand' => 'AddressController:ajaxCommand']);
$this->mapComponent(['GET'], 'Account', ['address/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'AddressController:getDataSource']);