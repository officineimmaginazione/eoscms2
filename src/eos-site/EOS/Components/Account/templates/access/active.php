<div id="account-active" class="w-100">
    <div id="account-active-container" class="container text-center">
        <label id="account-active-title">
            <?php
            $this->transPE('account.access.active.message');
            ?></label>
    </div>
</div>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();

