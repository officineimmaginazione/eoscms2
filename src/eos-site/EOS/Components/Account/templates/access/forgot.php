<form id="account-forgot" method="POST" class="w-100">
    <div id="account-forgot-container" class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <label id="account-forgot-title"><?php $this->transPE('account.access.forgot.title'); ?></label>
                <label id="account-forgot-subtitle"><?php $this->transPE('account.access.forgot.subtitle'); ?></label>
                <div class="form-group">
                    <input id="account-forgot-name" name="log-name" type="text" class="form-control" placeholder="<?php $this->transPE('account.access.forgot.logname' . $this->authType); ?>">
                </div>
                <div class="form-group" >
                    <button id="account-forgot-send" class="btn btn-primary"><?php $this->transPE('account.access.forgot'); ?></button>
                </div>               
            </div>
        </div>
    </div>
    <?php
    $this->util->writeAsyncTokenHtml();
    $this->util->writeAsyncRequestUrl();
    ?>
</form>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
