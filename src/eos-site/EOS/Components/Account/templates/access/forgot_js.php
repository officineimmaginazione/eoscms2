<script>
    $(function ()
    {
        $('#account-forgot-send').click(function (event)
        {
            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function ()
            {
                btn.removeAttr('disabled');
            });

            var data = $('#account-forgot').serializeFormJSON();
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('account', ['access', 'ajaxforgot']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        bootbox.alert(json.message, function ()
                        {
                            location.href = '<?php echo $this->path->urlFor('account', ['access', 'login']); ?>';
                        });
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                    btn.trigger('unlock');
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    btn.trigger('unlock');
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    });
</script>