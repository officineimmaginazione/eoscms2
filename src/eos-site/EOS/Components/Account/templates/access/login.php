<form id="account-login" method="POST" class="w-100">
    <div id="account-login-container" class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <label id="account-login-title"><?php $this->transPE('account.access.login.title'); ?></label>
                <div class="form-group">
                    <input id="account-login-name" name="log-name" type="text" class="form-control" placeholder="<?php $this->transPE('account.access.logname' . $this->authType); ?>">
                </div>
                <div class="form-group" >
                    <input id="account-login-pwd" name="log-pwd" type="password" class="form-control" placeholder="<?php $this->transPE('account.access.password'); ?>">
                </div>
                <div class="form-group" >
                    <button id="account-login-send" class="btn btn-primary"><?php $this->transPE('account.access.login'); ?></button>
                    <?php
                    if ($this->authForgot)
                    {
                        ?>
                        <a id="account-forgot-send" class="pull-right" href="<?php echo $this->path->urlFor('account', ['access', 'forgot']); ?>"><?php $this->transPE('account.access.login.forgot'); ?></a>
                    <?php } ?>
                </div>   
                <?php
                if ($this->authRegister)
                {
                    ?>
                    <div class="form-group" >
                        <a id = "account-register-send" class = "pull-right" href = "<?php echo $this->path->urlFor('account', ['access', 'register']); ?>"><?php $this->transPE('account.access.login.register'); ?></a>
                    </div>     
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
    $this->util->writeAsyncTokenHtml();
    $this->util->writeAsyncRequestUrl();
    ?>
</form>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
