<script>
    $(function ()
    {
        $('#account-login-send').click(function (event)
        {
            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function ()
            {
                btn.removeAttr('disabled');
            });
            var data = $('#account-login').serializeFormJSON();
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('account', ['access', 'ajaxlogin']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        location.href = json.redirect;
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                    btn.trigger('unlock');
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    btn.trigger('unlock');
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    });
</script>