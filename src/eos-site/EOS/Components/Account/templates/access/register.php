<form id="account-register" method="POST" class="w-100">
    <div id="account-register-container" class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <label id="account-register-title"><?php $this->transPE('account.access.register.title'); ?></label>
                <div class="form-group">
                    <label><?php $this->transPE('account.access.register.name'); ?></label>
                    <input id="account-register-name" name="reg-name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label><?php $this->transPE('account.access.register.email'); ?></label>
                    <input id="account-register-email" name="reg-email" type="email" class="form-control">
                </div>                <?php
                if ($this->authType != 1)
                {
                    ?>
                    <div class="form-group">
                        <label><?php $this->transPE('account.access.register.username'); ?></label>
                        <input id="account-register-username" name="reg-username" type="text" class="form-control">
                    </div>
<?php } ?>
                <div class="form-group" >
                    <label><?php $this->transPE('account.access.register.pwd1'); ?></label>
                    <input id="account-register-pwd1" name="reg-pwd1" type="password" class="form-control">
                </div>
                <div class="form-group" >
                    <label><?php $this->transPE('account.access.register.pwd2'); ?></label>
                    <input id="account-register-pwd2" name="reg-pwd2" type="password" class="form-control">
                </div>
                <div class="form-group reg-privacy-consent" >
                    <input id="account-register-consent" name="reg-consent" type="checkbox" class="form-control">
                    <?php $this->transP('account.access.register.consent'); ?>
                </div>
                <div class="form-group" >
                    <button id="account-register-send" class="btn btn-primary"><?php $this->transPE('account.access.register'); ?></button>
                </div>               
            </div>
        </div>
    </div>
    <?php
    $this->util->writeAsyncTokenHtml();
    $this->util->writeAsyncRequestUrl();
    ?>
</form>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
