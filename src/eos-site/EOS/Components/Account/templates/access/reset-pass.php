<div id="account-reset" class="w-100">
    <div id="account-reset-container" class="container text-center">
        <label id="account-reset-title">
            <?php
            $this->transPE('account.access.reset-pass.message');
            ?>
        </label>
    </div>
</div>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();

