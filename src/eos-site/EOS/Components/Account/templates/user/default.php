<?php 
$userlabel = 'Account non autenticato';
if(isset($this->controller->user)) {
    $user = $this->controller->user->getInfo();
    $userlabel = $user->username;
}
?>
<section class="admin-main-header">
    <div class="d-flex flex-row align-items-center align-content-center">
        <h1 class="main-header-title">Riepilogo utente</h1><br>
    </div>
    <div class="row">
        <div class="col-12">
            <h5>Benvenuto <?php echo $user->username; ?> (<?php echo $user->email; ?>)</h5>
        </div>
    </div>
    <div class="row">
        <?php
        $this->getEventDispatcher()->dispatch(EOS\Components\Account\Classes\Events::USER_INDEX_TEMPLATE_RENDER, new \EOS\System\Event\EchoEvent($this));
        ?>
    </div>
</section>