<div id="account-user-edit" class="w-100">
    <div id="account-user-edit-container" class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <label id="account-user-edit-title"><?php $this->transPE('account.user.edit.title'); ?></label>
                <div class="form-group">
                    <label><?php $this->transPE('account.user.edit.name'); ?></label>
                    <div class="input-group">
                        <input name="edit-name" type="text" class="form-control" value="<?php $this->printHtml($this->data['name']); ?>"  disabled>
                        <div class="input-group-append"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label><?php $this->transPE('account.user.edit.email'); ?></label>
                    <div class="input-group">
                        <input name="edit-email" type="email" class="form-control" value="<?php $this->printHtml($this->data['email']); ?>"  disabled>
                        <div class="input-group-append"></div>
                    </div>
                </div>                
                <?php
                if ($this->data['auth-type'] != 1)
                {
                    ?>
                    <div class="form-group">
                        <label><?php $this->transPE('account.user.edit.username'); ?></label>
                        <div class="input-group">
                            <input name="edit-username" type="text" class="form-control" value="<?php $this->printHtml($this->data['username']); ?>" disabled>
                            <div class="input-group-append"></div>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label><?php $this->transPE('account.user.edit.pwd'); ?></label>
                    <div class="input-group">
                        <input name="edit-pwd" type="password" class="form-control"  disabled>
                        <div class="input-group-append"><button id="account-user-edit-pwd" class="input-group-text fa fa-pencil-alt"></button></div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
</div>  
<?php
$this->includePartial('pwd');
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
