<script>
    $(function ()
    {
        $('#account-user-edit-pwd').click(function (event)
        {
            event.preventDefault();
            $('#account-user-modal-pwd').modal();
        });

        $('#account-user-modal-pwd').on('hidden.bs.modal', function ()
        {
            window.location.reload();
        });



        $('#account-user-modal-pwd-save').click(function (event)
        {
            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function ()
            {
                btn.removeAttr('disabled');
            });
            var data = $('#account-user-modal-pwd-form').serializeFormJSON();
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('account', ['user', 'ajaxedit']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json"
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        $('#account-user-modal-pwd').modal('hide');
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                    btn.trigger('unlock');
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    btn.trigger('unlock');
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    });
</script>