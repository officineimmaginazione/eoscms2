<div class="modal fade" id="account-user-modal-pwd" tabindex="-1" role="dialog" aria-labelledby="account-user-modal-pwd-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account-user-modal-pwd-title"><?php $this->transPE('account.user.modal.pwd.title'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="account-user-modal-pwd-form" method="POST">
                    <div class="form-group">
                        <label for="account-user-modal-pwd-old"><?php $this->transPE('account.user.modal.pwd.old'); ?></label>
                        <input type="password" class="form-control" id="account-user-modal-pwd-old" name="pwd-old">
                    </div>
                    <div class="form-group">
                        <label for="account-user-modal-pwd-new1"><?php $this->transPE('account.user.modal.pwd.new1'); ?></label>
                        <input type="password" class="form-control" id="account-user-modal-pwd-new1" name="pwd-new1">
                    </div>
                    <div class="form-group">
                        <label for="account-user-modal-pwd-new2"><?php $this->transPE('account.user.modal.pwd.new2'); ?></label>
                        <input type="password" class="form-control" id="account-user-modal-pwd-new2" name="pwd-new2">
                    </div>
                    <input type="hidden" name="command" value="update-pwd">
                    <?php
                    $this->util->writeAsyncTokenHtml();
                    $this->util->writeAsyncRequestUrl();
                    ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="account-user-modal-pwd-cancel"><?php $this->transPE('account.user.edit.cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="account-user-modal-pwd-save"><?php $this->transPE('account.user.edit.save'); ?></button>
            </div>
        </div>
    </div>
</div>