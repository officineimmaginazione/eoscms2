<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

class CartBase extends \EOS\System\Models\Model
{

    public $id = 0;
    public $rows = [];

    private function getDBRow($id)
    {
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart and cr.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $this->id);
        $q->bindValue(':id', $id);
        $q->execute();
        return $q->fetch();
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->session = $container->get('session');
        $this->open(false);
    }

    private function open($autocreate = true)
    {
        $this->checkDataCart();
        if ($this->session->has('cart.id'))
        {
            $this->id = (int) $this->session->get('cart.id');
            $cart = $this->getCart();
            if (isset($cart['status']) && $cart['status'] == 2)
            {
                $this->db->beginTransaction();
                $tbl = $this->db->tableFix('#__cart');
                $values = [
                    'status' => 0,
                    'total' => 0,
                ];
                $query = $this->db->newFluent()->insertInto($tbl, $values);
                $query->execute();
                $this->id = $this->db->lastInsertId();
                $this->session->set('cart.id', $this->id);
                $this->session->set('cart.time', time());
                $this->db->commit();
            }
        } else
        {
            $this->db->beginTransaction();
            $tbl = $this->db->tableFix('#__cart');
            $values = [
                'status' => 0,
                'total' => 0,
            ];
            $query = $this->db->newFluent()->insertInto($tbl, $values);
            $query->execute();
            $this->id = $this->db->lastInsertId();
            $this->session->set('cart.id', $this->id);
            $this->session->set('cart.time', time());
            $this->db->commit();
        }
        if (empty($this->id))
        {
            throw new Exception('Invalid ID Cart');
        } else
        {
            //carica riga
        }
    }

    public function clearCart()
    {
        $this->id = 0;
        $this->rows = [''];
        $this->session->set('cart.id', null);
        $this->session->set('cart.time', 0);
    }

    public function addRow($idproduct, $quantity, $options = [])
    {
        $this->open(true);
        $this->db->beginTransaction();
        $tbl = $this->db->tableFix('#__cart_row');
        $values = [
            'id_cart' => $this->id,
            'id_product' => $idproduct,
            'quantity' => $quantity,
            'options' => json_encode($options)
        ];
        $query = $this->db->newFluent()->insertInto($tbl, $values);
        $query->execute();
        $this->db->commit();
        $this->open(false);
    }

    public function addCustomRow($idproduct, $quantity, $type, $data)
    {
        $this->addRow($idproduct, $quantity, ['type' => $type, 'data' => $data]);
    }

    public function updateRow($idrow, $quantity)
    {
        $data = $this->getDBRow($idrow);
        $data['quantity'] = $quantity;
        if (empty($data))
        {
            throw new \Exception('Invalid row id!');
        }
        $ev = new \EOS\System\Event\DataEvent($this, $data);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_UPDATE_ROW, $ev);
        $evData = $ev->getData();
        $tbl = $this->db->tableFix('#__cart_row');
        $query = $this->db->newFluent()->update($tbl)->set($evData)->where('id', (int) $idrow);
        $query->execute();
        return true;
    }

    public function deleteRowById($id)
    {
        $this->db->beginTransaction();
        $tblCartProduct = $this->db->tableFix('#__cart_row');
        $query = $this->db->newFluent()->deleteFrom($tblCartProduct)->where('id', (int) $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function getProduct()
    {
        $sql = 'select p.id, p.price, pl.name, pl.description, ppc.id_category ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' inner join #__product_product_category ppc on p.id = ppc.id_product' .
            ' where pl.id_lang = :id_lang order by p.id ASC';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    // Uso in reservation per ottenere le righe senza usare gli eventi
    public function getDBRows($idCart = 0)
    {
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', (empty($idCart)) ? $this->id : $idCart);
        $q->execute();
        return $q->fetchAll();
    }

    public function getRows()
    {
        $sql = 'select cr.id, cr.id_product, pl.name, pl.description, cr.quantity, cr.options, p.price, pi.url, p.isbn ' .
            ' from #__cart_row cr ' .
            ' left outer join #__product p on cr.id_product = p.id ' .
            ' left outer join #__product_image pi on cr.id_product = pi.id_product and pi.pos = 0' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $this->id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();

        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getRowsByCart($id)
    {
        $sql = 'select cr.id, cr.id_product, pl.name, pl.description, cr.quantity, cr.options, p.price, pi.url, p.isbn ' .
            ' from #__cart_row cr ' .
            ' left outer join #__product p on cr.id_product = p.id ' .
            ' left outer join #__product_image pi on cr.id_product = pi.id_product and pi.pos = 0' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();

        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getRowQuantity($id_row)
    {
        $sql = 'select cr.quantity' .
            ' from #__cart_row cr ' .
            ' where cr.id = :id_row ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_row', $id_row);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getProductByID($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getProductPrice($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getCart()
    {
        $sql = 'select c.* ' .
            ' from #__cart c' .
            ' where c.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $this->id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getCartByID($id)
    {
        $sql = 'select c.* ' .
            ' from #__cart c' .
            ' where c.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function saveData(&$data)
    {
        $row = $this->checkRow($data['p_id'], $this->id);
        if (!isset($row['id']) || $row['id'] == '')
        {
            $this->addCustomRow($data['p_id'], $data['quantity'], 'product', null);
        } else
        {
            $cur_qty = $this->getRowQuantity($row['id'])["quantity"];
            $this->updateRow($row['id'], $cur_qty + $data['quantity']);
        }
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');
        $values['id'] = $data['cartid'];
        $values['status'] = $data['status'];
        if ((!isset($data['cartid'])) || ($data['cartid'] == 0))
        {
            $values['total'] = $data['total'];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else
        {
            $values['total'] = $data['total'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['cartid']);
        }
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['cartid']);
        $query->execute();
        $this->db->commit();

        return true;
    }

    public function updateOptions(&$data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');
        $values['options'] = $data['options'];
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function cartToOrder($id)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');
        $values['status'] = 2;
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function checkDataCart()
    {
        if ($this->session->has('cart.id') && $this->session->has('cart.time'))
        {
            $cartlifetime = 120 * 60;
            if (($this->session->get('cart.time') + $cartlifetime) < time())
            {
                $this->clearCart();
            }
        }
    }

    public function checkRow(int $id_product, int $id_cart): array
    {
        $sql = 'select cr.id ' .
            ' from #__cart_row cr' .
            ' where cr.id_cart = :idcart and cr.id_product = :idproduct';
        $q = $this->db->prepare($sql);
        $q->bindValue(':idcart', $id_cart);
        $q->bindValue(':idproduct', $id_product);
        $q->execute();
        $list = $q->fetchAll();
        $ev = new \EOS\System\Event\DataEvent($this, $list);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_CHECK_ROW, $ev);
        $checkedList = $ev->getData();
        $res = empty($checkedList) ? [] : array_values($checkedList)[0];
        return $res;
    }

    public function getCartListByID($id)
    {
        $sql = 'select c.* ' .
            ' from #__cart c' .
            ' order by c.id DESC limit 25 ';
        $q = $this->db->prepare($sql);
        //$q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

}
