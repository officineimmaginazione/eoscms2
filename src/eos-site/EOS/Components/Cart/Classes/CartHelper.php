<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

use EOS\System\Util\ArrayHelper;

class CartHelper
{

    public static function getCustomRowType($row)
    {
        $options = ArrayHelper::fromJSON($row['options']);
        return ArrayHelper::getStr($options, 'type');
    }

    public static function getCustomRowData($row)
    {
        $options = ArrayHelper::fromJSON($row['options']);
        return ArrayHelper::getArray($options, 'data');
    }

    public static function isCustomRow($row, $type)
    {
        return static::getCustomRowType($row) == $type;
    }

    public static function updateCustomRowData(&$row, $data)
    {
        $options = ArrayHelper::fromJSON($row['options']);
        if (empty(ArrayHelper::getStr($options, 'type')))
        {
            throw new \Exception('Invalid type custom row!!!!');
        }

        $options['data'] = $data;
        $row['options'] = ArrayHelper::toJSON($options);
    }

    public static function getCustomRowsByType($rows, $type)
    {
        $res = [];
        foreach ($rows as $row)
        {
            if (static::isCustomRow($row, $type))
            {
                $res[] = $row;
            }
        }
        return $res;
    }

}
