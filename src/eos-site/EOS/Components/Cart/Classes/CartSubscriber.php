<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

use EOS\System\Util\ArrayHelper;

class CartSubscriber extends \EOS\System\Event\Subscriber
{

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Account\Classes\Events::USER_INDEX_TEMPLATE_RENDER => 'displayCartRender'];
    }

    public function displayCartRender(\EOS\System\Event\EchoEvent $event)
    {
        $view = $event->getSubject();
        \EOS\UI\Loader\Library::load($view, 'Bootbox');
        $w = new \EOS\Components\Cart\Widgets\DisplayCartWidget($view);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        if(isset($view->controller->user)) {
            $user = $view->controller->user->getInfo();
            $id = $user->id;
        }
        $w->listcart = $m->getCartListByID($id);
        $w->write();
    }

}
