<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

class Events
{

    // Cart
    const CART_DATA_PARSE_ROWS = 'cart.cart.data.parse.rows';
    const CART_DATA_UPDATE_ROW = 'cart.cart.data.update.row';
    const CART_DATA_DELETE_ROW = 'cart.cart.data.delete.row';
    const CART_DATA_CHECK_ROW = 'cart.cart.data.check.row';

}
