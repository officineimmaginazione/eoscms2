<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class CartController extends \EOS\Components\System\Classes\SiteController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mc = new \EOS\Components\Product\Models\CatalogModel($this->container);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->cartid = $m->id;
        $v->rows = $m->getRows();
        $v->head = $m->getCartByID($m->id);
        $v->total = 0;
        $v->subtotal = 0;
        $v->shipping = 0;
        foreach ($v->rows as $row)
        {
            $v->subtotal += $row['quantity'] * $row['price'];
        }
        $v->total = $v->subtotal + $v->shipping;
        $v->title = $this->lang->transEncode('order.cart.title');
				return $v->render('cart/default', true);
    }
  

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Cart\Models\CartModel($this->container);
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            if (isset($data['command']))
            {
                switch ($data['command'])
                {
                    case 'savedata' :
                        $error = '';
                        $data = $fv->getInputData();
                        if ($m->saveData($data, $error))
                        {
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('order', 'order/index'));
                        } else
                        {
                            $fv->setMessage($error);
                        }
                        break;
                    case 'saveadddata' :
                        $error = '';
                        $data = $fv->getInputData();
                        if ($m->saveAddData($data, $error))
                        {
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        } else
                        {
                            $fv->setMessage($error);
                        }
                        break;
                    case 'updaterow' :
                        if ($m->updateRow($data['id'], $data['qty']))
                        {
                            $fv->setResult(true);
                        }
                        break;
                    case 'updatecart' :
                        if ($m->updateCart($data['id'], $data['idcustomer']))
                        {
                            $fv->setResult(true);
                        }
                        break;
                    case 'deleterow' :
                        if ($m->deleteRowById($data['id']))
                        {
                            $fv->setResult(true);
                        }
                        break;
                    case 'customerdata':
                        $data = $ma->getData(ArrayHelper::getInt($data, 'id-customer'));
                        $address = json_encode($data);
                        $fv->setOutputValue('address', $data);
                        $fv->setResult(true);
                        break;
                    default :
                        $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
                        break;
                }
            } else
            {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue')))
        {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Cart\Models\CartModel($this->container);
            switch ($datasource)
            {
                case 'address':
                    if(isset($this->user)) {
                        $user = $this->user->getInfo();
                        $id = $user->id;
                    }
                    $res = $m->getAddressList($m->getFuncFormatAddressName(), $id);
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

    public function confirm($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->title = $this->lang->transEncode('order.order.title');
        return $v->render('cart/confirm', true);
    }

}
