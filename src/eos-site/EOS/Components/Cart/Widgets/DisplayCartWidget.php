<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Widgets;

use EOS\System\Util\ArrayHelper;

class DisplayCartWidget extends \EOS\UI\Widget\Widget
{

    public $data = [];
    public $dataField = 'id_user';
    public $disabled = false; 
    public $multiple = false;

    public function write()
    {
    $this->renderTemplate('widget/displaycart');
    }

    public function getCurrentInfo()
    {
        $m = new \EOS\Components\Order\Models\OrderModel($this->view->controller->container);
    }

}
