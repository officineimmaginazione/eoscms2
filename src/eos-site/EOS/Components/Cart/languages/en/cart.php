<?php

return
    [
        'order.cart.title' => 'Cart',
        'order.cart.subtitle' => 'Summary',
        'order.cart.checkout' => 'Checkout',
        'order.cart.shipping.free' => 'Free shipping',
        'order.cart.product' => 'Product',
        'order.cart.quantity' => 'Quantity',
        'order.cart.subtotal' => 'Subtotal',
        'order.cart.shippingtotal' => 'Shipping price',
        'order.cart.total' => 'Total price'
];
