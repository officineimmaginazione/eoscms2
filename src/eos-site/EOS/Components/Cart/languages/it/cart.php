<?php

return
    [
        'order.cart.title' => 'Carrello',
        'order.cart.subtitle' => 'Riepilogo',
        'order.cart.checkout' => 'Procedi con il checkout',
        'order.cart.shipping.free' => 'Spedizione gratuita',
        'order.cart.product' => 'Prodotto',
        'order.cart.quantity' => 'Quantità',
        'order.cart.subtotal' => 'Subtotale',
        'order.cart.shippingtotal' => 'Spese di spedizione',
        'order.cart.total' => 'Totale',
        'product.search.search' => 'Cerca',
        'product.search.clean' => 'Pulisci filtri',
        'product.search.title' => 'Titolo',
        'product.search.select.category' => 'Seleziona genere',
        'product.search.select.manufacturer' => 'Seleziona brand'
];
