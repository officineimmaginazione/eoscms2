<?php $cartDescription = (new \EOS\Components\Content\Widgets\SectionWidget($this, 'cart-description'))->data; ?>
<section>
    <div class="page-mo-block green-page">
        <div class="title-block">
            <div class="container">
                <h3 class="title"><?php $this->transPE('order.cart.title'); ?></h3>
                <h3 class="subtitle"><?php $this->transPE('order.cart.subtitle'); ?></h3>
            </div>
        </div>
    </div>
    <div class="page-content">
        <div class="container">
            <div class="table-responsive hidden-xs">
                <table class="table">
                    <thead> 
                        <tr>  
                            <th><?php $this->transPE('order.cart.product'); ?></th>
                            <th>&nbsp;</th>
                            <th class="text-center"><?php $this->transPE('order.cart.quantity'); ?></th>
                            <th>&nbsp;</th>
                            <th><?php $this->transPE('order.cart.quantity'); ?></th> 
                        </tr> 
                    </thead> 
                    <tbody> 
                      <?php
                      foreach ($this->rows as $row)
                      {
                          ?>
                            <tr id="<?php echo $row['id']; ?>"> 
                                <td class="col-xs-4"><?php echo $row['name']; ?></td> 
                                <td class="col-xs-1"><button class="btn btn-circle-green minus" id="minus<?php echo $row['id']; ?>"><i class="fa fa-minus"></i></button></td> 
                                <td class="col-xs-1"><input id="qty<?php echo $row['id']; ?>" class="qty text-center" type="text" value="<?php echo $row['quantity']; ?>" data-id="<?php echo $row['id']; ?>" readonly/></td> 
                                <td class="col-xs-1 col-xs-offset-1"><button class="btn btn-circle-green add" id="add<?php echo $row['id']; ?>"><i class="fa fa-plus"></i></button></td> 
                                <td class="col-xs-1"><button class="btn btn-circle-bk delete" id="delete<?php echo $row['id']; ?>"><i class="fa fa-times"></i></button></td> 
                            </tr>
<?php } ?>  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="page-mo-block green-page">
        <div class="title-block">
            <div class="container">
              <?php
              if ($this->shipping > 0)
              {
                  ?>
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 text-left"><p class="cart-payment-summary"><?php $this->transPE('order.cart.subtotal'); ?></p></div>
                        <div class="col-xs-6 col-sm-5 text-right"><p class="cart-payment-summary"><?php echo number_format($this->subtotal, 2); ?></p></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 text-left"><p class="cart-payment-summary"><?php $this->transPE('order.cart.shippingtotal'); ?></p></div>
                        <div class="col-xs-6 col-sm-5 text-right"><p class="cart-payment-summary"><?php echo $this->shipping; ?></p></div>
                    </div>
<?php } ?>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 text-left"><p class="cart-payment-summary"><?php $this->transPE('order.cart.total'); ?></p></div>
                    <div class="col-xs-5 col-sm-5 text-right"><p class="cart-payment-summary"><?php echo number_format($this->total, 2); ?></p></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="white-azure-section">
    <div class="page-mo-block">
        <div class="title-block">
            <div class="container">
<?php echo $cartDescription["content"]; ?>
            </div>
        </div>
        <div class="section-content">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center">
<?php $this->includePartial('extra'); ?>
                </div>
                <div class="col-xs-12 col-sm-6 text-center">
                    <form id="cart" method="POST">
                        <input type="hidden" value="<?php echo $this->cartid; ?>" name="id">
                        <input type="hidden" value="1" name="status">
                        <input type="hidden" value="<?php echo number_format($this->total, 2); ?>" name="total">
                        <?php $this->writeTokenHtml(); ?>
                        <?php
                        if ($this->session->has('order.cart.id'))
                        {
                            ?>
                            <input type="hidden" value="savedata" name="command">
                            <button id="cart-confirm" class="btn btn-azure xs-mt-60"><?php $this->transPE('order.cart.checkout'); ?></button>
<?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
    function runAjax(data, resultEvent)
    {

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    resultEvent(json);
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $("#cart-confirm").click(function (event)
    {
        event.preventDefault();
        saveData();
    });

    function saveData()
    {
        var data = $('#cart').serializeFormJSON();
        runAjax(data, function (json)
        {
            location.href = json.redirect;
        });
    }

    $(function ()
    {
        var data;
        $('.add').on('click', function ()
        {
            $id = jQuery(this).attr("id");
            $id = $id.replace("add", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal))
            {
                $qty.val(currentVal + 1);
            }
            var data = {id: $id, qty: $qty.val(), command: 'updaterow', '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'};
            updateRow(data);
        });
        $('.minus').on('click', function ()
        {
            $id = jQuery(this).attr("id");
            $id = $id.replace("minus", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0)
            {
                $qty.val(currentVal - 1);
            }
            var data = {id: $id, qty: $qty.val(), command: 'updaterow', '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'};
            updateRow(data);
        });
        $('.delete').on('click', function ()
        {
            $id = jQuery(this).attr("id");
            $id = $id.replace("delete", "");
            var data = {id: $id, command: 'deleterow', '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'};
            deleteRow(data);
        });

    });

    function updateRow(data)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        setTimeout(function ()
        {
            window.location.reload(false)
        }, 200);
    }

    function deleteRow(data)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        setTimeout(function ()
        {
            window.location.reload(false)
        }, 200);
    }
</script>
<?php
$this->endCaptureScript();
