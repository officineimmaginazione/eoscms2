 <div class="col-12 col-sm-6">
    <h3>Carrelli</h3>
    <table class="table">
        <thead>
            <tr>
                <th>N° Carrello</th>
                <th>Customer</th>
                <th>Totale</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->listcart as $row) : ?>
                <tr class="<?php echo ($row["status"]==1) ? "table-success" : "" ?>">
                    <td><?php echo $row["id"]; ?></td>
                    <td><?php echo $row['id_customer']; ?></td>
                    <td><?php echo number_format($row['total'], 2); ?> &euro;</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
