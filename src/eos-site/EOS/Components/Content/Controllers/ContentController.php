<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Controllers;

use EOS\Components\Content\Classes\ContentType;
use \EOS\System\Util\ArrayHelper;
use EOS\System\Util\StringHelper;

class ContentController extends \EOS\Components\System\Classes\SiteController
{

    private function loadExternalItem($v, &$data)
    {
        if (ArrayHelper::getBool($data['options'], 'loadexternal', false))
        {
            $data['content'] = $v->renderExternal($data['content']);
            if (isset($data['options']))
            {
                $ext = ArrayHelper::getArray($data['options'], 'extra');

                for ($i = 0; $i < count($ext); $i++)
                {
                    if (isset($ext[$i]['html']))
                    {
                        for ($n = 0; $n < count($ext[$i]['html']); $n++)
                        {
                            $ext[$i]['html'][$n] = $v->renderExternal($ext[$i]['html'][$n]);
                        }
                    }
                }
                $data['options']['extra'] = $ext;
            }
        }
    }

    private function loadExternal($v)
    {
        $this->loadExternalItem($v, $v->data);

        if (isset($v->data['items']))
        {
            for ($i = 0; $i < count($v->data['items']); $i++)
            {
                $this->loadExternalItem($v, $v->data['items'][$i]);
            }
        }
    }

    public function page($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Content\Models\ContentModel($this->container);
        $idContent = StringHelper::convertToInt($args['id']);
        $v->data = $m->getItem($idContent, $this->lang->getCurrent()->id, ContentType::Page);
        if (empty($v->data))
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $this->seo->joinTitle($v, $v->data['title']);
        $this->seo->loadSeoView($v, 'content/page', $idContent, $this->lang->getCurrentID(), false);
        $this->currentParams['themeTemplate'] = $v->data['options']['template'];
        if ($v->data['options']['template'] == 'homepage')
        {
            $v->lastArticle = $m->getLastItem($this->lang->getCurrent()->id, ContentType::Article);
        }
        $this->loadExternal($v);
        $v->addBreadcrumb('content', 'page/' . $idContent, $v->data['title']);
        $v->addLangLinkList('content', 'page/'.$idContent, $m->getItemLangList($idContent, ContentType::Page));
        return $v->render('content/page', $v->data['options']['withtheme']);
    }

    public function article($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Content\Models\ContentModel($this->container);
        $idContent = StringHelper::convertToInt($args['id']);
        $v->data = $m->getItem($idContent, $this->lang->getCurrent()->id, ContentType::Article);
        if (empty($v->data))
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $parentList = [];
        $m->getParentList($v->data['parent'], $parentList);
        foreach ($parentList as $idParent)
        {
            $this->seo->loadSeoView($v, 'content/category', $idParent, $this->lang->getCurrentID(), true);
            $v->addBreadcrumb('content', 'category/' . $idParent, $m->getContentTitle($idParent, $this->lang->getCurrentID(), ContentType::Category));
        }
        $v->addBreadcrumb('content', 'article/' . $idContent, $v->data['title']);
        $this->seo->joinTitle($v, $v->data['title']);
        $this->seo->loadSeoView($v, 'content/article', $idContent, $this->lang->getCurrentID(), false);
        $this->currentParams['themeTemplate'] = $v->data['options']['template'];
        $this->loadExternal($v);
        $v->addLangLinkList('content', 'article/'.$idContent, $m->getItemLangList($idContent, ContentType::Article));
        return $v->render('content/article', $v->data['options']['withtheme']);
    }

    public function category($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Content\Models\ContentModel($this->container);
        $idContent = StringHelper::convertToInt($args['id']);
        $v->data = $m->getItem($idContent, $this->lang->getCurrent()->id, ContentType::Category);
        if (empty($v->data))
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $parentList = [];
        $m->getParentList($v->data['parent'], $parentList);
        foreach ($parentList as $idParent)
        {
            $this->seo->loadSeoView($v, 'content/category', $idParent, $this->lang->getCurrentID(), true);
            $v->addBreadcrumb('content', 'category/' . $idParent, $m->getContentTitle($idParent, $this->lang->getCurrentID(), ContentType::Category));
        }
        $v->addBreadcrumb('content', 'category/' . $idContent, $v->data['title']);
        $this->seo->joinTitle($v, $v->data['title']);
        $this->seo->loadSeoView($v, 'content/category', $idContent, $this->lang->getCurrentID(), false);
        $pageItemCount = $m->db->setting->getInt('content', 'category.page.count');
        $page = (int) $request->getQueryParam('page', 1);
        $items = $m->getItems($idContent, $this->lang->getCurrent()->id, ContentType::Article, $page, $pageItemCount);
        $v->data['items'] = $items['data'];
        $v->data['items-count'] = $items['count'];
        $v->data['page-count'] = ($pageItemCount == 0) ? 0 : ceil($items['count'] / $pageItemCount);
        if ($page > $v->data['page-count'])
        {
            $page = (int) $v->data['page-count'];
        }
        $v->data['page'] = (int) $page;
        $v->data['preview-length'] = $m->db->setting->getInt('content', 'category.page.preview.length');
        $this->currentParams['themeTemplate'] = $v->data['options']['template'];
        $this->loadExternal($v);

        $v->data['url-next'] = '';
        $url = $this->path->getUrlFor('content', 'category/' . $idContent);
        if (($v->data['page'] > 0) && ($v->data['page'] < $v->data['page-count']))
        {
            $v->data['url-next'] = $url . '?page=' . ($v->data['page'] + 1);
        }
        $v->data['url-prev'] = '';
        if (($v->data['page'] > 1) && ($v->data['page'] <= $v->data['page-count']))
        {
            $v->data['url-prev'] = $url;
            if ($v->data['page'] - 1 > 1)
            {
                $v->data['url-prev'] .= '?page=' . ($v->data['page'] - 1);
            }
        }
        if (empty(\EOS\System\Util\ArrayHelper::getStr($v->data['options'], 'customcode')))
        {
            if (!empty($v->data['url-prev']))
            {
                $v->addHeadLink(['rel' => 'prev', 'href' => $v->data['url-prev']]);
            }
            if (!empty($v->data['url-next']))
            {
                $v->addHeadLink(['rel' => 'next', 'href' => $v->data['url-next']]);
            }
        }
        $v->addLangLinkList('content', 'category/'.$idContent, $m->getItemLangList($idContent, ContentType::Category));
        return $v->render('content/category', $v->data['options']['withtheme']);
    }

}
