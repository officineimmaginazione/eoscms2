<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Models;

class ContentModel extends \EOS\System\Models\Model
{

    public function getItem($idContent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent)
            ->where('cl.id_lang = ?', (int) $idLang);
        if ($type == \EOS\Components\Content\Classes\ContentType::Article)
        {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
        }
        $r = $q->fetch();
        if (!empty($r))
        {
            if (empty($r['options']))
            {
                $r['options']['template'] = 'default';
            } else
            {
                $r['options'] = json_decode($r['options'], true);
            }

            if ((isset($r['publish_date']) && (!empty($r['publish_date']))))
            {
                $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
            }
            if ($type == \EOS\Components\Content\Classes\ContentType::Article)
            {
                $r['parent_title'] = $this->getContentTitle($r['parent'], $idLang, \EOS\Components\Content\Classes\ContentType::Category);
            }
        }

        return $r;
    }

    public function getLastItem($idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('cl.id_lang = ?', (int) $idLang)
            ->limit('1,1');

        if ($type == \EOS\Components\Content\Classes\ContentType::Article)
        {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
            $q->where('ca.publish_date <= ?', $this->db->util->nowToDBDateTime());
            $q->orderBy('ca.publish_date desc');
        } else if ($type == \EOS\Components\Content\Classes\ContentType::Article)
        {
            $q->orderBy('cl.ins_date desc');
        }
        $r = $q->fetch();
        if (!empty($r))
        {
            if (empty($r['options']))
            {
                $r['options']['template'] = 'default';
            } else
            {
                $r['options'] = json_decode($r['options'], true);
            }

            if ((isset($r['publish_date']) && (!empty($r['publish_date']))))
            {
                $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
            }
        }
        return $r;
    }

    public function getItems($idParent, $idLang, $type, $page, $pagesize)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))
            ->select(null)->select('c.id, cl.title, cl.subtitle, cl.content, cl.options, ca.publish_date')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            //->where('c.parent = ?', (int) $idParent) Remove to insert array or single value
            ->where('cl.id_lang = ?', (int) $idLang);
        if (is_array($idParent))
        {
            $q->where('c.parent in (' . implode(",", $idParent) . ')');
        } else
        {
            $q->where('c.parent = ?', (int) $idParent);
        }

        if ($type == \EOS\Components\Content\Classes\ContentType::Article)
        {
            $q->where('ca.publish_date <= ?', $this->db->util->nowToDBDateTime());
            $q->orderBy('ca.publish_date desc');
        } else
        {
            $q->orderBy('c.ins_date desc');
        }

        $start = ($page - 1) * $pagesize;
        $q->limit((int) $start . ',' . (int) $pagesize);
        $list = $q->fetchAll();
        if (!empty($list))
        {
            for ($i = 0; $i < count($list); $i++)
            {
                if ((isset($list[$i]['publish_date']) && (!empty($list[$i]['publish_date']))))
                {
                    $list[$i]['publish_date'] = $this->lang->dbDateToLangDate($list[$i]['publish_date']);
                }
                if (empty($list[$i]['options']))
                {
                    $list[$i]['options']['template'] = 'default';
                } else
                {
                    $list[$i]['options'] = json_decode($list[$i]['options'], true);
                }
            }
        }

        $q->limit(null)->orderBy(null);
        return ['data' => $list, 'count' => $q->count()];
    }

    public function getParentList($idParent, &$res)
    {
        if (!empty($idParent))
        {
            $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
                ->select(null)->select('c.id, c.parent')
                ->where('c.id = ?', (int) $idParent);

            $r = $q->fetch();
            if (!empty($r))
            {
                $this->getParentList($r['parent'], $res);
                $res[] = $idParent;
            }
        }
    }

    public function getContentTitle($idContent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('cl.title')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent)
            ->where('cl.id_lang = ?', (int) $idLang);
        $r = $q->fetch();
        if (empty($r))
        {
            return '';
        } else
        {
            return $r["title"];
        }
    }

    public function getItemLangList($idContent, $type)
    {
        $res = [];
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('distinct cl.id_lang')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent);
        $list = $q->fetchAll();
        if (!empty($list))
        {
            foreach ($list as $r)
            {
                $res[] = $r['id_lang'];
            }
        }
        return $res;
    }
    
    public function contentForLanguage($idContent, $type) {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->innerJoin($this->db->tableFix('#__lang l ON l.id = cl.id_lang'))
            ->select(null)->select('c.id')->select('cl.id_lang')->select('l.default_site')->select('l.language_code')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent);
        $r = $q->fetchAll();
        return $r;
    }

}
