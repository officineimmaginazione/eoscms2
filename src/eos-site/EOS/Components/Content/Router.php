<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content;

use EOS\System\Routing\Manager;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;

class Router extends \EOS\System\Routing\Router
{

    protected function process()
    {
        $useDef = true;
        if ($this->currentParams['dbRouter'])
        {
            if (count($this->params) == 2)
            {
                if ($this->params[0] == 'article')
                {
                    $db = $this->container->get('database');
                    $q = $db->prepare('select id, parent from #__content where id = :id');
                    $q->bindValue(':id', (int) $this->params[1]);
                    $q->execute();
                    $r = $q->fetch();
                    if (!empty($r))
                    {
                        $rm = new Manager($this->container);
                        $parenPath = 'content/category/' . $r['parent'];
                        $p = $rm->getPath($parenPath, $this->lang);
                        if (!is_null($p))
                        {
                            $q = $db->prepare('select id_content, title from #__content_lang where id_content = :id_content and id_lang = :id_lang');
                            $q->bindValue(':id_content', (int) $this->params[1]);
                            $q->bindValue(':id_lang', (int) $this->container['language']->getCurrentID());
                            $q->execute();
                            $r = $q->fetch();
                            if (!empty($r))
                            {
                                $this->segments = PathHelper::explodeSlash($p);
                                $this->segments[] = StringHelper::sanitizeUrl($r['title']) . '-' . $r['id_content'];
                            }
                        }
                    }
                }
            }
        }
        if ($useDef)
        {
            parent::process();
        }
    }

    public function resolveUrl($currentPath, $containerPath, &$newPath, &$isRedirect)
    {
        $origL = PathHelper::explodeSlash($currentPath);
        $gestL = PathHelper::explodeSlash($containerPath);
        if ((count($gestL) == 3) && (!empty($origL)))
        {
            if ($gestL[1] == 'category')
            {
                $pathArticle = end($origL);
                $artL = explode('-', $pathArticle);
                $idArt = end($artL);
                if (!empty($idArt))
                {
                    $idArt = (int) $idArt;
                    $db = $this->container->get('database');
                    // Verifico se la richiesta sia un'articolo valido e l'url corrisponda
                    $q = $db->prepare('select cl.id_content, cl.title from #__content_lang cl ' .
                        'inner join #__content c on c.id = cl.id_content ' .
                        'where cl.id_content = :id_content and cl.id_lang = :id_lang ' .
                        ' and c.type = :type and c.status = 1');
                    $q->bindValue(':id_content', (int) $idArt);
                    $q->bindValue(':id_lang', (int) $this->container['language']->getCurrentID());
                    $q->bindValue(':type', Classes\ContentType::Article);
                    $q->execute();
                    $r = $q->fetch();
                    if (!empty($r))
                    {
                        $pathArt = StringHelper::sanitizeUrl($r['title']) . '-' . $r['id_content'];
                        if (end($origL) != $pathArt)
                        {
                            $isRedirect = true;
                            $newPath = $this->path->getUrlFor('content', 'article/' . $r['id_content']);
                        } else
                        {
                            $newPath = 'content/article/' . $r['id_content'];
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
