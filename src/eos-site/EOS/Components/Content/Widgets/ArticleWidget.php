<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Widgets;

use EOS\Components\Content\Classes\ContentType;
use EOS\System\Util\ArrayHelper;

class ArticleWidget extends \EOS\UI\Widget\Widget
{

    public $data;

    protected function prepare()
    {
        parent::prepare();
        $m = new \EOS\Components\Content\Models\ContentModel($this->view->controller->container);
        $itemCount = $m->db->setting->getInt('content', 'category.widget.count');
        $items = $m->getItems([1,2], $m->lang->getCurrent()->id, ContentType::Article, 1, $itemCount);
        $this->data['items'] = $items['data'];
        $this->data['items-count'] = $items['count'];
        $this->data['preview-length'] = $m->db->setting->getInt('content', 'category.widget.preview.length');
    }

    public function write()
    {
        $this->renderTemplate('widget/article');
    }

}
