<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Widgets;

use EOS\Components\Content\Classes\ContentType;
use EOS\System\Util\ArrayHelper;

class CategoryWidget extends \EOS\UI\Widget\Widget
{

    public $data;

    private function loadExternalItem($v, &$data)
    {
        if (ArrayHelper::getBool($data['options'], 'loadexternal', false))
        {
            $data['content'] = $v->renderExternal($data['content']);
            if (isset($data['options']))
            {
                $ext = ArrayHelper::getArray($data['options'], 'extra');

                for ($i = 0; $i < count($ext); $i++)
                {
                    if (isset($ext[$i]['html']))
                    {
                        for ($n = 0; $n < count($ext[$i]['html']); $n++)
                        {
                            $ext[$i]['html'][$n] = $v->renderExternal($ext[$i]['html'][$n]);
                        }
                    }
                }
                $data['options']['extra'] = $ext;
            }
        }
    }

    private function loadExternal($v, &$data)
    {
        $this->loadExternalItem($v, $data);

        if (isset($data['items']))
        {
            for ($i = 0; $i < count($data['items']); $i++)
            {
                $this->loadExternalItem($v, $data['items'][$i]);
            }
        }
    }

    protected function prepare()
    {
        parent::prepare();

        $id = (int)$this->name;
        $m = new \EOS\Components\Content\Models\ContentModel($this->view->controller->container);
        $this->data = $m->getItem($id, $m->lang->getCurrent()->id, ContentType::Category);
        if (!empty($this->data))
        {
            $itemCount = $m->db->setting->getInt('content', 'category.widget.count');
            $items = $m->getItems($id, $m->lang->getCurrent()->id, ContentType::Article, 1, $itemCount);
            $this->data['items'] = $items['data'];
            $this->data['items-count'] = $items['count'];
            $this->data['preview-length'] = $m->db->setting->getInt('content', 'category.widget.preview.length');
            $this->loadExternal($this->view, $this->data);
        }
    }

    public function write()
    {
        $this->renderTemplate('widget/category');
    }
		
}
