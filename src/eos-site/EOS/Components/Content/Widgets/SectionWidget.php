<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Widgets;

class SectionWidget extends \EOS\UI\Widget\Widget
{

    public $data;

    private function getData($name)
    {
        $db = $this->view->controller->container->get('database');
        $lang = $this->view->controller->container->get('language');
        $q = $db->newFluent()->from($db->tableFix('#__content c'))
            ->innerJoin($db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->innerJoin($db->tableFix('#__content_section cs ON cs.id_content = c.id'))
            ->select(null)->select('c.id, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.status = 1')
            ->where('cs.name = ?', $name)
            ->where('cl.id_lang = ?', (int) $lang->getCurrentID());

        $res = $q->fetch();
        if (!empty($res))
        {
            if (!empty($res['options']))
            {
                $res['options'] = json_decode($res['options'], true);
            }
        }
        return $res;
    }

    protected function prepare()
    {
        parent::prepare();
        $this->data = $this->getData($this->name);
    }

    public function write()
    {
       $this->renderTemplate('widget/section');
    }
    
}
