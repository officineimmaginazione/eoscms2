<?php
$this->mapComponent(['GET', 'POST'], 'Content', ['page/{id}' => 'ContentController:page']);
$this->mapComponent(['GET', 'POST'], 'Content', ['category/{id}' => 'ContentController:category']);
$this->mapComponent(['GET', 'POST'], 'Content', ['article/{id}' => 'ContentController:article']);