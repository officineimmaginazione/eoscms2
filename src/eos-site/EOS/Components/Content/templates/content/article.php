<?php

if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode')))
{
    $css = $this->encodeHtml(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'css'));
    echo '<article class="article-block' . (empty($css) ? '' : ' ' . $css) . '">' . "\n";
    echo '<header>' . "\n";
    if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
    {
        echo '<h1 class="article-title">' . $this->encodeHtml($this->data['title']) . '</h1>' . "\n";
    }
    if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle'])))
    {
        echo '<h2 class="article-subtitle">' . $this->encodeHtml($this->data['subtitle']) . '</h2>' . "\n";
    }
    if (($this->data['options']['showdate']) && (!empty($this->data['publish_date'])))
    {
        printf('<p class="category-item-date">%s</p>' . "\n", $this->data['publish_date']);
    }
    echo '</header>' . "\n";
    echo '<div class="article-content">';
    echo $this->data['content'];
    echo '</div>' . "\n";
    echo '</article>' . "\n";
} else
{
    include($this->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode') . '.php');
}    