<?php

if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode')))
{
    $css = $this->encodeHtml(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'css'));
    echo '<div class="category-block' . (empty($css) ? '' : ' ' . $css) . '">' . "\n";
    if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
    {
        echo '<h1 class="category-title">' . $this->encodeHtml($this->data['title']) . '</h1>' . "\n";
    }
    if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle'])))
    {
        echo '<h2 class="category-subtitle">' . $this->encodeHtml($this->data['subtitle']) . '</h2>' . "\n";
    }
    if (!empty($this->data['content']))
    {
        echo '<div class="category-content">';
        echo $this->data['content'];
        echo '</div>' . "\n";
    }
    echo '<div class="category-items">' . "\n";
    foreach ($this->data['items'] as $item)
    {
        echo '<article class="category-item">' . "\n";
        echo '<header>' . "\n";
        if (($item['options']['showtitle']) && (!empty($item['title'])))
        {
            $link = $this->path->getUrlFor('content', 'article/' . $item['id']);
            printf('<h4 class="category-item-title"><a href="%s">%s</a></h4>' . "\n", $link, $this->encodeHtml($item['title']));
        }
        if (($item['options']['showdate']) && (!empty($item['publish_date'])))
        {
            printf('<p class="category-item-date">%s</p>' . "\n", $item['publish_date']);
        }
        echo '</header>' . "\n";
        ;
        echo '<div class="category-item-content">';
        echo EOS\System\Util\StringHelper::htmlPreview($item['content'], $this->data['preview-length']);
        echo '</div>' . "\n";
        echo '</article>' . "\n";
    }
    echo '</div>' . "\n";
    echo '<div class="category-paging">' . "\n";
    if (!empty($this->data['url-prev']))
    {
        printf('<a href="%s">%s</a>' . "\n", $this->data['url-prev'], $this->transE('system.common.prev'));
    }
    if (!empty($this->data['url-next']))
    {
        printf('<a href="%s">%s</a>' . "\n", $this->data['url-next'], $this->transE('system.common.next'));
    }
    echo '</div>' . "\n";
    echo '</div>' . "\n";
} else
{
    include($this->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode') . '.php');
}    

