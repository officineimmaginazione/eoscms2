<?php

if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode')))
{
    $css = $this->encodeHtml(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'css'));
    echo '<div class="page-block' . (empty($css) ? '' : ' ' . $css) . '">' . "\n";
    if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
    {
        echo '<h1 class="page-title">' . $this->encodeHtml($this->data['title']) . '</h1>' . "\n";
    }
    if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle'])))
    {
        echo '<h2 class="page-subtitle">' . $this->encodeHtml($this->data['subtitle']) . '</h2>' . "\n";
    }
    echo '<div class="page-content">';
    echo $this->data['content'];
    echo '</div>' . "\n";
    echo '</div>' . "\n";
} else
{
    include($this->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode') . '.php');
}    

