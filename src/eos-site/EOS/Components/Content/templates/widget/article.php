<?php

if (!empty($this->data))
{
    echo '<div class="article-widget">' . "\n";
    echo '<div class="article-widget-items">' . "\n";
    foreach ($this->data['items'] as $item)
    {
        echo '<article class="article-widget-item">' . "\n";
        echo '<header>' . "\n";
        if (($item['options']['showtitle']) && (!empty($item['title'])))
        {
            $link = $this->view->path->getUrlFor('content', 'article/' . $item['id']);
            printf('<h4 class="article-item-title"><a href="%s">%s</a></h4>' . "\n", $link, $this->view->encodeHtml($item['title']));
        }
        if (($item['options']['showdate']) && (!empty($item['publish_date'])))
        {
            printf('<p class="article-widget-item-date">%s</p>' . "\n", $item['publish_date']);
        }
        echo '</header>' . "\n";
        echo '<div class="article-widget-item-content">';
        echo EOS\System\Util\StringHelper::htmlPreview($item['content'], $this->data['preview-length']);
        echo '</div>' . "\n";
        echo '</article>' . "\n";
    }
    echo '</div>' . "\n";
    echo '</div>' . "\n";
} 
