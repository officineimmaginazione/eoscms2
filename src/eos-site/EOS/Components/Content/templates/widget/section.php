<?php
if (!empty($this->data))
{
    if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode')))
    {
        $css = \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'css');
        if (!empty($css))
        {
          $css = ' '.$css;
        }
        echo '<div class="section-content'.$css.'">';
        if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
        {
            echo '<h3 class="section-title">' . $this->view->encodeHtml($this->data['title']) . '</h3>' . "\n";
        }
        if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle'])))
        {
            echo '<h4 class="section-subtitle">' . $this->view->encodeHtml($this->data['subtitle']) . '</h4>' . "\n";
        }
        if (\EOS\System\Util\ArrayHelper::getBool($this->data['options'], 'loadexternal', false))
        {
            echo $this->view->renderExternal($this->data['content'], $this->name);
        } else
        {
            echo $this->data['content'];
        }

        echo '</div>' . "\n";
    } else
    {
        include($this->view->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode'). '.php');
    }
}