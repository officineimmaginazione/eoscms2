<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order;

class AuthRouter extends \EOS\Components\Account\Classes\Routers\AuthRouter
{
    public function isAuthenticated(string $route): bool
    {
        //$res = parent::isAuthenticated($route);
        $res = false;
        if($route != 'order/order/paypalipn/') {
            $res = $this->db->setting->getBool('order', 'auth', false);
        }   
        return $res;
    }
}