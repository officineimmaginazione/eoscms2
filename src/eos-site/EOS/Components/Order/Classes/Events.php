<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Classes;

class Events
{

    // Order
    const ORDER_FORM_AJAX_CONFIG = 'order.order.form.ajax.config';
    const ORDER_DATA_CREATE = 'order.order.data.create';
    const ORDER_DATA_PARSE_ROWS = 'order.order.data.parse.rows';
    const ORDER_DATA_PREPARE = 'order.order.data.prepare';

}
