<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Classes;

use EOS\System\Util\ArrayHelper;

class OrderSubscriber extends \EOS\System\Event\Subscriber
{

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Account\Classes\Events::USER_INDEX_TEMPLATE_RENDER => 'displayOrderRender'];
    }

    public function displayOrderRender(\EOS\System\Event\EchoEvent $event)
    {
        $view = $event->getSubject();
        $w = new \EOS\Components\Order\Widgets\DisplayOrderWidget($view);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        if(isset($view->controller->user)) {
            $user = $view->controller->user->getInfo();
            $id = $user->id;
        }
        $w->listorder = $m->getOrderByAccount($id);
        $w->write();
    }

}
