<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        parent::load();
        // Aggiungo il subscriber per i componenti
        if (($this->application->componentManager->componentExists('Account')) && ($this->isActive()))
        {
            $this->getEventDispatcher()->addSubscriber(new Classes\OrderSubscriber($this->getContainer()));
        }
    }

}