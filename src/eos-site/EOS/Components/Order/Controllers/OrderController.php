<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class OrderController extends \EOS\Components\System\Classes\SiteController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mc = new \EOS\Components\Cart\Models\CartModel($this->container);
        $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->product = $m->getProduct();
        $v->title = $this->lang->transEncode('order.order.title');
        $cart = $mc->getCart();
        if($cart['id_customer'] != '') {
            $v->address = $ma->getData(ArrayHelper::getInt($cart, 'id_customer'));
        }
        $v->rows = $mc->getRows();
        $v->total = $cart['total'];
        $v->cartid = $cart['id'];
        // ??  require_once _EOS_PATH_SYSTEM_ . 'EOS' . DIRECTORY_SEPARATOR . 'System' . DIRECTORY_SEPARATOR . 'ClassLoader.php';
        $v->addMetaRaw('http-equiv_refresh', ['http-equiv' => 'refresh', 'content' => (19 * 60) . '; URL=' . $this->path->getUrlFor('order', 'order/index')]);
        // ??? $loader = new \EOS\System\ClassLoader();
        return $v->render('order/default/default', true);
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        $fv->sanitizeMultilineFields = ['notes'];
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Order\Models\OrderModel($this->container);
            if (isset($data['command']))
            {
                switch ($data['command'])
                {
                    case 'savedata' :
                        $error = '';
                        $price = 0;
                        if ($this->container->get('database')->setting->getValue('order', 'debug.save'))
                        {
                            $lastid = $m->saveData($data, $error);
                            if (!empty($lastid))
                            {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                            } else
                            {
                                $fv->setMessage($error);
                            }
                        } elseif (!$this->container->get('database')->setting->getValue('order', 'payment.status'))
                        {
                            // Da verificare!!!!
                            if (isset($data['product-options']))
                            {
                                $product = $m->getProductByID((int) $data['product-options']);
                                $data['product-options-name'] = $product['name'];
                                $data['product-options-price'] = $product['price'];
                                $price = (float) $product['price'];
                            }
                            $product = $m->getProductByID((int) $data['product']);
                            $data['product-name'] = $product['name'];
                            $data['product-price'] = $product['price'];
                            $data['amount'] = ((float) $product['price'] + $price) * (int) $data['number-product'];
                            $emaildata = $m->getLocationEmail();
                            $sender = new \EOS\System\Mail\Mail($this->container);
                            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                            $sender->to = $data['email'];
                            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
                            $sender->subject = $this->lang->transEncode('order.order.email.subject');
                            $sender->message = $this->createMail($data);
                            $lastid = $m->saveData($data, $error);
                            if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error))
                            {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                            } else
                            {
                                $fv->setMessage($error);
                            }
                        } else
                        {
                            if ($m->check($data, $error))
                            {

                                if ($data['type-payment'] == 'pagonline')
                                {
                                    setcookie('orderdata', base64_encode(json_encode($data)), time() + 3600, "/");
                                    $payment = new \EOS\System\Payments\UnicreditPagonline($this->container);
                                    $paymentData = array('cart_id' => $data['cartid'], 'amount' => $data['amount']);
                                    $payres = $payment->payment($paymentData);
                                    if ($payres['result'])
                                    {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else
                                    {
                                        $fv->setMessage($payres['error']);
                                    }
                                } elseif ($data['type-payment'] == 'bankwire')
                                {
                                    $emaildata = $m->getLocationEmail();
                                    $sender = new \EOS\System\Mail\Mail($this->container);
                                    $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                                    $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                                    $sender->to = $data['email'];
                                    $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
                                    $sender->subject = $this->lang->transEncode('order.order.email.subject');
                                    $sender->message = $this->createMail($data);
                                    $lastid = $m->saveData($data, $error);
                                    if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error))
                                    {
                                        $fv->setResult(true);
                                        $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                                    } else
                                    {
                                        $fv->setMessage($error);
                                    }
                                } elseif ($data['type-payment'] == 'paypal')
                                {
                                    $paymentData = $data;
                                    $mc = new \EOS\Components\Order\Models\CartModel($this->container);
                                    $dataoptions = [];
                                    $dataoptions['id'] = $paymentData['cartid'];
                                    $dataoptions['options'] = json_encode($paymentData);
                                    $mc->updateOptions($dataoptions);
                                    $paymentData['cart-product'] = $cartProd = $mc->getRowsByCart($paymentData['cartid']);
                                    $countprod = 0;
                                    foreach ($cartProd as $row)
                                    {
                                        $countprod += $row['quantity'];
                                        $priceprod = $row['price'];
                                    }
                                    $npg = floor($countprod / 4);
                                    if ($countprod > 1)
                                    {
                                        $paymentData['discount'] = number_format(($npg * $priceprod) + (($countprod - $npg) * 2.00), 2);
                                    } else
                                    {
                                        $paymentData['discount'] = 0;
                                    }
                                    $payment = new \EOS\System\Payments\Paypal($this->container);
                                    $payres = $payment->paymentPaypal($paymentData);
                                    if ($payres['result'])
                                    {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else
                                    {
                                        $fv->setMessage($payres['error']);
                                    }
                                } elseif ($data['type-payment'] == 'xpay')
                                {
                                    $paymentData = $data;
                                    setcookie('orderdata', base64_encode(json_encode($data)), time() + 3600, "/");
                                    $payment = new \EOS\System\Payments\XPay($this->container);
                                    //$paymentData = array('cart_id' => $data['cartid'], 'amount' => $data['amount']);
                                    $payres = $payment->payment($paymentData);
                                    if ($payres['result'])
                                    {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else
                                    {
                                        $fv->setMessage($payres['error']);
                                    }
                                }
                            } else
                            {
                                $fv->setMessage($error);
                            }
                        }

                        break;
                    default :
                        $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
                        break;
                }
            } else
            {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function verify($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mc = new \EOS\Components\Order\Models\CartModel($this->container);
        $payment = new \EOS\System\Payments\UnicreditPagonline($this->container);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $verify = $payment->verifyPayment();
        if (isset($_COOKIE['orderdata']) && $verify['result'])
        {
            $cookie = $_COOKIE['orderdata'];
            $data = json_decode(base64_decode($cookie), true);
            $cartid = ((int) $_COOKIE['cart_id']);
            $data['cartid'] = $cartid;
            $data['txn-id'] = $verify['txn-id'];
            $mc = new \EOS\Components\Order\Models\CartModel($this->container);
            $data['cart-product'] = $mc->getRowsByCart($cartid);
            $data['status'] = 6;
            $lastid = $m->saveData($data, $error);
            $data['cart-product'] = $m->getRowsByOrder($lastid);
            $sender = new \EOS\System\Mail\Mail($this->container);
            $data['type-payment'] = $this->lang->transEncode('order.order.payment.creditcard');
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $data['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $lastid;
            $sender->message = $this->createMail($data);
            if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error))
            {
                $v->titleorder = $this->lang->transEncode('order.verify.confirm.title');
                $v->description = $this->lang->transEncode('order.verify.confirm.description');
                $mc->cartToOrder($cartid);
            } else
            {
                $v->titleorder = $this->lang->transEncode('order.verify.error.title');
                $v->description = $this->lang->transEncode('order.verify.error.description');
            }
        }
        return $v->render('order/verify', true);
    }

    public function paypalipn()
    {
        $payment = new \EOS\System\Payments\Paypal($this->container);
        $payres = $payment->validate_ipn();
        if ($payres['result'] == true)
        {
            $ipndata = $payres['ipn_data'];
            $cartid = $ipndata['custom'];
            $data['txn-id'] = $ipndata['txn_id'];
            $m = new \EOS\Components\Order\Models\OrderModel($this->container);
            $mc = new \EOS\Components\Order\Models\CartModel($this->container);
            $cart = $mc->getCartByID($cartid);
            $data = json_decode($cart['options'], true);
            $data['status'] = 5;
            $data['cart-product'] = $mc->getRowsByCart($cartid);
            $lastid = $m->saveData($data, $error);
            $data['cart-product'] = $m->getRowsByOrder($lastid);
            $data['type-payment'] = $this->lang->transEncode('order.order.payment.paypal');
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $data['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $lastid;
            $sender->message = $this->createMail($data);
            if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error))
            {
                $mc->cartToOrder($cartid);
            }
        }
    }

    public function confirm($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $v->titleorder = $this->lang->transEncode('order.verify.confirm.title');
        $v->description = $this->lang->transEncode('order.verify.confirm.description');
        return $v->render('order/confirm', true);
    }

    public function error($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $v->titleorder = $this->lang->transEncode('order.verify.error.title');
        $v->description = $this->lang->transEncode('order.verify.error.description');
        return $v->render('order/error', true);
    }

    public function createMail($data)
    {
        $m = new \EOS\Components\Reservation\Models\BookingModel($this->container);
        $content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
        $content .= '<html>';
        $content .= '<head>';
        $content .= '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">';
        $content .= '<title>' . $this->lang->transEncode('order.order.confirm.email.subject') . '</title>';
        $content .= '</head>';
        $content .= '<body>';
        $content .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">';
        $content .= '<p>' . $this->lang->transEncode('order.order.confirm.email.msg') . '</p>';
        $content .= '<h5>' . $this->lang->transEncode('order.order.requireinvoice') . '</h5>';
        $content .= '<p>' . $this->lang->transEncode('order.order.name') . ': ' . $data['name'] . '</p>';
        $content .= '<p>' . $this->lang->transEncode('order.order.surname') . ': ' . $data['surname'] . '</p>';
        if (isset($data['company']) && $data['company'] != '')
        {
            $content .= '<p>' . $this->lang->transEncode('order.order.company') . ': ' . $data['company'] . '</p>';
            $content .= '<p>' . $this->lang->transEncode('order.order.vat') . ': ' . $data['vat'] . '</p>';
        }
        if (isset($data['cif']) && $data['cif'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.cif') . ': ' . $data['cif'] . '</p>';
        $content .= '<p>' . $this->lang->transEncode('order.order.email') . ': ' . $data['email'] . '</p>';
        $content .= '<p>' . $this->lang->transEncode('order.order.address') . ': ' . $data['address'] . '</p>';
        $content .= '<p>' . $this->lang->transEncode('order.order.city') . ': ' . $data['city'] . '</p>';
        if (isset($data['shipping-name']) && $data['shipping-name'] != '')
            $content .= '<h5>' . $this->lang->transEncode('order.order.datarecipient') . '</h5>';
        if (isset($data['shipping-name']) && $data['shipping-name'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.name') . ': ' . $data['shipping-name'] . '</p>';
        if (isset($data['shipping-surname']) && $data['shipping-surname'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.surname') . ': ' . $data['shipping-surname'] . '</p>';
        if (isset($data['shipping-email']) && $data['shipping-email'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.email') . ': ' . $data['shipping-email'] . '</p>';
        if (isset($data['shipping-address']) && $data['shipping-address'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.address') . ': ' . $data['shipping-address'] . '</p>';
        if (isset($data['shipping-city']) && $data['shipping-city'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.city') . ': ' . $data['shipping-city'] . '</p>';
        if (isset($data['shipping-zipcode']) && $data['shipping-zipcode'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.zipcode') . ': ' . $data['shipping-zipcode'] . '</p>';
        if (isset($data['shipping-phone']) && $data['shipping-phone'] != '')
            $content .= '<p>' . $this->lang->transEncode('order.order.phone') . ': ' . $data['shipping-phone'] . '</p>';
        $content .= '<p>' . $this->lang->transEncode('order.order.amount') . ': ' . (float) $data['amount'] . ' &euro;</p>';
        if (isset($data['type-payment']) && $data['type-payment'] != '')
        {
            $content .= '<p>' . $this->lang->transEncode('order.order.payment.type') . ': ' . $data['type-payment'] . '</p>';
        }
        if (isset($data['txn-id']) && $data['txn-id'] != '')
        {
            $content .= '<p>' . $this->lang->transEncode('order.order.txn-id') . ': ' . $data['txn-id'] . '</p>';
        }

        $content .= '<table>';
        $content .= '<tr>';
        $content .= '<th>Prodotto</th><th>Q.tà</th><th>Prezzo</th>';
        $content .= '</tr>';
        foreach ($data['cart-product'] as $row)
        {
            $options = json_decode($row['options'], true);

            $content .= '<tr style="text-align:center;">';
            $content .= '<td width="200px">' . $row['name'] . '</td><td width="80px">' . $row['quantity'] . '</td><td width="100px">' . $row['price'] . '</td>';
            $content .= '</tr>';
        }
        $content .= '<tr>';
        $content .= '<td colspan="4" width="480px" style="text-align:right;padding-top:30px;">Totale</td><td width="100px" style="text-align:center; padding-top:30px;"><strong>' . (float) $data['amount'] . ' &euro;</strong></td>';
        $content .= '</tr>';
        $content .= '</table>';
        $privacy = (isset($data['consent1'])) ? $this->lang->transEncode('order.order.yes') : $this->lang->transEncode('order.order.no');
        $content .= '<p>' . $this->lang->transEncode('order.order.email.privacy.consent1') . ' ' . $privacy . '</p>';
        $content .= '</div>';
        $content .= '</body>';
        $content .= '</html>';
        return $content;
    }

}
