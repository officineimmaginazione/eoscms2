<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;
use EOS\Components\Cart\Classes\CartHelper;

class OrderModel extends \EOS\System\Models\Model
{

    public function getProduct()
    {
        $sql = 'select p.id, p.price, pl.name, pl.description, ppc.id_category ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
						' inner join #__product_product_category ppc on p.id = ppc.id_product' .
            ' where pl.id_lang = :id_lang  order by p.id ASC';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getProductByID($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $tbl = $this->db->tableFix('#__order');
            $values = [
                'name' => $data['name'],
                'surname' => $data['surname'],
                'company' => ArrayHelper::getStr($data, 'company'),
                'cif' => ArrayHelper::getStr($data, 'cif'),
                'vat' => ArrayHelper::getStr($data, 'vat'),
                'phone' => $data['phone'],
                'email' => $data['email'],
                'address' => $data['address'],
                'zip_code' => $data['zipcode'],
                'city' => $data['city'],
                'state' => $data['province'],
                'country' => $data['country'],
                'amount' => $data['amount']
            ];
            if (isset($data['status']) && $data['status'] != '')
            {
                $values['status'] = $data['status'];
            } else
            {
                $values['status'] = 3;
            }
            $values['privacy'] = (isset($data['consent1'])) ? 1 : 0;
            $options = ArrayHelper::getArray($data, 'custom-fields');         /* Copia nelle options i campi personalizzati degli eventi */
            $options['shipping-name'] = ArrayHelper::getStr($data, 'shipping-name');
            $options['shipping-surname'] = ArrayHelper::getStr($data, 'shipping-surname');
            $options['shipping-phone'] = ArrayHelper::getStr($data, 'shipping-phone');
            $options['shipping-email'] = ArrayHelper::getStr($data, 'shipping-email');
            $options['shipping-address'] = ArrayHelper::getStr($data, 'shipping-address');
            $options['shipping-zip_code'] = ArrayHelper::getStr($data, 'shipping-zipcode');
            $options['shipping-city'] = ArrayHelper::getStr($data, 'shipping-city');
            $options['shipping-state'] = ArrayHelper::getStr($data, 'shipping-province');
            $options['shipping-country'] = ArrayHelper::getStr($data, 'shipping-country');
            $options['txn-id'] = ArrayHelper::getStr($data, 'txn-id');
            $options['cartid'] = ArrayHelper::getStr($data, 'cartid');
            $values['options'] = json_encode($options);
            //$values['notes'] = $this->lang->transEncode('order.order.datarecipient').' '.$data['namedest'].' '.$data['surnamedest'].' '.$data['emaildest'].' '.$this->lang->transEncode('order.order.datarecipient').' '.$data['notes'].' '.$data['order-notes-add'];
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tbl, $values);
            $query->execute();
            $lastid = $this->db->lastInsertId();
            $lastidorder = $this->db->lastInsertId();
            if (isset($data['number-product']))
            {
                for ($i = 1; $i <= $data['number-product']; $i++)
                {
                    $tbl = $this->db->tableFix('#__order_row');
                    $values = [
                        'id_order' => $lastid,
                        'id_object' => $data['product'],
                        'product_name' => $data['product-name'],
                        'price' => $data['product-price']
                    ];
                    $values['ins_id'] = 0;
                    $values['ins_date'] = new \FluentLiteral('NOW()');
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                    $query->execute();
                }
                if (isset($data['product-options']))
                {
                    for ($i = 1; $i <= $data['number-product']; $i++)
                    {
                        $tbl = $this->db->tableFix('#__order_row');
                        $values = [
                            'id_order' => $lastid,
                            'id_object' => $data['product-options'],
                            'product_name' => $data['product-options-name'],
                            'price' => $data['product-options-price']
                        ];
                        $values['ins_id'] = 0;
                        $values['ins_date'] = new \FluentLiteral('NOW()');
                        $query = $this->db->newFluent()->insertInto($tbl, $values);
                        $query->execute();
                    }
                }
            } elseif ($data['cartid'])
            {
                $i = 0;
                $cartModel = new CartModel($this->container);
                $data['cart-product'] = $cartModel->getRowsByCart($data['cartid']);
                foreach ($data['cart-product'] as $row)
                {
                    $tbl = $this->db->tableFix('#__order_row');
                    $typeObject = CartHelper::getCustomRowType($row);
                    if (empty($typeObject))
                    {
                        $options = ArrayHelper::fromJSON($row['options']);
                    } else
                    {
                        // La struttura delle "options" del cart Custom è "type=>, data=>"
                        $options = CartHelper::getCustomRowData($row);
                    }

                    $values = [
                        'id_order' => $lastid,
                        'id_object' => $row['id_product'],
                        'type_object' => $typeObject,
                        'product_name' => $row['name'],
                        'price' => $row['price'],
                        'quantity' => $row['quantity'],
                        'options' => ArrayHelper::toJSON($options)
                    ];
                    $values['ins_id'] = 0;
                    $values['ins_date'] = new \FluentLiteral('NOW()');
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                    $query->execute();
                    $i++;
                }
            } else
            {
                
            }
            $this->db->commit();

            $ev = new \EOS\System\Event\DataEvent($this, ['id' => $lastidorder]);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_CREATE, $ev);
            return $lastidorder;
        }
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        if (empty($data['name']))
        {
            $error = $this->lang->trans('order.order.error.name');
            $res = false;
        } else if (empty($data['surname']))
        {
            $error = $this->lang->trans('order.order.error.surname');
            $res = false;
        } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
        {
            $error = $this->lang->trans('order.order.error.email');
            $res = false;
        } else if (empty($data['address']))
        {
            $error = $this->lang->trans('order.order.error.address');
            $res = false;
        } else if (empty($data['city']))
        {
            $error = $this->lang->trans('order.order.error.city');
            $res = false;
        } else if (empty($data['province']))
        {
            $error = $this->lang->trans('order.order.error.province');
            $res = false;
        } else if (empty($data['zipcode']))
        {
            $error = $this->lang->trans('order.order.error.zipcode');
            $res = false;
        } else if (empty($data['country']))
        {
            $error = $this->lang->trans('order.order.error.country');
            $res = false;
        } else if (empty($data['phone']))
        {
            $error = $this->lang->trans('order.order.error.phone');
            $res = false;
        } else if (!isset($data['consent1']))
        {
            $error = $this->lang->trans('order.order.error.privacy');
            $res = false;
        }


        if ($res)
        {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }

        return $res;
    }

    public function getLocationEmail()
    {
        $q = $this->db->prepare('select email, name from #__reservation_location');
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            return $r;
        } else
        {
            return 0;
        }
    }

    public function check(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        }
        return true;
    }

    public function getRowsByOrder($id)
    {
        $sql = 'select orow.id, orow.id_object, orow.type_object, orow.product_name, orow.quantity, orow.options, orow.price ' .
            ' from #__order_row orow ' .
            ' left outer join #__product p on orow.id_object = p.id ' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang' .
            ' where orow.id_order = :id_order ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_order', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();

        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getOrder($id)
    {
        $sql = 'select o.*' .
            ' from #__order o' .
            ' where o.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }
    
    public function getOrderByAccount($id)
    {
        if($id != 8363){
            $sql = 'select o.*, a.alias ' .
                ' from #__order o '.
                ' left outer join #__address a on o.id_customer = a.id '.
                ' where o.id_customer in (select a.id from #__address a where a.options like \'%agent":"agent%\' or a.id = :id) '.
                ' order by o.id DESC limit 25 ';
        } else {
            $sql = 'select o.*, a.alias ' .
                ' from #__order o '.
                ' left outer join #__address a on o.id_customer = a.id '.
                ' order by o.id DESC limit 25 ';
        }
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

}
