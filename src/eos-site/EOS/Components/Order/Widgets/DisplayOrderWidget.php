<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Widgets;

use EOS\System\Util\ArrayHelper;

class DisplayOrderWidget extends \EOS\UI\Widget\Widget
{

    public $data = [];
    public $dataField = 'id_user';
    public $disabled = false; 
    public $multiple = false;

    public function write()
    {
        $this->renderTemplate('widget/displayorder');
    }

    public function getCurrentInfo()
    {
        $m = new \EOS\Components\Order\Models\OrderModel($this->view->controller->container);
        //$q = $m->getListQuery();
        //$q->where('u.id', ArrayHelper::getInt($this->data, $this->dataField));
        //return $m->getTextList($q);
    }

}
