<?php

return
    [
        'order.order.title' => 'Ordine',
        'order.order.subtitle' => '',
        'order.order.title.address' => 'Indirizzo *',
        'order.order.companyprivate.company' => 'Azienda',
        'order.order.companyprivate.private' => 'Privato',
        'order.order.name' => 'Nome *',
        'order.order.surname' => 'Cognome *',
        'order.order.cif' => 'Codice fiscale *',
        'order.order.email' => 'E-mail *',
        'order.order.address' => 'Indirizzo *',
        'order.order.city' => 'Città *',
        'order.order.province' => 'Provincia *',
        'order.order.zipcode' => 'Codice postale *',
        'order.order.country' => 'Paese *',
        'order.order.phone' => 'Telefono *',
        'order.order.shipping.address' => 'Indirizzo di spedizione',
        'order.order.company' => 'Azienda',
        'order.order.vatnumber' => 'P. Iva.',
        'order.order.fiscalcode' => 'Codice Fiscale',
        'order.order.privacy.accept' => 'Accetta i nostri',
        'order.order.privacy.terms' => 'Termini e condizioni',
        'order.order.payment.methods' => 'Metodo di pagamento',
        'order.order.shipping.button' => 'Spedizione',
        'order.order.info' => 'Informazioni',
        'order.order.price' => 'Prezzo',
        'order.order.requireinvoice' => 'Dati di fatturazione',
        'order.order.databuyer' => 'Dati del richiedente',
        'order.order.datarecipient' => 'Dati del destinatario',
        'order.order.notes' => 'Note',
        'order.order.privacy.title' => 'Informativa Privacy',
        'order.order.privacy.consent1' => 'Consenso dell\'interessato al trattamento di propri dati personali comuni:',
        'order.order.privacy.consent2' => 'Consenso dell\'interessato per l\'invio futuro di comunicazioni e materiale ai fini promozionali',
        'order.order.email.privacy.consent1' => 'Consenso al trattamento di propri dati personali comuni:',
        'order.order.email.privacy.consent2' => 'Consenso invio futuro di comunicazioni e materiale ai fini promozionali',
        'order.order.authorise' => 'Autorizza',
        'order.order.send' => 'Invia',
        'order.order.yes' => 'Sì',
        'order.order.no' => 'No',
        'order.order.amount' => 'Totale',
        'order.order.consent.yes' => 'Do il consenso',
        'order.order.consent.no' => 'Nego il consenso',
        'order.order.error.invalidcommand' => 'Comando non valido!',
        'order.order.error.name' => 'Nome non valido!',
        'order.order.error.surname' => 'Cognome non valido!',
        'order.order.error.email' => 'EMail non valida!',
        'order.order.error.address' => 'Indirizzo non valido!',
        'order.order.error.city' => 'Città non valido!',
        'order.order.error.province' => 'Provincia non valido!',
        'order.order.error.zipcode' => 'CAP non valido!',
        'order.order.error.country' => 'Nazione non valido!',
        'order.order.error.phone' => 'Telefono non valido!',
        'order.order.error.privacy' => 'Confermare la privacy!',
        'order.order.confirm' => 'Richiesta<br>regalo',
        'order.order.email.subject' => 'Nuovo Ordine n° ',
        'order.order.confirm.email.msg' => 'Ti ringraziamo per il tuo ordine. Di seguito le informazioni:',
        'order.order.title' => 'Ordine',
        'order.verify.title' => 'Riepilogo ordine',
        'order.verify.confirm.title' => 'Il tuo ordine è stato ricevuto',
        'order.verify.confirm.description' => 'Il tuo ordine ordine è stato correttamente ricevuto. A breve partirà il tuo ordine',
        'order.verify.error.title' => 'Ordine non andato a buon fine',
        'order.verify.error.description' => 'Il tuo ordine non è stato creato per problemi. Contatta l\'assistenza se sei sicuro di aver pagato oppure clicca su Torna al carrello per provare a ricreare l\'ordine.',
        'order.order.check' => 'Di seguito il riepilogo dei prodotti acquistati e file di stampa associati. Dopo 1 h. dalla ricezione dell\'ordine non potranno più essere effettuate variazioni. Dai link nelle colonne "File originali" e "File di stampa" potete controllare ciò che avete caricato.',
        'order.order.payment.type' => 'Tipologia pagamento',
        'order.order.payment.paypal' => 'Paypal',
        'order.order.payment.creditcard' => 'Carta di credito',
        'order.order.txn-id' => 'ID Transazione',
        'order.order.checkout' => 'Conferma ordine'
];
