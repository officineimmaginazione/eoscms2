<?php
$this->mapComponent(['GET'], 'Order', ['order/index' => 'OrderController:index']);
$this->mapComponent(['POST'], 'Order', ['order/ajaxcommand' => 'OrderController:ajaxCommand']);
$this->mapComponent(['POST'], 'Order', ['order/verify' => 'OrderController:verify']);
$this->mapComponent(['GET'], 'Order', ['order/confirm' => 'OrderController:confirm']);
$this->mapComponent(['GET'], 'Order', ['order/error' => 'OrderController:error']);
$this->mapComponent(['POST'], 'Order', ['order/paypalipn' => 'OrderController:paypalipn']);