<form id="order" method="POST" class="form-container order-form form-horizontal">
    <div class="container">
        <div class="order-title">
            <div class="container">
                <h3 class="section-title text-center">   <?php $this->transPE('order.order.title'); ?></h3>
                <h3 class="section-subtitle text-center">   <?php $this->transPE('order.order.subtitle'); ?></h3>
            </div>
        </div>
        <div class="order-address text-center">
            <div class="container">
                <h4 class="white"><?php $this->transPE('order.order.title.address'); ?></h4>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <div class="radio-inline">
                    <label>
                        <input type="radio" value="true" id="company-yes" name="company-private">
                        <?php $this->transPE('order.order.companyprivate.company'); ?>
                    </label>
                </div>
                <div class="radio-inline">
                    <label>
                        <input type="radio" value="false" id="private-yes" name="company-private" checked>
                        <?php $this->transPE('order.order.companyprivate.private'); ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group company-active hidden">
            <label class="control-label col-sm-3" for="company"><?php $this->transPE('order.order.company'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="company"  name="company">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="name"><?php $this->transPE('order.order.name'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="name"  name="name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="surname"><?php $this->transPE('order.order.surname'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="surname" name="surname">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="cif"><?php $this->transPE('order.order.cif'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="cif"  name="cif">
            </div>
        </div>
        <div class="form-group company-active hidden">
            <label class="control-label col-sm-3" for="vat"><?php $this->transPE('order.order.vat'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="vat"  name="vat">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email"><?php $this->transPE('order.order.email'); ?>:</label>
            <div class="col-sm-9">
                <input type="email" class="form-control" id="email" name="email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="address"><?php $this->transPE('order.order.address'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="address" name="address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="city"><?php $this->transPE('order.order.city'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="city" name="city">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="province"><?php $this->transPE('order.order.province'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="province" name="province">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="zipcode"><?php $this->transPE('order.order.zipcode'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="zipcode" name="zipcode">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="country"><?php $this->transPE('order.order.country'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="country" name="country">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="phone"><?php $this->transPE('order.order.phone'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="phone" name="phone">
            </div>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-azure" id="btn-shipping"><?php $this->transPE('order.order.shipping.button'); ?></button>
        </div>
    </div>

    <div class="black-bg text-center xs-mt-60">
        <div class="container">
            <h4 class="white"><?php $this->transPE('order.order.shipping.address'); ?></h4>
        </div>
    </div>
    <div class="container xs-mt-60">
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-name"><?php $this->transPE('order.order.name'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-name" name="shipping-name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-surname"><?php $this->transPE('order.order.surname'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-surname" name="shipping-surname">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-email"><?php $this->transPE('order.order.email'); ?>:</label>
            <div class="col-sm-9">
                <input type="email" class="form-control" id="shipping-email" name="shipping-email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-address"><?php $this->transPE('order.order.address'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-address" name="shipping-address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-city"><?php $this->transPE('order.order.city'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-city" name="shipping-city">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-province"><?php $this->transPE('order.order.province'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-province" name="shipping-province">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-zipcode"><?php $this->transPE('order.order.zipcode'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-zipcode" name="shipping-zipcode">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-country"><?php $this->transPE('order.order.country'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-country" name="shipping-country">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="shipping-phone"><?php $this->transPE('order.order.phone'); ?>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="shipping-phone" name="shipping-phone">
            </div>
        </div>
        <div class="form-group text-center">
            <div class="checkbox">
                <label class="control-label"><input type="checkbox" name="consent1" value=""><?php $this->transPE('order.order.privacy.accept'); ?> <a href="/it/termini-e-condizioni/" target="_blank" ><?php $this->transPE('order.order.privacy.terms'); ?></a></label>
            </div>
        </div>
        <?php $this->writeTokenHtml(); ?>
        <input type="hidden" value="savedata" name="command">
        <input type="hidden" value="<?php echo $this->total; ?>" name="amount">
        <input type="hidden" value="<?php echo $this->cartid; ?>" name="cartid">
        <input type="hidden" value="" name="type-payment">
    </div>
    <div class="black-bg text-center xs-mt-60">
        <div class="container">
            <h4 class="white"><?php $this->transPE('order.order.payment.methods'); ?></h4>
        </div>
    </div>
    <div class="container xs-pt-80 xs-pb-80">
        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <button id="order-send-pagonline" class="btn btn-payment"><?php $this->transPE('order.order.payment.creditcard'); ?> <br /><br /><i class="icon-credit-card"></i></button>
        </div>
        <div class="col-xs-12 col-sm-4 col-sm-offset-2">
            <button id="order-send-paypal" class="btn btn-payment"><?php $this->transPE('order.order.payment.paypal'); ?> <br /><br /><i class="icon-paypal"></i></button>
        </div>
    </div>
</form>
<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
    function runAjax(data, resultEvent)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Order', 'order/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    resultEvent(json);
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $("input[name='company-private']").change(function ()
    {
        if ($("input[name=company-private]:checked").val() == "true")
        {
            $(".company-active").removeClass("hidden");
        } else
        {
            $(".company-active").addClass("hidden");
        }
    });

    $("#order-send-pagonline").click(function (event)
    {
        $("input[name='type-payment']").val('pagonline');
        event.preventDefault();
        saveData();
    });

    $("#order-send-paypal").click(function (event)
    {
        $("input[name='type-payment']").val('paypal');
        event.preventDefault();
        saveData();
    });

    function saveData()
    {
        var data = $('#order').serializeFormJSON();
        runAjax(data, function (json)
        {
            location.href = json.redirect;
        });
    }

    $("#btn-shipping").click(function (event)
    {
        event.preventDefault();
        copytoshipping();
    });

    function copytoshipping()
    {
        $('#shipping-name').val($('#name').val());
        $('#shipping-surname').val($('#surname').val());
        $('#shipping-email').val($('#email').val());
        $('#shipping-address').val($('#address').val());
        $('#shipping-city').val($('#city').val());
        $('#shipping-province').val($('#province').val());
        $('#shipping-zipcode').val($('#zipcode').val());
        $('#shipping-country').val($('#country').val());
        $('#shipping-phone').val($('#phone').val());
    }

</script>
<?php
$this->endCaptureScript();
