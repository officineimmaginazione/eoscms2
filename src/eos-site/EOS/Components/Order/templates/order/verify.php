<section>
    <div class="page-mo-block green-page">
        <div class="title-block">
            <div class="container">
                <h3 class="title"><?php echo $this->titleorder; ?></h3>
            </div>
        </div>
    </div>
    <div class="page-content">
        <div class="container">
            <p class="xs-mt-60 xs-mb-30 text-center"><?php echo $this->description; ?></p>
        </div>
    </div>
</section>
