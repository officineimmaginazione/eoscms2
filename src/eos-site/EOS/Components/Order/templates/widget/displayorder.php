

<div class="col-12 col-sm-6">
    <h3>Ordini</h3>
    <table class="table">
        <thead>
            <tr>
                <th>N° Ordine</th>
                <th>Azienda</th>
                <th>Totale</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->listorder as $row) : ?>
                <tr>
                    <td><?php echo $row["id"]; ?></td>
                    <td><?php echo $row['company']; ?></td>
                    <td><?php echo number_format($row['total_paid'], 2); ?> &euro;</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
