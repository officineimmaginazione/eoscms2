<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product;

class AuthRouter extends \EOS\Components\Account\Classes\Routers\AuthRouter
{
    public function isAuthenticated(string $route): bool
    {
        //$res = parent::isAuthenticated($route);
        $res = $this->db->setting->getBool('product', 'auth', false);
        if($this->db->setting->getBool('product', 'public', false)) {
            $user = $this->getUser();
            if($user->isLogged()) {
                $res = true;
            } else {
                $res = false;
            }
        }
        return $res;
    }
}