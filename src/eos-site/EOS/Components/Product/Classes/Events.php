<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Classes;

class Events
{

    // Catalog
    const CATALOG_FORM_AJAX_CONFIG = 'product.catalog.form.ajax.config';  
    const CATALOG_DATA_PARSE_ROWS = 'product.catalog.data.parse.rows';
    const CATALOG_DATA_PREPARE = 'product.catalog.data.prepare';
    // Stock
    const STOCK_DATA_GET = 'product.stock.data.get';
}
