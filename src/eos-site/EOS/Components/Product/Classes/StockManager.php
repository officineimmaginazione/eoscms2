<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Classes;

use EOS\System\Util\ArrayHelper;
use EOS\System\Util\DateTimeHelper;

class StockManager extends \EOS\System\Models\Model
{

    public $lockCartMinutes;
    public $session;

    protected function getStockQtyInCarts(int $idProduct, int $idProductChild, int $minutes, int $idCurrentCart): float
    {
        $res = 0;
        $upDate = $this->db->util->dateTimeToDBDateTime(DateTimeHelper::incMinute(DateTimeHelper::now(), -1 * $minutes));
        $sql = 'select sum(cr.quantity) as qty' .
            ' from #__cart c ' .
            ' inner join #__cart_row cr on c.id = cr.id_cart' .
            ' where c.status = 1 and cr.id_product = :id_product and cr.id_product_child = :id_product_child' .
            ' and c.up_date >= :up_date ' .
            ' and c.id <> :id_cart';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_product', $idProduct);
        $q->bindValue(':id_product_child', $idProductChild);
        $q->bindValue(':up_date', $upDate);
        $q->bindValue(':id_cart', $idCurrentCart);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            $res = (double) $r['qty'];
        }

        return $res;
    }

    protected function calcStockArrayAvailable(array &$stock)
    {
        $stock['quantity_available'] = $this->roundDecimal(
            $stock['quantity_physical'] - $stock['quantity_engaged'] - $stock['quantity_reserved'] +
            $stock['quantity_ordered1'] + $stock['quantity_ordered2']);
    }

    protected function decStockArrayQty(array &$stock, float $qty)
    {
        $qryLock = $stock['quantity_engaged'] + $stock['quantity_reserved'];
        // Devo scalare temporaneamente la quantità riservata
        $stock['quantity_physical'] = $stock['quantity_physical'] - $qryLock;
        $decList = ['quantity_physical', 'quantity_ordered1', 'quantity_ordered2'];
        // Scarico in modo proporzionale
        foreach ($decList as $f)
        {
            if ($stock[$f] > 0)
            {
                if ($stock[$f] >= $qty)
                {
                    $stock[$f] = $this->roundDecimal($stock[$f] - $qty);
                    $qty = 0;
                } else
                {
                    $qty = $this->roundDecimal($qty - $stock[$f]);
                    $stock[$f] = 0;
                }
            }
        }
        // La quantità rimasta la storno da quella fisica e potrebbe diventare negativo!
        $stock['quantity_physical'] = $this->roundDecimal($stock['quantity_physical'] - $qty + $qryLock);
        $this->calcStockArrayAvailable($stock);
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->lockCartMinutes = $this->db->setting->getInt('product', 'stock.carts.lock.minutes', 0); // Minuti
        $this->session = $this->container->get('session');
    }

    public function getDBStock(int $idProduct, int $idProductChild): array
    {
        $q = $this->db->prepare('select * from #__product_stock ' .
            ' where id_product = :id_product and id_product_child = :id_product_child');
        $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
        $q->bindValue(':id_product_child', $idProductChild, \EOS\System\Database\Database::PARAM_INT);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            $r = [];
        }
        $res['id'] = ArrayHelper::getInt($r, 'id');
        $res['id_product'] = ArrayHelper::getInt($r, 'id_product');
        // quantity_available = quantity_physical - quantity_engaged - quantity_reserved + quantity_ordered1 + quantity_ordered2
        $res['id_product_child'] = ArrayHelper::getInt($r, 'id_product_child');
        $res['quantity_available'] = ArrayHelper::getFloat($r, 'quantity_available');
        $res['quantity_physical'] = ArrayHelper::getFloat($r, 'quantity_physical');
        $res['quantity_engaged'] = ArrayHelper::getFloat($r, 'quantity_engaged');
        $res['quantity_reserved'] = ArrayHelper::getFloat($r, 'quantity_reserved');
        $res['quantity_ordered1'] = ArrayHelper::getFloat($r, 'quantity_ordered1');
        $res['quantity_ordered2'] = ArrayHelper::getFloat($r, 'quantity_ordered2');
        $dt1 = ArrayHelper::getStr($r, 'date_ordered1');
        $res['date_ordered1'] = $dt1 === '' ? null : $this->db->util->dbDateToDateTime($dt1);
        $dt2 = ArrayHelper::getStr($r, 'date_ordered2');
        $res['date_ordered2'] = $dt2 === '' ? null : $this->db->util->dbDateToDateTime($dt2);
        return $res;
    }

    public function getStock(int $idProduct, int $idProductChild, bool $lockCartsQty): array
    {
        $r = $this->getDBStock($idProduct, $idProductChild);
        if (($lockCartsQty) && ($this->lockCartMinutes > 0) && ($this->session->has('cart.id')))
        {
            // Non uso l'oggetto per velocizzare
            $cartID = (int) $this->session->get('cart.id');
            $this->decStockArrayQty($r, $this->getStockQtyInCarts($idProduct, $idProductChild, $this->lockCartMinutes, $cartID));
        }
        $ev = new \EOS\System\Event\DataEvent($this, $r);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::STOCK_DATA_GET, $ev);
        return $ev->getData();
    }

    public function getStockAvailable(int $idProduct, int $idProductChild, bool $lockCartsQty): float
    {
        return $this->getStock($idProduct, $idProductChild, $lockCartsQty)['quantity_available'];
    }

    public function roundDecimal(float $value): float
    {
        // PHP sottraendo i float potrebbe visualizzae dei decimali molto alti
        return round($value, 4);
    }

    public function decStock(int $idProduct, int $idProductChild, float $qty)
    {
        $stock = $this->getDBStock($idProduct, $idProductChild);
        $this->decStockArrayQty($stock, $qty);
        $q = $this->db->prepare('update #__product_stock 
             set quantity_available = :quantity_available, 
             quantity_physical = :quantity_physical,
             quantity_engaged = :quantity_engaged,
             quantity_reserved = :quantity_reserved,
             quantity_ordered1 = :quantity_ordered1,
             quantity_ordered2 = :quantity_ordered2
             where id_product = :id_product and id_product_child = :id_product_child');
        $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
        $q->bindValue(':id_product_child', $idProductChild, \EOS\System\Database\Database::PARAM_INT);
        $q->bindValue(':quantity_available', $stock['quantity_available']);
        $q->bindValue(':quantity_physical', $stock['quantity_physical']);
        $q->bindValue(':quantity_engaged', $stock['quantity_engaged']);
        $q->bindValue(':quantity_reserved', $stock['quantity_reserved']);
        $q->bindValue(':quantity_ordered1', $stock['quantity_ordered1']);
        $q->bindValue(':quantity_ordered2', $stock['quantity_ordered2']);
        $q->execute();
    }

}
