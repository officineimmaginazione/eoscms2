<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

use \EOS\System\Util\ArrayHelper;
use EOS\System\Util\FormValidator;

class CatalogController extends \EOS\Components\System\Classes\SiteController
{

    protected function newModel()
    {
        return new \EOS\Components\Product\Models\CatalogModel($this->container);
    }

    public function showcase($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();       
        $v->title = $this->lang->transEncode('product.catalog.title');
        $page = (int) $request->getQueryParam('page', 0);
        $order = $request->getQueryParam('order', 'name');
        $pl = $m->getProductList($page, $order);
        $v->products = $pl['products'];
        $order = $request->getQueryParam('order', 'name');
        /* filter */
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1, 0);
        /* cart */
        $v->isCartMode = $m->isCartMode();
        $v->rowShowAddCart = $m->rowShowAddCart();
        $v->displayside = false;
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        return $v->render('showcase/default');
    }
    
    public function category($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        $v->title = $this->lang->transEncode('product.catalog.title');
        $v->subtitle = $m->getCategoryTitle(1, $args['id']);
        $page = (int) $request->getQueryParam('page', 0);
        $order = $request->getQueryParam('order', 'name');
        $filter = ["category" => $args['id']];
        $pl = $m->getProductList($page, $order, $filter);
        $v->orderby = $order;
        $v->products = $pl['products'];
        $v->productsCount = $pl['products-count'];
        $v->pageIndex = $pl['page-index'];
        $v->pageCount = $pl['page-count'];
        $v->pageSize = $pl['page-size'];
        /* filter */
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1, $args['id']);
        $v->categoryparent = $m->getCategoryParent(1, $args['id']);
        /* cart */
        $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
        $v->isCartMode = $m->isCartMode();
        $v->rowShowAddCart = $m->rowShowAddCart();
        $v->displayside = true;
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        return $v->render('category/default');
    }
    
    public function brand($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        $v->title = $this->lang->transEncode('product.catalog.title');
        $v->subtitle = $m->getManufacturerTitle(1, $args['id']);
        $page = (int) $request->getQueryParam('page', 0);
        $order = $request->getQueryParam('order', 'name');
        $filter = ["manufacturer" => $args['id']];
        $pl = $m->getProductList($page, $order, $filter);
        $v->orderby = $order;
        $v->products = $pl['products'];
        $v->productsCount = $pl['products-count'];
        $v->pageIndex = $pl['page-index'];
        $v->pageCount = $pl['page-count'];
        $v->pageSize = $pl['page-size'];
        /* filter */
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1);
        /* cart */
        $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
        $v->isCartMode = $m->isCartMode();
        $v->rowShowAddCart = $m->rowShowAddCart();
        $v->displayside = true;
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        return $v->render('brand/default');
    }
    
    protected function loadSearch($request, $response, $args, $isMap)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        // Security
        $v->filters = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
        $page = ArrayHelper::getInt($v->filters, 'page', 0);
        $order = ArrayHelper::getStr($v->filters, 'order', 'name');
        $this->session->set('searchSearch',$v->filters['search']);
        $this->session->set('searchAuthor',$v->filters['author']);
        $this->session->set('searchCedola',$v->filters['cedola']);
        $this->session->set('searchSeries',$v->filters['series']);
        $this->session->set('searchIsbn',$v->filters['isbn']);
        $pl = $m->getProductList($page, $order, $v->filters);
        $v->orderby = $order;
        $v->products = $pl['products'];
        $v->productsCount = $pl['products-count'];
        $v->pageIndex = $pl['page-index'];
        $v->pageCount = $pl['page-count'];
        $v->pageSize = $pl['page-size'];
        /* filter */
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1);
        $v->author = $m->getAttribute(1, 1);
        $v->cedola = $m->getAttribute(1, 5);
        $v->series = $m->getAttribute(1, 4);
        /* cart */
        $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
        $v->isCartMode = $m->isCartMode();
        $v->rowShowAddCart = $m->rowShowAddCart();
        $v->displayside = true;
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        return $v->render('search/default', true);
    }
   
    public function item($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1);
        $v->data = $m->getData($args['id']);
        $v->relatedproduct = $m->getRelatedProducts($v->data['id_category'], $v->data['attribute-id-attribute-1'], $v->data['id']);
        $v->displayside = true;
        return $v->render('item/default');
    }
    
    public function search($request, $response, $args)
    {
        return $this->loadSearch($request, $response, $args, false);
    }


    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $mc = new \EOS\Components\Cart\Models\CartModel($this->container);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            $command = ArrayHelper::getStr($data, 'command');
            $m = $this->newModel();
            switch ($command)
            {
                case 'addcart':
                    if ($m->saveCart($data, $error))
                    {
                        $fv->setResult(true);
                        $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                    } else
                    {
                        $fv->setMessage($error);
                    }
                    break;
                case 'deleterow' :
                    if ($mc->deleteRowById($data['id']))
                    {
                        $fv->setResult(true);
                    }
                    break;
                default :
                    $fv->setMessage($this->lang->transEncode('product.catalog.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

}
