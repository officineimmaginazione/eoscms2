<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

use EOS\System\Util\ArrayHelper;
use EOS\System\Util\DateTimeHelper;

class CatalogModel extends \EOS\System\Models\Model
{

    public
        function getCurrency() {
        return new \EOS\System\Payments\Currencies\Currency($this->container);
    }

    public
        function getData($id) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product p'))
            ->select(null)
            ->select('p.id')
            ->select('p.status')
            ->select('p.price')
            ->select('p.ean13')
            ->select('p.isbn')
            ->select('p.upc')
            ->select('p.width')
            ->select('p.height')
            ->select('p.depth')
            ->select('p.weight')
            ->select('p.id_manufacturer')
            ->select('p.expected_date')
            ->where('p.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null)
        {
            $res = [];
        }
        else
        {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['price'] = $rc['price'];
            $res['ean13'] = $rc['ean13'];
            $res['isbn'] = $rc['isbn'];
            $res['upc'] = $rc['upc'];
            $res['width'] = $rc['width'];
            $res['height'] = $rc['height'];
            $res['depth'] = $rc['depth'];
            $res['weight'] = $rc['weight'];
            if(isset(rc['expected_date']))
                $res['expected-date'] = $this->lang->dbDateToLangDate($rc['expected_date']);
            $res['status'] = $rc['status'] == 1;
            foreach ($ll as $l)
            {
                $idlang = $this->lang->getCurrentID();
                $tblContent = $this->db->tableFix('#__product_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_product', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null)
                {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                    $res['descr-short-' . $idlang] = $rcl['description_short'];
                    $res['options-' . $idlang] = $rcl['options'];
                }           
            }
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($rc['id']);
            foreach ($listimage as $r)
            {
                $img = $image->getObject($rc['id'], $r['id'], \EOS\System\Media\Image::IMAGE_LR);
                $res['url-0'] = $img->url;
                //$res['pos-' . $i] = $r['pos'];
                //$i++;
            }
            $query = $this->db->newFluent()->from($this->db->tableFix('#__product_product_category ppc'))
                ->select(null)->select('pcl.name')->select('pcl.id_category')
                ->where('ppc.id_product', $rc['id'])
                ->where('id_lang = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = ppc.id_category'));
            $rcc = $query->fetch();
            $res['category'] = $rcc['name'];
            $res['id_category'] = $rcc['id_category'];
            $tblContent = $this->db->tableFix('#__manufacturer_lang');
            $query = $this->db->newFluent()->from($tblContent)
                ->select(null)
                ->select('name')
                ->select('description')
                ->where('id_manufacturer', $rc['id_manufacturer'])
                ->where('id_lang = 1');
            $rcm = $query->fetch();
            $res['manufacturer-name'] = $rcm['name'];
            $res['manufacturer-description'] = $rcm['description'];
            $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
            $query = $this->db->newFluent()->from($tblAttribute)->select('pal.name as attribute')->select('pagl.name as attributegroup')
                ->select('pagl.id as id')
                ->select('pal.id_attribute as id_attribute')
                ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                ->where('id_product', $rc['id']);
            $rca = $query->fetchAll();
            
            foreach ($rca as $ra)
            {
                $res['attribute-name-' . $ra['id']] = $ra['attribute'];
                $res['attribute-id-attribute-' . $ra['id']] = $ra['id_attribute'];
                $res['attribute-group-' . $ra['id']] = $ra['attributegroup'];
            }
        }
        //rint_r($res); exit();
        return $res;
    }

    public function prepareFilter($filterArray)
    {
        $fields = [];
        $where = [];
        $params = [];
        $join = [];
        $order = [];

        // Accetto solo quelle approvate
        // $where[] = 'f.status = 1';

        $category = ArrayHelper::getInt($filterArray, 'category');
        if (!empty($category))
        {
            $category = $this->getCategoryChild($category);
            $join[] = 'inner join eos_product_product_category ppc on p.id = ppc.id_product';
            $where[] = 'ppc.id_category in ('.$category.') ';
        }
        
        $manufacturer = ArrayHelper::getInt($filterArray, 'manufacturer');
        if (!empty($manufacturer))
        {
            $where[] = 'p.id_manufacturer = :manufacturer ';
            $params[':manufacturer'] = $manufacturer;
        }
        
        $search = ArrayHelper::getStr($filterArray, 'search');
        if (!empty($search))
        {
            $searchTerms = explode(' ', $search);
            foreach ($searchTerms as $term) {
                $term = trim($term);
                if (!empty($term)) {
                    $where[] = "pl.name like '%$term%'";
                }
            }
        }
        
        $search = ArrayHelper::getStr($filterArray, 'isbn');
        if (!empty($search))
        {
            $searchTerms = explode(' ', $search);
            foreach ($searchTerms as $term) {
                $term = trim($term);
                if (!empty($term)) {
                    $where[] = "p.isbn like '%$term%'";
                }
            }
        }
        
        $res['fields'] = implode(', ', $fields);
        $res['where'] = implode(' and ', $where);
        $res['params'] = $params;
        $res['order'] = empty($order) ? '' : implode(";", $order);
        $res['join'] = implode("\n", $join);
        return $res;
    }

    public
        function getProductList($page, $order, $filterArray = []) {

        $filter = $this->prepareFilter($filterArray);
        
        $res['products'] = [];
        $res['products-count'] = 0;
        $res['page-index'] = 0;
        $res['page-size'] = $this->db->setting->getInt('product', 'catalog.page.size', 50);
        $res['page-count'] = 0;

        $count = 0;
        $sqlSelect = 'select count(p.id) as res';
        $sqlFrom = ' from eos_product p ';
        $sqlInner = ' inner join eos_product_lang pl on p.id = pl.id_product ';
        $sqlInner .= ' inner join eos_manufacturer m on p.id_manufacturer = m.id ';
        $sqlInner .= ' inner join eos_manufacturer_lang ml on m.id = ml.id_manufacturer ';
        $sqlInner .= $filter['join'];
        $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status ';
        if (!empty($filter['where']))
        {
            $sqlWhere .= ' and ' . $filter['where'];
        }       

        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->bindValue(':status', 1);
        foreach ($filter['params'] as $p => $v)
        {
            $q->bindValue($p, $v);
        }
        //echo ($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere); exit();
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            $count = (int) $r['res'];
        }

        if ($count > 0)
        {
            $sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, pl.options, ml.name as manufacturer ';
            //$sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, pl.options ';
            switch ($order)
            {
                case 'price-asc':
                    $sqlOrderFields = ' price ASC ';
                    break;
                case 'price-desc':
                    $sqlOrderFields = ' price DESC ';
                    break;
                case 'isbn-asc':
                    $sqlOrderFields = ' isbn ASC ';
                    break;
                case 'isbn-desc':
                    $sqlOrderFields = ' isbn DESC ';
                    break;
                case 'name-asc':
                    $sqlOrderFields = ' name ASC ';
                    break;
                case 'name-desc':
                    $sqlOrderFields = ' name DESC ';
                    break;
                case 'expecteddate-asc':
                    $sqlOrderFields = ' expected_date ASC ';
                    break;
                case 'expecteddate-desc':
                    $sqlOrderFields = ' expected_date DESC ';
                    break;
                default:
                    $sqlOrderFields = ' id ';
                    break;
            }
            if(isset($ordertype) && $ordertype != '') {
                $ordertype = $sqlOrder = ' order by ' . $sqlOrderFields . $ordertype;
            }
            else {
                $sqlOrder = ' order by ' . $sqlOrderFields;
            }
            $page = (int) $page > 0 ? (int) $page : 1;
            $sqlLimit = ' limit ' . (($page - 1) * $res['page-size']) . ',' . $res['page-size'];
            //print_r($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlOrder . $sqlLimit); exit();
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlOrder . $sqlLimit);
            $q->bindValue(':id_lang', $this->lang->getCurrentID());
            $q->bindValue(':status', 1);
            foreach ($filter['params'] as $p => $v)
            {
                $q->bindValue($p, $v);
            }
            $q->execute();
            $r = $q->fetchAll();
            if (!empty($r))
            {
                $currency = $this->getCurrency();
                $tblimg = $this->db->tableFix('#__product_image');
                foreach ($r as &$item)
                {
                    $item['price-formatted'] = $currency->format($item['price']);
                    if(isset($item['expected_date']))
                        $item['expected-date'] = $this->lang->dbDateToLangDate($item['expected_date']);
                    $image = new \EOS\System\Media\Image($this->container, 'media.product');
                    $listimage = $image->getList($item['id']);
                    foreach ($listimage as $single)
                    {
                        $img = $image->getObject($item['id'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                        $item['url-0'] = $img->url;
                    }
            
                    $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
                    $query = $this->db->newFluent()->from($tblAttribute)->select('pal.name as attribute')->select('pagl.name as attributegroup')->select('pagl.id as id')
                        ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                        ->where('id_product', $item['id']);
                    $rca = $query->fetchAll();
                    
                    foreach ($rca as $ra)
                    {
                        $item['attribute-name-' . $ra['id']] = $ra['attribute'];
                        $item['attribute-group-' . $ra['id']] = $ra['attributegroup'];
                    }
                }
                $ev = new \EOS\System\Event\DataEvent($this, $r);
                $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_DATA_PARSE_ROWS, $ev);
                $r = $ev->getData();
                $res['products'] = $r;
                $res['page-count'] = (int) ceil($count / $res['page-size']);
                $res['page-index'] = $page > $res['page-count'] ? $res['page-count'] : $page;
                $res['products-count'] = $count;
            }
        }
        return $res;
    }

    public
        function isCartMode() {
        $res = $this->db->setting->getBool('product', 'catalog.cart', false);

        if (!$this->container['componentManager']->componentExists('Order'))
        {
            $res = false;
        }
        return $res;
    }

    public
        function rowShowAddCart() {
        return $this->isCartMode() && $this->db->setting->getBool('product', 'catalog.row.show.addcart', false);
    }

    protected
        function prepareData(&$data, &$error) {
        $res = true;
        $data['product'] = ArrayHelper::getInt($data, 'product');
        $data['quantity'] = ArrayHelper::getInt($data, 'quantity', 1);

        if (empty($data['product']))
        {
            $error = $this->lang->trans('product.catalog.error.product');
            $res = false;
        }
        if ($res)
        {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            $data = $ev->getData();
        }
        return $res;
    }

    public
        function saveCart(&$data, &$error) {
        if ($this->isCartMode())
        {
            if ($this->prepareData($data, $error))
            {
                $cart = new \EOS\Components\Order\Models\CartModel($this->container);
                $options = ArrayHelper::getArray($data, 'custom-fields');
                $cart->addRow($data['product'], $data['quantity'], $options);
                return true;
            }
        }
        else
        {
            $error = $this->lang->transEncode('product.catalog.error.invalidcommand');
        }
        return false;
    }

public
        function getRelatedProducts($category, $author, $id) {

        $count = 0;
        $sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, pl.name, pl.description, pl.options, ml.name as manufacturer ';
        $sqlFrom = ' from eos_product p ';
        $sqlInner = ' inner join eos_product_lang pl on p.id = pl.id_product ';
        $sqlInner .= ' inner join eos_manufacturer m on p.id_manufacturer = m.id ';
        $sqlInner .= ' inner join eos_manufacturer_lang ml on m.id = ml.id_manufacturer ';
        $sqlInner .= ' inner join eos_product_product_attribute_combination ppac on p.id = ppac.id_product ';
        $sqlInner .= ' inner join eos_product_product_category ppc on p.id = ppc.id_product ';
        $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status ';
        $sqlWhere .= ' and p.id != '.$id. ' ';
        $sqlWhereAdd = ' and ppac.id_attribute = :author ';
        $sqlLimit = ' ORDER BY RAND() LIMIT 6 ';
        
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlWhereAdd . $sqlLimit);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->bindValue(':status', 1);
        //$q->bindValue(':id', (int)$id);
        $q->bindValue(':author', (int)$author);
        $q->execute();
        $r = $q->fetchAll();
        if (empty($r))
        {
            $sqlWhereAdd = ' and ppc.id_category = :category ';
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlWhereAdd . $sqlLimit);
            $q->bindValue(':id_lang', $this->lang->getCurrentID());
            $q->bindValue(':status', 1);
            //$q->bindValue(':id', (int)$id);
            $q->bindValue(':category', (int)$category);
            $q->execute();
            $r = $q->fetchAll();
            if (empty($r))
            {
                $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlLimit);
                $q->bindValue(':id_lang', $this->lang->getCurrentID());
                $q->bindValue(':status', 1);
                //$q->bindValue(':id', (int)$id);
                $q->execute();
                $r = $q->fetchAll();
            }
        }
        
        $i = 0;
        foreach ($r as $item)
        {
            $currency = $this->getCurrency();
            $res[$i]['id'] = $item['id'];
            $res[$i]['price'] = $item['price'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['isbn'] = $item['isbn'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['options'] = $item['options'];
            $res[$i]['manufacturer'] = $item['manufacturer'];
            
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id']);
            foreach ($listimage as $single)
            {
                $img = $image->getObject($item['id'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                $res[$i]['url-0'] = $img->url;
            }
            $i++;
        }      
            
        return $res;
    }

    public
        function getManufacturerList($id_lang) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)->select('m.id, ml.name')
            ->where('ml.id_lang = ' . (int) $id_lang)
            ->where('m.status = 1')
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'))
            ->orderBy('ml.name');
        $list = $query->fetchAll();
        return $list;
    }

    public
        function getCategoriesList($id_lang, $id_category = null) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.id, pcl.name')
            ->where('pcl.id_lang = ' . (int) $id_lang)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))
            ->orderBy('pcl.name');
        if (isset($id_category) && $id_category)
        {
            $query->where('pc.parent = ' . (int) $id_category);
        }
        else
        {
            $query->where('pc.parent = 0');
        }
        $list = $query->fetchAll();
        if (count($list) == 0)
        {
            $queryparent = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.parent')
                ->where('pc.id = ' . (int) $id_category)
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
            $parent = $queryparent->fetch();
            $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.id, pcl.name')
                ->where('pcl.id_lang = ' . (int) $id_lang)
                ->where('pc.parent = ' . (int) $parent['parent'])
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))
                ->orderBy('pcl.name');
            $list = $query->fetchAll();
        }
        return $list;
    }
    
    public function getCategoryParent ($id_lang, $id_category) {
        $query= $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.parent')
            ->where('pc.id = ' . (int) $id_category)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
        $r = $query->fetch();
        if($r['parent'] != 0) {
            $query= $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.id, pcl.name')
                ->where('pc.id = ' . (int) $r['parent'])
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
            $res = $query->fetch();
        } else {
            $res['id'] = 0;
            $res['name'] = "Tutte le categorie";
        }
        return $res;
    }
    
    public
        function getCategoryChild($id_category) {
        $list = $id_category;
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.id')
            ->where('pc.parent = ' . (int) $id_category)
            ->where('pc.status = 1');
        $childs = $query->fetchAll();
        if (isset($childs) && $childs != '')
        {
            foreach ($childs as $child)
            {
                $list .= ',' . $child['id'];
                $querychild = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                    ->select(null)->select('pc.id')
                    ->where('pc.parent = ' . (int) $child['id'])
                    ->where('pc.status = 1');
                $childsofchild = $querychild->fetchAll();
                if (isset($childsofchild) && $childsofchild != '')
                {
                    foreach ($childsofchild as $childofchild)
                    {
                        $list .= ',' . $childofchild['id'];
                    }
                }
            }
        }
        //print_r($list); exit();
        return $list;
    }

    public
        function getManufacturerTitle($id_lang, $id) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)->select('ml.name')
            ->where('ml.id_lang = ' . (int) $id_lang)
            ->where('ml.id = ' . (int) $id)
            ->where('m.status = 1')
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'));
        $title = $query->fetch();
        return $title['name'];
    }

    public
        function getCategoryTitle($id_lang, $id) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pcl.name')
            ->where('pcl.id_lang = ' . (int) $id_lang)
            ->where('pc.id = ' . (int) $id)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
        $title = $query->fetch();
        return $title['name'];
    }

        
    public
        function getAttribute($id_lang, $id_group) {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
            ->select(null)->select('pal.name')->select('pa.id')
            ->where('pal.id_lang = ' . (int) $id_lang)
            ->where('pa.id_attribute_group = ' . (int) $id_group)
            ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pal.id_attribute = pa.id'))
            ->orderBy('pal.name');
        $r = $query->fetchAll();
        return $r;
    }
    
}