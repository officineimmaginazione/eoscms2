<?php
$this->mapComponent(['GET'], 'Product', ['showcase/index' => 'CatalogController:showcase']);
$this->mapComponent(['GET'], 'Product', ['catalog/item/{id}' => 'CatalogController:item']);
$this->mapComponent(['GET'], 'Product', ['catalog/category/{id}' => 'CatalogController:category']);
$this->mapComponent(['GET'], 'Product', ['catalog/brand/{id}' => 'CatalogController:brand']);
$this->mapComponent(['POST'], 'Product', ['catalog/ajaxcommand' => 'CatalogController:ajaxCommand']);
$this->mapComponent(['GET'], 'Product', ['search/index' => 'CatalogController:search']);