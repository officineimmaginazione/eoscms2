<script>
    var catalogService = {
        runAjax: function (data, resultEvent)
        {
            data['<?php echo $this->session->getTokenName() ?>'] = '<?php echo $this->session->getTokenValue(); ?>';
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Product', 'catalog/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        resultEvent(json);
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        },
        addCart: function (product, quantity)
        {
            var data = {'product': product, 'quantity': quantity, 'command':'addcart'};
            this.runAjax(data, function (json)
            {
                location.href = json.redirect;
            });
        },
        
        addCartCustom: function (product, quantity, customfields)
        {
            var data = {'product': product, 'quantity': quantity, 'custom-fields': customfields, 'command':'addcart'};
            this.runAjax(data, function (json)
            {
                location.href = json.redirect;
            });
        }
    };

</script>
