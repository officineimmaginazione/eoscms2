<?php
if ($this->pageCount > 1)
{
    ?>    
    <ul id="product-catalog-pagination" class="pagination">
        <?php
        $url = $this->path->getUrlFor('Product', 'catalog/index') . '?page=';
        for ($i = 1; $i <= $this->pageCount; $i++)
        {
            $class = $i == $this->pageIndex ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>', $class, $url . $i, $i);
        }
        ?>
    </ul> 
<?php } 