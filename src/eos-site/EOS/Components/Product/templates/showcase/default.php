<div id="product-catalog">
    <?php
    $this->includePartial('header');
    $this->includePartial('products');
    $this->includePartial('pagination');
    $this->includePartial('footer');
    ?>
</div>
<?php
$this->startCaptureScript();
$this->includePartial('js_service');
$this->includePartial('js');
$this->endCaptureScript();

