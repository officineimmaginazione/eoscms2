<?php
foreach ($this->products as $product)
{
    $url = $this->path->getUrlFor('Product', 'catalog/item/' . $product['id']);
    ?>
    <div class="product-catalog-item">
        <div class="product-catalog-item-name"><a href="<?php $this->printHtml($url); ?>"><?php $this->printHtml($product['name']); ?></a></div>
        <div class="product-catalog-item-description"><?php $this->printHtml($product['description']); ?></div>
        <div class="product-catalog-item-price"><?php $this->printHtml($product['price-formatted']); ?></div>
        <?php if ($this->rowShowAddCart)
        {
            ?>
        <button class="btn product-catalog-item-addcart" data-product="<?php echo $product['id']; ?>"><?php $this->transPE('product.catalog.addcart'); ?></button>
    <?php } ?>
    </div>
    <?php
}