<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Classes;

class Events
{
    const BOOKING_FORM_AJAX_CONFIG = 'reservation.booking.form.ajax.config';
    const BOOKING_DATA_PREPARE = 'reservation.booking.data.prepare';
    const BOOKING_DATA_BEFORE_SAVE = 'reservation.booking.data.before.save';
    const BOOKING_DATA_AFTER_SAVE = 'reservation.booking.data.after.save';
    const BOOKING_DATA_BEFORE_CART = 'reservation.booking.data.before.cart';
    const BOOKING_DATA_AFTER_CART = 'reservation.booking.data.after.cart';

}
