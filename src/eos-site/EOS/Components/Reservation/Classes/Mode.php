<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Classes;

class Mode
{
    const TOUR = 1; // Categoria - Tipo - Ora - Quantità
    const RESTAURANT = 2; // Data - Orario - Tipologia
    const HOTEL = 3;
    const TRAVEL = 4;
}
