<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Classes;

use \EOS\System\Util\ArrayHelper;
use \EOS\Components\Cart\Classes\CartHelper;
use \EOS\System\Util\DateTimeHelper;

class OrderSubscriber extends \EOS\System\Event\Subscriber
{

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Cart\Classes\Events::CART_DATA_UPDATE_ROW => 'cartDataUpdateRow',
            \EOS\Components\Cart\Classes\Events::CART_DATA_DELETE_ROW => 'cartDataDeleteRow',
            \EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS => 'cartDataParseRows',
            \EOS\Components\Order\Classes\Events::ORDER_DATA_PREPARE => 'orderDataPrepare',
            \EOS\Components\Order\Classes\Events::ORDER_DATA_CREATE => 'orderDataCreate',
            \EOS\Components\Order\Classes\Events::ORDER_DATA_PARSE_ROWS => 'orderDataParseRows'];
    }

    public function cartDataUpdateRow($event)
    {
        $row = $event->getData();
        if (CartHelper::isCustomRow($row, 'reservation'))
        {
            $bookingModel = new \EOS\Components\Reservation\Models\BookingModel($this->container);
            $custData = CartHelper::getCustomRowData($row);
            $dateLang = $bookingModel->lang->dbDateToLangDate($custData['date']);
            $qty = $bookingModel->getQuantityAvailable($dateLang, $custData['id_type'], $custData['id_timeslot'], false);
            if ($qty < $row['quantity'])
            {
                $row['quantity'] = $qty;
            }
            $custData['quantity'] = $row['quantity'];
            CartHelper::updateCustomRowData($row, $custData);
        }
        $event->setData($row);
    }

    public function cartDataDeleteRow($event)
    {
        //
    }

    public function cartDataParseRows($event)
    {
        //$model = $event->getSubject();
        $rows = $event->getData();
        $bookingModel = null;
        foreach ($rows as &$row)
        {
            if (CartHelper::isCustomRow($row, 'reservation'))
            {
                if (is_null($bookingModel))
                {
                    $bookingModel = new \EOS\Components\Reservation\Models\BookingModel($this->container);
                }
                $custData = CartHelper::getCustomRowData($row);
                $bookingType = $bookingModel->getType(ArrayHelper::getInt($custData, 'id_type'));
                $bookingTimeSlot = $bookingModel->getTimeSlot(ArrayHelper::getInt($custData, 'id_timeslot'));
                if ((!empty($bookingType)) && (!empty($bookingTimeSlot)))
                {
                    $row['name'] = $bookingType['name'] . ' - ' . $bookingModel->lang->dbDateToLangDate($custData['date']) . ' - ' .
                        $bookingTimeSlot['name'];
                    $row['fullprice'] = $bookingModel->getTypePrice($bookingType, $bookingModel->db->util->dbDateToDateTime($custData['date']), true);
                    $row['price'] = $bookingModel->getTypePrice($bookingType, $bookingModel->db->util->dbDateToDateTime($custData['date']), false);
                }
            }
        }
        $event->setData($rows);
    }

    public function orderDataPrepare($event)
    {
        $model = $event->getSubject();
        $data = $event->getData();
        $idCart = ArrayHelper::getInt($data, 'cartid');
        if (!empty($idCart))
        {
            $bookingModel = null;
            $cart = new \EOS\Components\Cart\Models\CartModel($this->container);
            $rows = $cart->getDBRows($idCart);
            foreach ($rows as &$row)
            {
                if (CartHelper::isCustomRow($row, 'reservation'))
                {
                    if (is_null($bookingModel))
                    {
                        $bookingModel = new \EOS\Components\Reservation\Models\BookingModel($this->container);
                    }
                    $custData = CartHelper::getCustomRowData($row);
                    $dateLang = $bookingModel->lang->dbDateToLangDate($custData['date']);
                    $qty = $bookingModel->getQuantityAvailable($dateLang, $custData['id_type'], $custData['id_timeslot'], false);                   
                    if ($qty < $row['quantity'])
                    {
                        $event->addError($model->lang->trans('reservation.booking.error.checkout.quantity'));
                        break;
                    }
                }
            }
        }
        $event->setData($data);       
    }

    public function orderDataCreate($event)
    {
        $model = $event->getSubject();
        $data = $event->getData();
        $idOrder = $data['id'];
        $q = $model->db->prepare('select * from #__order where id = :id');
        $q->bindValue(':id', (int) $idOrder);
        $q->execute();
        $order = $q->fetch();
        $bookingModel = null;
        if (!empty($order))
        {
            $rows = $model->getRowsByOrder($idOrder);
            foreach ($rows as $row)
            {
                if ($row['type_object'] == 'reservation')
                {
                    if (is_null($bookingModel))
                    {
                        $bookingModel = new \EOS\Components\Reservation\Models\BookingModel($this->container);
                    }
                    $optData = ArrayHelper::fromJSON($row['options']);
                    $startDate = $bookingModel->db->util->dbDateToDateTime($optData['date']);
                    $endDate = $startDate;
                    $quantity = $row['quantity'];
                    // Salvo i dati della persona dall'ordine
                    $optData['name'] = $order['name'];
                    $optData['surname'] = $order['surname'];
                    $optData['phone'] = $order['phone'];
                    $optData['email'] = $order['email'];
                    $optData['vat'] = $order['vat'];
                    $bookingModel->saveDB($startDate, $endDate, $quantity, $optData);
                }
            }
            $cartModel = new \EOS\Components\Cart\Classes\CartBase($this->container);
            $cartModel->cartToOrder($idOrder);
        }
    }

    public function orderDataParseRows($event)
    {
//        $bookingModel = null;
//        //$model = $event->getSubject();
//        $rows = $event->getData();
//        $bookingModel = null;
//        foreach ($rows as &$row)
//        {
//            if ($row['type_object'] == 'reservation')
//            {
//                if (is_null($bookingModel))
//                {
//                    $bookingModel = new \EOS\Components\Reservation\Models\BookingModel($this->container);
//                }
//                $custData = ArrayHelper::fromJSON($row['options']);
//                $bookingType = $bookingModel->getType(ArrayHelper::getInt($custData, 'id_type'));
//                $bookingTimeSlot = $bookingModel->getTimeSlot(ArrayHelper::getInt($custData, 'id_timeslot'));
//                if ((!empty($bookingType)) && (!empty($bookingTimeSlot)))
//                {
//                    $row['name'] = $bookingType['name'] . ' - ' . $bookingModel->lang->dbDateToLangDate($custData['date']) . ' - ' .
//                        $bookingTimeSlot['name'];
//                }
//            }
//        }
    }

}
