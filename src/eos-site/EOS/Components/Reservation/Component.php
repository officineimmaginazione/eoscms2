<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        if ($this->application->componentManager->componentExists('Order'))
        {
            $this->getEventDispatcher()->addSubscriber(new Classes\OrderSubscriber($this->getContainer()));
        }
    }

}
