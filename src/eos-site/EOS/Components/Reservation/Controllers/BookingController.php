<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;
use EOS\Components\Reservation\Classes\Events;

class BookingController extends \EOS\Components\System\Classes\SiteController
{

    protected function newModel()
    {
        return new \EOS\Components\Reservation\Models\BookingModel($this->container);
    }

    protected function sendMail($data, &$error)
    {
        $m = $this->newModel();
        if ($m->isMultiLocation())
        {
            $location = $m->getLocation($m->getDefaultLocationID());
        } else
        {
            $location = $m->getLocation($data['location']);
        }
        $sender = new \EOS\System\Mail\Mail($this->container);
        $sender->from = $location['email'];
        $sender->fromname = $location['name'];
        $sender->to = $data['email'];
        $sender->bcc = $location['email'];
        $sender->subject = $this->lang->transEncode('reservation.booking.confirm.email.subject');
        $tpl = new \EOS\System\Views\Template($this->container);
        $tpl->data = $data;
        $tpl->location = $location;
        $sender->message = $tpl->render('Reservation', 'booking/mail_mode' . $location['options']['mode']);

        return $sender->sendEmail($error);
    }

    public function index($request, $response, $args)
    {
        $m = $this->newModel();
        $idLocation = ArrayHelper::getInt($args, 'id', -1);
        $location = $m->getLocation($idLocation);
        if (empty($location))
        {
            return $response->withRedirect($this->path->getUrlFor('reservation', 'booking/index/' . $m->getDefaultLocationID()), 302);
        }

        $isMultiLocation = $m->isMultiLocation();
        if (($isMultiLocation) && ($idLocation != $m->getDefaultLocationID()))
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $v = $this->newView($request, $response);
        $v->locations = $isMultiLocation ? $m->getLocationList() : [$location];
        $v->idLocation = $idLocation;
        $v->location = $location;
        $v->mode = ArrayHelper::getInt($location['options'], 'mode', 1);
        $v->useCart = ArrayHelper::getBool($location['options'], 'usecart', false);
        $v->dateSettingsJS = $m->getDateSettingsJS($v->idLocation);
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->title = $this->lang->transEncode('reservation.booking.title');
        $v->isMultiLocation = $isMultiLocation;
        return $v->render('booking/default');
    }

    public function confirm($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->title = $this->lang->transEncode('reservation.booking.title');
        return $v->render('booking/confirm');
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        $fv->sanitizeMultilineFields = ['reservation-notes'];
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Reservation\Classes\Events::BOOKING_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            $m = $this->newModel();
            if (isset($data['command']))
            {
                $idLocation = ArrayHelper::getInt($data, 'location');
                $mode = ArrayHelper::getInt($data, 'mode');
                switch ($data['command'])
                {
                    case 'category':
                        $categories = $m->getCategoryList($idLocation);
                        $fv->setOutputValue('list', $categories);
                        $fv->setResult(true);
                        break;
                    case 'type' :
                        $list = [];
                        switch ($mode)
                        {
                            case \EOS\Components\Reservation\Classes\Mode::TOUR:
                                $list = $m->getTypeList($idLocation, ArrayHelper::getInt($data, 'category'));
                                break;
//                            case \EOS\Components\Reservation\Classes\Mode::RESTAURANT:
//                                if (ArrayHelper::getStr($data, 'daterange', 'false') == 'true')
//                                {
//                                    $fv->setOutputValue('list', $m->getTypeDay($data['datestart'], $data['timeslot'], 2));
//                                } else
//                                {
//                                    $fv->setOutputValue('list', $m->getTypeDay($data['date'], $data['timeslot'], 1));
//                                }
//                                break;
                        }
                        $fv->setOutputValue('list', $list);
                        $fv->setResult(true);
                        break;
                    case 'datesettings':
                        $date = $m->getDateSettings($idLocation);
                        $res['startdate'] = $date['startDate'];
                        $res['enddate'] = $date['endDate'];
                        $res['firstDate'] = $date['firstDate'];
                        $res['datesdisabled'] = $date['datesDisabled'];
                        $fv->setOutputValue('date', $res);
                        $fv->setResult(true);
                        break;


                    case 'timeslot' :
                        $list = [];
                        switch ($mode)
                        {
                            case \EOS\Components\Reservation\Classes\Mode::TOUR:
                                $list = $m->getTimeSlotType(ArrayHelper::getStr($data, 'date'), ArrayHelper::getInt($data, 'type'));
                                break;
//                            case \EOS\Components\Reservation\Classes\Mode::RESTAURANT:
//                                $list = $m->getTimeSlotDay($data['date'], 1);
                            // break;
                        }
                        $fv->setOutputValue('list', $list);
                        $fv->setResult(true);
                        break;
//                    case 'arrival' :
//                        $ts = $m->getTimeslotByID($data['timeslot']);
//                        $start = strtotime($ts['start']);
//                        $finish = strtotime($ts['finish']);
//                        $res = [];
//                        $n = 0;
//                        for ($i = $start; $i < $finish; $i += 1800)
//                        {
//                            $res[]['interval'] = date("H:i", $i);
//                        }
//                        $res[]['interval'] = date("H:i", $finish);
//                        $fv->setOutputValue('list', $res);
//                        $fv->setResult(true);
//                        break;
                    case 'quantitylist':
                        $list = $m->getQuantityList(ArrayHelper::getStr($data, 'date'), ArrayHelper::getInt($data, 'type'), ArrayHelper::getInt($data, 'timeslot'));
                        $fv->setOutputValue('list', $list);
                        $fv->setResult(true);
                        break;
                    case 'savedata' :
                        $error = '';
                        if ($m->isUseCart($data))
                        {
                            if ($m->saveCart($data, $error))
                            {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('order', 'cart/index'));
                            } else
                            {
                                $fv->setMessage($error);
                            }
                        } else
                        {
                            // Aggiungo l'invio mail come finale del prepare
                            $this->getEventDispatcher()->addListener(Events::BOOKING_DATA_PREPARE, function ($event)
                            {
                                $data = $event->getData();
                                $error = '';
                                if (!$this->sendMail($data, $error))
                                {
                                    $event->addError($error);
                                }
                            }, -9999);

                            if ($m->saveData($data, $error))
                            {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('reservation', 'booking/confirm'));
                            } else
                            {
                                $fv->setMessage($error);
                            }
                        }
                        break;
                    default :
                        $fv->setMessage($this->lang->transEncode('reservation.booking.error.invalidcommand'));
                        break;
                }
            } else
            {
                $fv->setMessage($this->lang->transEncode('reservation.booking.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function testMail($request, $response)
    {
        if (_EOS_DEBUG_)
        {
            $tpl = new \EOS\System\Views\Template($this->container);
            $tpl->data['name'] = 'name';
            $tpl->data['surname'] = 'surname';
            $tpl->data['email'] = 'email';
            $tpl->data['phone'] = 'phone';
            $tpl->data['date'] = $this->lang->dateTimeToLangDate(\EOS\System\Util\DateTimeHelper::now());
            $tpl->data['notes'] = "Row1\nRow2\nRow3";
            $m = $this->newModel();
            $tpl->location = $m->getLocation($m->getDefaultLocationID());
            return $response->write($tpl->render('Reservation', 'booking/mail_mode1'));
        } else
        {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
    }

}
