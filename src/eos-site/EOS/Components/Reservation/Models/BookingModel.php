<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Reservation\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class BookingModel extends \EOS\System\Models\Model
{

    private function getSpecialDayList(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate, $status)
    {
        $q = $this->db->prepare('select special_day from #__reservation_special_day '
            . 'where (special_day between :start and :end) and status = :status '
            . 'group by special_day');
        $q->bindValue(':start', $this->db->util->dateTimeToDBDate($startDate));
        $q->bindValue(':end', $this->db->util->dateTimeToDBDate($endDate));
        $q->bindValue(':status', $status);
        $q->execute();
        $rs = $q->fetchAll();
        $res = [];
        foreach ($rs as $r)
        {
            $res[] = DateTimeHelper::formatISO(DateTimeHelper::dateOf($this->db->util->dbDateTimeToDateTime($r['special_day'])));
        }
        return $res;
    }

    private function getCategoryIDFromType($idType)
    {
        $q = $this->db->prepare('select id, id_category from #__reservation_type where id = :id');
        $q->bindValue(':id', (int) $idType);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            return $r['id_category'];
        } else
        {
            throw new \Exception('Invalid category!!!!!');
        }
    }

    protected function prepareData(&$data, &$error)
    {
        $res = true;
        $loc = $this->getLocation(ArrayHelper::getInt($data, 'location', 0));
        $type = ArrayHelper::getInt($data, 'type', 0);
        $timeslot = ArrayHelper::getInt($data, 'timeslot', 0);
        $mode = ArrayHelper::getInt($data, 'mode', 0);
        $date = ArrayHelper::getStr($data, 'date');

        if (empty($type))
        {
            $error = $this->lang->trans('reservation.booking.error.type');
            $res = false;
        } else if ((empty($loc)) || (ArrayHelper::getInt($loc, 'id') != $this->getLocationIDFromType($type)))
        {
            $error = $this->lang->trans('reservation.booking.error.location');
            $res = false;
        } else if (empty($timeslot))
        {
            $error = $this->lang->trans('reservation.booking.error.timeslot');
            $res = false;
        }

        if ($res)
        {
            if (ArrayHelper::getStr($data, 'daterange', 'false') == 'true')
            {
                if ($this->lang->langDateToDateTime($data['datestart']) === false)
                {
                    $error = $this->lang->trans('reservation.booking.error.date');
                    $res = false;
                } else if ($this->lang->langDateToDateTime($data['dateend']) === false)
                {
                    $error = $this->lang->trans('reservation.booking.error.date');
                    $res = false;
                } else if (DateTimeHelper::compareDate($this->lang->langDateToDateTime($data['datestart']), $this->lang->langDateToDateTime($data['dateend'])) == DateTimeHelper::greaterThanValue)
                {
                    $error = $this->lang->trans('reservation.booking.error.date');
                    $res = false;
                }
            } else if ($this->lang->langDateToDateTime($date) === false)
            {
                $error = $this->lang->trans('reservation.booking.error.date');
                $res = false;
            }
        }

        if ($res)
        {
            switch ($mode)
            {
                case \EOS\Components\Reservation\Classes\Mode::TOUR:
                    $quantity = ArrayHelper::getInt($data, 'quantity', PHP_INT_MAX);
                    if ($quantity > $this->getQuantityAvailable($date, $type, $timeslot))
                    {
                        $error = $this->lang->trans('reservation.booking.error.quantity');
                        $res = false;
                    }
                    break;
                default:
                    $error = $this->lang->trans('reservation.booking.error.invalidcommand');
                    $res = false;
                    break;
            }
        }

        if ($res)
        {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Reservation\Classes\Events::BOOKING_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }

        return $res;
    }

    protected function loadDefaultEvent()
    {
        $this->getEventDispatcher()->addListener(\EOS\Components\Reservation\Classes\Events::BOOKING_DATA_PREPARE, function (\EOS\System\Event\DataEvent $event)
        {
            $data = $event->getData();
            $model = $event->getSubject();
            if (!$model->isUseCart($data))
            {
                if (empty($data['name']))
                {
                    $event->addError($model->lang->trans('reservation.booking.error.name'));
                } else if (empty($data['surname']))
                {
                    $event->addError($model->lang->trans('reservation.booking.error.surname'));
                } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
                {
                    $event->addError($model->lang->trans('reservation.booking.error.email'));
                } else if (!filter_var($data['date']))
                {
                    $event->addError($model->lang->trans('reservation.booking.error.email'));
                } else if (empty($data['phone']))
                {
                    $event->addError($model->lang->trans('reservation.booking.error.phone'));
                } else if ($data['consent1'] == false)
                {
                    $event->addError($model->lang->trans('reservation.booking.error.privacy'));
                }
            }
        });
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->loadDefaultEvent();
    }

    public function isMultiLocation()
    {
        return $this->db->setting->getBool('reservation', 'booking.multilocation');
    }

    public function getDefaultLocationID()
    {
        $q = $this->db->prepare('select id, name, address, city from #__reservation_location '
            . ' where default_location = 1');
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            $l = $this->getLocationList();
            if (!empty($l))
            {
                $r = $l[0];
            }
        }
        return ArrayHelper::getInt($r, 'id', 0);
    }

    public function getLocationList()
    {
        $q = $this->db->prepare('select id, name from #__reservation_location order by name');
        $q->execute();
        return $q->fetchAll();
    }

    public function getLocation($idLocation)
    {
        $q = $this->db->prepare('select id, name, email, options from #__reservation_location ' .
            ' where id = :id');
        $q->bindValue(':id', (int) $idLocation);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            $r['options'] = ArrayHelper::fromJSON($r['options']);
        }
        return $r;
    }

    public function getLocationOpenDay($idLocation)
    {
        $r = $this->getLocation($idLocation);
        if (!empty($r))
        {
            $days = ArrayHelper::getArray($r['options'], 'opendays');
        }
        return $days;
    }

    public function getLocationStartDate($idLocation)
    {
        $r = $this->getLocation($idLocation);
        $nowDateTime = DateTimeHelper::now();
        $res = $nowDateTime;
        if (!empty($r))
        {
            $startafterday = ArrayHelper::getInt($r['options'], 'startafterday', 0);
            $res = DateTimeHelper::incDay($res, $startafterday);
            $startaftertime = explode(':', ArrayHelper::getStr($r['options'], 'startaftertime', '00:00'));
            if (count($startaftertime) >= 2)
            {
                $startH = (int) $startaftertime[0];
                $startM = (int) $startaftertime[1];
                if (($startH > 0) || ($startM > 0))
                {
                    $tmp = $nowDateTime->setTime($startH, $startM, 59);
                    if ($nowDateTime > $tmp)
                    {
                        $res = DateTimeHelper::incDay($res, 1);
                    }
                }
            }
        }
        return DateTimeHelper::dateOf($res);
    }

    public function getDateLimit()
    {
        $limitMonth = $this->db->setting->getInt('reservation', 'booking.limitmonth', 3);
        $startAfterDays = $this->db->setting->getInt('reservation', 'booking.startafterday', 0);
        $currDateTime = DateTimeHelper::date();
        $res['startDate'] = DateTimeHelper::incDay($currDateTime, $startAfterDays);
        $res['endDate'] = DateTimeHelper::endOfAMonth(DateTimeHelper::incMonth($currDateTime, $limitMonth));
        return $res;
    }

    public function getDateSettings($idLocation)
    {
        $dateFormat = $this->lang->getCurrent()->dateformat;
        $dateRange = $this->getDateLimit();
        $locationStartDateTime = $this->getLocationStartDate($idLocation);
        if ($dateRange['startDate'] < $locationStartDateTime)
        {
            $dateRange['startDate'] = $locationStartDateTime;
        }

        $res['startDate'] = $dateRange['startDate']->format($dateFormat);
        $res['endDate'] = $dateRange['endDate']->format($dateFormat);
        $disableList = $this->getDisabledDateList($idLocation);
        $datesDisabled = [];
        foreach ($disableList as $d)
        {
            $datesDisabled[] = $d->format($dateFormat);
        }

        $res['datesDisabled'] = $datesDisabled;
        // Ricerco la prima data disponibile
        $activeDate = $dateRange['startDate'];
        $endDate = $dateRange['endDate'];
        $firstDate = $activeDate;
        while (DateTimeHelper::compareDate($activeDate, $endDate) <= DateTimeHelper::equalsValue)
        {
            if (!in_array($activeDate, $disableList))
            {
                $firstDate = $activeDate;
                break;
            }
            $activeDate = DateTimeHelper::incDay($activeDate, 1);
        }
        $res['firstDate'] = $firstDate->format($dateFormat);
        return $res;
    }

    public function getDateSettingsJS($idLocation)
    {
        $aus = $this->getDateSettings($idLocation);
        $res['startDate'] = $aus['startDate'];
        $res['endDate'] = $aus['endDate'];
        $res['firstDate'] = $aus['firstDate'];
        $datesDisabled = [];
        $list = $aus['datesDisabled'];
        foreach ($list as $d)
        {
            $datesDisabled[] = '"' . $d . '"';
        }
        $res['datesDisabled'] = implode(',', $datesDisabled);
        return $res;
    }

    public function getDisabledDateList($idLocation)
    {
        $res = [];
        $limit = $this->getDateLimit();
        $activeDate = $limit['startDate'];
        $openDays = $this->getLocationOpenDay($idLocation);
        $specialOpenDays = $this->getSpecialDayList($limit['startDate'], $limit['endDate'], 1);
        $specialCloseDays = $this->getSpecialDayList($limit['startDate'], $limit['endDate'], 0);
        while (DateTimeHelper::compareDate($activeDate, $limit['endDate']) <= DateTimeHelper::equalsValue)
        {
            $disable = true;
            if (in_array(DateTimeHelper::formatISO($activeDate), $specialOpenDays))
            {
                $disable = false;
            } else
            {
                if (in_array($activeDate->format('w'), $openDays))
                {
                    $disable = false;
                }

                if (in_array(DateTimeHelper::formatISO($activeDate), $specialCloseDays))
                {
                    $rs = $this->getTimeSlotSpecialDay($activeDate, 0);
                    $disable = count($rs) == 0;
                }
            }
            if ($disable)
            {
                $res[] = $activeDate;
            }

            $activeDate = DateTimeHelper::incDay($activeDate, 1);
        }
        return $res;
    }

    public function getDisabledDateListISO($idLocation)
    {
        $res = [];
        $l = $this->getDisabledDateList($idLocation);
        foreach ($l as $d)
        {
            $res[] = DateTimeHelper::formatISO($d);
        }

        return $res;
    }

    public function getLocationIDFromType($idType)
    {
        $q = $this->db->prepare('select id, id_location from #__reservation_type where id = :id');
        $q->bindValue(':id', (int) $idType);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            return $r['id_location'];
        } else
        {
            throw new \Exception('Invalid location!!!!!');
        }
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {

            $idLocation = (int) $data['location'];
            $quantity = ArrayHelper::getInt($data, 'quantity', 1);
            if (ArrayHelper::getStr($data, 'daterange', 'false') == 'true')
            {
                $startDate = $this->lang->langDateToDateTime($data['datestart']);
                $endDate = $this->lang->langDateToDateTime($data['dateend']);
            } else
            {
                $startDate = $this->lang->langDateToDateTime($data['date']);
                $endDate = $this->lang->langDateToDateTime($data['date']);
            }

            $values = [
                'id_category' => $this->getCategoryIDFromType($data['type']),
                'id_location' => $idLocation,
                'id_type' => ArrayHelper::getInt($data, 'type'),
                'id_timeslot' => ArrayHelper::getInt($data, 'timeslot'),
                'arrival' => ArrayHelper::getStr($data, 'arrival'),
                'name' => ArrayHelper::getStr($data, 'name'),
                'surname' => ArrayHelper::getStr($data, 'surname'),
                'email' => ArrayHelper::getStr($data, 'email'),
                'phone' => ArrayHelper::getStr($data, 'phone'),
                'company' => ArrayHelper::getStr($data, 'company'),
                'vat' => ArrayHelper::getStr($data, 'vatnumber'),
                'notes' => ArrayHelper::getStr($data, 'notes')
            ];

            $this->saveDB($startDate, $endDate, $quantity, $values);
            return true;
        }
    }

    public function isUseCartLocation($idLocation)
    {
        if ($this->isMultiLocation())
        {
            $location = $this->getLocation($this->getDefaultLocationID());
        } else
        {
            $location = $this->getLocation($idLocation);
        }
        return ArrayHelper::getBool(ArrayHelper::getArray($location, 'options'), 'usecart');
    }

    public function isUseCart($data)
    {
        return $this->isUseCartLocation(ArrayHelper::getInt($data, 'location'));
    }

    public function saveCart(&$data, &$error)
    {
        if (!$this->container['componentManager']->componentExists('Order'))
        {
            $error = 'Componente "Order non disponibile!"';
            return false;
        }

        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $cart = new \EOS\Components\Cart\Models\CartModel($this->container);
            $values = [
                'id_category' => $this->getCategoryIDFromType($data['type']),
                'id_location' => ArrayHelper::getInt($data, 'location'),
                'id_type' => ArrayHelper::getInt($data, 'type'),
                'id_timeslot' => ArrayHelper::getInt($data, 'timeslot'),
                'arrival' => ArrayHelper::getStr($data, 'arrival'),
                'date' => $this->lang->langDateToDbDate(ArrayHelper::getStr($data, 'date')),
                'quantity' => ArrayHelper::getInt($data, 'quantity', 1),
                'mode' => ArrayHelper::getInt($data, 'mode', 0)];
            $quantity = ArrayHelper::getInt($data, 'quantity', 1);

            $rows = \EOS\Components\Cart\Classes\CartHelper::getCustomRowsByType($cart->getDBRows(), 'reservation');
            $addCart = true;
            foreach ($rows as $row)
            {
                $cartData = \EOS\Components\Cart\Classes\CartHelper::getCustomRowData($row);
                if (($values['id_type'] == $cartData['id_type']) &&
                    ($values['id_timeslot'] == $cartData['id_timeslot']) &&
                    ($values['date'] == $cartData['date']))
                {
                    $addCart = false;
                    $quantity = $quantity + $row['quantity'];
                    $cart->updateRow($row['id'], $quantity);
                    break;
                }
            }

            if ($addCart)
            {
                $cart->addCustomRow(0, $quantity, 'reservation', $values);
            }
            return true;
        }
    }

    public function saveDB($startDate, $endDate, $quantity, $data)
    {
        $evBefore = new \EOS\System\Event\DataEvent($this, $data);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Reservation\Classes\Events::BOOKING_DATA_BEFORE_SAVE, $evBefore);
        $disableList = $this->getDisabledDateListISO($data['id_location']);
        $this->db->beginTransaction();
        $activeDate = $startDate;
        $tbl = $this->db->tableFix('#__reservation');
        while (DateTimeHelper::compareDate($activeDate, $endDate) <= DateTimeHelper::equalsValue)
        {
            if (!in_array(DateTimeHelper::formatISO($activeDate), $disableList))
            {
                for ($i = 1; $i <= $quantity; $i++)
                {
                    $values = [
                        'id_category' => $data['id_category'],
                        'id_location' => $data['id_location'],
                        'id_type' => $data['id_type'],
                        'id_timeslot' => $data['id_timeslot'],
                        'arrival' => ArrayHelper::getStr($data, 'arrival'),
                        'date_reservation' => $this->db->util->dateTimeToDbDate($activeDate),
                        'name' => ArrayHelper::getStr($data, 'name'),
                        'surname' => ArrayHelper::getStr($data, 'surname'),
                        'email' => ArrayHelper::getStr($data, 'email'),
                        'phone' => ArrayHelper::getStr($data, 'phone'),
                        'company' => ArrayHelper::getStr($data, 'company'),
                        'vat' => ArrayHelper::getStr($data, 'vatnumber'),
                        'notes' => ArrayHelper::getStr($data, 'notes')
                    ];
                    $values['privacy'] = (ArrayHelper::getBool($data, 'consent1') == true) ? 1 : 0;
                    $values['commercial'] = (ArrayHelper::getBool($data, 'consent2') == true) ? 1 : 0;
                    $values['ins_id'] = 0;
                    $values['ins_date'] = $this->db->util->nowToDBDateTime();
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                    $query->execute();
                }
            }
            $activeDate = DateTimeHelper::incDay($activeDate, 1);
        }
        $this->db->commit();

        $evAfter = new \EOS\System\Event\DataEvent($this, $data);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Reservation\Classes\Events::BOOKING_DATA_AFTER_SAVE, $evAfter);
    }

    public function getCategoryList($idLocation)
    {
        $q = $this->db->prepare('select ct.id, ctl.name ' .
            ' from #__reservation_category as ct' .
            ' inner join #__reservation_category_lang as ctl on ct.id = ctl.id_category' .
            ' where ct.id in (select id_category' .
            ' from #__reservation_type' .
            ' where id_location = :id_location and status = 1) and ctl.id_lang = :id_lang ');
        $q->bindValue(':id_location', $idLocation);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        return $q->fetchAll();
    }

    public function getTypeList($idLocation, $idCategory)
    {
        $q = $this->db->prepare('select tp.id, ' .
            ' tpl.name, tpl.description, price_reservation as price, options' .
            ' from #__reservation_type tp' .
            ' inner join #__reservation_type_lang tpl on tp.id = tpl.id_type' .
            ' where tp.id_location = :id_location and tp.id_category = :id_category and tpl.id_lang = :id_lang and tp.status = 1');
        $q->bindValue(':id_location', (int) $idLocation);
        $q->bindValue(':id_category', (int) $idCategory);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        return $q->fetchAll();
    }

    // Usato nel Subscriber
    public function getType($idType)
    {
        $q = $this->db->prepare('select tp.id, ' .
            ' tpl.name, tpl.description, price_reservation as price, options' .
            ' from #__reservation_type tp' .
            ' inner join #__reservation_type_lang tpl on tp.id = tpl.id_type' .
            ' where tp.id = :id and tpl.id_lang = :id_lang');
        $q->bindValue(':id', (int) $idType);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        return $q->fetch();
    }

    // Usato nel Subscriber
    public function getTimeSlot($idTimeSlot)
    {
        $q = $this->db->prepare('select ts.id, tsl.name' .
            ' from #__reservation_timeslot ts' .
            ' inner join #__reservation_timeslot_lang tsl on ts.id = tsl.id_timeslot' .
            ' where ts.id = :id and tsl.id_lang = :id_lang');
        $q->bindValue(':id', (int) $idTimeSlot);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        return $q->fetch();
    }

    public function getTimeSlotType($dateLang, $idType)
    {
        $res = [];
        if (!empty($dateLang))
        {
            $dt = $this->lang->langDateToDateTime($dateLang);
            if ($dt !== false)
            {
                $qo = $this->db->prepare('select count(id) as count from #__reservation_special_day '
                    . 'where special_day = :date and status = 1');
                $qo->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
                $qo->execute();
                $ro = $qo->fetch();
                $qc = $this->db->prepare('select count(id) as count from #__reservation_special_day '
                    . 'where special_day = :date and status = 0');
                $qc->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
                $qc->execute();
                $rc = $qc->fetch();
                $ssd = '';
                if (isset($ro) && ($ro['count'] > 0))
                {
                    $ssd = ' and tts.id_timeslot in (select id_timeslot from #__reservation_special_day where special_day = :date and status = 1)';
                }
                if (isset($rc) && ($rc['count'] > 0))
                {
                    $ssd = ' and tts.id_timeslot not in (select id_timeslot from #__reservation_special_day where special_day = :date and status = 0)';
                }
                $sql = 'select distinct t.id, tl.name, t.start, t.finish ' .
                    ' from #__reservation_timeslot t' .
                    ' inner join #__reservation_timeslot_lang tl on t.id = tl.id_timeslot' .
                    ' inner join #__reservation_type_timeslot tts on tts.id_timeslot = t.id' .
                    ' inner join #__reservation_type rt on rt.id = tts.id_type' .
                    ' where tl.id_lang = :id_lang and rt.id = :id_type' . $ssd . ' order by tl.name';
                $q = $this->db->prepare($sql);
                $q->bindValue(':id_lang', $this->lang->getCurrentID());
                $q->bindValue(':id_type', $idType);
                if ((isset($ro) && ($ro['count'] > 0)) || (isset($rc) && ($rc['count'] > 0)))
                {
                    $q->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
                }
                $q->execute();
                $res = $q->fetchAll();
            }
        }
        return $res;
    }

    public function getQuantityAvailable($dateLang, $idType, $idTimeSlot, $calcCart = true)
    {
        $res = 0;
        $reslimit = 0;
        $firstLockTypeAvialable = false;
        if ((!empty($dateLang)) && (!empty($idType)))
        {
            $dt = $this->lang->langDateToDateTime($dateLang);
            if ($dt !== false)
            {
                $q = $this->db->prepare('select t.availability_customer, tc.availability_customer_limit, t.id_category, tc.options as options_cat from #__reservation_type t ' .
                    ' inner join #__reservation_type_timeslot ts on t.id = ts.id_type' .
                    ' inner join #__reservation_category tc on t.id_category = tc.id' .
                    ' where t.id = :id_type and ts.id_timeslot = :id_timeslot');
                $q->bindValue(':id_type', $idType);
                $q->bindValue(':id_timeslot', $idTimeSlot);
                $q->execute();
                $r = $q->fetch();
                if (!empty($r))
                {
                    $res = (int) $r['availability_customer'];
                    $reslimit = (int) $r['availability_customer_limit'];
                    if (($reslimit > 0) && ($res > $reslimit))
                    {
                        $res = $reslimit;
                    }
                    $options = ArrayHelper::fromJSON($r['options_cat']);
                    $firstLockTypeAvialable = ArrayHelper::getBool($options, 'first_lock_type_available');
                }
                $category = ArrayHelper::getInt($r, 'id_category');
                $dbDate = $this->db->util->dateTimeToDBDate($dt);
                // Estrapolo la quantity prenotato
                $sql = 'select count(id) as tot, max(id_type) as typefirst from #__reservation res ' .
                    'where res.id_timeslot = :id_timeslot and res.date_reservation = :date_reservation';
                if ($reslimit > 0)
                {
                    $sql .= ' and res.id_type in (select id_type from #__reservation_type where id_category = :id_category)';
                } else
                {
                    $sql .= ' and res.id_type = :id_type';
                }
                $q = $this->db->prepare($sql);
                $q->bindValue(':date_reservation', $dbDate);
                $q->bindValue(':id_timeslot', $idTimeSlot);
                if ($reslimit > 0)
                {
                    $q->bindValue(':id_category', $category);
                } else
                {
                    $q->bindValue(':id_type', $idType);
                }
                $q->execute();
                $r = $q->fetch();
                if (!empty($r))
                {
                    $res = $res - (int) $r['tot'];
                    if (($reslimit > 0) && ($firstLockTypeAvialable) && (!empty($r['typefirst']))&& ((int)$r['typefirst'] != $idType))
                    {
                        $res = 0;
                    }
                }


                if (($calcCart) && ($this->isUseCartLocation($this->getLocationIDFromType($idType))) &&
                    ($this->container['componentManager']->componentExists('Order')))
                {
                    $cart = new \EOS\Components\Order\Models\CartModel($this->container);
                    $rows = \EOS\Components\Order\Classes\CartHelper::getCustomRowsByType($cart->getDBRows(), 'reservation');
                    foreach ($rows as $row)
                    {
                        $cartData = \EOS\Components\Order\Classes\CartHelper::getCustomRowData($row);
                        if ($reslimit > 0)
                        {
                            if (($category == $cartData['id_category']) &&
                                ($idTimeSlot == $cartData['id_timeslot']) &&
                                ($dbDate == $cartData['date']))
                            {
                                $res = $res - (int) $row['quantity'];
                                if(($firstLockTypeAvialable) && ((int)$cartData['id_type'] != $idType))
                                {
                                    $res = 0;
                                }
                            }
                        } else
                        {
                            if (($idType == $cartData['id_type']) &&
                                ($idTimeSlot == $cartData['id_timeslot']) &&
                                ($dbDate == $cartData['date']))
                            {
                                $res = $res - (int) $row['quantity'];
                            }
                        }
                    }
                }
            }
        }
        if ($res < 0)
        {
            $res = 0;
        }
        return $res;
    }

    public function getQuantityList($dateLang, $idType, $idTimeSlot)
    {
        $qty = $this->getQuantityAvailable($dateLang, $idType, $idTimeSlot);
        $res = [];
        for ($i = 1; $i <= $qty; $i++)
        {
            $res[] = ['id' => $i, 'name' => $i];
        }
        return $res;
    }

    private function getTimeSlotSpecialDay(\DateTimeImmutable $date, $status)
    {
        $q = $this->db->prepare('select id_timeslot from #__reservation_special_day '
            . 'where special_day = :date and status = :status ');
        $q->bindValue(':date', $this->db->util->dateTimeToDBDate($date));
        $q->bindValue(':status', $status);
        $q->execute();
        $rs = $q->fetchAll();
        foreach ($rs as $r)
        {
            $list[] = $r['id_timeslot'];
        }
        $sql = 'select distinct t.id, tl.name ' .
            ' from #__reservation_timeslot t' .
            ' inner join #__reservation_timeslot_lang tl on t.id = tl.id_timeslot' .
            ' where tl.id_lang = :id_lang ';
        if ($status == 1)
        {
            $sql .= ' and t.id in (' . implode(',', $list) . ')';
        } else
        {
            $sql .= ' and t.id not in (' . implode(',', $list) . ')';
        }
        $sql .= ' order by tl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        return $q->fetchAll();
    }

    public function getTypePrice($idtype, $date, $fullprice)
    {
        $type = $this->getType($idtype);
        $price = 0;
        if (!empty($type))
        {
            $options = ArrayHelper::fromJSON($type['options']);
            $price = $type['price'];
            if (!$fullprice)
            {
                if (DateTimeHelper::incDay($date, - ArrayHelper::getInt($options, 'deposit_day')) > DateTimeHelper::date())
                {
                    $price = $price * (ArrayHelper::getInt($options, 'deposit_percent') / 100);
                }
            }
        }
        return $price;
    }

//    public function getTimeSlotDay($dateLang, $idCategory)
//    {
//        $res = [];
//        if (!empty($dateLang))
//        {
//            $dt = $this->lang->langDateToDateTime($dateLang);
//            if ($dt !== false)
//            {
//                $qo = $this->db->prepare('select count(id) as count from #__reservation_special_day '
//                    . 'where special_day = :date and status = 1');
//                $qo->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
//                $qo->execute();
//                $ro = $qo->fetch();
//                $qc = $this->db->prepare('select count(id) as count from #__reservation_special_day '
//                    . 'where special_day = :date and status = 0');
//                $qc->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
//                $qc->execute();
//                $rc = $qc->fetch();
//                $ssd = '';
//                if (isset($ro) && ($ro['count'] > 0))
//                {
//                    $ssd = ' and tts.id_timeslot in (select id_timeslot from #__reservation_special_day where special_day = :date and status = 1)';
//                }
//                if (isset($rc) && ($rc['count'] > 0))
//                {
//                    $ssd = ' and tts.id_timeslot not in (select id_timeslot from #__reservation_special_day where special_day = :date and status = 0)';
//                }
//                $sql = 'select distinct t.id, tl.name, t.start, t.finish ' .
//                    ' from #__reservation_timeslot t' .
//                    ' inner join #__reservation_timeslot_lang tl on t.id = tl.id_timeslot' .
//                    ' left outer join #__reservation_type_timeslot tts on tts.id_timeslot = t.id' .
//                    ' left outer join #__reservation_type rt on rt.id = tts.id_type' .
//                    ' left outer join #__reservation_category c on c.id = rt.id_category' .
//                    ' where tl.id_lang = :id_lang and rt.id_category = :id_category' . $ssd . ' order by tl.name';
//                $q = $this->db->prepare($sql);
//                $q->bindValue(':id_lang', $this->lang->getCurrentID());
//                $q->bindValue(':id_category', $idCategory);
//                if ((isset($ro) && ($ro['count'] > 0)) || (isset($rc) && ($rc['count'] > 0)))
//                {
//                    $q->bindValue(':date', $this->db->util->dateTimeToDBDate($dt));
//                }
//                $q->execute();
//                $res = $q->fetchAll();
//            }
//        }
//        return $res;
//    }
//    
//    public function getTypeDay($dateLang, $idTimeSlot, $idCategory)
//    {
//        $res = [];
//        if (!empty($dateLang))
//        {
//            $dt = $this->lang->langDateToDbDate($dateLang);
//            if ($dt !== false)
//            {
//                $sql = 'select t.id, tl.name ' .
//                    ' from #__reservation_type t' .
//                    ' inner join #__reservation_type_lang tl on t.id = tl.id_type' .
//                    ' left outer join #__reservation_type_timeslot tts on (t.id = tts.id_type and tts.id_timeslot = :id_timeslot)' .
//                    ' where tl.id_lang = :id_lang and status = 1 and t.id_category = :id_category' .
//                    ' and t.availability_customer > (select count(id) from #__reservation res where res.id_type = t.id and res.id_timeslot = :id_timeslot and res.date_reservation = :date_reservation)' .
//                    ' order by tl.name';
//                $q = $this->db->prepare($sql);
//                $q->bindValue(':id_lang', $this->lang->getCurrentID());
//                $q->bindValue(':id_category', (int) $idCategory);
//                $q->bindValue(':id_timeslot', (int) $idTimeSlot);
//                $q->bindValue(':date_reservation', $dt);
//                $q->execute();
//                $res = $q->fetchAll();
//            }
//        }
//        return $res;
//    }
//    public function getReservationByDate($dateLang, $type)
//    {
//        $res = [];
//        if (!empty($dateLang))
//        {
//            $dt = $this->lang->langDateToDateTime($dateLang);
//            if ($dt !== false)
//            {
//                $sql = 'select count(id) ' .
//                    ' from #__reservation r' .
//                    ' where r.date_reservation = :date_reservation';
//                $q = $this->db->prepare($sql);
//                $q->bindValue(':date_reservation', $dateLang);
//                $q->bindValue(':id_type', $type);
//                $q->execute();
//                $res = $q->fetchAll();
//            }
//        }
//        return $res;
//    }
//    public function getTypeByID($id)
//    {
//        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_type rt'))
//            ->select(null)
//            ->select('rt.id')
//            ->select('rt.id_location')
//            ->select('rt.id_category')
//            ->select('rt.availability_total')
//            ->select('rt.availability_customer')
//            ->select('rt.price_reservation')
//            ->select('rt.status')
//            ->where('rt.id = ?', (int) $id);
//        $rc = $query->fetch();
//        if ($rc == null)
//        {
//            $res = [];
//        } else
//        {
//            $ll = $this->lang->getArrayFromDB(true);
//            $res['id'] = $rc['id'];
//            $res['id_location'] = $rc['id_location'];
//            $res['id_category'] = $rc['id_category'];
//            $res['availability_total'] = $rc['availability_total'];
//            $res['availability_customer'] = $rc['availability_customer'];
//            $res['price_reservation'] = $rc['price_reservation'];
//            $res['status'] = $rc['status'];
//            foreach ($ll as $l)
//            {
//                $idlang = $l['id'];
//                $tblContent = $this->db->tableFix('#__reservation_type_lang');
//                $query = $this->db->newFluent()->from($tblContent)->select('id')
//                    ->where('id_type', $rc['id'])
//                    ->where('id_lang', $idlang);
//                $rcl = $query->fetch();
//                if ($rcl != null)
//                {
//                    $res['name-' . $idlang] = $rcl['name'];
//                    $res['descr-' . $idlang] = $rcl['description'];
//                }
//            }
//            $tblTimeslot = $this->db->tableFix('#__reservation_type_timeslot');
//            $query = $this->db->newFluent()->from($tblTimeslot)->select('id_type, id_timeslot')
//                ->where('id_type', $rc['id']);
//            $rcts = $query->fetchAll();
//            if ($rcts != null)
//            {
//                foreach ($rcts as $timeslot)
//                {
//                    $res['timeslot-' . $timeslot['id_timeslot']] = $timeslot['id_timeslot'];
//                }
//            }
//        }
//        return $res;
//    }
//
//    public function getTimeslotByID($id)
//    {
//        $query = $this->db->newFluent()->from($this->db->tableFix('#__reservation_timeslot rt'))
//            ->select(null)
//            ->select('rt.id')
//            ->select('rt.start')
//            ->select('rt.finish')
//            ->where('rt.id = ?', (int) $id);
//        $rc = $query->fetch();
//        if ($rc == null)
//        {
//            $res = [];
//        } else
//        {
//            $ll = $this->lang->getArrayFromDB(true);
//            $res['id'] = $rc['id'];
//            $res['start'] = $rc['start'];
//            $res['finish'] = $rc['finish'];
//            foreach ($ll as $l)
//            {
//                $idlang = $l['id'];
//                $tblContent = $this->db->tableFix('#__reservation_timeslot_lang');
//                $query = $this->db->newFluent()->from($tblContent)->select('id')
//                    ->where('id_timeslot', $rc['id'])
//                    ->where('id_lang', $idlang);
//                $rcl = $query->fetch();
//                if ($rcl != null)
//                {
//                    $res['name-' . $idlang] = $rcl['name'];
//                }
//            }
//        }
//        return $res;
//    }
}
