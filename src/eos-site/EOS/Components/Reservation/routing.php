<?php

$this->mapComponent(['GET'], 'Reservation', ['booking/index' => 'BookingController:index']);
$this->mapComponent(['GET'], 'Reservation', ['booking/index/{id}' => 'BookingController:index']);
$this->mapComponent(['POST'], 'Reservation', ['booking/ajaxcommand' => 'BookingController:ajaxCommand']);
$this->mapComponent(['GET'], 'Reservation', ['booking/confirm' => 'BookingController:confirm']);
$this->mapComponent(['GET'], 'Reservation', ['booking/testmail' => 'BookingController:testMail']);


