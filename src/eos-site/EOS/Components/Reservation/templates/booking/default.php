<form id="reservation" method="POST" class="reservation-form">
    <div id="reservation-container"  class="container reservation-container">
        <div class="row">
            <?php
            $this->includePartial('header');
            $this->includePartial('location', true);
            $this->includePartial('mode' . $this->mode, true);
            if (!$this->useCart)
            {
                $this->includePartial('customer');
            }
            $this->includePartial('send', false);
            $this->includePartial('footer');
            ?>

        </div>
    </div>
</form>
<?php
$this->startCaptureScript();
$this->includePartial('js_service', false);
$this->includePartial('js', false);
$this->includePartial('js_mode' . $this->mode);
$this->endCaptureScript();

