<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-name"><?php $this->transPE('reservation.booking.name'); ?></label>
            <input type="text" name="name" id="reservation-name" class="form-control" maxlength="250">
        </div>
    </div>  
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-surname"><?php $this->transPE('reservation.booking.surname'); ?></label>
            <input type="text" name="surname" id="reservation-surname" class="form-control" maxlength="250">
        </div>
    </div>  
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-email"><?php $this->transPE('reservation.booking.email'); ?></label>
            <input type="text" name="email" id="reservation-email" class="form-control" maxlength="250">
        </div>
    </div>  
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-phone"><?php $this->transPE('reservation.booking.phone'); ?></label>
            <input type="text" name="phone" id="reservation-phone" class="form-control" maxlength="250">
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-company"><?php $this->transPE('reservation.booking.company'); ?></label>
            <input type="text" name="company" id="reservation-company" class="form-control">
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-vatnumber"><?php $this->transPE('reservation.booking.vatnumber'); ?></label>
            <input type="text" name="vatnumber" id="reservation-vatnumber" class="form-control" >
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-notes"><?php $this->transPE('reservation.booking.notes'); ?></label>
            <textarea rows="4" cols="50" name="notes" id="reservation-notes" class="form-control" maxlength="1000"></textarea>
        </div>
    </div> 
</div> 
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <label id="reservation-privacy-title"><?php $this->transPE('reservation.booking.privacy.title'); ?></label>
        <div class="form-group">
            <label id="reservation-privacy-consent1"><?php $this->transPE('reservation.booking.privacy.consent1'); ?></label>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="true" id="reservation-consent1-yes" name="consent1" checked>
                    <?php $this->transPE('reservation.booking.consent.yes'); ?>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="false" id="reservation-consent1-no" name="consent1">
                    <?php $this->transPE('reservation.booking.consent.no'); ?>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="form-group">
            <label id="reservation-privacy-consent2"><?php $this->transPE('reservation.booking.privacy.consent2'); ?></label>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="true" id="reservation-consent2-yes" name="consent2" checked>
                    <?php $this->transPE('reservation.booking.consent.yes'); ?>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="false" id="reservation-consent2-no" name="consent2">
                    <?php $this->transPE('reservation.booking.consent.no'); ?>
                </label>
            </div>
        </div>
    </div>
</div>