<script>
    $(function ()
    {
        $("#reservation-send").click(function (event)
        {
            event.preventDefault();
            $("#reservation-send").attr('disabled', 'disabled');
            setTimeout(function ()
            {
                $("#reservation-send").removeAttr('disabled');
            }, 1000);
            reservationService.save();
        });
        setTimeout(function ()
        {
            $("#reservation-location").trigger('change');
        }, 1);
    });
</script>
