<script>
    $(function ()
    {
        var optionsDate = {
            language: "<?php echo $this->lang->getCurrentISO(); ?>",
            format: "<?php echo $this->lang->getCurrentDatePickerFormat(); ?>",
            todayHighlight: true,
            autoclose: true,
            startDate: "<?php echo $this->dateSettingsJS['startDate']; ?>",
            endDate: "<?php echo $this->dateSettingsJS['endDate']; ?>",
            datesDisabled: [<?php echo $this->dateSettingsJS['datesDisabled']; ?>]
        };
        $("#reservation-date").datepicker(optionsDate);
       $("#reservation-date").datepicker('update', "<?php echo $this->dateSettingsJS['firstDate']; ?>");

        $("#reservation-location").on('change', function ()
        {
            var data = {"command": "category"};
            reservationService.runAjax(data, function (json)
            {
                reservationService.fillSelect('#reservation-category', json.list);
            });

            var data = {"command": "datesettings"};
            reservationService.runAjax(data, function (json)
            {
                var dt = $('#reservation-date');
                dt.datepicker('setStartDate', json.date.startdate);
                dt.datepicker('setEndDate', json.date.enddate);
                dt.datepicker('setDatesDisabled', json.date.datesdisabled);
            });
        });

        $("#reservation-category").on('change', function ()
        {
            var category = $("#reservation-category").val();
            var data = {'command': "type", 'category': category};
            reservationService.runAjax(data, function (json)
            {
                reservationService.fillSelect('#reservation-type', json.list);
            });
        });

        $("#reservation-type").on('change', function ()
        {
            $("#reservation-date").trigger('change');
        });

        $("#reservation-date").on('change', function ()
        {
            var type = $("#reservation-type").val();
            var date = $("#reservation-date").val();
            var data = {'command': "timeslot", 'type': type, 'date': date};
            reservationService.runAjax(data, function (json)
            {
                reservationService.fillSelect('#reservation-timeslot', json.list);
            });
        });

        $("#reservation-timeslot").on('change', function ()
        {
            var type = $("#reservation-type").val();
            var timeslot = $("#reservation-timeslot").val()
            var date = $("#reservation-date").val();
            var data = {'command': "quantitylist", 'type': type, 'timeslot': timeslot, 'date': date};
            reservationService.runAjax(data, function (json)
            {
                reservationService.fillSelect('#reservation-quantity', json.list);
            });
        });
    });

</script>
