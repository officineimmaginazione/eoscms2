<script>
    $(function ()
    {
        var optionsDate = {
            language: "<?php echo $this->lang->getCurrentISO(); ?>",
            format: "<?php echo $this->lang->getCurrentDatePickerFormat(); ?>",
            todayHighlight: true,
            autoclose: true,
            startDate: "<?php echo $this->dateSettingsJS['startDate']; ?>",
            endDate: "<?php echo $this->dateSettingsJS['endDate']; ?>",
            datesDisabled: [<?php echo $this->dateSettingsJS['datesDisabled']; ?>]
        };
        $("#reservation-date").datepicker(optionsDate);
        $("#reservation-date").change(function ()
        {
            updateTimeSlot();
            $("#reservation-time").trigger('change');
        });
        $("#reservation-time").change(function ()
        {
            updateType();
        });

        $("#reservation-location").change(function ()
        {
            updateLocation();
        });
    });


    function updateLocation()
    {
        var data = {"command": "datesettings"};
        reservationService.runAjax(data, function (json)
        {
            var dt = $('#reservation-date');
            dt.datepicker('setStartDate', json.date.startdate);
            dt.datepicker('setEndDate', json.date.enddate);
            dt.datepicker('setDatesDisabled', json.date.datesdisabled);
            dt.datepicker('clearDates');
        });
    }

    function updateTimeSlot()
    {
        var currDate = $("#reservation-date").val();
        var data = {"command": "timeslot", "date": currDate};
        reservationService.runAjax(data, function (json)
        {
            var options = $("#reservation-time");
            options.empty();
            $.each(json.list, function ()
            {
                options.append(new Option(this.name, this.id));
            });
        });
    }

    function updateType()
    {
        var currDate = $("#reservation-date").val();
        var currTs = $("#reservation-time").val();
        if (currTs === undefined || currTs === null)
        {
            currTs = 3;
        }
        var data = {"command": "type", "date": currDate, "timeslot": currTs};
        reservationService.runAjax(data, function (json)
        {
            var options = $("#reservation-type");
            options.empty();
            $.each(json.list, function ()
            {
                options.append(new Option(this.name, this.id));
            });
        });

    }
</script>
