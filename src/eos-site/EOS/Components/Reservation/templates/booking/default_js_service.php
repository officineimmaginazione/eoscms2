<script>
    var reservationService = {
        runAjax: function (data, resultEvent)
        {
            data['location'] = parseInt($("#reservation-location").val());
            data['mode'] = <?php echo $this->mode; ?>;
            data['<?php echo $this->session->getTokenName() ?>'] = '<?php echo $this->session->getTokenValue(); ?>';
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Reservation', 'booking/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        resultEvent(json);
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        },
        on: function (eventName, eventFunc)
        {
            $('#reservation').on('reservation-' + eventName, eventFunc);
        },
        trigger: function (eventName)
        {
            $('#reservation').trigger('reservation-' + eventName);
        },
        load: function ()
        {
            var reservationService = this;
            $(function ()
            {
                reservationService.trigger('load');
            });
        },
        save: function ()
        {
            this.trigger('save');
            var data = $('#reservation').serializeFormJSON();
            this.runAjax(data, function (json)
            {
                location.href = json.redirect;
            });
        },
        fillSelect: function (selectName, list)
        {
            var options = $(selectName);
            var oldVal = options.val();
            options.empty();
            $.each(list, function ()
            {
                options.append(new Option(this.name, this.id));
            });
            if (oldVal != null)
            {
                options.val(oldVal);
            }
            options.trigger('change');
        }
    };

    reservationService.load();

</script>
