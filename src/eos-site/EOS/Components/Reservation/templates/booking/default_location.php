<?php

if ($this->isMultiLocation)
{
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="reservation-location"><?php $this->transPE('reservation.booking.location'); ?></label>
                <select name="location" id="reservation-location" class="form-control">
                  <?php
                  foreach ($this->locations as $location)
                  {
                      $sel = $location['id'] == $this->idLocation ? ' selected' : '';
                      ?>
                        <option <?php echo $sel; ?> value="<?php $this->printHtml($location['id']) ?>"><?php $this->printHtml($location['name']) ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div> 
    </div> 
    <?php
} else
{
    ?>
    <input type="hidden"  name="location" id="reservation-location" value="<?php echo $this->idLocation; ?>">
    <?php
}