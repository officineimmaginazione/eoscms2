<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-category"><?php $this->transPE('reservation.booking.category'); ?></label>
            <select name="category" id="reservation-category" class="form-control">
            </select>
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-type"><?php $this->transPE('reservation.booking.type'); ?></label>
            <select name="type" id="reservation-type" class="form-control">
            </select>
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-date"><?php $this->transPE('reservation.booking.date'); ?></label>
            <div class="input-group date">
                <input type="text" name="date" id="reservation-date" class="form-control" maxlength="10">
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-timeslot"><?php $this->transPE('reservation.booking.timeslot'); ?></label>
            <select name="timeslot" id="reservation-timeslot" class="form-control">
            </select>
        </div>
    </div> 
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <label for="reservation-quantity"><?php $this->transPE('reservation.booking.quantity'); ?></label>
            <select name="quantity" id="reservation-quantity" class="form-control">
            </select>
        </div>
    </div> 
</div> 
