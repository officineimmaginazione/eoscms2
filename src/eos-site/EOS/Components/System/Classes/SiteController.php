<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

class SiteController extends \EOS\System\Controllers\Controller
{

    protected $seo;

    protected function loadParams()
    {
        parent::loadParams();
        $theme = $this->container->get('database')->setting->getStr('setting', 'theme');
        if (!empty($theme))
        {
            $this->currentParams['themeName'] = $theme;
        }
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->seo = new \EOS\System\Seo\Manager($this->container);
    }

    public function newView($request, $response, $classView = '')
    {
        if (empty($classView))
        {
            $classView = '\EOS\Components\System\Classes\SiteView';
        }
        $v = parent::newView($request, $response, $classView);

        $this->seo->loadSeoView($v, 'default', 0, $this->lang->getCurrentID(), false);
        return $v;
    }

}
