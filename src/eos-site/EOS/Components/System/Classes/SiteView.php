<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Classes;

use EOS\System\Util\TagParser;
use EOS\System\Routing\PathHelper;

class SiteView extends \EOS\System\Views\Html
{

    protected $breadcrumbs;
    protected $langlinks;

    public function __construct($controller, $request, $response)
    {
        parent::__construct($controller, $request, $response);
        $this->breadcrumbs = [];
        $this->langlinks = [];
    }

    public function renderExternal($content, $skipName = '')
    {
        $tp = new TagParser();
        $tp->value = $content;
        $view = $this;
        $tp->callBack = function ($command, &$result) use ($skipName, $view)
        {
            $p = strpos($command, ' ');
            if ($p !== false)
            {
                $method = mb_strtolower(substr($command, 0, $p));
                $params = substr($command, $p + 1);
                switch ($method)
                {
                    case 'loadsection':
                        if (mb_strtolower($params) != mb_strtolower($skipName))
                        {
                            $w = new \EOS\Components\Content\Widgets\SectionWidget($view, $params);
                            ob_start();
                            $w->write();
                            $result = ob_get_contents();
                            ob_end_clean();
                        }
                        break;

                    case 'loadmenu':
                        if (mb_strtolower($params) != mb_strtolower($skipName))
                        {
                            $w = new \EOS\Components\System\Widgets\MenuWidget($view, $params);
                            ob_start();
                            $w->write();
                            $result = ob_get_contents();
                            ob_end_clean();
                        }
                        break;
                    case 'loadwidget':
                        // Consento di creare delle classi per caricare un widget
                        $l = explode(',', $params);
                        if (count($l) > 1)
                        {
                            $className = trim($l[0]);
                            $name = trim($l[1]);
                            if ((mb_strtolower($name) != mb_strtolower($skipName))/* && (class_exists($className)) &&
                              ((is_subclass_of($className, '\EOS\UI\Widget\Widget'))) */)
                            {
                                $w = new $className($view, $name);
                                ob_start();
                                $w->write();
                                $result = ob_get_contents();
                                ob_end_clean();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        };
        $tp->parse();
        return $tp->result;
    }

    public function render($componentTemplate, $withTemplate = true)
    {
        $this->response = parent::render($componentTemplate, $withTemplate);
        $cdn = new \EOS\System\Routing\CDN($this->controller->container, $this->request, $this->response);
        $cdn->render();
        return $this->response;
    }

    public function addBreadcrumb($component, $path, $title)
    {
        $this->breadcrumbs[] = ['component' => $component, 'path' => $path, 'title' => $title, 'url' => ''];
    }

    public function getBreadcrumbs()
    {
        $rootUrl = $this->path->getSelfUrl();
        if ($this->controller->container->get('currentParams')['multiLanguage'])
        {
            $rootUrl = PathHelper::addTrailingSlash($rootUrl . $this->controller->container->get('language')->getCurrentISO());
        }
        $res[] = ['component' => '', 'path' => '', 'title' => $this->lang->trans('system.common.home'), 'url' => $rootUrl];
        foreach ($this->breadcrumbs as $b)
        {
            $b['url'] = $this->path->getUrlFor($b['component'], $b['path']);
            if ($b['url'] != $rootUrl)
            {
                $res[] = $b;
            }
        }
        return $res;
    }

    public function addLangLinkList($component, $path, array $idLangList)
    {
        if ($this->controller->container->get('currentParams')['multiLanguage'])
        {
            foreach ($idLangList as $idLang)
            {
                $l = $this->lang->getLangFromID($idLang);
                if ($l !== false)
                {
                    $this->langlinks[] = ['id' => $idLang, 'iso' => $l->iso, 'component' => $component, 'path' => $path];
                }
            }
        }
    }

    public function getLangLinks()
    {
        foreach ($this->langlinks as &$l)
        {
            if (!isset($l['url']))
            {
                $l['url'] = $this->path->getDomainUrl() . $this->path->getUrlFor($l['component'], $l['path'], $l['iso']);
            }
        }
        return $this->langlinks;
    }

    public function prepareLangLinks()
    {
        if ($this->controller->container->get('currentParams')['multiLanguage'])
        {
            $ll = $this->getLangLinks();
            if (count($ll) > 1)
            {
                foreach ($ll as $ll)
                {
                    $this->addHeadLink(['rel' => 'alternate', 'hreflang' => $ll['iso'], 'href' => $ll['url']]);
                }
            }
        }
    }

}
