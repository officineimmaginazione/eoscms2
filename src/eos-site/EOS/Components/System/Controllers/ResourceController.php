<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Controllers;

class ResourceController extends \EOS\System\Controllers\Controller
{

    public function appJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        return $v->render('resource/app', 'js');
    }

    public function keepAliveJS($request, $response, $args)
    {
        $v = new \EOS\System\Views\Raw($this, $request, $response->withHeader('Content-type', 'application/javascript'), 0);
        return $v->render('resource/keepalive-js', 'php');
    }

}
