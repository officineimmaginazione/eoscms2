<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Widgets;

use \EOS\System\Routing\PathHelper;

class BreadcrumbWidget extends \EOS\UI\Widget\Widget
{

    private function menuTree($path, $list, $level, &$menuList)
    {
        $res = false;
        foreach ($list as $el)
        {
            if ($el['custom'] == false)
            {

                $res = $path == $el['path'];
                if ((!$res) && (!empty($el['children'])))
                {
                    $res = $this->menuTree($path, $el['children'], $level + 1, $menuList);
                }

                if ($res)
                {
                    array_unshift($menuList, ['path' => $el['path'], 'title' => $el['title']]);
                    $res = true;
                    break;
                }
            }
        }
        return $res;
    }

    private function rebuildBreadCrumbs($menuList, $brcIdx)
    {
        $newData = [];
        $newData[] = $this->data[0]; // Salvo la root
        foreach ($menuList as $el)
        {
            $newEl = [];
            $newEl['title'] = $el['title'];
            if (empty($el['path']))
            {
                $newEl['component'] = '';
                $newEl['path'] = '';
                $newEl['url'] = 'javascript:void(0)'; // Disattiva l'url
            } else
            {
                $pa = PathHelper::explodeSlash($el['path']);
                $cp = $pa[0];
                array_shift($pa);
                $newEl['component'] = $cp;
                $newEl['path'] = PathHelper::implodeSlash($pa);
                $newEl['url'] = $this->view->path->urlFor($cp, $pa);
            }
            $newData[] = $newEl;
        }
        for ($i = $brcIdx + 1; $i < count($this->data); $i++)
        {
            $newData[] = $this->data[$i];
        }

        $this->data = $newData;
    }

    private function hookMenu()
    {
        if (!empty($this->name))
        {
            $db = $this->view->controller->container->get('database');
            $lang = $this->view->controller->container->get('language');
            $query = $db->newFluent()->from($db->tableFix('#__menu m'))
                ->leftJoin($db->tableFix('#__menu_lang ml') . ' ON m.id = ml.id_menu')
                ->select(null)
                ->select('m.id, m.name, m.status, ml.data')
                ->where('m.name = ?', $this->name)
                ->where('m.status = 1')
                ->where('ml.id_lang = ?', $lang->getCurrentID());
            $rc = $query->fetch();
            if (empty($rc))
            {
                $data = [];
            } else
            {
                $data = json_decode($rc['data'], true);
            }
            if (!empty($data))
            {
                for ($i = count($this->data) - 1; $i > 0; $i--)
                {
                    $el = $this->data[$i];
                    $path = $el['component'] . '/' . $el['path'];
                    $menuList = [];               
                    if ($this->menuTree($path, $data['menu'], 0, $menuList))
                    {
                        $this->rebuildBreadCrumbs($menuList, $i);
                        break;
                    }
                }
            }
        }
    }

    protected function prepare()
    {
        parent::prepare();
        $this->data = $this->view->getBreadcrumbs();
        $this->hookMenu();
    }

    public function write()
    {
        $this->renderTemplate('widget/breadcrumb');
    }

}
