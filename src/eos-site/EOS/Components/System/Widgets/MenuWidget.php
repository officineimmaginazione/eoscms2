<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\System\Widgets;

class MenuWidget extends \EOS\UI\Widget\Widget
{

    public $data;
    public $dropdown = true;
    public $cssClass = 'nav navbar-nav';
    public $cssId = '';

    private function getData()
    {
        $db = $this->view->controller->container->get('database');
        $lang = $this->view->controller->container->get('language');
        $query = $db->newFluent()->from($db->tableFix('#__menu m'))
            ->leftJoin($db->tableFix('#__menu_lang ml') . ' ON m.id = ml.id_menu')
            ->select(null)
            ->select('m.id, m.name, m.status, ml.data, ml.options')
            ->where('m.name = ?', $this->name)
            ->where('m.status = 1')
            ->where('ml.id_lang = ?', $lang->getCurrentID());
        $rc = $query->fetch();
        if (empty($rc))
        {
            $res = [];
        } else
        {
            $res = json_decode($rc['data'], true);
        }
        $options = json_decode($rc['options'], true);
        $this->cssClass .= ' '.$options['class'];
        $this->cssId = $options['id'];
        return $res;
    }

    protected function prepare()
    {
        parent::prepare();
        $this->data = $this->getData();
    }

    public function write()
    {
       $this->renderTemplate('widget/menu');
    }

}
