<?php
$this->mapComponent(['GET'], 'System', ['resource/app.js' => 'ResourceController:appJS']);
$this->mapComponent(['GET'], 'System', ['resource/keepalive.js' => 'ResourceController:keepAliveJS']);
$this->mapComponent(['GET'], 'System', ['service/keepalive' => 'ServiceController:keepAlive']);