<?php

if (count($this->data) > 1)
{
    echo '<ol class="breadcrumb">';
    for ($i = 0; $i < count($this->data); $i++)
    {
        $el = $this->data[$i];
        if ($i < count($this->data) - 1)
        {
            $class = ($i == 0) ? $class = ' class="breadcrumb-root"' : '';
            printf('<li><a href="%s"' . $class . '>%s</a></li>', $this->view->encodeHtml($el['url']), $this->view->encodeHtml($el['title']));
        } else
        {
            printf('<li class="active">%s</li>', $this->view->encodeHtml($el['title']));
        }
    }
    echo '</ol>';
}