<?php
$this->renderList = function ($list, $level, $currentPath, $langPrefix)
{
    if (!empty($list))
    {
        $cl = '';
        $id = $this->cssId;
        if ($level == 0)
        {
            $cl = $this->cssClass;
        } else
        {
            if ($this->dropdown)
            {
                $cl = 'dropdown-menu';
            }
        }
        if($id==='')
        {
            printf('<ul class="%s">', $cl) . "\n";
        }
        else   
        {
            printf('<ul id="%s" class="%s">', $id, $cl) . "\n";
        }
        
        foreach ($list as $li)
        {
            $classLI = '';
            $extraA = '';
            if (!empty($li['children']))
            {
                $classLI .= 'dropdown';
                $extraA = sprintf(' aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"');
            }
            $path = $li['path'];
            if ($li['custom'])
            {
                if (empty($langPrefix))
                {
                    if (EOS\System\Routing\PathHelper::removeSlash($path) == $currentPath)
                    {
                        $classLI .= ' active';
                    }
                } else
                {
                    // Verifico che se un url interno lo collego al menu
                    if (!\EOS\System\Util\StringHelper::startsWith($path, 'http'))
                    {
                        $paus = EOS\System\Routing\PathHelper::removeSlash(EOS\System\Routing\PathHelper::removeExtension($path));
                        if (\EOS\System\Util\StringHelper::startsWith($paus, $langPrefix))
                        {
                            $paus = substr($paus, strlen($langPrefix));
                        }
                        $paus = EOS\System\Routing\PathHelper::removeSlash($paus);
                        if ($paus == EOS\System\Routing\PathHelper::removeSlash($currentPath))
                        {
                            $classLI .= ' active';
                        }
                    }
                }
            } else
            {
                if ($path == $currentPath)
                {
                    $classLI .= ' active';
                }
                if (!empty($path))
                {
                    $pa = \EOS\System\Routing\PathHelper::explodeSlash($path);
                    $cp = $pa[0];
                    array_shift($pa);
                    $path = $this->view->path->urlFor($cp, $pa);
                }
            }
            if (!empty($classLI))
            {
                $classLI = ' class="' . $classLI . '"';
            }
            printf("<li%s>", $classLI);
            printf('<a href="%s"%s>%s</a>', $this->view->encodeHtml($path), $extraA, $this->view->encodeHtml($li['title']));

            if (!empty($li['children']))
            {
                $method = $this->renderList;
                $method($li['children'], $level + 1, $this->currentPath, $this->langPrefix);
            }
            echo "</li>";
        }
        echo "</ul>\n";
    }
};
if (!empty($this->data) && (isset($this->data['menu'])))
{
    $this->currentPath = \EOS\System\Routing\PathHelper::removeSlash($this->view->request->getUri()->getPath());
    $this->langPrefix = '';
    if ($this->view->controller->container->get('currentParams')['multiLanguage'])
    {
        $this->langPrefix = $this->view->controller->container->get('language')->getCurrentISO();
    }
    $method = $this->renderList;
    $method($this->data['menu'], 0, $this->currentPath, $this->langPrefix);
}