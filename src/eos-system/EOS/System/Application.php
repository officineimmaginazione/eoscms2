<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\ArrayHelper;

class Application extends \Slim\App
{
    /*
      private static $instance = NULL;

      public static function getInstance()
      {

      if (!isset(self::$instance))
      {
      static::$instance = new Application();
      }
      return self::$instance;
      }
     */

    private $_prepared = false;
    private $_currentParams;
    private $_dispatcherManager;
    private $_queueMiddleware = [];
    private $_mapMiddleware = [];
    public $componentManager = null;
    public $componentsPath;
    public $componentsOverridePath;

    private function addRoutingFile($path, $pathOverride)
    {
        $this->_currentParentRoutingFile = '';
        $filename = PathHelper::resolveFileName($path, $pathOverride, 'routing.php');
        if (file_exists($filename))
        {
            Util\IncludeCode::sandboxIncludeOnce($filename, $this);
        }

        if (!empty($pathOverride) && (is_dir($pathOverride)))
        {
            $filename = $pathOverride . 'routing_child.php';
            if (file_exists($filename))
            {
                Util\IncludeCode::sandboxIncludeOnce($filename, $this);
            }
        }
    }

    public function __construct($settings = [], $container = [])
    {
        parent::__construct($container);
        $this->setSystemSettings();
        $c = $this->getContainer();
        $db = \EOS\System\Database\Database::newConnection();
        $c['database'] = $db;
        $this->_currentParams = new \Slim\Collection();
        $c['currentParams'] = $this->_currentParams;
        $this->_currentParams['pageExtension'] = isset($settings['pageExtension']) ? $settings['pageExtension'] : '/';
        $this->_currentParams['themesPath'] = isset($settings['themesPath']) ? $settings['themesPath'] : _EOS_PATH_APP_ . 'themes' . DIRECTORY_SEPARATOR;
        $this->_currentParams['themeName'] = isset($settings['themeName']) ? $settings['themeName'] : 'default';
        $this->_currentParams['themeTemplate'] = 'default';
        $this->_currentParams['dbRouter'] = isset($settings['dbRouter']) ? $settings['dbRouter'] : false;
        $this->_currentParams['multiLanguage'] = isset($settings['multiLanguage']) ? $settings['multiLanguage'] : true;
        $this->_currentParams['languageType'] = isset($settings['languageType']) ? $settings['languageType'] : null;
        $this->_currentParams['extraExtensions'] = isset($settings['extraExtensions']) ? $settings['extraExtensions'] : ['js', 'css', 'png', 'jpg'];
        $this->_currentParams['sessionCookieHttpOnly'] = isset($settings['sessionCookieHttpOnly']) ? $settings['sessionCookieHttpOnly'] : true;
        $this->_currentParams['sessionTimeOut'] = isset($settings['sessionTimeOut']) ? $settings['sessionTimeOut'] : 0;
        $this->_currentParams['languageDetect'] = ArrayHelper::getBool($settings, 'languageDetect', false);
        $this->_currentParams['themeUI'] = ArrayHelper::getArray($settings, 'themeUI');
        $c['language'] = new Language\Manager($c);
        $c['path'] = new \EOS\System\Routing\Path($c);
        if (isset($settings['profiler']) && _EOS_DB_PROFILER_)
        {
            $db->setProfiler($settings['profiler']);
        }
        $c['profiler'] = isset($settings['profiler']) ? $settings['profiler'] : null;
        $this->_dispatcherManager = new Event\DispatcherManager();
        $c['eventDispatcherManager'] = $this->_dispatcherManager;
    }

    public function __destruct()
    {
        $this->_dispatcherManager->getDispatcher()->dispatch(Events::TERMINATE, new Event\SystemEvent($this));
    }

    // Imposta i parametri di sistema (anche se predefiniti previene eventuali modifiche)
    public function setSystemSettings()
    {
        if ((PHP_MAJOR_VERSION == 5) && (PHP_MINOR_VERSION <= 5))
        {
            // PHP 5.5 MAMP non è impostato correttamente su UTF-8
            // mbstring.internal_encoding
            if (mb_internal_encoding() !== 'UTF-8')
            {
                mb_internal_encoding('UTF-8');
            }
        }
        ini_set('default_charset', 'utf-8');
    }

    public function getCurrentParams()
    {
        return $this->_currentParams;
    }

    protected function loadErrors($container)
    {
        if (_EOS_DEBUG_ === true)
        {
            $container->get('settings')['displayErrorDetails'] = true;
        }

        $container['errorHandler'] = function ($c)
        {
            return new \EOS\System\Handlers\Error($c->get('settings')['displayErrorDetails']);
        };

        $container['notFoundHandler'] = function ()
        {
            return new \EOS\System\Handlers\NotFound;
        };

        $container['notAllowedHandler'] = function ()
        {
            return new \EOS\System\Handlers\NotAllowed;
        };
    }

    protected function loadSession($container)
    {
        $container['session'] = new \EOS\System\Session\Session($container);
    }

    protected function loadDefaultRoutes()
    {
        // Default routes
        $this->componentsPath = _EOS_PATH_APP_ . 'EOS' . DIRECTORY_SEPARATOR . 'Components' . DIRECTORY_SEPARATOR;
        $this->componentsOverridePath = (defined('_EOS_PATH_APP_OVERRIDE_')) ? _EOS_PATH_APP_OVERRIDE_ . 'EOS' . DIRECTORY_SEPARATOR . 'Components' . DIRECTORY_SEPARATOR : '';
        $this->_currentParams['componentsPath'] = $this->componentsPath;
        $this->_currentParams['componentsOverridePath'] = $this->componentsOverridePath;
        $this->addRoutingFile($this->componentsPath, $this->componentsOverridePath);
    }

    protected function loadDefaultMiddleware($container)
    {
        //NB: I middleware vengono eseguiti in LIFO

        $app = $this;
        $this->addMiddlewareQueue(function (Request $request, Response $response, callable $next) use ($app, $container)
        {
            $uri = $request->getUri();
            $path = $uri->getPath();
            if ($path != '/')
            {
                $pathlist = PathHelper::explodeSlash($path);
                if (count($pathlist) > 0)
                {
                    if ($app->componentManager->componentExists($pathlist[0]))
                    {
                        $comp = $app->componentManager->getComponent($pathlist[0]);
                        $path = $app->componentsPath . $comp->name . DIRECTORY_SEPARATOR;
                        $pathOverride = empty($app->componentsOverridePath) ? '' : $app->componentsOverridePath . $comp->name . DIRECTORY_SEPARATOR;
                        $app->addRoutingFile($path, $pathOverride);
                    }
                }
            }
            return $next($request, $response);
        });

        // Middleware DB router
        if ($this->_currentParams['dbRouter'])
            $this->addMiddlewareQueue(new \EOS\System\Middleware\Router($container));
        // Middleware Lingue
        if ($this->_currentParams['multiLanguage'])
            $this->addMiddlewareQueue(new \EOS\System\Middleware\Language($container));
        // Middleware riconosce lingua
        if ($this->_currentParams['multiLanguage'] && $this->_currentParams['languageDetect'])
            $this->addMiddlewareQueue(new \EOS\System\Middleware\LanguageDetect($container));
        // Middleware extensione
        $this->addMiddlewareQueue(new \EOS\System\Middleware\Extension($container));
        if (_EOS_PROFILER_)
        {
            $this->addMiddlewareQueue(new \EOS\System\Middleware\Profiler($container));
        }
    }

    protected function loadLanguages($container)
    {
        // Carico le lingue dal database
        if ($this->_currentParams['languageType'] != null)
        {
            $container['language']->loadFromDB($this->_currentParams['languageType'] == 'site');
        }
    }

    protected function loadComponents($container)
    {
        $this->componentManager = new Component\Manager($this);
        $this->componentManager->load();
        $container['componentManager'] = $this->componentManager;
    }

    protected function loadQueueMiddlewarea()
    {
        foreach ($this->_queueMiddleware as $m)
        {
            $this->add($m);
        }

        $this->_queueMiddleware = [];
    }

    // Da richiamare se devo forzare dei middleware nella index
    public function prepare()
    {
        if (!$this->_prepared)
        {
            $container = $this->getContainer();
            $this->loadErrors($container);
            $this->loadSession($container);
            $this->loadDefaultRoutes();
            $this->loadDefaultMiddleware($container);
            $this->loadLanguages($container);
            $this->loadComponents($container);
            $this->loadQueueMiddlewarea();
            $this->_prepared = true;
        }
    }

    public function run($silent = false)
    {
        $this->prepare();
        return parent::run($silent);
    }

    public function debugPrintRoutes()
    {
        if (_EOS_DEBUG_ === true)
        {
            $router = $this->getContainer()->get('router');
            $rl = $router->getRoutes();
            foreach ($rl as $r)
            {
                $call = 'Closure';
                if (is_string($r->getCallable()))
                {
                    $call = $r->getCallable();
                }
                echo implode(', ', [$r->getName(), $r->getIdentifier(), json_encode($r->getMethods()), $r->getPattern(), $call]) . '<br>';
            }
        }
    }

    public function removeRoute($methods, $pattern)
    {
        $router = $this->getContainer()->get('router');
        $rl = $router->getRoutes();
        foreach ($rl as $r)
        {
            if (empty(array_diff($r->getMethods(), $methods)) && ($r->getPattern() === $pattern))
            {
                // Devo impostare un nome perché di default è vuoto
                $remname = 'remove_' . $r->getIdentifier();
                $r->setName($remname);
                $router->removeNamedRoute($remname);
                break;
            }
        }
    }

    public function removeRouteComponentController($component, $controller)
    {
        $router = $this->getContainer()->get('router');
        $rl = $router->getRoutes();
        $fullController = strtolower(sprintf('\EOS\Components\%s\Controllers\%s:', $component, $controller));
        foreach ($rl as $r)
        {
            if (is_string($r->getCallable()))
            {
                $call = strtolower($r->getCallable());
                if (Util\StringHelper::startsWith($call, $fullController))
                {
                    $remname = 'remove_' . $r->getIdentifier();
                    $r->setName($remname);
                    $router->removeNamedRoute($remname);
                }
            }
        }
    }

    public function replaceRouteComponentController($component, $controllerOld, $controllerNew)
    {
        $router = $this->getContainer()->get('router');
        $rl = $router->getRoutes();
        $fullControllerOld = strtolower(sprintf('\EOS\Components\%s\Controllers\%s:', $component, $controllerOld));
        $fullControllerNew = sprintf('\EOS\Components\%s\Controllers\%s:', $component, $controllerNew);
        foreach ($rl as $r)
        {
            if (is_string($r->getCallable()))
            {
                $call = strtolower($r->getCallable());
                if (Util\StringHelper::startsWith($call, $fullControllerOld))
                {
                    $r->setCallable($fullControllerNew . substr($call, strlen($fullControllerOld)));
                }
            }
        }
    }

    public function mapComponent($methods, $component, $actions, array $options = []): array
    {
        $res = [];
        foreach ($actions as $p1 => $controller)
        {
            $class = sprintf('\EOS\Components\%s\Controllers\%s', $component, $controller);
            $pathr = PathHelper::addTrailingSlash(PathHelper::addLeadingSlash(strtolower($component)));
            $p2 = PathHelper::removeTrailingSlash(PathHelper::removeLeadingSlash($p1));
            if ($p2 != '')
            {
                $pathr .= PathHelper::addTrailingSlash($p2);
            }
            $r = $this->map($methods, $pathr, $class);
            $r->mapOptions = $options;
            foreach ($this->_mapMiddleware as $m)
            {
                $r->add($m);
            }

            $res[] = $r;
        }
        return $res;
    }

    public function removeMapComponent($methods, $component, $actions)
    {

        foreach ($actions as $p1)
        {
            $pathr = PathHelper::addTrailingSlash(PathHelper::addLeadingSlash(strtolower($component)));
            $p2 = PathHelper::removeTrailingSlash(PathHelper::removeLeadingSlash($p1));
            if ($p2 != '')
            {
                $pathr .= PathHelper::addTrailingSlash($p2);
            }
            $this->removeRoute($methods, $pathr);
        }
    }

    public function addMiddlewareQueue($callable, $lifo = true)
    {
        if ($lifo)
        {
            $this->_queueMiddleware[] = $callable;
        } else
        {
            array_unshift($this->_queueMiddleware, $callable);
        }
    }

    public function addMapMiddleware($callable, $lifo = true)
    {
        if ($lifo)
        {
            $this->_mapMiddleware[] = $callable;
        } else
        {
            array_unshift($this->_mapMiddleware, $callable);
        }
    }

}
