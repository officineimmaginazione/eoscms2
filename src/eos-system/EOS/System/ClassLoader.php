<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System;

class ClassLoader
{

    public $mapper = [];
    public $mapper_override = [];
    public $libs = [];
    public $libsMapper = [];

    public function loadEvent($class)
    {
        $l = explode('\\', $class);
        if ((count($l) >= 2) && ($l[0] == 'EOS'))
        {
            $filename = '';
            if (isset($this->mapper_override[$l[1]]))
            {
                $filename = $this->mapper_override[$l[1]] . implode(DIRECTORY_SEPARATOR, $l) . '.php';
                if (file_exists($filename))
                {
                    include_once($filename);
                } else
                {
                    $filename = '';
                }
            }
            if ((empty($filename)) && (isset($this->mapper[$l[1]])))
            {
                $filename = $this->mapper[$l[1]] . implode(DIRECTORY_SEPARATOR, $l) . '.php';
                if (file_exists($filename))
                {
                    include_once($filename);
                }
            }
        } else
        {
            // Load Libs
            if (isset($this->libs[$class]))
            {
                $filename = $this->libs[$class];
                if (file_exists($filename))
                {
                    include_once($filename);
                }
            } else
            {
                // Load with namespace
                if ((count($l) > 0) && (isset($this->libsMapper[$l[0]])))
                {
                    $filename = $this->libsMapper[$l[0]] . implode(DIRECTORY_SEPARATOR, $l) . '.php';
                    if (file_exists($filename))
                    {
                        include_once($filename);
                    }
                }
            }
        }
    }

}
