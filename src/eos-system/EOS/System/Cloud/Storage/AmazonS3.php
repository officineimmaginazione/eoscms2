<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

/*
 * https://github.com/aws/aws-sdk-php
 * Creazione utenti: http://docs.aws.amazon.com/AmazonS3/latest/dev/walkthrough1.html
 * Client: http://docs.aws.amazon.com/aws-sdk-php/v3/api/class-Aws.S3.S3ClientInterface.html
 */

use EOS\System\Routing\PathHelper;
use EOS\System\Cloud\Storage\StorageACL;
use EOS\System\Util\ArrayHelper;

class AmazonS3 extends Storage
{

    private $_bucket = '';
    private $_client = null;

    private function aclConvert(string $value): string
    {
        switch ($value)
        {
            case StorageACL::PRIVATE_ACCESS:
                $res = 'private';
                break;
            case StorageACL::PUBLIC_READ:
                $res = 'public-read';
                break;
            case StorageACL::PUBLIC_READ_WRITE:
                $res = 'public-read-write';
                break;
            default:
                $res = '';
                break;
        }

        return $res;
    }

    private function addACL(array &$options, string $acl)
    {
        if ($acl !== StorageACL::PARENT)
        {
            $options['ACL'] = $this->aclConvert($acl);
        }
    }

    private function getConverterObjectProc()
    {
        $proc = function ()
        {
            if (!empty($this->objectSource['@metadata']))
            {
                $metadata = $this->objectSource['@metadata'];
                $this->lastModified = $this->objectSource['LastModified'];
                $this->url = $metadata['effectiveUri'];
                $this->size = (int) $this->objectSource['ContentLength'];
                $this->contentType = $this->objectSource['ContentType'];
            } else
            {
                $this->key = $this->objectSource['Key'];
                $this->size = (int) $this->objectSource['Size'];
                $this->lastModified = $this->objectSource['LastModified'];
                $this->url = isset($this->objectSource['ObjectURL']) ? $this->objectSource['ObjectURL'] : '';
            }
        };
        return $proc;
    }

    private function getParams(string $key, array $options = []): array
    {
        $options['Bucket'] = $this->_bucket;
        $options['Key'] = $this->filterKeyName($key);
        return $options;
    }

    private function internalListObjects(string $prefix, array $options): \Iterator
    {
        $requestPrefix = $this->filterFolderName($prefix);
        $params = ['Bucket' => $this->_bucket,
            'Prefix' => $requestPrefix,
            'Delimiter' => '/'];
        if (isset($options['maxkeys']))
        {
            $params['MaxKeys'] = (int) $options['maxkeys'];
        }
        $results = $this->_client->getPaginator('ListObjects', $params);
        // Verifico e genero un'eccezione nel caso punto a una chiave che non esiste
//        foreach ($results as $result)
//        {
//            if (isset($result['Contents']))
//            {
//                break;
//            } else
//            {
//                // Genero un errore se non esistono risorse
//                throw new \Exception('AmazonS3 - Prefix "' . $prefix . '" not found! ');
//            }
//        }
        // Devo escludere me stesso dalla lista
        // http://jmespath.org/tutorial.html#filter-projections
        // Faccio una falsa conversione in JSON e recupero il valore restituito
        $t1 = ArrayHelper::toJSON([$this->filterFolderName($requestPrefix)]);
        $t2 = substr(substr($t1, 2), 0, -2);
        // Converto il carattere di escape così può essere utilizzato senza problemi
        $filterRes = str_replace('`', '\`', $t2);
        $iterator = $results->search('Contents[?Key != `' . $filterRes . '`]');

        return $iterator;
    }

    // https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_configuration.html   
    /*
      Parametri:
      [
      'region' => 'eu-west-1','version' => 'latest',
      'credentials' => ['key' => 'Chiave', 'secret' => 'Chiave segreta'],
      ]
     */

    protected function load()
    {
        parent::load();
        $this->_client = new \Aws\S3\S3Client($this->config);
    }

    // StorageInterface - Begin 

    public function setBucket(string $name): bool
    {
        $this->_bucket = $name;
        return $this->doesBucketExist($name);
    }

    public function getBucket(): string
    {
        return $this->_bucket;
    }

    // Devo avere i diritti a vedere tutti i bucket
    public function listBuckets(): array
    {
        $buckets = $this->_client->listBuckets();
        $res = [];
        foreach ($buckets['Buckets'] as $b)
        {
            $res[] = $b['Name'];
        }
        return $res;
    }

    public function doesBucketExist(string $name): bool
    {
        $result = $this->_client->doesBucketExist($name);
        return $result;
    }

    public function createFolder(string $key, string $acl = StorageACL::PARENT): StorageObject
    {
        return $this->putObject($this->filterFolderName($key), '', $acl);
    }

    public function isEmptyFolder(string $key): bool
    {
        return $this->listObjects($this->filterFolderName($key), ['maxkeys' => 1])->valid() === false;
    }

    /*
     * NB: S3 se le cartelle sono vuote le cancella tutte a cascata!
     */

    public function deleteFolder(string $key, bool $recursive = false)
    {
        $result = null;
        $fKey = $this->filterFolderName($key);
        if ($this->isFolderKey($fKey))
        {
            if (!$recursive)
            {
                if (!$this->isEmptyFolder($fKey))
                {
                    throw new \Exception('The folder object is not empty!');
                }
            }
            $obj = $this->_client->deleteObject($this->getParams($fKey));

            if (!is_null($obj))
            {
                $result = new StorageObject($obj, $this->getConverterObjectProc());
                $result->key = $fKey; // Non mi arriva come risultato
            }
        }
        return $result;
    }

    public function putFile(string $key, string $sourceFile, string $acl = StorageACL::PARENT): StorageObject
    {
        $result = null;
        $params = $this->getParams($key, ['SourceFile' => $sourceFile]);
        $this->addACL($params, $acl);
        $obj = $this->_client->putObject($params);
        if (!is_null($obj))
        {
            $result = new StorageObject($obj, $this->getConverterObjectProc());
            $result->key = $key; // Non mi arriva come risultato
        }
        return $result;
    }

    public function putObject(string $key, $body, string $acl = StorageACL::PARENT): StorageObject
    {
        $result = null;
        $params = $this->getParams($key, ['Body' => $body]);
        $this->addACL($params, $acl);
        $obj = $this->_client->putObject($params);
        if (!is_null($obj))
        {
            $result = new StorageObject($obj, $this->getConverterObjectProc());
            $result->key = $key; // Non mi arriva come risultato
        }
        return $result;
    }

    public function getObjectBody(string $key): string
    {
        return $this->getObjectStream($key);
    }

    public function getObjectStream(string $key): object
    {
        $obj = $this->_client->getObject($this->getParams($key));
        return $obj->get('Body');
    }

    public function getFile(string $key, string $destinationFile)
    {
        $this->_client->getObject($this->getParams($key, [
                'SaveAs' => $destinationFile
        ]));
    }

    /*
      'options' => [
      'presigned_request' => 'true o false, usa gli url con scadenza',
      'presigned_expires' => 'Secondi di scadenza dell'url'
      ]
     */

    public function getObjectUrl(string $key, array $options = []): string
    {
        $presignedRequest = ArrayHelper::getBool($options, 'presigned_request', false);
        $presignedExpires = ArrayHelper::getInt($options, 'presigned_expires', 0);
        if ($presignedRequest)
        {
            // https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-presigned-url.html
            $cmd = $this->_client->getCommand('GetObject', $this->getParams($key));
            $request = $this->_client->createPresignedRequest($cmd, '+' . $presignedExpires . ' seconds');
            return $request->getUri();
        } else
        {
            return $this->_client->getObjectUrl($this->_bucket, $this->filterKeyName($key));
        }
    }

    public function headObject(string $key): ?StorageObject
    {
        $result = null;
        $obj = $this->_client->headObject($this->getParams($key));

        if (!is_null($obj))
        {
            $result = new StorageObject($obj, $this->getConverterObjectProc());
            $result->key = $key; // Non mi arriva come risultato
        }
        return $result;
    }

    public function deleteObject(string $key)//: ?StorageObject; PHP 7.2 richiesto
    {
        $result = null;
        $fKey = $this->filterKeyName($key);
        if ($this->isFolderKey($fKey))
        {
            $result = $this->deleteFolder($fKey, false);
        } else
        {
            $obj = $this->_client->deleteObject($this->getParams($fKey));

            if (!is_null($obj))
            {
                $result = new StorageObject($obj, $this->getConverterObjectProc());
                $result->key = $fKey; // Non mi arriva come risultato
            }
        }
        return $result;
    }

    public function copyObject(string $keySource, string $keyDest, string $acl = StorageACL::PARENT): StorageObject
    {
        $result = null;
        $obj = $this->_client->copy($this->_bucket, $keySource, $this->_bucket, $keyDest, $this->aclConvert($acl));
        if (!is_null($obj))
        {
            $result = new StorageObject($obj, $this->getConverterObjectProc());
            $result->key = $keyDest; // Non mi arriva come risultato
        }
        return $result;
    }

    // http://docs.aws.amazon.com/AmazonS3/latest/dev/ListingObjectKeysUsingPHP.html
    // Importantissimo mettere il delimiter altrimenti legge in modo ricorsivo anche le sotto cartelle
    // se metto '/' non vedo le cartelle come prefix
    // devo emulare il filesystem e quindi forzo che funzioni solo su cartelle
    /* Options:
      'options' => [
      'maxkeys' => 'numero limite di lettura',
      ]
     */
    public function listObjects(string $prefix, array $options = []): StorageObjectList
    {
        $iterator = $this->internalListObjects($prefix, $options);
        $result = new StorageObjectList($iterator, $this->getConverterObjectProc());
        return $result;
    }

    // Da usare su risultati molto grandi e dovrebbe consumare meno memoria
    public function eachObjects(string $prefix, \Closure $eachProc, array $options = [])
    {
        $iterator = $this->internalListObjects($prefix, $options);
        foreach ($iterator as $object)
        {
            if ($eachProc(new StorageObject($object, $this->getConverterObjectProc())) === false)
            {
                return;
            }
        }
    }

    public function doesObjectExist(string $key): bool
    {
        return $this->_client->doesObjectExist($this->_bucket, $key);
    }

    public function getCapabilities(): StorageCapabilities
    {
        $sc = new StorageCapabilities();
        $sc->presigned = true;
        $sc->acl = true;
        $sc->maxkeys = true;
        $sc->recursiveDeleteFolder = true;
        return $sc;
    }

    // StorageInterface - End 
}
