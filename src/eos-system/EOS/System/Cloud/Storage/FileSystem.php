<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

use EOS\System\Routing\PathHelper;
use EOS\System\Cloud\Storage\StorageACL;
use EOS\System\Util\ArrayHelper;

class FileSystem extends Storage
{

    private $_path = '';
    private $_url = '';
    private $_bucket = '';
    private $_bucketPath = '';
    private $_useBucket = true;

    private function prepareBucket()
    {
        if ((strlen($this->_bucket) === 0) && ($this->_useBucket === true))
        {
            throw new \Exception('Bucket empty!');
        }

        $this->_bucketPath = PathHelper::addDirSep($this->_path . $this->_bucket);
    }

    private function getRealPath(string $key): string
    {
        if (strlen($this->_bucketPath) === 0)
        {
            $this->prepareBucket();
        }
        $path = $this->_bucketPath . PathHelper::implodeDirSep(PathHelper::explodeSlash($this->filterKeyName($key)));
        return $path;
    }

    private function getConverterObjectProc()
    {
        $bucketPath = $this->_bucketPath;
        $url = $this->_url;
        $bucket = $this->_bucket;
        $proc = function () use ($bucketPath, $url, $bucket)
        {
            $this->key = str_replace(DIRECTORY_SEPARATOR, '/', mb_substr($this->objectSource->getPathname(), mb_strlen($bucketPath)));
            if ($this->objectSource->isDir())
            {
                $this->key = PathHelper::addTrailingSlash($this->key);
            }
            $this->lastModified = (new \DateTimeImmutable())->setTimestamp($this->objectSource->getMTime());
            $this->size = $this->objectSource->getSize();
            $this->contentType = mime_content_type($this->objectSource->getPathname());
            $this->url = PathHelper::addTrailingSlash($url . $bucket) . $this->key;
        };

        return $proc;
    }

    private function createStorageObject(string $filename)
    {
        if (file_exists($filename))
        {
            return new StorageObject(new \SplFileInfo($filename), $this->getConverterObjectProc());
        } else
        {
            return null;
        }
    }

    /*
      [
      'path' => 'Percorso fisico dei buckets
      'url' => 'Url dei buckets'
      ]

     */

    protected function load()
    {
        parent::load();
        $this->_useBucket = ArrayHelper::getBool($this->config, 'usebucket', true);
        if (!isset($this->config['path']))
        {
            throw new \Exception('Invalid path!');
        }

        if (!file_exists($this->config['path']))
        {
            throw new \Exception('Path not found!');
        }
        $this->_path = PathHelper::addDirSep($this->config['path']);
        if (isset($this->config['url']))
        {
            $this->_url = PathHelper::addTrailingSlash($this->config['url']);
        }
    }

    // StorageInterface - Begin

    public function setBucket(string $name): bool
    {
        $this->_bucket = $name;
        $this->prepareBucket();
        return $this->doesBucketExist($name);
    }

    public function getBucket(): string
    {
        return $this->_bucket;
    }

    public function listBuckets(): array
    {
        $res = [];
        $list = new \FilesystemIterator($this->_path, \FilesystemIterator::SKIP_DOTS);
        foreach ($list as $b)
        {
            if ($b->isDir())
            {
                $res[] = $b->getFilename();
            }
        }
        return $res;
    }

    public function doesBucketExist(string $name): bool
    {
        return file_exists($this->_path . PathHelper::addDirSep($name));
    }

    public function createFolder(string $key, string $acl = StorageACL::PARENT): StorageObject
    {
        return $this->putObject($this->filterFolderName($key), '', $acl);
    }

    public function isEmptyFolder(string $key): bool
    {
        return $this->listObjects($this->filterFolderName($key))->valid() === false;
    }

    public function deleteFolder(string $key, bool $recursive = false)
    {
        $fKey = $this->filterKeyName($key);
        $realName = $this->getRealPath($fKey);
        $obj = null;
        if (is_dir($realName))
        {
            $obj = $this->createStorageObject($realName);
            rmdir($realName);
        }
        return $obj;
    }

    public function putFile(string $key, string $sourceFile, string $acl = StorageACL::PARENT): StorageObject
    {
        return $this->putObject($this->filterKeyName($key), fopen($sourceFile, 'r'), $acl);
    }

    public function putObject(string $key, $body, string $acl = StorageACL::PARENT): StorageObject
    {
        $realName = $this->getRealPath($key);
        // Se è vuoto il body e finisco con la slash è una cartella
        if ((empty($body) && (substr($key, -1) == '/')))
        {
            if (!file_exists($realName))
            {
                mkdir($realName, 0777, true);
            }
        } else
        {
            // Devo creare le cartelle che contengolo l'oggetto in modo ricorsivo
            $dir = dirname($realName);
            if (!file_exists($dir))
            {
                mkdir($dir, 0777, true);
            }
            file_put_contents($realName, $body);
        }
        return $this->createStorageObject($realName);
    }

    public function getObjectBody(string $key): string
    {
        return file_get_contents($this->getRealPath($key));
    }

    public function getObjectStream(string $key): object
    {
        $fh = fopen($this->getRealPath($key), 'r');
        return new \Slim\Http\Stream($fh);
    }

    public function getFile(string $key, string $destinationFile)
    {
        copy($this->getRealPath($key), $destinationFile);
    }

    public function getObjectUrl(string $key, array $options = []): string
    {
        return $this->headObject($this->filterKeyName($key))->url;
    }

    public function headObject(string $key): ?StorageObject
    {
        return $this->createStorageObject($this->getRealPath($key));
    }

    public function deleteObject(string $key) //: ?StorageObject; PHP 7.2 richiesto
    {
        $fKey = $this->filterKeyName($key);
        if ($this->isFolderKey($fKey))
        {
            $obj = $this->deleteFolder($fKey, false);
        } else
        {
            $obj = null;
            $realName = $this->getRealPath($fKey);
            if (file_exists($realName))
            {
                $obj = $this->createStorageObject($this->getRealPath($fKey));
                unlink($realName);
            }
        }

        return $obj;
    }

    public function copyObject(string $keySource, string $keyDest, string $acl = StorageACL::PARENT): StorageObject
    {
        copy($this->getRealPath($keySource), $this->getRealPath($keyDest));
        return $this->createStorageObject($this->getRealPath($keyDest));
    }

    public function listObjects(string $prefix, array $options = []): StorageObjectList
    {
        $fName = $this->getRealPath($this->filterFolderName($prefix));
        if (is_dir($fName))
        {
            $list = new \FilesystemIterator($fName, \FilesystemIterator::SKIP_DOTS);
        } else
        {
            $list = new \ArrayIterator([]);
        }
        $result = new StorageObjectList($list, $this->getConverterObjectProc());
        return $result;
    }

    public function eachObjects(string $prefix, \Closure $eachProc, array $options = [])
    {
        foreach ($this->listObjects($prefix, $options) as $o)
        {
            if ($eachProc($o) === false)
            {
                return;
            }
        }
    }

    public function doesObjectExist(string $key): bool
    {
        return file_exists($this->getRealPath($key));
    }

    public function getCapabilities(): StorageCapabilities
    {
        $sc = new StorageCapabilities();
        $sc->presigned = false;
        return $sc;
    }

    // StorageInterface - End 
}
