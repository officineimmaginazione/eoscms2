<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;

abstract class Storage implements StorageInterface
{

    protected $config = [];

    // S3 accetta anche caratteri con punti ecc... 
    // Usando il FileSystem per motivi di sicurezza non posso accettare i ".." e cose strane per evitare l'escalation
    // quindi eseguo una conversione

    protected function filterKeyName(string $key): string
    {
        $res = str_replace(['../', '\\', './'], ['__/', '_', '_/'], $key);
        return $res;
    }

    protected function filterFolderName(string $key): string
    {
        return PathHelper::addTrailingSlash(PathHelper::removeLeadingSlash($this->filterKeyName($key)));
    }

    protected function isFolderKey(string $key): bool
    {
        return StringHelper::endsWith($key, '/');
    }

    protected function load()
    {
        
    }

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->load();

        // Se ho la chiave bucket nella configurazione lo imposto
        $bk = \EOS\System\Util\ArrayHelper::getStr($this->config, 'bucket');
        if (strlen($bk) > 0)
        {
            $this->setBucket($bk);
        }
    }

}
