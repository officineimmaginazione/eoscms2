<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

final class StorageACL
{

    const PARENT = '';
    const PRIVATE_ACCESS =
    'private';
    const PUBLIC_READ =
    'public-read';
    const PUBLIC_READ_WRITE =
    'public-read-write';
    }

    