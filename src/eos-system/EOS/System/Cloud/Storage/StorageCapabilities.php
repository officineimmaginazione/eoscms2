<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

final class StorageCapabilities
{

    public $presigned = false;
    public $maxkeys = false;
    public $acl = false;
    public $recursiveDeleteFolder = false;

}
