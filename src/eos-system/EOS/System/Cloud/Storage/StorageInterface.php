<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

use EOS\System\Cloud\Storage\StorageACL;

interface StorageInterface
{

    public function setBucket(string $name): bool;

    public function getBucket(): string;

    public function listBuckets(): array;

    public function doesBucketExist(string $name): bool;

    public function createFolder(string $key, string $acl = StorageACL::PARENT): StorageObject;
    
    public function isEmptyFolder(string $key): bool;
    
    public function deleteFolder(string $key, bool $recursive = false);

    public function putFile(string $key, string $sourceFile, string $acl = StorageACL::PARENT): StorageObject;

    public function putObject(string $key, $body, string $acl = StorageACL::PARENT): StorageObject;

    public function getObjectBody(string $key): string;

    public function getObjectStream(string $key): object;

    public function getFile(string $key, string $destinationFile);

    public function getObjectUrl(string $key, array $options = []): string;

    public function headObject(string $key): ?StorageObject;

    public function deleteObject(string $key); //: ?StorageObject; PHP 7.2 richiesto

    public function copyObject(string $keySource, string $keyDest, string $acl = StorageACL::PARENT): StorageObject;

    public function listObjects(string $prefix, array $options = []): StorageObjectList;
    
    public function eachObjects(string $prefix, \Closure $eachProc, array $options = []);

    public function doesObjectExist(string $key): bool;

    public function getCapabilities(): StorageCapabilities;
}
