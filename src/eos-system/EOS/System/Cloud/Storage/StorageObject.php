<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

class StorageObject
{

    public $key = '';
    public $size = 0;
    public $lastModified = null;
    public $contentType = '';
    public $url = '';
    public $objectSource = null;

    public function __construct($objectSource, \Closure $converter)
    {
        if ((!empty($objectSource)) && ($converter instanceof \Closure))
        {
            $this->objectSource = $objectSource;
            $bound = \Closure::bind($converter, $this);
            $bound();
        }
    }
    
    public function isFolderKey()
    {
        return substr($this->key, -1) === '/'; 
    }

}
