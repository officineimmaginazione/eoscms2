<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Cloud\Storage;

class StorageObjectList implements \Iterator
{

    private $_source;
    private $_converter;

    public function __construct(\Iterator $source, \Closure $converter)
    {
        $this->_source = $source;
        $this->_converter = $converter;
    }

    public function current()
    {
        $res = $this->_source->current();
        if (is_null($res))
        {
            return null;
        } else
        {
            return new StorageObject($res, $this->_converter);
        }
    }

    public function key()
    {
        return $this->_source->key();
    }

    public function next()
    {
        $this->_source->next();
    }

    public function rewind()
    {
        $this->_source->rewind();
    }

    public function valid()
    {
        return $this->_source->valid();
    }

}
