<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Component;

class Component
{

    protected $application;
    public $name;

    protected function getContainer()
    {
        return $this->application->getContainer();
    }

    protected function getEventDispatcher($name = null)
    {
        return $this->getContainer()->get('eventDispatcherManager')->getDispatcher($name);
    }

    public function __construct($application, $name)
    {
        $this->application = $application;
        $this->name = $name;
    }

    // Metodo richiamato all'avvio dell'applicazione
    public function load()
    {
        
    }

    // Metodo richiamato all'avvio di un controller del componente
    public function loadController($controller)
    {
        
    }

    public function isActive()
    {
        return $this->getContainer()->get('database')->setting->getInt(strtolower($this->name), 'dbversion') > 0;
    }

}
