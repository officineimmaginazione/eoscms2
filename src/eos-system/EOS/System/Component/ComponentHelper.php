<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Component;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\IncludeCode;

class ComponentHelper
{

    public static function includeRoot(object $container, string $name, object $currentThis, bool $child = true)
    {
        $curr = $container->get('currentParams');
        $componentsPath = $curr['componentsPath'];
        $componentsOverridePath = $curr['componentsOverridePath'];
        $cm = $container->get('componentManager');
        $components = $cm->getComponentList();
        $fList = [$name.'.php'];
        if ($child)
        {
            $fList[] = $name.'_child.php';
        }
        foreach ($components as $comp)
        {
            foreach ($fList as $fName)
            {
                $file = PathHelper::addDirSep($comp->name) . $fName;
                $file = PathHelper::resolveFileName($componentsPath, $componentsOverridePath, $file);
                if (file_exists($file))
                {
                    IncludeCode::sandboxIncludeOnce($file, $currentThis);
                }
            }
        }
    }

}
