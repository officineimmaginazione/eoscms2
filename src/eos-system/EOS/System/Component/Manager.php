<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Component;

use \EOS\System\Routing\PathHelper;

class Manager
{

    private $_application;
    private $_currentParams;
    private $components;

    public function load()
    {
        $this->components = [];
        $compPath = $this->_currentParams['componentsPath'];
        $compOvPath = $this->_currentParams['componentsOverridePath'];
        $isValidOv = (!empty($compOvPath)) && (is_dir($compOvPath));
        $fsList = PathHelper::resolveComponentList($compPath, $compOvPath);
        foreach ($fsList as $fsComp)
        {
            $class = sprintf('\EOS\Components\%s\Component', $fsComp);
            if ($isValidOv)
            {
                $classChild = sprintf('\EOS\Components\%s\ComponentChild', $fsComp);
                if (class_exists($classChild))
                {
                    $class = $classChild;
                }
            }

            if (!class_exists($class))
            {
                $class = '\EOS\System\Component\Component';
            }
            $comp = new $class($this->_application, $fsComp);
            $this->components[strtolower($fsComp)] = $comp;
        }
        // Eseguo il load dopo per avere l'elenco dei componenti già disponibile
        foreach ($this->components as $comp)
        {
            $comp->load();
        }
    }

    public function __construct($application)
    {
        $this->_application = $application;
        $this->_currentParams = $application->getContainer()['currentParams'];
        $this->components = [];
    }

    public function componentExists($name)
    {
        return (isset($this->components[strtolower($name)]));
    }

    public function getComponent($name)
    {
        if (!$this->componentExists($name))
        {
            throw new \Exception(sprintf('Component %s not exists!', $name));
        }
        return $this->components[strtolower($name)];
    }
    
    public function getComponentList(): array
    {
        return array_values($this->components);
    }

}
