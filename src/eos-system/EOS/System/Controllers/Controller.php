<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Controllers;

class Controller
{

    public $component = null;
    public $container = null;
    public $currentParams = null;
    public $session = null;
    public $path = null;
    public $lang = null;

    protected function loadParams()
    {
        $list = explode('\\', (new \ReflectionObject($this))->getName());
        $componentName = $list[count($list) - 3];
        $this->currentParams['componentPath'] = $this->currentParams['componentsPath'] . $componentName . DIRECTORY_SEPARATOR;
        $this->currentParams['componentName'] = $componentName;
        $this->currentParams['componentTemplatesPath'] = $this->currentParams['componentPath'] . 'templates' . DIRECTORY_SEPARATOR;
        if (empty($this->currentParams['componentsOverridePath']))
        {
            $this->currentParams['componentOverridePath'] = '';
            $this->currentParams['componentOverrideName'] = '';
            $this->currentParams['componentOverrideTemplatesPath'] = '';
        } else
        {
            $this->currentParams['componentOverridePath'] = $this->currentParams['componentsOverridePath'] . $componentName . DIRECTORY_SEPARATOR;
            $this->currentParams['componentOverrideName'] = $componentName;
            $this->currentParams['componentOverrideTemplatesPath'] = $this->currentParams['componentOverridePath'] . 'templates' . DIRECTORY_SEPARATOR;
        }
    }

    protected function loadComponent()
    {
        // Carico il componente e richiamo il load controller
        $this->component = $this->container->get('componentManager')->getComponent($this->currentParams['componentName']);
        $this->component->loadController($this);
        
        $this->getEventDispatcher()->dispatch(\EOS\System\Events::CONTROLLER, new \EOS\System\Event\SystemEvent($this));   
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->session = $container->get('session');
        $curr = $this->container->get('currentParams');
        $this->currentParams = $curr;
        $this->lang = $container->get('language');
        $this->path = $this->container->get('path');
        $this->loadParams();
        $this->loadComponent();
    }

    public function newView($request, $response, $classView = '')
    {
        if ($classView == '')
            $v = new \EOS\System\Views\Html($this, $request, $response);
        else
            $v = new $classView($this, $request, $response);
        return $v;
    }

    public function getEventDispatcher($name = null)
    {
        return $this->container->get('eventDispatcherManager')->getDispatcher($name);
    }

}
