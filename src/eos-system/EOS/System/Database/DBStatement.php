<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Database;

class DBStatement extends \PDOStatement
{

    protected $database;
    private $_profParams = [];

    protected function __construct($database)
    {
        $this->database = $database;
    }

    private function paramTypeToStr($const)
    {
        $class = new \ReflectionClass('EOS\System\Database\Database');
        foreach ($class->getConstants() as $k => $v)
        {
            if ((substr($k, 0, 6) == 'PARAM_') && ($v == $const))
            {
                return substr($k, 6);
            }
        }
        return '';
    }

    private function addProfParams($parameter, &$value, $data_type)
    {
        if ($this->database->isActiveProfiler())
        {
            $b = $parameter . '=';
            switch ($data_type)
            {
                case Database::PARAM_INT:
                case Database::PARAM_STR:
                    $b .= $value;
                    break;
                default:
                    break;
            }

            $this->_profParams[] = sprintf('%s(%s)', $b, $this->paramTypeToStr($data_type));
        }
    }

    public function execute($input_parameters = null)
    {
        $p = $this->database->startProfiler();
        $r = parent::execute($input_parameters);
        if ($p)
        {
            $d = $this->queryString;
            if (count($this->_profParams) > 0)
            {
                $d .= ' {' . implode(' | ', $this->_profParams) . '}';
            }
            $this->database->stopProfiler($d);
        }
        return $r;
    }

    public function bindValue($parameter, $value, $data_type = Database::PARAM_STR)
    {
        $this->addProfParams($parameter, $value, $data_type);
        return parent::bindValue($parameter, $value, $data_type);
    }

    public function bindParam($parameter, &$variable, $data_type = Database::PARAM_STR, $length = null, $driver_options = null)
    {
        $this->addProfParams($parameter, $variable, $data_type);
        parent::bindParam($parameter, $variable, $data_type, $length, $driver_options);
    }

}
