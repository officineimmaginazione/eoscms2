<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Database;

class Database extends \PDO
{

    private static $_prefix = '#__';
    private $_profiler = null;
    public $params;
    public $util;
    public $driverName = '';

    public static function newConnection()
    {
        return new Database(_EOS_DB_DSN_, _EOS_DB_USER_, _EOS_DB_PASS_);
    }

    public function __construct($dsn, $username = null, $password = null, $driver_options = null)
    {
        $charset = '';
        if (\EOS\System\Util\StringHelper::startsWith($dsn, 'mysql'))
        {
            //  Devo impostare il charset o driver init attr prima altrimenti non prende il charsert
            $driver_options[\PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'";
            $charset = ';charset=utf8mb4'; // Rafforza il comando sopra
        }
        parent::__construct($dsn . $charset, $username, $password, $driver_options);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception 
        $this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('EOS\System\Database\DBStatement', array($this)));
        // Crea risultato campo=valore e non 1 valore per posizione e uno per campo
        $this->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $this->defaultDriverSettings();
        $this->setting = new \EOS\System\Database\Setting($this, 'setting');
        $this->util = new \EOS\System\Database\Util($this);
    }

    private function defaultDriverSettings()
    {
        $this->driverName = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
        switch ($this->driverName)
        {
            case 'mysql':
                // $this->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'");

                break;
            case 'sqlite':
                //$this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, 0);
                $this->exec("PRAGMA foreign_keys = ON;");
                //$this->exec('PRAGMA encoding = "UTF-8"'); // Default utf-8
                break;
        }
    }

    public function startProfiler()
    {
        if ($this->_profiler != null)
        {
            $this->_profiler->start();
        }
        return $this->_profiler != null;
    }

    public function stopProfiler($str)
    {
        if ($this->_profiler != null)
        {
            $this->_profiler->stop($str);
        }
    }

    public function isActiveProfiler()
    {
        return $this->_profiler != null;
    }

    public function tableFix($sql)
    {
        return str_replace(static::$_prefix, _EOS_DB_PREFIX_, $sql);
    }

    public function tableExists($tablename)
    {
        switch ($this->driverName)
        {
            case 'sqlite':
                return $this->query("PRAGMA table_info(" . $tablename . ")")->fetch() != null;
              //  break;
            default:
                 return $this->query("SHOW TABLES LIKE '" . $tablename . "'")->fetch() != null;
              //  break;
        }
    }

    public function exec($statement)
    {
        $this->startProfiler();
        $r = parent::exec($this->tableFix($statement));
        $this->stopProfiler($statement);
        return $r;
    }

    public function lastInsertId($name = null)
    {
        return parent::lastInsertId($this->tableFix($name));
    }

    public function query($statement)
    {
        $this->startProfiler();
        $r = parent::query($this->tableFix($statement));
        $this->stopProfiler($statement);
        return $r;
    }

    public function quote($string, $parameter_type = Database::PARAM_STR)
    {
        return parent::quote($this->tableFix($string), $parameter_type);
    }

    public function prepare($statement, $driver_options = [])
    {
        return parent::prepare($this->tableFix($statement), $driver_options);
    }

    public function setProfiler($profiler)
    {
        $this->_profiler = $profiler;
    }

    public function newFluent()
    {
        return new \FluentPDO($this);
    }

}
