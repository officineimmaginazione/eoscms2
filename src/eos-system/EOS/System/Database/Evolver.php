<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Database;

class Evolver
{

    public $db;
    public $section = 'system';
    public $key = 'dbversion';
    private $list = [];
    public $logEvent = null;

    public function __construct()
    {
        $this->db = Database::newConnection();
    }

    public function writeLog($value)
    {
        if ($this->logEvent instanceof \Closure)
        {
            $bound = $this->logEvent;
            $bound($value);
        }
    }

    public function getVersion()
    {
        $res = $this->db->setting->getInt($this->section, $this->key);
        return (int) $res;
    }

    public function setVersion($value)
    {
        $this->db->setting->setInt($this->section, $this->key, (int) $value);
    }

    public function add($version, $callable)
    {
        if ($callable instanceof \Closure)
        {
            $this->list[$version] = $callable;
        }
        return $this;
    }

    public function execute()
    {
        $countUpdate = 0;
        foreach ($this->list as $version => $callable)
        {
            if ($this->getVersion() < $version)
            {
                $countUpdate++;
                $this->writeLog($version . ' - start');
                $this->db->beginTransaction();
                try
                {
                    $bound = \Closure::bind($callable, $this);
                    $bound();
                    $this->setVersion($version);
                    $this->db->commit();
                } catch (\Exception $e)
                {
                    $this->writeLog($version . ' - error: ' . $e->getMessage());
                    // Le DDL su MySQL sono in implicit commit (scrive sempre) !!! 
                    //  http://php.net/manual/en/pdo.rollback.php
                    // http://dev.mysql.com/doc/refman/5.7/en/implicit-commit.html
                    $this->db->rollBack();
                    throw $e;
                }
                $this->writeLog($version . ' - end');
            }
        }
        if (empty($this->list))
        {
            $this->writeLog('No items');
        } else
        {
            $this->writeLog($this->db->driverName . ' | ' . $this->section . ' | Update: ' . $countUpdate);
        }
    }

    public function createDefaultTables()
    {
        switch ($this->db->driverName)
        {
            case 'sqlite':
                if (!$this->db->tableExists('#__setting'))
                {
                    $this->db->exec(
                        'CREATE TABLE IF NOT EXISTS  #__setting ( 
                    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    object varchar(150) NOT NULL COLLATE NOCASE, 
                    name varchar(150) NOT NULL COLLATE NOCASE,
                    value mediumtext NOT NULL COLLATE NOCASE);');
                    $this->db->exec('CREATE UNIQUE INDEX idx_setting_objectname ON #__setting(object, name)');
                }
                break;

            default:
                if (!$this->db->tableExists('#__setting'))
                {
                    $this->db->exec('
                    CREATE TABLE `#__setting` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `object` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
                      `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
                      `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                     PRIMARY KEY (`id`)
                     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');
                    $this->db->exec('CREATE UNIQUE INDEX idx_setting_objectname ON #__setting(object, name)');
                }
                break;
        }
    }

    public function execArray(array $list)
    {
        $idx = 1;
        foreach ($list as $q)
        {
            $idx++;
            try
            {
                $this->db->exec($q);
            } catch (\Exception $e)
            {
                throw new \Exception($e->getMessage(). "\n | n. $idx SQL: $q", 0, $e);
            }
        }
    }

}
