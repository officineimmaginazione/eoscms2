<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Database;

class Setting
{

    private $_db;
    private $_tablename;

    public function __construct($db, $tablename)
    {
        $this->_db = $db;
        $this->_tablename = "#__" . $tablename;
    }

    private function exists($object, $name)
    {
        $q = $this->_db->prepare('select object, name from ' . $this->_tablename .
            ' where object = :object and name = :name');
        $q->bindValue(':object', $object);
        $q->bindValue(':name', $name);
        $q->execute();
        return ($q->fetch() != null);
    }

    public function getValue($object, $name)
    {
        $q = $this->_db->prepare('select value from ' . $this->_tablename .
            ' where object = :object and name = :name');
        $q->bindValue(':object', $object);
        $q->bindValue(':name', $name);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            return null;
        } else
        {
            return $r['value'];
        }
    }

    public function setValue($object, $name, $value)
    {
        if ($this->exists($object, $name))
        {
            $sql = 'update ' . $this->_tablename . ' set value = :value where object = :object and name = :name';
        } else
        {
            $sql = 'insert into ' . $this->_tablename . ' (object, name, value) values (:object, :name, :value)';
        }
        $q = $this->_db->prepare($sql);
        $q->bindValue(':object', $object);
        $q->bindValue(':name', $name);
        $q->bindValue(':value', $value);
        $q->execute();
    }

    public function getStr($object, $name, $default = '')
    {
        $value = $this->getValue($object, $name);
        if (is_null($value))
        {
            return $default;
        } else
        {
            return $value;
        }
    }

    public function setStr($object, $name, $value)
    {
        $this->setValue($object, $name, $value);
    }

    public function getInt($object, $name, $default = 0)
    {
        $val = $this->getValue($object, $name);
        if (empty($val))
        {
            return (int) $default;
        } else
        {
            return (int) $val;
        }
    }

    public function getBool($object, $name, $default = false)
    {
        $val = $this->getStr($object, $name, ($default == true) ? 'true' : 'false');
        return ($val == 'true') ? true : false;
    }

    public function setInt($object, $name, $value)
    {
        $this->setValue($object, $name, (int) $value);
    }

    public function setBool($object, $name, $value)
    {
        $this->setStr($object, $name, ($value == true) ? 'true' : 'false');
    }

}
