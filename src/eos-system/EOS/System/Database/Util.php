<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Database;

class Util
{

    protected $db;
    protected $dbFormatDateTime = 'Y-m-d H:i:s';
    protected $dbFormatDate = 'Y-m-d';

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function dateTimeToDBDateTime($value)
    {
        // $data = new \DateTime($value);
        return $value->format('Y-m-d H:i:s');
        //$data->format($data::ISO8601);
    }

    public function dateTimeToDBDate($value)
    {
        return $value->format('Y-m-d');
    }

    public function dbDateTimeToDateTime($value)
    {
        return \DateTimeImmutable::createFromFormat($this->dbFormatDateTime, $value);
    }

    public function dbDateToDateTime($value)
    {
        $vf = substr($value, 0, 10); // Elimina l'eventuale ora 
        $v = \DateTimeImmutable::createFromFormat($this->dbFormatDate, $vf);
        if ($v !== false)
        {
            return $v->setTime(0, 0, 0);
        } else
        {
            return $v;
        }
    }

    public function nowToDBDateTime()
    {
        return $this->dateTimeToDBDateTime(new \DateTimeImmutable());
    }

    public function prepareLikeValue($value)
    {
        $res = $value;
        switch ($this->db->driverName)
        {
            case 'mysql':
                // Devo sostituire i punti su MySQL
                $res = str_replace('.', '_', $res);
                break;
            case 'sqlite':
                break;
        }
        return $res;
    }

}
