<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */
/*
  Estensione di http://www.fpdf.org/
 */

namespace EOS\System\Drawing;

class PDF extends \FPDF
{

    public $charset = 'windows-1252';
    public $autoConvertCharset = true;

    protected function convText($value)
    {
        if ($this->autoConvertCharset)
        {
            return iconv('UTF-8', $this->charset, $value);
        } else
        {
            return $value;
        }
    }

    public function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        parent::Cell($w, $h, $this->convText($txt), $border, $ln, $align, $fill, $link);
    }

    public function Text($x, $y, $txt)
    {
        parent::Text($x, $y, $this->convText($txt));
    }

    public function MultiCell($w, $h, $txt, $border = 0, $align = 'J', $fill = false)
    {
        // Non converto perché usa internamente Cell
        parent::MultiCell($w, $h, $txt, $border, $align, $fill);
    }

    public function Write($h, $txt, $link = '')
    {
        // Non converto perché usa internamente Cell
        parent::Write($h, $txt, $link);
    }

    public function toBuffer()
    {
        return $this->Output('S');
    }

    public function saveToFile($filename)
    {
        $this->Output('F', $filename);
    }

    public function toResponse($response, $filename, $forceDownload = false)
    {
        if ($forceDownload)
        {
            $resHFile = 'inline; filename=' . basename($filename);
        } else
        {
            $resHFile = 'inline; filename=' . basename($filename);
        }
        return $response->withHeader('Content-Type', 'application/pdf')
                ->withHeader('Content-Description', 'File Transfer')
                ->withHeader('Content-Disposition', $resHFile)
                ->withHeader('Content-Transfer-Encoding', 'binary')
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public');
    }
    

}
