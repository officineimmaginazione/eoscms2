<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class DataEvent extends SystemEvent
{

    private $_isValid = true;
    protected $errors = [];
    protected $data;

    public function __construct($subject = null, $data = null)
    {
        $this->data = $data;
        parent::__construct($subject);
    }

    public function stopPropagation()
    {
        parent::stopPropagation();
        $this->_isValid = false;
    }

    public function isValid()
    {
        return $this->_isValid;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getErrorsString($separator = "\n")
    {
        return implode($separator, $this->errors);
    }

    public function addError($value)
    {
        $this->errors[] = $value;
        $this->stopPropagation();
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

}
