<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class DispatcherManager
{

    const SYSTEM_DISPATCHER = 'system.dispatcher';

    private $_list;

    public function __construct()
    {
        $this->_list = [];
    }

    public function getDispatcher($name = null)
    {
        $nameL = empty($name) ? self::SYSTEM_DISPATCHER : strtolower($name);
        if (!isset($this->_list[$nameL]))
        {
            $this->_list[$nameL] = new Dispatcher();
        }
        return $this->_list[$nameL];
    }
}
