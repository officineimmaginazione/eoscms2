<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class Event
{

    private $_propagationStopped = false;

    public function isPropagationStopped()
    {
        return $this->_propagationStopped;
    }

    public function stopPropagation()
    {
        $this->_propagationStopped = true;
    }

}
