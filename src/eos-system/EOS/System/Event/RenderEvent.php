<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class RenderEvent extends SystemEvent
{

    protected $output = '';

    public function __construct($subject = null, $output = '')
    {
        $this->output = $output;
        parent::__construct($subject);
    }

    public function appendOutput($output)
    {
        $this->output .= $output;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function setOutput($output)
    {
        $this->output = $output;
    }

}
