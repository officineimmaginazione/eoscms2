<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class Subscriber implements SubscriberInterface
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }
    
    public static function getSubscribedEvents()
    {
        return [];
    }
    

}
