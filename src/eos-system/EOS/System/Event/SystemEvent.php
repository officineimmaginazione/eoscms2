<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Event;

class SystemEvent extends Event
{

    protected $subject;

    public function __construct($subject = null)
    {
        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

}
