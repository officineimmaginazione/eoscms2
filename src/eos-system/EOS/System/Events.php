<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System;

final class Events
{   
    const CONTROLLER = 'system.controller';
    const TERMINATE = 'system.terminate';
}
