<?php
/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 **/

namespace EOS\System\Handlers;

class Error extends \Slim\Handlers\Error
{
  
  private function HookMessage($m)
  {
    return str_replace('Slim Application Error', 'EOS Application Error', $m); 
  }
  
  protected function renderHtmlErrorMessage(\Exception $exception)
  {
    return $this->HookMessage(parent::renderHtmlErrorMessage($exception));
  }
  
  protected function renderJsonErrorMessage(\Exception $exception)
  {
    return $this->HookMessage(parent::renderJsonErrorMessage($exception));
  }
  
  protected function renderXmlErrorMessage(\Exception $exception)
  {
    return $this->HookMessage(parent::renderXmlErrorMessage($exception));
  }
  
  public function __invoke(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Message\ResponseInterface $response, \Exception $exception)
  { 
    return parent::__invoke($request, $response, $exception);
  }
}
