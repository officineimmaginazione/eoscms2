<?php
/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 **/

namespace EOS\System\Handlers;

class NotAllowed extends \Slim\Handlers\NotAllowed
{
}
