<?php

namespace EOS\System\IO;

class DelimitedTextReader implements \Iterator
{

    protected $fileObject = null;
    protected $data;
    public $delimiter = ',';
    public $csv = true; // Usa lo standard CSV, diversamente esplode con Escape 
    public $utf8 = true;
    public $escape = '\\';
    public $enclosure = '"';

    protected function explodeEscaped(string $str, string $delimiter, string $escapeChar = '\\')
    {
        // "\0" è un carattere non visibile 
        $double = "\0\0\0_doub";
        $escaped = "\0\0\0_esc";
        $str = str_replace($escapeChar . $escapeChar, $double, $str);
        $str = str_replace($escapeChar . $delimiter, $escaped, $str);
        $split = explode($delimiter, $str);
        foreach ($split as &$val)
        {
            $val = str_replace([$double, $escaped], [$escapeChar, $delimiter], $val);
        }
        unset($val);
        return $split;
    }

    protected function parseLine()
    {
        $this->data = [];
        if (!$this->fileObject->eof())
        {
            if (strlen($this->delimiter) === 0)
            {
                $this->delimiter = ',';
            }

            $rLine = trim($this->fileObject->current());
            $line = $this->utf8 ? $rLine : utf8_encode($rLine);
            if ($this->csv)
            {
                $this->data = str_getcsv($line, $this->delimiter, $this->enclosure, $this->escape);
            } else
            {
                $this->data = $this->explodeEscaped($line, $this->delimiter, $this->escape); //str_getcsv($line, $this->delimiter, '', '\\');
            }
        }
    }

    public function __construct(string $filename, bool $forceAutoDetectLineEndings = false)
    {
        $this->data = null;
        if (!file_exists($filename))
        {
            throw new \Exception("File not found");
        }
        if ($forceAutoDetectLineEndings)
        {
            $original = ini_get("auto_detect_line_endings");
            ini_set("auto_detect_line_endings", true);
        }
        $this->fileObject = new \SplFileObject($filename, 'r');
        $this->fileObject->setFlags(\SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);
        if ($forceAutoDetectLineEndings)
        {
            ini_set("auto_detect_line_endings", $original);
        }
    }

    public function __destruct()
    {
        
    }

    public function rewind()
    {
        $this->fileObject->rewind();
    }

    public function current(): array
    {
        $this->parseLine();
        return $this->data;
    }

    public function key(): int
    {
        return $this->fileObject->key();
    }

    public function next()
    {
        $this->fileObject->next();
    }

    public function valid(): bool
    {
        return $this->fileObject->valid();
    }

}
