<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Language;

class Language
{
  public $id;
  public $name;
  public $status;
  public $iso;
  public $langcode;
  public $dateformat;
  public $datetimeformat;
  public $timeformat;
  public $enabled;

  public function __construct($id, $name, $iso, $langcode)
  {
    $this->id = $id;
    $this->name = $name;
    $this->status = true;
    $this->iso = $iso;
    $this->langcode = $langcode;
    $this->dateformat = 'd/m/Y';
    $this->datetimeformat = 'd/m/Y H:i:s';
    $this->timeformat = 'H:i:s';
    $this->enabled = true;
  }

}
