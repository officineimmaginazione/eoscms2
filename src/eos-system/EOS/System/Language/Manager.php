<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Language;

use EOS\System\Routing\PathHelper;

class Manager
{

    protected $container;
    protected $currentParams;
    private $_langlist;
    private $_current = null;
    private $_translation;
    private $_fileLoaded;

    private function loadTransFromFile($lang, $componentPath, $eleName)
    {
        // Cercare la chiave nell'array del componente e lo carico
        $langFile = $componentPath . 'languages' . DIRECTORY_SEPARATOR . $lang . DIRECTORY_SEPARATOR . $eleName . '.php';
        if (!isset($this->_fileLoaded[$langFile]))
        {
            if (file_exists($langFile))
            {
                $cla = \EOS\System\Util\IncludeCode::sandboxInclude($langFile, $this);
                if (is_array($cla))
                {
                    foreach ($cla as $k => $v)
                    {
                        $this->_translation[$lang][strtolower($k)] = $v;
                    }
                }
            }
            $this->_fileLoaded[$langFile] = true;
        }
    }

    private function loadCompTransFile($lang, $compName, $elemName, $pathComp)
    {
        if (!empty($pathComp))
        {
            $this->loadTransFromFile($lang, PathHelper::realComponentPath($compName, $pathComp), $elemName);
        }
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->currentParams = $container['currentParams'];
        $this->_langlist = [];
        $this->_translation = [];
        $this->_fileLoaded = [];
        $this->_current = $this->addLanguage(1, 'Italiano', 'it', 'it-IT');
    }

    public function addLanguage($id, $name, $iso, $langcode)
    {
        $l = new Language($id, $name, $iso, $langcode);
        $this->_langlist[] = $l;
        $this->_translation[$iso] = [];
        return $l;
    }

    public function getCurrent()
    {
        return $this->_current;
    }

    public function setLangFromISO($iso)
    {
        foreach ($this->_langlist as $l)
        {
            if (($l->iso == $iso) && ($l->enabled))
            {
                $this->_current = $l;
                return true;
            }
        }
        return false;
    }

    public function getLangFromISO($iso)
    {
        foreach ($this->_langlist as $l)
        {
            if (($l->iso == $iso) && ($l->enabled))
            {
                return $l;
            }
        }
        return false;
    }

    public function getLangFromID($id)
    {
        foreach ($this->_langlist as $l)
        {
            if (($l->id == $id) && ($l->enabled))
            {
                return $l;
            }
        }
        return false;
    }

    public function setLangFromID($id)
    {
        foreach ($this->_langlist as $l)
        {
            if (($l->id == $id) && ($l->enabled))
            {
                $this->_current = $l;
                return true;
            }
        }
        return false;
    }

    public function dbDateTimeToLangDateTime($value)
    {
        $db = $this->container->get('database');
        return $db->util->dbDateTimeToDateTime($value)->format($this->_current->datetimeformat);
    }

    public function dbDateToLangDate($value)
    {
        $db = $this->container->get('database');
        return $db->util->dbDateToDateTime($value)->format($this->_current->dateformat);
    }

    public function langDateTimeToDbDateTime($value)
    {
        $db = $this->container->get('database');
        $dt = \DateTimeImmutable::createFromFormat($this->_current->datetimeformat, $value);
        return $db->util->dateTimeToDBDateTime($dt);
    }

    public function langDateToDbDate($value)
    {
        $db = $this->container->get('database');
        $dt = \DateTimeImmutable::createFromFormat($this->_current->dateformat, $value);
        return $db->util->dateTimeToDBDate($dt);
    }

    public function dateTimeToLangDateTime(\DateTimeImmutable $dateTime)
    {
        return $dateTime->format($this->_current->datetimeformat);
    }

    public function dateTimeToLangDate(\DateTimeImmutable $dateTime)
    {
        return $dateTime->format($this->_current->dateformat);
    }

    public function langDateToDateTime($value)
    {
        $r = \DateTimeImmutable::createFromFormat($this->_current->dateformat, $value);
        if ($r !== false)
        {
            // Azzero l'ora altrimenti prende quella corrente
            return $r->setTime(0, 0, 0);
        } else
        {
            return $r;
        }
    }

    public function langDateTimeToDateTime($value)
    {
        return \DateTimeImmutable::createFromFormat($this->_current->datetimeformat, $value);
    }

    public function findTrans($key, $lang = '')
    {
        $key = strtolower($key);
        $seg = explode('.', $key);
        $res = false;
        if ($lang == '')
        {
            $lang = $this->_current->iso;
        }
        if ((count($seg) >= 3) && (isset($this->_translation[$lang])))
        {
            if (isset($this->_translation[$lang][$key]))
            {
                $res = $this->_translation[$lang][$key];
            } else
            {

                $path = $this->container->get('path');
                $compName = strtolower($seg[0]);
                $elemName = strtolower($seg[1]);
                // 1 Componente
                $this->loadCompTransFile($lang, $compName, $elemName, $this->currentParams['componentsPath']);
                // 2 Override
                $this->loadCompTransFile($lang, $compName, $elemName, $this->currentParams['componentsOverridePath']);
                // 3 Template
                $this->loadCompTransFile($lang, $compName, $elemName, $path->getThemeOverridePath());
                //
                if (isset($this->_translation[$lang][$key]))
                {
                    $res = $this->_translation[$lang][$key];
                }
            }
        }

        return $res;
    }

    public function tryTransList($keyList, &$resValue, $lang = '')
    {
        foreach ($keyList as $key)
        {
            $resValue = $key;
            $v = $this->findTrans($key, $lang);
            if ($v !== FALSE)
            {
                $resValue = $v;
                return true;
            }
        }
        return false;
    }

    // $key = system.common.key
    public function trans($key, $lang = '')
    {
        $res = $this->findTrans($key, $lang);
        return ($res === false) ? $key : $res;
    }

    public function transEncode($key, $lang = '')
    {
        return \EOS\System\Util\StringHelper::encodeHtml($this->trans($key, $lang));
    }

    public function loadFromDB($site)
    {
        $db = $this->container->get('database');
        $q = $db->prepare('select * from #__lang order by name');
        $q->execute();
        $list = $q->fetchAll();
        if (!empty($list))
        {
            $this->_langlist = [];
            foreach ($list as $r)
            {
                $l = $this->addLanguage($r['id'], $r['name'], $r['iso_code'], $r['language_code']);
                $l->dateformat = $r['datelite_format'];
                $l->datetimeformat = $r['datefull_format'];
                if ($site)
                {
                    $l->enabled = ($r['status_site'] == 1);
                    if ($r['default_site'])
                    {
                        $this->_current = $l;
                        $l->enabled = true;
                    }
                } else
                {
                    $l->enabled = ($r['status_admin'] == 1);
                    if ($r['default_admin'])
                    {
                        $this->_current = $l;
                        $l->enabled = true;
                    }
                }
                if ($l->enabled)
                {
                    $this->_current = $l;
                }
            }
            return true;
        } else
        {
            return false;
        }
    }

    public function getArrayFromDB($site)
    {
        $db = $this->container->get('database');
        $sql = 'select * from #__lang where ';
        if ($site)
        {
            $sql .= ' status_site = 1';
        } else
        {
            $sql .= ' status_admin  = 1';
        }
        $sql .= ' order by name';
        $q = $db->prepare($sql);
        $q->execute();
        return $q->fetchAll();
    }

    public function getDefaultFromDB($site)
    {
        $db = $this->container->get('database');
        $sql = 'select * from #__lang where ';
        if ($site)
        {
            $sql .= ' default_site = 1';
        } else
        {
            $sql .= ' default_admin  = 1';
        }
        $sql .= ' order by name';
        $q = $db->prepare($sql);
        $q->execute();
        return $q->fetch();
    }

    public function getLangFromDB($id_lang)
    {
        $db = $this->container->get('database');
        $sql = 'select * from #__lang where id = :id';
        $q = $db->prepare($sql);
        $q->bindValue(':id', (int) $id_lang);
        $q->execute();
        return $q->fetch();
    }

    public function getCurrentID()
    {
        return $this->_current->id;
    }

    public function getCurrentDatePickerFormat()
    {
        $convFormat = str_replace('Y', 'yyyy', $this->_current->dateformat);
        $convFormat = str_replace('d', 'dd', $convFormat);
        $convFormat = str_replace('m', 'mm', $convFormat);
        return $convFormat;
    }

    public function getCurrentISO()
    {
        return $this->_current->iso;
    }

    public function getLangList()
    {
        $res = [];
        foreach ($this->_langlist as $l)
        {
            if ($l->enabled)
            {
                $res[] = $l;
            }
        }
        return $res;
    }

    public function getLangListID()
    {
        $res = [];
        foreach ($this->_langlist as $l)
        {
            if ($l->enabled)
            {
                $res[] = $l->id;
            }
        }
        return $res;
    }

}
