<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Mail;

use PHPMailer;

class Mail
{

    protected $container;
    protected $mail;
    public $from = '';
    public $fromname = null;
    public $to;
    public $toname = null;
    public $replyto = null;
    public $cc = null;
    public $bcc = null;
    public $subject = null;
    public $message;
    public $attachments = [];
    public $altbody = null;
    public $html = true;
    public $hookMailer = null; // Clouser che intercetta il mailer per aggiungere allegati o altro

    public function __construct($container)
    {
        $this->container = $container;
    }
    
    public function addAttachmentString($binary, $name, $type = 'application/octet-stream')
    {
        $this->attachments [] = ['binary' => $binary, 'name' => $name, 'type' => $type];
    }

    public function addAttachment($path, $name = '', $type = 'application/octet-stream')
    {
        $this->AddAttachmentBinary(file_get_contents($path), empty($name) ? basename($path) : $name, $type);
    }

    public function sendEmail(&$error)
    {
        $setting = $this->container->get('database')->setting;
        $mail = new \PHPMailer();
        $mail->isSMTP();
        /*$mail->SMTPDebug = 2;
          $mail->Debugoutput = 'html'; */
        $mail->CharSet = 'UTF-8';
        $mail->Host = $setting->getStr('email_setting', 'host');
        $mail->Port = $setting->getInt('email_setting', 'port', 25);
        $mail->SMTPAuth = $setting->getBool('email_setting', 'auth', false);
        $mail->SMTPSecure = $setting->getStr('email_setting', 'secure');
        $mail->Username = $setting->getStr('email_setting', 'username');
        $mail->Password = $setting->getStr('email_setting', 'password');
        if (empty($this->from))
        {
            $mail->setFrom($setting->getStr('email_setting', 'from'), null);
        } else
        {
            $mail->setFrom($this->from, $this->fromname);
        }
        $mail->addAddress($this->to, $this->toname);
        if ($this->replyto != null)
            $mail->addReplyTo($this->replyto);
        if ($this->cc != null)
            $mail->addCC($this->cc);
        if ($this->bcc != null)
            $mail->addBCC($this->bcc);
        $mail->isHTML($this->html);
        $mail->Subject = $this->subject;
        $mail->Body = $this->message;
        //$mail->msgHTML($this->message);
        $mail->SMTPAutoTLS = false;
        if ($this->altbody != null)
            $mail->AltBody = $this->altbody;

        if ((!empty($this->hookMailer)) && ($this->hookMailer instanceof \Closure))
        {
            $bound = \Closure::bind($this->hookMailer, $mail);
            $bound();
        }
        
        foreach($this->attachments as $att) {
            $mail->addStringAttachment($att['binary'], $att['name'], 'base64', $att['type']);
        }

        if (!$mail->send())
        {
            $error = $mail->ErrorInfo;
            return false;
        } else
        {
            return true;
        }
    }

}
