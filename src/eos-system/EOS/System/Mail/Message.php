<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Mail;

class Message
{

    // Temporaneo
    private $_queueData = [];
    //
    protected $container;
    protected $attachments = [];
    protected $immediateProcess = true;
    public $templateComponent = 'System';
    public $templateName = 'message/mail';
    public $loadTemplate = true;
    public $subject = '';
    public $content = '';
    public $footer = '';
    public $to = '';
    public $cc = null;
    public $bcc = null;
    public $processError = null;
    
    protected function loadSettings()
    {
     $db = $this->container->get('database');
     $this->footer = $db->setting->getStr('setting', 'message.footer');
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->loadSettings();
    }

    public function loadComponentContent($component, $template, $data = [])
    {
        $tmpl = new \EOS\System\Views\Template($this->container);
        // Faccio l'Injection del messaggio così da poter modificare le impostazioni dentro
        $tmpl->message = $this;
        $tmpl->data = $data;
        $this->content = $tmpl->render($component, $template);
    }

    public function loadContent($componentTemplate, $data = [])
    {
        $this->loadComponentContent($this->container['currentParams']['componentName'], $componentTemplate, $data);
    }

    public function addAttachmentBinary($binary, $name, $type = 'application/octet-stream')
    {
        $this->attachments [] = ['base64' => base64_encode($binary), 'name' => $name, 'type' => $type];
    }

    public function addAttachment($path, $name = '', $type = 'application/octet-stream')
    {
        $this->AddAttachmentBinary(file_get_contents($path), empty($name) ? basename($path) : $name, $type);
    }

    // Momentaneamente spedisce direttamente la mail
    public function send()
    {
        if ($this->loadTemplate)
        {
            $tmpl = new \EOS\System\Views\Template($this->container);
            $tmpl->content = $this->content;
            $tmpl->subject = $this->subject;
            $tmpl->footer = $this->footer;
            // Faccio l'Injection del messaggio così da poter modificare le impostazioni dentro
            $tmpl->message = $this;
            $message = $tmpl->render($this->templateComponent, $this->templateName);
        } else
        {
            $message = $this->content;
        }
        $this->_queueData['to'] = $this->to;
        $this->_queueData['cc'] = $this->cc;
        $this->_queueData['bcc'] = $this->bcc;
        $this->_queueData['subject'] = $this->subject;
        $this->_queueData['message'] = $message;
        $this->_queueData['attachment'] = $this->attachments;
        if ($this->immediateProcess)
        {
            return $this->processQueue();
        } else
        {
            // Qui dovrei aggiungere alla coda
            return true;
        }
    }

    public function processQueue()
    {
        $this->sendError = null;
        if (!empty($this->_queueData))
        {
            $mail = new Mail($this->container);
            $mail->to = $this->_queueData['to'];
            $mail->cc = $this->_queueData['cc'];
            $mail->bcc = $this->_queueData['bcc'];
            $mail->subject = $this->_queueData['subject'];
            $mail->message = $this->_queueData['message'];
            foreach($this->_queueData['attachment'] as $att) {
                $mail->addAttachmentString(base64_decode($att['base64']), $att['name'], $att['type']);
            }
            return $mail->sendEmail($this->processError);
        } else
        {
            return true;
        }
    }

}
