<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

use EOS\System\Util\StringHelper;

class Attachment extends \EOS\System\Media\Storage
{

    protected function load()
    {
        $this->folderName = 'att';
        $this->settingName = 'attachment';
        $this->mimeList = [
            'pdf' => 'application/pdf',
            'zip' => 'application/x-zip-compressed',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'mp3' => 'audio/mpeg',
            'mp4' => 'video/mp4'];
        parent::load();
    }

    public function putFile(string $sourceFilename, int $idObject, string $name): MediaObject
    {
        $this->checkFileExists($sourceFilename);
        $res = $this->putStream(fopen($sourceFilename, 'r'), $idObject, mime_content_type($sourceFilename), filesize($sourceFilename), $name);
        return $res;
    }

    public function putStream($stream, int $idObject, string $mime, int $size, string $name): MediaObject
    {
        $r = null;
        $path = StringHelper::sanitizeFileName($name, true);
        $this->execTransaction(function () use (&$r, $stream, $idObject, $mime, $size, $path)
        {
            $q = $this->db->prepare('insert into  #__attachment (type_object, id_object, mime, size, path,
                   ins_id, ins_date, up_id, up_date) values 
                   (:type_object, :id_object, :mime, :size, :path, 
                   :ins_id, :ins_date, :up_id, :up_date)');
            $q->bindValue(':type_object', $this->getTypeObjectName());
            $q->bindValue(':id_object', $idObject);
            $q->bindValue(':mime', $mime);
            $q->bindValue(':size', $size);
            $q->bindValue(':path', $path);
            $q->bindValue(':ins_id', $this->userID);
            $q->bindValue(':ins_date', $this->db->util->nowToDBDateTime());
            $q->bindValue(':up_id', $this->userID);
            $q->bindValue(':up_date', $this->db->util->nowToDBDateTime());
            $q->execute();
            $id = $this->db->lastInsertId();
            // Aggiorno con il nome secondo i parametri
            $qn = $this->db->prepare('update #__attachment set path = :path where id = :id');
            $qn->bindValue('id', $id);
            $path = $this->prepareName($id, $path);
            $qn->bindValue('path', $path);
            $qn->execute();

            $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject, $id, $path]);
            $r = $this->storageToMediaObject($this->storage->putObject($key, $stream), $idObject, $id, $path);
        });
        return $r;
    }

    public function getList(int $idObject)
    {
        $q = $this->db->prepare('select * from #__attachment 
              where id_object = :id_object and type_object = :type_object');
        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $res = $q->fetchAll();
        return empty($res) ? [] : $res;
    }

    public function getItem(int $idObject, int $id, string $path = null): array
    {
        $res = [];
        $q = $this->db->prepare('select * from #__attachment 
             where id = :id and id_object = :id_object and type_object = :type_object');
        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':id', $id);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            if (!is_null($path))
            {
                if (mb_strtolower($path) !== mb_strtolower($r['path']))
                {
                    return $res;
                }
            }
            $res = $r;
        }
        return $res;
    }

    // Posso decidere di controllare che il path corrisponda
    public function getObject(int $idObject, int $id, string $path = null): ?MediaObject
    {
        $res = null;
        $q = $this->db->prepare('select id, path from #__attachment 
             where id = :id and id_object = :id_object and type_object = :type_object');
        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':id', $id);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            if (!is_null($path))
            {
                if (mb_strtolower($path) !== mb_strtolower($r['path']))
                {
                    return $res;
                }
            }
            $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject, $id, $r['path']]);
            $res = $this->storageToMediaObject($this->storage->headObject($key), $idObject, $id, $r['path']);
        }
        return $res;
    }

    public function getStream(int $idObject, int $id, string $path = null) //: ?object
    {
        $res = null;
        $o = $this->getObject($idObject, $id, $path);
        if (!empty($o))
        {
            $res = $this->storage->getObjectStream($o->path);
        }
        return $res;
    }

    public function deleteAll(int $idObject)
    {
        $list = $this->getList($idObject);
        foreach ($list as $l)
        {
            $this->deleteObject($idObject, (int) $l['id']);
        }
        $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject]);
        $this->storage->deleteFolder($key);
    }

    public function deleteObject(int $idObject, int $id): bool
    {
        $r = $this->getItem($idObject, $id);
        if (empty($r))
        {
            return false;
        }
        $keyList = [$this->getIDObjectRoot($r['id_object']), $r['id_object'], $r['id']];
        $key = $this->prepareKey(array_merge($keyList, [$r['path']]));
        $this->storage->deleteObject($key);
        $key = $this->prepareKey($keyList);
        $this->storage->deleteFolder($key);

        $q = $this->db->prepare('delete from #__attachment where id = :id');
        $q->bindValue(':id', $r['id']);
        $q->execute();
        return true;
    }

}
