<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

use EOS\System\Util\StringHelper;

class Image extends \EOS\System\Media\Storage
{

    const IMAGE_HR = 'h';
    const IMAGE_LR = 'l';
    const IMAGE_THUMBNAIL = 't';

    protected $imageTypes = [];

    protected function isJPG(string $filename): bool
    {
        return in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)), ['jpg', 'jpeg']);
    }

    protected function load()
    {
        $this->mimeList = [
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif'];

        $this->imageTypes[static::IMAGE_HR] = ['width' => 1280, 'heigth' => 720, 'quality' => 90];
        $this->imageTypes[static::IMAGE_LR] = ['width' => 400, 'heigth' => 400, 'quality' => 70];
        $this->imageTypes[static::IMAGE_THUMBNAIL] = ['width' => 50, 'heigth' => 50, 'quality' => 70];
        $this->folderName = 'img';
        $this->settingName = 'image';
        parent::load();
    }

    // Utilità
    // La immagini provenienti dal telefono non sono girate ma usano l'exif per l'orientamento
    // questo dopo vari test crea problemi anche poi con i PDF
    // quindi leggo l'immagine e se trovo l'exif la giro e la risalvo in modo che sia sempre corretta
    protected function newImageAndFixOrientationAndWidth(MediaObject $tempObject, bool $fixOrientation = true, int $maxWidth = 0): \EOS\System\Drawing\Image
    {
        $filename = $tempObject->path;
        $res = new \EOS\System\Drawing\Image($filename);
        $imgInfo = $res->getOriginalInfo();
        $replaceSource = false;
        if (($fixOrientation) && (isset($imgInfo['exif']['Orientation'])))
        {
            // Calcola l'orientamento quando è necessario
            if ($imgInfo['exif']['Orientation'] !== 1)
            {
                $res->autoOrient();
                $replaceSource = true;
            }
        }
        if ($maxWidth > 0)
        {
            if ($res->getWidth() > $maxWidth)
            {
                $res->fitToWidth($maxWidth);
                $replaceSource = true;
            }
        }
        if ($replaceSource)
        {
            $quality = $this->isJPG($filename) ? 95 : null;
            $res->save($filename, $quality);
            unset($res);
            $res = new \EOS\System\Drawing\Image($filename);
        }
        return $res;
    }

    protected function storeTempImages(MediaObject $tempObject, int $idObject, int $id, string $path, int &$size): MediaObject
    {
        $res = null;
        $images = $this->newTempImages($tempObject);
        foreach ($images as $k => $to)
        {
            $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject, $id, $k, $path]);
            $r = $this->storageToMediaObject($this->storage->putFile($key, $to->path), $idObject, $id, $path);
            if ($k === static::IMAGE_HR)
            {
                $res = $r;
                // Calcolo dimensione dal file temporaneo
                $size = filesize($to->path);
            }
            $this->getTemp()->deleteObject($to);
        }
        return $res;
    }

    public function getImageTypes(): array
    {
        return $this->imageTypes;
    }

    public function putFile(string $sourceFilename, int $idObject, int $cover, int $pos, string $name): MediaObject
    {
        $this->checkFileExists($sourceFilename);
        $res = $this->putStream(fopen($sourceFilename, 'r'), $idObject, $cover, $pos, filesize($sourceFilename), $name);
        return $res;
    }

    public function putStream($stream, int $idObject, int $cover, int $pos, int $size, string $name): MediaObject
    {
        $r = null;
        $this->execTransaction(function () use (&$r, $stream, $idObject, $cover, $pos, $size, $name)
        {
            // Prelevo il nome e aggiungo jpg
            $path = StringHelper::sanitizeFileName(pathinfo($name, PATHINFO_FILENAME), true) . '.jpg';
            $q = $this->db->prepare('insert into  #__image (type_object, id_object, cover, pos, size, path,
                   ins_id, ins_date, up_id, up_date) values 
                   (:type_object, :id_object, :cover, :pos, :size, :path, 
                   :ins_id, :ins_date, :up_id, :up_date)');
            $q->bindValue(':type_object', $this->getTypeObjectName());
            $q->bindValue(':id_object', $idObject);
            $q->bindValue(':cover', $cover);
            $q->bindValue(':pos', $pos);
            $q->bindValue(':size', $size);
            $q->bindValue(':path', $path);
            $q->bindValue(':ins_id', $this->userID);
            $q->bindValue(':ins_date', $this->db->util->nowToDBDateTime());
            $q->bindValue(':up_id', $this->userID);
            $q->bindValue(':up_date', $this->db->util->nowToDBDateTime());
            $q->execute();
            $id = $this->db->lastInsertId();
            // Calcolo il nome del file/path in base alle impostazioni
            $path = $this->prepareName($id, $path);
            // Salvo lo stream su un file temporaneo
            $tmpExt = trim(pathinfo($name, PATHINFO_EXTENSION));
            $to = $this->getTemp()->putStream($stream, $tmpExt === '' ? '.jpg' : $tmpExt);
            $r = $this->storeTempImages($to, $idObject, $id, $path, $size); // Rileggo la dimensione dall'HR
            $this->getTemp()->deleteObject($to);
            // Aggiorno con il nome secondo i parametri e anche la dimensione con le dimensioni dell HR
            $qn = $this->db->prepare('update #__image set path = :path, size = :size where id = :id');
            $qn->bindValue('id', $id);
            $qn->bindValue('size', $size);
            $qn->bindValue('path', $path);
            $qn->execute();
        });
        return $r;
    }

    public function getList(int $idObject)
    {
        $q = $this->db->prepare('select * from #__image 
              where id_object = :id_object and type_object = :type_object
              order by pos');

        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $res = $q->fetchAll();
        return empty($res) ? [] : $res;
    }

    public function getItem(int $idObject, int $id, string $path = null): array
    {
        $res = [];
        $q = $this->db->prepare('select * from #__image 
             where id = :id and id_object = :id_object and type_object = :type_object');

        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':id', $id);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            if (!is_null($path))
            {
                if (mb_strtolower($path) !== mb_strtolower($r['path']))
                {
                    return $res;
                }
            }
            $res = $r;
        }
        return $res;
    }

    // Posso decidere di controllare che il path corrisponda
    public function getObject(int $idObject, int $id, string $type, string $path = null): ?MediaObject
    {
        if (!in_array($type, array_keys($this->imageTypes)))
        {
            throw new \Exception('Invalid image type', $type);
        }

        $res = null;
        $q = $this->db->prepare('select id, path from #__image 
             where id = :id and id_object = :id_object and type_object = :type_object');
        $q->bindValue(':id_object', $idObject);
        $q->bindValue(':id', $id);
        $q->bindValue(':type_object', $this->getTypeObjectName());
        $q->execute();
        $r = $q->fetch();
        if (!empty($r))
        {
            if (!is_null($path))
            {
                if (mb_strtolower($path) !== mb_strtolower($r['path']))
                {
                    return $res;
                }
            }
            $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject, $id, $type, $r['path']]);
            $stObj = $this->storage->headObject($key);
            $res = $this->storageToMediaObject($stObj, $idObject, $id, $r['path']);
        }
        return $res;
    }

    public function getObjectList(int $idObject, int $id, array $types = []): array
    {
        $res = [];
        if (empty($types))
        {
            $types = array_keys($this->imageTypes);
        }

        foreach ($types as $t)
        {
            $res[$t] = $this->getObject($idObject, $id, $t);
        }
        return $res;
    }

    public function getStream(int $idObject, int $id, string $type, string $path = null): object
    {
        $res = null;
        $o = $this->getObject($idObject, $id, $type, $path);
        if (!empty($o))
        {
            $res = $this->storage->getObjectStream($o->path);
        }
        return $res;
    }

    public function deleteAll(int $idObject)
    {
        $list = $this->getList($idObject);
        foreach ($list as $l)
        {
            $this->deleteObject($idObject, (int) $l['id']);
        }
        $key = $this->prepareKey([$this->getIDObjectRoot($idObject), $idObject]);
        $this->storage->deleteFolder($key);
    }

    public function deleteObject(int $idObject, int $id): bool
    {
        $r = $this->getItem($idObject, $id);
        if (empty($r))
        {
            return false;
        }
        $keyList = [$this->getIDObjectRoot($r['id_object']), $r['id_object'], $r['id']];

        foreach ($this->imageTypes as $k => $v)
        {
            // Cancello il file
            $key = $this->prepareKey(array_merge($keyList, [$k, $r['path']]));
            $this->storage->deleteObject($key);
            // Cancello la cartella della risoluzione
            $key = $this->prepareKey(array_merge($keyList, [$k]));
            $this->storage->deleteFolder($key);
        }
        $key = $this->prepareKey($keyList);
        $this->storage->deleteFolder($key);

        $q = $this->db->prepare('delete from #__image where id = :id');
        $q->bindValue(':id', $r['id']);
        $q->execute();
        return true;
    }

    public function newTempImages(MediaObject $tempObject, bool $scaleSource = false): array
    {
        $res = [];
        $image = null;
        foreach ($this->imageTypes as $k => $v)
        {
            if (is_null($image))
            {
                if ($k !== static::IMAGE_HR)
                {
                    throw new \Exception('Media\Image la prima immagine non è ' . static::IMAGE_HR);
                }
                $maxWidth = 0;
                if ($scaleSource)
                {
                    $maxWidth = $v['width'];
                }
                $image = $this->newImageAndFixOrientationAndWidth($tempObject, true, $maxWidth);
            }
            $to = $this->getTemp()->newTempObject('jpg', $k);
            // Non scalo le immagini con la stessa larghezza
            if ((int) $v['width'] !== (int) $image->getWidth())
            {
                $image->fitToWidth($v['width']);
            }

            $image->save($to->path, $v['quality'], 'jpg');
            $res[$k] = $to;
        }
        return $res;
    }

    public function setCover(int $idObject, int $id)
    {
        if ($id !== 0)
        {
            $this->execTransaction(function () use ($idObject, $id)
            {
                $qn = $this->db->prepare('update #__image set cover = :cover where id = :id and id_object = :id_object');
                $items = $this->getList($idObject);
                foreach ($items as $item)
                {
                    $cover = (int) $item['id'] === $id ? 1 : 0;
                    $qn->bindValue('id', $item['id']);
                    $qn->bindValue('id_object', $item['id_object']);
                    $qn->bindValue('cover', $cover);
                    $qn->execute();
                }
            });
        }
    }

    public function updatePos(int $idObject, int $id, int $pos)
    {
        if ($id !== 0)
        {
            $qn = $this->db->prepare('update #__image set pos = :pos where id = :id and id_object = :id_object');
            $qn->bindValue('id', $id);
            $qn->bindValue('id_object', $idObject);
            $qn->bindValue('pos', $pos);
            $qn->execute();
        }
    }

}
