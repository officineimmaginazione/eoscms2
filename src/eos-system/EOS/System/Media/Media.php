<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

use EOS\System\Routing\PathHelper;

class Media
{

    protected $settings;
    protected $container;
    protected $path;
    protected $db;
    protected $typeObject;
    protected $typeObjectParent = 'media';
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
    protected $mimeList = [];
    protected $settingName = '';
    protected $folderName = '';
    protected $userID = 0;
    protected $maxSize = 20; //MB

    private function prepareTypeObject($typeObject)
    {
        if (strlen($typeObject) == 0)
        {
            throw new \Exception('Invalid type object!');
        }

        $p = explode('.', $typeObject);

        if (count($p) > 1)
        {
            $this->typeObjectParent = $p[0];
            unset($p[0]);
        }
        $this->typeObject = implode('.', $p);
    }

    protected function getTypeObjectName(): string
    {
        return $this->typeObjectParent . '.' . $this->typeObject;
    }

    protected function getFolderName(): string
    {
        $p = explode('.', $this->getTypeObjectName());
        $p[] = $this->folderName;
        return PathHelper::addTrailingSlash(PathHelper::implodeSlash($p));
    }

    protected function load()
    {
        
    }

    protected function checkFileExists(string $filename)
    {
        if (!file_exists($filename))
        {
            throw new \Exception('File not found: ' . $filename);
        }
    }

    public function __construct($container, string $typeObject)
    {
        $this->prepareTypeObject(mb_strtolower($typeObject));
        $this->container = $container;
        $this->path = $this->container->get('path');
        $this->db = $this->container->get('database');
        $this->load();
        if ($this->settingName === '')
        {
            throw new \Exception('Media invalid setting name!');
        }
        if ($this->folderName === '')
        {
            throw new \Exception('Media invalid folder name!');
        }
    }

    public function getPath(): string
    {
        
    }

    public function getUrl(): string
    {
        
    }

    /*
      ['pdf' => 'application/pdf',
      'xlsx' => 'application/xlsx'
      ]

     */

    public function setMimeList(array $mimeList)
    {
        $this->mimeList = $mimeList;
    }
    
    public function getMimeList(): array
    {
        return $this->mimeList;
    }
    
    public function setMaxSize(int $size)
    {
        $this->maxSize = $size;
    }

    public function getMaxSizeByte()
    {
        $res = $this->maxSize;
        if ($res > 0)
        {
            $res = 1024 * 1024 * $res;
        } else
        {
            $res = 1024 * 1024 * 12;
        }
        return $res;
    }
    
    public function getExtFromMime(string $mime): string
    {
        $res = '';
        foreach ($this->mimeList as $k => $v)
        {
            if ($mime === $v)
            {
                $res = $k;
                break;
            }
        }
        return $res;
    }

}
