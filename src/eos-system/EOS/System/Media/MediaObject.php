<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

class MediaObject
{
   public $id = null;
   public $idObject = null;
   public $typeObject = null;
   public $name = '';
   public $path = '';
   public $url = '';
   public $tilte = '';
   public $cloudStorageObject = null;
   
   
   public function isCloud(): bool
   {
       return !empty($this->cloudStorageObject);
   }
}
