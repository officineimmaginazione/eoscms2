<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

use EOS\System\Util\ArrayHelper;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Security\CryptHelper;

class Storage extends \EOS\System\Media\Media
{

    const NAME_FORMAT_ORIGIN = 0;
    const NAME_FORMAT_OFFUSCATE = 1;
    const NAME_FORMAT_WITH_ID = 2;
    const NAME_FORMAT_ONLY_ID = 3;
    const NAME_CASE_ORIGIN = 0;
    const NAME_CASE_LOWER = 1;
    const NAME_CASE_UPPER = 2;
    const NAME_SANITIZE_NONE = 0;
    const NAME_SANITIZE_URL = 1;

    protected $storage;
    protected $temp = null;
    //
    protected $nameFormat;
    protected $nameCase;
    protected $nameSanitize;
    //
    protected $maxItems;

    /*
      {
      storage: {
      "config": {"amazons3": {settings....}, "filesystem": {}}
      "active": "filesystem"
      };

      }
     */ 

    protected function storageToMediaObject(?\EOS\System\Cloud\Storage\StorageObject $obj, int $idObject, int $id, string $name): ?MediaObject
    {
        if (is_null($obj))
        {
            $r = null;
        } else
        {
            $r = new MediaObject();
            $r->cloudStorageObject = $obj;
            $r->path = $r->cloudStorageObject->key;
            $r->url = $r->cloudStorageObject->url;
            $r->id = $id;
            $r->name = $name;
            $r->idObject = $idObject;
        }
        return $r;
    }

    protected function getIDObjectRoot(int $idObject): int
    {
        return ceil($idObject / 1000) * 1000;
    }

    protected function load()
    {
        parent::load();

        $setting = ArrayHelper::fromJSON($this->db->setting->getStr($this->typeObjectParent, $this->typeObject . '.' . $this->settingName));
        $this->nameFormat = ArrayHelper::getInt($setting, 'name-format', static::NAME_FORMAT_ONLY_ID);
        $this->nameCase = ArrayHelper::getInt($setting, 'name-case', static::NAME_CASE_LOWER);
        $this->nameSanitize = ArrayHelper::getInt($setting, 'name-sanitize', static::NAME_SANITIZE_URL);
        $this->maxSize = ArrayHelper::getInt($setting, 'max-size', 10); // 10 MB
        $this->maxItems = ArrayHelper::getInt($setting, 'max-items', 1);
        $stos = ArrayHelper::getArray($setting, 'storage');
        $stoActive = ArrayHelper::getStr($stos, 'active');
        $stoConfig = ArrayHelper::getMdArray($stos, ['config', $stoActive]);
        switch ($stos)
        {
            case 'amazons3':
                $this->storage = new \EOS\System\Cloud\Storage\AmazonS3($stoConfig);
                break;
            default:
                // filesystem
                // metto dei parametri predefiniti per il file system     
                $stoConfig['bucket'] = ArrayHelper::getStr($stoConfig, 'bucket', '');
                $stoConfig['usebucket'] = false; // Normalmente nel file system non imposto un bucket ed uso la cartella diretta
                $stoConfig['path'] = ArrayHelper::getStr($stoConfig, 'path', $this->path->getUploadsPath());
                $stoConfig['url'] = ArrayHelper::getStr($stoConfig, 'url', $this->path->getUploadsUrl());
                $this->storage = new \EOS\System\Cloud\Storage\FileSystem($stoConfig);
                break;
        }
    }

    protected function prepareName(int $id, string $name): string
    {
        $part = pathinfo(StringHelper::sanitizeFileName($name, true));
        $rName = $part['filename'];
        $rExt = mb_strtolower($part['extension']);
        if ($rName === '')
        {
            throw new \Exception('Invalid filename!');
        }

        if ($rExt === '')
        {
            throw new \Exception('Invalid extension!');
        }

        switch ($this->nameFormat)
        {
            case static::NAME_FORMAT_ORIGIN:
                // Mantiene il nome uguale
                break;
            case static::NAME_FORMAT_OFFUSCATE:
                $rName = CryptHelper::randomString(32) . '-' . $id;
                break;
            case static::NAME_FORMAT_WITH_ID:
                $rName .= '-' . $id;
                break;

            default: // NAME_FORMAT_ONLY_ID
                $rName = (string) $id;
                break;
        }

        switch ($this->nameSanitize)
        {
            case static::NAME_SANITIZE_URL:
                $rName = StringHelper::sanitizeUrl($rName);
                break;
            default: // static::NAME_SANITIZE_NONE
                break;
        }

        switch ($this->nameCase)
        {
            case static::NAME_CASE_LOWER:
                $rName = mb_strtolower($rName);
                break;
            case static::NAME_CASE_UPPER:
                $rName = mb_strtoupper($rName);
                break;
            default:
                break;
        }


        return $rName . '.' . $rExt;
    }

    protected function prepareKey(array $values): string
    {
        return $this->getFolderName() . PathHelper::implodeSlash($values);
    }

    protected function execTransaction(\Closure $proc)
    {
        $useTrans = !$this->db->inTransaction();
        if ($useTrans)
        {
            $this->db->beginTransaction();
        }
        $proc();
        if ($useTrans)
        {
            $this->db->commit();
        }
    }

    public function getTemp(): \EOS\System\Media\Temp
    {
        if (is_null($this->temp))
        {
            $this->temp = new \EOS\System\Media\Temp($this->container, $this->getTypeObjectName());
            $this->temp->setMimeList($this->mimeList);
            $this->temp->setMaxSize($this->maxSize);
            // Forza la pulizia della cartella
            $this->temp->clear();
        }
        return $this->temp;
    }

    public function setNameFormat(int $value)
    {
        $this->nameFormat = $value;
    }

    public function setNameCase(int $value)
    {
        $this->nameCase = $value;
    }

    public function setNameSanitize(int $value)
    {
        $this->nameSanitize = $value;
    }

    public function setMaxItems(int $maxItems)
    {
        $this->maxItems = $maxItems;
    }

    public function getMaxItems()
    {
        return $this->maxItems;
    }

}
