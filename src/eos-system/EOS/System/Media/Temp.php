<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Media;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Security\CryptHelper;

class Temp extends \EOS\System\Media\Media
{

    private $_clearTimeout;
    private $_clearAuto;
    private $_path;
    private $_url;

    private function createUploadFolder()
    {
        if (!is_dir($this->_path))
        {
            mkdir($this->_path, 0777, true);
        }
    }

    private function newObject(string $name): MediaObject
    {
        if (strlen($name) === '')
        {
            throw new \Exception('Invalid object name!');
        }
        $res = new MediaObject();
        $res->typeObject = $this->getTypeObjectName();
        $res->name = $name;
        $res->path = $this->getPath() . $res->name;
        $res->url = $this->getUrl() . $res->name;
        return $res;
    }

    protected function load()
    {
        $this->settingName = 'temp';
        $this->folderName = 'tmp';
        parent::load();

        // Cancello sempre i file temporanei con più di 2 ore di vita
        $this->_clearTimeout = 120 * 60;
        $this->_clearAuto = true;

        $this->mimeList = ['tmp' => 'application/octet-stream'];


        $fName = $this->getFolderName();
        $this->_path = PathHelper::addDirSep($this->path->getUploadsPath() . PathHelper::slashToDirSep($fName));
        $this->_url = PathHelper::addTrailingSlash($this->path->getUploadsUrl() . $fName);
    }

    // L'univocità del nome non è garantita e quindi conviene nel codice controllare se esiste il file
    public function newTempName(string $ext, string $suffix = ''): string
    {
        // Aggiungo il punto se ho un estensione valida (potrebbe non averla il file per vari motivi)
        if ((strlen($ext) > 0) && (!StringHelper::startsWith($ext, '.')))
        {
            $ext = '.' . $ext;
        }
        $tp = [];
        $tp[] = str_replace([':', '-', '+', 'T', '.'], '', DateTimeHelper::now()->format('Y-m-d\TH:i:s.u'));
        $tp[] = md5(uniqid(CryptHelper::randomString(16), true));
        $tp[] = CryptHelper::randomString(10);
        if ($suffix !== '')
        {
            $tp[] = StringHelper::sanitizeFileName($suffix, false);
        }
        return StringHelper::sanitizeFileName(implode('-', $tp), false) . $ext;
    }

    public function newTempObject(string $ext, string $suffix = ''): MediaObject
    {
        if ($this->_clearAuto)
        {
            $this->clear();
        }
        $loopCount = 0;
        do
        {
            if ($loopCount > 0)
            {
                // Aspetto 50 millisecondi prima di ritentare
                usleep(50000);
            }
            if ($loopCount >= 10)
            {
                throw new \Exception('"\EOS\System\Media\MediaTemp::newFileName()" '
                    . 'non riesce a creare un nome di file univoco dopo ' . $loopCount . ' tentativi!');
            }
            $loopCount++;

            $name = $this->newTempName($ext, $suffix);
            $res = $this->newObject($name);
        } while (is_file($res->path));
        return $res;
    }

    public function clear(int $timeout = 0)
    {
        $timeout = $timeout > 0 ? $timeout : $this->_clearTimeout;
        $currenTime = time();
        $exts = array_keys($this->mimeList);
        foreach (new \DirectoryIterator($this->getPath()) as $file)
        {
            if ($file->isFile())
            {
                if (($file->getMTime() + $timeout < $currenTime) && (in_array(mb_strtolower($file->getExtension()), $exts)))
                {
                    unlink($file->getPathname());
                }
            }
        }
    }

    public function putFile(string $sourceFilename, string $ext = '', string $suffix = ''): MediaObject
    {
        $this->checkFileExists($sourceFilename);
        if ($ext === '')
        {
            $ext = mb_strtolower(pathinfo($sourceFilename, PATHINFO_EXTENSION));
        }
        return $this->putStream(fopen($sourceFilename, 'r'), $ext, $suffix);
    }

    public function putStream($stream, string $ext, string $suffix = ''): MediaObject
    {
        $f = $this->newTempObject($ext, $suffix);
        file_put_contents($f->path, $stream);
        return $f;
    }

    public function getPath(): string
    {
        $this->createUploadFolder();
        if (!is_dir($this->_path))
        {
            mkdir($this->_path);
        }
        return $this->_path;
    }

    public function getUrl(): string
    {
        return $this->_url;
    }

    public function getObject(string $name): ?MediaObject
    {
        $res = $this->newObject(StringHelper::sanitizeFileName(basename($name)));
        if (!is_file($res->path))
        {
            $res = null;
        }
        return $res;
    }

    public function deleteObject(?MediaObject $obj)
    {
        if (!is_null($obj))
        {
            if (is_file($obj->path))
            {
                unlink($obj->path);
            }
        }
    }

}
