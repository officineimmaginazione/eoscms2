<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;

class Extension extends \EOS\System\Middleware\Middleware
{

  public function __invoke(Request $request, Response $response, callable $next)
  {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/')
    {
      $ext = $this->container['currentParams']['pageExtension'];
      if (($ext != '/') && ($this->container['currentParams']['multiLanguage']))
      {
        if (strlen(PathHelper::removeSlash(PathHelper::removeExtension($path))) == 2)
        {
          $ext = '/'; // Forzo la / per la root come ISO di una lingua 
        }
      }
      $extraExts = $this->container['currentParams']['extraExtensions'];
      $pie = pathinfo($path, PATHINFO_EXTENSION);
      if (in_array($pie, $extraExts))
      {
        $newuri = $uri->withPath(PathHelper::addTrailingSlash($path));
        // Le extra extension dovranno essere mappate come /modulo/libs.js/ <- La slash finale è importante
        // devo mettere anche qua il codice per i siti non multi lingua
        return $next($request->withUri($newuri), $response);
      } else
      {
        $pathext = '.' . $pie;
        if (substr($path, -strlen($ext)) == $ext)
        {
          $newuri = $uri->withPath(PathHelper::addTrailingSlash(substr($path, 0, strlen($path) - strlen($pathext))));
          return $next($request->withUri($newuri), $response);
        } else
        {
          if (($pathext == '.') && (substr($path, -1) != '.'))
          {
            $uri = $uri->withPath(PathHelper::removeTrailingSlash($path) . $ext);
            return $response->withRedirect((string) $uri, 301);
          }
        }
      }
    }
    return $next($request, $response);
  }

}
