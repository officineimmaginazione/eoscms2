<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;

class Language extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $uri = $request->getUri();
        $path = $uri->getPath();
        $lang = $this->container->get('language');
        if ($path == '/')
        {
            return $response->withRedirect($uri->withPath(PathHelper::addTrailingSlash($lang->getCurrent()->iso)), 302);
        } else
        {
            $segments = PathHelper::explodeSlash($path);
            if ($lang->setLangFromISO($segments[0]))
            {
                unset($segments[0]);
                $newuri = $uri->withPath(PathHelper::addTrailingSlash(PathHelper::implodeSlash($segments)));
                // Le extra extension dovranno essere mappate come /modulo/libs.js/ <- La slash finale è importante
                return $next($request->withUri($newuri), $response);
            } else
            {

                // Se non esiste la lingua genero un errore
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
        }

        return $next($request, $response);
    }

}
