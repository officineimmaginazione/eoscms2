<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;

class LanguageDetect extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $uri = $request->getUri();
        $path = $uri->getPath();
        if ($path == '/')
        {
            $lang = $this->container->get('language');
            if (isset($this->container->get('environment')['HTTP_ACCEPT_LANGUAGE']))
            {
                $accLangList = explode(',', $this->container->get('environment')['HTTP_ACCEPT_LANGUAGE']);
                $isoRedirect = '';
                foreach ($accLangList as $accLang)
                {
                    // Le prime 2 righe sono l'iso: Accept-Language: it,en;q=0.8,es;q=0.5,en-US;q=0.3
                    $lfind = $lang->getLangFromISO(substr($accLang, 0, 2));
                    if ($lfind !== FALSE)
                    {
                        $isoRedirect = $lfind->iso;
                        break;
                    }
                }

                if (!empty($isoRedirect) && ($this->container['currentParams']['dbRouter'] = true))
                {
                    // Potrebbe succedere che sul DB la lingua è attiva ma non esiste la pagina
                    // per sicurezza verifico che esista la route
                    $rd = new \EOS\System\Routing\Manager($this->container);
                    if (is_null($rd->getRoute('', $isoRedirect)))
                    {
                        $isoRedirect = '';
                    }
                }

                if (!empty($isoRedirect))
                {
                    return $response->withRedirect($uri->withPath(PathHelper::addTrailingSlash($isoRedirect)), 302);
                }
            }
        }

        return $next($request, $response);
    }

}
