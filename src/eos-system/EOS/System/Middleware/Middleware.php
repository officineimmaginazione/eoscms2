<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

abstract class Middleware
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    abstract public function __invoke(Request $request, Response $response, callable $next);
}
