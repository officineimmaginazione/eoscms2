<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Profiler extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $response = $next($request, $response);
        if /* (!(\EOS\System\Util\MessageHelper::isContentTypeTextHtml($response)) && */
        ($this->container['profiler'] != null) //)
        {
            $response = $response->withHeader('EOS-Profiler', str_replace(["\n", "\r"], " ", $this->container['profiler']->toString()));
        }
        return $response;
    }

}
