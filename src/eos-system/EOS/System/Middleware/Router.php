<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;

class Router extends \EOS\System\Middleware\Middleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $uri = $request->getUri();
        $lang = $this->container->get('language');
        $rd = new \EOS\System\Routing\Manager($this->container);
        // Trasformo spazio(%20) o altri caratteri speciali anche se non dovrei averne
        $currentPath = StringHelper::decodeUrl($uri->getPath($uri));
        $newPath = $rd->getRoute($currentPath, $lang->getCurrent()->iso);
        if (!is_null($newPath))
        {
            $newuri = $uri->withPath(PathHelper::removeLeadingSlash(PathHelper::addTrailingSlash($newPath)));
            return $next($request->withUri($newuri), $response);
        } else
        {
            // Cerco se ho un router container eliminado l'ultimo segmento
            if (!empty($uri))
            {
                $seg = PathHelper::explodeSlash($currentPath);
                if (count($seg) > 0)
                {
                    array_pop($seg);
                    $newPath = $rd->getRouteContainer(PathHelper::implodeSlash($seg), $lang->getCurrent()->iso);
                    if (!is_null($newPath))
                    {
                        $compPath = PathHelper::explodeSlash($newPath);
                        if (count($compPath) > 1)
                        {
                            // Devo calcolare il nome reale del componente altrimenti i loader su linux non trova la classe
                            $currParams = $this->container->get('currentParams');
                            $realCompName = PathHelper::resolveComponentName($compPath[0], $currParams['componentsPath'], $currParams['componentsOverridePath']);
                            $routerClass = sprintf("EOS\Components\%s\Router", $realCompName);
                            if (class_exists($routerClass))
                            {
                                $rt = new $routerClass($this->container);
                                $isRedirect = false;
                                if ($rt->resolveUrl($currentPath, $newPath, $newPath, $isRedirect))
                                {
                                    if ($isRedirect)
                                    {
                                        return $response->withRedirect((string) $newPath, 301);
                                    } else
                                    {
                                        $newuri = $uri->withPath(PathHelper::removeLeadingSlash(PathHelper::addTrailingSlash($newPath)));
                                        return $next($request->withUri($newuri), $response);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $next($request, $response);
        }
    }

}
