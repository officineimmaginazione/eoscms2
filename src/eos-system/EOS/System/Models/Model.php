<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Models;

class Model
{

    protected $container;
    protected $currentParams;
    public $db;
    public $lang;

    public function __construct($container)
    {
        $this->container = $container;
        $this->currentParams = $container['currentParams'];
        $this->db = $container->get('database');
        $this->lang = $container->get('language');
    }

    public function getEventDispatcher($name = null)
    {
        return $this->container->get('eventDispatcherManager')->getDispatcher($name);
    }

}
