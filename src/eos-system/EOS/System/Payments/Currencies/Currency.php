<?php

namespace EOS\System\Payments\Currencies;

class Currency
{

    public const SYMBOL_BEFORE = 0;
    public const SYMBOL_AFTER = 1;

    public $decimals;
    public $decimalPoint;
    public $thousandsSeparator;
    public $symbol;
    public $symbolPosition;

    public function __construct($container)
    {
        $db = $container->get('database');
        $this->decimals = $db->setting->getInt('system', 'currency.decimals', 2);
        $this->decimalPoint = $db->setting->getStr('system', 'currency.decimalpoint', ',');
        $this->thousandsSeparator = $db->setting->getStr('system', 'currency.thousandsseparator', '.');
        $this->symbol = $db->setting->getStr('system', 'currency.symbol', '€');
        $this->symbolPosition = $db->setting->getStr('system', 'currency.symbolposition', static::SYMBOL_BEFORE);
    }

    public function format(float $value): string
    {
        $res = number_format($value, $this->decimals, $this->decimalPoint, $this->thousandsSeparator);
        switch ($this->symbolPosition)
        {
            case static::SYMBOL_AFTER:
                $res = $res . ' ' . $this->symbol;
                break;
            default:
                $res = $this->symbol . ' ' . $res;
                break;
        }
        return $res;
    }

}
