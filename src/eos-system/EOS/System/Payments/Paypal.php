<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Payments;

use EOS\System\Util\ArrayHelper;

class Paypal
{

    private $sandbox = false;
    private $IgfsLangID = 'IT'; //ISO- IT
    private $path;
    private $testurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    private $liveurl = 'https://www.paypal.com/cgi-bin/webscr'; //https://www.paypal.com/cgi-bin/webscr
    private $ipnurl = "https://ipnpb.paypal.com/cgi-bin/webscr";
    private $business = "sandbox@paypal.com";
    private $last_error;
    private $ipn_response;
    private $ipn_data = array();

    function __construct($container)
    {
        $setting = json_decode($container->get('database')->setting->getStr('order', 'payment.paypal'), true);
        $this->sandbox = ArrayHelper::getValue($setting, 'sandbox');
        $this->apiuser = ArrayHelper::getStr($setting, 'apiuser');
        $this->business = ArrayHelper::getStr($setting, 'business');
        $this->liveCurrencyCode = 'EUR'; //ISO - EUR
        $this->IgfsLangID = 'IT';
        $this->path = $container->get('path');
    }

    public function paymentPaypal($paymentData = array())
    {
        $query = array();
        $url = \EOS\System\Routing\PathHelper::removeTrailingSlash($this->path->getBaseUrl());
        $this->setCookiesValue('payment', 'paypal');
        $query['lc'] = 'GB';
        $query['return'] = $url . $this->path->getUrlFor('order', 'order/confirm');
        $query['notify_url'] = $url . $this->path->getUrlFor('order', 'order/paypalipn');
        $query['cancel_return'] = $url . $this->path->getUrlFor('cart', 'cart/index');
        $query['cmd'] = '_cart';
        $query['upload'] = '1';
        $query['currency_code'] = 'EUR';
        $query['no_note'] = 1;
        $query['custom'] = $paymentData['cartid'];
        $query['discount_amount_cart'] = $paymentData['discount'];
        $query['business'] = $this->business;
        $query['address_override'] = 1;
        $query['<first_name>'] = $paymentData['name'];
        $query['<last_name>'] = $paymentData['surname'];
        $query['<email>'] = $paymentData['email'];
        $query['<address1>'] = ArrayHelper::getStr($paymentData, 'shipping-name');
        $query['<city>'] = ArrayHelper::getStr($paymentData, 'shipping-city');
        $query['<state>'] = ArrayHelper::getStr($paymentData, 'shipping-province');
        $query['<zip>'] = ArrayHelper::getStr($paymentData, 'shipping-zipcode');
        //$query['country'] = 'IT';
        if($paymentData['lc'] == 'it') {
            $query['lc'] = 'IT';
        }
        $query['amount'] = number_format($paymentData['amount'], 2);
        $query['charset'] ="utf-8";
        $i = 1;
        foreach ($paymentData['cart-product'] as $row)
        {
            $query['item_name_' . $i] = $row['name'];
            $query['item_number_' . $i] = $row['id'];
            $query['amount_' . $i] = number_format($row['price'], 2);
            $query['quantity_' . $i] = $row['quantity'];
            $i++;
        }
        // Prepare query string
        $query_string = http_build_query($query);
        if ($this->sandbox)
        {
            $url = $this->testurl;
        } else
        {
            $url = $this->liveurl;
        }
        $result['url'] = $url . '?' . $query_string;
        $result['result'] = true;
        return $result;
    }

    function validate_ipn()
    {

        if ($this->sandbox)
        {
            $url_parsed = parse_url($this->testurl);
        } else
        {
            $url_parsed = parse_url($this->ipnurl);
        }
        $post_string = '';
        foreach ($_POST as $field => $value)
        {
            $this->ipn_data["$field"] = $value;
            $post_string .= $field . '=' . urlencode(stripslashes($value)) . '&';
        }
        $post_string .= "cmd=_notify-validate"; // append ipn command
        // open the connection to paypal
        $fp = fsockopen("ssl://" . $url_parsed['host'],"443",$err_num,$err_str,30);
        //$fp = fsockopen($url_parsed[host], "80", $err_num, $err_str, 30);
        //$fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);
        if (!$fp)
        {

            // could not open the connection.  If loggin is on, the error message
            // will be in the log.
            $result['error'] = "fsockopen error no. $errnum: $errstr";
            error_log("fsockopen error no. $errnum: $errstr");
            return false;
        } else
        {
            // Post the data back to paypal
            fputs($fp, "POST $url_parsed[path] HTTP/1.1\r\n");
            fputs($fp, "Host: $url_parsed[host]\r\n"); 
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($post_string) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $post_string . "\r\n\r\n");

            // loop through the response from the server and append to variable
            while (!feof($fp))
            {
                $this->ipn_response .= fgets($fp, 1024);
            }
            fclose($fp); // close connection
        }

        if (preg_match( "~VERIFIED~i", $this->ipn_response))
        {
            // Valid IPN transaction.
            $result['ipn_data'] = $this->ipn_data;
            $result['result'] = true;
        } else
        {
            // Invalid IPN transaction.  Check the log for details.
            $this->last_error = 'IPN Validation Failed.';
            $result['error'] = 'IPN Validation Failed.';
            error_log($this->ipn_response);
            $result['result'] = false;
        }
        return $result;
    }
    
    protected function getCookiesValue($key = null)
    {

        if (isset($_COOKIE[$key]))
        {
            return $_COOKIE[$key];
        } else
        {
            return false;
        }
    }

    protected function setCookiesValue($cookie_name = null, $cookie_value = null)
    {

        if ($cookie_value != null)
        {
            setcookie($cookie_name, $cookie_value, time() + (3600 * 30), "/"); // 3600 = 1 hour
        }
    }

    public function deleteCookiesValue($key = null)
    {
        setcookie($key, "", time() - (8400 * 30), "/"); // 3600 = 1 hour	
    }

}
