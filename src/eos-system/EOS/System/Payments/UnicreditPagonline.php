<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Payments;

use EOS\System\Util\ArrayHelper;

class UnicreditPagonline
{

    private $ssl = '';
    private $testmode = '';
    private $testurl = '';
    private $liveurl = '';
    private $IgfsNotifyURL = '';
    private $IgfsErrorURL = '';
    private $IgfsTimeout = 1500;
    private $testTreminalId = '';
    private $testApikSig = '';
    private $testShopUserRef = '';
    private $liveTreminalId = '';
    private $liveApikSig = '';
    private $liveShopUserRef = '';
    private $IgfsCurrencyCode = 'EUR'; //ISO - EUR
    private $IgfsLangID = 'IT'; //ISO- IT
    private $path;

    function __construct($container)
    {
        $setting = json_decode($container->get('database')->setting->getStr('order', 'payment.unicredit'), true);
        $this->ssl = ArrayHelper::getValue($setting, 'ssl');
        $this->testmode = ArrayHelper::getValue($setting, 'sandbox-status');
        $this->testurl = ArrayHelper::getStr($setting, 'sandbox-urlserver');
        $this->liveurl = ArrayHelper::getStr($setting, 'live-urlserver');
        $this->testTreminalId = ArrayHelper::getStr($setting, 'sandbox-terminalid');
        $this->testApikSig = ArrayHelper::getStr($setting, 'sandbox-requestkey');
        $this->testShopUserRef = ArrayHelper::getStr($setting, 'sandbox-userref');
        $this->liveTreminalId = ArrayHelper::getStr($setting, 'live-terminalid');
        $this->liveApikSig = ArrayHelper::getStr($setting, 'live-requestkey');
        $this->liveShopUserRef = ArrayHelper::getStr($setting, 'live-userref');
        $this->liveCurrencyCode = 'EUR'; //ISO - EUR
        $this->IgfsLangID = 'IT';
        $this->path = $container->get('path');
    }

    public function payment($paymentData = array())
    {
        $init = new \IgfsCgInit();
        if ($this->testmode == true)
        {
            $init->serverURL = $this->testurl;
            $init->tid = $this->testTreminalId;
            $init->kSig = $this->testApikSig;
            $init->shopUserRef = $this->testShopUserRef;
            $init->disableCheckSSLCert();
        } else
        {
            $init->serverURL = $this->liveurl;
            $init->tid = $this->liveTreminalId;
            $init->kSig = $this->liveApikSig;
            $init->shopUserRef = $this->liveShopUserRef;
        }
        $this->setCookiesValue('cart_id', $paymentData['cart_id']);
        $this->setCookiesValue('shopid', $paymentData['cart_id'] . date("YmdHis"));
        $init->timeout = $this->IgfsTimeout;
        $init->shopID = $paymentData['cart_id'] . date("YmdHis");
        $init->trType = "PURCHASE";
        $init->currencyCode = $this->IgfsCurrencyCode; //iso_code;
        $init->amount = str_replace('.', '', number_format($paymentData['amount'], 2, '.', ''));
        $init->langID = $this->IgfsLangID;
        $url = \EOS\System\Routing\PathHelper::removeTrailingSlash($this->path->getBaseUrl());
        $init->notifyURL = $url . $this->path->getUrlFor('order', 'order/verify');
        $init->errorURL = $url . $this->path->getUrlFor('order', 'order/verify');
        $result = [];
        if (!$init->execute())
        {
            //assign error 
            $result['error'] = $init->rc . '<br>' . $init->errorDesc;
            $result['result'] = false;
        } else
        {
            $payment_id = $init->paymentID;
            $this->setCookiesValue('payment_id', $payment_id);
            $result['url'] = $init->redirectURL;
            $result['result'] = true;
        }
        return $result;
    }

    public function verifyPayment()
    {
        $verify = new \IgfsCgVerify();
        $payment_id = $this->getCookiesValue('payment_id');
        $cart_id = $this->getCookiesValue('cart_id');
        $shopid = $this->getCookiesValue('shopid');
        if ($payment_id)
        {
            if ($this->testmode == true)
            {
                $verify->serverURL = $this->testurl;
                $verify->tid = $this->testTreminalId;
                $verify->kSig = $this->testApikSig;
                $verify->disableCheckSSLCert();
            } else
            {
                $verify->serverURL = $this->liveurl;
                $verify->tid = $this->liveTreminalId;
                $verify->kSig = $this->liveApikSig;
            }
            $verify->timeout = $this->IgfsTimeout;
            $verify->shopID = $shopid;
            $verify->paymentID = $payment_id;
            $result = [];
            if ($verify->execute())
            {
                $this->deleteCookiesValue('payment_id');
                $result['result'] = true;
                $result['paymentid'] = $verify->paymentID;
                $result['txn-id'] = $verify->tranID;
                return $result;
            }
            else {
                $result['result'] = false;
                $result['error'] = $verify->errorDesc;
            }
        }
        return $result['result'];
    }

    protected function getCookiesValue($key = null)
    {

        if (isset($_COOKIE[$key]))
        {
            return $_COOKIE[$key];
        } else
        {
            return false;
        }
    }

    protected function setCookiesValue($cookie_name = null, $cookie_value = null)
    {

        if ($cookie_value != null)
        {
            setcookie($cookie_name, $cookie_value, time() + (3600 * 30), "/"); // 3600 = 1 hour
        }
    }

    public function deleteCookiesValue($key = null)
    {
        setcookie($key, "", time() - (8400 * 30), "/"); // 3600 = 1 hour	
    }

}
