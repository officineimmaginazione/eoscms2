<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Payments;

use EOS\System\Util\ArrayHelper;
use EOS\System\Util\DateTimeHelper;

class XPay
{

    private $url = '';
    private $urlpost = '';
    private $urlback = '';
    private $urlerror = '';
    private $alias = '';
    private $mac = '';
    private $CurrencyCode = 'EUR'; //ISO - EUR
    private $LangID = 'ITA'; //ISO- IT
    private $path;

    function __construct($container)
    {
        $this->path = $container->get('path');
        $setting = json_decode($container->get('database')->setting->getStr('order', 'payment.xpay'), true);
        $url = \EOS\System\Routing\PathHelper::removeTrailingSlash($this->path->getBaseUrl());
        $this->url = ArrayHelper::getValue($setting, 'url');
        $this->urlsuccess = $url . $this->path->getUrlFor('order', 'order/confirm');
        $this->urlpost = $url . $this->path->getUrlFor('order', 'order/verify');
        $this->urlback = $url . $this->path->getUrlFor('order', 'cart/index');
        $this->urlerror = $url . $this->path->getUrlFor('order', 'cart/index');
        $this->alias = ArrayHelper::getValue($setting, 'alias');
        $this->mac = ArrayHelper::getValue($setting, 'mac');
        $this->CurrencyCode = 'EUR'; //ISO - EUR
        $this->path = $container->get('path');
    }

    public function payment($paymentData = array())
    {
        $this->setCookiesValue('cart_id', $paymentData['cartid']);
        $this->setCookiesValue('shopid', $paymentData['cartid'] . date("YmdHis"));
        /* $date = DateTimeHelper::now();
          $idOrder = $paymentData['cartid'] . date("YmdHis");
          $amount = intval(strval($paymentData['amount'] * 100));
          $email = $paymentData['email'];
          //$mac = (sha1('codTrans=' . $idOrder . 'esito=OKimporto=' . $amount . 'divisa=' . $this->CurrencyCode . 'data=' . date("Ymd")  . 'orario=' . date("His") . 'codAut=TESTOK' . $this->encodesha1));
          $mac = sha1("codTrans=" . $idOrder . "divisa=EURimporto=" . $amount . $this->encodesha1);
          $result['url'] = $this->url . "?alias=" . $this->alias . "&importo=" . $amount . "&divisa=" . $this->CurrencyCode . "&codTrans=" . $idOrder . "&mail=" . $email . "&url=" . $this->urlsuccess . "?utm_nooverride=1&urlpost=" . $this->urlpost . "&url_back=" . $this->urlback . "&urlerror=" . $this->urlerror . "?utm_nooverride=1&mac=" . $mac . "&languageId=" . $this->LangID;
          $result['result'] = true;
          return $result; */


        $requestUrl = $this->url;
        $codTrans = $paymentData['cartid'] .'-'. date("YmdHis");
        $importo = intval(strval($paymentData['amount'] * 100));
        $chiaveSegreta = $this->mac;
        $divisa = "EUR";

        //CALCOLO MAC
        $mac = sha1('codTrans=' . $codTrans . 'divisa=' . $divisa . 'importo=' . $importo . $chiaveSegreta);
        
        /*print_r('codTrans=' . $codTrans . 'divisa=' . $divisa . 'importo=' . $importo . $chiaveSegreta);
        print_r($mac); exit();*/

        //Param Obbligatori
        $requestParams = array(
            'importo' => $importo,
            'alias' => $this->alias,
            'divisa' => $divisa,
            'codTrans' => $codTrans,
            'url' => $this->urlsuccess,
            'url_back' => $this->urlback,
            'mac' => $mac,
            'urlpost' => $this->urlpost
        );


        $aRequestParams = array();
        foreach ($requestParams as $param => $value)
        {
            $aRequestParams[] = $param . "=" . $value;
        }

        $stringRequestParams = implode("&", $aRequestParams);

        $result['url'] = $requestUrl . "?" . $stringRequestParams;
        $result['result'] = true;
        return $result;
    }

    public function verifyPayment($datarequest)
    {
        $result = [];
        $result['result'] = true;
        $requiredParams = array('codTrans', 'esito', 'importo', 'divisa', 'data', 'orario', 'codAut', 'mac');
        foreach ($requiredParams as $param)
        {
            if (!isset($datarequest[$param]))
            {
                $result['message'] = 'Paramentro mancante';
                $result['result'] = false;
            }
        }

        if($result['result'] != false) {
            //CALCOLO MAC CON I PARAMETRI DI RITORNO
            $macCalculated = sha1('codTrans=' . $datarequest['codTrans'] .
                'esito=' . $datarequest['esito'] .
                'importo=' . $datarequest['importo'] .
                'divisa=' . $datarequest['divisa'] .
                'data=' . $datarequest['data'] .
                'orario=' . $datarequest['orario'] .
                'codAut=' . $datarequest['codAut'] .
                $this->mac
            );


            //VERIFICO CORRISPONDENZA TRA MAC CALCOLATO E MAC DI RITORNO
            if ($macCalculated != $datarequest['mac'])
            {
                $result['message'] = 'S2S errore MAC: ' . $macCalculated . ' NON CORRISPONDENTE A ' . $datarequest['mac'];
                $result['result'] = false;
            }

            //NEL CASO IN CUI NON CI SIANO ERRORI GESTISCO IL PARAMETRO esito
            if ($datarequest['esito'] == 'OK')
            {
                $result['message'] =  'La transazione ' . $datarequest['codTrans'] . " è avvenuta con successo; codice autorizzazione: " . $datarequest['codAut'];
                $result['result'] = true;
            } else
            {
                $result['message'] =  'La transazione ' . $datarequest['codTrans'] . " è stata rifiutata; descrizione errore: " . $datarequest['messaggio'];
                $result['result'] = false;
            }

        }
        //$this->view->paymentResult = $datarequest->get('esito');
        return $result;
    }

    protected function getCookiesValue($key = null)
    {

        if (isset($_COOKIE[$key]))
        {
            return $_COOKIE[$key];
        } else
        {
            return false;
        }
    }

    protected function setCookiesValue($cookie_name = null, $cookie_value = null)
    {

        if ($cookie_value != null)
        {
            setcookie($cookie_name, $cookie_value, time() + (3600 * 30), "/"); // 3600 = 1 hour
        }
    }

    public function deleteCookiesValue($key = null)
    {
        setcookie($key, "", time() - (8400 * 30), "/"); // 3600 = 1 hour	
    }

}
