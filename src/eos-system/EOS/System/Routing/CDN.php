<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Routing;

use EOS\System\Util\TagParser;
use EOS\System\Util\StringHelper;
use EOS\System\Util\MessageHelper;

class CDN
{

    private $container;
    private $request;
    private $response;
    public $tags = [];

    private function updateValueWithCdn(&$value, $baseName, $cdnPath)
    {
        $res = false;
        // Non supporto i // perché credo siano url esterni come http
        if ((!StringHelper::startsWith($value, 'http')) && (!StringHelper::startsWith($value, '//')))
        {
            if (StringHelper::startsWith($value, '/'))
            {
                $value = $cdnPath . $value;
            } else
            {
                $value = $cdnPath . $baseName . $value;
            }
            $res = true;
        }
        return $res;
    }

    private function updateStyleValueWithCdn(&$value, $baseName, $cdnPath)
    {
        $updated = false;
        $items = explode(';', $value);
        if (!empty($items))
        {
            foreach ($items as &$item)
            {
                $st = explode(':', $item);
                if (count($st) > 1)
                {
                    $stName = trim($st[0]);
                    if ($stName === 'background-image')
                    {
                        $stValue = trim($st[1]);
                        if ((!empty($stValue)) && StringHelper::startsWith($stValue, 'url(') && StringHelper::endsWith($stValue, ')'))
                        {
                            $stValue = substr($stValue, 4, strlen($stValue) - 5);
                            $isQuoted = StringHelper::startsWith($stValue, '\'') && StringHelper::endsWith($stValue, '\'');
                            if ($isQuoted)
                            {
                                $stValue = substr($stValue, 1, strlen($stValue) - 2);
                            }
                            if ($this->updateValueWithCdn($stValue, $baseName, $cdnPath))
                            {
                                if ($isQuoted)
                                {
                                    $stValue = '\'' . $stValue . '\'';
                                }
                                $item = $stName . ': url(' . $stValue . ')';
                                $updated = true;
                                break;
                            }
                        }
                    }
                }
            }
            if ($updated)
            {
                $value = implode('; ', $items);
            }
        }
        return $updated;
    }

    private function explodeTagAndAttributes($str, $acceptTags)
    {
        $res = [];
        $offset = strpos($str, ' ');
        if ($offset !== FALSE)
        {
            // 1 Tag
            $tag = substr($str, 0, $offset);
            if (isset($acceptTags[$tag]))
            {
                $res[] = $tag;
                while ($offset < strlen($str))
                {
                    $newOffset = strpos($str, ' ', $offset);
                    if ($newOffset !== FALSE)
                    {
                        $offset = $newOffset + 1;
                        $newOffset = strpos($str, '="', $offset);
                        if ($newOffset !== FALSE)
                        {
                            $newOffset = strpos($str, '"', $newOffset + 2);
                        }
                    }

                    if ($newOffset === FALSE)
                    {
                        $res[] = substr($str, $offset);
                        $offset = strlen($str);
                    } else
                    {
                        $res[] = substr($str, $offset, $newOffset - $offset + 1);
                        $offset = $newOffset + 1;
                    }
                }
            }
        }
        return $res;
    }

    public function __construct($container, $request, $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->container = $container;
        $this->tags['link'] = 'href';
        $this->tags['script'] = 'src';
        $this->tags['img'] = 'src';
        // Tag con eventuale background-image
        $this->tags['body'] = 'style';
        $this->tags['a'] = 'style';
        $this->tags['div'] = 'style';
        $this->tags['section'] = 'style';
        $this->tags['header'] = 'style';
        $this->tags['main'] = 'style';
        $this->tags['footer'] = 'style';
        $this->tags['article'] = 'style';
    }

    public function parseCdnString($html, $baseName, $cdnPath)
    {
        $tp = new TagParser();
        $tp->tagOpen = '<';
        $tp->tagClose = '>';
        $tp->value = $html;
        $tags = $this->tags;
        // Aggiungo/Rimuovo le slash così da non farlo nel parser
        $baseNameCb = PathHelper::addSlash($baseName);
        $cdnPathCb = PathHelper::removeTrailingSlash($cdnPath);
        $tp->callBack = function ($command, &$result) use ($tags, $tp, $baseNameCb, $cdnPathCb)
        {
            $attributes = $this->explodeTagAndAttributes($command, $tags);
            if (!empty($attributes))
            {
                $isChanged = false;
                for ($i = 1; $i < count($attributes); $i++)
                {
                    $attr = explode('=', $attributes[$i]);
                    if (count($attr) > 1)
                    {
                        $attrName = $attr[0];
                        if ($attrName === $tags[$attributes[0]])
                        {
                            $attrValue = trim($attr[1], '"');
                            if (!empty($attrValue))
                            {
                                if ($attrName === 'style')
                                {
                                    $updated = $this->updateStyleValueWithCdn($attrValue, $baseNameCb, $cdnPathCb);
                                } else
                                {
                                    $updated = $this->updateValueWithCdn($attrValue, $baseNameCb, $cdnPathCb);
                                }
                                if ($updated)
                                {
                                    $attributes[$i] = $attrName . '="' . $attrValue . '"';
                                    $isChanged = true;
                                }
                            }
                        }
                    }
                }
                if ($isChanged)
                {
                    $result = $tp->tagOpen . implode(' ', $attributes) . $tp->tagClose;
                }
            }
        };
        $tp->parse();
        return $tp->result;
    }

    public function render()
    {
        $res = $this->response;
        if ((MessageHelper::isContentTypeTextHtml($res)) && ($res->getStatusCode() == 200))
        {
            $cdnPath = $this->container->get('database')->setting->getStr('system', 'cdn');
            if (!empty($cdnPath))
            {
                $basePath = $this->container->get('path')->getSiteUrl();
                $html = (string) $res->getBody();
                $body = $res->getBody();
                $body->rewind();
                $body->write($this->parseCdnString($html, $basePath, $cdnPath));
            }
        }
        return $res;
    }

}
