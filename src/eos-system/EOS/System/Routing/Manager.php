<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Routing;

use EOS\System\Util\StringHelper;

class Manager
{

    private $_db;
    private $_lang;
    
    public $userID = 0;

    public function __construct($container)
    {
        $this->_db = $container->get('database');
        $this->_lang = $container->get('language');
    }

    public function getPath($route, $lang)
    {
        if (is_null($route))
        {
            return null;
        } else
        {
            if (empty($lang))
            {
                $lang = $this->_lang->getCurrentISO();
            }
            $q = $this->_db->prepare('select path from #__router where route = :route and lang = :lang');
            $q->bindValue(':route', PathHelper::removeSlash($route));
            $q->bindValue(':lang', PathHelper::removeSlash($lang));
            $q->execute();
            $r = $q->fetch();
            if (!empty($r))
            {
                return $r['path'];
            } else
            {
                return null;
            }
        }
    }

    public function getRoute($path, $lang)
    {
        if (is_null($path))
        {
            return null;
        } else
        {
            if (empty($lang))
            {
                $lang = $this->_lang->getCurrentISO();
            }
            $q = $this->_db->prepare('select route from #__router where path = :path and lang = :lang');
            $q->bindValue(':path', PathHelper::removeSlash($path));
            $q->bindValue(':lang', PathHelper::removeSlash($lang));
            $q->execute();
            $r = $q->fetch();
            if (!empty($r))
            {
                return $r['route'];
            } else
            {
                return null;
            }
        }
    }

    public function getRouteContainer($path, $lang)
    {
        if (is_null($path))
        {
            return null;
        } else
        {
            if (empty($lang))
            {
                $lang = $this->_lang->getCurrentISO();
            }
            $q = $this->_db->prepare('select route from #__router where path = :path and lang = :lang and container = 1');
            $q->bindValue(':path', PathHelper::removeSlash($path));
            $q->bindValue(':lang', PathHelper::removeSlash($lang));
						$q->execute();
            $r = $q->fetch();
            if (!empty($r))
            {
                return $r['route'];
            } else
            {
                return null;
            }
        }
    }

    public function setRoute($route, $lang, $path, $container, $menu)
    {
        $this->deleteRoute($route, $lang);
        $q = $this->_db->prepare('insert into  #__router (route, lang, path, container, menu, ins_id, ins_date, up_id, up_date) values (:route, :lang, :path, :container, :menu, :ins_id, :ins_date, :up_id, :up_date)');
        $q->bindValue(':route', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route)));
        $q->bindValue(':lang', PathHelper::removeSlash($lang));
        $q->bindValue(':path', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($path)));
        $q->bindValue(':container', (int) $container);
        $q->bindValue(':menu', (int) $menu);
        $q->bindValue(':ins_id', $this->userID);
        $q->bindValue(':ins_date', $this->_db->util->nowToDBDateTime());
        $q->bindValue(':up_id', $this->userID);
        $q->bindValue(':up_date', $this->_db->util->nowToDBDateTime());
        $q->execute();
    }

    public function deleteRoute($route, $lang)
    {
        if (!is_null($route))
        {
            $q = $this->_db->prepare('delete from #__router where (route = :route) and (lang = :lang)');
            $q->bindValue(':route', StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route)));
            $q->bindValue(':lang', PathHelper::removeSlash($lang));
            $q->execute();
        }
    }

    public function isValidRoutePath($route, $lang, $path)
    {
        $r = $this->getRoute($path, $lang);
        return (is_null($r) || ($r == StringHelper::sanitizeUrlSegment(PathHelper::removeSlash($route))));
    }

}
