<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Routing;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;

class Path
{

    protected $container;
    protected $currentParams;

    public function __construct($container)
    {
        $this->container = $container;
        $this->currentParams = $container['currentParams'];
    }

    public function getSelfUrl()
    {
        //$s = $_SERVER['PHP_SELF'];
        //$s = filter_input(INPUT_SERVER, 'PHP_SELF');
        $s = $this->container->get('environment')['PHP_SELF'];
        return PathHelper::addTrailingSlash(dirname($s));
    }

    public function getPageUrl()
    {
        //  return $this->container->get('environment')['REDIRECT_URL']; // Non funziona in CGI
        $res = $this->container->get('environment')['REQUEST_URI'];
        $pParams = strpos($res, '?');
        if ($pParams !== false)
        {
            $res = substr($res, 0, $pParams);
        }
        return $res;
    }

    // Visualizza url corrente come root del modulo (www.ecosoft.it/admin => /admin/)
    // se sono nella index.php del sito ma nella cartella superiore ottendo /site/
    // Usare SELF Url per avere la root rispetto alla index
    public function getAppUrl()
    {
        $add = '';

        if (_EOS_PATH_FOLDER_ != '')
        {
            $add = PathHelper::addTrailingSlash(_EOS_PATH_FOLDER_);
        }

        return $this->getSelfUrl() . $add;
    }

    // Visualizza url corrente come root dell dominio (www.ecosoft.it/ => /)	
    public function getSiteUrl()
    {
        $url = $this->getSelfUrl();
        if (_EOS_PATH_FOLDER_ != '')
            $add = '';
        else
            $add = $this->extractUrl(_EOS_PATH_APP_);

        $url = substr($url, 0, strlen($url) - strlen($add));
        if (substr($url, -1, 1) != '/')
            $url .= '/';
        return $url;
    }

    public function isHttps()
    {
        //if (isset($_SERVER['HTTPS']))
        //if (filter_input(INPUT_SERVER, 'HTTPS') != null)
        return isset($this->container->get('environment')['HTTPS']);
    }

    // Visualizza url solo dominio
    public function getDomainUrl()
    {

        if ($this->isHttps())
        {
            $protocol = 'https://';
        } else
        {
            $protocol = 'http://';
        }
        // $protocol .= $_SERVER['HTTP_HOST'];
        // $protocol .= filter_input(INPUT_SERVER, 'HTTP_HOST');
        $protocol .= $this->container->get('environment')['HTTP_HOST'];
        return $protocol;
    }

    // Visualizza url corrente come root del dominio completo di tutto
    public function getBaseUrl()
    {
        return $this->getDomainUrl() . $this->getSiteUrl();
    }

    public function getLibraryUrl()
    {
        if (substr(_EOS_URL_LIBRARY_, 0, 4) != 'http')
            return $this->getSiteUrl() . PathHelper::addTrailingSlash(_EOS_URL_LIBRARY_);
        else
            return _EOS_URL_LIBRARY_;
    }

    public function extractUrl($path)
    {
        return PathHelper::dirSepToSlash(substr($path, strlen(_EOS_PATH_ROOT_)));
    }

    public function getThemeUrl()
    {
        return $this->getSiteUrl() . $this->extractUrl($this->getThemePath());
    }

    public function getUploadsUrl()
    {
        return $this->extractUrl($this->getUploadsPath());
    }

    public function getUploadsPath()
    {
        return PathHelper::addDirSep(_EOS_PATH_UPLOADS_);
    }

    public function getThemePath()
    {
        $templatePath = $this->currentParams['themesPath'] . $this->currentParams['themeName'];

        return $templatePath . DIRECTORY_SEPARATOR;
    }

    public function getThemeTemplateFile()
    {
        return $this->getThemePath() . $this->currentParams['themeTemplate'] . '.php';
    }

    public function getThemeOverridePath()
    {
        return $this->getThemePath() . 'override' . DIRECTORY_SEPARATOR;
    }

    public function getThemeOverrideComponentPath()
    {
        return $this->getThemeOverridePath() . $this->currentParams['componentName'] . DIRECTORY_SEPARATOR;
    }

    public function getThemeOverrideComponentTemplatesPath()
    {
        return $this->getThemeOverrideComponentPath() . 'templates' . DIRECTORY_SEPARATOR;
    }

    public function getThemeCustomPath()
    {
        return $this->getThemePath() . 'custom' . DIRECTORY_SEPARATOR;
    }

    public function urlFor($component, $params = [], $rawparams = [], $lang = '')
    {
        $router = null;
        $comppath = strtolower($component);
//        // Carico in modo dinamico la classe Router del component se esiste
//        if (($component != '') && ($this->currentParams->has('componentsPath')))
//        {
//            $componentPath = PathHelper::realComponentPath($comppath, $this->currentParams['componentsPath']);
//            $frouter = $componentPath . 'Router.php';
//            if (file_exists($frouter))
//            {
//                $rClass = sprintf('\EOS\Components\%s\Router', basename(PathHelper::removeDirSep($componentPath)));
//                $router = new $rClass($this->container);
//            }
//        }
        $cpm = $this->container['componentManager'];
        if ($cpm->componentExists($component))
        {
            $rClass = sprintf('\EOS\Components\%s\Router', $cpm->getComponent($component)->name);
            if (class_exists($rClass))
            {
                $router = new $rClass($this->container);
            }
        }

        if ($router == null)
        {
            $router = new \EOS\System\Routing\Router($this->container);
        }
        $res = $router->urlFor($comppath, $params, $rawparams, $lang);
        return $res;
    }

    public function getUrlFor($component, $path, $lang = '')
    {
        return $this->urlFor($component, PathHelper::explodeSlash($path), [], $lang);
    }

    public function extraUrlFor($component, $params = [], $lang = '')
    {
        if (($this->currentParams['multiLanguage'] == true) && (empty($lang)))
        {
            $lang = $this->container['language']->getCurrent()->iso;
        }

        $newPath = array_merge([StringHelper::sanitizeUrl(strtolower($component))], $params);
        if (!empty($lang))
        {
            $newPath = array_merge([strtolower($lang)], $newPath);
        }
        $router = new \EOS\System\Routing\Router($this->container);
        $res = $router->arrayToEncString($newPath);
        return $this->getSelfUrl() . $res;
    }

    public function getExtraUrlFor($component, $path, $lang = '')
    {
        return $this->extraUrlFor($component, PathHelper::explodeSlash($path), $lang);
    }

}
