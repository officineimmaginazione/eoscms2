<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Routing;

class PathHelper
{

    public static function dirSepToSlash($text)
    {
        return str_replace(DIRECTORY_SEPARATOR, '/', $text);
    }

    public static function slashToDirSep($text)
    {
        return str_replace('/', DIRECTORY_SEPARATOR, $text);
    }

    public static function addDirSep($str)
    {
        return $str . (substr($str, -1) == DIRECTORY_SEPARATOR ? '' : DIRECTORY_SEPARATOR);
    }

    public static function removeDirSep($str)
    {
        return substr($str, -1) == DIRECTORY_SEPARATOR ? substr($str, 0, strlen($str) - 1) : $str;
    }

    public static function addTrailingSlash($str)
    {
        return $str . (substr($str, -1) == '/' ? '' : '/');
    }

    public static function addLeadingSlash($str)
    {
        return (substr($str, 0, 1) == '/' ? '' : '/') . $str;
    }

    public static function removeTrailingSlash($str)
    {
        return substr($str, -1) == '/' ? substr($str, 0, strlen($str) - 1) : $str;
    }

    public static function removeLeadingSlash($str)
    {
        return substr($str, 0, 1) == '/' ? substr($str, 1) : $str;
    }

    public static function removeSlash($str)
    {
        return static::removeTrailingSlash(static::removeLeadingSlash($str));
    }

    public static function removeExtension($str)
    {
        $ext = pathinfo($str, PATHINFO_EXTENSION);
        return empty($ext) ? $str : substr($str, -strlen($ext) - 1);
    }

    public static function addSlash($str)
    {
        return static::addTrailingSlash(static::addLeadingSlash($str));
    }

    public static function explodeSlash($str)
    {
        return (empty($str)) ? [] : explode('/', static::removeSlash($str));
    }

    public static function explodeDirSep($str)
    {
        return (empty($str)) ? [] : explode(DIRECTORY_SEPARATOR, static::removeDirSep($str));
    }

    public static function implodeSlash($arr)
    {
        return implode('/', $arr);
    }

    public static function implodeDirSep($arr)
    {
        return implode(DIRECTORY_SEPARATOR, $arr);
    }

    private static $_compCache = []; // Cache per ridurre IO

    // Ricerca in modo case-insensitive la cartella di un componente

    public static function realComponentPath($component, $path, $useCache = true)
    {
        //
        $keyCache = strtolower(static::addDirSep($path) . $component);
        if (($useCache) && (isset(static::$_compCache[$keyCache])))
        {
            return static::$_compCache[$keyCache];
        }
        //    
        $compName = ucfirst(basename($component));
        $pathName = static::addDirSep($path);
        if ((!empty($component)) && (is_dir($path)))
        {
            $list = glob($pathName . $compName[0] . '*', GLOB_ONLYDIR);
            if (!empty($list))
            {
                foreach ($list as $item)
                {
                    $item = basename($item);
                    if (strtolower($item) == strtolower($compName))
                    {
                        $compName = $item;
                        break;
                    }
                }
            }
        }
        $res = static::addDirSep($pathName . $compName);
        //
        if ($useCache)
        {
            static::$_compCache[$keyCache] = $res;
        }
        //
        return $res;
    }

    public static function resolveFileName($path, $pathOverride, $fileName)
    {
        if ((!empty($pathOverride)) && (file_exists($pathOverride . $fileName)))
        {
            return $pathOverride . $fileName;
        }
        return $path . $fileName;
    }

    public static function resolveFileNameArray($path, $pathOverrideArray, $fileName)
    {
        foreach ($pathOverrideArray as $pathOverride)
        {
            if ((!empty($pathOverride)) && (file_exists($pathOverride . $fileName)))
            {
                return $pathOverride . $fileName;
            }
        }
        return $path . $fileName;
    }

    public static function resolveComponentName($component, $path, $pathOverride)
    {
        $compPath = '';
        if (!empty($pathOverride))
        {
            $compPath = static::realComponentPath($component, $pathOverride);
            if (!is_dir($compPath))
            {
                $compPath = '';
            }
        }
        if (empty($compPath))
        {
            $compPath = static::realComponentPath($component, $path);
        }
        if (empty($compPath))
        {
            return $component;
        } else
        {
            $pa = static::explodeDirSep($compPath);
            return end($pa);
        }
    }

    public static function resolveComponentList($path, $pathOverride)
    {
        $res = [];
        $list = glob(static::addDirSep($path) . '*', GLOB_ONLYDIR);
        foreach ($list as $item)
        {
            $comp = basename($item);
            $res[] = $comp;
        }
        if ((!empty($pathOverride)) && (is_dir($pathOverride)))
        {
            $list = glob(static::addDirSep($pathOverride) . '*', GLOB_ONLYDIR);
            foreach ($list as $item)
            {
                $comp = basename($item);
                if (!in_array($comp, $res))
                {
                    $res[] = $comp;
                }
            }
        }
        return $res;
    }

}
