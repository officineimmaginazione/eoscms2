<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Routing;

use EOS\System\Util\StringHelper;

class Router
{

  protected $container;
  protected $currentParams;
  protected $path;
  protected $segments;
  protected $component = '';
  protected $lang = '';
  protected $params = [];
  protected $rawparams = [];
  protected $multiLanguage;

  public function __construct($container)
  {
    $this->container = $container;
    $this->currentParams = $container['currentParams'];
    $this->path = $container['path'];
    $this->multiLanguage = $this->currentParams['multiLanguage'];
  }

  public function arrayToEncString($l)
  {
    $r = [];
    foreach ($l as $v)
    {
      $r[] = StringHelper::encodeUrl($v);
    }
    return PathHelper::implodeSlash($r);
  }

  public function urlFor($component, $params, $rawparams = [], $lang = '')
  {
    if ($lang != '')
      $this->multiLanguage = true;
    $res = '';
    $this->segments = [];
    $this->lang = $lang;
    if (($this->lang == '') && ($this->multiLanguage))
      $this->lang = $this->container['language']->getCurrent()->iso;
    $this->params = StringHelper::sanitizeUrlArray($params);
    $this->rawparams = StringHelper::sanitizeUrlArray($rawparams);
    if ($component != '')
      $this->segments[] = $component;
    if (count($this->segments) > 0)
    {
      $this->segments = array_merge($this->segments, $this->params);
      $this->process();

      if (($this->multiLanguage) && ($this->lang != ''))
        $this->segments = array_merge([$this->lang], $this->segments);

      $ext = $this->currentParams['pageExtension'];
      if (($this->multiLanguage) && (count($this->segments) == 1) && (count($this->rawparams) == 0))
      {
         if ($this->segments[0] == $this->lang)   
         {
             $ext = '/'; // La root della lingua deve essere senza estensione
         }
      }
      else
      {
        if ((!$this->multiLanguage) && (empty($this->segments)) && (empty($this->rawparams))) 
        {
           $ext = '';  // La root non deve avere estensione
        }
      }
      
      $res .= $this->arrayToEncString(array_merge($this->segments, $this->rawparams)) . $ext;
    }
    else
    {
      if ($this->multiLanguage)
        $res .= PathHelper::addTrailingSlash($this->lang);
    }
    return $this->path->getSelfUrl() . $res;
  }

  // Metodo da ereditare per ricostruire eventualmente i segmenti
  protected function process()
  {
    if ($this->currentParams['dbRouter'])
    {
      $rm = new Manager($this->container);
      $p = $rm->getPath(PathHelper::implodeSlash($this->segments), $this->lang);
      if (!is_null($p))
      {
        $this->segments = PathHelper::explodeSlash($p);
      }
    }
  }
  
  // Questo metodo viene chiamato dal middleware per risolvere gli url dei container
  public function resolveUrl($currentPath, $containerPath, &$newPath, &$isRedirect)
  {
     return false;  
  }

}
