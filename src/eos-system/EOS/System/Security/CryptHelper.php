<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Security;

class CryptHelper
{

    public static function randomBytes($length)
    {
        if (function_exists('random_bytes')) // PHP 7.x
        {
            $res = random_bytes($length);
        } else if (function_exists('mcrypt_create_iv')) // Server
        {
            $res = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
        } else if (function_exists('openssl_random_pseudo_bytes')) // OpenSSL
        {
            $res = openssl_random_pseudo_bytes($length);
        } else
        {
            $res = '';
        }

        if (empty($res))
        {
            new \Exception('CryptHelper::randomBytes not supported, check server compatibility!');
        }
        return $res;
    }

    public static function randomString($bytesLength)
    {
        return bin2hex(static::randomBytes($bytesLength));
    }

    // Previene i Timing attack in PHP
    public static function hashEquals($known_string, $user_string)
    {
        if (function_exists('hash_equals')) // PHP 5.6 >
        {
            return hash_equals($known_string, $user_string);
        } else
        {
            // Emulate system
            return substr_count($known_string ^ $user_string, "\0") * 2 === strlen($known_string . $user_string);
        }
    }
}
