<?php

namespace EOS\System\Security;

class Password
{

    protected $container;
    private $cost;
    private $salt;

    public function __construct($container)
    {
        $this->container = $container;
        $this->cost = 11;
        $this->salt = 22;
    }

    public function hashPassword($password)
    {
        if (function_exists('random_bytes')) // PHP 7.x
        {
            $options = [
                'cost' => $this->cost];
        } else
        {
            $options = [
                'cost' => $this->cost,
                'salt' => mcrypt_create_iv($this->salt, MCRYPT_ENCRYPT),
            ];
        }
        $passwordHashed = password_hash($password, PASSWORD_BCRYPT, $options);
        return $passwordHashed;
    }

    // Non è super sicura ma dovrebbe bastare con lunghezza
    public function generateRandom($length = 16, $characters = 'lower_case,upper_case,numbers,special_symbols')
    {
        $symbols = array();
        $used_symbols = '';
        $pass = '';

        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = '!?@#-_+$';
        $list = explode(",", $characters); // get characters types to be used for the passsword
        foreach ($list as $value)
        {
            $used_symbols .= $symbols[$value]; // build a string with all characters
        }
        $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

        for ($i = 0; $i < $length; $i++)
        {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
        }

        return $pass; // return the generated password 
    }

}
