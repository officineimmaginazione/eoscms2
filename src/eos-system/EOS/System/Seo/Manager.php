<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Seo;

use EOS\System\Util\ArrayHelper;
use EOS\System\Database\Database;

class Manager
{

    private $_db;
    public $userID = 0;

    public function __construct($container)
    {
        $this->_db = $container->get('database');
    }

    public function getSeo($component, $idobject, $idlang)
    {
        $q = $this->_db->prepare('select id_object, component, id_lang, slug, meta_title, meta_description, keywords, meta_raw ' .
            ' from #__seo where id_object = :id_object and component = :component and id_lang = :id_lang');
        $q->bindValue(':component', $component);
        $q->bindValue(':id_object', (int) $idobject, Database::PARAM_INT);
        $q->bindValue(':id_lang', (int) $idlang, Database::PARAM_INT);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            return null;
        } else
        {
            return $r;
        }
    }

    private function isEmpty($list)
    {
        if (!empty($list))
        {
            foreach ($list as $v)
            {
                if (!empty($v))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public function setSeo($component, $idobject, $idlang, $slug, $meta_title, $meta_description, $keywords, $meta_raw)
    {
        $this->deleteSeo($component, $idobject, $idlang);
        if (!$this->isEmpty([$slug, $meta_title, $meta_description, $keywords, $meta_raw]))
        {
            $q = $this->_db->prepare('INSERT INTO  #__seo ' .
                '(`id_object`,`component`,`id_lang`,`slug`, `meta_title`, `meta_description`, ' .
                '`keywords`, `meta_raw`, ins_id, `ins_date`, up_id, `up_date`) VALUES ' .
                ' (:id_object, :component, :id_lang, :slug, :meta_title, :meta_description, ' .
                ':keywords, :meta_raw, :ins_id, :ins_date, :up_id, :up_date);');
            $q->bindValue(':component', $component);
            $q->bindValue(':id_object', $idobject);
            $q->bindValue(':id_lang', $idlang);
            $q->bindValue(':slug', $slug);
            $q->bindValue(':meta_title', $meta_title);
            $q->bindValue(':meta_description', $meta_description);
            $q->bindValue(':keywords', $keywords);
            $q->bindValue(':meta_raw', $meta_raw);
            $q->bindValue(':ins_id', $this->userID);
            $q->bindValue(':up_id', $this->userID);
            $q->bindValue(':ins_date', $this->_db->util->nowToDBDateTime());
            $q->bindValue(':up_date', $this->_db->util->nowToDBDateTime());
            $q->execute();
        }
    }

    public function deleteSeo($component, $idobject, $idlang)
    {
        $q = $this->_db->prepare('delete from #__seo where id_object = :id_object and component = :component and id_lang = :id_lang');
        $q->bindValue(':component', $component);
        $q->bindValue(':id_object', $idobject, Database::PARAM_INT);
        $q->bindValue(':id_lang', $idlang, Database::PARAM_INT);
        $q->execute();
    }

    private function joinField($source, $idlang, $field, &$array)
    {
        $array['seo-' . $idlang . '-' . $field] = $source[$field];
    }

    public function getSeoArray($component, $idobject, $idlang, &$array)
    {
        $r = $this->getSeo($component, $idobject, $idlang);
        if (!empty($r))
        {
            $this->joinField($r, $idlang, 'slug', $array);
            $this->joinField($r, $idlang, 'meta_title', $array);
            $this->joinField($r, $idlang, 'meta_description', $array);
            $this->joinField($r, $idlang, 'keywords', $array);
            $metaRaw = empty($r['meta_raw']) ? [] : json_decode($r['meta_raw'], true);
            $array['seo-' . $idlang . '-' . 'meta_raw'] = $metaRaw;
        } else
        {
            return null;
        }
    }

    private function getFieldArValue($source, $idlang, $field)
    {
        $field = 'seo-' . $idlang . '-' . $field;

        if (isset($source[$field]))
        {
            return $source[$field];
        } else
        {
            return '';
        }
    }

    private function getFieldArValueAsArray($source, $idlang, $field)
    {
        $field = 'seo-' . $idlang . '-' . $field;

        if (isset($source[$field]))
        {
            if (is_array($source[$field]))
            {
                return $source[$field];
            } else
            {
                return [$source[$field]];
            }
        } else
        {
            return [];
        }
    }

    public function setSeoArray($component, $idobject, $idlang, $source)
    {
        $metaRaw = $this->getFieldArValue($source, $idlang, 'meta_raw');
        if (!empty($metaRaw))
        {
            $metaRawJson = json_decode($metaRaw, true);
            $metaRawArr = [];
            if (empty($metaRawJson))
            {
                $metaRaw = '';
            } else
            {
                foreach ($metaRawJson as $v)
                {
                    $metaName = ArrayHelper::getStr($v, 'name', '');
                    if (!empty($metaName))
                    {
                        $metaRawArr[] = ['type' => ArrayHelper::getInt($v, 'type', 0),
                            'name' => $metaName,
                            'content' => ArrayHelper::getStr($v, 'content', '')];
                    }
                }
                // Ordino per nome per pulizia
                usort($metaRawArr, function($a, $b)
                {
                    return ($a['name'] < $b['name']) ? -1 : 1;
                });
                $metaRaw = json_encode($metaRawArr);
            }
        }
        $this->setSeo($component, $idobject, $idlang, $this->getFieldArValue($source, $idlang, 'slug'), $this->getFieldArValue($source, $idlang, 'meta_title'), $this->getFieldArValue($source, $idlang, 'meta_description'), $this->getFieldArValue($source, $idlang, 'keywords'), $metaRaw);
    }

    private function getValue($source, $field)
    {
        return (isset($source[$field]) && (!empty($source[$field]))) ? $source[$field] : null;
    }

    public function loadSeoView(\EOS\System\Views\Html $view, $component, $idobject, $idlang, $skipTitle = true)
    {
        $s = $this->getSeo($component, $idobject, $idlang);
        if (!is_null($s))
        {
            if (!$skipTitle)
            {
                if (!empty($this->getValue($s, 'meta_title')))
                {
                    $view->title = $this->getValue($s, 'meta_title');
                }
            }
            if (!empty($this->getValue($s, 'meta_description')))
            {
                $view->description = $this->getValue($s, 'meta_description');
            }
            if (!empty($this->getValue($s, 'keywords')))
            {
                $view->keywords = $this->getValue($s, 'keywords');
            }

            if (!empty($this->getValue($s, 'meta_raw')))
            {
                $metaRaw = json_decode($this->getValue($s, 'meta_raw'), true);
                foreach ($metaRaw as $item)
                {
                    $view->addMeta($item['name'], $item['content'], $item['type'] == 1);
                }
            }
        }
    }

    public function joinTitle(\EOS\System\Views\Html $view, $title)
    {
        if ((!empty($view->title)) && (!empty($title)))
        {
            $view->title = $title . ' - ' . $view->title;
        } else
        {
            if (!empty($title))
            {
                $view->title = $title;
            }
        }
    }

}
