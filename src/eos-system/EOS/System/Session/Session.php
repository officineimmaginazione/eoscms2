<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Session;

use EOS\System\Security\CryptHelper;

class Session
{

    protected $container;
    protected $currentParams;
    protected $path;
    public $timeOut;

    public function __construct($container)
    {
        $this->container = $container;
        $this->currentParams = $container['currentParams'];
        $this->path = $container['path'];
        $this->timeOut = 60 * 20; // 20 minuti
        $this->start();
    }

    /*
      private function getAgent()
      {
      $res = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
      if ($res === false)
      {
      $res = '';
      }
      return $res;
      }
     */

    // Imposto i parametri delle sessioni a livello di sistema
    private function setSystemSettings()
    {
        ini_set('session.use_cookies', '1');
        ini_set('session.use_only_cookies', '1');
        ini_set('session.use_trans_sid', '0');
        ini_set('session.cookie_path', $this->path->getSelfUrl());
        ini_set('session.cookie_secure', $this->path->isHttps());
        ini_set('session.cookie_httponly', $this->currentParams['sessionCookieHttpOnly']);
        ini_set('session.gc_maxlifetime', $this->timeOut);
        ini_set('session.gc_probability', 1); // Rafforza parametri di default
        ini_set('session.gc_divisor', 100); // Rafforza parametri di default
    }

    public function start()
    {
        $strongTimeOut = false;
        if (!empty($this->currentParams['sessionTimeOut']))
        {
            $this->timeOut = (int) $this->currentParams['sessionTimeOut'];
            $strongTimeOut = true;
        }
        $sessionname = md5(_EOS_VERSION_ . $this->path->getSelfUrl()/* .$this->getAgent() .$this->getClientIP() */); // Cambio il session name con uno crittografato 
        if (session_id() === '')
        {
            $this->setSystemSettings();
            // I cookie vanno in timeout solo alla chiusura del browser
            if ($strongTimeOut)
            {
                session_set_cookie_params($this->timeOut);
            } else
            {
                session_set_cookie_params(0);
            }
            session_name($sessionname);
            session_start();
        }

        if ($strongTimeOut)
        {
            if (filter_input(INPUT_COOKIE, $sessionname))
            {
                // Riaggiorno il cookie con la nuova data/ora
                setcookie($sessionname, filter_input(INPUT_COOKIE, $sessionname), time() + $this->timeOut, $this->path->getSelfUrl(), '', $this->path->isHttps(), $this->currentParams['sessionCookieHttpOnly']);
            }
        }


        $this->set('__session.timeout', time() + $this->timeOut);
    }

    public function destroy()
    {
        session_unset();
        session_destroy();
    }

    public function set($name, $value = null)
    {
        if ($value == null)
            unset($_SESSION[$name]);
        else
            $_SESSION[$name] = $value;
    }

    public function get($name, $default = null)
    {
        if (isset($_SESSION[$name]))
            return $_SESSION[$name];
        else
            return $default;
    }

    public function has($name)
    {
        return isset($_SESSION[$name]);
    }

    private function newTokenUnique($letterPrefix, $bytesLengthStart, $bytesLengthEnd)
    {
        // Devo iniziare con una lettera così in json serializza in oggetti
        if ($letterPrefix)
        {
            $letters = range('a', 'z');
            $c = $letters[rand(0, count($letters) - 1)];
        } else
        {
            $c = '';
        }
        return $c . CryptHelper::randomString(rand($bytesLengthStart, $bytesLengthEnd));
    }

    /*
     * Creo nome di token random di almeno 40 caratteri per non andare in conflitto con le variabili
     * Questo però verrà confrontato normalmente perché non è il value che da la massima sicurezza e riduco i byte usati
     */

    public function getTokenName($tokenID = '')
    {
        $key = '__session.token.name.' . $tokenID;
        if (!$this->has($key))
        {
            $this->set($key, $this->newTokenUnique(true, 20, 24));
        }
        return $this->get($key);
    }

    /*
      http://stackoverflow.com/questions/6287903/how-to-properly-add-csrf-token-using-php
     *  csrf deve usare un random potente per e devo confrontarlo con hash_equal per i timing attack
     */

    public function getTokenValue($tokenID = '')
    {
        $key = '__session.token.value.' . $tokenID;
        if (!$this->has($key))
        {
            $this->set($key, $this->newTokenUnique(false, 32, 36));
        }
        return $this->get($key);
    }

    public function resetToken($tokenID = '')
    {
        $this->set('__session.token.name.' . $tokenID, null);
        $this->set('__session.token.value.' . $tokenID, null);
    }

    public function isValidToken($name, $value, $tokenID = '')
    {
        return ($this->getTokenName($tokenID) === $name) &&
            (CryptHelper::hashEquals($this->getTokenValue($tokenID), $value));
    }

    public function isValidTokenArrayKey($array, $tokenID = '')
    {
        $k = $this->getTokenName($tokenID);
        return ((isset($array[$k])) && (CryptHelper::hashEquals($array[$k], $this->getTokenValue())));
    }

    public function getSessionID()
    {
        return session_id();
    }

    public function getClientIP()
    {
        $ev = $this->container->get('environment');
        $ipaddress = '';
        if (isset($ev['HTTP_CLIENT_IP']))
            $ipaddress = $ev['HTTP_CLIENT_IP'];
        else if (isset($ev['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $ev['HTTP_X_FORWARDED_FOR'];
        else if (isset($ev['HTTP_X_FORWARDED']))
            $ipaddress = $ev['HTTP_X_FORWARDED'];
        else if (isset($ev['HTTP_X_CLUSTER_CLIENT_IP']))
            $ipaddress = $ev['HTTP_X_CLUSTER_CLIENT_IP'];
        else if (isset($ev['HTTP_FORWARDED_FOR']))
            $ipaddress = $ev['HTTP_FORWARDED_FOR'];
        else if (isset($ev['HTTP_FORWARDED']))
            $ipaddress = $ev['HTTP_FORWARDED'];
        else if (isset($ev['REMOTE_ADDR']))
            $ipaddress = $ev['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
