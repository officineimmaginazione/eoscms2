<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Tasks;

use EOS\System\Util\ArrayHelper;

class Task extends \EOS\System\Models\Model
{

    protected $params;
    protected $debug = false;
    public $logProc = null;

    protected function replacePathTag(string $value): string
    {
        return str_replace(['{uploads}', '{root}'], [$this->path->getUploadsPath(),
            \EOS\System\Routing\PathHelper::addDirSep(_EOS_PATH_ROOT_)], $value);
    }

    protected function load()
    {
        
    }

    public function __construct($container, array $params)
    {
        parent::__construct($container);
        $this->params = $params;
        $this->path = $this->container->get('path');
        $this->debug = ArrayHelper::getBool($this->params, 'debug', $this->debug);
        $this->load();
    }

    public function addLog(string $value)
    {
        if (!is_null($this->logProc))
        {
            call_user_func($this->logProc, $value);
        }
    }

    public function run()
    {
        
    }

}
