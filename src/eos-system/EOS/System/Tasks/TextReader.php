<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Tasks;

use EOS\System\Util\ArrayHelper;

class TextReader extends \EOS\System\Tasks\Task
{

    protected $captureException = false;
    protected $captureExceptionToLog = false;
    protected $exceptionList = [];
    protected $readIdx = -1;

    protected function mapRow(array $row, array $mapping): array
    {
        if (empty($mapping))
        {
            return $row;
        }
        $res = [];

        foreach ($mapping as $k => $v)
        {
            if (isset($v['value']))
            {
                $res[$k] = $v['value'];
            } else
            {
                $pos = ArrayHelper::getInt($v, 'pos', -1);
                $res[$k] = ArrayHelper::getValue($row, $pos, null);
            }
        }
        if ($this->debug)
        {
            $this->addLog(json_encode($res));
        }
        return $res;
    }

    // Metodo da ereditare per le transazioni o riscrivere loop
    protected function read(\Iterator $reader)
    {
        $this->readIdx = -1;
        $mapping = ArrayHelper::getArray($this->params, 'mapping');
        $this->exceptionList = [];
        foreach ($reader as $row)
        {
            $this->readIdx++;
            try
            {
                if ($this->debug)
                {
                    $this->addLog(json_encode($row));
                }
                $this->readRow($this->mapRow($row, $mapping));
            } catch (\Exception $e)
            {
                if ($this->captureException)
                {
                    $error = 'Row: ' . ($this->readIdx + 1) . ' - Exception: ' . $e->getMessage();
                    if ($this->captureExceptionToLog)
                    {
                        $this->addLog($error);
                    }
                    $this->exceptionList[] = $error;
                } else
                {
                    throw $e;
                }
            }
        }
    }

    // Metodo da ereditare per le righe
    protected function readRow(array $row)
    {
        
    }

    public function run()
    {
        $name = ArrayHelper::getStr($this->params, 'name');
        $this->addLog('Start task: ' . $name);
        $profiler = new \EOS\System\Util\Profiler();
        $profiler->start();
        $format = ArrayHelper::getStr($this->params, 'format');
        $settings = ArrayHelper::getArray($this->params, 'settings');
        $reader = null;
        $filename = $this->replacePathTag(ArrayHelper::getStr($this->params, 'filename'));
        if (file_exists($filename))
        {
            switch ($format)
            {
                case 'delimited':
                    $reader = new \EOS\System\IO\DelimitedTextReader(
                        $filename, ArrayHelper::getBool($settings, 'forceautodetectlineendings', false));
                    $reader->csv = ArrayHelper::getBool($settings, 'csv', true);
                    $reader->utf8 = ArrayHelper::getBool($settings, 'utf8', true);
                    $reader->delimiter = ArrayHelper::getStr($settings, 'delimiter', ',');
                    $reader->escape = ArrayHelper::getStr($settings, 'escape', '\\');
                    $reader->enclosure = ArrayHelper::getStr($settings, 'enclosure', '"');
                    break;

                default:
                    throw new \Exception('Invalid format!');
                    break;
            }
            $this->read($reader);
        } else
        {
            $this->addLog('File not found: ' . $filename);
        }
        $profiler->stop();
        $this->addLog('End task: ' . $name . ' | Rows: ' . ($this->readIdx + 1) . ' | ' . $profiler->toString());
    }

    public function getExceptionList(): array
    {
        return $this->exceptionList;
    }

}
