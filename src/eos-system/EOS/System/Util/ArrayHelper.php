<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class ArrayHelper
{

    public static function getValue(array $array, $key, $default = null)
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }

    public static function getStr(array $array, $key, string $default = ''): string
    {
        return (string) static::getValue($array, $key, $default);
    }

    public static function getInt(array $array, $key, int $default = 0): int
    {
        return (int) static::getValue($array, $key, $default);
    }

    public static function getFloat(array $array, $key, float $default = 0): float
    {
        return (float) static::getValue($array, $key, $default);
    }

    public static function getBool(array $array, $key, bool $default = false): bool
    {
        return (bool) static::getValue($array, $key, $default);
    }

    public static function getArray(array $array, $key, array $default = []): array
    {
        return (array) static::getValue($array, $key, $default);
    }

    public static function setValueSkipDef(array &$array, $key, $value, $skipDefValue = null)
    {
        if ($value !== $skipDefValue)
        {
            $array[$key] = $value;
        }
    }

    public static function setStrSkipDef(array &$array, $key, string $value, string $skipDefValue = '')
    {
        static::setValueSkipDef($array, $key, $value, $skipDefValue);
    }

    public static function setIntSkipDef(array &$array, $key, int $value, int $skipDefValue = 0)
    {
        static::setValueSkipDef($array, $key, $value, $skipDefValue);
    }

    public static function setBoolSkipDef(array &$array, $key, bool $value, bool $skipDefValue = false)
    {
        static::setValueSkipDef($array, $key, $value, $skipDefValue);
    }

    public static function fromJSON(?string $json): array
    {
        $res = empty($json) ? [] : json_decode($json, true);
        if (is_null($res))
        {
            throw new \RuntimeException(json_last_error_msg(), json_last_error());
        }
        return $res;
    }

    public static function toJSON(array $array): string
    {
        $res = json_encode($array);
        if ($res === false)
        {
            throw new \RuntimeException(json_last_error_msg(), json_last_error());
        }
        return $res;
    }

    // Da usare per array multidimensionali
    public static function getMdValue(array $array, array $mdKeys, $default = null)
    {
        $res = $default;
        if (!empty($array) && (!empty($mdKeys)))
        {
            $level = $array;
            for ($i = 0; $i < count($mdKeys); $i++)
            {
                $key = $mdKeys[$i];
                if (isset($level[$key]))
                {
                    $value = $level[$key];
                    if ($i == count($mdKeys) - 1)
                    {
                        $res = $value;
                    } else
                    {
                        if (is_array($value))
                        {
                            $level = $value;
                        } else
                        {
                            break;
                        }
                    }
                } else
                {
                    break;
                }
            }
        }
        return $res;
    }

    public static function getMdStr(array $array, array $mdKeys, string $default = ''): string
    {
        return (string) static::getMdValue($array, $mdKeys, $default);
    }

    public static function getMdInt(array $array, array $mdKeys, int $default = 0): int
    {
        return (int) static::getMdValue($array, $mdKeys, $default);
    }

    public static function getMdBool(array $array, array $mdKeys, bool $default = false): bool
    {
        return (boolean) static::getMdValue($array, $mdKeys, $default);
    }

    public static function getMdArray(array $array, array $mdKeys, array $default = [])
    {
        return (array) static::getMdValue($array, $mdKeys, $default);
    }

}
