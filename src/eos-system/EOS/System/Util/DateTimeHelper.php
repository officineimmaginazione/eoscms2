<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class DateTimeHelper
{

    const lessThanValue = -1;
    const equalsValue = 0;
    const greaterThanValue = 1;

    public static function incSecond(\DateTimeImmutable $value, $seconds)
    {
        return $value->modify((int) $seconds . ' second');
    }    
    
    public static function incMinute(\DateTimeImmutable $value, $minutes)
    {
        return $value->modify((int) $minutes . ' minute');
    }

    public static function incDay(\DateTimeImmutable $value, $days)
    {
        return $value->modify((int) $days . ' day');
    }

    public static function incMonth(\DateTimeImmutable $value, $months)
    {
        return $value->modify((int) $months . ' month');
    }

    public static function incYear(\DateTimeImmutable $value, $years)
    {
        return $value->modify((int) $years . ' year');
    }

    public static function now()
    {
        return new \DateTimeImmutable();
    }

    public static function date()
    {
        return static::dateOf(new \DateTimeImmutable());
    }

    public static function startOfAMonth(\DateTimeImmutable $value)
    {
        return $value->modify('first day of this month');
    }

    public static function endOfAMonth(\DateTimeImmutable $value)
    {
        return static::startOfAMonth($value)->modify('last day of this month');
    }

    public static function dateOf(\DateTimeImmutable $value)
    {
        return $value->setTime(0, 0, 0);
    }

    public static function compareDate(\DateTimeImmutable $aDate, \DateTimeImmutable $bDate)
    {
        $fADate = $aDate->format('Y-m-d');
        $fBDate = $bDate->format('Y-m-d');
        if ($fADate == $fBDate)
        {
            return static::equalsValue;
        } else
        {
            return ($fADate < $fBDate) ? static::lessThanValue : static::greaterThanValue;
        }
    }

    public static function formatISO(\DateTimeImmutable $value)
    {
        return $value->format(\DateTime::ISO8601);
    }

    public static function createFromISO($isoDate)
    {
        return \DateTimeImmutable::createFromFormat(\DateTime::ISO8601, $isoDate);
    }

    public static function formatATOM(\DateTimeImmutable $value)
    {
        return $value->format(\DateTime::ATOM);
    }

    public static function createFromATOM($atomDate)
    {
        return \DateTimeImmutable::createFromFormat(\DateTime::ATOM, $atomDate);
    }

}
