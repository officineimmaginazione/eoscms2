<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class DebugHelper
{
   
   public static function exportVar($variable)
   {
     return '<pre>'.print_r($variable, true).'</pre>';
   }
}