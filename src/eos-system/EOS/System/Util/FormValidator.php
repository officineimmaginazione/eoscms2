<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

use EOS\System\Util\StringHelper;
use EOS\System\Routing\PathHelper;

class FormValidator
{

    private $_controller;
    private $_request;
    private $_response;
    private $_args;
    private $_outputData = [];
    private $_inputData = [];
    public $sanitizeSkipFields = [];
    public $sanitizeMultilineFields = [];
    public $sanitizeTrimBE = true;
    public $sanitiveStripLow = true;
    public $tokenID = '';
    public $requestUrlField = 'request_url';
    public $responseXRobotsTag = 'noindex, nofollow';
    public $validateRequestUrlField = false;
    public $validateOriginHeader = false; // Non funziona su Firefox!!!
    public $validateRequestXhr = false;

    private function getDomainUrl()
    {
        return PathHelper::removeTrailingSlash($this->_controller->container->get('path')->getDomainUrl());
    }

    private function isValidOrigin()
    {
        $res = true;
        if ($this->validateRequestUrlField)
        {
            // Controllo il dominio di origine
            $urlRequest = PathHelper::removeTrailingSlash(ArrayHelper::getStr($this->_inputData, $this->requestUrlField));
            $res = StringHelper::startsWith($urlRequest, $this->getDomainUrl());
        }
        if (($res) && ($this->validateOriginHeader))
        {
            $res = $this->_request->hasHeader('Origin');
            if ($res)
            {
                $urlOrigin = PathHelper::removeTrailingSlash($this->_request->getHeader('Origin')[0]);
                $res = StringHelper::startsWith($urlOrigin, $this->getDomainUrl());
            }
        }
        if (($res) && ($this->validateRequestXhr))
        {
            $res = $this->_request->isXhr();
        }
        if (!$res)
        {
            $this->setMessage($this->_controller->lang->trans('system.common.invalid.origin'));
        }
        return $res;
    }

    public function __construct($controller, $request, $response, $args)
    {
        $this->_controller = $controller;
        $this->_request = $request;
        $this->_response = $response;
        $this->_args = $args;
        $this->setResult(false);
    }

    public function prepareInputDataFromJson()
    {
        $r = false;
        $this->_inputData = [];
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($this->_request))
        {
            $this->_inputData = StringHelper::sanitizeFieldArray($this->_request->getParsedBody(), $this->sanitizeMultilineFields, $this->sanitizeSkipFields);
            if ($this->isValidOrigin())
            {
                if ($this->_controller->session->isValidTokenArrayKey($this->_inputData, $this->tokenID))
                {
                    $r = true;
                } else
                {
                    $this->setMessage($this->_controller->lang->trans('system.common.invalid.token'));
                }
            }
        } else
        {
            $this->setMessage($this->_controller->lang->trans('system.common.invalid.json'));
        }
        return $r;
    }

    public function prepareInputDataFromMultiPart()
    {
        $r = false;
        $this->_inputData = [];
        if (\EOS\System\Util\MessageHelper::isContentType($this->_request, 'multipart/form-data'))
        {
            $this->_inputData = StringHelper::sanitizeFieldArray($this->_request->getParsedBody(), $this->sanitizeMultilineFields, $this->sanitizeSkipFields);
            if ($this->isValidOrigin())
            {
                if ($this->_controller->session->isValidTokenArrayKey($this->_inputData, $this->tokenID))
                {
                    $r = true;
                } else
                {
                    $this->setMessage($this->_controller->lang->trans('system.common.invalid.token'));
                }
            }
        } else
        {
            $this->setMessage($this->_controller->lang->trans('system.common.invalid.multipart'));
        }
        return $r;
    }

    public function toJsonResponse()
    {
        $resp = empty($this->responseXRobotsTag) ? $this->responseXRobotsTag : $this->_response->withHeader('X-Robots-Tag', $this->responseXRobotsTag);
        return $resp->withJson($this->_outputData);
    }

    public function getInputData()
    {
        return $this->_inputData;
    }

    public function getOutputData()
    {
        return $this->_outputData;
    }

    public function setOutputValue($key, $value)
    {
        $this->_outputData[$key] = $value;
    }

    public function setResult($value)
    {
        $this->setOutputValue('result', $value);
    }

    public function setMessage($value)
    {
        $this->setOutputValue('message', $value);
    }

    public function setRedirect($value)
    {
        $this->setOutputValue('redirect', $value);
    }

}
