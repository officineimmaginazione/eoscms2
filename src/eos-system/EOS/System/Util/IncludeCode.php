<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */
/*
  Devo usare un oggetto per fare l'include perché PHP 5.x non non fa il bind del this nei metodi statici.
  Da PHP 7 > funziona
 */

namespace EOS\System\Util;

class IncludeCode
{

    protected function executeClosure($callable, $thisObject)
    {
        $bound = \Closure::bind($callable, $thisObject);
        return $bound();
    }

    public function includeFile($filename, $thisObject)
    {
        $__filename = $filename;
        return $this->executeClosure(function () use ($__filename)
            {
                return include($__filename);
            }, $thisObject);
    }

    public function includeOnceFile($filename, $thisObject)
    {
        $__filename = $filename;
        return $this->executeClosure(function () use ($__filename)
            {
                return include_once($__filename);
            }, $thisObject);
    }

    public static function sandboxInclude($filename, $thisObject)
    {
        $o = new IncludeCode();
        return $o->includeFile($filename, $thisObject);
    }

    public static function sandboxIncludeOnce($filename, $thisObject)
    {
        $o = new IncludeCode();
        return $o->includeOnceFile($filename, $thisObject);
    }

}
