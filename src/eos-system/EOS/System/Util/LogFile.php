<?php

/*
  EcoSoft e Officine Immaginazione Copyright (c) 2018
  www.ecosoft.it - www.officineimmaginazione.com
 */

namespace EOS\System\Util;

class LogFile
{

    protected $filename;
    public $resetSize = 500000; // Azzero il file superato i 500KB (1000 * 500)
    public $captureExceptionErrorEvent = null; // Viene richiamato quando si solleva un errore dove collegare la classe di mail

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function add(string $message, bool $addDateTime = true)
    {
        if (file_exists($this->filename))
        {
            if (filesize($this->filename) > $this->resetSize)
            {
                file_put_contents($this->filename, '');
            }
        }
        if ($addDateTime)
        {
            $prefix = sprintf('[%s] ', (new \DateTimeImmutable('now'))->format('Y-m-d H:i:s'));
        } else
        {
            $prefix = '';
        }

        file_put_contents($this->filename, $prefix . $message . "\n", FILE_APPEND | LOCK_EX);
    }

    public function captureException(\Closure $method)
    {
        try
        {
            if (is_callable($method))
            {
                $method();
            }
        } catch (\Exception $e)
        {
            $error = sprintf("Exception: %s\n%s", $e->getMessage(), $e->getTraceAsString());
            $this->add($error, true);
            $capErrEvent = $this->captureExceptionErrorEvent;
            if (is_callable($capErrEvent))
            {
                $capErrEvent($error);
            }
            throw $e;
        }
    }

}
