<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class MessageHelper
{

    public static function getContentType(\Psr\Http\Message\MessageInterface $message): string
    {
        $result = $message->getHeader('Content-Type');
        return empty($result) ? '' : $result[0];
    }

    public static function isContentType(\Psr\Http\Message\MessageInterface $message, string $type): bool
    {
        return StringHelper::startsWith(static::getContentType($message), $type);
    }

    public static function isContentTypeJson(\Psr\Http\Message\MessageInterface $message): bool
    {
        return static::isContentType($message, 'application/json');
    }

    public static function isContentTypeForm(\Psr\Http\Message\MessageInterface $message): bool
    {
        return static::isContentType($message, 'application/x-www-form-urlencoded');
    }

    public static function isContentTypeTextHtml(\Psr\Http\Message\MessageInterface $message): bool
    {
        return static::isContentType($message, 'text/html');
    }

    public static function setBodyJson(\Psr\Http\Message\MessageInterface $message, $data, $options = 0)
    {
        $res = static::setContentTypeJson($message);
        $json = json_encode($data, $options);
        if ($json === false)
        {
            throw new \RuntimeException(json_last_error_msg(), json_last_error());
        }
        $res->getBody()->write($json);
        return $res;
    }

    public static function setContentType(\Psr\Http\Message\MessageInterface $message, $type)
    {
        return $message->withHeader('Content-type', $type);
    }

    public static function setContentTypeJson(\Psr\Http\Message\MessageInterface $message)
    {
        return static::setContentType($message, 'application/json');
    }

}
