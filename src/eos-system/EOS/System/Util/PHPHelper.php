<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class PHPHelper
{

    public static function directiveValueToBytes(string $directive): int
    {
        $valueDir = ini_get($directive);

        $matches = [];
        preg_match('/^(?<value>\d+)(?<option>[K|M|G]*)$/i', $valueDir, $matches);

        $value = (int) $matches['value'];
        $option = strtoupper($matches['option']);

        if ($option)
        {
            if ($option === 'K')
            {
                $value *= 1024;
            } elseif ($option === 'M')
            {
                $value *= 1024 * 1024;
            } elseif ($option === 'G')
            {
                $value *= 1024 * 1024 * 1024;
            }
        }

        return $value;
    }

    public static function getPostMaxSize(): int
    {
        return static::directiveValueToBytes('post_max_size');
    }

    public static function getUploadMaxSize(): int
    {
        return static::directiveValueToBytes('upload_max_filesize');
    }

}
