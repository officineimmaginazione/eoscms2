<?php

/* +
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
  + */

namespace EOS\System\Util;

class Profiler
{

    // public static $instance = NULL;
    private $_lastTime = 0;
    private $timeList = [];

    public function __construct()
    {
        
    }

//  public static function getInstance()
//  {
//
//    if (!isset(self::$instance))
//    {
//      self::$instance = new Profiler();
//    }
//    return self::$instance;
//  }

    private function getContentType()
    {
        $headers = headers_list();
        // get the content type header
        foreach ($headers as $header)
        {
            if (substr(strtolower($header), 0, 13) == "content-type:")
            {
                $v = explode(": ", $header);
                if (count($v) == 2)
                {
                    return strtolower($v[1]);
                }
            }
        }
        return '';
    }

    public function start()
    {
        $this->_lastTime = microtime(true);
    }

    public function stop($str = '')
    {
        $duration = microtime(true) - $this->_lastTime;
        $this->timeList[] = sprintf('[%.3f s | %.0f KB] %s', $duration, memory_get_peak_usage(true) / 1000, $str);
    }

    public function render($endLine = "<br>\n")
    {
        switch ($this->getContentType())
        {
            case 'text/html; charset=utf-8':
                foreach ($this->timeList as $v)
                {
                    echo $v . $endLine;
                }
                break;
        }
    }

    public function toString()
    {
        return implode(' | ', $this->timeList);
    }

}
