<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */


namespace EOS\System\Util;

use Leafo\ScssPhp\Compiler;

class ScssHelper
{
    /**
     * Compiles all .scss files in a given folder into .css files in a given folder
     *
     * @param string $scss_folder source folder where you have your .scss files
     * @param string $css_folder destination folder where you want your .css files
     * @param string $format_style CSS output format, see http://leafo.net/scssphp/docs/#output_formatting for more.
     */
    static public function compile($scss_folder, $css_folder, $format_style = "Leafo\ScssPhp\Formatter\Compressed")
    {
        // scssc will be loaded automatically via Composer
        $scss_compiler =  new Compiler();
        // set the path where your _mixins are
        $scss_folder = \EOS\System\Routing\PathHelper::addDirSep($scss_folder);
        $css_folder = \EOS\System\Routing\PathHelper::addDirSep($css_folder);
        $scss_compiler->setImportPaths($scss_folder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $scss_compiler->setFormatter($format_style);
        // get all .scss files from scss folder
        $filelist = glob($scss_folder . "*.scss");
        // step through all .scss files in that folder
        foreach ($filelist as $file_path) {
            // get path elements from that file
            $file_path_elements = pathinfo($file_path);
            // get file's name without extension
            $file_name = $file_path_elements['filename'];
            // get .scss's content, put it into $string_sass
            $string_sass = file_get_contents($scss_folder . $file_name . ".scss");
            // compile this SASS code to CSS
            $string_css = $scss_compiler->compile($string_sass);
            // write CSS into file with the same filename, but .css extension
            file_put_contents($css_folder. $file_name . ".css", $string_css);
        }
    }
}