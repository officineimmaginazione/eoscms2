<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class StringHelper
{

    private static $_filterUrl = '$-_.+!*\'(),{}|\\^~[]`<>#%";/?:@&=. ';
    private static $_filterFileName = ['<', '>', ':', '"', '/', '\\', '|', '?', '*'];

    public static function fastHtml($body, $head = '')
    {
        return sprintf("<!DOCTYPE html>\n<html>\n<head><meta charset=\"UTF-8\">%s</head><body>%s</body>\n</html>", $head, $body);
    }

    public static function startsWith($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return !$length || substr($haystack, -$length) === $needle;
    }

    /* Converte in html tutti i caratteri anche '<' come '&lt 
     * e aggiungo di quotare gli apici doppi per evitare problemi con javascript e l'interfaccia di UI
     */

    public static function encodeHtml($text)
    {
        return htmlspecialchars($text, ENT_COMPAT | ENT_HTML5);
    }

    /*  Uso html_entity_decode e non htmlspecialchars_decode perché non converte queste stringhe: modalit&agrave; */

    public static function decodeHtml($text)
    {
        return html_entity_decode($text, ENT_QUOTES | ENT_HTML5);
    }

    public static function encodeHtmlArray($array, array $fieldsList = [])
    {
        $res = [];
        if (!empty($array)) {
            foreach ($array as $r) {
                if (empty($fieldsList)) {
                    foreach ($r as $f => $v) {
                        $r[$f] = static::encodeHtml($v);
                    }
                } else {
                    foreach ($fieldsList as $f) {
                        $r[$f] = static::encodeHtml($r[$f]);
                    }
                }
                $res[] = $r;
            }
        }
        return $res;
    }

    public static function printHtml($text)
    {
        echo static::encodeHtml($text);
    }

    public static function encodeUrl($path)
    {
        return urlencode($path);
    }

    public static function decodeUrl($path)
    {
        return urldecode($path);
    }

    public static function sanitizeUrl($path)
    {
        for ($i = 0; $i < strlen(static::$_filterUrl); $i++) {
            $arr[static::$_filterUrl[$i]] = '-';
        }
        $res = mb_strtolower(strtr($path, $arr));
        // Rimuovo - duplicati
        $res = preg_replace('/--+/', '-', $res);
        // Evito che il testo finisca per -
        return (substr($res, -1, 1) === '-') ? substr($res, 0, strlen($res) - 1) : $res;
    }

    public static function sanitizeUrlArray($list)
    {
        $res = [];
        // Così filtro $list = null
        if (!empty($list)) {
            foreach ($list as $v) {
                if (strlen($v) > 0) {
                    $res[] = static::sanitizeUrl($v);
                }
            }
        }
        return $res;
    }

    public static function sanitizeUrlSegment($path)
    {
        return implode('/', static::sanitizeUrlArray(explode('/', $path)));
    }

    // http://www.w3schools.com/php/filter_sanitize_string.asp
    public static function sanitizeField($value, $trimBE = true, $stripLow = true)
    {
        $filter = $stripLow ? (FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW) : FILTER_FLAG_NO_ENCODE_QUOTES;
        return filter_var(($trimBE) ? trim($value) : $value, FILTER_SANITIZE_STRING, $filter);
    }

    public static function sanitizeFieldMultiline($value, $stripLow = true)
    {
        return implode("\n", array_map(function ($v) use ($stripLow) {
            return static::sanitizeField($v, false, $stripLow);
        }, explode("\n", $value)));
    }

    // $multilinefields e $skipfields controlla l'inizio del valore del campo:
    // es: title-1 e title-2 mettendo title vengono esclusi
    // aiuta con i campi multilingua 
    public static function sanitizeFieldArray($data, $multilinefields = [], $skipfields = [], $trimBE = true, $stripLow = true)
    {
        if (!is_array($data) || !count($data)) {
            return array();
        }
        foreach ($data as $k => $v) {
            if (is_string($v)) {
                $f = array_filter($skipfields, function ($af) use ($k) {
                    return static::startsWith($k, $af);
                });

                if (count($f) == 0) {
                    $f = array_filter($multilinefields, function ($af) use ($k) {
                        return static::startsWith($k, $af);
                    });
                    if (count($f) == 0) {
                        $data[$k] = static::sanitizeField($v, $trimBE, $stripLow);
                    } else {
                        $data[$k] = static::sanitizeFieldMultiline($v, $stripLow);
                    }
                }
            } else if (is_array($v)) {
                $data[$k] = static::sanitizeFieldArray($v, $multilinefields, $skipfields, $trimBE, $stripLow);
            }
        }
        return $data;
    }

    public static function htmlPreview($str, $length, $suffix = '...')
    {
        $res = static::decodeHtml(static::sanitizeField($str));
        if (mb_strlen($str) > $length) {
            $space = mb_strpos($res, ' ', $length);
            if ($space !== false) {
                $res = mb_substr($res, 0, $space);
            }
            return static::encodeHtml($res . $suffix);
        }
        return static::encodeHtml($res);
    }

    // https://msdn.microsoft.com/en-us/library/aa365247(VS.85).aspx
    public static function sanitizeFileName($value, $unicode = true)
    {
        if (!$unicode) {
            // Converte èéà in eea
            $transliterator = \Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: Lower(); :: NFC;', \Transliterator::FORWARD);
            $value = $transliterator->transliterate($value);
            // rimuovo i caratteri sopra il 127
            $value = filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        // Dopo aver sanato la stringa faccio il trim hai lati per rimuovere i punti
        $value = trim(static::sanitizeField(str_replace(static::$_filterFileName, '', trim($value))), '.');
        // Esplodo il file e pulisco l'estensioni da eventuali doppi puntini, quindi non supporto la doppia estensione .php.jpg
        $part = pathinfo($value);
        $fname = str_replace('.', '', $part['filename']);
        $fext = isset($part['extension']) ? str_replace('.', '', $part['extension']) : '';
        if (!empty($fext)) {
            $fname .= '.' . $fext;
        }
        return $fname;
    }

    // Funzione che fa la conversione di campi arrivati da URL o POST che potrebbero contenere stringhe strane
    // Il cast (int) restituisce l'intero pulito anche se trova caratteri quindi non va sempre bene.
    // 1,2,ddd4/ o 1aaa restituisce 1, invece io vorrei che restituisse 0 o un valore di default
    public static function convertToInt($value, $default = 0)
    {
        if (is_int($value)) {
            $res = $value;
        } else {
            $res = $default;
            if (ctype_digit($value)) {
                $res = $value;
            }
        }
        return (int) $res;
    }
}
