<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Util;

class TagParser
{

    public $tagOpen = '{';
    public $tagClose = '}';
    public $callBack = null;
    public $value = '';
    public $result = '';
    public function parse()
    {
        $this->result = '';
        $offset = 0;
        $l = strlen($this->value);
        $lOpen = strlen($this->tagOpen);
        $lClose = strlen($this->tagClose);
        while ($offset < $l)
        {
            $pOpen = strpos($this->value, $this->tagOpen, $offset);
            if ($pOpen !== false)
            {
                $pClose = strpos($this->value, $this->tagClose, $offset + $lOpen);
                if ($pClose !== false)
                {
                    $this->result .= substr($this->value, $offset, $pOpen - $offset);
                    $offset = $pClose + $lClose;
                    $result = substr($this->value, $pOpen, $pClose - $pOpen + $lClose);
                    $command = substr($this->value, $pOpen + $lOpen, $pClose - $pOpen - 1);
                    $method = $this->callBack;
                    if (($method instanceof \Closure))
                    {
                        $method($command, $result);
                    }
                    $this->result .= $result;
                } else
                {
                    $this->result .= substr($this->value, $offset);
                    $offset = strlen($this->value);
                }
            } else
            {
                $this->result .= substr($this->value, $offset);
                $offset = strlen($this->value);
            }
        }
    }

}
