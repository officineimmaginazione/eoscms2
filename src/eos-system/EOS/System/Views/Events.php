<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Views;

final class Events
{
    // SystemEvent
    const HTML = 'system.view.html';
    const RAW = 'system.view.raw';
    // RenderEvent  
    const HTML_BEFORE_RENDER = 'system.view.html.before.render';
    const HTML_AFTER_RENDER = 'system.view.html.after.render';
    // EchoEvent
    const HTML_BEFORE_CONTENT = 'system.view.html.before.content';
    const HTML_AFTER_CONTENT = 'system.view.html.after.content';
    // RenderEvent
    const RAW_BEFORE_RENDER = 'system.view.raw.before.render';
    const RAW_AFTER_RENDER = 'system.view.raw.after.render';

}
