<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Views;

use EOS\System\Routing\PathHelper;
use EOS\System\Security\CryptHelper;

class Html
{

    private $_internalIncludeLevel = [];
    private $_inRendering = false;
    private $_counterID = 0;
    private $_lastID = '';
    protected $_headList;
    protected $_linkList;
    protected $_linkSheeStyleList;
    protected $_linkSheeStyleListJS;
    protected $_scriptLinkList;
    protected $_scriptList;
    protected $_scriptListReady;
    protected $_meta;
    protected $componentTemplate = '';
    protected $eventDispacher;
    public $request;
    public $response; // Pubblico per i custom form
    public $session;
    public $title = '';
    public $description = '';
    public $keywords = '';
    public $path;
    public $controller;
    public $lang;
    public $generator = '';
	public $settings = [];

    private function obStart()
    {
        ob_start();
    }

    private function obEnd()
    {
        $res = ob_get_contents();
        ob_end_clean();
        return $res;
    }

    private function internalIncludeFile($filename)
    {
        \EOS\System\Util\IncludeCode::sandboxInclude($filename, $this);
    }

    private function internalIncludeTemplateFile($path, $pathOverrideArray, $templateName, $exceptionIfNotExists)
    {
        $fileTpl = str_replace('/', DIRECTORY_SEPARATOR, $templateName) . '.php';
        $filename = PathHelper::resolveFileNameArray($path, $pathOverrideArray, $fileTpl);
        if (file_exists($filename))
        {
            if (in_array($filename, $this->_internalIncludeLevel))
            {
                $errpath = $templateName;
                if (_EOS_DEBUG_)
                {
                    $errpath = $filename;
                }
                throw new \Exception('Cannot include self or parent template "' . $errpath . '"');
            }
            array_push($this->_internalIncludeLevel, $filename);
            $this->internalIncludeFile($filename);
            array_pop($this->_internalIncludeLevel);
        } else
        {
            if ($exceptionIfNotExists)
            {
                $errpath = $templateName;
                if (_EOS_DEBUG_)
                {
                    $errpath = $filename;
                }
                throw new \Exception('Cannot load template "' . $errpath . '"');
            }
        }
    }

    public function __construct($controller, $request, $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->controller = $controller;
        $this->session = $controller->container->get('session');
        $this->path = $controller->path;
        $this->lang = $controller->container->get('language');

        $this->_headList = [];
        $this->_linkList = [];
        $this->_linkSheeStyleList = [];
        $this->_linkSheeStyleListJS = [];
        $this->_scriptLinkList = [];
        $this->_scriptList = [];
        $this->_scriptListReady = [];
        $this->_meta = [];
        // Evento in creazione della view 
        $this->eventDispacher = $this->controller->getEventDispatcher();
        $this->eventDispacher->dispatch(Events::HTML, new \EOS\System\Event\SystemEvent($this));
        $this->util = new HtmlUtil($this);
    }

    public function checkRendering()
    {
        if (!$this->_inRendering)
        {
            // throw new \Exception('Method not allowed out render!');
        }
    }

    public function render($componentTemplate, $withTemplate = true)
    {
        $this->componentTemplate = $componentTemplate;
        $renderBeforeEvent = new \EOS\System\Event\RenderEvent($this);
        $this->eventDispacher->dispatch(Events::HTML_BEFORE_RENDER, $renderBeforeEvent);
        $this->_inRendering = true;
        if ($withTemplate === true)
        {
            $tf = $this->path->getThemeTemplateFile();
            if (file_exists($tf))
            {
                $this->obStart();
                try
                {
                    $this->internalIncludeFile($tf);
                } finally
                {
                    $renderBeforeEvent->appendOutput($this->obEnd());
                }
            } else
            {
                throw new \Exception('Error: Could not load template ' . $tf . '!');
            }
        } else
        {
            $this->obStart();
            try
            {
                $this->writeContent();
            } finally
            {
                $renderBeforeEvent->appendOutput($this->obEnd());
            }
        }
        $this->_inRendering = false;
        $renderAfterEvent = new \EOS\System\Event\RenderEvent($this, $renderBeforeEvent->getOutput());
        unset($renderBeforeEvent);
        $this->eventDispacher->dispatch(Events::HTML_AFTER_RENDER, $renderAfterEvent);
        $this->response->getBody()->write($renderAfterEvent->getOutput());
        return $this->response;
    }

    public function addHead($text)
    {
        $this->_headList[] = $text;
    }

    public function addScriptLink($src, $async = false)
    {
        if (!isset($this->_scriptLinkList[$src]))
        {
            $this->_scriptLinkList[$src] = $async;
        }
    }

    public function removeScriptLink($src)
    {
        if (isset($this->_scriptLinkList[$src]))
        {
            unset($this->_scriptLinkList[$src]);
        }
    }

    public function addStyleSheetLink($href, $useJS = false)
    {
        if ($useJS)
        {
            if (!isset($this->_linkSheeStyleListJS[$href]))
            {
                $this->_linkSheeStyleListJS[$href] = '';
            }
        } else
        {
            if (!isset($this->_linkSheeStyleList[$href]))
            {
                $this->_linkSheeStyleList[$href] = '';
            }
        }
    }

    public function removeStyleSheetLink($href)
    {
        if (isset($this->_linkSheeStyleListJS[$href]))
        {
            unset($this->_linkSheeStyleListJS[$href]);
        }
        if (isset($this->_linkSheeStyleList[$href]))
        {
            unset($this->_linkSheeStyleList[$href]);
        }
    }

    public function addHeadLink($attributes)
    {
        $l = '<link ';
        foreach ($attributes as $key => $value)
        {
            $l .= $key . '="' . $value . '" ';
        }
        $l .= '>';
        $this->_linkList[] = $l;
    }

    public function addHeadLinkRaw($rawvalue)
    {
        $this->_linkList[] = $rawvalue;
    }

    public function addMetaRaw($keyname, $values)
    {
        $this->_meta[$keyname] = $values;
    }

    public function addMeta($name, $content, $isproperty = false)
    {
        if ($isproperty)
        {
            $this->addMetaRaw($name, ['property' => $name, 'content' => $content]);
        } else
        {
            $this->addMetaRaw($name, ['name' => $name, 'content' => $content]);
        }
    }

    public function writeTokenHtml($tokenID = '')
    {
        $this->util->writeTokenHtml($tokenID);
    }

    public function writeContent()
    {
        $curr = $this->controller->currentParams;
        $this->eventDispacher->dispatch(Events::HTML_BEFORE_CONTENT, new \EOS\System\Event\EchoEvent($this));
        $ovPath = [$this->path->getThemeOverrideComponentTemplatesPath(), $curr['componentOverrideTemplatesPath']];
        $this->internalIncludeTemplateFile($curr['componentTemplatesPath'], $ovPath, $this->componentTemplate, true);
        $this->eventDispacher->dispatch(Events::HTML_AFTER_CONTENT, new \EOS\System\Event\EchoEvent($this));
    }

    public function includeParent($useOverride = true)
    {
        $curr = $this->controller->currentParams;
        $ovPath = $useOverride ? [$curr['componentOverrideTemplatesPath']] : [];
        $this->internalIncludeTemplateFile($curr['componentTemplatesPath'], $ovPath, $this->componentTemplate, true);
    }

    public function includePartial($partialName, $optional = true)
    {
        $curr = $this->controller->currentParams;
        $ovPath = [$this->path->getThemeOverrideComponentTemplatesPath(), $curr['componentOverrideTemplatesPath']];
        $this->internalIncludeTemplateFile($curr['componentTemplatesPath'], $ovPath, $this->componentTemplate . '_' . $partialName, !$optional);
    }

    public function includePartialParent($partialName, $optional = true, $useOverride = true)
    {
        $curr = $this->controller->currentParams;
        $ovPath = $useOverride ? [$curr['componentOverrideTemplatesPath']] : [];
        $this->internalIncludeTemplateFile($curr['componentTemplatesPath'], $ovPath, $this->componentTemplate . '_' . $partialName, !$optional);
    }

    public function writeHead($default = true)
    {
        $this->checkRendering();
        $extraMeta = [];
        if ($default)
        {
            echo '<base href="' . $this->path->getBaseUrl() . '">' . "\n";
            if (!empty($this->generator))
            {
                $extraMeta['generator'] = ['name' => 'generator', 'content' => $this->generator];
            }
        }
        if (!empty($this->title))
        {
            echo "<title>" . $this->encodeHtml($this->title) . "</title>" . "\n";
        }

        if (!empty($this->description))
        {
            $extraMeta['description'] = ['name' => 'description', 'content' => $this->description];
        }
        if (!empty($this->keywords))
        {
            $extraMeta['keywords'] = ['name' => 'keywords', 'content' => $this->keywords];
        }

        if (!empty($extraMeta))
        {
            $this->_meta = array_merge($extraMeta, $this->_meta);
        }

        foreach ($this->_meta as $meta => $value)
        {
            $r = '<meta';
            foreach ($value as $k => $v)
            {
                $r .= sprintf(' %s="%s"', $this->encodeHtml($k), $this->encodeHtml($v));
            }
            $r .= '>';
            echo $r . "\n";
        }

        foreach ($this->_headList as $t)
            echo $t . "\n";
    }

    public function writeScript()
    {
        $this->checkRendering();
        if (!empty($this->_linkSheeStyleListJS))
        {
            echo '<script>' . "\n";
            $x = rand(1, 1000);
            echo 'function loadLinkSheetStyle' . $x . '(link) {';
            echo 'var stylesheet = document.createElement("link");';
            echo 'stylesheet.href = link;';
            echo 'stylesheet.rel = "stylesheet";';
            echo'stylesheet.type = "text/css";';
            echo 'document.getElementsByTagName("head")[0].appendChild(stylesheet);}' . "\n";
            foreach ($this->_linkSheeStyleListJS as $href => $v)
            {
                echo 'loadLinkSheetStyle' . $x . '("' . $href . '");' . "\n";
            }
            echo '</script>' . "\n";
            echo '<noscript>' . "\n";
            foreach ($this->_linkSheeStyleListJS as $href => $v)
            {
                echo '<link rel="stylesheet" type="text/css" href="' . $href . '">' . "\n";
            }
            echo'</noscript>' . "\n";
        }

        foreach ($this->_scriptLinkList as $src => $async)
        {
            echo '<script ' . ($async == true ? 'async ' : '') . 'src="' . $src . '"></script>' . "\n";
        }
        foreach ($this->_scriptList as $t)
        {
            echo $t . "\n";
        }

        if (count($this->_scriptListReady) > 0)
        {
            echo '<script>' . "\n";
            echo '$(function() {' . "\n";
            foreach ($this->_scriptListReady as $t)
            {
                echo $t;
                if (substr($t, strlen($t) - 1) != "\n")
                {
                    echo "\n";
                }
            }
            echo '});' . "\n";
            echo '</script>' . "\n";
        }
    }

    public function writeLink($forceUseJS = false)
    {
        $this->checkRendering();
        if ($forceUseJS)
        {
            foreach ($this->_linkSheeStyleList as $href => $v)
            {
                $this->addStyleSheetLink($href, true);
            }
        } else
        {
            foreach ($this->_linkSheeStyleList as $href => $v)
            {
                echo '<link rel="stylesheet" type="text/css" href="' . $href . '">' . "\n";
            }
        }
        foreach ($this->_linkList as $t)
        {
            echo $t . "\n";
        }
    }

    public function startCaptureScript()
    {
        $this->obStart();
    }

    public function endCaptureScript()
    {
        $this->_scriptList[] = $this->obEnd();
    }

    public function startCaptureScriptReady()
    {
        $this->obStart();
    }

    public function endCaptureScriptReady()
    {
        $this->_scriptListReady[] = $this->obEnd();
    }

    public function createUrl($url, $decode = true)
    {
        if ($decode)
        {
            return htmlentities($this->getCurrentUrl() . $url);
        } else
            return $this->getCurrentUrl() . $url;
    }

    public function responseError($error, $code = 500)
    {
        $this->controller->responseError($error, $code);
    }

    public function encodeHtml($text)
    {
        return \EOS\System\Util\StringHelper::encodeHtml($text);
    }

    public function printHtml($text)
    {
        $this->checkRendering();
        echo \EOS\System\Util\StringHelper::encodeHtml($text);
    }

    public function trans($key, $lang = '')
    {
        return $this->lang->trans($key, $lang);
    }

    public function transE($key, $lang = '')
    {
        return $this->lang->transEncode($key, $lang);
    }

    public function transP($key, $lang = '')
    {
        $this->checkRendering();
        echo $this->lang->trans($key, $lang);
    }

    public function transPE($key, $lang = '')
    {
        $this->checkRendering();
        echo $this->lang->transEncode($key, $lang);
    }

    public function getComponentName()
    {
        return $this->controller->currentParams['componentName'];
    }

    public function getComponentTemplate()
    {
        return $this->componentTemplate;
    }

    public function getThemeTemplateName()
    {
        return $this->controller->currentParams['themeTemplate'];
    }

    public function inRendering()
    {
        return $this->_inRendering;
    }

    public function createID($prefix = '', $suffix = '')
    {
        $this->_counterID++;
        $this->_lastID = (empty($prefix) ? 'id_' : $prefix) . $this->_counterID . '_' . md5(uniqid(CryptHelper::randomString(16), true)) . $suffix;
        return $this->_lastID;
    }

    public function getLastID()
    {
        if (empty($this->_lastID))
        {
            $this->createID();
        }
        return $this->_lastID;
    }

    public function getEventDispatcher($name = null)
    {
        return $this->controller->getEventDispatcher($name);
    }

}
