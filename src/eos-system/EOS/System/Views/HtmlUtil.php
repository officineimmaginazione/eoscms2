<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Views;

class HtmlUtil
{

    protected $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function addScriptBlock($js)
    {
        $this->view->startCaptureScript();
        echo '<script>' . $js . '</script>';
        $this->view->endCaptureScript();
    }

    public function addScriptBlockReady($js)
    {
        $this->addScriptBlock('$(function(){' . $js . '});');
    }

    public function writeTokenHtml($tokenID = '')
    {
        $this->view->checkRendering();
        echo '<input type="hidden" name="' . $this->view->session->getTokenName($tokenID) . '" value="' . $this->view->session->getTokenValue($tokenID) . '">' . "\n";
    }

    public function writeAsyncTokenHtml($tokenID = '', $jsTimeout = 200)
    {
        $this->view->checkRendering();
        $name = $this->view->session->getTokenName($tokenID);
        $id = $this->view->createID();
        echo '<input  id="' . $id . '" type="hidden" name="' . $name . '" value="">' . "\n";
        $js = '$(\'#' . $id . '\').val(\'' . $this->view->session->getTokenValue($tokenID) . '\');';
        if ($jsTimeout > 0)
        {
            $js = sprintf('setTimeout(function(){%s}, %d);', $js, $jsTimeout);
        }
        $this->addScriptBlockReady($js);
    }

    public function writeAsyncRequestUrl($name = 'request_url', $jsTimeout = 200, $topLocation = false)
    {
        $this->view->checkRendering();
        $id = $this->view->createID();
        echo '<input id="' . $id . '" type="hidden" name="' . $this->view->encodeHtml($name) . '" value="">' . "\n";
        // topLocation = true serve a bloccare negli iFrame le richieste
        $location = $topLocation ? 'window.top.location' : 'window.location';
        $js = '$(\'#' . $id . '\').val(' . $location . '.href.match(/^[^\#\?]+/)[0]);';
        if ($jsTimeout > 0)
        {
            $js = sprintf('setTimeout(function(){%s}, %d);', $js, $jsTimeout);
        }
        $this->addScriptBlockReady($js);
    }

}
