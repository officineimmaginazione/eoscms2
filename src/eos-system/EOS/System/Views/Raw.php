<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Views;

use EOS\System\Routing\PathHelper;

class Raw
{

    protected $eventDispacher;

    private function internalIncludeFile($fileName)
    {
        \EOS\System\Util\IncludeCode::sandboxInclude($fileName, $this);
    }

    public function __construct($controller, $request, $response, $cacheMaxAge)
    {
        $this->request = $request;
        $this->response = $response->withoutHeader('Expires')->withoutHeader('Pragma')->withoutHeader('Cache-Control');
        if ($cacheMaxAge > 0)
        {
            $this->response = $this->$this->response->withHeader('Cache-Control', 'max-age=' . $cacheMaxAge);
        }
        $this->controller = $controller;
        $this->session = $controller->container->get('session');
        $this->path = $controller->path;
        $this->lang = $controller->container->get('language');

        // Evento in creazione della view 
        $this->eventDispacher = $this->controller->getEventDispatcher();
        $this->eventDispacher->dispatch(Events::RAW, new \EOS\System\Event\SystemEvent($this));
    }

    public function render($componentTemplate, $extension = 'php')
    {
        $curr = $this->controller->currentParams;
        $fileTpl = str_replace('/', DIRECTORY_SEPARATOR, $componentTemplate) . '.' . $extension;
        $tmplFilename = PathHelper::resolveFileNameArray($curr['componentTemplatesPath'], [$this->path->getThemeOverrideComponentTemplatesPath(), $curr['componentOverrideTemplatesPath']], $fileTpl);
        $renderBeforeEvent = new \EOS\System\Event\RenderEvent($this);
        $this->eventDispacher->dispatch(Events::RAW_BEFORE_RENDER, $renderBeforeEvent);
        if (file_exists($tmplFilename))
        {
            ob_start();
            $this->internalIncludeFile($tmplFilename);
            $renderBeforeEvent->appendOutput(ob_get_contents());
            ob_end_clean();
        } else
        {
            $errpath = $componentTemplate;
            if (_EOS_DEBUG_ == true)
            {
                $errpath = $tmplFilename;
            }

            throw new \Exception('Cannot load view template "' . $errpath . '"');
        }
        $renderAfterEvent = new \EOS\System\Event\RenderEvent($this, $renderBeforeEvent->getOutput());
        unset($renderBeforeEvent);
        $this->eventDispacher->dispatch(Events::RAW_AFTER_RENDER, $renderAfterEvent);
        $this->response->getBody()->write($renderAfterEvent->getOutput());
        return $this->response;
    }

}
