<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\System\Views;

use EOS\System\Routing\PathHelper;

class Template
{

    private $_inRendering;
    protected $container;
    public $lang;
    public $path;

    protected function checkRendering()
    {
        if (!$this->_inRendering)
        {
            throw new \Exception('Method not allowed out render!');
        }
    }

    public function __construct($container)
    {
        $this->container = $container;
        $this->path = $container['path'];
        $this->lang = $container['language'];
    }

    public function render($component, $template)
    {
        $res = '';
        $compName = $this->container['componentManager']->getComponent($component)->name;
        $fileTpl = 'templates' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $template) . '.php';
        $curr = $this->container['currentParams'];
        $compPath = $curr['componentsPath'] . $compName . DIRECTORY_SEPARATOR;
        $compOver = empty($curr['componentsOverridePath']) ? '' : $curr['componentsOverridePath'] . $compName . DIRECTORY_SEPARATOR;
        $compThemeOver = $this->path->getThemeOverridePath() . $compName . DIRECTORY_SEPARATOR;
        $tmplFilename = PathHelper::resolveFileNameArray($compPath, [$compOver, $compThemeOver], $fileTpl);
        if (file_exists($tmplFilename))
        {
            $this->_inRendering = true;
            ob_start();
            \EOS\System\Util\IncludeCode::sandboxInclude($tmplFilename, $this);
            $res = ob_get_contents();
            ob_end_clean();
            $this->_inRendering = false;
        } else
        {
            $errpath = $compName . '/' . $template;
            if (_EOS_DEBUG_ == true)
            {
                $errpath = $tmplFilename;
            }

            throw new \Exception('Cannot load template "' . $errpath . '"');
        }
        return $res;
    }

    public function encodeHtml($text)
    {
        return \EOS\System\Util\StringHelper::encodeHtml($text);
    }

    public function printHtml($text)
    {
        $this->checkRendering();
        echo \EOS\System\Util\StringHelper::encodeHtml($text);
    }

    public function trans($key, $lang = '')
    {
        return $this->lang->trans($key, $lang);
    }

    public function transE($key, $lang = '')
    {
        return $this->lang->transEncode($key, $lang);
    }

    public function transP($key, $lang = '')
    {
        $this->checkRendering();
        echo $this->lang->trans($key, $lang);
    }

    public function transPE($key, $lang = '')
    {
        $this->checkRendering();
        echo $this->lang->transEncode($key, $lang);
    }

}
