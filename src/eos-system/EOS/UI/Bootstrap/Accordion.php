<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Accordion extends \EOS\UI\Html\Div
{

    private $_header = '';
	private $_id = '';

    public function __construct($id = '')
    {
        parent::__construct();
        $this->id($id);
		$this->_id = $id;
        $this->attributes['class'] = 'card';
    }

    public function title($title)
    { 
		$this->_header = '<div class="card-header" id="heading' . $this->_id . '">';
        $this->_header .= '<h5 class="mb-0">';
		$this->_header .= '<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse' . $this->_id . '" aria-expanded="false" aria-controls="collapse' . $this->_id . '">';
        $this->_header .= \EOS\System\Util\StringHelper::encodeHtml($title);
		$this->_header .= '</button>' . "\n";
        $this->_header .= '</h5>' . "\n";
		$this->_header .= '</div>';
        return $this;
    }

    public function render()
    {
        $h = '';
        if (strlen($this->_header) > 0)
        {
            $h = $this->_header;
        }
        $bodyH = '<div id="collapse' . $this->_id . '" class="collapse" aria-labelledby="heading' . $this->_id . '" data-parent="#accordion"><div class="card-body">' . "\n";
        $bodyF = '</div></div>' . "\n";
        $this->contentValue = $h . $bodyH . $this->contentValue . $bodyF;
        return parent::render();
    }

    public function startHeader()
    {
        ob_start();
        return $this;
    }

    public function endHeader()
    {
        $this->_header .= ob_get_contents();
        ob_end_clean();
        return $this;
    }

    public function addHeader($callable, $view)
    {
        if ($callable instanceof \Closure)
        {
            $this->startHeader();
            $bound = \Closure::bind($callable, $view);
            $bound();
            $this->endHeader();
        }
        return $this;
    }

}
