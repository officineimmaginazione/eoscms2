<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Box extends \EOS\UI\Html\Div
{

    private $_header = '';
    private $_footer = '';

    public function __construct($id = '')
    {
        parent::__construct();
        $this->id($id);
        $this->attributes['class'] = 'box';
    }

    public function title($title)
    {
        $this->_header = '<h3 class="box-title">';
        $this->_header .= \EOS\System\Util\StringHelper::encodeHtml($title);
        $this->_header .= '</h3>' . "\n";
        return $this;
    }

    public function render()
    {
        $h = '';
        if (strlen($this->_header) > 0)
        {
            $h = '<div class="box-header with-border">' . "\n";
            $h .= $this->_header;
            $h .= '</div>' . "\n";
        }
        $f = '';
        if (strlen($this->_footer) > 0)
        {
            $f = '<div class="box-footer">';
            $f .= $this->_footer;
            $f .= '</div>' . "\n";
        }
        $bodyH = '<div class="box-body">' . "\n";
        $bodyF = '</div>' . "\n";
        $this->contentValue = $h . $bodyH . $this->contentValue . $bodyF . $f;
        return parent::render();
    }

    public function startHeader()
    {
        ob_start();
        return $this;
    }

    public function endHeader()
    {
        $this->_header .= ob_get_contents();
        ob_end_clean();
        return $this;
    }

    public function addHeader($callable, $view)
    {
        if ($callable instanceof \Closure)
        {
            $this->startHeader();
            $bound = \Closure::bind($callable, $view);
            $bound();
            $this->endHeader();
        }
        return $this;
    }

    public function startFooter()
    {
        ob_start();
        return $this;
    }

    public function endFooter()
    {
        $this->_footer .= ob_get_contents();
        ob_end_clean();
        return $this;
    }

    public function addFooter($callable, $view)
    {
        if ($callable instanceof \Closure)
        {
            $this->startFooter();
            $bound = \Closure::bind($callable, $view);
            $bound();
            $this->endFooter();
        }
        return $this;
    }

}
