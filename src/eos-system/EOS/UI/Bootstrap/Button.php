<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Button extends \EOS\UI\Html\Button
{

    public function __construct($id = '')
    {
        parent::__construct();
        $this->id($id);
        $this->attributes['class'] = 'btn';
    }

}
