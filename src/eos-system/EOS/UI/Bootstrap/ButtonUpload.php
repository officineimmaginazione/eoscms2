<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class ButtonUpload extends \EOS\UI\Bootstrap\Button
{

    protected $ajaxUrl = '';
    protected $tokenName = '';
    protected $tokenValue = '';
    protected $accept = '';
    protected $uploadMaxSize;
    protected $sendSuccess;

    public function __construct(string $id, string $ajaxUrl, string $tokenName, string $tokenValue)
    {
        parent::__construct($id);

        $this->ajaxUrl = $ajaxUrl;
        $this->tokenName = $tokenName;
        $this->tokenValue = $tokenValue;
        $this->uploadMaxSize = \EOS\System\Util\PHPHelper::getPostMaxSize();
        if ($this->uploadMaxSize > \EOS\System\Util\PHPHelper::getUploadMaxSize())
        {
            $this->uploadMaxSize = \EOS\System\Util\PHPHelper::getUploadMaxSize();
        }
    }

    public function uploadMaxSize(int $value): object
    {
        $this->uploadMaxSize = $value;
        return $this;
    }

    public function accept(string $value): object
    {

        $this->accept = $value;
        return $this;
    }

    public function onSendSuccess($eventJS): object
    {
        $this->sendSuccess = $eventJS;
        return $this;
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true): object
    {
        if (is_null($view))
        {
            throw new \Exception(get_class($this) . 'required View param in printRender!');
        }
        if ($this->ajaxUrl !== '')
        {
            $maxSizeError = $view->lang->transEncode('system.common.invalid.uploadsize');
            $addAccept = $this->accept !== '' ? 'inputHtml.accept = "' . $this->accept . '";' : '';
            $inputID = $this->attributes['id'] . '-input-'.$view->createID();

            $j = '$("#' . $this->attributes['id'] . '").on("click", function ()
                {
                   var showAlert = function (message)
                     {
                       if (typeof bootbox !== "undefined")
                       {
                           bootbox.alert(message)
                        }
                        else
                        {
                           alert(message);
                        }
                     };
                   var inputName = "'.$inputID.'";
                   var removeInputMethod = function ()
                   {
                      var remHtml = document.getElementById(inputName)
                      if (remHtml !== null)
                      {
                          document.body.removeChild(remHtml); 
                        }
                   }
                   removeInputMethod();
                   var inputHtml = document.createElement("input");
                   inputHtml.id = inputName;
                   inputHtml.style.display = "none";
                   inputHtml.type = "file";
                   ' . $addAccept . '
                   var uploadMaxSize = ' . $this->uploadMaxSize . ';
                   document.body.appendChild(inputHtml);    
                   var changeEvent = function ()
                     {
                         if (this.files === undefined)
                         {         
                            return false;
                         }
                         if ((this.files.length > 0) && (this.files[0].size > uploadMaxSize))
                         {
                            showAlert("' . addslashes($maxSizeError) . '");
                            return false;
                         }
                            
                        if (this.files.length > 0)
                        {
                            var fData = new FormData();
                            fData.append("' . $this->tokenName . '", "' . $this->tokenValue . '");
                            fData.append("command", "upload-file");
                            fData.append("id", "' . $this->attributes['id'] . '");
                            fData.append("request_url", window.location.href);
                            fData.append("data", this.files[0]);
                            this.value = ""; 
                             
                            $.ajax({
                                url : "' . addslashes($this->ajaxUrl) . '",
                                type : "POST",
                                data : fData,
                                processData: false,  
                                contentType: false,  
                                success : function(data) {
                                   if (data.result)
                                   {
                                     (' . $this->sendSuccess . ')(data);
                                   }
                                   else
                                   { 
                                      showAlert(data.message);
                                   }
                                   removeInputMethod();
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                                    showAlert(errorThrown);
                                    removeInputMethod();
                                }
                            });
                        }
                     };
                   inputHtml.addEventListener("change", changeEvent, false);
                   $(inputHtml).trigger("click");
                });';


            $this->jqueryReady[] = $j;
        }

        return parent::printRender($view, $tagscript, $tagready);
    }

}
