<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

namespace EOS\UI\Bootstrap;

class ButtonUploadServer
{

    protected $container;
    protected $mimeList = [];
    protected $extensionsList = [];
    protected $uploadMaxSize;

    protected function isValidUpload($newFile, &$error): bool
    {
        $res = true;
        if ($newFile->getError() !== UPLOAD_ERR_OK)
        {
            $error = $this->lang->transEncode('system.common.invalid.upload');
            $res = false;
        } else
        if ((!empty($this->mimeList)) && (!in_array($newFile->getClientMediaType(), $this->mimeList)))
        {
            $error = $this->lang->transEncode('system.common.invalid.mimetype');
            $res = false;
        } else
        if ((!empty($this->extensionsList)) && (!in_array(pathinfo($newFile->getClientFileName(), PATHINFO_EXTENSION), $this->extensionsList)))
        {
            $error = $this->lang->transEncode('system.common.invalid.extension');
            $res = false;
        } else
        if ($newFile->getSize() > $this->uploadMaxSize)
        {
            $error = $this->lang->transEncode('system.common.invalid.size');
            $res = false;
        }
        return $res;
    }

    public function __construct(object $container)
    {
        $this->container = $container;
        $this->lang = $this->container->get('language');
        $this->uploadMaxSize = \EOS\System\Util\PHPHelper::getPostMaxSize();
        if ($this->uploadMaxSize > \EOS\System\Util\PHPHelper::getUploadMaxSize())
        {
            $this->uploadMaxSize = \EOS\System\Util\PHPHelper::getUploadMaxSize();
        }
    }

    public function setMimeList(array $list)
    {
        $this->mimeList = $list;
    }

    public function setExtensionsList(array $list)
    {
        $this->extensionsList = $list;
    }

    public function setUploadMaxSize(int $size)
    {
        $this->uploadMaxSize = $size;
    }

    public function upload(\EOS\System\Controllers\Controller $controller, $request, $response, \Closure $method)
    {
        $fv = new \EOS\System\Util\FormValidator($controller, $request, $response, []);
        $fv->requestUrlField = true;
        $fv->validateRequestXhr = true; // Controllo che le chiamate siano di JQUERY o XMLHttpRequest
        if ($fv->prepareInputDataFromMultiPart())
        {
            $rqData = $fv->getInputData();
            switch (\EOS\System\Util\ArrayHelper::getStr($rqData, 'command'))
            {
                case 'upload-file' :
                    $files = $request->getUploadedFiles();
                    if (empty($files['data']))
                    {
                        $fv->setMessage($this->lang->transEncode('system.common.filenotfound'));
                    } else
                    {
                        $newFile = $files['data'];
                        $error = '';
                        $resFile = '';
                        if ($this->isValidUpload($newFile, $error))
                        {
                            $fv->setResult(true);
                            $res = $method($fv, $newFile, $error);
                            if ($res === false)
                            {
                                $fv->setResult(false);
                                $fv->setMessage($error);
                            }
                        } else
                        {
                            $fv->setMessage($error);
                        }
                    }
                    break;
                default :
                    $fv->setMessage($this->lang->transEncode('system.common.invalid.command'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

}
