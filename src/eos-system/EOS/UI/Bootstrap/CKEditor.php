<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class CKEditor extends \EOS\UI\Html\Textarea
{

    private static $_init = true;
    private $_compactMode = false;
    private $_baseUrl = '';
    private $_fileBrowserUrl = '';
    private $_language = '';
    private $_sourceMode = false;

    public function __construct($nameAndID)
    {
        parent::__construct();
        $this->name($nameAndID);
        $this->id($nameAndID);
    }

    public function sourceMode($value)
    {
        $this->_sourceMode = $value;
        return $this;
    }

    public function bindSourceMode($data, $field)
    {
        $this->sourceMode(isset($data[$field]) && ($data[$field] == 'source'));
        return $this;
    }

    public function render()
    {
        $config = [];
        if ($this->_compactMode)
        {
            $config[] = "
      toolbar: [['Maximize'],
      ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat', '-'],
      ['Link', 'Unlink'],
      ['Cut', 'Copy', 'Paste', 'PasteText'],
      ['Undo', 'Redo'], 
      ['ShowBlocks'], ['Source']], \n";
        } else
        {
            $config[] = "
      toolbar: [
      ['Maximize'],
      ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat', '-', 'Subscript', 'Superscript'],
      ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
      ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
      ['SpecialChar'],
      ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
      ['Source'],
      '/',
      ['Undo', 'Redo'], ['Format', 'Font', 'FontSize'],
      ['TextColor', 'BGColor'],
      ['Link', 'Unlink', 'Anchor'],
      ['Image', 'Table', 'HorizontalRule', '-', 'CreateDiv', 'ShowBlocks'],
      ]";
            $config[] = 'extraPlugins:"showblocks"'; // Modulo esterno che visualizza i div
        }
        if (strlen($this->_fileBrowserUrl) > 0)
        {
            $config[] = 'filebrowserBrowseUrl:"' . $this->_fileBrowserUrl . '"';
            ;
        }
        if (strlen($this->_language) > 0)
        {
            $config[] = 'language:"' . $this->_language . '"';
        }
        if (strlen($this->_baseUrl) > 0)
        {
            $config[] = ' baseHref:"' . $this->_baseUrl . '"';
            $config[] = ' baseUrl:"' . $this->_baseUrl . '"';
        }
        $config[] = 'startupMode:"' . ($this->_sourceMode ? 'source' : 'wysiwyg') . '"';
        $config[] = 'autoParagraph:false';
        $config[] = 'autoUpdateElement:true';
        $config[] = 'allowedContent:true'; // Obbligatorio altrimenti non trasforma le immagini
        //  $config[] = ' enterMode: CKEDITOR.ENTER_BR ';
        //  $config[] =  ' shiftEnterMode: CKEDITOR.ENTER_P';
        // http://docs.ckeditor.com/#!/guide/dev_howtos_output -> Cambia <br /> in <br>
        // http://docs.ckeditor.com/#!/guide/dev_output_format - Rimuove la riga vuota dopo i P
        $config[] = 'on: 
        {
          instanceReady: function( ev ) {
            ev.editor.dataProcessor.writer.selfClosingEnd = ">";
            this.dataProcessor.writer.setRules("p", { 
                indent: false,
                breakBeforeOpen: true,
                breakAfterOpen: false,
                breakBeforeClose: false,
                breakAfterClose: false
            });
            

            ev.editor.dataProcessor.htmlFilter.addRules(
            {
                elements:
                {
                 $: function (element) {
              
                   // Output dimensions of images as width and height
                    if (element.name == "img") {
                        var style = element.attributes.style;

                        if (style) {
                            // Get the width from the style.
                            var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
                                width = match && match[1];

                            // Get the height from the style.
                            match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                            var height = match && match[1];

                            if (width) {
                               element.attributes.style = element.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, "");
                               element.attributes.width = width;
                            }

                            if (height) {
                                element.attributes.style = element.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, "");
                               element.attributes.height = height;
                            }
                        }
                        
                    }

                    if (!element.attributes.style)
                        delete element.attributes.style;
                    return element;
                  }
                }
            });
          },
          mode: function (ev)
           {
             $("#' . $this->attributes['id'] . '-mode").val(this.mode);
            }
        }';
        if (static::$_init)
        {
            // Fix font awesome 
            $this->jqueryReady[] = 'CKEDITOR.dtd.$removeEmpty["i"] = false;';
            static::$_init = false;
        }
        $this->jqueryReady[] = 'CKEDITOR.replace("' . $this->attributes['id'] . '", {' . implode(",\n", $config) . '});';
        $res = parent::render();
        $res .= '<input type="hidden" name="' . $this->attributes['name'] . '-mode" id="' . $this->attributes['id'] . '-mode">' . "\n";
        return $res;
    }

    public function compactMode($value)
    {
        $this->_compactMode = $value;
        return $this;
    }

    public function baseUrl($value)
    {
        $this->_baseUrl = $value;
        return $this;
    }

    public function fileBrowserUrl($value)
    {
        $this->_fileBrowserUrl = $value;
        return $this;
    }

    public function language($value)
    {
        $this->_language = $value;
        return $this;
    }

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->content($array[$name]);
        }
        return $this;
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        if (!is_null($view))
        {
            $this->language($view->lang->getCurrent()->iso);
            $this->baseUrl($view->path->getBaseUrl());
        }
        return parent::printRender($view, $tagscript, $tagready);
    }

}
