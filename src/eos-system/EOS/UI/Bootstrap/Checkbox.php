<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Checkbox extends \EOS\UI\Html\Input
{

    public function __construct($nameAndID)
    {
        parent::__construct();
        $this->attributes['class'] = 'icheckbox_square';//'icheckbox_square-blue';
        $this->type('checkbox');
        $this->name($nameAndID);
        $this->id($nameAndID);
    }

    public function render()
    {
        $this->jqueryReady[] = '$("#' . $this->attributes['id'] . '").iCheck({checkboxClass: "' . $this->attributes['class'] . '"});';
        unset($this->attributes['class']); // Azzero la class
        return parent::render();
    }

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            if ($array[$name] === true)
            {
                $this->attr('checked', '');
            } else
            {
                unset($this->attributes['checked']);
            }
        }
        return $this;
    }

}
