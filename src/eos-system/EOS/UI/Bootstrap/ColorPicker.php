<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class ColorPicker extends \EOS\UI\Bootstrap\Input
{

    public function __construct($nameAndID)
    {
        parent::__construct($nameAndID);
        $this->id($nameAndID);
        $this->attributes[] = '<span class="input-group-addon"><i></i></span>';
    }

    public function render()
    {
        $this->jqueryReady[] = '$("#'.$this->attributes['id'].'").colorpicker();';
        return parent::render();
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        return parent::printRender($view, $tagscript, $tagready);
    }
}