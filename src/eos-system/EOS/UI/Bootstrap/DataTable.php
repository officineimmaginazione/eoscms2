<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class DataTable extends \EOS\UI\Control
{

    protected $columns = [];
    protected $ajaxUrl = '';
    protected $tokenName = '';
    protected $tokenValue = '';
    protected $rawParams = [];
    protected $jsEvents = [];
    protected $ajaxSend = '';
    protected $langUrl = '';
    protected $columnsOrder = [];

    public function __construct($id, $ajaxUrl = '', $tokenName = '', $tokenValue = '')
    {
        parent::__construct();
        $this->id($id);
        $this->tagName = 'table';
        $this->breakBlock = true;
        $this->ajaxUrl = $ajaxUrl;
        $this->tokenName = $tokenName;
        $this->tokenValue = $tokenValue;
        $this->attr('class', 'table table-striped table-hover');
        $this->attr('style', 'width: 100%');
        $this->addRawParams('pageLength', 25);
        //https://datatables.net/reference/option/stateDuration
        $this->addRawParams('stateSave', true);
        $this->addRawParams('orderMulti', true);
        $this->addRawParams('scrollX', true);
        $this->addRawParams('stateDuration', -1);
    }

    public function addRawParams($key, $value)
    {
        $this->rawParams[$key] = $value;
        return $this;
    }

    public function onAjaxSend($event)
    {
        $this->ajaxSend = $event;
        return $this;
    }

    public function addColumn($field, $title, $orderable = true, $searchable = true, $visible = true, $className = '')
    {
        $this->columns[] = ['field' => $field, 'title' => $title,
            'orderable' => $orderable, 'searchable' => $searchable,
            'visible' => $visible, 'className' => $className, 'render' => ''];
        return $this;
    }

    // http://www.datatables.net/examples/api/row_details.html
    public function addColumnCustom($title, $className, $defaultContent, $visible = true)
    {
        $this->addColumn(null, $title, false, false, $visible);
        $p = count($this->columns) - 1;
        $this->columns[$p]['className'] = $className;
        $this->columns[$p]['defaultContent'] = $defaultContent;
        $this->columns[$p]['render'] = '';
        return $this;
    }

    // https://datatables.net/examples/basic_init/table_sorting.html
    public function addColumnOrder($field, $asc = true)
    {
        $this->columnsOrder[$field] = $asc ? 'asc' : 'desc';
    }

    // https://www.datatables.net/
    // https://www.datatables.net/manual/server-side
    public function render()
    {
        if (strlen($this->ajaxUrl) > 0)
        {
            $this->addRawParams('serverSide', 'true');
            $this->addRawParams('processing', 'true');
            $this->addRawParams('language', '{"url": "' . $this->langUrl . '"}');
            $ajValue = sprintf('{"url":"%s", ', $this->ajaxUrl);
            if (strlen($this->tokenName) > 0)
            {
                if ($this->ajaxSend != '')
                {
                    $rawData = sprintf('(%s)(data);', $this->ajaxSend);
                } else
                {
                    $rawData = '';
                }
                $ajValue .= sprintf('"data":function (data) {data.%s = "%s";%s},', $this->tokenName, $this->tokenValue, $rawData);
            }
            $ajValue .= '"type":"POST"}';
            $this->addRawParams('ajax', $ajValue);
        }

        $cl = [];
        $clDef = [];
        $clOrder = [];
        $idx = -1;
        foreach ($this->columns as $col)
        {
            $item = [];
            if ($col['field'] == null)
            {
                $item['data'] = null;
                $item['title'] = $col['title'];
                $item['className'] = $col['className'];
                $item['defaultContent'] = $col['defaultContent'];
            } else
            {
                $item['data'] = $col['field'];
                $item['title'] = $col['title'];
                if ($col['className'] != '')
                {
                    $item['className'] = $col['className'];
                }
            }
            $cl[] = json_encode($item, JSON_UNESCAPED_UNICODE);
            $idx++;
            $item = [];
            $item['targets'] = $idx;
            $item['orderable'] = $col['orderable'];
            $item['searchable'] = $col['searchable'];
            $item['visible'] = $col['visible'];
            $clDefStr = json_encode($item, JSON_UNESCAPED_UNICODE);
            if ($col['render'] != '')
            {
                $clDefStr = substr($clDefStr, 0, strlen($clDefStr) - 1);
                $clDefStr .= ',"render":' . $col['render'];
                $clDefStr .= '}';
            }

            $clDef[] = $clDefStr;
            if ((!is_null($col['field'])) && ($col['orderable']) && (isset($this->columnsOrder[$col['field']])))
            {
                $clOrder[] = sprintf('[%d, "%s"]', $idx, $this->columnsOrder[$col['field']]);
            }
        }

        $this->addRawParams('columns', '[' . implode(',', $cl) . ']');
        $this->addRawParams('columnDefs', '[' . implode(',', $clDef) . ']');
        if (count($clOrder) > 0)
        {
            $this->addRawParams('order', '[' . implode(',', $clOrder) . ']');
        }

        $sr = '';
        $jid = '#' . $this->attributes['id'];
        $sr .= '$("' . $jid . '").DataTable({' . "\n";
        $idx = 0;
        foreach ($this->rawParams as $key => $v)
        {
            $idx++;
            $sr .= "\t\t" . sprintf('"%s":%s', $key, $v);
            if ($idx != count($this->rawParams))
            {
                $sr .= ",";
            }
            $sr .= "\n";
        }
        $sr .= "\t" . '});' . "\n";
        foreach ($this->jsEvents as $ev)
        {
            switch ($ev['event'])
            {
                case 'clickRow':
                    $evclass = $ev['class'] != '' ? ' .' . $ev['class'] : '';
                    $sr .= "\t" . '$("' . $jid . ' tbody").on("click", "tr' . $evclass . '", function (event) {' . "\n";
                    if ($evclass == '')
                    {
                        $sr .= "\t\t" . 'var tr = this;' . "\n";
                    } else
                    {
                        $sr .= "\t\t" . 'var tr = $(this).closest("tr");' . "\n";
                    }
                    $sr .= "\t\t" . 'var row = $("' . $jid . '").DataTable().row(tr);' . "\n";
                    $sr .= "\t\t" . '(' . $ev['method'] . ')(event, row);' . "\n";
                    ;
                    $sr .= "\t});\n";
                    break;

                default:
                    break;
            }
        }
        $this->jqueryReady[] = $sr;
        return parent::render();
    }

    public function bindList($list)
    {
        $this->addRawParams('data', json_encode($list, JSON_UNESCAPED_UNICODE));
        return $this;
    }

    // https://datatables.net/examples/advanced_init/events_live.html
    // method = function (event, row) { /* var data = row.data(); */ }
    // il return view ribaldato all'evento di click
    // class = css class per singolo elemento
    public function clickRow($method, $class = '')
    {
        $this->jsEvents[] = ['event' => 'clickRow', 'method' => $method, 'class' => $class];
        return $this;
    }

    //https://datatables.net/reference/option/columns.render
    /*
      function ( data, type, full, meta ) {
      return '<a href="'+data+'">Download</a>';
      }
     */
    public function onColumnRender($field, $method)
    {
        for ($i = 0; $i < count($this->columns); $i++)
        {
            if ($this->columns[$i]['field'] == $field)
            {
                $this->columns[$i]['render'] = $method;
                break;
            }
        }
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        if (!empty($view))
        {
            $this->langUrl = $view->path->getLibraryUrl() . 'datatables/1.10.18/langs/' . $view->lang->getCurrentISO() . '.json';
        }
        return parent::printRender($view, $tagscript, $tagready);
    }

}
