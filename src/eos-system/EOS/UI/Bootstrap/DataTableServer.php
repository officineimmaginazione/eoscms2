<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class DataTableServer
{

    private $fluentQuery;
    private $columns = [];
    private $encodeColumns = [];
    private $renderColumns = [];
    private $textMatchColumns = [];
    private $session;
    private $request;
    private $db;

    public function __construct($session, $request, \SelectQuery $fluentQuery, \EOS\System\Database\Database $db)
    {
        $this->fluentQuery = $fluentQuery;
        $this->request = $request;
        $this->session = $session;
        $this->fluentQuery = $fluentQuery;
        $this->db = $db;
    }

    private function select($params, $fluentQuery, $columns)
    {
        $fluentQuery->select(null); // Azzera select
        $parCols = $params['columns'];
        foreach ($parCols as $col)
        {
            if ($col['data'] != null)
            {
                if ((isset($columns[$col['data']])))
                {
                    $fluentQuery->select($columns[$col['data']] . ' AS ' . $col['data']);
                } else
                {
                    $fluentQuery->select("'' AS " . $col['data']);
                }
            }
        }
    }

    private function filter($params, $fluentQuery, $columns)
    {
        $wr = '';
        $parCols = $params['columns'];
        if (isset($params['search']) && $params['search']['value'] != '')
        {
            $str = '%' . $this->db->util->prepareLikeValue($params['search']['value']) . '%';
            foreach ($parCols as $col)
            {
                if (($col['searchable'] == 'true') && (isset($columns[$col['data']])))
                {
                    // Manca l'or  
                    //  $fluentQuery->where($columns[$col['data']] . ' like ?', $str);
                    if($this->textMatchColumns[$col['data']]) {
                        if (strlen($wr) > 0)
                        {
                            $wr .= ' OR ';
                        }
                        $wr .= sprintf("MATCH (%s) AGAINST ('%s*' IN BOOLEAN MODE)", $columns[$col['data']], $str);
                    } else {
                        if (strlen($wr) > 0)
                        {
                            $wr .= ' OR ';
                        }
                        $wr .= sprintf("(%s LIKE %s)", $columns[$col['data']], $this->db->quote($str));
                    }
                }
            }
        }
        // Individual column filtering
        if (isset($params['columns']))
        {
            $str = '%' . $params['search']['value'] . '%';
            foreach ($parCols as $col)
            {
                $str = $col['search']['value'];
                if (($col['searchable'] == 'true') && (isset($columns[$col['data']])) && ($str != ''))
                {
                    $str = '%' . $str . '%';
                    //
                    //$fluentQuery->where($columns[$col['data']] . ' like ?', $str);
                    $str = $col['search']['value'];
                    if (($col['searchable'] == 'true') && (isset($columns[$col['data']])) && ($str != ''))
                    {
                        $str = '%' . $str . '%';
                        //
                        //$fluentQuery->where($columns[$col['data']] . ' like ?', $str);
                        if($this->textMatchColumns[$col['data']]) {
                            if (strlen($wr) > 0)
                            {
                                $wr .= ' OR ';
                            }
                            $wr .= sprintf("MATCH (%s) AGAINST ('%s*' IN BOOLEAN MODE)", $columns[$col['data']], $str);
                        } else {
                            if (strlen($wr) > 0)
                            {
                                $wr .= ' OR ';
                            }
                            $wr .= sprintf("(%s LIKE %s)", $columns[$col['data']], $this->db->quote($str));
                        }
                    }
                }
            }
        }
        if (strlen($wr) > 0)
        {
            $fluentQuery->where('('.$wr.')');
        }
    }

    private function order($params, $fluentQuery, $columns)
    {
        $order = '';
        if (isset($params['order']) && count($params['order']))
        {
            $parOrders = $params['order'];
            $parCols = $params['columns'];
            $orderList = [];
            foreach ($parOrders as $order)
            {
                $col = $parCols[$order['column']];
                if (($col['orderable'] == 'true') && (isset($columns[$col['data']])))
                {
                    $field = $columns[$col['data']];
                    $dir = $order['dir'] === 'asc' ? 'ASC' : 'DESC';
                    $orderList[] = $field . ' ' . $dir;
                }
            }
            if (count($orderList) > 0)
            {
                $fluentQuery->orderBy(null);
                $fluentQuery->orderBy(implode(', ', $orderList));
            }
        }
    }

    private function limit($params, $fluentQuery)
    {
        if (isset($params['start']) && $params['length'] != -1)
        {
            //$fluentQuery->limit(intval($params['start']));
            //$fluentQuery->offset(intval($params['length']));
            $fluentQuery->limit(intval($params['start']) . ',' . intval($params['length']));
        }
    }

    public function addColumn($name, $dbname, $encodeHtml = false, $match = false)
    {
        $this->columns[$name] = $dbname;
        $this->textMatchColumns[$name] = $match;
        if ($encodeHtml)
        {
            $this->encodeColumns[] = $name;
        }
        return $this;
    }

    //https://www.datatables.net/manual/server-side
    //https://github.com/DataTables/DataTables/blob/master/examples/server_side/scripts/ssp.class.php
    /*
      $columns['alias'] = 'table.field';
      $columns = ['alias1' => 'table.field1', 'alias2' => 'table.field2'];
     */

    public function toArray()
    {
        $res['recordsTotal'] = 0;
        $res['recordsFiltered'] = 0;
        $res['data'] = [];
        $params = $this->request->getParsedBody();
        $res['draw'] = $params['draw'];
        if ($this->session->isValidTokenArrayKey($params))
        {
            if (count($this->columns) > 0)
            {
                $this->select($params, $this->fluentQuery, $this->columns);
                $this->filter($params, $this->fluentQuery, $this->columns);
                $this->order($params, $this->fluentQuery, $this->columns);
                $this->limit($params, $this->fluentQuery);
                $data = $this->fluentQuery->fetchAll();
                if ((count($this->encodeColumns) > 0) || (count($this->renderColumns) > 0))
                {
                    for ($i = 0; $i < count($data); $i++)
                    {
                        foreach ($this->encodeColumns as $col)
                        {
                            $data[$i][$col] = \EOS\System\Util\StringHelper::encodeHtml($data[$i][$col]);
                        }

                        foreach ($this->renderColumns as $col => $method)
                        {
                            if ($method instanceof \Closure)
                            {
                                $data[$i][$col] = $method($data[$i][$col], $data[$i]);
                            }
                        }
                    }
                }
                $res['data'] = $data;
                $this->fluentQuery->limit(null)->orderBy(null);
                $res['recordsTotal'] = $this->fluentQuery->count();
                $res['recordsFiltered'] = $res['recordsTotal'];
            }
        } else
        {
            $res['error'] = 'Invalid token!';
        }
        return $res;
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    public function onColumnRender($field, $method)
    {
        $this->renderColumns[$field] = $method;
    }

}
