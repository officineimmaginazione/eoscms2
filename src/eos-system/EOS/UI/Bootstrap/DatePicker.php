<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class DatePicker extends \EOS\UI\Bootstrap\Input
{

    private $_language = 'it';
    private $_dateFormat = 'dd/mm/yyyy';

    public function __construct($nameAndID)
    {
        parent::__construct($nameAndID);
        $this->id($nameAndID);
    }

    public function language($value)
    {
        $this->_language = $value;
        return $this;
    }

    public function dateFormat($value)
    {
        $this->_dateFormat = $value;
        return $this;
    }

    public function render()
    {
        $this->jqueryReady[] = '$("#'.$this->attributes['id'].'").datepicker({format: "' . $this->_dateFormat . '", language: "' . $this->_language . '", disableTouchKeyboard: false });';
        return parent::render();
    }
    
    
    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        if (!is_null($view))
        {
            $this->dateFormat($view->lang->getCurrentDatePickerFormat());
            $this->language($view->lang->getCurrentISO());
        }
        return parent::printRender($view, $tagscript, $tagready);
    }

}
