<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class DropDownMenu extends \EOS\UI\Control
{

    protected $currentkey = null;
    protected $buttonkey = 'data-key1';
    protected $items = [];
    protected $eventchange = '';
	protected $classes = '';

    public function __construct($id)
    {
        parent::__construct();
        $this->tagName = 'button';
        $this->id($id);
        $this->attr('class', 'btn btn-default dropdown-toggle');
        $this->attr('data-toggle','dropdown');
    }

    public function change($event)
    {
        $this->addJQueryEvent('change', $event);
        return $this;
    }

    public function disabled($value)
    {
        if ($value)
        {
            $this->attributes['disabled'] = '';
        } else
        {
            unset($this->attributes['disabled']);
        }
        return $this;
    }

    public function addMenu($key1, $key2, $title)
    {
        $this->items[] = ['key1' => $key1, 'key2' => $key2, 'title' => $title];
        return $this;
    }
	
	public function dropDownMenuClasses ($classes) {
		$this->classes = $classes;
	}

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->currentkey = $array[$name];
        }
        return $this;
    }

    public function bindList($array, $key1, $key2, $title)
    {
        $this->buttonkey = ($key2 == '') ?  'data-key2' :  'data-key1';
        foreach ($array as $item)
        {
            $this->addMenu($item[$key1], $item[$key2], $item[$title]);
        }
        return $this;
    }

    public function render()
    {
        $res = parent::render();
        
        $eventSel = '$("#' . $this->attributes['id'] . '").html($("#' . $this->attributes['id'] . '-list .dropdown-select")[0].getAttribute("data-title"));'."\n";
        $clearsel = '$("#' . $this->attributes['id'] . '-list a").removeClass("dropdown-select"); $(this).addClass("dropdown-select");'."\n";
        $this->jqueryReady[] = '$("#' . $this->attributes['id'] . '-list a").click(function (event){'.$clearsel.' '.$eventSel.' event.preventDefault();(' . $this->eventchange . ')(this);});'."\n";
        $this->jqueryReady[] = $eventSel;
        
		$res .= "<ul id='".$this->attributes['id']."-list' class='dropdown-menu " . $this->classes . "'>";
        foreach ($this->items as $item)
        {
            $sel = '';
            if($this->currentkey == $item['key1']) {
                $this->jqueryReady[] = '(' . $this->eventchange . ')($("#' . $this->attributes['id'] . '-list .dropdown-select")[0])'."\n";
                $sel = "class='dropdown-select'";
            }
            $res .= sprintf("<li class='dropdown-item'><a ".$sel." href='%s' data-key1='%s' data-key2='%s' data-title='%s'>%s</a></li>\n", '', $item['key1'], $item['key2'], $item['title'], $item['title']);
        }
        $res .= "</ul>";
        //$btn = '<button id="'. $this->attributes['id'].'-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"></button>';
        return $res;
    }

    public function onChange($event)
    {
        $this->eventchange = $event;
    }
	
	

}