<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class GlyphIcon extends \EOS\UI\Control
{

    public function __construct($icon)
    {
        parent::__construct();
        $this->tagName = 'span';
        $this->attributes['class'] = 'glyphicon '.  strtolower($icon);
        $this->attributed['aria-hidden'] = 'true';
    }
}
