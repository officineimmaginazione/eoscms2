<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Heading1 extends \EOS\UI\Html\Heading
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'h1';
        $this->attributes['class'] = 'content-header-title';
    }

}
