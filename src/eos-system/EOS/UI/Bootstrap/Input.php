<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Input extends \EOS\UI\Html\Input
{

    public function __construct($name)
    {
        parent::__construct();
        $this->attributes['class'] = 'form-control';
        $this->name($name);
    }

}
