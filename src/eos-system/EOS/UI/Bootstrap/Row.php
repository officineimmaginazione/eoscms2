<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Row extends \EOS\UI\Html\Div
{

    public function __construct($id = '')
    {
        parent::__construct();
        $this->id($id);
        $this->attributes['class'] = 'row';
    }

}
