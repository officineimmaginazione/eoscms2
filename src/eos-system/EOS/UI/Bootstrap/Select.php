<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Select extends \EOS\UI\Html\Select
{

    public function __construct($name)
    {
        parent::__construct();
        $this->attributes['class'] = 'custom-select';
        $this->name($name);
    }
}
