<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Select2 extends \EOS\UI\Html\Select {

	protected $rawParams = [];
	private $_language = 'it';

	public function __construct($nameAndID) {
		parent::__construct();
		$this->attributes['class'] = 'custom-select';
		$this->name($nameAndID);
		$this->id($nameAndID);
	}

	public function addRawParams($key, $value) {
		$this->rawParams[$key] = $value;
		return $this;
	}

	public function language($value) {
		$this->_language = $value;
		return $this;
	}

	public function render() {
		$this->addRawParams('language', '"' . $this->_language . '"');
		$params = [];
		foreach ($this->rawParams as $k => $v) {
			$params[] = $k . ':' . $v;
		}
		$this->jqueryReady[] = '$("#' . $this->attributes['id'] . '").select2({' . implode(',', $params) . '});';
		return parent::render();
	}

	public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true) {
		if (!is_null($view)) {
			$this->language($view->lang->getCurrentISO());
		}
		return parent::printRender($view, $tagscript, $tagready);
	}

}
