<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Tab extends \EOS\UI\Control
{

    private $_items = [];

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'div';
        $this->attributes['class'] = 'nav-tabs-custom';
    }

    public function addItem($id, $title)
    {
        $item['title'] = \EOS\System\Util\StringHelper::encodeHtml($title);
        $item['content'] = '';
        $this->_items[$id] = $item;
        return $this;
    }

    public function render()
    {
        $activeLi = 'active';
		$activeA = 'active show';
        $this->contentValue .= '<ul class="nav nav-tabs nav-fill">' . "\n";
        foreach ($this->_items as $k => $v)
        {
            $this->contentValue .= sprintf('<li class="nav-item %s" ><a class="nav-link %s" data-toggle="tab" href="#%s">%s</a></li>', $activeLi, $activeA, $k, $v['title']) . "\n";
            $activeLi = '';
			$activeA = '';
        }
        $this->contentValue .= '</ul>' . "\n";
        $this->contentValue .= '<div class="tab-content">' . "\n";
        $active = ' active show';
        foreach ($this->_items as $k => $v)
        {
            $this->contentValue .= sprintf('<div class="tab-pane fade%s" id="%s">%s</div>', $active, $k, $v['content']) . "\n";
            $active = '';
        }
        $this->contentValue .= '</div>' . "\n";
        $this->_items = []; // Azzero tutto
        return parent::render();
    }

    public function startTab()
    {
        ob_start();
        return $this;
    }

    public function endTab($id)
    {
        $this->_items[$id]['content'] .= ob_get_contents();
        ob_end_clean();
        return $this;
    }

}
