<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Textarea extends \EOS\UI\Html\Textarea
{

    public function __construct($name)
    {
        parent::__construct();
        $this->attributes['class'] = 'form-control';
        $this->name($name);
    }

}
