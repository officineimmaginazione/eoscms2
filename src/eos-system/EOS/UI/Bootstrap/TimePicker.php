<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class TimePicker extends \EOS\UI\Bootstrap\Input
{

    public function __construct($nameAndID)
    {
        parent::__construct($nameAndID);
        $this->id($nameAndID);
    }

    public function render()
    {
        $this->jqueryReady[] = '$("#'.$this->attributes['id'].'").timepicker({'
            . 'template:false, minuteStep: 1,showSeconds: false, showMeridian: false});';
        return parent::render();
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        return parent::printRender($view, $tagscript, $tagready);
    }
}