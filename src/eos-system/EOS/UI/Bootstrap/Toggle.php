<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Bootstrap;

class Toggle extends \EOS\UI\Control {
	
	private $_checked = '';

	public function __construct($nameAndID) {
		parent::__construct();
		$this->attributes['class'] = 'toggle';
		$this->tagName = 'label';
		$this->name($nameAndID);
		$this->id($nameAndID);
	}

	public function render() {
		$this->contentValue = '<input id="'. $this->attributes['id'] . '" name="'. $this->attributes['name'] . '" type="checkbox" ' . $this->_checked . '>'
				. '<span class="slider"></span>';
		//var_dump($this->contentValue); exit;
		return parent::render();
	}

	public function bind($array, $name) {
		if (isset($array[$name])) {
			if ($array[$name] === true) {
				$this->_checked = 'checked';
			}
			else {
				$this->_checked = ''; 
			}
		}
		return $this;
	}
}