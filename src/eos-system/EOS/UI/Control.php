<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI;

abstract class Control
{

    protected $attributes = [];
    protected $contentValue = '';
    protected $tagName = '';
    protected $isBlock = true;
    protected $breakBlock = false;
    protected $javaScriptList = [];
    protected $jqueryEvent = [];
    protected $jqueryReady = [];

    private function insertOrAdd(&$list, $key, $value, $separator)
    {
        if (isset($list[$key]))
        {
            $list[$key] .= $separator . $value;
        } else
        {
            $list[$key] = $value;
        }
    }

    protected function renderOpen($tag, $attributes)
    {
        $res = '<' . $tag;
        if (count($attributes) > 0)
        {
            if (isset($attributes['id']))
            {
                $res .= ' id="' . $attributes['id'] . '"';
            }
            foreach ($attributes as $key => $value)
            {
                if ($key != 'id')
                {
                    $res .= ' ' . $key;
                    if (strlen($value) > 0)
                    {
                        $res .= '="' . $value . '"';
                    }
                }
            }
        }
        $res .='>';
        return $res;
    }

    protected function renderClose($tag)
    {
        return '</' . $tag . '>';
    }

    protected function removeAttrListValue($attrname, $attrvalue, $attrsep)
    {
        if (isset($this->attributes[$attrname]))
        {
            $tmp = explode($attrsep, $this->attributes[$attrname]);
            $this->attributes[$attrname] = implode($attrsep, array_diff($tmp, [$attrvalue]));
        }
    }

    protected function addAttrListValue($attrname, $attrvalue, $attrsep)
    {
        if (isset($this->attributes[$attrname]))
        {
            $tmp = explode($attrsep, $this->attributes[$attrname]);
            if (!in_array($attrvalue, $tmp))
            {
                $this->attributes[$attrname] = implode($attrsep, array_merge($tmp, [$attrvalue]));
            }
        }
    }

    protected function addJQueryEvent($eventName, $func)
    {
        if ((count($this->jqueryEvent) == 0) && (!isset($this->attributes['id'])))
        {
            $nl = explode('\\', static::class);
            $this->attributes['id'] = strtolower(end($nl) . '_' . uniqid());
        }
        $this->jqueryEvent[$eventName] = $func;
    }

    protected function bind($array, $name)
    {
        return $this;
    }

    protected function tag($value)
    {
        $this->tagName = strtolower($value);
        return $this;
    }

    public function __construct()
    {
        
    }

    public function render()
    {
        $eol = ($this->breakBlock) ? "\n" : '';
        $r = $this->renderOpen($this->tagName, $this->attributes) . $eol;
        if ($this->isBlock)
        {
            if (strlen($this->contentValue) > 0)
            {
                $r .= $this->contentValue;
            }
            $r .= $this->renderClose($this->tagName) . "\n";
        }
        return $r;
    }

    public function renderJavaScript($tagscript = true, $tagready = true)
    {
        $r = '';

        if ((count($this->javaScriptList) > 0))
        {
            if ($tagscript)
            {
                $r .= '<script>' . "\n";
            }
            $r .= implode("\n", $this->javaScriptList);

            if ($tagscript)
            {
                $r .= '</script>' . "\n";
            }

            $this->javaScriptList = [];
            $this->jqueryEvent = [];
        }

        if ((count($this->jqueryReady) > 0) || (count($this->jqueryEvent) > 0))
        {
            if ($tagscript)
            {
                $r .= '<script>' . "\n";
            }
            if ($tagready)
            {
                $r .= '$(function () {' . "\n";
            }

            $idElem = $this->attributes['id'];
            if ((count($this->jqueryEvent) > 0) && (strlen($idElem) > 0))
            {
                foreach ($this->jqueryEvent as $e => $v)
                {
                    $r .= "\t" . sprintf('$("#%s").on("%s",%s);', $idElem, $e, $v) . "\n";
                }
            }

            foreach ($this->jqueryReady as $v)
            {
                $r .= "\t" . $v . "\n";
            }

            if ($tagready)
            {
                $r .= '});' . "\n";
            }

            if ($tagscript)
            {
                $r .= '</script>' . "\n";
            }
            $this->jqueryReady = [];
        }



        return $r;
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        echo $this->render();
        if (count($this->javaScriptList) > 0)
        {
            if ($view != null)
            {
                $view->startCaptureScript();
                $bkReady = $this->jqueryReady;
                $bkEvent = $this->jqueryEvent;
                $this->jqueryReady = [];
                $this->jqueryEvent = [];
            }
            echo $this->renderJavaScript($tagscript, $tagready);
            if ($view != null)
            {
                $view->endCaptureScript();
                $this->jqueryReady = $bkReady;
                $this->jqueryEvent = $bkEvent;
                $bkReady = null;
                $bkEvent = null;
            }
        }

        if ((count($this->jqueryReady) > 0) || (count($this->jqueryEvent) > 0))
        {
            if ($view != null)
            {
                $tagscript = false;
                $tagready = false;
                $view->startCaptureScriptReady();
            }
            echo $this->renderJavaScript($tagscript, $tagready);
            if ($view != null)
            {
                $view->endCaptureScriptReady();
            }
        }
        return $this;
    }

    public function content($value)
    {
        $this->contentValue = $value;
        return $this;
    }

    public function contentEncode($value)
    {
        $this->contentValue = \EOS\System\Util\StringHelper::encodeHtml($value);
        return $this;
    }

    public function startContent()
    {
        ob_start();
        return $this;
    }

    public function endContent()
    {
        $this->contentValue .= ob_get_contents();
        ob_end_clean();
        return $this;
    }

    public function addContent($callable, $view)
    {
        if ($callable instanceof \Closure)
        {
            $this->startContent();
            $bound = \Closure::bind($callable, $view);
            $bound();
            $this->endContent();
        }
        return $this;
    }

    public function name($value)
    {
        $this->attributes['name'] = $value;
        return $this;
    }

    public function id($value)
    {
        if (strlen($value) > 0)
        {
            $this->attributes['id'] = strtolower($value);
        }
        return $this;
    }

    public function attr($name, $value)
    {
        $lname = strtolower($name);
        switch ($lname)
        {
            case 'class':
                $this->insertOrAdd($this->attributes, $lname, $value, ' ');
                break;
            case 'style':
                $this->insertOrAdd($this->attributes, $lname, $value, ';');
                break;
            default :
                $this->attributes[$lname] = $value;
                break;
        }
        return $this;
    }

}
