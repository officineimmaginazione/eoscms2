<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

class Button extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'button';
    }

    public function click($event)
    {
        $this->addJQueryEvent('click', $event);
        return $this;
    }

    public function disabled($value)
    {
        if ($value)
        {
            $this->attributes['disabled'] = '';
        } else
        {
            unset($this->attributes['disabled']);
        }
        return $this;
    }

}
