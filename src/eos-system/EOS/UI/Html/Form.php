<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

use EOS\System\Util\StringHelper;

class Form extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'form';
        $this->breakBlock = true;
    }

    public function method($value)
    {
        $this->attributes['method'] = StringHelper::encodeHtml(strtolower($value));
        return $this;
    }

    public function action($value)
    {
        $this->attributes['action'] = StringHelper::encodeHtml($value);
        return $this;
    }

}
