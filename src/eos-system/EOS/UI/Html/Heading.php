<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

use EOS\System\Util\StringHelper;

class Heading extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'h1';
    }

}
