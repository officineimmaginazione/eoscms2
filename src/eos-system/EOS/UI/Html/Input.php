<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

use EOS\System\Util\StringHelper;

class Input extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'input';
        $this->isBlock = false;
        $this->breakBlock = true;
        $this->type('text');
    }

    public function value($value)
    {
        $this->attributes['value'] = StringHelper::encodeHtml($value);
        return $this;
    }

    public function name($value)
    {
        if (strlen($value) > 0)
        {
            $this->attributes['name'] = StringHelper::encodeHtml(strtolower($value));
        }
        return $this;
    }

    public function type($value)
    {
        $this->attributes['type'] = StringHelper::encodeHtml($value);
        return $this;
    }

    public function placeholder($value)
    {
        $this->attributes['placeholder'] = StringHelper::encodeHtml($value);
        return $this;
    }

    public function disabled($value)
    {
        if ($value)
        {
            $this->attributes['disabled'] = '';
        } else
        {
            unset($this->attributes['disabled']);
        }
        return $this;
    }

    public function required($value)
    {
        if ($value)
        {
            $this->attributes['required'] = '';
        } else
        {
            unset($this->attributes['required']);
        }
        return $this;
    }

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->value($array[$name]);
        }
        return parent::bind($array, $name);
    }

}
