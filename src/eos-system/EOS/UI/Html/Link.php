<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

class Link extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'a';
    }
    
    public function href($value)
    {
        $this->attributes['href'] = \EOS\System\Util\StringHelper::encodeHtml($value);
        return $this;
    }

}
