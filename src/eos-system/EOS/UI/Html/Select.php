<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

use EOS\System\Util\StringHelper;

class Select extends \EOS\UI\Control
{

    protected $currentkey = null;

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'select';
        $this->breakBlock = true;
    }

    public function change($event)
    {
        $this->addJQueryEvent('change', $event);
        return $this;
    }

    public function disabled($value)
    {
        if ($value)
        {
            $this->attributes['disabled'] = '';
        } else
        {
            unset($this->attributes['disabled']);
        }
        return $this;
    }

    public function addOption($value, $content, $selected = false)
    {
        $extra = '';
        if ($selected == true)
        {
            $extra = ' selected';
        }
        $this->contentValue .= sprintf("<option value='%s'%s>%s</option>\n", StringHelper::encodeHtml($value), $extra, $content);
        return $this;
    }

    public function addOptionEncode($value, $text, $selected = false)
    {
        return $this->addOption($value, StringHelper::encodeHtml($text), $selected);
    }

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->currentkey = $array[$name];
        }
        return $this;
    }

    public function bindList($array, $key, $descr, $firstblank = false)
    {
        $isMultiple = is_array($this->currentkey);
        if($firstblank == true) {
            $this->addOptionEncode(0, ' --- ');
        }
        
        foreach ($array as $item)
        {
            if ($isMultiple)
            {
                $sel = in_array($item[$key], $this->currentkey);
            } else
            {
                $sel = $item[$key] == $this->currentkey ? true : false;
            }
            $this->addOptionEncode($item[$key], $item[$descr], $sel);
        }
        return $this;
    }

    public function multiple()
    {
        return $this->attr('multiple', 'multiple');
    }

}
