<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Html;

class Textarea extends \EOS\UI\Control
{

    public function __construct()
    {
        parent::__construct();
        $this->tagName = 'textarea';
        $this->breakBlock = true;
        $this->rows(4);
        $this->cols(50);
    }

    public function rows($value)
    {
        return $this->attr('rows', $value);
    }

    public function cols($value)
    {
        return $this->attr('cols', $value);
    }

    public function name($value)
    {
        if (strlen($value) > 0)
        {
            $this->attributes['name'] = strtolower($value);
        }
        return $this;
    }

    public function disabled($value)
    {
        if ($value)
        {
            $this->attributes['disabled'] = '';
        } else
        {
            unset($this->attributes['disabled']);
        }
        return $this;
    }
    
    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->contentEncode($array[$name]);
        }
        return $this;
    }

}
