<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Bootbox extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "bootbox/4.4.0/js/bootbox.min.js");
    }

}
