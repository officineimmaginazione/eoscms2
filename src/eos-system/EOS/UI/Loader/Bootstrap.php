<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Bootstrap extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
      static::load($this->view, 'JQuery');
			if($this->getVersion('bootstrap') == 3) {
				$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "bootstrap/3.3.7/css/bootstrap.min.css");
				$this->view->addScriptLink($this->view->path->getLibraryUrl() . "bootstrap/3.3.7/js/bootstrap.min.js");
			} 
			else {
				$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "bootstrap/4.3.1/css/bootstrap.min.css");
				$this->view->addScriptLink($this->view->path->getLibraryUrl() . "popper/1.14.6/popper.min.js");
				$this->view->addScriptLink($this->view->path->getLibraryUrl() . "bootstrap/4.3.1/js/bootstrap.min.js");
			}
    }

}
