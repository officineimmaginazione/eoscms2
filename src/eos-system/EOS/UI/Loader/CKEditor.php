<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class CKEditor extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'JQuery');
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "ckeditor/4.7.1/ckeditor.js");
    }

}
