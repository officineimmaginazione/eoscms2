<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class ColorPicker extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "colorpicker/2.5.1/css/bootstrap-colorpicker.min.css");
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "colorpicker//2.5.1/js/bootstrap-colorpicker.min.js");
    }

}
