<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class DataTables extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
		if($this->getVersion('datatables') == "1.10.15") {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "datatables/1.10.15/datatables.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.15/datatables.min.js");
		} 
		else {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "datatables/1.10.18/datatables.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.18/datatables.min.js");
		}
    }

}
