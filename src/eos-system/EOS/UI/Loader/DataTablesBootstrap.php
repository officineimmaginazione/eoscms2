<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class DataTablesBootstrap extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $lang = $this->view->lang->getCurrentISO();
		if($this->getVersion('datatables') == "1.10.15") {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "datatables/1.10.15/datatables.bootstrap4.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.15/datatables.min.js");
            $this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.15/dataTables.bootstrap4.min.js");
        } 
		else {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "datatables/1.10.18/DataTables-1.10.18/css/datatables.bootstrap4.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.18/datatables.min.js");
            $this->view->addScriptLink($this->view->path->getLibraryUrl() . "datatables/1.10.18/DataTables-1.10.18/js/dataTables.bootstrap4.min.js");
        }
    }

}
