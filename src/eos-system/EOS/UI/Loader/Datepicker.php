<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Datepicker extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "datepicker/1.6.4/css/bootstrap-datepicker3.min.css");
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "datepicker/1.6.4/js/bootstrap-datepicker.min.js");
        $lang = $this->view->lang->getCurrentISO();
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . 'datepicker/1.6.4/locales/bootstrap-datepicker.'.$lang.'.min.js');
    }

}
