<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class FullCalendar extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "moment/2.18.1/moment.min.js");
        $lang = $this->view->lang->getCurrentISO();
        //$this->view->addScriptLink($this->view->path->getLibraryUrl() . 'moment/2.18.1/locale/'.$lang.'.js');
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "fullcalendar/3.4.0/fullcalendar.min.js");
        $lang = $this->view->lang->getCurrentISO();
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . 'fullcalendar/3.4.0/locale/'.$lang.'.js');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "fullcalendar/3.4.0/fullcalendar.min.css");
    }

}
