<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Icons extends \EOS\UI\Loader\Library
{

	public function prepare()
	{
		if ($this->getVersion('icons') == 4) {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "font-awesome/4.7.0/css/font-awesome.min.css");
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "ionicons/2.0.1/css/ionicons.min.css");
		} else {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "font-awesome/5.15.1/css/all.min.css");
		}
	}
}
