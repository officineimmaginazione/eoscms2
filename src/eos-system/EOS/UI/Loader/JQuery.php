<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class JQuery extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
		if($this->getVersion('jquery') == 2) {
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "jquery/2.2.4/jquery.min.js");
		} 
		else {
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "jquery/3.4.1/jquery.min.js");
		}
    }

}
