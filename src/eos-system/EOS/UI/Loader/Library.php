<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Library
{

    protected $view;

	protected function getSettings(array $keys) {
		return \EOS\System\Util\ArrayHelper::getMdInt($this->view->settings, $keys);
	}
	
	protected function getVersion(string $key) {
		return \EOS\System\Util\ArrayHelper::getMdInt($this->view->settings, [$key, 'version']);
	}
	
    public function __construct(\EOS\System\Views\Html $view)
    {
        $this->view = $view;
    }

    public function prepare()
    {
        
    }

    public static function load(\EOS\System\Views\Html $view, $librayName)
    {
        $class = '\\EOS\UI\\Loader\\' . $librayName;
        $o = new $class($view);
        $o->prepare();
    }

}
