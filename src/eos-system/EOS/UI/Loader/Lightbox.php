<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Lightbox extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'JQuery');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "ekko-lightbox/5.2.0/ekko-lightbox.min.css");
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "ekko-lightbox/5.2.0/ekko-lightbox.min.js");
    }

}
