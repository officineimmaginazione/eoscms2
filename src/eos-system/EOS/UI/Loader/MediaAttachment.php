<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2017
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class MediaAttachment extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Icons');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "eos/media/css/eos.media.attachment.css");
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "eos/media/js/eos.media.attachment.js");
    }

}
