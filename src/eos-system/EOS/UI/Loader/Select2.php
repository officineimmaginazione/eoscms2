<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Select2 extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
		if($this->getVersion('bootstrap') == 3) {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "select2/bootstrap3/4.0.3/select2.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "select2/bootstrap3/4.0.3/select2.min.js");
		} 
		else {
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "select2/bootstrap4/1.0.2/select2.min.css");
			$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "select2/bootstrap4/1.0.2/select2.bootstrap4.min.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "select2/bootstrap4/1.0.2/select2.min.js");
		}
		
        // Devo caricare anche il js della lingua
		$this->view->addScriptLink($this->view->path->getLibraryUrl() . "select2/i18n/" . $this->view->lang->getCurrentISO() . ".js");
    }

}
