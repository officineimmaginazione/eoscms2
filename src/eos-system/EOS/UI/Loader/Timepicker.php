<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class Timepicker extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
        $this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "timepicker/0.5.2/bootstrap-timepicker.min.css");
        $this->view->addScriptLink($this->view->path->getLibraryUrl() . "timepicker/0.5.2/bootstrap-timepicker.min.js");
    }

}
