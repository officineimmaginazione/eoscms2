<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Loader;

class iCheck extends \EOS\UI\Loader\Library
{

    public function prepare()
    {
        static::load($this->view, 'Bootstrap');
		$this->view->addStyleSheetLink($this->view->path->getLibraryUrl() . "icheck/1.0.2/skins/square/_all.css");
			$this->view->addScriptLink($this->view->path->getLibraryUrl() . "icheck/1.0.2/icheck.min.js");
		
    }

}
