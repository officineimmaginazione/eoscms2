<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\System\Media;

use EOS\System\Util\PHPHelper;

class Attachment extends \EOS\UI\Html\Div
{

    private $media;
    private $commandUrl;
    private $idContainer;
    private $name;
    private $value;
    private $deleteBeforeOnUnload = true;
    private $readOnly = false;

    public function __construct(\EOS\System\Media\Attachment $media, string $name, string $commandUrl)
    {
        parent::__construct();
        $this->media = $media;
        $this->name = $name;
        $this->idContainer = $name . '-container';
        $this->id($this->idContainer);
        $this->attributes['class'] = 'media-attachment';
        $this->commandUrl = $commandUrl;
        $this->value = '';
        // Pulisco la temp in visualizzazione
        $this->media->getTemp()->clear();
    }

    public function render()
    {
        return parent::render();
    }

    public function printRender(\EOS\System\Views\Html $view = null, $tagscript = true, $tagready = true)
    {
        if (is_null($view))
        {
            throw new \Exception(get_class($this) . 'required View param in printRender!');
        }

        $size = $this->media->getMaxSizeByte();
        $size = ($size <= PHPHelper::getPostMaxSize()) ? $size : PHPHelper::getPostMaxSize();
        $size = ($size <= PHPHelper::getUploadMaxSize()) ? $size : PHPHelper::getUploadMaxSize();

        $uniName = 'emf_' . $view->createID();
        $tokenID = '';
        $this->jqueryReady[] = sprintf("var %s = new EOS.Media.Attachment('#%s', '%s');", $uniName, $this->idContainer, $this->name);
        $this->jqueryReady[] = sprintf("%s.commandUrl = '%s';", $uniName, addslashes($this->commandUrl));
        $this->jqueryReady[] = sprintf("%s.token = {'%s': '%s'};", $uniName, addslashes($view->session->getTokenName($tokenID)), addslashes($view->session->getTokenValue($tokenID)));
        $this->jqueryReady[] = sprintf("%s.uploadMaxSize = %d;", $uniName, $size);
        $this->jqueryReady[] = sprintf("%s.maxItems = %d;", $uniName, $this->media->getMaxItems());
        if ($this->readOnly)
        {
            $this->jqueryReady[] = sprintf("%s.readOnly = true;", $uniName);
        }
        // Messaggi
        $this->jqueryReady[] = sprintf("%s.msgSizeError = '%s';", $uniName, addslashes($view->lang->transEncode('system.media.attachment.error.size')));
        $this->jqueryReady[] = sprintf("%s.msgDeleteItem = '%s';", $uniName, addslashes($view->lang->transEncode('system.media.attachment.delete')));
        //
        $this->jqueryReady[] = sprintf("%s.draw();", $uniName);
        //
        if ($this->value !== '')
        {
            $this->jqueryReady[] = sprintf("%s.load('%s');", $uniName, addslashes($this->value));
        }
        if ($this->deleteBeforeOnUnload)
        {
            $this->jqueryReady[] = sprintf("%s.deleteBeforeOnUnload();", $uniName);
        }
        return parent::printRender($view, $tagscript, $tagready);
    }

    public function deleteBeforeOnUnload(bool $value): object
    {
        $this->deleteBeforeOnUnload = $value;
        return $this;
    }

    public function readOnly(bool $value): object
    {
        $this->readOnly = $value;
        return $this;
    }

    public function value(string $value)
    {
        $this->value = $value;
        return $this;
    }

    public function bind($array, $name)
    {
        if (isset($array[$name]))
        {
            $this->value($array[$name]);
        }
        return $this;
    }

}
