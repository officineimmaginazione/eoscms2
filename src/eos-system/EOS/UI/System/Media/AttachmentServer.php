<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\System\Media;

use EOS\System\Util\ArrayHelper;
use EOS\System\Util\MessageHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Security\CryptHelper;

class AttachmentServer
{

    protected $media;
    protected $container;
    protected $lang;
    protected $session;
    protected $tokenSalad = '';

    protected function getToken(int $id, string $name): string
    {
        return hash('sha256', (string) $id . '-' . $name . '-' . $this->tokenSalad, false);
    }

    public function __construct($container, \EOS\System\Media\Attachment $media)
    {
        $this->media = $media;
        $this->container = $container;
        $this->lang = $this->container->get('language');
        $this->session = $this->container->get('session');
        $tkName = 'EOS\UI\System\Media\AttachmentServer.TokenSalad';
        $this->tokenSalad = $this->session->get($tkName);
        if ($this->tokenSalad === '')
        {
            $this->tokenSalad = CryptHelper::randomString(10);
            $this->session->set($tkName, $this->tokenSalad);
        }
    }

    protected function isValidUpload($newFile, &$error): bool
    {
        $res = true;
        if ($newFile->getError() !== UPLOAD_ERR_OK)
        {
            $error = $this->lang->transEncode('system.media.attachment.error.uploaderror');
            $res = false;
        } else
        if (!in_array($newFile->getClientMediaType(), array_values($this->media->getMimeList())))
        {
            $error = $this->lang->transEncode('system.media.attachment.error.mimetype');
            $res = false;
        } else if ($newFile->getSize() > $this->media->getMaxSizeByte())
        {
            $error = $this->lang->transEncode('system.media.attachment.error.size');
            $res = false;
        }
        return $res;
    }

    protected function checkItemsToken(array $items, string &$error): bool
    {
        $res = true;
        // Controllo sicurezza Token
        foreach ($items as $item)
        {
            $id = ArrayHelper::getInt($item, 'id');
            $name = ArrayHelper::getStr($item, 'name');
            $token = ArrayHelper::getStr($item, 'token');
            if (($name !== '') || ($id !== 0))
            {
                if (!hash_equals($this->getToken($id, $name), $token))
                {
                    $error = $this->lang->transEncode('system.media.attachment.error.invalidcommand');
                    $res = false;
                    break;
                }
            }
        }
        return $res;
    }

    protected function clearTemp(array $items)
    {
        $counter = 0;
        $maxItems = $this->media->getMaxItems();
        $tempMedia = $this->media->getTemp();
        foreach ($items as $item)
        {
            $counter++;
            // Blocco di sicurezza!
            if ($counter <= $maxItems)
            {
                $id = ArrayHelper::getInt($item, 'id');
                $name = ArrayHelper::getStr($item, 'name');
                if (($id === 0) && ($name !== ''))
                {
                    $tempMedia->deleteObject($tempMedia->getObject($name));
                }
            }
        }
    }

    public function save(int $idObject, array $data, string $key, string $newName, bool $clearTemp, string &$error): bool
    {
        $res = true;
        // Il valore è una stringa JSON da convertire
        $value = ArrayHelper::fromJSON(ArrayHelper::getStr($data, $key));
        $items = ArrayHelper::getArray($value, 'items');
        if (!empty($items))
        {
            if (!$this->checkItemsToken($items, $error))
            {
                return false;
            }

            $oldList = $this->media->getList($idObject);
            $oldIDList = array_column($oldList, 'id');
            $newIDList = array_column($items, 'id');
            // Elimino gli ID che non esistono più 
            foreach ($oldIDList as $id)
            {
                if (!in_array($id, $newIDList))
                {
                    $this->media->deleteObject($idObject, $id);
                }
            }
            $counter = 0;
            $maxItems = $this->media->getMaxItems();
            //$coverID = 0;
            foreach ($items as $item)
            {
                $counter++;
                // Blocco di sicurezza!
                if ($counter <= $maxItems)
                {
                    $id = ArrayHelper::getInt($item, 'id');
                    $name = ArrayHelper::getStr($item, 'name');
                    //$cover = ArrayHelper::getInt($item, 'cover');
                    //$pos = ArrayHelper::getInt($item, 'pos');
                    if (($id === 0) && ($name !== ''))
                    {

                        $tObj = $this->getMedia()->getTemp()->getObject($name);
                        if (!is_null($tObj))
                        {
                            if ($newName !== '')
                            {
                                $name = StringHelper::sanitizeFileName(pathinfo($newName, PATHINFO_FILENAME), true);
                                $name .= '.' . pathinfo($name, PATHINFO_EXTENSION);
                            }
                            $obj = $this->media->putFile($tObj->path, $idObject, $name);
                            $id = $obj->id;
                        }
                    }
                }
            }
            if ($clearTemp)
            {
                $this->clearTemp($items);
            }
        }
        return $res;
    }

    public function load(int $idObject, array &$data, string $key)
    {
        $list = $this->media->getList($idObject);
        $resItems = [];
        foreach ($list as $item)
        {
            $r = [];
            $r['id'] = ArrayHelper::getInt($item, 'id');
            $r['name'] = ArrayHelper::getStr($item, 'path');
            $r['url'] = ArrayHelper::getStr($item, 'url');
            $r['source'] = ArrayHelper::getStr($item, 'path');
            $r['token'] = $this->getToken($r['id'], $r['name']);
            $resItems[] = $r;
        }
        $res = [];
        $res['items'] = $resItems;
        $data[$key] = ArrayHelper::toJSON($res);
    }

    public function getMedia(): \EOS\System\Media\Attachment
    {
        return $this->media;
    }

    public function upload(\EOS\System\Controllers\Controller $controller, $request, $response)
    {
        $fv = new \EOS\System\Util\FormValidator($controller, $request, $response, []);
        $fv->requestUrlField = true;
        $fv->validateRequestXhr = true; // Controllo che le chiamate siano di JQUERY o XMLHttpRequest
        if (MessageHelper::isContentTypeJson($request))
        {
            $valid = $fv->prepareInputDataFromJson();
        } else
        {
            $valid = $fv->prepareInputDataFromMultiPart();
        }
        if ($valid)
        {
            $rqData = $fv->getInputData();
            switch (\EOS\System\Util\ArrayHelper::getStr($rqData, 'command'))
            {
                case 'upload-attachment' :
                    $files = $request->getUploadedFiles();
                    if (empty($files['attachment']))
                    {
                        $fv->setMessage($this->lang->transEncode('system.media.attachment.error.filenotfound'));
                    } else
                    {
                        $newFile = $files['attachment'];
                        $error = '';
                        $resFile = '';
                        if ($this->isValidUpload($newFile, $error))
                        {
                            $ext = $this->media->getExtFromMime($newFile->getClientMediaType());
                            $mo = $this->media->getTemp()->newTempObject($ext);
                            $newFile->moveTo($mo->path);
                            $fv->setOutputValue('id', 0);
                            $fv->setOutputValue('name', $mo->name);
                            $fv->setOutputValue('url', $mo->url);
                            $source = StringHelper::sanitizeFileName(pathinfo($newFile->getClientFileName(), PATHINFO_FILENAME), true);
                            $fv->setOutputValue('source', $source . '.' . $ext);
                            //$fv->setOutputValue('attachment', $mo);
                            $fv->setOutputValue('token', $this->getToken(0, $mo->name));
                            $fv->setResult(true);
                        } else
                        {
                            $fv->setMessage($error);
                        }
                    }
                    break;
                case 'delete-temp-attachment':
                    $items = ArrayHelper::getArray($rqData, 'items');
                    $error = '';
                    if ($this->checkItemsToken($items, $error))
                    {
                        $this->clearTemp($items);
                        $fv->setResult(true);
                    } else
                    {
                        $fv->setMessage($error);
                    }
                    break;
                default :
                    $fv->setMessage($this->lang->transEncode('system.media.attachment.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

}
