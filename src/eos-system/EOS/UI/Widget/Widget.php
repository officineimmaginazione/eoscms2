<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\UI\Widget;

use EOS\System\Routing\PathHelper;

abstract class Widget
{

    public $view;
    public $name;
    protected $params = [];

    public function __construct(\EOS\System\Views\Html $view, $name = '')
    {
        $this->view = $view;
        $this->name = $name;
        $this->prepare();
    }

    protected function prepare()
    {
        
    }

    protected function renderTemplate($widgetTemplate)
    {
        $list = explode('\\', (new \ReflectionObject($this))->getName());
        $compName = $list[count($list) - 3];
        $fileTpl = $compName . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $widgetTemplate) . '.php';
        $tmplFilename = $this->view->path->getThemeOverridePath() . $fileTpl;
        if (file_exists($tmplFilename))
        {
            \EOS\System\Util\IncludeCode::sandboxInclude($tmplFilename, $this);
        } else
        {
            $currParams = $this->view->controller->container['currentParams'];
            $tmplFilename = PathHelper::resolveFileName($currParams['componentsPath'], $currParams['componentsOverridePath'], $fileTpl);
            if (file_exists($tmplFilename))
            {
               \EOS\System\Util\IncludeCode::sandboxInclude($tmplFilename, $this);
            } else
            {
                $errpath = $widgetTemplate;
                if (_EOS_DEBUG_ == true)
                {
                    $errpath = $tmplFilename;
                }

                throw new \Exception('Cannot load widget template "' . $errpath . '"');
            }
        }
    }

    public function write()
    {
        
    }

}
