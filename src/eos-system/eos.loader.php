<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */
define('_EOS_PATH_SYSTEM_LIBS_', __DIR__ . DIRECTORY_SEPARATOR . 'libs' . DIRECTORY_SEPARATOR);
define('_EOS_PATH_SYSTEM_VENDOR_', __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR);
require_once _EOS_PATH_SYSTEM_VENDOR_ . 'autoload.php';

define('_EOS_VERSION_', '1.2.2 - 13/08/2018 ');

require_once _EOS_PATH_SYSTEM_ . 'EOS' . DIRECTORY_SEPARATOR . 'System' . DIRECTORY_SEPARATOR . 'ClassLoader.php';
$loader = new \EOS\System\ClassLoader();
$loader->mapper = [
    'System' => _EOS_PATH_SYSTEM_,
    'Common' => defined('_EOS_PATH_COMMON_') ? _EOS_PATH_COMMON_ : _EOS_PATH_SYSTEM_,
    'UI' => _EOS_PATH_SYSTEM_,
    'Components' => _EOS_PATH_APP_];
if (defined('_EOS_PATH_APP_OVERRIDE_'))
{
    $loader->mapper_override['Components'] = _EOS_PATH_APP_OVERRIDE_;
}

if (defined('_EOS_PATH_COMMON_OVERRIDE_'))
{
    $loader->mapper_override['Common'] = _EOS_PATH_COMMON_OVERRIDE_;
}

$loader->libs = [
    'FluentPDO' => _EOS_PATH_SYSTEM_LIBS_ . 'FluentPDO' . DIRECTORY_SEPARATOR . 'FluentPDO.php',
    'FluentLiteral' => _EOS_PATH_SYSTEM_LIBS_ . 'FluentPDO' . DIRECTORY_SEPARATOR . 'FluentLiteral.php',
    'PHPMailer' => _EOS_PATH_SYSTEM_LIBS_ . 'Phpmailer' . DIRECTORY_SEPARATOR . 'eos.phpmailer.loader.php',
    'IgfsCgInit' => _EOS_PATH_SYSTEM_LIBS_ . 'Payments' . DIRECTORY_SEPARATOR . 'igfsgateway' . DIRECTORY_SEPARATOR . 'eos.igfs.loader.php',
    'IgfsCgVerify' => _EOS_PATH_SYSTEM_LIBS_ . 'Payments' . DIRECTORY_SEPARATOR . 'igfsgateway' . DIRECTORY_SEPARATOR . 'eos.igfs.loader.php',
    'FPDF' => _EOS_PATH_SYSTEM_LIBS_ . 'FPDF' . DIRECTORY_SEPARATOR . 'fpdf.php',
    'Mailin' => _EOS_PATH_SYSTEM_LIBS_ . 'Sendinblue' . DIRECTORY_SEPARATOR . 'eos.sendinblue.loader.php'];
$loader->libsMapper = [
    'Leafo' => _EOS_PATH_SYSTEM_LIBS_,
    'Curl' => _EOS_PATH_SYSTEM_LIBS_,
    'Box' => _EOS_PATH_SYSTEM_LIBS_
];
spl_autoload_register([$loader, 'loadEvent']);
