<?php
$root = __DIR__ .DIRECTORY_SEPARATOR.'5.2.24'.DIRECTORY_SEPARATOR;
$classes = ['phpmailer', 'smtp', 'phpmaileroauth', 'phpmaileroauthgoogle'];
foreach ($classes as $c)
{
  require_once $root.'class.'.$c.'.php';
}