<?php

return
    [
        'system.common.invalid.token' => 'Invalid security token, reload the page or change browser!',
        'system.common.invalid.json' => 'Invalid JSON!',
        'system.common.invalid.multipart' => 'Invalid MultiPart!',
        'system.common.invalid.origin' => 'Invalid request origin, reload page or change browser!',
        'system.common.prev' => 'Previous',
        'system.common.next' => 'Next',
        'system.common.home' => 'Home',
        'system.common.readmore' => 'Read more'
];
