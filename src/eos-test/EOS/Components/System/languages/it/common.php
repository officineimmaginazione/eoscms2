<?php

return
    [
        'system.common.invalid.token' => 'Token di protezione non valido, ricarica la pagina o cambia browser!',
        'system.common.invalid.json' => 'JSON non valido!',
        'system.common.invalid.multipart' => 'MultiPart non valido!',
        'system.common.invalid.origin' => 'Origine della richiesta non valida, ricarica la pagina o cambia browser!',
        'system.common.prev' => 'Precedente',
        'system.common.next' => 'Successivo',
        'system.common.home' => 'Home',
        'system.common.readmore' => 'Leggi tutto'
];
