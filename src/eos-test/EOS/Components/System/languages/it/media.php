<?php

return
    [
        'system.media.image.error.uploaderror' => 'Errore imprevisto durante l\'upload!',
        'system.media.image.error.mimetype' => 'Formato immagine non supportato!',
        'system.media.image.error.size' => 'Ridurre le dimensioni del file!',
        'system.media.image.error.filenotfound' => 'Errore imprevisto non trovo il file scaricato!'
];
