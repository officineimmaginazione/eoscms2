<?php

namespace EOS\Components\Test\Controllers;

use EOS\System\Util\DebugHelper;

class AnyController extends \EOS\System\Controllers\Controller
{
  public function index($request, $response, $arg)
  {
    $name = $request->getAttribute('name')."\n<br>";
    return $response->write( \EOS\System\Util\StringHelper::fastHtml($name));
  }
}