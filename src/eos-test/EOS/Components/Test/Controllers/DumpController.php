<?php

namespace EOS\Components\Test\Controllers;

use EOS\System\Util\DebugHelper;

class DumpController extends \EOS\System\Controllers\Controller
{

  public function index($request, $response, $args)
  {
    $v = $this->newView($request, $response);
    return $v->render('dump/default', true);
  }

  public function dump($request, $response)
  {
    switch ($request->getAttribute('name'))
    {
      case 'container':
        $res = DebugHelper::exportVar($this->container);
        break;
      case 'currentparams':
        $res = DebugHelper::exportVar($this->container->currentParams);
        break;
      default:
        $res = 'Not found dump';
        break;
    }
    return $response->write($res);
  }

}
