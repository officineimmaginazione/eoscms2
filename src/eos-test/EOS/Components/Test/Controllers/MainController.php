<?php

namespace EOS\Components\Test\Controllers;

use EOS\System\Util\DebugHelper;

class MainController extends \EOS\System\Controllers\Controller
{

  public function index($request, $response, $args)
  {
    $v = $this->newView($request, $response);
    return $v->render('main/default', true);
  }
  
  public function index3($request, $response)
  {
  //  $request = $this->getRequest();
    $name = $request->getAttribute('name');
    //$response = $this->getResponse();
    $response->getBody()->write("Attribute: $name<br>");

    $v = $this->newView($request);
    return $v->render($response, 'test1/default');
    
    //return $response;
  }


}
