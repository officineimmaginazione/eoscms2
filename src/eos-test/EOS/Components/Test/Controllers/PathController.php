<?php

namespace EOS\Components\Test\Controllers;

class PathController extends \EOS\System\Controllers\Controller
{
  public function index($request, $response, $args)
  {
    $v = $this->newView($request, $response);
    return $v->render('path/default', true);
  }
  
}