<?php

namespace EOS\Components\Test\Controllers;

class StringHelperController extends \EOS\System\Controllers\Controller
{
  public function index($request, $response, $args)
  {
    $v = $this->newView($request, $response);
    return $v->render('stringhelper/default', true);
  }
  
}