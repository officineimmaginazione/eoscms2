<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Test;

class Router extends \EOS\System\Routing\Router
{
  protected function process()
  {
    if (count($this->params) > 0)
    {
      if ($this->params[0] == 'custom')
      {
        $this->rawparams[] = 'super';
      }
    }
    parent::process();
  }
}