<?php
$this->mapComponent(['GET'], 'Test', ['/' => 'MainController:index']);

$this->mapComponent(['GET'], 'Test', ['/dump/' => 'DumpController:index',
    '/dump/{name}/' => 'DumpController:dump']);

$this->mapComponent(['GET'], 'Test', ['/path/' => 'PathController:index']);


$this->mapComponent(['GET'], 'Test', ['/any/'=> 'AnyController:index',
      '/any/%' => 'AnyController:index']);

$this->mapComponent(['GET'], 'Test', ['stringhelper' => 'StringHelperController:index']);