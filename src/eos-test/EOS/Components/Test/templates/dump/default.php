<?php
$tests = 
[ 'container' => 'container',
   'currentparams' => 'currentparams',
]
?>
<ul>
  <?php foreach ($tests as $k => $v) { 
    echo '<li><a href="'. $this->path->urlFor('test', ['dump', $v]).'">'.  $this->encodeHtml($v).'</a></li>';
  } ?>
</ul>