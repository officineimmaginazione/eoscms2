<?php
$tests = [['Test', 'path', 'Test - Path'],
  ['Test', 'dump', 'Dump'],
  ['Test', 'stringhelper', 'String Helper'],
  ['uitest', 'main/index', 'UI Test'],
  ['uitest', 'main/cdn', 'UI Test - con CDN'],
  ['uitest', 'datatable/index', 'UI Data table'],
  ['uitest', 'media/index', 'Media']];
?>
<ul>
  <?php foreach ($tests as $k) { 
    echo '<li><a href="'. $this->path->urlFor($k[0], \EOS\System\Routing\PathHelper::explodeSlash($k[1])).'">'.  $this->encodeHtml($k[2]).'</a></li>';
  } ?>
</ul>