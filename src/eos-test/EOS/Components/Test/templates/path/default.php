<?php

$p = $this->path;

$pr = [];
$pr[] = 'getPageUrl = ' . $p->getPageUrl();
$pr[] = 'getSelfUrl = ' . $p->getSelfUrl();
$pr[] = 'getAppUrl = ' . $p->getAppUrl();
$pr[] = 'getSiteUrl = ' . $p->getSiteUrl();
$pr[] = 'getDomainUrl = ' . $p->getDomainUrl();
$pr[] = 'getBaseUrl = ' . $p->getBaseUrl();
$pr[] = 'getLibraryUrl = ' . $p->getLibraryUrl();
$pr[] = 'getThemeUrl = ' . $p->getThemeUrl();
$pr[] = 'getUploadsUrl = ' . $p->getUploadsUrl();
$pr[] = 'getUploadsPath = ' . $p->getUploadsPath();
$pr[] = 'getThemePath = ' . $p->getThemePath();
$pr[] = 'getThemeTemplateFile = ' . $p->getThemeTemplateFile();
$pr[] = '';
$pr[] = 'Sanitize:';
$pr[] = 'Prova di: € | 日本国 | città | pø | _- | / | ,=>' . EOS\System\Util\StringHelper::sanitizeUrl('Prova di: € | 日本国 | città | pø | _-');
$pr[] = '';
for ($i = 0; $i <= 3; $i++)
{
  $lang = '';
  switch ($i)
  {
    case 1:
      $pr[] = 'Hack force root domain';
      $pr[] = 'PHP_SELF = ' . $this->controller->container->get('environment')['PHP_SELF'];
      $this->controller->container->get('environment')['PHP_SELF'] = '/index.php';
      $pr[] = 'Hack: ' . $this->controller->container->get('environment')['PHP_SELF'];
      break;
    case 2:
      $pr[] = 'Hack page extension';
      $this->controller->container->get('currentParams')['pageExtension'] = '.html';
      break;
    case 3:
      $pr[] = 'Add lang "it"';
      $lang = 'en';
      break;
    default:
      $pr[] = 'Default';
      break;
  }

  $pr[] = ' = ' . $p->urlFor('UITest', ['', ''], [], $lang).'  (blank params)';
  $pr[] = ' = ' . $p->urlFor('', [], [], $lang);
  $pr[] = '/test/ = ' . $p->urlFor('test', [], [], $lang);
  $pr[] = '/test/sub/15 = ' . $p->urlFor('test', ['sub', 15], [], $lang);
  $pr[] = '/test/sub/€漢字/>>>>&&/ = ' . $p->urlFor('test', ['sub', '漢字'], [], $lang);
  $pr[] = '/test/ = ' . $p->urlFor('test', ['path'], [], $lang);
  $pr[] = '/test/path with space = ' . $p->urlFor('test', ['path with space'], [], $lang);
  $pr[] = '/test/ = ' . $p->urlFor('test', ['path'], ['testo libero'], $lang);
  $pr[] = '/test/ = ' . $p->urlFor('test', ['custom'], [], $lang);
  $pr[] = '';
}
$pr[] = 'SessionID : '.$this->session->getSessionID();
$pr[] = ini_get('session.gc-maxlifetime');
$pr[] = '';
foreach ($pr as $v)
{
  echo '<p>' . $this->encodeHtml($v) . '</p>'."\n";
}
