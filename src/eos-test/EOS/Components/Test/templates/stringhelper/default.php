<?php
echo $this->encodeHtml('<script type="text/javascript">alert("XSS")</script> == ') . "<br>\n";
echo \EOS\System\Util\StringHelper::sanitizeField('<script type="text/javascript">alert("XSS")</script>') . "<br>\n";
echo "------\n";
$text = '<p>Testo interno al paragrafo.</p><!-- commento --> <a href="#ancora">Altro<br>testo</a>';
echo $this->encodeHtml('sanitizeField: ' . $text . ' == ') . "<br>\n";
echo \EOS\System\Util\StringHelper::sanitizeField($text) . "<br>\n";
echo "------\n";
echo "SanitizeField:<br>\n";
$str = " <h1>Hello WorldÆØÅ!\n Capo\tTab</h1>";
echo $this->encodeHtml($str . '==') . \EOS\System\Util\StringHelper::sanitizeField($str) . "<br>\n";
echo $this->encodeHtml($str . '(trim = false)== ') . \EOS\System\Util\StringHelper::sanitizeField($str, false) . "<br>\n";
echo $this->encodeHtml($str . '(trim = false, stripLow = false)== ') . \EOS\System\Util\StringHelper::sanitizeField($str, false, false) . "<br>\n";
echo "------\n";
echo "SanitizeFieldMultiline:<br>\n";
echo \EOS\System\Util\StringHelper::sanitizeFieldMultiline($str) . "<br>\n";
echo 'stripLow=false:' . "\n";
echo \EOS\System\Util\StringHelper::sanitizeFieldMultiline($str, false) . "<br>\n";
$par = new EOS\System\Util\TagParser();
$par->value = '{v0} prova di una {v1} e {v2}';
$par->callBack = function ($command, &$result)
{
    switch ($command)
    {
        case 'v0':
            $result = 'Funziona';
            break;

        default:
            break;
    }
};
$par->parse();
echo 'stringparse:' . $par->value . "<br>\n";
echo 'stringparse-result:' . $par->result . "<br>\n";

$cdn = new EOS\System\Routing\CDN(null, null, null);
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="http://www.pippo.it/eoscms/src/" >
        <meta name="generator" content="Eos" >
        <title>Titolo</title>
        <link rel="stylesheet" type="text/css" href="/eoscms/src/library/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/eoscms/src/library/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="container">
            <p><strong><img alt="" height="233" src="data-customer/uploads/test.png" width="1280"></strong></p>
        </div>
        <script src="/eoscms/src/library/jquery/2.2.0/jquery.min.js"></script>
        <script src="/eoscms/src/library/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script>
            $('#form-login').submit(function (e)
            {
                //e.preventDefault();
                var data = $(this).serializeFormJSON();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/eoscms/src/eos-admin/it/system/access/signin/',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                    .done(function (json)
                    {
          
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
         
                    });
                return false;
            });
        </script>
    </body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
$res = $cdn->parseCdnString($html, '/eoscms/src/', 'http://www.cdn.it');
echo 'parseCdnString - Orig: ' . $this->encodeHtml($html) . "<br>\n";
echo 'parseCdnString - Res: ' . $this->encodeHtml($res) . "<br>\n";
