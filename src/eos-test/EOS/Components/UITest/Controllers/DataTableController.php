<?php

namespace EOS\Components\UITest\Controllers;

class DataTableController extends \EOS\System\Controllers\Controller
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        return $v->render('datatable/default', true);
    }

    public function ajaxData($request, $response, $args)
    {
        $model = new \EOS\Components\UITest\Models\DataTableModel($this->container);
        $q = $model->getQuery();

        $dt = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $q, $this->container->get('database'));
        $dt->addColumn('id', 'a.id');
        $dt->addColumn('name', 'a.name');
        $dt->addColumn('iso', 'a.iso_code');
        return $response->withJson($dt->toArray());
    }

    public function ajaxDataPrenotazioni($request, $response, $args)
    {
        $model = new \EOS\Components\UITest\Models\DataTableModel($this->container);
        $q = $model->getQueryPrenotazioni();

        $dt = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $q, $this->container->get('database'));
        $dt->addColumn('id', 'a.id');
        $dt->addColumn('datacompilazione_prenotazione', 'a.datacompilazione_prenotazione');
        $dt->addColumn('nome_prenotazione', 'a.nome_prenotazione', true);
        $dt->addColumn('richieste_prenotazioni', 'a.richieste_prenotazioni');
        $lang = $this->lang;
        $dt->onColumnRender('datacompilazione_prenotazione', function ($value, $row) use ($lang)
        {
            return $lang->dbDateToLangDate($value).' ['.$row['id'].']';
        });
        return $response->withJson($dt->toArray());
        //return \EOS\System\Util\MessageHelper::setBodyJson($response, $dt->toArray());
    }

}
