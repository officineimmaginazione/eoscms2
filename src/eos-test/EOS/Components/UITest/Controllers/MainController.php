<?php

namespace EOS\Components\UITest\Controllers;

use EOS\System\Util\DebugHelper;

class MainController extends \EOS\System\Controllers\Controller
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        return $v->render('main/default', true);
    }

    public function cdn($request, $response, $args)
    {
        $cdnOld = $this->container->get('database')->setting->getStr('system', 'cdn');
        try
        {
            $this->container->get('database')->setting->setStr('system', 'cdn', $this->path->getBaseUrl());
            $v = $this->newView($request, $response);
            $newResponse = $v->render('main/default', true);
            $cdn = new \EOS\System\Routing\CDN($this->container, $request, $newResponse);
            $cdn->render();
        } finally
        {
            if ($cdnOld !== '')
            {
                $this->container->get('database')->setting->setStr('system', 'cdn', $cdnOld);
            }
        }
        return $newResponse;
    }

    public function datatable($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        return $v->render('main/datatable', true);
    }

    public function ajaxButtonUpload($request, $response)
    {
        $btnServer = new \EOS\UI\Bootstrap\ButtonUploadServer($this->container);
        $btnServer->setExtensionsList(['jpg', 'xlsx']);
        $method = function ($form, $file, &$error)
        {
            $form->setMessage($file->getClientFileName() . ' => ' . $file->file);
        };

        return $btnServer->upload($this, $request, $response, $method);
    }

}
