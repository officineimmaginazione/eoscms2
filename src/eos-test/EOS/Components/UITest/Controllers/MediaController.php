<?php

namespace EOS\Components\UITest\Controllers;

use EOS\System\Util\DebugHelper;
use EOS\System\Util\FormValidator;

class MediaController extends \EOS\System\Controllers\Controller
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\UITest\Models\MediaModel($this->container);
        $v->mediaImage = $m->newMediaServer()->getMedia();
        $v->data = $m->getData();
        return $v->render('media/default');
    }

    public function ajaxImageCommand($request, $response, $args)
    {
        $m = new \EOS\Components\UITest\Models\MediaModel($this->container);
        $mediaServer = $m->newMediaServer();
        return $mediaServer->upload($this, $request, $response);
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        // $fv->sanitizeSkipFields = ['media-image-test'];

        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\UITest\Models\MediaModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('uitest', 'media/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

}
