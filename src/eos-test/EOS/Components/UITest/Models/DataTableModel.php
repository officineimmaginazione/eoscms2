<?php

namespace EOS\Components\UITest\Models;

class DataTableModel extends \EOS\System\Models\Model
{

    public function getQuery()
    {
        return $this->db->newFluent()->from($this->db->tableFix('#__lang as a'));
    }
    
    public function getQueryPrenotazioni()
    {
        return $this->db->newFluent()->from($this->db->tableFix('#__prenotazioni as a'))->orderBy('id');
    }

}
