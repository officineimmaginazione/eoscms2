<?php

namespace EOS\Components\UITest\Models;

class MediaModel extends \EOS\System\Models\Model
{

    public function newMediaServer(): \EOS\UI\System\Media\ImageServer
    {
        $mediaImage = new \EOS\System\Media\Image($this->container, 'test');
        // Test forzare parametri
        // $mediaImage->setNameCase(\EOS\System\Media\Image::NAME_CASE_UPPER);
        // $mediaImage->setNameFormat(\EOS\System\Media\Image::NAME_FORMAT_WITH_ID);
        // $mediaImage->setNameSanitize(\EOS\System\Media\Image::NAME_SANITIZE_URL);
        // Forzo più items
        $mediaImage->setMaxItems(4);
        return new \EOS\UI\System\Media\ImageServer($this->container, $mediaImage);
    }

    public function saveData(array $data, string &$error): bool
    {
        $error = '';
        $ms = $this->newMediaServer();
        $res = $ms->save(18, $data, 'media-image-test', 'Codice di esempio', true, $error);
        return $res;
    }

    public function getData()
    {
        $res = [];
        $ms = $this->newMediaServer();
        $ms->load(18, $res, 'media-image-test');

        return $res;
    }

}
