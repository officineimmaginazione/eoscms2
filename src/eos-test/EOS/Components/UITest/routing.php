<?php

$this->mapComponent(['GET'], 'UITest', ['/main/index' => 'MainController:index']);
$this->mapComponent(['GET'], 'UITest', ['/main/cdn' => 'MainController:cdn']);
$this->mapComponent(['POST'], 'UITest', ['/main/ajaxbuttonupload' => 'MainController:ajaxButtonUpload']);
$this->mapComponent(['GET'], 'UITest', ['/datatable/index' => 'DataTableController:index']);
$this->mapComponent(['POST'], 'UITest', ['/datatable/ajaxdata' => 'DataTableController:ajaxData']);
$this->mapComponent(['POST'], 'UITest', ['/datatable/ajaxdataprenotazioni' => 'DataTableController:ajaxDataPrenotazioni']);

$this->mapComponent(['GET'], 'UITest', ['media/index' => 'MediaController:index']);
$this->mapComponent(['POST'], 'UITest', ['media/ajaximagecommand' => 'MediaController:ajaxImageCommand']);
$this->mapComponent(['POST'], 'UITest', ['media/ajaxsave' => 'MediaController:ajaxSave']);
