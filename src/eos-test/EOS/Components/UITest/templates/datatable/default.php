<?php

/*
  $bx = new \EOS\UI\Bootstrap\Box();
  $bx->title('Lingue');
  $bx->startContent();
  $dt = new \EOS\UI\Bootstrap\DataTable('data-table',
  $this->path->urlFor('uitest', ['datatable', 'ajaxdata']),
  $this->session->getTokenName(), $this->session->getTokenValue());
  $dt->addColumn('id', 'ID');
  $dt->addColumn('name', 'Nome');
  $dt->addColumn('iso', 'ISO');

  $dt->printRender($this);
  $bx->endContent();
  $bx->printRender($this);
 */

$bx = new \EOS\UI\Bootstrap\Box();
$bx->title('Prenotazioni');
$bx->startContent();
(new \EOS\UI\Html\Label())->id('select')->content('')->printRender($this);
$dt = new \EOS\UI\Bootstrap\DataTable('data-table-pr', $this->path->urlFor('uitest', ['datatable', 'ajaxdataprenotazioni']), $this->session->getTokenName(), $this->session->getTokenValue());
$dt->attr('class', 'table-bordered table-striped');
$dt->addColumn('id', 'ID', false, false, false);
$dt->addColumnCustom('Prova', 'checkbox-ln', '<input type="checkbox">');
$dt->addColumn('nome_prenotazione', 'Nome €€');
$dt->addColumn('datacompilazione_prenotazione', 'Data', false, false, true, 'data-class');
$dt->addColumn('richieste_prenotazioni', 'Richieste', false);
$dt->addColumnCustom('Prova', 'prova', '<button class="btn">PROVA</button>');
$dt->clickRow('function (e, r) {clickCheck(r); e.stopPropagation();}', 'checkbox-ln');
$dt->clickRow('function (e, r) {alert(r.data().id+"_prova"); e.stopPropagation();}', 'prova');
$dt->clickRow('function (e, r) {$("#select").html("Click ID:" + r.data().id);}');
$dt->onColumnRender('nome_prenotazione', 'function ( data, type, full, meta ) {return "<strong>"+data+"</strong><br>"+full.id;}');
$dt->printRender($this);
$bx->endContent();
$bx->printRender($this);

$bx = new \EOS\UI\Bootstrap\Box();
$bx->title('No ajax');
$bx->startContent();
//$dt = new \EOS\UI\Bootstrap\DataTable('data-table-pr',
//    $this->path->urlFor('uitest', ['datatable', 'ajaxdataprenotazioni']),
//    $this->session->getTokenName(), $this->session->getTokenValue());
$dt = new \EOS\UI\Bootstrap\DataTable('data-table-pr2');
$dt->addRawParams('ordering', 'false');
$dt->addRawParams('paging', 'false');
$dt->addRawParams('info', 'false');
$dt->addRawParams('bFilter', 'false');
$dt->attr('class', 'table-bordered table-striped');
$dt->addColumn('id', 'ID', false, false, false);
$dt->addColumn('nome_prenotazione', 'Nome');
//$dt->addColumn('datacompilazione_prenotazione', 'Data');
//$dt->addColumn('richieste_prenotazioni', 'Richieste', false);
$list[] = ['id' => 1, 'nome_prenotazione' => 'Emanuele'];
$list[] = ['id' => 2, 'nome_prenotazione' => '<span class="label label-success">Ciao</span>'];
$dt->bindList($list);

$dt->printRender($this);
$bx->endContent();
$bx->printRender($this);


$this->startCaptureScript();
?>
<script>
    function clickCheck(row)
    {
        var list = [];
        $('#data-table-pr input[type=checkbox]').each(function ()
        {
            if ($(this).is(":checked"))
            {
                var tr = $(this).closest("tr");
                var row = $("#data-table-pr").DataTable().row(tr);
                list.push(row.data().id);
            }
        });
        $("#select").html("Select ID:" + list);
    }
</script>
<?php

$this->endCaptureScript();




