<section class="content">
    <div class="row">
        <div class="col-md-6">
          <?php
          echo '<h1>' . $this->transE('uitest.main.title') . '</h1>' . "\n";
          echo '<h2>' . $this->transE('uitest.main.subtitle') . '</h2>' . "\n";
          $div = new \EOS\UI\Html\Div('div-html');
          $div->startContent();
          (new EOS\UI\Html\Label())->contentEncode('Label: Test')->printRender();
          (new EOS\UI\Html\Block('h2'))->contentEncode('Block: h2')->printRender();
          echo "Test raw";
          (new EOS\UI\Html\Textarea())->contentEncode('PROVA')->printRender();
          (new \EOS\UI\Html\Button())->content('html\button')->click('function (e) {bootbox.alert("ok-no-global-ready");}')->printRender($this);
          (new \EOS\UI\Html\Select())
              ->addOptionEncode('1', 'Prova 1')
              ->addOptionEncode('2', 'Prova2')->printRender();
          $div->endContent()->printRender();

          echo '<br><br>';
          //
          $btnUpload = new EOS\UI\Bootstrap\ButtonUpload('btn-upload', $this->path->urlFor('uitest', ['main', 'ajaxbuttonupload']),
              $this->session->getTokenName(), $this->session->getTokenValue());
          $btnUpload->contentEncode('Upload test');
          $btnUpload->accept('image/*;application/*');
          $btnUpload->onSendSuccess('function (data)
          {
              bootbox.alert(data.message);
          }');
          $btnUpload->printRender($this);
          //
          ?>
        </div>
        <div class="col-md-6">
            <?php
            $box = (new \EOS\UI\Bootstrap\Box())
                    ->title('Prova')
                    ->attr('class', 'box-primary')->startContent();
            $box->startHeader();
            $box->endHeader();
            $form = (new EOS\UI\Html\Form())->startContent();

            (new \EOS\UI\Bootstrap\Button('btn-test'))
                ->contentEncode('Funziona!!!!')
                ->attr('style', 'width:50px')
                ->attr('style', 'height:50px')
                ->attr('class', 'btn-default')
                ->disabled(false)
                ->disabled(true)->printRender();
            (new \EOS\UI\Bootstrap\FormGroup())->addContent(function ()
            {
                (new \EOS\UI\Html\Label())->content('LabelGruppo')->printRender();
                (new \EOS\UI\Bootstrap\Input('valore'))->placeholder('inserisci un valore')->printRender();
            }, $this)->printRender();
            (new \EOS\UI\Html\Link())->href('http://www.google.it')->content('google')->printRender();
            (new \EOS\UI\Bootstrap\Input('test-input'))->value('pippo')->required(true)->printRender();
            (new \EOS\UI\Bootstrap\Textarea('test-area'))->contentEncode('pippo')->printRender();
            (new \EOS\UI\Bootstrap\Select('select-prova'))
                ->addOptionEncode('1', 'Prova 1')
                ->addOptionEncode('2', 'Prova2')->printRender();
            $form->endContent()->printRender();
            $box->endContent();
            $box->startFooter();
            (new \EOS\UI\Bootstrap\Button('test2'))->content('bootstrap\button')->click('function (e) {
            bootbox.confirm("Are you sure?", function(result) {
            });}')->printRender($this);
            (new \EOS\UI\Bootstrap\Checkbox('check-test'))->printRender($this);
            $box->endFooter();
            $box->printRender();


            $box = (new \EOS\UI\Bootstrap\Box())
                ->title('Editor')
                ->attr('class', 'box-primary');
            $box->addContent(function ()
            {
                (new EOS\UI\Bootstrap\CKEditor('editor1'))->printRender($this);
            }
                , $this);
            $box->printRender($this);
            ?>
        </div>
    </div>
</section>