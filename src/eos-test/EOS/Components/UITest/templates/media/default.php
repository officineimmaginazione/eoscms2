<form id="edit" method="POST" class="form">
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <?php
                $this->util->writeAsyncTokenHtml();
                echo '<h1>' . $this->transE('uitest.media.title') . '</h1>' . "\n";
                echo '<h2>' . $this->transE('uitest.media.subtitle') . '</h2>' . "\n";
                echo '<button class="btn" id="btn-save">Salva</button>';
                $div = new \EOS\UI\Html\Div('div-html');
                $div->startContent();

                $mImage = new \EOS\UI\System\Media\Image(
                    $this->mediaImage, 'media-image-test',
                    $this->path->urlFor('uitest', ['media', 'ajaximagecommand']));
                $mImage->bind($this->data, 'media-image-test');
                $mImage->readOnly(false);
                $mImage->printRender($this);
                $div->endContent()->printRender();

                echo '<br><br>';
                ?>
            </div>

        </div>
    </section>
</form>
<?php
$this->startCaptureScript();
?>
<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('uitest', ['media', 'ajaxsave']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        })
            .done(function (json)
            {
                if (json.result === true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }
    $(function ()
    {
        $('#btn-save').on('click', function (ev)
        {
            ev.preventDefault();
            saveData();
        });
    });
</script>
<?php
$this->endCaptureScript();
