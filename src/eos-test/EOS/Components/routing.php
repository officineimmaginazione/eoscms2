<?php

$this->get('/', function ($request, $response, $args)
{
  $p = $this->get('path');
  return EOS\System\Util\StringHelper::fastHtml(sprintf('<a href="%s">Vai ai test</a>', $p->urlFor('test')));
});
