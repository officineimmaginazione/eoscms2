<?php

define('_EOS_PATH_FOLDER_', '');
define('_EOS_PATH_APP_', __DIR__ . DIRECTORY_SEPARATOR);
require_once '..' . DIRECTORY_SEPARATOR .'eos-config'.DIRECTORY_SEPARATOR . 'eos.defines.php';

if (_EOS_PROFILER_)
{
    $profiler = new \EOS\System\Util\Profiler();
    $profiler->start();
} else
{
    $profiler = null;
}
$configCore = [];//['settings' => ['outputBuffering' => false]];
$app = new \EOS\System\Application(
    ['dbRouter' => false,
    'multiLanguage' => true,
    'pageExtension' => '/',
    'profiler' => $profiler], $configCore);
$app->run();

if (_EOS_PROFILER_)
{
    unset($app);
    $profiler->stop('Global');
    $profiler->render();
    unset($profiler);
}