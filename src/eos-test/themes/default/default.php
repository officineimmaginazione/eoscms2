<?php
//\EOS\UI\Loader\Library::load($this, 'JQuery');
//\EOS\UI\Loader\Library::load($this, 'Bootstrap');
\EOS\UI\Loader\Library::load($this, 'Bootbox');
\EOS\UI\Loader\Library::load($this, 'DataTables');
\EOS\UI\Loader\Library::load($this, 'CKEditor');
\EOS\UI\Loader\Library::load($this, 'iCheck');
\EOS\UI\Loader\Library::load($this, 'Icons');
\EOS\UI\Loader\Library::load($this, 'MediaImage');
//$this->addScriptLink($this->path->extraUrlFor('System', ['resource', 'app.js']));
$this->addScriptLink('/it/system/resource/app.js');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        $this->writeHead();
        $this->addStyleSheetLink($this->path->getThemeUrl() . "css/admin.css");
        $this->addStyleSheetLink($this->path->getThemeUrl() . "css/skin-light.css");
        $this->writeLink();
        ?>
        <title>Home</title>
    <body>
      <?php
      $this->writeContent();
      $this->writeScript();
      ?>
    </body>
</html>

