<?php

define('_EOS_PATH_FOLDER_', 'eos-site');
define('_EOS_PATH_APP_', __DIR__ . DIRECTORY_SEPARATOR . _EOS_PATH_FOLDER_ . DIRECTORY_SEPARATOR);
define('_EOS_PATH_APP_OVERRIDE_', __DIR__ . DIRECTORY_SEPARATOR . 'data-customer' . DIRECTORY_SEPARATOR . 'override' . DIRECTORY_SEPARATOR . 'eos-site' . DIRECTORY_SEPARATOR);
require_once __DIR__ . DIRECTORY_SEPARATOR . 'eos-config' . DIRECTORY_SEPARATOR . 'eos.defines.php';

if (_EOS_PROFILER_)
{
    $profiler = new \EOS\System\Util\Profiler();
    $profiler->start();
} else
{
    $profiler = null;
}
$app = new \EOS\System\Application([
    'pageExtension' => '/',
    'profiler' => $profiler,
    'languageType' => 'site',
    'dbRouter' => true,
    'multiLanguage' => true,
    'themesPath' => _EOS_PATH_THEMES_]);

$app->prepare();

unset($app->getContainer()['notFoundHandler']);
$app->getContainer()['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $response = new \Slim\Http\Response(404);
        return $response->withHeader('Content-Type', 'text/html;charset=utf-8')->write('<!DOCTYPE html>
                <html>
                <head>
                    <meta charset="utf-8">
                    <title>Page Not Found TEST</title>
                </head>
                <body>
                   <h1>Page not found 2</h1>
                </body>
            </html>');
    };
};

 

$app->run();

if (_EOS_PROFILER_)
{
    unset($app);
    $profiler->stop();
    $profiler->render();
    unset($profiler);
}