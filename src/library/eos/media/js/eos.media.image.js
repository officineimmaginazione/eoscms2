/*
 * Autore: Alessandro Savoiardo
 * versione: 1.0.0.0 del 05/10/2018 
 */

var EOS = EOS || {};
EOS.Media = EOS.Media || {};
EOS.Media.Image = function (htmlDiv, storeName)
{
    this.htmlObject = document.querySelector(htmlDiv);
    this.htmlObject.classList.add('media-image');
    this.commandUrl = '';
    this.token = {};
    this.items = [];
    this.currentItem = null;
    // Input store
    this.inputStore = document.createElement('input');
    this.inputStore.type = 'hidden';
    this.inputStore.name = storeName;
    this.htmlObject.appendChild(this.inputStore);
    // Input upload
    this.inputUpload = document.createElement('input');
    this.inputUpload.type = 'file';
    this.inputUpload.accept = 'image/*';
    this.inputUpload.style.display = 'none';
    this.htmlObject.appendChild(this.inputUpload);
    var m = this;
    this.inputUpload.addEventListener('change', function (ev)
    {
        m.runUpload();
    }, false);
    // Lista
    this.htmlList = document.createElement('div');
    this.htmlList.className = 'media-image-list';
    this.htmlObject.appendChild(this.htmlList);
    // Messaggi
    this.uploadMaxSize = 4294967295;
    this.msgSizeError = 'Invalid file size!';
    this.msgDeleteItem = 'Would you like delete image?';
    this.maxItems = 1;
    this.useBootbox = typeof bootbox !== "undefined";
    this.autoSaveStore = true;
    this.readOnly = false;
    //
    this.clear();
    this.asyncRemoteCommand = true;
};

EOS.Media.Image.prototype = {
    load: function (value)
    {
        this.fromObject(JSON.parse(value));
    },
    toObject: function ()
    {
        var items = [];
        for (var i = 0; i < this.items.length; i++)
        {
            items.push(this.items[i].toObject());
        }
        return {
            'items': items
        };
    },
    fromObject: function (obj)
    {
        this.draw();
        if (obj.items !== undefined)
        {

            for (var i = 0; i < obj.items.length; i++)
            {
                if (i < this.maxItems)
                {
                    this.items[i].fromObject(obj.items[i]);
                }
            }
        }
    },
    saveToStore: function ()
    {
        this.inputStore.value = JSON.stringify(this.toObject());
    },
    clear: function ()
    {
        this.htmlList.innerHTML = '';
        this.items = [];
    },
    draw: function ()
    {
        if (this.items.length === 0)
        {
            for (var i = 0; i < this.maxItems; i++)
            {
                var img = new EOS.Media.Image.Item(this);
            }
        } else
        {
            this.items.forEach(function (v)
            {
                v.draw();
            });
        }
        if (this.readOnly)
        {
            this.inputUpload.type = 'hidden';
        }
    },
    showAlert: function (message)
    {
        if (this.useBootbox)
            bootbox.alert(message);
        else
            alert(message);
    },
    showConfirm: function (message, event)
    {
        if (this.useBootbox)
        {
            bootbox.confirm(message,
                function (result)
                {
                    if (result)
                    {
                        event();
                    }
                });
        } else
        {
            if (confirm(message))
            {
                event();
            }
        }
    },
    showUpload: function (item)
    {
        this.currentItem = item;
        this.inputUpload.click();
    },
    deleteBeforeOnUnload()
    {
        var m = this;
        window.addEventListener("beforeunload", function (event)
        {
            m.runDeleteList(m.items);
        });
    },
    runUpload: function ()
    {
        if (this.currentItem === null)
        {
            return false;
        }
        if (this.inputUpload.files === undefined)
        {
            return false;
        }
        if ((this.inputUpload.files.length > 0) && (this.inputUpload.files[0].size > this.uploadMaxSize))
        {
            this.showAlert(this.msgSizeError);
            return false;
        }
        if (this.inputUpload.files.length > 0)
        {
            var fData = new FormData();
            for (var prop in this.token)
            {
                fData.append(prop, this.token[prop]);
            }
            fData.append('command', 'upload-image');
            fData.append('request_url', window.location.href);
            fData.append('image', this.inputUpload.files[0]);
            this.inputUpload.value = ''; // Elimino il file selezionato
            var m = this;
            var resultItem = this.currentItem; // Così fa upload contemporanei
            this.runRemoteCommand(this.commandUrl, fData, function (data)
            {
                resultItem.load(data.id, data.name, data.source, false, data.images, data.token);
            }, function ()
            {

            },
                function (percentage)
                {
                    console.log(percentage.toString() + '%');
                });
        }
    },
    runDeleteList: function (items)
    {
        var fData = {};
        for (var prop in this.token)
        {
            fData[prop] = this.token[prop];
        }
        fData['command'] = 'delete-temp-image';
        fData['request_url'] = window.location.href;
        var fDataItems = [];
        items.forEach(function (item)
        {
            if (item.id === 0)
            {
                fDataItems.push(item.toObject());
            }
        });
        if (fDataItems.length > 0)
        {
            fData['items'] = fDataItems;
            this.asyncRemoteCommand = false;
            this.runRemoteCommand(this.commandUrl, JSON.stringify(fData),
                function ()
                {

                });
        }
    },
    runRemoteCommand: function (commandUrl, fData, resultEvent, afterEvent, onProgress)
    {
        if (this.readOnly)
            return;

        var m = this;
        var errorProc = function (xhr)
        {
            var err = xhr.responseText;
            m.showAlert(err);
            if (afterEvent !== undefined)
            {
                afterEvent();
            }
        };
        var xhr = new XMLHttpRequest();
        xhr.open('POST', commandUrl, this.asyncRemoteCommand);
        if (typeof fData === 'string')
        {
            xhr.setRequestHeader('Content-type', 'application/json');
        }
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        xhr.onreadystatechange = function ()
        {
            if (xhr.readyState === XMLHttpRequest.DONE)
            {
                if (xhr.status === 200)
                {
                    var data = JSON.parse(xhr.response);
                    if (data.result === true)
                    {
                        resultEvent(data);
                    } else
                    {
                        if (data.message !== '')
                        {
                            m.showAlert(data.message);
                        } else
                        {
                            m.showAlert('_error_');
                        }
                        if (afterEvent !== undefined)
                        {
                            afterEvent();
                        }
                    }

                } else
                {
                    errorProc(xhr);
                }
            }
        };
        xhr.onerror = function ()
        {
            errorProc(xhr);
        };
        xhr.upload.addEventListener('progress', function (evt)
        {
            if ((evt.lengthComputable) && (onProgress !== undefined))
            {
                var percentage = Math.round((evt.loaded / evt.total) * 100);
                onProgress(percentage);
            }
        }, false);
        xhr.send(fData);
    }

};


EOS.Media.Image.Item = function (mediaObject)
{
    mediaObject.items.push(this);
    this.clear();
    this.pos = mediaObject.items.length;
    this.mediaObject = mediaObject;
    this.htmlObject = document.createElement('div');
    this.htmlObject.className = 'media-image-item';
    mediaObject.htmlList.appendChild(this.htmlObject);
    this.htmlImg = document.createElement('div');
    this.htmlImg.className = 'media-image-item-img';
    this.htmlImg.alt = '';
    this.htmlObject.appendChild(this.htmlImg);
    this.htmlAdd = document.createElement('div');
    this.htmlAdd.className = 'media-image-item-add';
    if (!mediaObject.readOnly)
    {
        this.htmlAdd.innerHTML = '<div><span>+</span></div>';
    }
    this.htmlObject.appendChild(this.htmlAdd);
    var item = this;
    if (!mediaObject.readOnly)
    {
        this.htmlAdd.addEventListener('click', function ()
        {
            mediaObject.showUpload(item);
        }, false);
    }

    this.htmlDel = document.createElement('div');
    this.htmlDel.className = 'media-image-item-del';
    if (!mediaObject.readOnly)
    {
        this.htmlDel.innerHTML = '<span class="fa fa-trash-alt"></span>';
    }
    this.htmlObject.appendChild(this.htmlDel);

    if (!mediaObject.readOnly)
    {
        this.htmlDel.addEventListener('click', function ()
        {

            mediaObject.showConfirm(mediaObject.msgDeleteItem, function ()
            {
                if (item.id === 0)
                {
                    mediaObject.runDeleteList([item]);
                }
                item.clear();
                item.draw();
            });

        }, false);
    }

    this.htmlCover = document.createElement('div');
    this.htmlCover.className = 'media-image-item-cover';
    this.htmlCover.innerHTML = '<span class="far fa-check-circle"></span>';
    this.htmlObject.appendChild(this.htmlCover);
    if (!mediaObject.readOnly)
    {
        this.htmlCover.addEventListener('click', function ()
        {
            item.cover = !item.cover;
            mediaObject.items.forEach(function (v)
            {
                if (v !== item)
                {
                    v.cover = false;
                }
            });

            mediaObject.draw();
        }, false);
    }
    this.draw();
};

EOS.Media.Image.Item.prototype = {

    toObject: function ()
    {
        var images = {};
        for (var prop in this.images)
        {
            images[prop] = {'name': this.images[prop].name};
        }
        return {'id': this.id,
            'name': this.name,
            'source': this.source,
            'cover': this.cover,
            'pos': this.pos,
            'images': images,
            'token': this.token
        };
    },
    fromObject: function (obj)
    {
        this.load(obj.id, obj.name, obj.source, obj.cover, obj.images, obj.token);
    },
    load: function (id, name, source, cover, images, token)
    {
        this.id = id;
        this.name = name;
        this.source = source;
        this.cover = cover;
        this.images = images;
        this.token = token;
        this.blank = false;
        this.draw();
    },
    clear: function ()
    {
        this.blank = true;
        this.id = 0;
        this.name = '';
        this.source = '';
        this.cover = false;
        this.token = '';
        this.images = {};
    },
    draw: function ()
    {
        if (this.blank)
        {
            this.htmlAdd.style.display = 'block';
            this.htmlDel.style.display = 'none';
            this.htmlCover.style.display = 'none';
            this.htmlImg.style.display = 'none';
            this.htmlImg.style.backgroundImage = '';

        } else
        {
            this.htmlAdd.style.display = 'none';
            this.htmlDel.style.display = 'block';
            this.htmlCover.style.display = 'block';
            this.htmlImg.style.display = 'block';
            if (this.images.h !== undefined)
            {
                this.htmlImg.style.backgroundImage = "url('" + encodeURI(this.images.h.url) + "')";
            }
            this.htmlCover.classList.remove('selected');
            if (this.cover)
            {
                this.htmlCover.classList.add('selected');
            }
        }
        if (this.mediaObject.autoSaveStore)
            this.mediaObject.saveToStore();
    }


};