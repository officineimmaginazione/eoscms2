<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */
class Setup
{

  protected function run()
  {
    $currentPath = dirname(filter_input(INPUT_SERVER, 'SCRIPT_FILENAME'));
    define('_EOS_PATH_FOLDER_', '');
    define('_EOS_PATH_APP_', $currentPath . DIRECTORY_SEPARATOR);
    require_once dirname($currentPath) . DIRECTORY_SEPARATOR . 'eos-config' . DIRECTORY_SEPARATOR . 'eos.defines.php';

    $dbtype = 'mysql';
    if (\EOS\System\Util\StringHelper::startsWith(_EOS_DB_DSN_, 'sqlite'))
    {
      $dbtype = 'sqlite';
    }

    if (empty(filter_input(INPUT_GET, 'module')))
    {
      $typePath = $currentPath . DIRECTORY_SEPARATOR . $dbtype . DIRECTORY_SEPARATOR;
      $list = glob($typePath . '*.php');
      echo "<strong>Click module to install/update database:</strong><br>\n";
      foreach ($list as $l)
      {
        $module = pathinfo($l, PATHINFO_FILENAME);
        printf('<a href="?module=%s">%s</a>' . "<br>\n", $module, $module);
      }
    } else
    {
      $module = basename(filter_input(INPUT_GET, 'module'));
      require_once $currentPath . DIRECTORY_SEPARATOR . $dbtype . DIRECTORY_SEPARATOR . $module . '.php';
    }
  }

  protected function newEvolver($section)
  {
    $evolver = new EOS\System\Database\Evolver();
    $evolver->section = $section;
    $evolver->logEvent = function ($value)
    {
      echo $value . "<br>\n";
    };
    $evolver->createDefaultTables();
    return $evolver;
  }

  public static function execute()
  {
    $setup = new \Setup();
    $setup->run();
  }
  
}

\Setup::execute();
