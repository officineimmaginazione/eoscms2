
--
-- Indici per le tabelle scaricate
--









--
-- Indici per le tabelle `eos_admin_group`
--
ALTER TABLE `eos_admin_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_admin_group_status` (`status`);

--
-- Indici per le tabelle `eos_admin_user`
--
ALTER TABLE `eos_admin_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_admin_user_email` (`email`);

--
-- Indici per le tabelle `eos_attachment`
--
ALTER TABLE `eos_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_attachment_mime` (`mime`);

--
-- Indici per le tabelle `eos_cart`
--
ALTER TABLE `eos_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_cart_status` (`status`);

--
-- Indici per le tabelle `eos_cart_cart_rule`
--
ALTER TABLE `eos_cart_cart_rule`
  ADD KEY `eos_cart_cart_rule_id_cart` (`id_cart`),
  ADD KEY `eos_cart_cart_rule_id_cart_rule` (`id_cart_rule`);

--
-- Indici per le tabelle `eos_cart_row`
--
ALTER TABLE `eos_cart_row`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eos_cart_row_id_cart` (`id_cart`);

--
-- Indici per le tabelle `eos_catalog_rules`
--
ALTER TABLE `eos_catalog_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_content`
--
ALTER TABLE `eos_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_type` (`type`),
  ADD KEY `idx_content_status` (`status`),
  ADD KEY `idx_content_parent` (`parent`);

--
-- Indici per le tabelle `eos_content_article`
--
ALTER TABLE `eos_content_article`
  ADD KEY `fk_content_article` (`id_content_lang`);

--
-- Indici per le tabelle `eos_content_category`
--
ALTER TABLE `eos_content_category`
  ADD KEY `idx_content_category_id_content` (`id_content`),
  ADD KEY `idx_content_category_id_category` (`id_category`);

--
-- Indici per le tabelle `eos_content_lang`
--
ALTER TABLE `eos_content_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_lang_id_content` (`id_content`),
  ADD KEY `idx_content_lang_id_lang` (`id_lang`);

--
-- Indici per le tabelle `eos_content_section`
--
ALTER TABLE `eos_content_section`
  ADD PRIMARY KEY (`id_content`),
  ADD UNIQUE KEY `idx_content_section_name` (`name`);

--
-- Indici per le tabelle `eos_currency`
--
ALTER TABLE `eos_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_feature`
--
ALTER TABLE `eos_feature`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_feature_lang`
--
ALTER TABLE `eos_feature_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lang` (`id_lang`,`name`);

--
-- Indici per le tabelle `eos_feature_product`
--
ALTER TABLE `eos_feature_product`
  ADD PRIMARY KEY (`id_feature`,`id_product`,`id_feature_value`),
  ADD KEY `id_feature_value` (`id_feature_value`),
  ADD KEY `id_product` (`id_product`);

--
-- Indici per le tabelle `eos_feature_shop`
--
ALTER TABLE `eos_feature_shop`
  ADD PRIMARY KEY (`id_feature`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indici per le tabelle `eos_feature_value`
--
ALTER TABLE `eos_feature_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feature` (`id_feature`);

--
-- Indici per le tabelle `eos_feature_value_lang`
--
ALTER TABLE `eos_feature_value_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_image`
--
ALTER TABLE `eos_image`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_lang`
--
ALTER TABLE `eos_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_lcdp_cart_rule_free`
--
ALTER TABLE `eos_lcdp_cart_rule_free`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lcdp_cart_rule_free_status` (`status`),
  ADD KEY `idx_lcdp_cart_rule_free_date_start` (`date_start`),
  ADD KEY `idx_lcdp_cart_rule_free_date_end` (`date_end`);

--
-- Indici per le tabelle `eos_lcdp_cart_rule_free_item`
--
ALTER TABLE `eos_lcdp_cart_rule_free_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lcdp_cart_rule_free_item_id_rule` (`id_rule`),
  ADD KEY `idx_lcdp_cart_rule_free_item_id_item_type` (`id_item_type`);

--
-- Indici per le tabelle `eos_localization_city`
--
ALTER TABLE `eos_localization_city`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_localization_country`
--
ALTER TABLE `eos_localization_country`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_localization_country_lang`
--
ALTER TABLE `eos_localization_country_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_localization_region`
--
ALTER TABLE `eos_localization_region`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_localization_state`
--
ALTER TABLE `eos_localization_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_state_id_region` (`id_region`);

--
-- Indici per le tabelle `eos_manufacturer`
--
ALTER TABLE `eos_manufacturer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_manufacturer_status` (`status`);

--
-- Indici per le tabelle `eos_manufacturer_lang`
--
ALTER TABLE `eos_manufacturer_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_manufacturer_lang_id_manufacturer` (`id_manufacturer`),
  ADD KEY `idx_eos_manufacturer_lang_id_lang` (`id_lang`);

--
-- Indici per le tabelle `eos_menu`
--
ALTER TABLE `eos_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_menu_lang`
--
ALTER TABLE `eos_menu_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_order`
--
ALTER TABLE `eos_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_order_status` (`status`);

--
-- Indici per le tabelle `eos_order_row`
--
ALTER TABLE `eos_order_row`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_order_id_order` (`id_order`),
  ADD KEY `idx_eos_order_type_object` (`type_object`);

--
-- Indici per le tabelle `eos_order_state`
--
ALTER TABLE `eos_order_state`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_order_state_lang`
--
ALTER TABLE `eos_order_state_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_order_state_lang_id_order_state` (`id_order_state`);

--
-- Indici per le tabelle `eos_product`
--
ALTER TABLE `eos_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_status` (`status`);

--
-- Indici per le tabelle `eos_product_accessory`
--
ALTER TABLE `eos_product_accessory`
  ADD KEY `idx_eos_product_accessory_id_product_1` (`id_product_1`),
  ADD KEY `idx_eos_product_accessory_id_product_2` (`id_product_2`);

--
-- Indici per le tabelle `eos_product_attribute`
--
ALTER TABLE `eos_product_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_attribute_id_attribute_group` (`id_attribute_group`);

--
-- Indici per le tabelle `eos_product_attribute_group`
--
ALTER TABLE `eos_product_attribute_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_id_product_group_type` (`group_type`);

--
-- Indici per le tabelle `eos_product_attribute_group_lang`
--
ALTER TABLE `eos_product_attribute_group_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_attribute_group_lang_id_attribute` (`id_attribute_group`),
  ADD KEY `idx_eos_product_attribute_group_lang_id_lang` (`id_lang`);

--
-- Indici per le tabelle `eos_product_attribute_lang`
--
ALTER TABLE `eos_product_attribute_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_attribute_lang_id_attribute` (`id_attribute`),
  ADD KEY `idx_eos_product_attribute_lang_id_lang` (`id_lang`);

--
-- Indici per le tabelle `eos_product_category`
--
ALTER TABLE `eos_product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_status` (`status`);

--
-- Indici per le tabelle `eos_product_category_lang`
--
ALTER TABLE `eos_product_category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_category_lang` (`id_category`);

--
-- Indici per le tabelle `eos_product_child`
--
ALTER TABLE `eos_product_child`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_child_id_product` (`id_product`);

--
-- Indici per le tabelle `eos_product_child_attribute_combination`
--
ALTER TABLE `eos_product_child_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`,`id_product_child`),
  ADD KEY `idx_eos_product_child_attribute_combination_id_attribute` (`id_attribute`),
  ADD KEY `idx_eos_product_child_attribute_combination_id_product_child` (`id_product_child`);

--
-- Indici per le tabelle `eos_product_image`
--
ALTER TABLE `eos_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_id_product` (`id_product`);

--
-- Indici per le tabelle `eos_product_lang`
--
ALTER TABLE `eos_product_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_lang` (`id_product`);

--
-- Indici per le tabelle `eos_product_product_attribute_combination`
--
ALTER TABLE `eos_product_product_attribute_combination`
  ADD PRIMARY KEY (`id_product`,`id_attribute`),
  ADD KEY `idx_eos_product_attribute_combination_id_product_attribute` (`id_product`) USING BTREE;

--
-- Indici per le tabelle `eos_product_product_category`
--
ALTER TABLE `eos_product_product_category`
  ADD PRIMARY KEY (`id_product`,`id_category`),
  ADD KEY `idx_eos_product_product_category_id_product` (`id_product`),
  ADD KEY `idx_eos_product_product_category_id_category` (`id_category`);

--
-- Indici per le tabelle `eos_product_stock`
--
ALTER TABLE `eos_product_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_product_stock_id_product` (`id_product`);

--
-- Indici per le tabelle `eos_router`
--
ALTER TABLE `eos_router`
  ADD PRIMARY KEY (`path`,`lang`),
  ADD UNIQUE KEY `idx_router_route` (`route`,`lang`),
  ADD KEY `idx_router_container` (`container`);

--
-- Indici per le tabelle `eos_seo`
--
ALTER TABLE `eos_seo`
  ADD PRIMARY KEY (`component`,`id_object`,`id_lang`);

--
-- Indici per le tabelle `eos_setting`
--
ALTER TABLE `eos_setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_setting_objectname` (`object`,`name`);

--
-- Indici per le tabelle `eos_sync`
--
ALTER TABLE `eos_sync`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `eos_sync_detail`
--
ALTER TABLE `eos_sync_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_sync_detail_resource` (`id_sync`,`resource`,`id_resource`);

--
-- Indici per le tabelle `eos_tax`
--
ALTER TABLE `eos_tax`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_tax_status` (`status`);

--
-- Indici per le tabelle `eos_tax_lang`
--
ALTER TABLE `eos_tax_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eos_tax_lang_id_tax` (`id_tax`),
  ADD KEY `idx_eos_tax_lang_id_lang` (`id_lang`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `eos_account_group`
--
ALTER TABLE `eos_account_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_log`
--
ALTER TABLE `eos_account_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_router`
--
ALTER TABLE `eos_account_router`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_router_group`
--
ALTER TABLE `eos_account_router_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_user`
--
ALTER TABLE `eos_account_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_user_cmd`
--
ALTER TABLE `eos_account_user_cmd`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_account_user_group`
--
ALTER TABLE `eos_account_user_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_address`
--
ALTER TABLE `eos_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_admin_group`
--
ALTER TABLE `eos_admin_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_admin_user`
--
ALTER TABLE `eos_admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_attachment`
--
ALTER TABLE `eos_attachment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_cart`
--
ALTER TABLE `eos_cart`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_cart_row`
--
ALTER TABLE `eos_cart_row`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_catalog_rules`
--
ALTER TABLE `eos_catalog_rules`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_content`
--
ALTER TABLE `eos_content`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_content_lang`
--
ALTER TABLE `eos_content_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_currency`
--
ALTER TABLE `eos_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_feature`
--
ALTER TABLE `eos_feature`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_feature_lang`
--
ALTER TABLE `eos_feature_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_feature_value`
--
ALTER TABLE `eos_feature_value`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_feature_value_lang`
--
ALTER TABLE `eos_feature_value_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_image`
--
ALTER TABLE `eos_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_lang`
--
ALTER TABLE `eos_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_lcdp_cart_rule_free`
--
ALTER TABLE `eos_lcdp_cart_rule_free`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_lcdp_cart_rule_free_item`
--
ALTER TABLE `eos_lcdp_cart_rule_free_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_localization_city`
--
ALTER TABLE `eos_localization_city`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_localization_country`
--
ALTER TABLE `eos_localization_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_localization_country_lang`
--
ALTER TABLE `eos_localization_country_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_localization_region`
--
ALTER TABLE `eos_localization_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_localization_state`
--
ALTER TABLE `eos_localization_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_manufacturer`
--
ALTER TABLE `eos_manufacturer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_manufacturer_lang`
--
ALTER TABLE `eos_manufacturer_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_menu`
--
ALTER TABLE `eos_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_menu_lang`
--
ALTER TABLE `eos_menu_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_order`
--
ALTER TABLE `eos_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_order_row`
--
ALTER TABLE `eos_order_row`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_order_state`
--
ALTER TABLE `eos_order_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_order_state_lang`
--
ALTER TABLE `eos_order_state_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product`
--
ALTER TABLE `eos_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_attribute`
--
ALTER TABLE `eos_product_attribute`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_attribute_group`
--
ALTER TABLE `eos_product_attribute_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_attribute_group_lang`
--
ALTER TABLE `eos_product_attribute_group_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_attribute_lang`
--
ALTER TABLE `eos_product_attribute_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_category`
--
ALTER TABLE `eos_product_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_category_lang`
--
ALTER TABLE `eos_product_category_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_child`
--
ALTER TABLE `eos_product_child`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_image`
--
ALTER TABLE `eos_product_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_lang`
--
ALTER TABLE `eos_product_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_product_stock`
--
ALTER TABLE `eos_product_stock`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_setting`
--
ALTER TABLE `eos_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_sync`
--
ALTER TABLE `eos_sync`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_sync_detail`
--
ALTER TABLE `eos_sync_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_tax`
--
ALTER TABLE `eos_tax`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eos_tax_lang`
--
ALTER TABLE `eos_tax_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `eos_order_row`
--
ALTER TABLE `eos_order_row`
  ADD CONSTRAINT `fk_id_order` FOREIGN KEY (`id_order`) REFERENCES `eos_order` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `eos_sync_detail`
--
ALTER TABLE `eos_sync_detail`
  ADD CONSTRAINT `fk_sync_detail_id_sync` FOREIGN KEY (`id_sync`) REFERENCES `eos_sync` (`id`);
