
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_group`
--

DROP TABLE IF EXISTS `eos_account_group`;
CREATE TABLE `eos_account_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reduction` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `def` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Indici per le tabelle `eos_account_group`
--
ALTER TABLE `eos_account_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_group_status` (`status`),
  ADD KEY `idx_account_group_def` (`def`);
