
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_log`
--

DROP TABLE IF EXISTS `eos_account_log`;
CREATE TABLE `eos_account_log` (
  `id` bigint(20) NOT NULL,
  `logdate` datetime NOT NULL,
  `logname` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `cmd` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indici per le tabelle `eos_account_log`
--
ALTER TABLE `eos_account_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_log_logdate` (`logdate`),
  ADD KEY `idx_account_log_ip` (`ip`),
  ADD KEY `idx_account_log_id_user` (`id_user`);
