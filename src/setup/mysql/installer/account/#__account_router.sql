
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_router`
--

DROP TABLE IF EXISTS `eos_account_router`;
CREATE TABLE `eos_account_router` (
  `id` bigint(20) NOT NULL,
  `route` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `auth` tinyint(4) NOT NULL DEFAULT '1',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indici per le tabelle `eos_account_router`
--
ALTER TABLE `eos_account_router`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_account_router_route` (`route`),
  ADD KEY `idx_account_router_container` (`container`),
  ADD KEY `idx_account_router_auth` (`auth`);
