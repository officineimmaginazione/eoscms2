
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_router_group`
--

DROP TABLE IF EXISTS `eos_account_router_group`;
CREATE TABLE `eos_account_router_group` (
  `id` bigint(20) NOT NULL,
  `id_router` bigint(20) NOT NULL,
  `id_group` int(11) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indici per le tabelle `eos_account_router_group`
--
ALTER TABLE `eos_account_router_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_router_group_router` (`id_router`),
  ADD KEY `fk_account_router_group_group` (`id_group`);
