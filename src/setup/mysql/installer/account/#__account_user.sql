
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_user`
--

DROP TABLE IF EXISTS `eos_account_user`;
CREATE TABLE `eos_account_user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `id_lang` int(11) NOT NULL,
  `options` longtext COLLATE utf8mb4_unicode_ci,
  `password_date` datetime DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `reference` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_id` int(11) NOT NULL DEFAULT '0',
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL DEFAULT '0',
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indici per le tabelle `eos_account_user`
--
ALTER TABLE `eos_account_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_account_user_username` (`username`),
  ADD UNIQUE KEY `idx_account_user_email` (`email`),
  ADD KEY `idx_account_user_status` (`status`),
  ADD KEY `idx_account_user_id_lang` (`id_lang`);
