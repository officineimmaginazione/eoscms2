
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_user_cmd`
--

DROP TABLE IF EXISTS `eos_account_user_cmd`;
CREATE TABLE `eos_account_user_cmd` (
  `id` bigint(20) NOT NULL,
  `token` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `cmd` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmddate` datetime NOT NULL,
  `cmdexpiration` datetime NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Indici per le tabelle `eos_account_user_cmd`
--
ALTER TABLE `eos_account_user_cmd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_user_cmd_user` (`id_user`),
  ADD KEY `idx_account_user_cmd_cmddate` (`cmddate`),
  ADD KEY `idx_account_user_cmd_cmdexpiration` (`cmdexpiration`);
