
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_account_user_group`
--

DROP TABLE IF EXISTS `eos_account_user_group`;
CREATE TABLE `eos_account_user_group` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_group` int(11) NOT NULL,
  `ins_id` int(11) DEFAULT '0',
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL DEFAULT '0',
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indici per le tabelle `eos_account_user_group`
--
ALTER TABLE `eos_account_user_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_user_group_user` (`id_user`),
  ADD KEY `fk_account_user_group_group` (`id_group`);
