
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_address`
--

DROP TABLE IF EXISTS `eos_address`;
CREATE TABLE `eos_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_country` int(111) UNSIGNED NOT NULL,
  `id_state` int(11) UNSIGNED DEFAULT NULL,
  `id_customer` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `id_manufacturer` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `id_city` bigint(20) NOT NULL DEFAULT '0',
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `reference` varchar(40) DEFAULT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1',
  `options` mediumtext,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle `eos_address`
--
ALTER TABLE `eos_address`
  ADD PRIMARY KEY (`id`);
