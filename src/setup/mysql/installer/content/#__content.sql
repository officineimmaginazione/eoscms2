
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_content`
--

DROP TABLE IF EXISTS `eos_content`;
CREATE TABLE `eos_content` (
  `id` bigint(20) NOT NULL,
  `type` smallint(6) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `extra_status` tinyint(4) NOT NULL DEFAULT '0',
  `extra` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
