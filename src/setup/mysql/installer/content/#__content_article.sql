
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_content_article`
--

DROP TABLE IF EXISTS `eos_content_article`;
CREATE TABLE `eos_content_article` (
  `id_content_lang` bigint(20) NOT NULL,
  `author` int(11) NOT NULL,
  `publish_date` datetime NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
