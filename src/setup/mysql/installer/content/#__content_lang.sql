
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_content_lang`
--

DROP TABLE IF EXISTS `eos_content_lang`;
CREATE TABLE `eos_content_lang` (
  `id` bigint(20) NOT NULL,
  `id_content` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `subtitle` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `content` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
