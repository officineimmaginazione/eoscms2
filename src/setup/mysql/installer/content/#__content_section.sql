
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_content_section`
--

DROP TABLE IF EXISTS `eos_content_section`;
CREATE TABLE `eos_content_section` (
  `id_content` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
