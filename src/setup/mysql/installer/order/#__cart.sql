
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_cart`
--

DROP TABLE IF EXISTS `eos_cart`;
CREATE TABLE `eos_cart` (
  `id` bigint(20) NOT NULL,
  `up_date` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `total` decimal(20,6) NOT NULL,
  `id_currency` bigint(20) NOT NULL DEFAULT '0',
  `id_customer` bigint(20) NOT NULL DEFAULT '0',
  `id_address_delivery` bigint(20) DEFAULT NULL,
  `id_address_invoice` bigint(20) DEFAULT NULL,
  `id_agent` bigint(20) NOT NULL DEFAULT '0',
  `options` mediumtext,
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
