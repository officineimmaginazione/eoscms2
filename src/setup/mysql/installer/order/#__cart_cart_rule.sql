
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_cart_cart_rule`
--

DROP TABLE IF EXISTS `eos_cart_cart_rule`;
CREATE TABLE `eos_cart_cart_rule` (
  `id_cart` bigint(20) NOT NULL,
  `id_cart_rule` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
