
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_cart_row`
--

DROP TABLE IF EXISTS `eos_cart_row`;
CREATE TABLE `eos_cart_row` (
  `id` bigint(20) NOT NULL,
  `id_cart` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_product_child` bigint(20) NOT NULL DEFAULT '0',
  `id_customer` bigint(20) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
