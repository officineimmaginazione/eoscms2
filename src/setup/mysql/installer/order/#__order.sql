
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_order`
--

DROP TABLE IF EXISTS `eos_order`;
CREATE TABLE `eos_order` (
  `id` int(11) NOT NULL,
  `reference` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy` smallint(6) DEFAULT NULL,
  `id_transaction` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_customer` bigint(20) NOT NULL DEFAULT '0',
  `id_address_delivery` bigint(20) NOT NULL DEFAULT '0',
  `id_address_invoice` int(11) NOT NULL DEFAULT '0',
  `id_agent` int(11) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `options` mediumtext COLLATE utf8mb4_unicode_ci,
  `order_date_add` datetime DEFAULT NULL,
  `order_date_upd` datetime DEFAULT NULL,
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
