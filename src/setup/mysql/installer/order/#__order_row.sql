
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_order_row`
--

DROP TABLE IF EXISTS `eos_order_row`;
CREATE TABLE `eos_order_row` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT '0',
  `id_object` int(11) NOT NULL,
  `type_object` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `product_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `options` mediumtext COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
