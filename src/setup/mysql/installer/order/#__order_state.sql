
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_order_state`
--

DROP TABLE IF EXISTS `eos_order_state`;
CREATE TABLE `eos_order_state` (
  `id` int(11) NOT NULL,
  `invoice` tinyint(1) UNSIGNED DEFAULT '0',
  `send_email` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `color` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
