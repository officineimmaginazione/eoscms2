
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_order_state_lang`
--

DROP TABLE IF EXISTS `eos_order_state_lang`;
CREATE TABLE `eos_order_state_lang` (
  `id` int(11) NOT NULL,
  `id_order_state` int(11) NOT NULL DEFAULT '0',
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
