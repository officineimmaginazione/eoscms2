
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product`
--

DROP TABLE IF EXISTS `eos_product`;
CREATE TABLE `eos_product` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) DEFAULT NULL,
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_tax` bigint(20) NOT NULL DEFAULT '0',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `min_quantity` int(11) NOT NULL DEFAULT '0',
  `quantity_per_pack` bigint(20) NOT NULL DEFAULT '0',
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `type_condition` int(11) NOT NULL DEFAULT '0',
  `online_only` tinyint(1) NOT NULL DEFAULT '0',
  `ean13` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isbn` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upc` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `out_of_stock` tinyint(4) NOT NULL DEFAULT '2',
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `extra_display_info` int(11) NOT NULL DEFAULT '0',
  `expected_date` date DEFAULT NULL,
  `up_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `ins_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
