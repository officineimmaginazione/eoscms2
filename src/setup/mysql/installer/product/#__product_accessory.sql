
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_accessory`
--

DROP TABLE IF EXISTS `eos_product_accessory`;
CREATE TABLE `eos_product_accessory` (
  `id_product_1` bigint(20) NOT NULL,
  `id_product_2` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
