
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_attribute`
--

DROP TABLE IF EXISTS `eos_product_attribute`;
CREATE TABLE `eos_product_attribute` (
  `id` bigint(20) NOT NULL,
  `id_attribute_group` bigint(20) NOT NULL,
  `color` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
