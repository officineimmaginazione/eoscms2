
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_attribute_group`
--

DROP TABLE IF EXISTS `eos_product_attribute_group`;
CREATE TABLE `eos_product_attribute_group` (
  `id` bigint(20) NOT NULL,
  `is_color_group` tinyint(1) NOT NULL,
  `group_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
