
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_attribute_group_lang`
--

DROP TABLE IF EXISTS `eos_product_attribute_group_lang`;
CREATE TABLE `eos_product_attribute_group_lang` (
  `id` bigint(20) NOT NULL,
  `id_attribute_group` bigint(20) NOT NULL,
  `id_lang` bigint(11) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `public_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
