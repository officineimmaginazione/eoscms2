
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_attribute_lang`
--

DROP TABLE IF EXISTS `eos_product_attribute_lang`;
CREATE TABLE `eos_product_attribute_lang` (
  `id` bigint(20) NOT NULL,
  `id_attribute` bigint(20) NOT NULL,
  `id_lang` bigint(20) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
