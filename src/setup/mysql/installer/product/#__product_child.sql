
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_child`
--

DROP TABLE IF EXISTS `eos_product_child`;
CREATE TABLE `eos_product_child` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
