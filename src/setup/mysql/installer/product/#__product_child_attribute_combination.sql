
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_child_attribute_combination`
--

DROP TABLE IF EXISTS `eos_product_child_attribute_combination`;
CREATE TABLE `eos_product_child_attribute_combination` (
  `id_attribute` int(10) NOT NULL,
  `id_product_child` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
