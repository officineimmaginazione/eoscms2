
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_image`
--

DROP TABLE IF EXISTS `eos_product_image`;
CREATE TABLE `eos_product_image` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL,
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
