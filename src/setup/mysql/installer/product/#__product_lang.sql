
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_lang`
--

DROP TABLE IF EXISTS `eos_product_lang`;
CREATE TABLE `eos_product_lang` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci,
  `description` mediumtext CHARACTER SET utf8,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
