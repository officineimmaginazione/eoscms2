
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_product_attribute_combination`
--

DROP TABLE IF EXISTS `eos_product_product_attribute_combination`;
CREATE TABLE `eos_product_product_attribute_combination` (
  `id_product` bigint(20) NOT NULL,
  `id_attribute` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
