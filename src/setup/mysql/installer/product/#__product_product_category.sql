
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_product_category`
--

DROP TABLE IF EXISTS `eos_product_product_category`;
CREATE TABLE `eos_product_product_category` (
  `id_product` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
