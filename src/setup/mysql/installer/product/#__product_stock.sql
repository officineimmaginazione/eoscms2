
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_product_stock`
--

DROP TABLE IF EXISTS `eos_product_stock`;
CREATE TABLE `eos_product_stock` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_product_child` bigint(20) DEFAULT NULL,
  `quantity_available` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `quantity_physical` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `quantity_engaged` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `quantity_reserved` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `quantity_ordered1` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `quantity_ordered2` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `date_ordered1` date DEFAULT NULL,
  `date_ordered2` date DEFAULT NULL,
  `up_id` int(11) NOT NULL DEFAULT '0',
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ins_id` int(11) NOT NULL DEFAULT '0',
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
