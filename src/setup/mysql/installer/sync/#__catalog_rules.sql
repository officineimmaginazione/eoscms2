
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_catalog_rules`
--

DROP TABLE IF EXISTS `eos_catalog_rules`;
CREATE TABLE `eos_catalog_rules` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_product` bigint(20) UNSIGNED NOT NULL,
  `id_currency` bigint(20) UNSIGNED NOT NULL,
  `id_group` bigint(20) UNSIGNED NOT NULL,
  `id_customer` bigint(20) UNSIGNED NOT NULL,
  `id_product_attribute` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_sum2` decimal(10,0) DEFAULT '0',
  `reduction_sum3` decimal(10,0) DEFAULT '0',
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` varchar(255) NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `reference` varchar(40) DEFAULT NULL,
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL DEFAULT '0',
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ins_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
