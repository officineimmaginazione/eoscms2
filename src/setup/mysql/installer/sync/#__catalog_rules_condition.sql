
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_catalog_rules_condition`
--

DROP TABLE IF EXISTS `eos_catalog_rules_condition`;
CREATE TABLE `eos_catalog_rules_condition` (
  `id` bigint(20) NOT NULL,
  `id_catalog_rules` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
