
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_currency`
--

DROP TABLE IF EXISTS `eos_currency`;
CREATE TABLE `eos_currency` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `conversion_rate` decimal(13,6) NOT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
