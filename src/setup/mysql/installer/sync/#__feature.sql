
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature`
--

DROP TABLE IF EXISTS `eos_feature`;
CREATE TABLE `eos_feature` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reference` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
