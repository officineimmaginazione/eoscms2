
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature_lang`
--

DROP TABLE IF EXISTS `eos_feature_lang`;
CREATE TABLE `eos_feature_lang` (
  `id` int(11) NOT NULL,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
