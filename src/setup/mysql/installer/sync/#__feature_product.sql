
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature_product`
--

DROP TABLE IF EXISTS `eos_feature_product`;
CREATE TABLE `eos_feature_product` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
