
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature_shop`
--

DROP TABLE IF EXISTS `eos_feature_shop`;
CREATE TABLE `eos_feature_shop` (
  `id_feature` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
