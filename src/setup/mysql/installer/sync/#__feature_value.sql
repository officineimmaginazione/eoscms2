
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature_value`
--

DROP TABLE IF EXISTS `eos_feature_value`;
CREATE TABLE `eos_feature_value` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `custom` tinyint(3) UNSIGNED DEFAULT NULL,
  `reference` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
