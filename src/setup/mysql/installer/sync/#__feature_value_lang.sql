
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_feature_value_lang`
--

DROP TABLE IF EXISTS `eos_feature_value_lang`;
CREATE TABLE `eos_feature_value_lang` (
  `id` int(11) NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
