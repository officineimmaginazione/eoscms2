
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_lcdp_cart_rule_free`
--

DROP TABLE IF EXISTS `eos_lcdp_cart_rule_free`;
CREATE TABLE `eos_lcdp_cart_rule_free` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `qty_calc` int(11) NOT NULL DEFAULT '0',
  `qty_free` int(11) NOT NULL DEFAULT '0',
  `free_calc_type` tinyint(4) NOT NULL DEFAULT '1',
  `product_include` tinyint(4) NOT NULL DEFAULT '0',
  `product_exclude` tinyint(4) NOT NULL DEFAULT '0',
  `change_product` tinyint(4) NOT NULL DEFAULT '0',
  `stock_incoming` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
