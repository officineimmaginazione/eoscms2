
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_lcdp_cart_rule_free_item`
--

DROP TABLE IF EXISTS `eos_lcdp_cart_rule_free_item`;
CREATE TABLE `eos_lcdp_cart_rule_free_item` (
  `id` bigint(20) NOT NULL,
  `id_rule` int(11) NOT NULL,
  `item_type` enum('product','manufacturer','category') NOT NULL,
  `id_item_type` bigint(20) NOT NULL,
  `link_type` tinyint(4) NOT NULL DEFAULT '1',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
