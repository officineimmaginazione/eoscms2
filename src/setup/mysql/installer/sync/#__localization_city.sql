
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_localization_city`
--

DROP TABLE IF EXISTS `eos_localization_city`;
CREATE TABLE `eos_localization_city` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_state` int(11) NOT NULL,
  `zip_code` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` float NOT NULL DEFAULT '0',
  `longitude` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
