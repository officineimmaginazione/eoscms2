
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_localization_country`
--

DROP TABLE IF EXISTS `eos_localization_country`;
CREATE TABLE `eos_localization_country` (
  `id` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `iso_code` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `up_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `ins_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
