
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_localization_country_lang`
--

DROP TABLE IF EXISTS `eos_localization_country_lang`;
CREATE TABLE `eos_localization_country_lang` (
  `id` int(11) NOT NULL,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `up_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `ins_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
