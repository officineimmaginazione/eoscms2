
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_localization_region`
--

DROP TABLE IF EXISTS `eos_localization_region`;
CREATE TABLE `eos_localization_region` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
