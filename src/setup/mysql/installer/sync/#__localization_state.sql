
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_localization_state`
--

DROP TABLE IF EXISTS `eos_localization_state`;
CREATE TABLE `eos_localization_state` (
  `id` int(11) NOT NULL,
  `iso_code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_region` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
