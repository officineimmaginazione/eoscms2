
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_manufacturer_lang`
--

DROP TABLE IF EXISTS `eos_manufacturer_lang`;
CREATE TABLE `eos_manufacturer_lang` (
  `id` bigint(20) NOT NULL,
  `id_manufacturer` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
