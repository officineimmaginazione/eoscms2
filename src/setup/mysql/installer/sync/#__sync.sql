
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_sync`
--

DROP TABLE IF EXISTS `eos_sync`;
CREATE TABLE `eos_sync` (
  `id` int(11) NOT NULL,
  `name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `status` smallint(6) NOT NULL,
  `ins_date` datetime DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
  `ins_id` int(11) DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
