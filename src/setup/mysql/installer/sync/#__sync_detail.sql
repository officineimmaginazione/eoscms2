
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_sync_detail`
--

DROP TABLE IF EXISTS `eos_sync_detail`;
CREATE TABLE `eos_sync_detail` (
  `id` bigint(20) NOT NULL,
  `id_sync` int(11) NOT NULL,
  `resource` varchar(40) CHARACTER SET utf8 NOT NULL,
  `id_resource` bigint(20) NOT NULL,
  `sync_res` varchar(40) CHARACTER SET utf8 NOT NULL,
  `sync_res_field` varchar(40) CHARACTER SET utf8 NOT NULL,
  `sync_res_value` varchar(40) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
