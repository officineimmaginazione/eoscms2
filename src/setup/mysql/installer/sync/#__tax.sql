
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_tax`
--

DROP TABLE IF EXISTS `eos_tax`;
CREATE TABLE `eos_tax` (
  `id` bigint(20) NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1',
  `reference` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
