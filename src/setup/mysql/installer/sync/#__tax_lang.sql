
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_tax_lang`
--

DROP TABLE IF EXISTS `eos_tax_lang`;
CREATE TABLE `eos_tax_lang` (
  `id` bigint(20) NOT NULL,
  `id_tax` bigint(20) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
