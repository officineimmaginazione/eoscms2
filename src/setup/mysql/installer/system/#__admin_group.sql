
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_admin_group`
--

DROP TABLE IF EXISTS `eos_admin_group`;
CREATE TABLE `eos_admin_group` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authorization` mediumtext CHARACTER SET utf8,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
