
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_admin_user`
--

DROP TABLE IF EXISTS `eos_admin_user`;
CREATE TABLE `eos_admin_user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_group` bigint(20) NOT NULL,
  `registration_date` datetime DEFAULT NULL,
  `lastvisit_date` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
