
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_attachment`
--

DROP TABLE IF EXISTS `eos_attachment`;
CREATE TABLE `eos_attachment` (
  `id` bigint(20) NOT NULL,
  `type_object` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_object` bigint(20) NOT NULL,
  `mime` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `path` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
