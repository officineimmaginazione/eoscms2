
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_image`
--

DROP TABLE IF EXISTS `eos_image`;
CREATE TABLE `eos_image` (
  `id` bigint(20) NOT NULL,
  `type_object` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_object` bigint(20) NOT NULL,
  `cover` tinyint(4) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `path` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
