
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_lang`
--

DROP TABLE IF EXISTS `eos_lang`;
CREATE TABLE `eos_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `status_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_site` tinyint(4) NOT NULL DEFAULT '0',
  `status_site` tinyint(4) NOT NULL DEFAULT '0',
  `iso_code` char(2) CHARACTER SET utf8 NOT NULL,
  `language_code` varchar(32) CHARACTER SET utf8 NOT NULL,
  `datelite_format` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT 'Y-m-d',
  `datefull_format` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT 'Y-m-d H:i:s'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
