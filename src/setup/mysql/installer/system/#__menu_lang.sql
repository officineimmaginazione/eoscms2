
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_menu_lang`
--

DROP TABLE IF EXISTS `eos_menu_lang`;
CREATE TABLE `eos_menu_lang` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
