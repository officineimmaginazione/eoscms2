
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_router`
--

DROP TABLE IF EXISTS `eos_router`;
CREATE TABLE `eos_router` (
  `path` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `menu` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
