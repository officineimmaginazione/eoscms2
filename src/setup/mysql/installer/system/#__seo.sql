
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_seo`
--

DROP TABLE IF EXISTS `eos_seo`;
CREATE TABLE `eos_seo` (
  `id_object` bigint(20) NOT NULL,
  `component` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_lang` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_raw` mediumtext COLLATE utf8mb4_unicode_ci,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
