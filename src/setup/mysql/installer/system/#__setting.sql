
-- --------------------------------------------------------

--
-- Struttura della tabella `eos_setting`
--

DROP TABLE IF EXISTS `eos_setting`;
CREATE TABLE `eos_setting` (
  `id` int(11) NOT NULL,
  `object` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
