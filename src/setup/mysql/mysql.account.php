<?php

$evolver = $this->newEvolver('account');
$evolver->add(1000, function ()
{
    $list = [];
    $list[] = "CREATE TABLE `#__account_user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(255) NULL, 
  `status` tinyint NOT NULL DEFAULT 0,
  `id_lang` int(11) NOT NULL,
  `options` longtext NOT NULL,
  `password_date` datetime NULL, 
  `last_access` datetime NULL, 
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL,
   CONSTRAINT pk_account_user PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    $list[] = "ALTER TABLE `#__account_user`
        ADD UNIQUE KEY `idx_account_user_username` (`username`),
        ADD UNIQUE KEY `idx_account_user_email` (`email`),
        ADD KEY `idx_account_user_status` (`status`),
        ADD KEY `idx_account_user_id_lang` (`id_lang`);";

    $list[] = "CREATE TABLE `#__account_group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL, 
  `status` tinyint NOT NULL DEFAULT 0,
  `def` tinyint NOT NULL DEFAULT 0,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL,
   CONSTRAINT pk_account_group PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__account_group`
        ADD KEY `idx_account_group_status` (`status`),
        ADD KEY `idx_account_group_def` (`def`);";

    $list[] = "CREATE TABLE `#__account_user_group` (
  `id` BIGINT NULL AUTO_INCREMENT,
  `id_user` BIGINT NOT NULL,
  `id_group` INT NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL,
   CONSTRAINT pk_account_user_group PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    $list[] = "ALTER TABLE `#__account_user_group`
    ADD CONSTRAINT `fk_account_user_group_user` FOREIGN KEY (`id_user`) REFERENCES `#__account_user` (`id`) ON DELETE CASCADE;";
    $list[] = "ALTER TABLE `#__account_user_group`
    ADD CONSTRAINT `fk_account_user_group_group` FOREIGN KEY (`id_group`) REFERENCES `#__account_group` (`id`) ON DELETE CASCADE";


    $list[] = "CREATE TABLE `#__account_user_cmd` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(150) NOT NULL,
  `id_user` BIGINT NOT NULL,
  `cmd` VARCHAR(40) NOT NULL,
  `cmddate` datetime NOT NULL,
  `cmdexpiration` datetime NOT NULL,
  `ip` varchar(45) NULL, 
  `options` longtext NOT NULL,
   CONSTRAINT pk_account_user_cmd PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__account_user_cmd`
    ADD CONSTRAINT `fk_account_user_cmd_user` FOREIGN KEY (`id_user`) REFERENCES `#__account_user` (`id`) ON DELETE CASCADE;";
    $list[] = "ALTER TABLE `#__account_user_cmd`
        ADD KEY `idx_account_user_cmd_cmddate` (`cmddate`),
        ADD KEY `idx_account_user_cmd_cmdexpiration` (`cmdexpiration`)";
    // https://stackoverflow.com/questions/1076714/max-length-for-client-ip-address
    $list[] = "CREATE TABLE `#__account_log` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `logdate` datetime NOT NULL,
  `logname` varchar(150) NULL,  
  `ip` varchar(45) NULL, 
  `id_user` int(11) NULL,
  `cmd` VARCHAR(40) NULL,
  `status` tinyint NOT NULL DEFAULT 0,
   CONSTRAINT pk_account_log PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__account_log`
        ADD KEY `idx_account_log_logdate` (`logdate`),
        ADD KEY `idx_account_log_ip` (`ip`),
        ADD KEY `idx_account_log_id_user` (`id_user`);";

    $list[] = "CREATE TABLE `#__account_router` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `route` varchar(190) NOT NULL,
  `container` tinyint NOT NULL DEFAULT 0,
  `auth` tinyint NOT NULL DEFAULT 1,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
  CONSTRAINT pk_account_router PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__account_router`
        ADD UNIQUE KEY `idx_account_router_route` (`route`),
        ADD KEY `idx_account_router_container` (`container`),
        ADD KEY `idx_account_router_auth` (`auth`)";

    $list[] = "CREATE TABLE `#__account_router_group` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `id_router` BIGINT NOT NULL,
  `id_group` INT NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
  CONSTRAINT pk_account_router_group PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__account_router_group`
    ADD CONSTRAINT `fk_account_router_group_router` FOREIGN KEY (`id_router`) REFERENCES `#__account_router` (`id`) ON DELETE CASCADE;";
    $list[] = "ALTER TABLE `#__account_router_group`
    ADD CONSTRAINT `fk_account_router_group_group` FOREIGN KEY (`id_group`) REFERENCES `#__account_group` (`id`)";
    $this->execArray($list);

    $list = [];
    $list[] = sprintf("insert into #__account_group (name, status, def, ins_id, up_date) values ('auth', 1, 1, 0, '%s')", $this->db->util->nowToDBDateTime());
    $this->execArray($list);
});
$evolver->execute();
