<?php

$evolver = $this->newEvolver('content');
$evolver->add(1000, function ()
{
    $list = [];
    $list[] = "CREATE TABLE `#__content` (
  `id` bigint(20) NOT NULL,
  `type` smallint(6) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__content_article` (
  `id_content_lang` bigint(20) NOT NULL,
  `author` int(11) NOT NULL,
  `publish_date` datetime NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__content_category` (
  `id_content` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__content_lang` (
  `id` bigint(20) NOT NULL,
  `id_content` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `options` mediumtext,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__content_section` (
  `id_content` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_type` (`type`),
  ADD KEY `idx_content_status` (`status`),
  ADD KEY `idx_content_parent` (`parent`);";
    $list[] = "ALTER TABLE `#__content_category`
  ADD KEY `idx_content_category_id_content` (`id_content`),
  ADD KEY `idx_content_category_id_category` (`id_category`);";
    $list[] = "ALTER TABLE `#__content_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_lang_id_content` (`id_content`),
  ADD KEY `idx_content_lang_id_lang` (`id_lang`);";
    $list[] = "ALTER TABLE `#__content_section`
  ADD PRIMARY KEY (`id_content`),
  ADD UNIQUE KEY `idx_content_section_name` (`name`);";
    $list[] = "ALTER TABLE `#__content`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__content_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__content_article`
  ADD CONSTRAINT `fk_content_article` FOREIGN KEY (`id_content_lang`) REFERENCES `#__content_lang` (`id`);  ";
    $list[] = "ALTER TABLE `#__content_section`
  ADD CONSTRAINT `fk_content_section` FOREIGN KEY (`id_content`) REFERENCES `#__content` (`id`);";
    $list[] = "ALTER TABLE `#__content_lang`
  ADD CONSTRAINT `fk_content_lang` FOREIGN KEY (`id_content`) REFERENCES `#__content` (`id`);";
    $this->execArray($list);
});

$evolver->add(1001, function ()
{
    $list = [];
    $list[] = 'ALTER TABLE `#__content` ADD `extra_status` TINYINT NOT NULL DEFAULT 0  AFTER `parent`,  ADD `extra` VARCHAR(150) NULL  AFTER `extra_status`;';
    $this->execArray($list);
});
$evolver->execute();
