<?php

$evolver = $this->newEvolver('order');
$evolver->add(1000, function ()
{
    $list = [];
    $list[] = "CREATE TABLE `#__order` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy` smallint(6) NOT NULL,
  `id_transaction` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` mediumtext NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) NOT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__order_row` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT '0',
  `id_object` int(11) NOT NULL,
  `type_object` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) NOT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__order_state` (
  `id` int(11) NOT NULL,
  `invoice` tinyint(1) unsigned DEFAULT '0',
  `send_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `color` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime DEFAULT NULL,
  `upd_id` int(11) NOT NULL,
  `upd_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__order_state_lang` (
  `id` int(11) NOT NULL,
  `id_order_state` int(11) NOT NULL DEFAULT '0',
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_order_status` (`status`);";
    $list[] = "ALTER TABLE `#__order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__order_row`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_order_id_order` (`id_order`),
  ADD KEY `idx_order_type_object` (`type_object`);";
    $list[] = "ALTER TABLE `#__order_row`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__order_state`
  ADD PRIMARY KEY (`id`);";
    $list[] = "ALTER TABLE `#__order_state_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_order_state_lang_id_order_state` (`id_order_state`);";
    $list[] = "ALTER TABLE `#__order_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__order_state_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__order_row`
  ADD CONSTRAINT `fk_order_row` FOREIGN KEY (`id_order`) REFERENCES `#__order` (`id`);";
    $list[] = "ALTER TABLE `#__order_state_lang`
  ADD CONSTRAINT `fk_order_state_lang` FOREIGN KEY (`id_order_state`) REFERENCES `#__order_state` (`id`);";
    $this->execArray($list);
});
$evolver->add(1100, function ()
{
    $list = [];
    $list[] = "CREATE TABLE `eos_order_cart` (
  `id` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `total` decimal(20,6) NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
    $list[] = "CREATE TABLE `eos_order_cart_product` (
  `id` bigint(20) NOT NULL,
  `id_order_cart` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
    $list[] = "ALTER TABLE `eos_order_cart`  ADD PRIMARY KEY (`id`),
 ADD KEY `idx_eos_order_cart_id_order` (`status`);";
    $list[] = "ALTER TABLE `eos_order_cart_product` ADD PRIMARY KEY (`id`),
  ADD KEY `eos_order_cart_product_id_order_cart` (`id_order_cart`);";
    $list[] = "ALTER TABLE `eos_order_cart` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `eos_order_cart_product` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `eos_order_cart_product`
  ADD CONSTRAINT `fk_eos_order_cart_produc` FOREIGN KEY (`id_order_cart`) REFERENCES `eos_order_cart` (`id`);";
    $list[] = "ALTER TABLE `eos_order_row` ADD `options` MEDIUMTEXT NOT NULL AFTER `price`; ";
    $this->execArray($list);
});
$evolver->add(1200, function ()
{
    $list = [];
   $list[] = "ALTER TABLE `eos_reservation_type` ADD `options` MEDIUMTEXT NULL AFTER `status`;  ";
    $this->execArray($list);
});

$evolver->execute();
