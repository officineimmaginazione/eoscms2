<?php

$evolver = $this->newEvolver('product');
$evolver->add(1000, function ()
{
    $list = [];
    $list[] = "CREATE TABLE `#__product` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `id_category` bigint(20) NOT NULL DEFAULT '0',
  `price` float(7,4) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
$list[] = "CREATE TABLE `#__product_category` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
$list[] = "CREATE TABLE `#__product_category_lang` (
  `id` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
$list[] = "CREATE TABLE `#__product_lang` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `options` mediumtext CHARACTER SET utf8,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
$list[] = "ALTER TABLE `#__product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_status` (`status`),
  ADD KEY `idx_product_id_category` (`id_category`);";
$list[] = "ALTER TABLE `#__product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_status` (`status`);";
$list[] = "ALTER TABLE `#__product_category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_category_lang` (`id_category`);";
$list[] = "ALTER TABLE `#__product_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_lang` (`id_product`);";
$list[] = "ALTER TABLE `#__product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
$list[] = "ALTER TABLE `#__product_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
$list[] = "ALTER TABLE `#__product_category_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
$list[] = "ALTER TABLE `#__product_lang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
$list[] = "ALTER TABLE `#__product_lang`
  ADD CONSTRAINT `fk_product_lang` FOREIGN KEY (`id_product`) REFERENCES `#__product` (`id`);";
$list[] = "ALTER TABLE `#__product_category_lang`
  ADD CONSTRAINT `fk_product_category_lang` FOREIGN KEY (`id_category`) REFERENCES `#__product_category` (`id`);";
    $this->execArray($list);
});
$evolver->execute();
