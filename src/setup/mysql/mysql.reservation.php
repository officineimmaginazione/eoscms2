<?php

$evolver = $this->newEvolver('reservation');
$evolver->add(1000, function ()
{
    $list = [];
    // Le chiavi su MySQL sono BIGINT, gli autoincrement su Sqlite possono solo essere INTEGER
    $list[] = "CREATE TABLE `#__reservation` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_timeslot` int(11) NOT NULL,
  `arrival` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_reservation` date NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy` smallint(1) NOT NULL,
  `commercial` smallint(1) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_category` (
  `id` int(11) NOT NULL COMMENT 'ID Type',
  `ins_id` int(11) DEFAULT NULL,
  `ins_date` datetime DEFAULT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_category_lang` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_location` (
  `id` int(11) NOT NULL COMMENT 'ID Location',
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Name location',
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Address location',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'City location',
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'State/province location',
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Country location',
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Phone location',
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Email location',
  `default_location` smallint(6) NOT NULL DEFAULT '0',
  `options` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_special_day` (
  `id` int(11) NOT NULL,
  `id_timeslot` int(11) NOT NULL,
  `special_day` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_timeslot` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `start` time NOT NULL,
  `finish` time NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_timeslot_lang` (
  `id` int(11) NOT NULL,
  `id_timeslot` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_type` (
  `id` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `availability_total` int(11) NOT NULL DEFAULT '0',
  `availability_customer` int(11) NOT NULL DEFAULT '0',
  `price_reservation` float(7,4) NOT NULL DEFAULT '0.0000',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_type_lang` (
  `id` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__reservation_type_timeslot` (
  `id_type` int(11) NOT NULL,
  `id_timeslot` int(11) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_id_category` (`id_category`),
  ADD KEY `idx_reservation_id_location` (`id_location`),
  ADD KEY `idx_reservation_id_type` (`id_type`),
  ADD KEY `idx_reservation_id_timeslot` (`id_timeslot`),
  ADD KEY `idx_reservation_date_reservation` (`date_reservation`),
  ADD KEY `idx_reservation_status` (`status`);";
    $list[] = "ALTER TABLE `#__reservation_category`
  ADD PRIMARY KEY (`id`);";
    $list[] = "ALTER TABLE `#__reservation_category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_category_lang` (`id_category`);";
    $list[] = "ALTER TABLE `#__reservation_location`
  ADD PRIMARY KEY (`id`);";
    $list[] = "ALTER TABLE `#__reservation_special_day`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_special_day_id_timeslot` (`id_timeslot`),
  ADD KEY `idx_reservation_special_day_special_day` (`special_day`),
  ADD KEY `idx_reservation_special_day_status` (`status`);";
    $list[] = "ALTER TABLE `#__reservation_timeslot`
  ADD PRIMARY KEY (`id`);";
    $list[] = "ALTER TABLE `#__reservation_timeslot_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_timeslot_lang_id_timeslot` (`id_timeslot`);";
    $list[] = "ALTER TABLE `#__reservation_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_type_id_category` (`id_category`),
  ADD KEY `idx_reservation_type_id_location` (`id_location`),
  ADD KEY `idx_reservation_type_status` (`status`);";
    $list[] = "ALTER TABLE `#__reservation_type_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_reservation_type_lang_id_type` (`id_type`);";
    $list[] = "ALTER TABLE `#__reservation_type_timeslot`
  ADD KEY `idx_reservation_type_timeslot_id_type` (`id_type`),
  ADD KEY `idx_reservation_type_timeslot_id_timeslot` (`id_timeslot`);";
    $list[] = "ALTER TABLE `#__reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Type';";
    $list[] = "ALTER TABLE `#__reservation_category_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Location';";
    $list[] = "ALTER TABLE `#__reservation_special_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_timeslot_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_type_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__reservation_category_lang`
  ADD CONSTRAINT `fk_reservation_category_lang` FOREIGN KEY (`id_category`) REFERENCES `#__reservation_category` (`id`);";
    $list[] = "ALTER TABLE `#__reservation_type_lang`
  ADD CONSTRAINT `fk_reservation_type_lang` FOREIGN KEY (`id_type`) REFERENCES `#__reservation_type` (`id`);";
    $list[] = "ALTER TABLE `#__reservation_timeslot_lang`
  ADD CONSTRAINT `fk_reservation_timeslot_lang` FOREIGN KEY (`id_timeslot`) REFERENCES `#__reservation_timeslot` (`id`);";
    $this->execArray($list);
});
$evolver->execute();
