<?php

$evolver = $this->newEvolver('system');
$evolver->add(1000, function ()
{
    $list = [];
    // Le chiavi su MySQL sono BIGINT, gli autoincrement su Sqlite possono solo essere INTEGER
    $list[] = "CREATE TABLE `#__lang` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `status_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_site` tinyint(4) NOT NULL DEFAULT '0',
  `status_site` tinyint(4) NOT NULL DEFAULT '0',
  `iso_code` char(2) NOT NULL,
  `language_code` varchar(32) NOT NULL,
  `datelite_format` varchar(32) NOT NULL DEFAULT 'Y-m-d',
  `datefull_format` varchar(32) NOT NULL DEFAULT 'Y-m-d H:i:s'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__menu_lang` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `data` mediumtext NOT NULL,
  `options` mediumtext NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__router` (
  `path` varchar(190) NOT NULL,
  `route` varchar(190) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `container` tinyint(4) NOT NULL DEFAULT '0',
  `menu` tinyint(4) NOT NULL DEFAULT '0',
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) DEFAULT NULL,
  `up_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__seo` (
  `id_object` bigint(20) NOT NULL,
  `component` varchar(50) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `meta_title` varchar(150) DEFAULT NULL,
  `meta_description` varchar(300) DEFAULT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `meta_raw` mediumtext,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__admin_user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "CREATE TABLE `#__admin_user_profile` (
  `id_user` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `name` varchar(75) NOT NULL,
  `surname` varchar(75)  NOT NULL,
  `registration_date` datetime DEFAULT NULL,
  `lastvisit_date` datetime DEFAULT NULL,
  `options` mediumtext  NOT NULL,
  `ins_id` int(11) NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int(11) NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
    $list[] = "ALTER TABLE `#__menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menu_name` (`name`);";
    $list[] = "ALTER TABLE `#__menu_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu_lang_menu` (`id_menu`),
  ADD KEY `fk_menu_lang_id_lang` (`id_lang`); ";
    $list[] = "ALTER TABLE `#__menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__menu_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__lang`
  ADD PRIMARY KEY (`id`);";
    $list[] = "ALTER TABLE `#__lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__menu_lang`
  ADD CONSTRAINT `fk_menu_lang_menu` FOREIGN KEY (`id_menu`) REFERENCES `#__menu` (`id`);";
    $list[] = "ALTER TABLE `#__router`
  ADD PRIMARY KEY (`path`,`lang`),
  ADD UNIQUE KEY `idx_router_route` (`route`,`lang`),
  ADD KEY `idx_router_container` (`container`);";
    $list[] = "ALTER TABLE `#__seo`
  ADD PRIMARY KEY (`component`,`id_object`,`id_lang`);";
    $list[] = "ALTER TABLE `#__admin_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_admin_user_email` (`email`);";
    $list[] = "ALTER TABLE `#__admin_user_profile`
  ADD KEY `idx_admin_user_profile_type` (`type`);";
    $list[] = "ALTER TABLE `#__admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    $list[] = "ALTER TABLE `#__admin_user_profile`
  ADD CONSTRAINT `fk_admin_user_profile` FOREIGN KEY (`id_user`) REFERENCES `#__admin_user` (`id`);";
    $this->execArray($list);

    $list = [];
    //Inserisco un utente di amministrazione predefinito
    $pass = new EOS\System\Security\Password(null);
    $list[] = sprintf("INSERT INTO `#__admin_user`
    (`email`,`username`,`password`,`status`,
    `ins_id`,`ins_date`,`up_id`,`up_date`)
    VALUES('%s', '%s', '%s', 1, 0, '%s', 0, '%s');", 'root@eoscms.it', 'eos.root', $pass->hashPassword('a$98765432'), $this->db->util->nowToDBDateTime(), $this->db->util->nowToDBDateTime());
    $list[] = "INSERT INTO `eos_lang` (`id`, `name`, `status_admin`, `default_admin`, `default_site`, `status_site`, `iso_code`, 
    `language_code`, `datelite_format`, `datefull_format`) VALUES
    (1, 'Italiano (Italian)', 1, 1, 1, 1, 'it', 'it-it', 'd/m/Y', 'd/m/Y H:i:s'),
    (2, 'English', 0, 0, 0, 1, 'en', 'en-en', 'Y-m-d', 'Y-m-d H:i:s')";
    $this->execArray($list);
});
$evolver->execute();
