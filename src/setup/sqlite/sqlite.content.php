<?php

$evolver = $this->newEvolver('content');
$evolver->add(1000, function ()
{
  $list = [];
  // Le chiavi su MySQL sono BIGINT, gli autoincrement su Sqlite possono solo essere INTEGER
  $list[] = 'CREATE TABLE `#__content` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `type` smallint NOT NULL,
  `status` tinyint NOT NULL DEFAULT 0,
  `parent` INTEGER NOT NULL DEFAULT 0,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL);';
  $list[] = 'CREATE TABLE `#__content_lang` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `id_content` INTEGER NOT NULL,
  `id_lang` int NOT NULL,
  `title` varchar(250) NOT NULL COLLATE NOCASE,
  `subtitle` varchar(250) COLLATE NOCASE,
  `content` mediumtext COLLATE NOCASE,
  `options` mediumtext COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL,
   FOREIGN KEY (`id_content`) REFERENCES `#__content` (`id`))';
  $list[] = 'CREATE INDEX `idx_content_lang` ON `#__content_lang` (`id_content`,`id_lang`)';
  $list[] = 'CREATE TABLE `#__content_article` (
  `id_content_lang` INTEGER NOT NULL PRIMARY KEY,
  `author` int NOT NULL,
  `publish_date` datetime NOT NULL,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL,
   FOREIGN KEY (`id_content_lang`) REFERENCES `#__content_lang` (`id`));';
  $list[] = 'CREATE INDEX `idx_content_article_publish_date` ON `#__content_article` (`publish_date`)';
  $list[] = 'CREATE TABLE `#__content_category` (
  `id_content` INTEGER NOT NULL,
  `id_category` INTEGER NOT NULL,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL,
   PRIMARY KEY (`id_content`,`id_category`),
   FOREIGN KEY (`id_content`) REFERENCES `#__content` (`id`));';
  $list[] = 'CREATE TABLE `#__content_section` (
  `id_content` INTEGER NOT NULL,
  `name` varchar(100) NOT NULL COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL,
   PRIMARY KEY (`id_content`),
   FOREIGN KEY (`id_content`) REFERENCES `#__content` (`id`));';
  $list[] = 'CREATE UNIQUE INDEX `idx_content_section_name` ON `#__content_section` (`name`)';
  $this->execArray($list);
});

$evolver->add(1001, function ()
{
    $list = [];
    $list[] = 'ALTER TABLE `#__content` ADD COLUMN `extra_status` TINYINT NOT NULL DEFAULT 0;';
    $list[] = 'ALTER TABLE `#__content` ADD COLUMN `extra` VARCHAR(150) NULL;';
    $this->execArray($list);
});
$evolver->execute();
