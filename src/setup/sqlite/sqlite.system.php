<?php

$evolver = $this->newEvolver('system');
$evolver->add(1000, function ()
{
  $list = [];
  $list[] = 'CREATE TABLE #__admin_user (
 `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `email` varchar(150) NOT NULL COLLATE NOCASE,
  `username` varchar(150) NOT NULL COLLATE NOCASE,
  `password` varchar(100) NOT NULL COLLATE NOCASE,
  `status` tinyint NOT NULL,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL);';
  $list[] = 'CREATE INDEX idx_admin_user_email ON #__admin_user(`email`);';
  $list[] = 'CREATE INDEX idx_admin_user_username ON #__admin_user(`username`);';
  $list[] = 'CREATE TABLE #__admin_user_profile (
  `id_user` int NOT NULL,
  `type` smallint NOT NULL,
  `name` varchar(75) NOT NULL COLLATE NOCASE,
  `surname` varchar(75) NOT NULL COLLATE NOCASE,
  `registration_date` datetime DEFAULT NULL,
  `lastvisit_date` datetime DEFAULT NULL,
  `options` mediumtext NOT NULL COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  up_date datetime NOT NULL,
  PRIMARY KEY (id_user),
  FOREIGN KEY (id_user) REFERENCES #__admin_user(id) ON DELETE CASCADE ON UPDATE CASCADE);';
  $list[] = 'CREATE INDEX idx_admin_user_profile_type on #__admin_user_profile(`type`);';
  $list[] = "CREATE TABLE `#__lang` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `name` varchar(32) NOT NULL COLLATE NOCASE,
  `status_admin` tinyint NOT NULL DEFAULT 0,
  `default_admin` tinyint NOT NULL DEFAULT 0,
  `default_site` tinyint NOT NULL DEFAULT 0,
  `status_site` tinyint NOT NULL DEFAULT 0,
  `iso_code` varchar(2) NOT NULL COLLATE NOCASE,
  `language_code` varchar(5) NOT NULL COLLATE NOCASE,
  `datelite_format` varchar(32) NOT NULL DEFAULT 'Y-m-d' COLLATE NOCASE,
  `datefull_format` varchar(32) NOT NULL DEFAULT 'Y-m-d H:i:s' COLLATE NOCASE)";
  $list[] = "CREATE TABLE `#__router` (
  `path` varchar(190) NOT NULL COLLATE NOCASE,
  `route` varchar(190)  NOT NULL COLLATE NOCASE,
  `lang` varchar(2)  NOT NULL COLLATE NOCASE,
  `container` tinyint NOT NULL DEFAULT 0,
  `menu` tinyint NOT NULL DEFAULT 0,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
   PRIMARY KEY (`path`,`lang`))";
  $list[] = 'CREATE UNIQUE INDEX idx_router_route ON #__router(`route`,`lang`);';
  $list[] = 'CREATE INDEX idx_router_container ON #__router(container);';
  $list[] = 'CREATE TABLE `#__menu` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `name` varchar(100) NOT NULL COLLATE NOCASE,
  `status` tinyint NOT NULL DEFAULT 0,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL)';
  $list[] = 'CREATE UNIQUE INDEX idx_menu_name ON #__menu(`name`);';
  $list[] = 'CREATE TABLE `#__menu_lang` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `id_menu` int NOT NULL,
  `id_lang` int NOT NULL,
  `data` mediumtext NOT NULL COLLATE NOCASE,
  `options` mediumtext NOT NULL COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
   FOREIGN KEY (`id_menu`) REFERENCES `#__menu` (`id`)
   );';
  // Sqlite non indicizza la foreing key
  $list[] = 'CREATE INDEX idx_menu_lang_id_menu on #__menu_lang(id_menu);';
  $list[] = 'CREATE INDEX idx_menu_lang_id_lang  ON #__menu_lang (id_lang)';
  $list[] = 'CREATE TABLE `#__position` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `name` varchar(100) NOT NULL COLLATE NOCASE,
  `widget_class` varchar(190) NOT NULL COLLATE NOCASE,
  `widget_name` varchar(100)  NOT NULL COLLATE NOCASE,
  `status` tinyint NOT NULL DEFAULT 0,
  `ordering` int NOT NULL DEFAULT 0,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL)';
  $list[] = 'CREATE INDEX `idx_position_name` ON `#__position` (`name`)';
  $list[] = 'CREATE INDEX `idx_position_widget_name` ON `#__position` (`widget_name`)';
  $list[] = 'CREATE INDEX `idx_position_ordering` ON `#__position`(`ordering`)';
  $list[] = 'CREATE TABLE `#__position_lang` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `id_position` INTEGER NOT NULL,
  `id_lang` int NOT NULL,
  `filter_type` smallint NOT NULL DEFAULT 0,
  `data` mediumtext NOT NULL COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int DEFAULT NULL,
  `up_date` datetime DEFAULT NULL,
   FOREIGN KEY (`id_position`) REFERENCES `#__position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE)';
  $list[] = 'CREATE INDEX `idx_position_lang_position` ON `#__position_lang` (`id_position`)';
  $list[] = 'CREATE INDEX `idx_position_id_lang` ON `#__position_lang` (`id_lang`)';
  $list[] = 'CREATE TABLE `eos_seo` (
  `id_object` BIGINT NOT NULL,
  `component` varchar(50) NOT NULL COLLATE NOCASE,
  `id_lang` int NOT NULL,
  `slug` varchar(250) NOT NULL COLLATE NOCASE,
  `meta_title` varchar(150) COLLATE NOCASE,
  `meta_description` varchar(300) COLLATE NOCASE,
  `keywords` varchar(300) COLLATE NOCASE,
  `meta_raw` mediumtext COLLATE NOCASE,
  `ins_id` int NOT NULL,
  `ins_date` datetime NOT NULL,
  `up_id` int NOT NULL,
  `up_date` datetime NOT NULL,
   PRIMARY KEY (`component`,`id_object`,`id_lang`))';

  //Inserisco un utente di amministrazione predefinito
  $pass = new EOS\System\Security\Password(null);
  $list[] = sprintf("INSERT INTO `#__admin_user`
    (`email`,`username`,`password`,`status`,
    `ins_id`,`ins_date`,`up_id`,`up_date`)
    VALUES('%s', '%s', '%s', 1, 0, '%s', 0, '%s');", 'root@eoscms.it', 'eos.root', $pass->hashPassword('a$98765432'), $this->db->util->nowToDBDateTime(), $this->db->util->nowToDBDateTime());
  $list[] = "INSERT INTO `eos_lang` (`id`, `name`, `status_admin`, `default_admin`, `default_site`, `status_site`, `iso_code`, 
    `language_code`, `datelite_format`, `datefull_format`) VALUES
    (1, 'Italiano (Italian)', 1, 1, 1, 1, 'it', 'it-it', 'd/m/Y', 'd/m/Y H:i:s'),
    (2, 'English', 0, 0, 0, 1, 'en', 'en-en', 'Y-m-d', 'Y-m-d H:i:s')";
  $this->execArray($list);
});

$evolver->execute();
