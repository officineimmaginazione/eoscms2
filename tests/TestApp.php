<?php

use Slim\Http\Environment;
use Slim\Http\Request;

class TestApp
{

    public static function newApp(): \EOS\System\Application
    {
        $app = new \EOS\System\Application([
            'pageExtension' => '/',
            'languageType' => 'site',
            'dbRouter' => true,
            'multiLanguage' => true,
            'themesPath' => _EOS_PATH_THEMES_]);
        $app->prepare();
        return $app;
    }

    public static function newAppRequest($requestMethod = 'GET', $requestUri = '/'): \Slim\Http\Response
    {
        $app = static::newApp();
        $env = Environment::mock([
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri,
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $app->run(true);
        return $response;
    }

}
