<?php

namespace EOS\System\Cloud\Storage;

require_once('StorageTest.php');

class FileSystemTest extends \EOS\System\Cloud\Storage\StorageTest
{

    protected function setUp()
    {
        parent::setUp();
        $buckets = $this->tempDir . 'buckets' . DIRECTORY_SEPARATOR;
        if (!file_exists($buckets))
        {
            mkdir($buckets);
        }
        if (!file_exists($buckets . 'ecosoft-test1'))
        {
            mkdir($buckets . 'ecosoft-test1');
        }
        $this->object = new FileSystem(['path' => $buckets, 'url' => 'http://www.ecosoft.it/uploads']);
        $this->object->setBucket('ecosoft-test1');
    }

}
