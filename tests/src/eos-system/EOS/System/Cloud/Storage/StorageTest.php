<?php

namespace EOS\System\Cloud\Storage;

use PHPUnit\Framework\TestCase;
use EOS\System\Routing\PathHelper;

class StorageTest extends TestCase
{

    protected $object;
    protected $tempDir;
    protected $app;
    protected $path;

    public function __construct()
    {
        // Devo creare l'oggetto qui altrimenti ho problemi che invia già i dati di header
        $this->app = \TestApp::newApp();
    }

    protected function setUp()
    {

        $this->path = $this->app->getContainer()->get('path');
        $this->tempDir = PathHelper::addDirSep($this->path->getUploadsPath() . 'cloud-test');
        if (!is_dir($this->tempDir))
        {
            mkdir($this->tempDir);
        }
    }

    protected function tearDown()
    {
        
    }

    public function testListBuckets()
    {
        // Se non ho i diritti completi non funziona

        $this->assertNotEmpty($this->object->listBuckets());
    }

    public function testDoesBucketExist()
    {
        $this->assertTrue($this->object->doesBucketExist('ecosoft-test1'));
    }

    public function testCreateFolder()
    {
        $folderTest = ['test-create-folder/', 'test-create-folder/sub/',
            'test-create\'-folder/',
            'test-create`-folder/'];
        foreach ($folderTest as $folder)
        {
            $this->object->createFolder($folder);
            $this->assertTrue($this->object->doesObjectExist($folder));
            $this->object->deleteFolder($folder);
            $this->assertFalse($this->object->doesObjectExist($folder));
        }
    }

    public function testIsEmptyFolder()
    {
        $path = 'test-empty/';
        if (!$this->object->doesObjectExist($path))
        {
            $this->object->createFolder($path);
        }

        $this->assertTrue($this->object->isEmptyFolder($path));
    }

    public function testPutObject()
    {
        $res = $this->object->putObject('test/test.txt', (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601), '');
        $this->assertNotTrue(is_null($res));
        //  var_dump($res);
    }

    public function testPutFile()
    {
        $res = $this->object->putFile('test/put-test.txt', __FILE__, '');
        print_r($res);
        $this->assertNotTrue(is_null($res));
    }

    public function testGetFile()
    {
        $dest = $this->tempDir . 'testGetFile.txt';
        $this->object->getFile('test/test.txt', $dest);
        $this->assertTrue(file_exists($dest));
    }

    public function testGetObjectBody()
    {
        $res = $this->object->getObjectBody('test/test.txt');
        $this->assertNotTrue(empty($res));
    }

    public function testGetObjectStream()
    {
        $st = $this->object->getObjectStream('test/test.txt');
        $this->assertTrue((string) $st === $this->object->getObjectBody('test/test.txt'));
    }

    public function testGetObjectUrl()
    {
        $url = $this->object->getObjectUrl('test/test.txt');
        $this->assertTrue(strlen($url) > 0);
    }

    public function testGetObjectUrlPresigned()
    {
        $url = $this->object->getObjectUrl('test/test.txt', ['presigned_request' => true, 'presigned_expires' => 3]);
        $this->assertTrue(strlen($url) > 0);
    }

    public function testDoesObjectExist()
    {
        $this->assertTrue($this->object->doesObjectExist('test/test.txt'));
    }

    public function testHeadObject()
    {
        $res = $this->object->headObject('test/test.txt');
        $this->assertTrue($res->size > 0);
    }

    public function testDeleteObject()
    {
        $res = $this->object->deleteObject('test/put-test.txt');
        $this->assertNotTrue(is_null($res));
        // Creo una cartella e poi la cancello
        $this->object->createFolder('test-folder/');
        $this->object->deleteObject('test-folder/');
        $this->assertNotTrue($this->object->doesObjectExist('test-folder/'));
    }

    public function testCopyObject()
    {
        $this->object->putObject('test/obj-source.txt', (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601), '');
        $res = $this->object->copyObject('test/obj-source.txt', 'test/obj-copy.txt');
        $this->assertTrue(strlen($res->size) > 0);
    }

    public function testListObjects()
    {
        if (!$this->object->doesObjectExist('test/'))
        {
            $this->object->createFolder('test/');
        }
        $list = $this->object->listObjects('test/');
        $this->assertTrue($list->valid());
        foreach ($list as $v)
        {
            echo $v->key . ($v->isFolderKey() ? ' [folder]' : '') . "\n";
        }
    }

    public function testListObjectsEmptyFolder()
    {
        if (!$this->object->doesObjectExist('test-empty/'))
        {
            $this->object->createFolder('test-empty/');
        }

        $list = $this->object->listObjects('test-empty/');

        $this->assertTrue(iterator_count($list) === 0);
    }

    public function testListObjectsNonEmptyAndValidKey()
    {
        $isEmpty = true;
        $list = $this->object->listObjects('test/');
        foreach ($list as $v)
        {
            $isEmpty = false;
            if (strlen($v->key) === 0)
            {
                $isEmpty = true;
                break;
            }
        }
        $this->assertFalse($isEmpty);
    }

    public function testListObjectsNotFound()
    {
        $this->assertTrue(iterator_count($this->object->listObjects('not-found')) == 0);
    }

    public function testEachObjects()
    {
        if (!$this->object->doesObjectExist('test/'))
        {
            $this->object->createFolder('test/');
        }
        $lo = 0;
        foreach ($this->object->listObjects('test/') as $v)
        {
            $lo++;
        }
        $eo = 0;
        $this->object->eachObjects('test/', function ($o) use (&$eo)
        {
            $eo++;
        });

        $this->assertEquals($lo, $eo);
    }

    // Verifico il limite di file di AWS che il listObject dovrebbe consentirmi di superare
    public function testUp1000()
    {
//        if ($this->object->doesObjectExist('test/limit-test/'))
//        {
//            $this->object->deleteObject('test/limit-test/');
//        }
        $loopLimit = 1100;
        $this->object->createFolder('test/limit-test/');

        if ($this->object->isEmptyFolder('test/limit-test/'))
        {

            for ($i = 1; $i <= $loopLimit; $i++)
            {
                $fname = str_pad($i, 4, "0", STR_PAD_LEFT);
                $this->object->putObject("test/limit-test/$fname.txt", $fname);
            }
        }

        $count = 0;
        echo "test_loop:\n";
        $list = $this->object->listObjects('test/limit-test/');
        foreach ($list as $v)
        {
            $count++;
        }
        $this->assertEquals($count, $loopLimit);
        // Ripeto con il count
        $list = $this->object->listObjects('test/limit-test/');
        $this->assertEquals(iterator_count($list), $loopLimit);
    }

//    public function testCountItem()
//    {
//        $count = 0;
//        $this->object->createFolder('test/folder-extra');
//        $list = $this->object->listObjects('test/');
//        foreach ($list as $v)
//        {
//            $count++;
//        }
//        $this->assertEquals($count, 5);
//    }
}
