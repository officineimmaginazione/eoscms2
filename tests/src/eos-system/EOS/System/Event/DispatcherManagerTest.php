<?php

namespace EOS\System\Event;

use PHPUnit\Framework\TestCase;

class DispatcherManagerTest extends TestCase
{

    /**
     * @var EventDispatcherManager
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new DispatcherManager;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Event\EventDispatcherManager::getDispacher
     * @todo   Implement testGetDispacher().
     */
    public function testGetDispatcher()
    {
        $this->assertNotNull($this->object->getDispatcher());
        $this->assertNotNull($this->object->getDispatcher('prova'));
    }

}
