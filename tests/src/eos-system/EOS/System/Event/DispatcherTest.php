<?php

namespace EOS\System\Event;

use PHPUnit\Framework\TestCase;

class DispatcherTest extends TestCase
{

    /**
     * @var EventDispatcher
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Dispatcher;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::dispatch
     * @todo   Implement testDispatch().
     */
    public function testDispatch()
    {
        $res = false;
        $eventName = 'test.event';
        $this->object->addListener($eventName, function (Event $ev) use (&$res)
        {
            $res = true;
        });
        $this->object->dispatch($eventName);

        $this->assertTrue($res);
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::getListeners
     * @todo   Implement testGetListeners().
     */
    public function testGetListeners()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::getListenerPriority
     * @todo   Implement testGetListenerPriority().
     */
    public function testGetListenerPriority()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::hasListeners
     * @todo   Implement testHasListeners().
     */
    public function testHasListeners()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::addListener
     * @todo   Implement testAddListener().
     */
    public function testAddListener()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::removeListener
     * @todo   Implement testRemoveListener().
     */
    public function testRemoveListener()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::addSubscriber
     * @todo   Implement testAddSubscriber().
     */
    public function testAddSubscriber()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Event\EventDispatcher::removeSubscriber
     * @todo   Implement testRemoveSubscriber().
     */
    public function testRemoveSubscriber()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

}
