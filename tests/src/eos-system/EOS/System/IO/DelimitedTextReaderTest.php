<?php

namespace EOS\System\IO;

use PHPUnit\Framework\TestCase;

class DelimitedTextReaderTest extends TestCase
{

    protected $tempFile;
    protected $testData = [];
    

    protected function setUp()
    {
        $this->tempFile = tempnam(sys_get_temp_dir(), '_test') . '.txt';
        for ($i = 0; $i < 2; $i++)
        {
            $this->testData[] = ['prova,1', (string) $i];
        }
    }

    protected function tearDown()
    {
        if (file_exists($this->tempFile))
        {
            unlink($this->tempFile);
        }
    }

    protected function runTestEOL(string $e, bool $forceAutoDetectLineEndings = false)
    {
        $this->putTemp(",", false, true, $e);
        $reader = new DelimitedTextReader($this->tempFile, $forceAutoDetectLineEndings);
        $reader->csv = false;
        $this->assertTrue(iterator_count($reader) === count($this->testData));
        $ar = iterator_to_array($reader);
        $this->assertEquals(json_encode($ar), json_encode($this->testData));
    }

    protected function putTemp(string $delimiter, bool $csv = true, bool $utf8 = true, string $eol = PHP_EOL)
    {
        $f = new \SplFileObject($this->tempFile, 'w');
        foreach ($this->testData as $r)
        {
            if (!$utf8)
            {
                foreach ($r as &$v)
                {
                    $v = utf8_decode($v);
                }
                unset($v);
            }
            if ($csv)
            {
                $f->fputcsv($r);
            } else
            {
                foreach ($r as &$v)
                {
                    $v = str_replace('\\', '\\\\', $v);
                    $v = str_replace($delimiter, '\\' . $delimiter, $v);
                }
                unset($v);
                $f->fwrite(implode($delimiter, $r) . $eol);
            }
        }
    }

    public function testCSVDefault()
    {

        $this->putTemp(",");
        $reader = new DelimitedTextReader($this->tempFile);
        $this->assertTrue(iterator_count($reader) === count($this->testData));
        $ar = iterator_to_array($reader);
        $this->assertEquals(json_encode($ar), json_encode($this->testData));
    }

    public function testCSVISO88591()
    {
        $this->putTemp(",", true, false);
        $reader = new DelimitedTextReader($this->tempFile);
        $reader->utf8 = false;
        $this->assertTrue(iterator_count($reader) === count($this->testData));
        $ar = iterator_to_array($reader);
        $this->assertEquals(json_encode($ar), json_encode($this->testData));
    }

    public function testTabSeparator()
    {

        $this->putTemp("\t", false);
        $reader = new DelimitedTextReader($this->tempFile);
        $reader->delimiter = "\t";
        $reader->csv = false;
        $this->assertTrue(iterator_count($reader) === count($this->testData));
        $ar = iterator_to_array($reader);
        $this->assertEquals(json_encode($ar), json_encode($this->testData));
    }

    public function testCommaEspaceSeparator()
    {
        $this->putTemp(",", false);
        $reader = new DelimitedTextReader($this->tempFile);
        $reader->csv = false;
        $this->assertTrue(iterator_count($reader) === count($this->testData));
        $ar = iterator_to_array($reader);
        $this->assertEquals(json_encode($ar), json_encode($this->testData));
    }

    public function testEOL_Linux()
    {
        $this->runTestEOL("\n");
    }

    public function testEOL_Excel()
    {
        $this->runTestEOL("\r", true);
    }

    public function testEOL_Windows()
    {
        $this->runTestEOL("\r\n");
    }

}
