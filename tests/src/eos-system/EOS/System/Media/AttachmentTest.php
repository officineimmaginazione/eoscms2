<?php

namespace EOS\System\Media;

use PHPUnit\Framework\TestCase;
use EOS\System\Util\StringHelper;

class AttachmentTest extends TestCase
{

    protected $object;
    protected $tmp;
    protected $app;

    public function __construct()
    {
        // Devo creare l'oggetto qui altrimenti ho problemi che invia già i dati di header
        $this->app = \TestApp::newApp();
    }

    protected function setUp()
    {
        $this->object = new \EOS\System\Media\Attachment($this->app->getContainer(), 'media.test');
        $this->object->setMimeList(['txt']);
        $this->tmp = $this->object->getTemp();
    }

    protected function tearDown()
    {
        
    }

    public function testPutFile()
    {
        $ft = $this->tmp->putStream((new \DateTimeImmutable('now'))->format(\DateTime::ISO8601), 'txt');
        $f = $this->object->putFile($ft->path, 9999, 'Name.txT');
        $this->assertNotEmpty($f->path);
        $this->assertNotEmpty($f->id);
    }

    public function testPutStream()
    {
        $body = (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601);
        $f = $this->object->putStream($body, 10001, 'text/plain', strlen($body), 'prova.txt');
        $this->assertNotEmpty($f->path);
        $this->assertNotEmpty($f->id);
    }

    public function testFormatName()
    {
        $body = (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601);
        $fl = [Storage::NAME_FORMAT_ORIGIN, Storage::NAME_FORMAT_OFFUSCATE,
            Storage::NAME_FORMAT_WITH_ID, Storage::NAME_FORMAT_ONLY_ID];
        foreach ($fl as $f)
        {
            $this->object->setNameFormat($f);
            $f = $this->object->putStream($body, 1000, 'text/plain', strlen($body), $f . '_prova.txt');
            $this->assertNotEmpty($f->path);
        }
    }

    public function testGetStream()
    {
        $body = (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601);
        $f = $this->object->putStream($body, 10001, 'text/plain', strlen($body), 'prova.txt');
        $bodyR = $this->object->getStream($f->idObject, $f->id);
        $this->assertNotEmpty($bodyR);
    }

    public function testGetObjectNull()
    {
        $this->assertEmpty($this->object->getObject(0, 0));
    }

    public function testDeleteAll()
    {
        $this->object->deleteAll(9999);
        $this->object->deleteAll(1000);
        $this->object->deleteAll(10001);
        $this->assertTrue(true);
    }

}
