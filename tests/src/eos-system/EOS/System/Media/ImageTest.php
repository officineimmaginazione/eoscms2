<?php

namespace EOS\System\Media;

use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{

    protected $object;
    protected $tmp;
    protected $app;

    public function __construct()
    {
        // Devo creare l'oggetto qui altrimenti ho problemi che invia già i dati di header
        $this->app = \TestApp::newApp();
    }

    public function getRemoteImage()
    {
        return file_get_contents('https://www.google.it/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png');
    }

    protected function setUp()
    {
        $this->object = new \EOS\System\Media\Image($this->app->getContainer(), 'media.test');
        $this->object->setMimeList(['jpg']);
        $this->tmp = $this->object->getTemp();
    }

    protected function tearDown()
    {
        
    }

    public function testPutFile()
    {
        $ft = $this->tmp->putStream($this->getRemoteImage(), 'png');
        $f = $this->object->putFile($ft->path, 9999, 0, 0, 'test.png');
        $this->assertNotEmpty($f->path);
        $this->assertNotEmpty($f->id);
        unlink($ft->path);
    }

    public function testPutStream()
    {
        $body = $this->getRemoteImage();
        $f = $this->object->putStream($body, 10001, 0, 0, strlen($body), 'test.png');
        $this->assertNotEmpty($f->path);
        $this->assertNotEmpty($f->id);
    }

    public function testFormatName()
    {
        $body = $this->getRemoteImage();
        $fl = [Storage::NAME_FORMAT_ORIGIN, Storage::NAME_FORMAT_OFFUSCATE,
            Storage::NAME_FORMAT_WITH_ID, Storage::NAME_FORMAT_ONLY_ID];
        foreach ($fl as $f)
        {
            $this->object->setNameFormat($f);
            $f = $this->object->putStream($body, 1000, 0, 0, strlen($body), $f . '_prova.png');
            $this->assertNotEmpty($f->path);
        }
    }

    public function testGetStream()
    {
        $body = $this->getRemoteImage();
        $f = $this->object->putStream($body, 10001, 0, 0, strlen($body), 'test.png');
        $bodyR = $this->object->getStream($f->idObject, $f->id, Image::IMAGE_HR);
        $this->assertNotEmpty($bodyR);
    }

    public function testGetObjectNull()
    {
        $this->assertEmpty($this->object->getObject(0, 0, Image::IMAGE_HR));
    }

    public function testDeleteAll()
    {
        $this->object->deleteAll(9999);
        $this->object->deleteAll(1000);
        $this->object->deleteAll(10001);
        $this->assertTrue(true);
    }

}
