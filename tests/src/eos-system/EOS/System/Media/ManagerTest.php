<?php

namespace EOS\System\Media;

use PHPUnit\Framework\TestCase;
use EOS\System\Util\StringHelper;

class ManagerTest extends TestCase
{

    protected $object;
    protected $app;

    public function __construct()
    {
        // Devo creare l'oggetto qui altrimenti ho problemi che invia già i dati di header
        $this->app = \TestApp::newApp();
    }

    protected function setUp()
    {
        $this->object = new Manager($this->app->getContainer(), 'media-test');
    }

    protected function tearDown()
    {
        
    }

    public function testGetTempPath()
    {
        $this->assertTrue(file_exists($this->object->getTempPath()));
    }

    public function testGetTempUrl()
    {
        $this->assertTrue(StringHelper::startsWith($this->object->getTempUrl(), $this->app->getContainer()->get('path')->getUploadsUrl()));
    }

    public function testNewTempName()
    {
        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f = $this->object->newTempName($ext, $suffix);
        $this->assertTrue(StringHelper::endsWith($f, $suffix . $ext));
    }

    public function testNewTempNameLoop()
    {
        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f1 = $this->object->newTempName($ext, $suffix);
        $res = true;
        for ($i = 0; $i < 100; $i++)
        {
            $f2 = $this->object->newTempName($ext, $suffix);
            if ($f1 == $f2)
            {
                $res = false;
                break;
            }
        }
        $this->assertTrue($res);
    }

    public function testNewTempFile()
    {
        $ext = '.jpg';
        $f = $this->object->newTempFile($ext);
        $this->assertFalse(file_exists($f->path));
        $this->assertTrue(StringHelper::endsWith($f->path, $ext));
        $ext = 'jpg';
        $f = $this->object->newTempFile($ext);
        $this->assertFalse(file_exists($f->path));
        $this->assertTrue(StringHelper::endsWith($f->path, '.' . $ext));
    }

    public function testNewTempFileLoop()
    {

        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f1 = $this->object->newTempFile($ext, $suffix);
        $res = true;
        for ($i = 0; $i < 100; $i++)
        {
            $f2 = $this->object->newTempFile($ext, $suffix);
            if ($f1->path == $f2->path)
            {
                $res = false;
                break;
            }
        }
        $this->assertTrue($res);
    }

    public function testClearTempPath()
    {
        $this->object->clearTempPath(1);
        $fi = new \FilesystemIterator($this->object->getTempPath(), \FilesystemIterator::SKIP_DOTS);
        $count = 0;
        foreach ($fi as $file)
        {
            if ($file->getFilename() !== '.DS_Store')
            {
                $count++;
            }
        }
        $this->assertEquals($count, 0);
    }

}
