<?php

namespace EOS\System\Media;

use PHPUnit\Framework\TestCase;
use EOS\System\Util\StringHelper;

class TempTest extends TestCase
{

    protected $object;
    protected $app;

    public function __construct()
    {
        // Devo creare l'oggetto qui altrimenti ho problemi che invia già i dati di header
        $this->app = \TestApp::newApp();
    }

    protected function setUp()
    {
        $this->object = new \EOS\System\Media\Temp($this->app->getContainer(), 'media.test');
        $this->object->setMimeList(['txt' => 'text/plain']);
    }

    protected function tearDown()
    {
        
    }

    public function testGetPath()
    {
        $this->assertTrue(file_exists($this->object->getPath()));
    }

    public function testGetUrl()
    {
        $this->assertTrue(StringHelper::startsWith($this->object->getUrl(), $this->app->getContainer()->get('path')->getUploadsUrl()));
    }

    public function testNewTempName()
    {
        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f = $this->object->newTempName($ext, $suffix);
        $this->assertTrue(StringHelper::endsWith($f, $suffix . $ext));
    }

    public function testNewTempNameLoop()
    {
        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f1 = $this->object->newTempName($ext, $suffix);
        $res = true;
        for ($i = 0; $i < 100; $i++)
        {
            $f2 = $this->object->newTempName($ext, $suffix);
            if ($f1 == $f2)
            {
                $res = false;
                break;
            }
        }
        $this->assertTrue($res);
    }

    public function testNewTempResource()
    {
        $ext = '.jpg';
        $f = $this->object->newTempObject($ext);
        $this->assertFalse(file_exists($f->path));
        $this->assertTrue(StringHelper::endsWith($f->path, $ext));
        $ext = 'jpg';
        $f = $this->object->newTempObject($ext);
        $this->assertFalse(file_exists($f->path));
        $this->assertTrue(StringHelper::endsWith($f->path, '.' . $ext));
    }

    public function testNewTempResourceLoop()
    {

        $ext = '.jpg';
        $suffix = 'prova_1234';
        $f1 = $this->object->newTempObject($ext, $suffix);
        $res = true;
        for ($i = 0; $i < 100; $i++)
        {
            $f2 = $this->object->newTempObject($ext, $suffix);
            if ($f1->path == $f2->path)
            {
                $res = false;
                break;
            }
        }
        $this->assertTrue($res);
    }

    public function testClear()
    {
        $this->object->clear(1);
        $fi = new \FilesystemIterator($this->object->getPath(), \FilesystemIterator::SKIP_DOTS);
        $count = 0;
        foreach ($fi as $file)
        {
            if ($file->getFilename() !== '.DS_Store')
            {
                $count++;
            }
        }
        $this->assertEquals($count, 0);
    }

    public function testPutStream()
    {
        $body = (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601);
        $f = $this->object->putStream($body, 'txt');
        $this->assertTrue(file_exists($f->path));
    }

    public function testPutFile()
    {
        $body = (new \DateTimeImmutable('now'))->format(\DateTime::ISO8601);
        $f = $this->object->putStream($body, 'txt');
        $f2 = $this->object->putFile($f->path);
        $this->assertTrue(file_exists($f2->path));
    }

}
