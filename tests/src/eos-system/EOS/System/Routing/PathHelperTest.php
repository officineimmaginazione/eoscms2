<?php

namespace EOS\System\Routing;


use PHPUnit\Framework\TestCase;

class PathHelperTest extends TestCase
{

    protected $compPath;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->compPath = _EOS_PATH_APP_ . 'EOS' . DIRECTORY_SEPARATOR . 'Components' . DIRECTORY_SEPARATOR;
        $this->compPathOverride = _EOS_PATH_DATA_ . 'override' . DIRECTORY_SEPARATOR . 'eos-site' . DIRECTORY_SEPARATOR . 'EOS' . DIRECTORY_SEPARATOR . 'Components' . DIRECTORY_SEPARATOR;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Routing\PathHelper::dirSepToSlash
     * @todo   Implement testDirSepToSlash().
     */
    public function testDirSepToSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::slashToDirSep
     * @todo   Implement testSlashToDirSep().
     */
    public function testSlashToDirSep()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::addDirSep
     * @todo   Implement testAddDirSep().
     */
    public function testAddDirSep()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::removeDirSep
     * @todo   Implement testRemoveDirSep().
     */
    public function testRemoveDirSep()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::addTrailingSlash
     * @todo   Implement testAddTrailingSlash().
     */
    public function testAddTrailingSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::addLeadingSlash
     * @todo   Implement testAddLeadingSlash().
     */
    public function testAddLeadingSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::removeTrailingSlash
     * @todo   Implement testRemoveTrailingSlash().
     */
    public function testRemoveTrailingSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::removeLeadingSlash
     * @todo   Implement testRemoveLeadingSlash().
     */
    public function testRemoveLeadingSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::removeSlash
     * @todo   Implement testRemoveSlash().
     */
    public function testRemoveSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::removeExtension
     * @todo   Implement testRemoveExtension().
     */
    public function testRemoveExtension()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::addSlash
     * @todo   Implement testAddSlash().
     */
    public function testAddSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::explodeSlash
     * @todo   Implement testExplodeSlash().
     */
    public function testExplodeSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::explodeDirSep
     * @todo   Implement testExplodeDirSep().
     */
    public function testExplodeDirSep()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::implodeSlash
     * @todo   Implement testImplodeSlash().
     */
    public function testImplodeSlash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::implodeDirSep
     * @todo   Implement testImplodeDirSep().
     */
    public function testImplodeDirSep()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Routing\PathHelper::realComponentPath
     * @todo   Implement testRealComponentPath().
     */
    public function testRealComponentPath()
    {
        $this->assertEquals(PathHelper::realComponentPath('sYstem', $this->compPath), $this->compPath . 'System' . DIRECTORY_SEPARATOR);
    }

    public function testRealComponentPerformance()
    {
        clearstatcache();
        for ($i = 0; $i < 100000; $i++)
        {
            PathHelper::realComponentPath('sYstem', $this->compPath);
        }

        $this->assertTrue(true);
    }


    public function testResolveFileName()
    {
        $this->assertEquals(PathHelper::resolveFileName($this->compPath, $this->compPath, ''), $this->compPath);
    }

    public function testResolveFileNameArray()
    {
        $this->assertEquals(PathHelper::resolveFileNameArray($this->compPath, ['', $this->compPath], ''), $this->compPath);
    }
    
    public function testResolveComponentName()
    {
        $this->assertEquals(PathHelper::resolveComponentName('sYstem', $this->compPath, $this->compPath), 'System');
    }

    public function testResolveComponentNamePerformance()
    {
        clearstatcache();
        for ($i = 0; $i < 100000; $i++)
        {
            PathHelper::resolveComponentName('sYstem', $this->compPath, $this->compPath);
        }

        $this->assertTrue(true);
    }

    public function testResolveComponentList()
    {
        $list = PathHelper::resolveComponentList($this->compPath, $this->compPathOverride);
        $this->assertNotEmpty($list);
    }

}
