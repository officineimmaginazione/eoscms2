<?php

namespace EOS\System\Security;

use PHPUnit\Framework\TestCase;

class CryptHelperTest extends TestCase
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {

    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Security\CryptHelper::randomBytes
     * @todo   Implement testRandomBytes().
     */
    public function testRandomBytes()
    {
       $this->assertTrue(strlen(CryptHelper::randomBytes(32)) === 32);
    }

    /**
     * @covers EOS\System\Security\CryptHelper::randomString
     * @todo   Implement testRandomString().
     */
    public function testRandomString()
    {
        // Controllo lunghezza
        $this->assertTrue(strlen(CryptHelper::randomString(16)) === 32);
        $this->assertNotEquals(CryptHelper::randomString(16), CryptHelper::randomString(16));
    }

    /**
     * @covers EOS\System\Security\CryptHelper::hashEquals
     * @todo   Implement testHashEquals().
     */
    public function testHashEquals()
    {
       $this->assertTrue(CryptHelper::hashEquals('abcd', 'abcd'));
    }

}
