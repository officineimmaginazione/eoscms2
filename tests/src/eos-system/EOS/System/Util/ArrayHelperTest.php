<?php

namespace EOS\System\Util;

use PHPUnit\Framework\TestCase;

class ArrayHelperTest extends TestCase
{

    /**
     * @var ArrayHelper
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new ArrayHelper;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    public function testGetValue()
    {
        $data['field'] = 'value';
        $this->assertEquals(ArrayHelper::getValue($data, 'field'), 'value');
        $this->assertEquals(ArrayHelper::getValue($data, 'field-n', 'df'), 'df');
    }

    public function testGetStr()
    {
        $data['field'] = 'value';
        $this->assertEquals(ArrayHelper::getStr($data, 'field'), 'value');
        $this->assertEquals(ArrayHelper::getStr($data, 'field-n', 'df'), 'df');
    }

    public function testGetInt()
    {
        $data['field'] = '0';
        $this->assertEquals(ArrayHelper::getInt($data, 'field'), 0);
        $this->assertEquals(ArrayHelper::getInt($data, 'field-n', 2), 2);
    }

    public function testGetFloat()
    {
        $data['field'] = '1.1';
        $this->assertEquals(ArrayHelper::getFloat($data, 'field'), 1.1);
        $this->assertEquals(ArrayHelper::getFloat($data, 'field-n', 2), 2);
    }

    public function testGetBool()
    {
        $data['field'] = false;
        $this->assertEquals(ArrayHelper::getBool($data, 'field'), false);
        $this->assertEquals(ArrayHelper::getBool($data, 'field-n', true), true);
    }

    public function testGetArray()
    {
        $data['field'] = ['a'];
        $this->assertEquals(ArrayHelper::getArray($data, 'field'), ['a']);
        $this->assertEquals(ArrayHelper::getArray($data, 'field-n', ['b']), ['b']);
    }

    public function testSetValueSkipDef()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testSetStrSkipDef()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testSetIntSkipDef()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testSetBoolSkipDef()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testFromJSON()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testToJSON()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testGetMdValue()
    {
        $mdArray[0]['name'][0] = 'ok';
        $this->assertNull(ArrayHelper::getMdValue($mdArray, []));
        $this->assertTrue(is_array(ArrayHelper::getMdValue($mdArray, [0])));
        $this->assertNull(ArrayHelper::getMdValue($mdArray, [1]));
        $this->assertEquals(ArrayHelper::getMdValue($mdArray, [0, 'name', 0]), 'ok');
    }

    public function testGetMdStr()
    {
        $mdArray[0]['name'] = 'ok';
        $this->assertEquals(ArrayHelper::getMdStr($mdArray, [0, 'name.err'], 'def'), 'def');
        $this->assertEquals(ArrayHelper::getMdStr($mdArray, [0, 'name']), 'ok');
    }

    public function testGetMdInt()
    {
        $mdArray[0]['name'] = 10;
        $this->assertEquals(ArrayHelper::getMdInt($mdArray, [0, 'name.err'], 100), 100);
        $this->assertEquals(ArrayHelper::getMdInt($mdArray, [0, 'name']), 10);
    }

    public function testGetMdBool()
    {
        $mdArray[0]['name'] = true;
        $this->assertEquals(ArrayHelper::getMdBool($mdArray, [0, 'name.err'], false), false);
        $this->assertEquals(ArrayHelper::getMdBool($mdArray, [0, 'name']), true);
    }

    public function testGetMdArray()
    {
        $mdArray[0]['name'] = ['a'];
        $this->assertEquals(ArrayHelper::getMdArray($mdArray, [0, 'name.err'], []), []);
        $this->assertEquals(ArrayHelper::getMdArray($mdArray, [0, 'name']), ['a']);
    }

}
