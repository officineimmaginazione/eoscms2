<?php

namespace EOS\System\Util;

use PHPUnit\Framework\TestCase;

class DateTimeHelperTest extends TestCase
{

    /**
     * @var DateTimeHelper
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new DateTimeHelper;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::incDay
     * @todo   Implement testIncDay().
     */
    public function testIncDay()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::incMonth
     * @todo   Implement testIncMonth().
     */
    public function testIncMonth()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::incYear
     * @todo   Implement testIncYear().
     */
    public function testIncYear()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::now
     * @todo   Implement testNow().
     */
    public function testNow()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::date
     * @todo   Implement testDate().
     */
    public function testDate()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::startOfAMonth
     * @todo   Implement testStartOfAMonth().
     */
    public function testStartOfAMonth()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::endOfAMonth
     * @todo   Implement testEndOfAMonth().
     */
    public function testEndOfAMonth()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::dateOf
     * @todo   Implement testDateOf().
     */
    public function testDateOf()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::compareDate
     * @todo   Implement testCompareDate().
     */
    public function testCompareDate()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::formatISO
     * @todo   Implement testFormatISO().
     */
    public function testFormatISO()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @covers EOS\System\Util\DateTimeHelper::createFromISO
     * @todo   Implement testCreateFromISO().
     */
    public function testCreateFromISO()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

}
