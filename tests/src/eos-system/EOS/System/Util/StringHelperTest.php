<?php

namespace EOS\System\Util;

use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::fastHtml
     * @todo   Implement testFastHtml().
     */
    public function testFastHtml()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::startsWith
     * @todo   Implement testStartsWith().
     */
    public function testStartsWith()
    {
        $this->assertTrue(StringHelper::startsWith('test url with space €', 'test'));
        $this->assertFalse(StringHelper::startsWith('test url with space €', 'false start'));
    }

    /**
     * @covers EOS\System\Util\StringHelper::encodeHtml
     * @todo   Implement testEncodeHtml().
     */
    public function testEncodeHtml()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::decodeHtml
     * @todo   Implement testDecodeHtml().
     */
    public function testDecodeHtml()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::encodeHtmlArray
     * @todo   Implement testEncodeHtmlArray().
     */
    public function testEncodeHtmlArray()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::printHtml
     * @todo   Implement testPrintHtml().
     */
    public function testPrintHtml()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::encodeUrl
     * @todo   Implement testEncodeUrl().
     */
    public function testEncodeUrl()
    {
        $this->assertEquals(StringHelper::encodeUrl('test url with space €'), 'test+url+with+space+%E2%82%AC');
    }

    /**
     * @covers EOS\System\Util\StringHelper::decodeUrl
     * @todo   Implement testDecodeUrl().
     */
    public function testDecodeUrl()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeUrl
     * @todo   Implement testSanitizeUrl().
     */
    public function testSanitizeUrl()
    {
        $this->assertEquals(StringHelper::sanitizeUrl('--test--of_value_€€€_日本語'), '-test-of-value-€€€-日本語');
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeUrlArray
     * @todo   Implement testSanitizeUrlArray().
     */
    public function testSanitizeUrlArray()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeUrlSegment
     * @todo   Implement testSanitizeUrlSegment().
     */
    public function testSanitizeUrlSegment()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeField
     * @todo   Implement testSanitizeField().
     */
    public function testSanitizeField()
    {
        $this->assertEquals(StringHelper::sanitizeField('<script type="text/javascript">alert("XSS")</script>'), 'alert("XSS")');
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeFieldMultiline
     * @todo   Implement testSanitizeFieldMultiline().
     */
    public function testSanitizeFieldMultiline()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeFieldArray
     * @todo   Implement testSanitizeFieldArray().
     */
    public function testSanitizeFieldArray()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::htmlPreview
     * @todo   Implement testHtmlPreview().
     */
    public function testHtmlPreview()
    {
        
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeFileName
     * @todo   Implement sanitizeFileName().
     */
    public function testSanitizeFileName()
    {
        //  $this->assertEquals(StringHelper::sanitizeFileName('<script type="text/javascript">alert("XSS")</script>'), 'alert("XSS")');
        $this->assertEquals(StringHelper::sanitizeFileName('C:\root\test.jpg'), 'Croottest.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('\\root\test.jpg'), 'roottest.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('/root/test.jpg'), 'roottest.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('../../root/test.jpg'), 'roottest.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('c:../../root/test.jpg'), 'croottest.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName(' città.jpg'), 'città.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName(' with space.jpg'), 'with space.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('বাংলা.jpg'), 'বাংলা.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('..বাংলা.jpg..'), 'বাংলা.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName(' বাংলা.jpg.php '), 'বাংলাjpg.php');
        $this->assertEquals(StringHelper::sanitizeFileName('città.jpg', FALSE), 'citta.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName(' città.jpg', FALSE), 'citta.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName(' èéà.jpg', FALSE), 'eea.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('ùûúūуũưǔųŭůűǖǜǚǘবাংলা.jpg', FALSE), 'uuuuuuuuuuuuuuu.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('città..jpg', FALSE), 'citta.jpg');
        $this->assertEquals(StringHelper::sanitizeFileName('.htaccess', FALSE), 'htaccess');
    }

    /**
     * @covers EOS\System\Util\StringHelper::sanitizeFileName
     * @todo   Implement sanitizeFileName().
     */
    public function testConvertToInt()
    {
        $this->assertEquals(StringHelper::convertToInt('12345', -1), 12345);
        $this->assertEquals(StringHelper::convertToInt(12345, -1), 12345);
        $this->assertEquals(StringHelper::convertToInt(42, -1), 42);
        $this->assertEquals(StringHelper::convertToInt('1,2,ddd4/', -1), -1);
        $this->assertEquals(StringHelper::convertToInt('af3', -1), -1);
        $this->assertEquals(StringHelper::convertToInt(10.3, -1), -1);
    }

}
